package cn.js189.common.domain;

import lombok.Data;
import java.io.Serializable;
import java.util.Set;

/**
 * 渠道权限
 */
@Data
public class EuaVo implements Serializable {
	
	private static final long serialVersionUID = 584730022192857507L;
	
	/** 接口编码 */
	private String actionCode;
	
	/** 区域编号列表 */
	private Set<String> areaCodeList;
	
	/** 区域编号 */
	private String areaCode;
	
	/** 路由 */
	private String routeFlag;
	
}
