package cn.js189.common.domain;

import cn.js189.common.util.Utils;
import org.apache.commons.httpclient.methods.PostMethod;

import java.io.IOException;

/**
 * 客户端-应答消息封装类
 * 
 * @author zhangpz
 * 
 */
public class Response {

    private String retCode;
    private String errorMsg;
    private int httpStatusCode;
    private Object entity;
    private PostMethod postMethod;

    public Response(PostMethod postMethod, int httpStatusCode) {
        this.retCode = null;
        this.errorMsg = null;
        this.httpStatusCode = -1;
        this.entity = null;
        this.httpStatusCode = httpStatusCode;
        this.postMethod = postMethod;
    }

    public String getCode() {
        if (this.httpStatusCode == 200) {
            this.retCode = postMethod.getResponseHeader("bssRetCode")
                    .getValue();
        } else {
            this.retCode = "HTTP" + this.httpStatusCode;
        }
        return this.retCode;
    }

    public String getErrorMsg() throws ServiceClientException {
        if (this.retCode.startsWith("HTTP")) {
            this.errorMsg = "服务器返回的HTTP状态码错误:" + this.httpStatusCode;
        } else if (!this.retCode.equals("0")) {
            this.errorMsg = getEntityAsString();
        }
        return this.errorMsg;
    }

    public String getEntityAsString() throws ServiceClientException {
        if (this.entity == null)
            try {
                this.entity = Utils.getString(postMethod
                        .getResponseBodyAsStream(), "UTF-8");//新计费接口返回类型
            } catch (IOException e) {
                throw new ServiceClientException("取返回string IO异常:", e);
            }
        return (String) this.entity;
    }
    public String getEntityAsString(String codeStr) throws ServiceClientException {
        if (this.entity == null)
            try {
                this.entity = Utils.getString(postMethod
                        .getResponseBodyAsStream(), codeStr);//新计费接口返回类型
            } catch (IOException e) {
                throw new ServiceClientException("取返回string IO异常:", e);
            }
        return (String) this.entity;
    }
    
    public Object getEntityAsObject() throws ServiceClientException {
        if (this.entity == null) {
            Object obj = null;
            try {
                obj = Utils.getObject(postMethod.getResponseBodyAsStream());
            } catch (IOException e) {
                throw new ServiceClientException("取返回object IO异常:", e);
            } catch (ClassNotFoundException e) {
                throw new ServiceClientException("取返回object 无此class异常:", e);
            }
            this.entity = obj;
        }
        return this.entity;
    }

    public Object getEntity() throws ServiceClientException {
        if (this.entity == null) {
            Object obj = null;
            try {
                obj = Utils.getObject(postMethod.getResponseBodyAsStream());
            } catch (IOException e) {
                throw new ServiceClientException("取返回object IO异常:", e);
            } catch (ClassNotFoundException e) {
                throw new ServiceClientException("取返回object 无此class异常:", e);
            }
            this.entity = obj;
        }
        return this.entity;
    }

}

