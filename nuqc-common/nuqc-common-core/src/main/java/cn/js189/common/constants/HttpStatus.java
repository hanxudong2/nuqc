package cn.js189.common.constants;

/**
 * 返回状态码
 * 
 * @author hxd
 */
public class HttpStatus
{
    /**
     * 操作成功
     */
    public static final int SUCCESS = 200;

    /**
     * 对象创建成功
     */
    public static final int CREATED = 201;

    /**
     * 请求已经被接受
     */
    public static final int ACCEPTED = 202;

    /**
     * 操作已经执行成功，但是没有返回数据
     */
    public static final int NO_CONTENT = 204;

    /**
     * 资源已被移除
     */
    public static final int MOVED_PERM = 301;

    /**
     * 重定向
     */
    public static final int SEE_OTHER = 303;

    /**
     * 资源没有被修改
     */
    public static final int NOT_MODIFIED = 304;

    /**
     * 参数列表错误（缺少，格式不匹配）
     */
    public static final int BAD_REQUEST = 400;

    /**
     * 未授权
     */
    public static final int UNAUTHORIZED = 401;

    /**
     * 访问受限，授权过期
     */
    public static final String FORBIDDEN = "403";

    /**
     * 资源，服务未找到
     */
    public static final int NOT_FOUND = 404;

    /**
     * 不允许的http方法
     */
    public static final int BAD_METHOD = 405;

    /**
     * 资源冲突，或者资源被锁
     */
    public static final int CONFLICT = 409;

    /**
     * 不支持的数据，媒体类型
     */
    public static final int UNSUPPORTED_TYPE = 415;

    /**
     * 系统内部错误
     */
    public static final int ERROR = 500;

    /**
     * 接口未实现
     */
    public static final int NOT_IMPLEMENTED = 501;
    

    /**
     * 系统警告消息
     */
    public static final int WARN = 601;

    /** 请求常量 */
    public static final String POST = "POST";
    public static final String GET = "GET";
    public static final String PUT = "PUT";
    public static final String DELETED = "DELETE";

    public static final String UTF_ENCODING = "UTF-8";
    
    /**
     * 渠道不再访问范围为空
     */
    public static final String EUA_CHANNEL_ERROR = "euaa_10001";
    
    
    /**
     *  actionCode不在访问范围内
     */
    public static final String EUA_ACTION_CODE_ERROR = "euaa_10002";
    
    /**
     *  区号不在访问范围内
     */
    public static final String EUA_AREA_CODE_ERROR = "euaa_10003";
    
    /**
     * 渠道密钥为空
     */
    public static final String EUA_CHANNEL_KEY_ERROR = "euaa_10004";
    
    /**
     * 参数错误
     */
    public static final String PARAM_ERROR = "uqc_common_10000";
    
    /**
     * 验签失败
     */
    public static final String AUTH_ERROR = "uqc_common_10003";
    
    /**
     * 访问受限，授权过期
     */
    public static final String SQL_FILTER_ERROR = "10005";
}
