/**    
 * @{#} SystemEnum.java Create on 2011-8-26 上午11:28:00    
 *    
 * Copyright (c) 2010 by 创发科技.    
 */
package cn.js189.common.enums;

/**
 * @author <a href="h124436797@yahoo.com.cn">hrg</a>
 * @version 1.0 Description:系统常量配置
 */

public enum SystemEnum {

    CONFIG_PARAM_XML_PATH("com/transfar/inters/param/conf/"), INTERFACE_BEFORE_TASK("1"), // 接口统一前置任务类型
    INTERFACE_AFTER_TASK("2"), // 接口统一后置任务类型
    
    STAFF_CODE_BSS_WAP("-10015"), 
    CHANNEL_ID_BSS_WAP("-10015"),
    STAFF_CODE("-10012"), // 受理员工号
    CHANNEL_ID("-10062"), // 受理渠道号
    CHANNEL_ID_POINT("-10012"), // 受理渠道号(积分专用)
    COL_STAFF_CODE("-10052"), //积分兑换受理员工号
    COL_CHANNEL_ID("-10052"), //积分兑换受理渠道编号
    PROVICE_CODE("000"), // 全省标识
    
    /***************SGW切换参数************************/
    SGW_ORG_ID("80003"), //SGW计费接口切换网厅组织机构号
    SGW_PASSWORD("Dq123_qr"), //SGW计费接口切换网厅密码

    /***************返回系统定义符************************/
    TSR_RESULT, //返回的统一结果代码
    TSR_CODE, //返回的统一结果编码
    TSR_MSG, //返回的统一的消息
    TUXEDO_MSG, //tuxedo返回的额消息

    /*****************计费相关编码***************/
    ORG_ID("20000"), //计费来源标识
    STAFF_ID("1000"), //充值平台操作工号
    PASSWORD("1000"), //充值平台工号口令
    REMOTE_SOURCE("00003"), //支付来源标识
    REMOTE_PASSWORD("CT1000123456"), //密码
    REQ_SOURCE("js10000"),
    REMOTE_SOURCE_SDM("113"), //翼支付来源标识
    REMOTE_PASSWORD_SDM("Ghk$82dHM"), //翼支付密码
    BLANCE_SOURCE("80003"), //余额结转来源标识
    BLANCE_PASSWORD("Dq123_qr"), //余额结转密码
    /*****************bss3.0相关***************/
    BSS_STAFF_ID("-10062"), BSS_CHINNEL_ID("-10062"),

    /*****************爱游戏***************/
    ENJOY_GAME_CHANNEL_ID("1592"),
    
    /** bss 新积分接口相关 **/
    
    /**
     * 同步接口
     */
    BSS_NEW_EVENT_TYPE_SYNC("SYNC"),
    
    /**
     * 异步接口
     */
    BSS_NEW_EVENT_TYPE_ASYNC("ASYNC"),
    
    /**
     * 异常重发
     */
    BSS_NEW_EVENT_TYPE_ERROR("ERROR"),
    
    /**
     * 接口业务编码 积分服务
     */
    BSS_NEW_BUSCODE_POINT_SERV("POINT_SERV"),
    
    /**
     * 接口业务编码 会员服务
     */
    BSS_NEW_BUSCODE_CLUB_SERV("CLUB_SERV"),
    
    /**
     * 接口服务编码 当前积分查询
     */
    BSS_NEW_SERVICECODE_QRY1001("QRY1001"),
    
    /**
     * 接口服务编码 历史积分查询
     */
    BSS_NEW_SERVICECODE_QRY1002("QRY1002"),
    
    
    /**
     * 接口服务编码 延期清零积分余额查询
     */
    BSS_NEW_SERVICECODE_QRY1019("QRY1019"),
    /**
     * 接口业务编码 星级会员
     */
    
    BSS_NEW_BUSCODE_STAR_MEMBER("STAR_MEMBER"),
    
    /**
     * 星级会员基本信息
     */
    BSS_NEW_SERVICECODE_QRY2001("QRY2001"),
    /**
     * 星级会员权益认证
     */
    BSS_NEW_SERVICECODE_QRY2002("QRY2002"),
    /**
     * 星级会员信息以及权益查询
     */
    BSS_NEW_SERVICECODE_QRY2003("QRY2003"),
    /**
     * 会员更变查询
     */
    BSS_NEW_SERVICECODE_QRY2004("QRY2004"),
    /**
     * 行权历史查询
     */
    BSS_NEW_SERVICECODE_QRY2005("QRY2005"),
    /**
     *权益变更查询
     */
    BSS_NEW_SERVICECODE_QRY2006("QRY2006"),
    /**
     * 星级会员权益次数查询
     */
    BSS_NEW_SERVICECODE_QRY2007("QRY2007"),
    
    /**
     * 星级权益及其次数查询接口（该接口只为省内微信公众号使用）
     */
    BSS_NEW_SERVICECODE_QRY3001("QRY3001"),
    /**
     * 权益次数扣减
     */
    BSS_NEW_SERVICECODE_SERV5001("SERV5001"),
    
    /**
     * 权益次数扣减返销
     */
    BSS_NEW_SERVICECODE_SERV5002("SERV5002"),
    
    /**
     * 星级购买后通知接口
     */
    BSS_NEW_SERVICECODE_SERV5003("SERV5003"),
    
    /**
     * 接口服务编码 积分消费历史查询
     */
    BSS_NEW_SERVICECODE_QRY1003("QRY1003"),
    
    /**
     * 接口服务编码 查询设置短信接收号码
     */
    BSS_NEW_SERVICECODE_SERV2007("SERV2007"),
    
    BSS_CHECK_3001("CHECK3001"),
    
    /**
     * 接口服务编码 子俱乐部信息查询
     */
    BSS_NEW_SERVICECODE_QRY1007("QRY1007"),
    
    BSS_NEW_SERVICECODE_QRY1009("QRY1009"),
    
    /**
     * 接口服务编码 子俱乐部信息变动 设置短信接收号码
     */
    BSS_NEW_SERVICECODE_SERV2006("SERV2006"),
    
    
    /**
     * 接口服务编码 子俱乐部变动接口 积分扣减服务
     */
    BSS_NEW_SERVICECODE_SERV2001("SERV2001"),
    /**
     * 接口服务编码 积分抵消
     */
    BSS_NEW_SERVICECODE_SERV2002("SERV2002"),
    
    BSS_NEW_KEY("123456"),
    
    
    /**支付来源标识*/
    BAL_REMOTE_SOURCE("40001"),
    /**各个来源认证口令，用于鉴定来源的合法性（调用不同的服务可以使用不同的口令，具体视实际情况而定）（测试环境传入：CS40001  正式环境传入：WT40001）*/
    BAL_REMOTE_PASSWORD("CS40001"),
    ;
    private String value;

    
    
    private SystemEnum() {

    }

    private SystemEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}
