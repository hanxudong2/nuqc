package cn.js189.common.util.sign;


import cn.hutool.core.text.CharSequenceUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.asymmetric.Sign;
import cn.hutool.crypto.asymmetric.SignAlgorithm;

public class SignUtils {

    /**
     * 签名验证
     * @param data 参数
     * @param signStr 签名字符串
     * @return 验证结果
     */
    public  static boolean validSign(String data,String signStr){
       return CharSequenceUtil.isNotBlank(data) && data.equals(signStr);
    }


}
