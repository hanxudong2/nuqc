/**************************************************************************************** 

 ****************************************************************************************/
package cn.js189.common.constants;

/****************************************************************************************

/**
 * 从配置文件中读取的接口路径
 * @author daill
 *
 */

public class URLKeyContants {
	
	private URLKeyContants(){}
	
    
    /*SGW计费通用url*/
    public static final String UQC_CALLSGWCOMMON_UQL = "uqc.callSgwCommon.uql";
    
    /*账单查询接口*/
    public static final String UQC_CALLNEWBAILLREQ_URL = "uqc.callNewBillReq.url";
    
    /*区域查询接口*/
    public static final String UQC_CALLNEWAREACODE_URL = "uqc.callNewAreaCode.url";
    

    
    /*实时话费查询*/
    public static final String UQC_CALLACCTITEMFQREQ_URL = "uqc_callAcctItemfqReq_url";
    
    

    
    /*充值缴费查询*/
    public static final String UQC_CALLNEWPAYREQ_URL = "uqc_callNewPayReq_url";
    
 
    
    
    /**
     * 2.44 充值-查询业务类型接口
     */
    public static final String UQC_CALLNEWUOTHGETTYPE_URL = "uqc_callNewUOthGetType_url";
    /*#crm国际漫游wsdl*/
    public static final String UQC_CRMFORGCSERVICEIMPL_URL = "uqc_crmForGcServiceImpl_url";
    
    /*#crm客户信息wsdl*/
    public static final String UQC_CUSTOMERSERVICEASERVICE_URL = "uqc_customerServiceAService_url";
    

    /**
     * 计费通用接口
     */
    public static final String UQC_COMMONBILLSERVICE_URL = "uqc_commonBillService_url";

    /*#osswsdl*/
    public static final String UQC_OSSINTERFACEFOR10000SERVICE_URL = "uqc_oSSInterfaceFor10000Service_url";
    
    /*#账单查询*/
    public static final String UQC_CALLBILLQUERY_URL = "uqc.callSgwCommon.uql";

    
    /*#短信通信详单*/
    public static final String UQC_CALLNEWSMTICKET_URL = "uqc.callSgwCommon.uql";
    
    /*#手机语音清单*/
    public static final String UQC_CALLNEWVOICETICKET_URL = "uqc.callSgwCommon.uql";
    
    /*#移动用户上网及数据通信详单查询*/
    public static final String UQC_CALLNEWDATATICKETQRREQ_URL = "uqc.callSgwCommon.uql";
    
    /*#其他增值业务清单*/
    public static final String UQC_CALLNEWVALUETICKETQR_URL = "uqc.callSgwCommon.uql";
    
    /*#校园卡账单-C网清单/手机账单-C网短信/彩信增值清单*/
    public static final String UQC_CALLNEWSMSVALUETICKETQR_URL = "uqc_callNewSmsValueTicketQr_url";
    
    /*#长话话单信息 */
    public static final String UQC_CALLNEWCALLTICKET_URL = "uqc_callNewCallTicket_url";

   
    

    
    
    /*新验证接口*/
    public static final String UQC_NEWVERIFICATION_URL = "uqc_newVerification_url";
    
    /*根据订单号查询订单竣工状态接口*/
    public static final String UQC_ORDERNUMBERQCSTATUS_URL = "uqc_orderNumberQCStatus_url";
    
    /*证件查询接口*/
    public static final String UQC_DOCUMENTQUERY_URL = "uqc_documentQuery_url";
    
    /*新身份资料认证*/
    public static final String UQC_NEWAUTHENTICATIONINFORMATION_URL = "uqc_newAuthenticationInformation_url";
    
    /*客户发送短信号码的查询接口*/
    public static final String UQC_CUSTOMERSENDSMSNUMBERQUERY_URL = "uqc_customerSendSMSNumberQuery_url";
    
    /*根据产品ID查询该用户是否是校园用户*/
    public static final String UQC_CALLCUSTMSG_URL = "uqc_callCustMsg_url";
    public static final String UQC_CALLCUSTMSG_KEY = "uqc_callCustMsg_key";
    /**查询服务实例id*/
    public static final String UQC_QUERYSERVERID ="uqc_queryserverId";
    /**查询销售品介绍*/
    public static final String UQC_QUERYOFFERDESC ="uqc_queryofferdesc";
    /**查询可订购短信提醒*/
    public static final String UQC_QUERYAVALIBALESMS ="uqc_queryAvalibaleSms";
    /**查询已经存在的短信提醒*/
    public static final String UQC_QUERYEXISTSMS ="uqc_queryExistSms";
    /**保障催单流程*/
    public static final String UQC_CALLBARRREPAIR_URL = "uqc_callBarrRepair_url";
    public static final String UQC_CALLBARRREPAIR_NAMESPACE = "uqc_callBarrRepair_nameSpace";
    public static final String UQC_CALLBARRREPAIR_METHODNAME = "uqc_callBarrRepair_methodName";
    /**根据partyId查询未竣工的购物车*/
    public static final String UQC_QUERYORDERNBRBYPARTYID_URL="uqc_queryOrderNbrByPartyId_url";
    /**装移机查询*/
    public static final String UQC_CALLMOVEREPAIRINFO_URL= "uqc_callMoveRepairInfo_url";
    /**根据身份证查询partyId*/
    public static final String UQC_QUERYPARTYIDBYID_URL ="uqc_queryPartyIdById_url";
    /**根据宽带号查最大速率*/
    public static final String UQC_NOCQUERYMAXSPEEDBYNETNUM_URL ="uqc_nocQueryMaxSpeedByNetNum_url";
    /**根据地址查最大速率*/
    public static final String UQC_NOCQUERYMAXSPEEDBYADDR_URL = "uqc_nocQueryMaxSpeedByAddr_url";
    /**一证五卡，接入号查询宽带号地址等*/
    public static final String UQC_QUERYPRODUCT_URL ="uqc_queryproduct_url";
    
    /*积分兑换（话费） buyong*/
    public static final String UQC_INTEGRALEXCHANGEFEE_URL = "uqc_integralExchangeFee_url";
    
    /*积分兑换（发票）*/
    public static final String UQC_INTEGRALEXCHANGEINVOICE_URL = "uqc_integralExchangeInvoice_url";
    
    /*积分兑换（业务）,buyong*/
    public static final String UQC_INTEGRALEXCHANGESERVICE_URL = "uqc_integralExchangeService_url";
    
    /*宽带安装进度查询*/
    public static final String UQC_BROADBANDINSTALLATIONQUERY_URL = "uqc_broadbandInstallationQuery_url";
    
    
    /*扩展查询服务名*/
    public static final String UQC_EXTENDEDQUERYSERVICENAME_URL = "uqc_extendedQueryServiceName_url";
    
    
    /*宽带提速业务是否能开通接口*/
    public static final String UQC_BROADBANDSPEEDISABLEOPEN_URL = "uqc_broadbandSpeedIsAbleOpen_url";
    
    /*宽带提速业务开通接口*/
    public static final String UQC_BROADBANDSPEEDOPEN_URL = "uqc_broadbandSpeedOpen_url";
    
    /*查询一致性接口*/
    public static final String UQC_QUERY_CONSISTENCE_URL = "uqc_query_consistence_url";
    
    /*空中卫士接口*/
    public static final String UQC_SECURE_GUARDIAN_URL = "uqc_secure_guardian_url";
    
    /*根据proid查询账户下所有产品信息接口*/
    public static final String UQC_QRYPRODUCTINFOSBYPRODID_URL = "uqc_qryproductinfosbyprodid_url";
    
	//营销信息查询url
	public static final String CBA_CBAMARKETING_URL="cba_cbamarketing_url";
	
	//回单url
	public static final String CBA_CBAREBACK_URL="cba_cbareback_url";
	//综调投诉建议查询
	public static final String ZD_TOUSUQRY_URL="zd_tousuqry_url";
	public static final String ZD_TOUSUQRY_NAMESPACE="zd_tousuqry_namespace";
	public static final String ZD_TOUSUQRY_METHODNAME="zd_tousuqry_methodname";
	//综调宽带转移进度
	public static final String ZD_ZHUANGYIQRY_URL = "zd_zhuangyi_url";
	public static final String ZD_ZHUANGYIQRY_NAMESPACE="zd_zhuangyiqry_namespace";
	public static final String ZD_ZHUANGYIQRY_METHODNAME="zd_zhuangyiqry_methodname";
	//综调投诉建议
	public static final String ZD_TOUSU_URL ="zd_tousu_url";
	public static final String ZD_TOUSU_NAMESPACE="zd_tousu_namespace";
	public static final String ZD_TOUSU_METHODNAME="zd_tousu_methodname";
	//综调报障接口
	public static final String ZD_BAOZHANG_URL ="zd_baozhang_url";
	public static final String ZD_BAOZHANG_NAMESPACE ="zd_baozhang_namespace";
	public static final String ZD_BAOZHANG_METHODNAME ="zd_baozhang_methodname";
	
	public static final String ZD_QUERYBILL_METHODNAME ="zd_querybill_methodname";

	//账单查询接口
	public static final String UQC_CALLNEWBILLREQ_URL = "uqc_callNewBillReq_url";
	//密码接口
	public static final String UQC_CHANGE_CIPHER_URL = "uqc_changePassword_url";
	public static final String UQC_CHANGE_CIPHER_NAMESPACE = "uqc_changePassword_namespace";
	public static final String UQC_CHANGE_CIPHER_METHODNAME = "uqc_changePassword_methodname";
	//密码重置接口
	public static final String UQC_CALLRESETPASS_URL = "uqc_callResetPass_url";
	//根据产品ID查询已订购的附属销售品
	public static final String UQC_CALLBSSSUBSCRIBEDPROD_URL = "uqc_callBssSubscribedProd_url";
	//根据产品ID查询已订购的附属销售品需要过滤的销售品
	public static final String UQC_SUBOFFERFILTER_ID = "uqc_subOfferFilter_id";
	//VSOP订购关系查询
	public static final String UQC_CALLBSSVSOPORDERRELA_URL = "uqc_callBssVsopOrderRela_url";
	//VSOP申请订购
	public static final String UQC_CALLBSSVSOPORDERAPPL_URL = "uqc_callBssVsopOrderAppl_url";
	//当前积分查询
	public static final String UQC_CALLQUERYSCORE_URL = "uqc_callQueryScore_url";
	/**
     * 窄带查询地址 <br>
     */
    public static final String UQC_CALLNEWNARROWDATAQR_URL = "uqc_callNewNarrowDataQr_url";
    
    /**
     * 互联星空查询 <br>
     */
    public static final String UQC_CALLNEWINFODATAQR_URL = "uqc_callNewInfoDataQr_url";
    
    /**
     * 新业务清单查询 <br>
     */
    public static final String UQC_CALLNEWHTTPSVRTICQR_URL = "uqc_callNewHttpSvrTicQr_url";
    
    /**
     * 电话清单查询 <br>
     */
    public static final String UQC_CALLNEWMESSAGETICKETQR_URL = "uqc_callNewMessageTicketQr_url";
    /**
     * 查询订单信息
     */
    public static final String UQC_QUERYORDERINFO_URL = "uqc_queryOrderInfo_url";
    /**
     * 新装入网查询接口
     */
    public static final String UQC_QUERYNEWINSTALL_URL="uqc_queryNewInstall_url";
    /**
     * adsl和lan密码重置
     */
    public static final String UQC_CALL_ADSL_OR_LAN_RESET_CIPHER_URL = "uqc_callAdslOrLanResetPwd_url";
    /**
     * 算费接口
     */
    public static final String  UQC_CALLCHARGESERVICE_URL = "uqc_callChargeService_url";
    
    /**
     * CRM接口
     */
    public static final String UQC_CALLCUSTOMERSERVICE_URL = "uqc_callCustomerService_url";
    /**
     * 营帐接口
     */
    public static final String UQC_CALLSKYBILLSERVICE_URL = "uqc_callSkyBillService_url";
    /**
     * 主销售品查询
     */
    public static final String UQC_QUERYMAINOFFER_URL = "uqc_queryMainOffer_url";
    /**
     * 生成购物车
     */
    public static final String UQC_SHENGCHENGGOUWUCHE_URL = "uqc_shengChengGouWuChe_url";
    /**
     * 查询附属销售品角色以及服务角色对象信息  19002
     */
    public static final String  UQC_QUERYFORMKTINFO_URL ="uqc_queryForMktInfo_url";
    /**
     * 规则校验
     */
    public static final String UQC_CHECKRULE_URL = "uqc_checkRule_url";
    /**
     *  转换业务结构对象
     */
    public static final String UQC_CONVERTTOXSD_URL = "uqc_convertToXsd_url";
    /**
     * 批量受理
     */
    public static final String UQC_BATCHPREPARESO_URL = "uqc_batchPrepareSo_url";
    /**
     *增量保存并调用规则 
     */
    public static final String UQC_INCSAVECHECKRULE_URL = "uqc_incSaveCheckRule_url";
    /**
     * 提交购物车 19007
     */
    public static final String UQC_OLSAVECHECKRULE_URL = "uqc_olSaveCheckRule_url";
    /**
     *将状态置为可选状态 19008 
     */
    public static final String UQC_UPDATESTATUS_URL = "uqc_updateStatus_url";
    /**
     * 查找CDMA所有附属销售品 19010
     */
    public static final String UQC_QUERYCDMAMKT_URL = "uqc_queryCdmaMkt_url";
    /**
     * 查询产品下的服务实例 19012
     */
    public static final String UQC_QUERYOFFER_URL = "uqc_queryOffer_url";
    /**
     *查询销售品的详细信息，到期时间  
     */
    public static final String UQC_QROFFERDETAIL_URL = "uqc_qrOfferDetail_url";
    /**
     * 费用清单
     */
    public static final String UQC_FEEBILLQR_URL = "uqc_feeBillQr_url";
    /**
     * 根据产品ID查询合账付费的产品列表
     */
    public static final String UQC_QUERYPRODLISTFORBYID_URL = "uqc_queryProdListById_url";
    
    /**
     * 宽带接入类型查询
     */
    public static final String UQC_BROADBANDACCESSTYPE_URL = "uqc_broadbandAccessType_url";
    /**
     * 云宽带加速历史记录查询接口
     */
    public static final String UQC_GETCLOUDBROADBANDPRIVILEGELIST_URL = "uqc_getCloudBroadbandPrivilegeList_url";
    /**
     * squid对应ip
     */
    public static final String UQC_SQUID_IP = "uqc_squid_ip";
    
    /**
     * 宽带续约提档信息查询接口
     */
    public static final String UQC_QUERYBROADEXTEND_URL = "uqc_queryBroadbandExtend_url";
    
    /**
     * 宽带续约到期时间查询接口
     */
    public static final String UQC_ENDDTFORBROADEXTEND_URL = "uqc_endDtForBroadbandExtend_url";
    
    /**
     * 宽带续约校验接口
     */
    public static final String UQC_ISOKBROADEXTEND_URL = "uqc_isOKBroadbandExtend_url";
    
    /**
     * 集团宽带续约校验+提档信息查询接口
     */
    public static final String UQC_QUERYBROADBANDEXTENDINFOFORJT_URL = "uqc_queryBroadbandExtendInfoForJT_url";

    /**
    *新电子发票鉴权接口
    */
   public static final String UQC_NEWAUTHENTICATION_URL = "uqc_newAuthentication_url";
	public static final String UQC_INTEGR_STARMEMBER_URL = "uqc_inter_starMember_url";
	
	
    /**
    *支付宝pdf调用
    */
	public static final String ALI_PDF_URL = "ali_pdf_url";

}
