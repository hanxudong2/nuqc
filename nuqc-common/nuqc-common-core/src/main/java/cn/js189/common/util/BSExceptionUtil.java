package cn.js189.common.util;

import cn.js189.common.enums.BSErrorEnum;
import cn.js189.common.util.exception.BSException;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;

@Slf4j
public class BSExceptionUtil {
	
	private BSExceptionUtil(){}

	public static void throwBillExcepiton(String errorCode, String errorMsg) throws BSException {

		log.error("errorCode: {}  errorMsg: {}", errorCode, errorMsg);
		throw new BSException(errorCode, errorMsg);
	}

	public static JSONObject checkBillResponse(String responseString, String codeKey, String msgKey, String successCode) throws BSException {

		if (StringUtils.isBlank(responseString)) {
			BSExceptionUtil.throwBillExcepiton(BSErrorEnum.BS_RESPONSE_EMPTY.getErrorCode(), BSErrorEnum.BS_RESPONSE_EMPTY.getErrorMsg());
		}

		JSONObject resObj = JSON.parseObject(responseString);

		String errorCode = resObj.getString(codeKey);

		if (!StringUtils.equals(successCode, errorCode)) {
			String errorMsg = resObj.getString(msgKey);
			BSExceptionUtil.throwBillExcepiton(errorCode, errorMsg);
		}

		return resObj;
	}

}
