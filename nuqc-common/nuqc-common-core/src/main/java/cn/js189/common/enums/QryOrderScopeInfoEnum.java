package cn.js189.common.enums;

/**
 * 查询产品范围的枚举类型
 * 
 * @author Mid
 */
public enum QryOrderScopeInfoEnum {
	// 客户订单
	CUSTOMER_ORDER("customerOrder", 1),
	// 客户订单属性
	ORDER_ATTR("orderAttr", 2),
	// 订单经办人信息
	ORDER_HANDLER("orderHandler", 3),
	// 订单联系人信息
	ORDER_CONTACT_INFO("orderContactInfo", 4),
	// 订单项
	ORDER_ITEM("orderItem", 5),
	// 订单项属性
	ORDER_ITEM_ATTR("orderItemAttr", 6),
	// 订单-发展人信息
	ORD_DEV_STAFF_INFO("ordDevStaffInfo", 7),
	//订单实体
	ORD_PROD_INST("ordProdInst", 8);
		
	
	

	// 范围编码
	private String scopeCode;
	// 范围索引
	private int scopeIndex;

	private QryOrderScopeInfoEnum(String scopeCode, int scopeIndex) {
		this.scopeCode = scopeCode;
		this.scopeIndex = scopeIndex;
	}

	public String getScopeCode() {
		return scopeCode;
	}

	public int getScopeIndex() {
		return scopeIndex;
	}
	public static QryOrderScopeInfoEnum getQryOrderScopeInfoEnum(String scopeCode){
	    for(QryOrderScopeInfoEnum k:QryOrderScopeInfoEnum.values()){
	        if(scopeCode.equals(k.scopeCode)){
	            return k;
	        }
	    }
	    return null;
	}
}
