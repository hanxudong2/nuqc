package cn.js189.common.constants;

public class JtdqContants {

	private JtdqContants(){/*empty*/}
	
	public static final String SYS_CODE ="S17080";
	
	public static final String APP_CODE = "P402030101";
	
	public static final String VERSION = "0000";
	
	public static final String JTDQ_SECRET ="a7a45532a66a498294f39368fd19410d";
	
	public static final String SECRET_VERSION ="00";
	
	
	/*  ============== 限购策略信息 start =================  */
	/**
	 * 限购策略系统编码
	 */
	public static final String AST_SYS_CODE = "XGHMD";
	
	/**
	 * 限购策略应用编码
	 */
	public static final String AST_APPCODE = "PURCHASERESTRICTIONS";
	
	/**
	 * 限购策略系统接口版本号
	 */
	public static final int AST_VERSION_PORT = 1;
	
	/**
	 * 限购策略方法编码
	 */
	public static final String AST_METHOD = "purchaseRestrictions";
	
	/**
	 * 限购策略校验
	 */
	public static final String PR_METHOD = "I00700";
	
	public static final String INTERFACE_NAME = "/osa/interface";
	
	/**
	 * 拦截校验
	 */
	public static final String RC_CHECK_METHOD = "I00101";
	
	/*  ============== 限购策略信息 end =================  */

	
	
}
