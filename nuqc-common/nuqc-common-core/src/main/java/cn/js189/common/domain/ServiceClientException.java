package cn.js189.common.domain;

/**
 * 客户端-异常封装类
 * @author zhangpz
 *
 */
public class ServiceClientException extends Exception {
    private static final long serialVersionUID = 4231081627393346132L;
    public ServiceClientException(){
        super();
    }
    public ServiceClientException(String msg){
        super(msg);
    }
    public ServiceClientException(String msg, Throwable e){
        super(msg,e);
    }
    public ServiceClientException(Throwable e){
        super(e);
    }
}
