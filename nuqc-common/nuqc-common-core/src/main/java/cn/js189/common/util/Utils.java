package cn.js189.common.util;

import cn.hutool.core.text.CharSequenceUtil;
import cn.js189.common.constants.ExpConstant;
import cn.js189.common.domain.RespHead;
import cn.js189.common.domain.RespInfo;
import cn.js189.common.util.exception.BSException;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.xpath.DefaultXPath;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

/**
* 客户端-工具类
* @author zhaoxin
*
*/
@Slf4j
public class Utils {
	
	private Utils(){}

    public static final String SUCCESS = "success";
    
    public static final String RSP = "RSP";
    
    private static final Map<String, String> map = new HashMap<>();

    private static final Map<String,String> id5Map = new HashMap<>();
    
    private static final Map<String,String> areaCodeToAreaName = new HashMap<>();
    private static final Map<String,String> aaaRegionToAreaCode = new HashMap<>();

    /** 企信3级地区ID与区号对照集合. */
    public static final Map<String, String> areaCodeMap = new HashMap<>();

    /** 企信四地区ID与OSS对照集合. */
    public static final Map<String, String> areaIdMap = new HashMap<>();
    static {
        aaaRegionToAreaCode.put("0001", "025");
        aaaRegionToAreaCode.put("0002", "0512");
        aaaRegionToAreaCode.put("0003", "0510");
        aaaRegionToAreaCode.put("0004", "0519");
        aaaRegionToAreaCode.put("0005", "0511");
        aaaRegionToAreaCode.put("0006", "0514");
        aaaRegionToAreaCode.put("0007", "0513");
        aaaRegionToAreaCode.put("0008", "0523");
        aaaRegionToAreaCode.put("0009", "0516");
        aaaRegionToAreaCode.put("0010", "0517");
        aaaRegionToAreaCode.put("0011", "0515");
        aaaRegionToAreaCode.put("0012", "0518");
        aaaRegionToAreaCode.put("0013", "0527");

        areaCodeMap.put("025", "3");
        areaCodeMap.put("0511", "4");
        areaCodeMap.put("0510", "15");
        areaCodeMap.put("0512", "20");
        areaCodeMap.put("0513", "26");
        areaCodeMap.put("0514", "33");
        areaCodeMap.put("0515", "39");
        areaCodeMap.put("0516", "48");
        areaCodeMap.put("0517", "60");
        areaCodeMap.put("0518", "63");
        areaCodeMap.put("0519", "69");
        areaCodeMap.put("0523", "79");
        areaCodeMap.put("0527", "84");

        areaIdMap.put("1", "cn");
        areaIdMap.put("2", "js.cn");
        areaIdMap.put("3", "nj.js.cn");
        areaIdMap.put("4", "zj.js.cn");
        areaIdMap.put("5", "dy.zj.js.cn");
        areaIdMap.put("6", "jr.zj.js.cn");
        areaIdMap.put("7", "yz.zj.js.cn");
        areaIdMap.put("8", "sq.zj.js.cn");
        areaIdMap.put("9", "sq.nj.js.cn");
        areaIdMap.put("10", "jn.nj.js.cn");
        areaIdMap.put("11", "ls.nj.js.cn");
        areaIdMap.put("12", "gc.nj.js.cn");
        areaIdMap.put("13", "lh.nj.js.cn");
        areaIdMap.put("14", "jp.nj.js.cn");
        areaIdMap.put("15", "wx.js.cn");
        areaIdMap.put("16", "sq.wx.js.cn");
        areaIdMap.put("17", "yd.yc.js.cn");
        areaIdMap.put("18", "yx.wx.js.cn");
        areaIdMap.put("19", "jy.wx.js.cn");
        areaIdMap.put("20", "sz.js.cn");
        areaIdMap.put("21", "sq.sz.js.cn");
        areaIdMap.put("22", "xp.yz.js.cn");
        areaIdMap.put("23", "wj.sz.js.cn");
        areaIdMap.put("24", "hm.nt.js.cn");
        areaIdMap.put("25", "qd.nt.js.cn");
        areaIdMap.put("26", "nt.js.cn");
        areaIdMap.put("27", "tz.nt.js.cn");
        areaIdMap.put("28", "sq.nt.js.cn");
        areaIdMap.put("29", "rd.nt.js.cn");
        areaIdMap.put("30", "rg.nt.js.cn");
        areaIdMap.put("31", "ha.nt.js.cn");
        areaIdMap.put("32", "yz.yz.js.cn");
        areaIdMap.put("33", "yz.js.cn");
        areaIdMap.put("34", "sq.yz.js.cn");
        areaIdMap.put("35", "hj.yz.js.cn");
        areaIdMap.put("36", "jd.yz.js.cn");
        areaIdMap.put("37", "gy.yz.js.cn");
        areaIdMap.put("38", "by.yz.js.cn");
        areaIdMap.put("39", "yc.js.cn");
        areaIdMap.put("40", "sq.yc.js.cn");
        areaIdMap.put("41", "df.yc.js.cn");
        areaIdMap.put("42", "dt.yc.js.cn");
        areaIdMap.put("43", "sy.yc.js.cn");
        areaIdMap.put("44", "fn.yc.js.cn");
        areaIdMap.put("45", "bh.yc.js.cn");
        areaIdMap.put("46", "xs.yc.js.cn");
        areaIdMap.put("47", "jh.yc.js.cn");
        areaIdMap.put("48", "xz.js.cn");
        areaIdMap.put("49", "sq.xz.js.cn");
        areaIdMap.put("50", "ts.xz.js.cn");
        areaIdMap.put("51", "sn.xz.js.cn");
        areaIdMap.put("52", "pix.xz.js.cn");
        areaIdMap.put("53", "xy.xz.js.cn");
        areaIdMap.put("54", "pex.xz.js.cn");
        areaIdMap.put("55", "fx.xz.js.cn");
        areaIdMap.put("56", "jh.ha.js.cn");
        areaIdMap.put("57", "xy.ha.js.cn");
        areaIdMap.put("58", "hy.ha.js.cn");
        areaIdMap.put("59", "hz.ha.js.cn");
        areaIdMap.put("60", "ha.js.cn");
        areaIdMap.put("61", "sq.ha.js.cn");
        areaIdMap.put("62", "ls.ha.js.cn");
        areaIdMap.put("63", "lyg.js.cn");
        areaIdMap.put("64", "sq.lyg.js.cn");
        areaIdMap.put("65", "gay.lyg.js.cn");
        areaIdMap.put("66", "guy.lyg.js.cn");
        areaIdMap.put("67", "dh.lyg.js.cn");
        areaIdMap.put("68", "gn.lyg.js.cn");
        areaIdMap.put("69", "cz.js.cn");
        areaIdMap.put("70", "sq.cz.js.cn");
        areaIdMap.put("71", "suy.sq.js.cn");
        areaIdMap.put("72", "jt.cz.js.cn");
        areaIdMap.put("73", "ly.cz.js.cn");
        areaIdMap.put("74", "ks.sz.js.cn");
        areaIdMap.put("75", "tc.sz.js.cn");
        areaIdMap.put("76", "cs.sz.js.cn");
        areaIdMap.put("77", "zjg.sz.js.cn");
        areaIdMap.put("78", "jj.tz.js.cn");
        areaIdMap.put("79", "tz.js.cn");
        areaIdMap.put("80", "sq.tz.js.cn");
        areaIdMap.put("81", "tx.tz.js.cn");
        areaIdMap.put("82", "jy.tz.js.cn");
        areaIdMap.put("83", "xh.tz.js.cn");
        areaIdMap.put("84", "sq.js.cn");
        areaIdMap.put("85", "sq.sq.js.cn");
        areaIdMap.put("86", "sh.sq.js.cn");
        areaIdMap.put("87", "shy.sq.js.cn");
        areaIdMap.put("88", "sy.sq.js.cn");
        areaIdMap.put("89", "cz.ha.js.cn");
        areaIdMap.put("90", "jwq.xz.js.cn");
        areaIdMap.put("1000", "xx.nj.js.cn");

        map.put("解放军", "解放军");
        map.put("部队", "部队");
        map.put("军区", "军区");
        map.put("海军", "海军");
        map.put("空军", "空军");

        id5Map.put("00","一致");
        id5Map.put("01","身份证号不存在，请检查并确认身份证号真实有效");
        id5Map.put("02","姓名与身份证号不一致，请检查输入是否有误");
        id5Map.put("03","请输入真实有效的身份证号");
        id5Map.put("04","请输入不包含特殊字符的真实姓名");
	    id5Map.put("05","姓名不一致曾用名一致");
        id5Map.put("51","系统正在开小差，无法校验身份证信息，请稍后再试");
        
        //四个直辖市+港澳
        areaCodeToAreaName.put("010", "北京");areaCodeToAreaName.put("021", "上海");areaCodeToAreaName.put("022", "天津");areaCodeToAreaName.put("023", "重庆");areaCodeToAreaName.put("852", "香港");areaCodeToAreaName.put("853", "澳门");
        //河北
        areaCodeToAreaName.put("0310", "邯郸");areaCodeToAreaName.put("0311", "石家庄");areaCodeToAreaName.put("0312", "保定");areaCodeToAreaName.put("0313", "张家口");areaCodeToAreaName.put("0314", "承德");areaCodeToAreaName.put("0315", "唐山");areaCodeToAreaName.put("0316", "廊坊");areaCodeToAreaName.put("0317", "沧州");areaCodeToAreaName.put("0318", "衡水");areaCodeToAreaName.put("0319", "邢台");areaCodeToAreaName.put("0335", "秦皇岛");
        //浙江
        areaCodeToAreaName.put("0570", "衢州");areaCodeToAreaName.put("0571", "杭州");areaCodeToAreaName.put("0572", "湖州");areaCodeToAreaName.put("0573", "嘉兴");areaCodeToAreaName.put("0574", "宁波");areaCodeToAreaName.put("0575", "绍兴");areaCodeToAreaName.put("0576", "台州");areaCodeToAreaName.put("0577", "温州");areaCodeToAreaName.put("0578", "丽水");areaCodeToAreaName.put("0579", "金华");areaCodeToAreaName.put("0580", "舟山");
        //辽宁
        areaCodeToAreaName.put("024", "沈阳,抚顺,本溪,铁岭");areaCodeToAreaName.put("0411", "大连");areaCodeToAreaName.put("0412", "鞍山");areaCodeToAreaName.put("0415", "丹东");areaCodeToAreaName.put("0416", "锦州");areaCodeToAreaName.put("0417", "营口");areaCodeToAreaName.put("0418", "阜新");areaCodeToAreaName.put("0419", "辽阳");areaCodeToAreaName.put("0421", "朝阳");areaCodeToAreaName.put("0427", "盘锦");areaCodeToAreaName.put("0429", "葫芦岛");
        //湖北
        areaCodeToAreaName.put("027", "武汉");areaCodeToAreaName.put("0710", "襄阳");areaCodeToAreaName.put("0711", "鄂州");areaCodeToAreaName.put("0712", "孝感");areaCodeToAreaName.put("0713", "黄冈");areaCodeToAreaName.put("0714", "黄石");areaCodeToAreaName.put("0715", "咸宁");areaCodeToAreaName.put("0716", "荆州");areaCodeToAreaName.put("0717", "宜昌");areaCodeToAreaName.put("0718", "恩施");areaCodeToAreaName.put("0719", "十堰,神龙架");areaCodeToAreaName.put("0722", "随州");areaCodeToAreaName.put("0724", "荆门");areaCodeToAreaName.put("0728", "仙桃,潜江,天门");
        //江苏
        areaCodeToAreaName.put("025", "南京");areaCodeToAreaName.put("0510", "无锡");areaCodeToAreaName.put("0511", "镇江");areaCodeToAreaName.put("0512", "苏州");areaCodeToAreaName.put("0513", "南通");areaCodeToAreaName.put("0514", "扬州");areaCodeToAreaName.put("0515", "盐城");areaCodeToAreaName.put("0516", "徐州");areaCodeToAreaName.put("0517", "淮安");areaCodeToAreaName.put("0518", "连云港");areaCodeToAreaName.put("0519", "常州");areaCodeToAreaName.put("0523", "泰州");areaCodeToAreaName.put("0527", "宿迁");
        //内蒙古
        areaCodeToAreaName.put("0470", "呼伦贝尔");areaCodeToAreaName.put("0471", "呼和浩特");areaCodeToAreaName.put("0472", "包头");areaCodeToAreaName.put("0473", "乌海");areaCodeToAreaName.put("0474", "乌兰察布");areaCodeToAreaName.put("0475", "通辽");areaCodeToAreaName.put("0476", "赤峰");areaCodeToAreaName.put("0477", "鄂尔多斯");areaCodeToAreaName.put("0478", "巴彦淖尔");areaCodeToAreaName.put("0479", "锡林郭勒");areaCodeToAreaName.put("0482", "兴安盟");areaCodeToAreaName.put("0483", "阿拉善");
        //江西
        areaCodeToAreaName.put("0701", "鹰潭");areaCodeToAreaName.put("0790", "新余");areaCodeToAreaName.put("0791", "南昌");areaCodeToAreaName.put("0792", "九江");areaCodeToAreaName.put("0793", "上饶");areaCodeToAreaName.put("0794", "抚州");areaCodeToAreaName.put("0795", "宜春");areaCodeToAreaName.put("0796", "吉安");areaCodeToAreaName.put("0797", "赣州");areaCodeToAreaName.put("0798", "景德镇");areaCodeToAreaName.put("0799", "萍乡");
        //山西
        areaCodeToAreaName.put("0349", "朔州");areaCodeToAreaName.put("0350", "忻州");areaCodeToAreaName.put("0351", "太原");areaCodeToAreaName.put("0352", "大同");areaCodeToAreaName.put("0353", "阳泉");areaCodeToAreaName.put("0354", "晋中");areaCodeToAreaName.put("0355", "长治");areaCodeToAreaName.put("0356", "晋城");areaCodeToAreaName.put("0357", "临汾");areaCodeToAreaName.put("0358", "吕梁");areaCodeToAreaName.put("0359", "运城");
        //甘肃
        areaCodeToAreaName.put("0930", "临夏");areaCodeToAreaName.put("0931", "兰州");areaCodeToAreaName.put("0932", "定西");areaCodeToAreaName.put("0933", "平凉");areaCodeToAreaName.put("0934", "庆阳");areaCodeToAreaName.put("0935", "金昌,武威");areaCodeToAreaName.put("0936", "张掖");areaCodeToAreaName.put("0937", "嘉峪关,酒泉");areaCodeToAreaName.put("0938", "天水");areaCodeToAreaName.put("0939", "陇南");areaCodeToAreaName.put("0941", "甘南");areaCodeToAreaName.put("0943", "白银");
        //山东
        areaCodeToAreaName.put("0530", "菏泽");areaCodeToAreaName.put("0531", "济南");areaCodeToAreaName.put("0532", "青岛");areaCodeToAreaName.put("0533", "淄博");areaCodeToAreaName.put("0534", "德州");areaCodeToAreaName.put("0535", "烟台");areaCodeToAreaName.put("0536", "潍坊");areaCodeToAreaName.put("0537", "济宁");areaCodeToAreaName.put("0538", "泰安");areaCodeToAreaName.put("0539", "临沂");areaCodeToAreaName.put("0543", "滨州");areaCodeToAreaName.put("0546", "东营");areaCodeToAreaName.put("0631", "威海");areaCodeToAreaName.put("0632", "枣庄");areaCodeToAreaName.put("0633", "日照");areaCodeToAreaName.put("0634", "莱芜");areaCodeToAreaName.put("0635", "聊城");
        //黑龙江
        areaCodeToAreaName.put("0451", "哈尔滨");areaCodeToAreaName.put("0452", "齐齐哈尔");areaCodeToAreaName.put("0453", "牡丹江");areaCodeToAreaName.put("0454", "佳木斯");areaCodeToAreaName.put("0455", "绥化");areaCodeToAreaName.put("0456", "黑河");areaCodeToAreaName.put("0457", "大兴安岭");areaCodeToAreaName.put("0458", "伊春");areaCodeToAreaName.put("0459", "大庆");areaCodeToAreaName.put("0467", "鸡西");areaCodeToAreaName.put("0468", "鹤岗");areaCodeToAreaName.put("0469", "双鸭山");
        //福建
        areaCodeToAreaName.put("0591", "福州");areaCodeToAreaName.put("0592", "厦门");areaCodeToAreaName.put("0593", "宁德");areaCodeToAreaName.put("0594", "莆田");areaCodeToAreaName.put("0595", "泉州");areaCodeToAreaName.put("0596", "漳州");areaCodeToAreaName.put("0597", "龙岩");areaCodeToAreaName.put("0598", "三明");areaCodeToAreaName.put("0599", "南平");
        //广东
        areaCodeToAreaName.put("020", "广州");areaCodeToAreaName.put("0660", "汕尾");areaCodeToAreaName.put("0662", "阳江");areaCodeToAreaName.put("0663", "揭阳");areaCodeToAreaName.put("0668", "茂名");areaCodeToAreaName.put("0750", "江门");areaCodeToAreaName.put("0751", "韶关");areaCodeToAreaName.put("0752", "惠州");areaCodeToAreaName.put("0753", "梅州");areaCodeToAreaName.put("0754", "汕头");areaCodeToAreaName.put("0755", "深圳");areaCodeToAreaName.put("0756", "珠海");areaCodeToAreaName.put("0757", "佛山");areaCodeToAreaName.put("0758", "肇庆");areaCodeToAreaName.put("0759", "湛江");areaCodeToAreaName.put("0760", "中山");areaCodeToAreaName.put("0762", "河源");areaCodeToAreaName.put("0763", "清远");areaCodeToAreaName.put("0766", "云浮");areaCodeToAreaName.put("0768", "潮州");
        //四川
        areaCodeToAreaName.put("028", "成都,眉山,资阳");areaCodeToAreaName.put("0812", "攀枝花");areaCodeToAreaName.put("0813", "自贡");areaCodeToAreaName.put("0816", "绵阳");areaCodeToAreaName.put("0817", "南充");areaCodeToAreaName.put("0818", "达州");areaCodeToAreaName.put("0825", "遂宁");areaCodeToAreaName.put("0826", "广安");areaCodeToAreaName.put("0827", "巴中");areaCodeToAreaName.put("0830", "泸州");areaCodeToAreaName.put("0831", "宜宾");areaCodeToAreaName.put("0832", "内江");areaCodeToAreaName.put("0833", "乐山");areaCodeToAreaName.put("0834", "凉山");areaCodeToAreaName.put("0835", "雅安");areaCodeToAreaName.put("0836", "甘孜");areaCodeToAreaName.put("0837", "阿坝");areaCodeToAreaName.put("0838", "德阳");areaCodeToAreaName.put("0839", "广元");
        //湖南
        areaCodeToAreaName.put("0730", "岳阳");areaCodeToAreaName.put("0731", "长沙,株洲,湘潭");areaCodeToAreaName.put("0734", "衡阳");areaCodeToAreaName.put("0735", "郴州");areaCodeToAreaName.put("0736", "常德");areaCodeToAreaName.put("0737", "益阳");areaCodeToAreaName.put("0738", "娄底");areaCodeToAreaName.put("0739", "邵阳");areaCodeToAreaName.put("0743", "湘西");areaCodeToAreaName.put("0744", "张家界");areaCodeToAreaName.put("0745", "怀化");areaCodeToAreaName.put("0746", "永州");
        //河南
        areaCodeToAreaName.put("0370", "商丘");areaCodeToAreaName.put("0371", "郑州,开封");areaCodeToAreaName.put("0372", "安阳");areaCodeToAreaName.put("0373", "新乡");areaCodeToAreaName.put("0374", "许昌");areaCodeToAreaName.put("0375", "平顶山");areaCodeToAreaName.put("0376", "信阳");areaCodeToAreaName.put("0377", "南阳");areaCodeToAreaName.put("0379", "洛阳");areaCodeToAreaName.put("0391", "焦作");areaCodeToAreaName.put("0392", "鹤壁");areaCodeToAreaName.put("0393", "濮阳");areaCodeToAreaName.put("0394", "周口");areaCodeToAreaName.put("0395", "漯河");areaCodeToAreaName.put("0396", "驻马店");areaCodeToAreaName.put("0398", "三门峡");
        //云南
        areaCodeToAreaName.put("0691", "西双版纳");areaCodeToAreaName.put("0692", "德宏");areaCodeToAreaName.put("0870", "昭通");areaCodeToAreaName.put("0871", "昆明");areaCodeToAreaName.put("0872", "大理");areaCodeToAreaName.put("0873", "个旧");areaCodeToAreaName.put("0874", "曲靖");areaCodeToAreaName.put("0875", "保山");areaCodeToAreaName.put("0876", "文山");areaCodeToAreaName.put("0877", "玉溪");areaCodeToAreaName.put("0878", "楚雄");areaCodeToAreaName.put("0879", "普洱");areaCodeToAreaName.put("0883", "临沧");areaCodeToAreaName.put("0886", "怒江");areaCodeToAreaName.put("0887", "迪庆");areaCodeToAreaName.put("0888", "丽江");
        //安徽
        areaCodeToAreaName.put("0550", "滁州");areaCodeToAreaName.put("0551", "合肥");areaCodeToAreaName.put("0552", "蚌埠");areaCodeToAreaName.put("0553", "芜湖");areaCodeToAreaName.put("0554", "淮南");areaCodeToAreaName.put("0555", "马鞍山");areaCodeToAreaName.put("0556", "安庆");areaCodeToAreaName.put("0557", "宿州");areaCodeToAreaName.put("0558", "阜阳,亳州");areaCodeToAreaName.put("0559", "黄山");areaCodeToAreaName.put("0561", "淮北");areaCodeToAreaName.put("0562", "铜陵");areaCodeToAreaName.put("0563", "宣城");areaCodeToAreaName.put("0564", "六安");areaCodeToAreaName.put("0566", "池州");
        //宁夏
        areaCodeToAreaName.put("0951", "银川");areaCodeToAreaName.put("0952", "石嘴山");areaCodeToAreaName.put("0953", "吴忠");areaCodeToAreaName.put("0954", "固原");areaCodeToAreaName.put("0955", "中卫");
        //吉林
        areaCodeToAreaName.put("0431", "长春");areaCodeToAreaName.put("0432", "吉林");areaCodeToAreaName.put("0433", "延边");areaCodeToAreaName.put("0434", "四平");areaCodeToAreaName.put("0435", "通化");areaCodeToAreaName.put("0436", "白城");areaCodeToAreaName.put("0437", "辽源");areaCodeToAreaName.put("0438", "松原");areaCodeToAreaName.put("0439", "白山");
        //广西
        areaCodeToAreaName.put("0770", "防城港");areaCodeToAreaName.put("0771", "南宁,崇左");areaCodeToAreaName.put("0772", "柳州,来宾");areaCodeToAreaName.put("0773", "桂林");areaCodeToAreaName.put("0774", "梧州,贺州");areaCodeToAreaName.put("0775", "贵港");areaCodeToAreaName.put("0776", "百色");areaCodeToAreaName.put("0777", "钦州");areaCodeToAreaName.put("0778", "河池");areaCodeToAreaName.put("0779", "北海");
        //贵州
        areaCodeToAreaName.put("0851", "贵阳");areaCodeToAreaName.put("0852", "遵义");areaCodeToAreaName.put("0853", "安顺");areaCodeToAreaName.put("0854", "黔南");areaCodeToAreaName.put("0855", "黔东南");areaCodeToAreaName.put("0856", "铜仁");areaCodeToAreaName.put("0857", "毕节");areaCodeToAreaName.put("0858", "六盘水");areaCodeToAreaName.put("0859", "黔西南");
        //陕西
        areaCodeToAreaName.put("029", "西安,咸阳");areaCodeToAreaName.put("0911", "延安");areaCodeToAreaName.put("0912", "榆林");areaCodeToAreaName.put("0913", "渭南");areaCodeToAreaName.put("0914", "商洛");areaCodeToAreaName.put("0915", "安康");areaCodeToAreaName.put("0916", "汉中");areaCodeToAreaName.put("0917", "宝鸡");areaCodeToAreaName.put("0919", "铜川");
        //青海
        areaCodeToAreaName.put("0970", "海北");areaCodeToAreaName.put("0971", "西宁");areaCodeToAreaName.put("0972", "海东");areaCodeToAreaName.put("0973", "黄南");areaCodeToAreaName.put("0974", "海南州");areaCodeToAreaName.put("0975", "果洛");areaCodeToAreaName.put("0976", "玉树");areaCodeToAreaName.put("0979", "海西");
        //海南
        areaCodeToAreaName.put("0898", "海南省");
        //西藏
        areaCodeToAreaName.put("0891", "拉萨");areaCodeToAreaName.put("0892", "日喀则");areaCodeToAreaName.put("0893", "山南");areaCodeToAreaName.put("0894", "林芝");areaCodeToAreaName.put("0895", "昌都");areaCodeToAreaName.put("0896", "那曲");areaCodeToAreaName.put("0897", "阿里");
        //新疆
        areaCodeToAreaName.put("0901", "塔城");areaCodeToAreaName.put("0902", "哈密");areaCodeToAreaName.put("0903", "和田");areaCodeToAreaName.put("0906", "阿勒泰");areaCodeToAreaName.put("0908", "克孜勒苏");areaCodeToAreaName.put("0909", "博尔塔拉");areaCodeToAreaName.put("0990", "克拉玛依");areaCodeToAreaName.put("0991", "乌鲁木齐");areaCodeToAreaName.put("0992", "奎屯");areaCodeToAreaName.put("0993", "石河子");areaCodeToAreaName.put("0994", "昌吉");areaCodeToAreaName.put("0995", "吐鲁番");areaCodeToAreaName.put("0996", "巴音郭楞");areaCodeToAreaName.put("0997", "阿克苏");areaCodeToAreaName.put("0998", "喀什");areaCodeToAreaName.put("0999", "伊犁");
    }

    public static String getAreaCodeByAAA(String key){
        return aaaRegionToAreaCode.get(key);
    }
    public static String getId5Msg(String key){
        return id5Map.get(key);
    }

    public static String getAreaName(String key){
        return areaCodeToAreaName.get(key);
    }
    public static String getCurrentTime(){
        Date time = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(time);
    }
    
	public static String md5(String str) {
		return DigestUtils.md5Hex(str);
	}

	public static String getDateFormatString(Date date) {
		SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		return sDateFormat.format(date);
	}

	public static Object getObject(InputStream inputStream)
			throws ClassNotFoundException, IOException {
		try(ObjectInputStream ois = new ObjectInputStream(inputStream)) {
			return ois.readObject();
		} finally {
			if (inputStream != null) {
				inputStream.close();
			}
		}
	}

	public static JSONObject getStandardParam(JSONObject requestJSON,String actionCode) throws Exception{
	    JSONObject head = requestJSON.getJSONObject("head");
        JSONObject body = requestJSON.getJSONObject("body");
        String tranId =  head.getString("tranId");
        //  密匙
        String auth = head.getString("auth");
        String areaCode = body.getString("areaCode");
        if(isBlank(tranId)||isBlank(auth)||isBlank(areaCode)){
            throw new Exception("入参有空，请检查入参 !!!");
        }
        //  封装参数
        JSONObject request = body;
        request.put("actionCode", actionCode);
        request.put("serialNumber", tranId);
        request.put("auth", auth);
        request.put("areaCode", areaCode);
	    return request;
	}
	
	public static boolean isBlank(String str){
	    return (CharSequenceUtil.isBlank(str) || "null".equals(str));
	}
	
	public static String getString(InputStream inputStream, String charset)
			throws IOException {
		try (BufferedReader in = new BufferedReader(new InputStreamReader(inputStream, charset)); CharArrayWriter data = new CharArrayWriter()) {
			char[] buf = new char[4096];
			int ret;
			while ((ret = in.read(buf, 0, 4096)) != -1) {
				data.write(buf, 0, ret);
			}
			return data.toString().trim();
		} finally {
			if (inputStream != null) {
				inputStream.close();
			}
		}
	}
	
	
	// 响应结果构建
	public static JSONObject builderResponse(String result, RespHead responseHead, RespInfo responseJson, String tranId) {
        String responseTime = Utils.getCurrentTime();
        if (StringUtils.isNotBlank(result)) {
            JSONObject json = JSON.parseObject(result);
            String isResult = json.getString("result");
            if (StringUtils.equals(isResult, "0")) {
                responseHead.setMsg(Utils.SUCCESS);
                responseHead.setStatus("00");
                responseHead.setResponseId(Utils.RSP + tranId);
                responseHead.setResponseTime(responseTime);
                responseJson.setBody(json);
            } else {
                responseHead.setMsg(isResult);
                responseHead.setStatus("00");
                responseHead.setResponseId(Utils.RSP + tranId);
                responseHead.setResponseTime(responseTime);
                responseJson.setBody(json);
            }
        } else {
            responseHead.setMsg("");
            responseHead.setStatus("99");
            responseHead.setResponseId(Utils.RSP + tranId);
            responseHead.setResponseTime(responseTime);
        }
        return (JSONObject) JSON.toJSON(responseJson);
    }
	  public static JSONObject dealExceptionResp(RespHead responseHead, String tranId, RespInfo responseJson) {
	        String responseTime = Utils.getCurrentTime();
	        responseHead.setMsg("服务内部错误");
	        responseHead.setStatus("99");
	        responseHead.setResponseId(Utils.RSP + tranId);
	        responseHead.setResponseTime(responseTime);
	        return (JSONObject) JSON.toJSON(responseJson);
	    }

	public static JSONObject dealResponseForSgw(String errorCode, String errorMsg, String success, JSONObject body) {

		if (null != body) {
			// 原有body加上错误码标识
			body.put("errorCode", errorCode);
			body.put("errorMsg", errorMsg);
			body.put("success", success);
			return body;
		} else {
			JSONObject response = new JSONObject();
			response.put("errorCode", errorCode);
			response.put("errorMsg", errorMsg);
			response.put("success", success);
			return response;
		}

	}

    public static JSONObject dealExceptionRespWithMsg(RespHead responseHead, String tranId,String msg, RespInfo responseJson){
        String responseTime = Utils.getCurrentTime();
        responseHead.setMsg(msg);
        responseHead.setStatus("99");
        responseHead.setResponseId(Utils.RSP + tranId);
        responseHead.setResponseTime(responseTime);
        return (JSONObject) JSON.toJSON(responseJson);
    }
	    
    public static JSONObject dealExceptionRespWithMsgAndStatus(RespHead responseHead, String tranId,String msg, String status,RespInfo responseJson){
        String responseTime = Utils.getCurrentTime();
        responseHead.setMsg(msg);
        responseHead.setStatus(status);
        responseHead.setResponseId(Utils.RSP + tranId);
        responseHead.setResponseTime(responseTime);
        return (JSONObject) JSON.toJSON(responseJson);
    }

    public static JSONObject dealSuccessResp(RespHead responseHead, String tranId,JSONObject body, RespInfo responseJson){
        String responseTime = Utils.getCurrentTime();
        responseHead.setMsg(Utils.SUCCESS);
        responseHead.setStatus("00");
        responseHead.setResponseId(Utils.RSP + tranId);
        responseHead.setResponseTime(responseTime);
        responseJson.setBody(body);
        return (JSONObject) JSON.toJSON(responseJson);
    }

	public static JSONObject dealErrorResp(RespHead responseHead, String tranId, JSONObject body, String status, String msg, RespInfo responseJson) {
		String responseTime = Utils.getCurrentTime();
		responseHead.setMsg(msg);
		responseHead.setStatus(status);
		responseHead.setResponseId(Utils.RSP + tranId);
		responseHead.setResponseTime(responseTime);
		responseJson.setBody(body);
		return (JSONObject) JSON.toJSON(responseJson);
	}
    public static JSONObject dealSuccessRespWithMsgAndStatus(RespHead responseHead, String tranId,JSONObject body, String msg,String status,RespInfo responseJson){
        String responseTime = Utils.getCurrentTime();
        responseHead.setMsg(msg);
        responseHead.setStatus(status);
        responseHead.setResponseId(Utils.RSP + tranId);
        responseHead.setResponseTime(responseTime);
        responseJson.setBody(body);
        return (JSONObject) JSON.toJSON(responseJson);
    }

	  public static JSONObject dealBaseAppExceptionResp(RespHead responseHead, String tranId, RespInfo responseJson) {
          String responseTime = Utils.getCurrentTime();
          responseHead.setMsg("入参校验失败");
          responseHead.setStatus("03");
          responseHead.setResponseId(Utils.RSP + tranId);
          responseHead.setResponseTime(responseTime);
          return (JSONObject) JSON.toJSON(responseJson);
      }

	public static JSONObject dealBaseAppExceptionResp(RespHead responseHead, String tranId, RespInfo responseJson, String msg, String status) {
		String responseTime = Utils.getCurrentTime();
		responseHead.setMsg(msg);
		responseHead.setStatus(status);
		responseHead.setResponseId(Utils.RSP + tranId);
		responseHead.setResponseTime(responseTime);
		return (JSONObject) JSON.toJSON(responseJson);
	}

	public static JSONObject dealBaseAppExceptionRespNew(String tranId, String msg, String status) {
		String responseTime = Utils.getCurrentTime();
		JSONObject response = new JSONObject();
		JSONObject head = new JSONObject();
		head.put("msg", msg);
		head.put("status", status);
		head.put("responseId", Utils.RSP + tranId);
		head.put("responseTime", responseTime);
		response.put("head", head);
		return response;
	}

	public static JSONObject dealSuccessRespWithBodyAndTranId(String tranId, JSONObject body) {

		String responseTime = Utils.getCurrentTime();
		JSONObject response = new JSONObject();
		JSONObject head = new JSONObject();
		head.put("msg", "success");
		head.put("status", "00");
		head.put("responseId", Utils.RSP + tranId);
		head.put("responseTime", responseTime);
		response.put("body", body);
		response.put("head", head);
		return response;
	}

	public static JSONObject dealExceptionRespNew(String tranId) {
		JSONObject response = new JSONObject();
		JSONObject head = new JSONObject();
		String responseTime = Utils.getCurrentTime();
		head.put("msg", "服务器内部错误");
		head.put("status", "99");
		head.put("responseId", Utils.RSP + tranId);
		head.put("responseTime", responseTime);
		response.put("head", head);
		return response;
	}

    public static JSONObject dealBaseAppExceptionRespWithCode(RespHead responseHead, String tranId, String code,RespInfo responseJson) {
        String responseTime = Utils.getCurrentTime();
        responseHead.setMsg("系统正在开小差，无法校验身份证信息，请稍后再试");
        responseHead.setStatus(code);
        responseHead.setResponseId(Utils.RSP + tranId);
        responseHead.setResponseTime(responseTime);
        return (JSONObject) JSON.toJSON(responseJson);
    }
	  /**
	     * soap xml解析，根据节点解析
	     * @param strXml 所需解析的xml字符串
	     * @param xpathExpr 解析节点表达式
	     * @return json 返回json对象
	     * */
	    public static JSONObject parseSoapXML(String strXml , String xpathExpr) throws DocumentException {
	        JSONObject jo = null;
	        if(StringUtils.isBlank(strXml)){
	            jo = new JSONObject();
	            jo.put("errorCode", ExpConstant.ERROR_OUTPUT_NULL);
	            jo.put("errorMsg", "unknown error，response is empty,plc check.");
	            jo.put("success", "OK");
	            jo.put("bodys", strXml);
	            jo.put("aaa", "");
	            return jo;
	        }
	        org.dom4j.Document document = DocumentHelper.parseText(strXml);
	        DefaultXPath xpath = new DefaultXPath(xpathExpr);
	        List<?> list = xpath.selectNodes(document);
	        Iterator<?> iterator = list.iterator();
	        while (iterator.hasNext())
	        {
	            jo = new JSONObject();
	            Element node = (Element) iterator.next();
	            XML2JSON.dom4j2Json(node, jo);
	        }
	        return jo;
	    }
	public static JSONObject builderResponse(String result, RespHead responseHead, RespInfo responseJson, String tranId,String resultStr) {
        JSONObject response = null;
        String responseTime = Utils.getCurrentTime();
        if (StringUtils.isNotBlank(result)) {
            JSONObject json = JSON.parseObject(result);
            String [] resultArr = resultStr.split(":");
            String isResult ="";
            for(int i=0;i<resultArr.length;i++){
                if(i==resultArr.length-1){
                    isResult = json.getString(resultArr[i]);
                }else{
                    json = json.getJSONObject(resultArr[i]);
                }
            }
            if (StringUtils.equals(isResult, "0")) {
                responseHead.setMsg(Utils.SUCCESS);
                responseHead.setStatus("00");
                responseHead.setResponseId(Utils.RSP + tranId);
                responseHead.setResponseTime(responseTime);
                responseJson.setBody(json);
            } else {
                responseHead.setMsg(isResult);
                responseHead.setStatus("00");
                responseHead.setResponseId(Utils.RSP + tranId);
                responseHead.setResponseTime(responseTime);
                responseJson.setBody(json);
            }
        } else {
            responseHead.setMsg("return is null");
            responseHead.setStatus("99");
            responseHead.setResponseId(Utils.RSP + tranId);
            responseHead.setResponseTime(responseTime);
        }
        response = (JSONObject) JSON.toJSON(responseJson);
        return response;
    }
	public static JSONObject builderResponse(String result, RespHead responseHead, RespInfo responseJson, String tranId,String resultStr,String bodyStr,String errMsgStr) {
        JSONObject response = null;
        String responseTime = Utils.getCurrentTime();
        if (StringUtils.isNotBlank(result)) {
            JSONObject json = JSON.parseObject(result);
            String [] resultArr = resultStr.split(":");
            String isResult ="";
            for(int i=0;i<resultArr.length;i++){//取出是否成功标识
                if(i==resultArr.length-1){
                    isResult = json.getString(resultArr[i]);
                }else{
                    json = json.getJSONObject(resultArr[i]);
                }
            }
            if (StringUtils.equals(isResult, "0")) {
                responseHead.setMsg(Utils.SUCCESS);
                responseHead.setStatus("00");
                responseHead.setResponseId(Utils.RSP + tranId);
                responseHead.setResponseTime(responseTime);
                JSONObject json2 = JSON.parseObject(result);
                if( !"".equals(bodyStr)){
                    String [] bodyArr = bodyStr.split(":");
                    for(int i=0;i<bodyArr.length;i++){//取出精简的Body
                        if(i==bodyArr.length-1){
                            String [] bodyFormatArr = bodyArr[i].split("##");
                            //直接自己判断，不再经过##来判断，防止代码错误
                            String jsonStr = json2.getString(bodyFormatArr[0]);
                            if(jsonStr.startsWith("[")){//入参是jsonArr,则直接返回jsonArr
                              JSONArray bodyList = json2.getJSONArray(bodyFormatArr[0]);
                              json2.clear();
                              json2.put(bodyFormatArr[0], bodyList);
                            }else{//入参是jsonObject,则根据1，2来判断返回jsonObject或者jsonArr
                                if("2".equals(bodyFormatArr[1])){//2代表是jsonArray
                                  JSONArray bodyList = new JSONArray();
                                  JSONObject obj = json2.getJSONObject(bodyFormatArr[0]);
                                  bodyList.add(obj);
                                  json2.clear();
                                  json2.put(bodyFormatArr[0], bodyList);
                              }else if("3".equals(bodyFormatArr[1])){//3代表是String
                                  String bodyObject=json2.getString(bodyFormatArr[0]);
                                  json2.clear();
                                  json2.put(bodyFormatArr[0], bodyObject);
                              }else{
                                  JSONObject bodyObject=json2.getJSONObject(bodyFormatArr[0]);
                                  json2.clear();
                                  json2.put(bodyFormatArr[0], bodyObject);
                              }
                            }
                        }else{
                            json2 = json2.getJSONObject(bodyArr[i]);
                        }
                    }
                }
                responseJson.setBody(json2);
            } else {//调用成功，业务失败
                responseHead.setStatus("01");
                responseHead.setResponseId(Utils.RSP + tranId);
                responseHead.setResponseTime(responseTime);
                JSONObject json3 = JSON.parseObject(result);
                String errMSg ="";
                if (!errMsgStr.equals("")) {
                    String[] errMsgArr = errMsgStr.split(":");
                    for (int i = 0; i < errMsgArr.length; i++) {// 取出企信返回的报错信息
                        if (i == errMsgArr.length - 1) {
                            errMSg = json3.getString(errMsgArr[i]);
                        } else {
                            json3 = json3.getJSONObject(errMsgArr[i]);
                        }
                    }
                }else{
                    errMSg = result;
                }
                responseHead.setMsg(errMSg);
                responseJson.setBody(json);
            }
        } else {
            responseHead.setMsg("调用企信失败");
            responseHead.setStatus("02");
            responseHead.setResponseId(Utils.RSP + tranId);
            responseHead.setResponseTime(responseTime);
        }
        response = (JSONObject) JSON.toJSON(responseJson);
        return response;
    }
	
	/**
	 * 
	 * Description: 星级会员查询结果构建<br> 
	 *  
	 * @author zhpp<br>
	 * @taskId <br>
	 * @param result
	 * @param responseHead
	 * @param responseJson
	 * @param tranId
	 * @param resultStr
	 * @return <br>
	 */
	public static JSONObject builderResponseForStar(String result, RespHead responseHead, RespInfo responseJson, String tranId,String resultStr) {
        JSONObject response = null;
        String responseTime = Utils.getCurrentTime();
        if (StringUtils.isNotBlank(result)) {
            JSONObject json = JSON.parseObject(result);
            JSONObject responseMess = json.getJSONObject("responseMess");
            String isResult = responseMess.getString(resultStr);
            if (StringUtils.equals(isResult, "0")) {
                String memberLevel = responseMess.getJSONObject("memberInfo").getString("memberLevel");
                json.clear();
                json.put("TSR_LEVEL", changeLevel(memberLevel));
                responseHead.setMsg(Utils.SUCCESS);
                responseHead.setStatus("00");
                responseHead.setResponseId(Utils.RSP + tranId);
                responseHead.setResponseTime(responseTime);
                responseJson.setBody(json);
            } else {
                responseHead.setMsg(responseMess.getString("rspDesc"));
                responseHead.setStatus(isResult);
                responseHead.setResponseId(Utils.RSP + tranId);
                responseHead.setResponseTime(responseTime);
            }
        } else {
            responseHead.setMsg("调用企信失败");
            responseHead.setStatus("02");
            responseHead.setResponseId(Utils.RSP + tranId);
            responseHead.setResponseTime(responseTime);
        }
        response = (JSONObject) JSON.toJSON(responseJson);
        return response;
    }
	
	public static JSONObject builderResponseFroScoreConsumptionHistroy(String result, RespHead responseHead, RespInfo responseJson, String tranId,String resultStr) {
	    JSONObject response = null;
        String responseTime = Utils.getCurrentTime();
        if (StringUtils.isNotBlank(result)) {
            JSONObject json = JSON.parseObject(result);
            JSONObject responseMess = json.getJSONObject("responsemess");
            String isResult = responseMess.getString(resultStr);
            if (StringUtils.equals(isResult, "0")) {
                responseHead.setMsg(Utils.SUCCESS);
                responseHead.setStatus("00");
                responseHead.setResponseId(Utils.RSP + tranId);
                responseHead.setResponseTime(responseTime);
                responseJson.setBody(responseMess);
            } else {
                responseHead.setMsg(responseMess.getString("resultmsg"));
                responseHead.setStatus(isResult);
                responseHead.setResponseId(Utils.RSP + tranId);
                responseHead.setResponseTime(responseTime);
            }
        } else {
            responseHead.setMsg("QX return is null");
            responseHead.setStatus("02");
            responseHead.setResponseId(Utils.RSP + tranId);
            responseHead.setResponseTime(responseTime);
        }
        response = (JSONObject) JSON.toJSON(responseJson);
        return response;
    }
	 /**
     * 
     * Description: 转化星级等级<br> 
     *  
     * @author zhpp<br>
     * @taskId <br>
     * @param memberLevel
     * @return <br>
     */
    private static String changeLevel(String memberLevel) {
        String level = "";
        if("3100".equals(memberLevel)) {
            level = "1";
        } else if("3200".equals(memberLevel)) {
            level = "2";
        } else if("3300".equals(memberLevel)) {
            level = "3";
        } else if("3400".equals(memberLevel)) {
            level = "4";
        } else if("3500".equals(memberLevel)) {
            level = "5";
        } else if("3600".equals(memberLevel)) {
            level = "6";
        } else if("3700".equals(memberLevel)) {
            level = "7";
        }
        return level;
    }
	
	/**
     * 
     * Description: array结果构建<br> 
     *  
     * @author zhpp<br>
     * @taskId <br>
     * @param result
     * @param responseHead
     * @param responseJson
     * @param tranId
     * @return <br>
     */
    public static JSONObject builderResponseForArray(String result, RespHead responseHead, RespInfo responseJson, String tranId) {
        JSONObject response = null;
        String responseTime = Utils.getCurrentTime();
        if (StringUtils.isNotBlank(result)) {
            if(result.startsWith("[")) {
                JSONObject jsonArray = new JSONObject();
                JSONArray convertArray = JSON.parseArray(result);
                JSONArray array = new JSONArray();
                for(int i = 0;i < convertArray.size(); i++) {
                    JSONObject json = convertArray.getJSONObject(i);
                    array.add(json);
                }
                jsonArray.put("param", array);
                responseHead.setMsg(Utils.SUCCESS);
                responseHead.setStatus("00");
                responseHead.setResponseId(Utils.RSP + tranId);
                responseHead.setResponseTime(responseTime);
                responseJson.setBody(jsonArray);
            } else {
                JSONObject json = JSON.parseObject(result);
                responseHead.setMsg(Utils.SUCCESS);
                responseHead.setStatus("00");
                responseHead.setResponseId(Utils.RSP + tranId);
                responseHead.setResponseTime(responseTime);
                responseJson.setBody(json);
            }
        } else {
            responseHead.setMsg("调用企信失败");
            responseHead.setStatus("02");
            responseHead.setResponseId(Utils.RSP + tranId);
            responseHead.setResponseTime(responseTime);
        }
        response = (JSONObject) JSON.toJSON(responseJson);
        return response;
    }
	
	/**
	 * 
	 * Description: adsl专用<br> 
	 *  
	 * @author zhpp<br>
	 * @taskId <br>
	 * @param result
	 * @param responseHead
	 * @param responseJson
	 * @param tranId
	 * @return <br>
	 */
	public static JSONObject builderResponseForADSL(String result, RespHead responseHead, RespInfo responseJson, String tranId, String msg) {
        JSONObject response = null;
        String responseTime = Utils.getCurrentTime();
        if (StringUtils.isNotBlank(msg)) {
            JSONObject json = JSON.parseObject(result);
            responseHead.setMsg(Utils.SUCCESS);
            responseHead.setStatus("00");
            responseHead.setResponseId(Utils.RSP + tranId);
            responseHead.setResponseTime(responseTime);
            responseJson.setBody(json);
        } else {
            responseHead.setMsg("");
            responseHead.setStatus("99");
            responseHead.setResponseId(Utils.RSP + tranId);
            responseHead.setResponseTime(responseTime);
        }
        response = (JSONObject) JSON.toJSON(responseJson);
        return response;
    }
	
	public static JSONObject builderResponseForstarCmsManager(String result, RespHead responseHead, RespInfo responseJson, String tranId) {
		JSONObject response = null;
		String responseTime = Utils.getCurrentTime();
		if (StringUtils.isNotBlank(result)) {
			JSONObject data = JSON.parseObject(result).getJSONObject("data");
			JSONObject json = new JSONObject();
			json.put("content", data);
			responseHead.setMsg(Utils.SUCCESS);
			responseHead.setStatus("00");
			responseHead.setResponseId(Utils.RSP + tranId);
			responseHead.setResponseTime(responseTime);
			responseJson.setBody(json);
		} else {
			responseHead.setMsg("调用企信失败");
			responseHead.setStatus("02");
			responseHead.setResponseId(Utils.RSP + tranId);
			responseHead.setResponseTime(responseTime);
		}
		response = (JSONObject) JSON.toJSON(responseJson);
		return response;
	}
    
	public static JSONArray buildRsp(JSONObject jsonObject,JSONObject json){
		JSONArray blances = new JSONArray();
		String str = jsonObject.getString("NARROW_DATA_GUlist");
		if(str.startsWith("[")) {
			JSONArray array = jsonObject.getJSONArray("NARROW_DATA_GUlist");
			json.put("count", String.valueOf(array.size()));
			for(int i = 0; i< array.size(); i++) {
				JSONObject obj = new JSONObject();
				JSONObject jsonBlance = array.getJSONObject(i);
				obj.put("ticketNumber",MainUtils.getNullEmpty(jsonBlance.getString("TICKET_NUMBER")));// 序号
				obj.put("ticketId",MainUtils.getNullEmpty(jsonBlance.getString("TICKET_ID")));// 话单标识
				obj.put("areaCode",MainUtils.getNullEmpty(jsonBlance.getString("AREA_CODE")));// 区域编码
				obj.put("nbr",MainUtils.getNullEmpty(jsonBlance.getString("ACC_NBR")));//对方号码
				obj.put("accName", MainUtils.getNullEmpty(jsonBlance.getString("ACCT_NAME")));//宽带账号
				obj.put("calledNbr", MainUtils.getNullEmpty(jsonBlance.getString("CALLED_NBR")));//被叫号码
				obj.put("ticketType", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_TYPE"))); //业务类型
				obj.put("startDateNew", MainUtils.getNullEmpty(jsonBlance.getString("START_DATE_NEW")));//上线日期
				obj.put("startTimeNew",MainUtils.getNullEmpty(jsonBlance.getString("START_TIME_NEW")));//上线时间
				obj.put("endDateNew",MainUtils.getNullEmpty(jsonBlance.getString("END_DATE_NEW")));//上线日期
				obj.put("endTimeNew",MainUtils.getNullEmpty(jsonBlance.getString("END_TIME_NEW")));//上线时间
				obj.put("duartionCh",MainUtils.getNullEmpty(jsonBlance.getString("DURATION_CH")));//使用时长
				obj.put("acctItemChargeCh",MainUtils.getNullEmpty(jsonBlance.getString("ACCT_ITEM_CHARGE_CH")));//话费
				obj.put("inBytesCnt",MainUtils.getNullEmpty(jsonBlance.getString("IN_BYTES_CNT")));//流量
				obj.put("outBytesCnt",MainUtils.getNullEmpty(jsonBlance.getString("OUT_BYTES_CNT")));
				obj.put("bytesCnt",MainUtils.getNullEmpty(jsonBlance.getString("BYTES_CNT")));
				obj.put("cycleEndDate",MainUtils.getNullEmpty(jsonBlance.getString("CYCLE_END_DATE")));
				obj.put("productName",MainUtils.getNullEmpty(jsonBlance.getString("PRODUCT_NAME")));
				obj.put("ticketTypeNew",MainUtils.getNullEmpty(jsonBlance.getString("TICKET_TYPE_NEW")));//流量
				obj.put("durationType",MainUtils.getNullEmpty(jsonBlance.getString("DURATION＿TYPE")));//流量
				blances.add(obj);
			}
		} else {
			JSONObject jsonBlance = jsonObject.getJSONObject("NEW_SVR_TICGUlist");
			JSONObject obj=new JSONObject();
			json.put("count", "1");
			obj.put("ticketNumber",MainUtils.getNullEmpty(jsonBlance.getString("TICKET_NUMBER")));// 序号
			obj.put("ticketId",MainUtils.getNullEmpty(jsonBlance.getString("TICKET_ID")));// 话单标识
			obj.put("areaCode",MainUtils.getNullEmpty(jsonBlance.getString("AREA_CODE")));// 区域编码
			obj.put("nbr",MainUtils.getNullEmpty(jsonBlance.getString("ACC_NBR")));//对方号码
			obj.put("accName", MainUtils.getNullEmpty(jsonBlance.getString("ACCT_NAME")));//宽带账号
			obj.put("calledNbr", MainUtils.getNullEmpty(jsonBlance.getString("CALLED_NBR")));//被叫号码
			obj.put("ticketType", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_TYPE"))); //业务类型
			obj.put("startDateNew", MainUtils.getNullEmpty(jsonBlance.getString("START_DATE_NEW")));//上线日期
			obj.put("startTimeNew",MainUtils.getNullEmpty(jsonBlance.getString("START_TIME_NEW")));//上线时间
			obj.put("endDateNew",MainUtils.getNullEmpty(jsonBlance.getString("END_DATE_NEW")));//上线日期
			obj.put("endTimeNew",MainUtils.getNullEmpty(jsonBlance.getString("END_TIME_NEW")));//上线时间
			obj.put("duartionCh",MainUtils.getNullEmpty(jsonBlance.getString("DURATION_CH")));//使用时长
			obj.put("acctItemChargeCh",MainUtils.getNullEmpty(jsonBlance.getString("ACCT_ITEM_CHARGE_CH")));//话费
			obj.put("inBytesCnt",MainUtils.getNullEmpty(jsonBlance.getString("IN_BYTES_CNT")));//流量
			obj.put("outBytesCnt",MainUtils.getNullEmpty(jsonBlance.getString("OUT_BYTES_CNT")));
			obj.put("bytesCnt",MainUtils.getNullEmpty(jsonBlance.getString("BYTES_CNT")));
			obj.put("cycleEndDate",MainUtils.getNullEmpty(jsonBlance.getString("CYCLE_END_DATE")));
			obj.put("productName",MainUtils.getNullEmpty(jsonBlance.getString("PRODUCT_NAME")));
			obj.put("ticketTypeNew",MainUtils.getNullEmpty(jsonBlance.getString("TICKET_TYPE_NEW")));//流量
			obj.put("durationType",MainUtils.getNullEmpty(jsonBlance.getString("DURATION＿TYPE")));//流量
			blances.add(obj);
		}
		return blances;
	}

	public static String checkBssInnerAreaId(String areaCode) {
        String bssInnerAreaId = "";
        if(areaCode.equals("025")) {
            bssInnerAreaId = "3";
        } else if(areaCode.equals("0511")) {
            bssInnerAreaId = "4";
        } else if(areaCode.equals("0510")) {
            bssInnerAreaId = "15";
        } else if(areaCode.equals("0512")) {
            bssInnerAreaId = "20";
        } else if(areaCode.equals("0513")) {
            bssInnerAreaId = "26";
        } else if(areaCode.equals("0514")) {
            bssInnerAreaId = "33";
        } else if(areaCode.equals("0515")) {
            bssInnerAreaId = "39";
        } else if(areaCode.equals("0516")) {
            bssInnerAreaId = "48";
        } else if(areaCode.equals("0517")) {
            bssInnerAreaId = "60";
        } else if(areaCode.equals("0518")) {
            bssInnerAreaId = "63";
        } else if(areaCode.equals("0519")) {
            bssInnerAreaId = "69";
        } else if(areaCode.equals("0523")) {
            bssInnerAreaId = "79";
        } else if(areaCode.equals("0527")) {
            bssInnerAreaId = "84";
        }
        return bssInnerAreaId;
    }
	
	public static String getNativeNetId(String areaCode) {
        /*
         * 0000003 南京分公司 0000004 无锡分公司 0000006 常州分公司 0000007 苏州分公司 0000008 南通分公司 0000009 连云港分公司 0000010 淮安分公司 0000012 扬州分公司 0000013 镇江分公司 0000014
         * 泰州分公司 0000015 宿迁分公司 0000011 盐城分公司 0000005 徐州分公司
         */
        String nativeNetId = "";
        if ("025".equals(areaCode)) {
            nativeNetId = "250010000";// 南京分公司
        } else if ("0510".equals(areaCode)) {
            nativeNetId = "100000011";// 无锡分公司
        } else if ("0519".equals(areaCode)) {
            nativeNetId = "190000012";// 常州分公司
        } else if ("0512".equals(areaCode)) {
            nativeNetId = "120000512";// 苏州分公司
        } else if ("0513".equals(areaCode)) {
            nativeNetId = "130000011";// 南通分公司
        } else if ("0518".equals(areaCode)) {
            nativeNetId = "180000320";// 连云港分公司
        } else if ("0517".equals(areaCode)) {
            nativeNetId = "170000001";// 淮安分公司
        } else if ("0514".equals(areaCode)) {
            nativeNetId = "140001399";// 扬州分公司
        } else if ("0511".equals(areaCode)) {
            nativeNetId = "4";// 镇江分公司
        } else if ("0523".equals(areaCode)) {
            nativeNetId = "230000011";// 泰州分公司
        } else if ("0527".equals(areaCode)) {
            nativeNetId = "270000012";// 宿迁分公司
        } else if ("0515".equals(areaCode)) {
            nativeNetId = "150002804";// 盐城分公司
        } else if ("0516".equals(areaCode)) {
            nativeNetId = "160000220"; // 徐州分公司
        } else if ("000".equals(areaCode)) {
            nativeNetId = "320000";// 省公司本部及直属部门
        }
        return nativeNetId;
    }

    public static JSONObject buildResponse(String errorMsg, String errorCode, String tranId, JSONObject body) {

        JSONObject response = new JSONObject();
        JSONObject head = new JSONObject();

        String responseTime = Utils.getCurrentTime();
        head.put("responseId", Utils.RSP + tranId);
        head.put("status", errorCode);
        head.put("msg", errorMsg);
        head.put("responseTime", responseTime);

        if (null != body) {
            response.put("body", body);
        }
        response.put("head", head);

        return response;

    }
	
	/**
	 * 将jsonObejct和jsonArr不明的json转为标准的jsonArr
	 * Description: <br> 
	 *  
	 * @author yangyang<br>
	 * 2019年4月12日
	 * @param object
	 * @return JSONArray<br>
	 */
	public static JSONArray getStandardJSONArray(Object object){
	    JSONArray returnArr = new JSONArray();
	    if(object instanceof JSONObject){//如果是jsonObject,组装返回
	        returnArr.add(object);
	        return returnArr;
	    }
	    return (JSONArray)object;//是jsonArray，直接返回
	}
	
	public static void throwBillExcepiton(String errorCode, String errorMsg) throws BSException {
		
		log.error("errorCode: {}  errorMsg: {}", errorCode, errorMsg);
		throw new BSException(errorCode, errorMsg);
	}
	
	public static JSONObject checkBillResponse(String responseString, String codeKey, String msgKey, String successCode) throws BSException {
		
		if (StringUtils.isBlank(responseString)) {
			throwBillExcepiton("bs_response_empty","计费接口返回空");
		}
		
		JSONObject resObj = JSON.parseObject(responseString);
		
		String errorCode = resObj.getString(codeKey);
		
		if (!StringUtils.equals(successCode, errorCode)) {
			String errorMsg = resObj.getString(msgKey);
			throwBillExcepiton(errorCode, errorMsg);
		}
		
		return resObj;
	}

    /**
     *
     * Description: 成功出参构造<br>
     *
     * @author zhpp<br>
     * @taskId <br>
     * @param responseHead
     * @param responseJson
     * @param tranId
     * @return <br>
     */
    public static JSONObject builderResponseForSuccess(RespHead responseHead, RespInfo responseJson, String tranId, JSONObject bodyJson) {
        JSONObject response = null;
        String responseTime = getCurrentTime();
        responseHead.setMsg(SUCCESS);
        responseHead.setStatus("00");
        responseHead.setResponseId(Utils.RSP + tranId);
        responseHead.setResponseTime(responseTime);
        responseJson.setBody(bodyJson);
        response = (JSONObject) JSON.toJSON(responseJson);
        return response;
    }
    /**
     *
     * Description: 失败出参构造<br>
     *
     * @author zhpp<br>
     * @taskId <br>
     * @param responseHead
     * @param responseJson
     * @param tranId
     * @param status
     * @param msg
     * @return <br>
     */
    public static JSONObject builderResponseForFail(RespHead responseHead, RespInfo responseJson, String tranId, String status, String msg) {
        JSONObject response = null;
        String responseTime = getCurrentTime();
        responseHead.setMsg(msg);
        responseHead.setStatus(status);
        responseHead.setResponseId(Utils.RSP + tranId);
        responseHead.setResponseTime(responseTime);
        responseJson.setBody(JSON.parseObject("{}"));
        response = (JSONObject) JSON.toJSON(responseJson);
        return response;
    }
}

