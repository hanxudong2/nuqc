package cn.js189.common.domain;

import java.util.Date;

/**
 * 客户端-鉴权类
 * 
 * @author zhangpz
 * 
 */
public class Authentication {

    // 子系统id
    private String sysId;

    // 密码
    private String pwd;

    // 请求时间
    private Date reqTime;

    public Authentication(String sysId, String pwd) {
        this.sysId = sysId;
        this.pwd = pwd;
        this.reqTime = new Date();
    }

    public Authentication(String sysId, String pwd, Date date) {
        this.sysId = sysId;
        this.pwd = pwd;
        this.reqTime = date;
    }

    public String getSysId() {
        return sysId;
    }

    public String getPwd() {
        return pwd;
    }

    public Date getReqTime() {
        return reqTime;
    }

    public void setReqTime(Date reqTime) {
        this.reqTime = reqTime;
    }

}

