package cn.js189.common.enums;

/**
 * 查询客户范围的枚举类型
 * 
 * @author Mid
 */
public enum QryCustomerScopeInfoEnum {
	// 客户
	CUSTOMER("customer", 1),
	// 客户属性
	CUST_ATTR("custAttr", 2),
	// 客户标签
	CUST_LABEL("custLabel", 3),
	// 客户特殊名单
	SPECIAL_LIST("specialList", 4),
	// 客户服务等级
	SERVICE_GRADE("serviceGrade", 5),
	// 客户联系信息
	CUST_CONTACT_INFO_REL("custContactsInfo", 6),
	// 参与人信息
	PARTY("party", 7),
	// 参与人证件
	PARTY_CERT("partyCert", 8);

	// 范围编码
	private String scopeCode;
	// 范围索引
	private int scopeIndex;

	private QryCustomerScopeInfoEnum(String scopeCode, int scopeIndex) {
		this.scopeCode = scopeCode;
		this.scopeIndex = scopeIndex;
	}

	public String getScopeCode() {
		return scopeCode;
	}

	public int getScopeIndex() {
		return scopeIndex;
	}
}
