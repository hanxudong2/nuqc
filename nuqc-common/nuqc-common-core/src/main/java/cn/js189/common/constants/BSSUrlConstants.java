package cn.js189.common.constants;

/**
 * 智慧BSS，接口地址类
 *
 * @author Mid
 */
public class BSSUrlConstants {
	
	private static final String JT_EOP_BASE_PATH = "http://jsjteop.telecomjs.com:8764/jseop/billing_zw";
	//政企账单汇总
	public static final String FUNDMANAGECUSTBILL = JT_EOP_BASE_PATH + "/FundManageCustBill";
	//政企账单分项
	public static final String FUNDMANAGECUSTBILLDET = JT_EOP_BASE_PATH + "/FundManageCustBillDet";
	// 新版第三期费用查询
	public static final String GET_ACCT_ITEM_FQR = JT_EOP_BASE_PATH + "/getAcctItemFQr-controller/getAcctItemFQr";
	
	// 量本使用明细查询
	public static final String ACCU_USE_DETAIL_QRY = JT_EOP_BASE_PATH + "/accuUseDetailQryAbilityProvince";
	
	// 流量卡有效期余额查询 - 流量卡余量查询
	public static final String GET_FLOW_ACCU_QR = JT_EOP_BASE_PATH + "/getFlowAccuQr/getFlowAccuQr";
	
	// 区域编码查询 - 号码归属地查询
	public static final String QRY_AREA_CD_BY_NBR = JT_EOP_BASE_PATH + "/qryAreaCdByNbr/qryAreaCdByNbr";
	
	// 20007余额查询 - 余额查询
	public static final String GET_BALANCE_SVR_BALANCE_QR = JT_EOP_BASE_PATH + "/getBalanceSvrBalanceQr/getBalanceSvrBalanceQr";
	
	// 宽带清单查询 - 宽带清单查询
	public static final String TKT_GU_BROAD_DATA_QR = JT_EOP_BASE_PATH + "/TktGuBroadDataQr/TktGuBroadDataQr";
	
	// 长话话单信息 - 长途清单查询
	public static final String TKT_GU_CALL_TICKET_QR = JT_EOP_BASE_PATH + "/TktGuCallTicketQr/TktGuCallTicketQr";
	
	// 16101-彩信E详单 - 彩E使用详单
	public static final String TKT_YI_CE_TICKET_QR = JT_EOP_BASE_PATH + "/TktYiCeTicketQr/TktYiCeTicketQr";
	
	// 流量卡充值记录查询 - 流量卡充值记录查询
	public static final String GET_FLOW_PAY_QR = JT_EOP_BASE_PATH + "/getFlowPayQr/getFlowPayQr";
	
	// 流量卡使用详单查询 - 流量卡使用记录查询
	public static final String TGET_TKT_YI_F_FLOW_CARD_USERD_QR = JT_EOP_BASE_PATH + "/getTktYiFFlowCardUserdQr/getTktYiFFlowCardUserdQr";
	
	// 互联星空清单 - 互联星空清单
	public static final String TKT_GU_INFO_DATA_QR = JT_EOP_BASE_PATH + "/TktGuInfoDataQr/TktGuInfoDataQr";
	
	// IPTV使用清单 - ITV清单
	public static final String TKT_GU_IPTV_DATA_QR = JT_EOP_BASE_PATH + "/TktGuIptvDataQr/TktGuIptvDataQr";
	
	// ITV点播清单 - ITV点播清单
	public static final String TKT_GU_IPTV_VALUE_ADDED_QR = JT_EOP_BASE_PATH + "/TktGuIptvValueAddedQr/TktGuIptvValueAddedQr";
	
	// 市话话单信息 - 市话清单
	public static final String TKT_GU_LS_TICKET_QR = JT_EOP_BASE_PATH + "/TktGuLsTicketQr/TktGuLsTicketQr";
	
	// 电话清单查询 - 电话信息清单
	public static final String TKT_GU_MESSAGE_TICKET_QR = JT_EOP_BASE_PATH + "/TktGuMessageTicketQr/TktGuMessageTicketQr";
	
	// 窄带清单查询 - 窄带清单
	public static final String TKT_GU_NARROW_DATA_QR = JT_EOP_BASE_PATH + "/TktGuNarrowDataQr/TktGuNarrowDataQr";
	
	// 账单查询 - 老的客户化帐单接口
	public static final String QRY_NEW_BILL_SVR = JT_EOP_BASE_PATH + "/qryNewBillSvr/qryNewBillSvr";
	
	//查询增值业务详单
	public static final String QRY_SP_DETAIL = JT_EOP_BASE_PATH + "/qrySpDetail";
	
	public static final String QRY_TKT_YI_VALUE_TICKET_QR_BY_DAYCOUNT = "http://jsjteop.telecomjs.com:8764/jseop/billing_cx/getTktYiValueTicketQrByDayCount";
	
	// 移动用户上网及数据通信详单查询 - 移动用户上网及数据通信详单查询
	public static final String TKT_YI_NEW_DATA_TICKET_QR = JT_EOP_BASE_PATH + "/TktYiNewDataTicketQr/TktYiNewDataTicketQr";
	
	// 充值缴费查询 - 充值缴费记录查询
	public static final String GET_NEW_QR_PAY = JT_EOP_BASE_PATH + "/getNewQrPay/getNewQrPay";
	
	//新版第三期一致性余额
	public static final String CON_QUERY_BALANCE = JT_EOP_BASE_PATH + "/ECQueryBalance";
	//新版第三期一致性信用额度查询
	public static final String CON_QUERY_CREDIT = JT_EOP_BASE_PATH + "/ECQueryCreditLimit";
	//新版第三期一致性实时费用明细
	public static final String CON_QUERY_INSTANTFEE_LIST = JT_EOP_BASE_PATH + "/ECQryInstantFeeList";
	//新版第三期一致性详单查询
	public static final String CON_QUERY_BILL_ITEMS = JT_EOP_BASE_PATH + "/ECQueryBillItems";
	
	//新版第三期一致性累积量
	public static final String CON_QUERY_ACCU_USE = JT_EOP_BASE_PATH + "/ECAccuUseQry";
	// 量本使用查询新地址(非第三期)
	public static final String ACC_USER_QRY_PROVINCE = JT_EOP_BASE_PATH + "/accuUseQryProvince/accuUseQryProvince";
	public static final String QUERY_BALANCE = JT_EOP_BASE_PATH + "/QueryBalanceNew/QueryBalanceNew";
	
	//  新业务清单查询
	public static final String NEW_SVR_TIC_GU = JT_EOP_BASE_PATH+ "/TktGuNewSvrTicQr/TktGuNewSvrTicQr";
	
	//   查询账户信息OTH_GET_ACCT
	public static final String OTH_GET_ACCT = JT_EOP_BASE_PATH+ "/OthGetAcct/OthGetAcct";
	
	//第三次集团一致性
	public static final String CON_U_GET_ACCT_OWE = JT_EOP_BASE_PATH+ "/ECQryBill";
	public static final String U_GET_ACCT_OWE = JT_EOP_BASE_PATH+ "/QryBillNew/QryBillNew";
	
	//   短信通信清单_移动TKT_YI_V_SM_TICKET_QR
	public static final String TKT_YI_V_SM_TICKET_QR = JT_EOP_BASE_PATH+ "/TktYiVSmTicketQr/TktYiVSmTicketQr";
	
	//   C网短信_彩信增值详单_移动
	public static final String TKT_YI_SMS_VALUE_TICKET_QR = JT_EOP_BASE_PATH+ "/TktYiSmsValueTicketQr/TktYiSmsValueTicketQr";
	
	//本金、赠款销账金额查询CUSTOMER_QRY(cust_use_qry)
	public static final String CUSTOMER_QRY = JT_EOP_BASE_PATH+ "/CustomerQry/CustomerQry";
	
	//宽带清单查询 TKT_GU_BROAD_DATA_QR
	public static final String TKT_GU_BROAD_DATA_QR_N = JT_EOP_BASE_PATH+ "/TktGuBroadDataQr/TktGuBroadDataQr";
	
	//语音详单查询  TKT_YI_VOICE_TICKET_QR
	public static final String TKT_YI_VOICE_TICKET_QR = JT_EOP_BASE_PATH+ "/TktYiVoiceTicketQr/TktYiVoiceTicketQr";
	
	//一致性余额变动汇总查询CON_U_GET_EX_BAL
	public static final String CON_U_GET_EX_BAL = JT_EOP_BASE_PATH+ "/QryBalanceRecord/QryBalanceRecord";
	
	// 三期 账单查询U_GET_CUSTBILL
	public static final String U_GET_CUSTBILL = JT_EOP_BASE_PATH+ "/QryCustBillNew/QryCustBillNew";
	
	//一致性充值记录查询CON_U_GET_PAY_BILL
	public static final String CON_U_GET_PAY_BILL = JT_EOP_BASE_PATH+ "/ECQryPayment";
	
	//欠费查询接口OTH_GET_OWE
	public static final String OTH_GET_OWE = JT_EOP_BASE_PATH+ "/OthGetOwe/OthGetOwe";
	
	//查询业务接口类型OTH_GET_TYPE
	public static final String OTH_GET_TYPE = JT_EOP_BASE_PATH+ "/OthGetServType/OthGetServType";
	
	// 短信订制清单  TKT_YI_UNI_MSG_TICKET_QR
	public static final String TKT_YI_UNI_MSG_TICKET_QR = JT_EOP_BASE_PATH+ "/TktYiUniMsgTicketQr/TktYiUniMsgTicketQr";
	
	//其他增值详单  TKT_YI_VALUE_TICKET_QR
	public static final String  TKT_YI_VALUE_TICKET_QR = JT_EOP_BASE_PATH+ "/TktYiValueTicketQr/TktYiValueTicketQr";
	
	//WLAN清单查询  TKT_GU_WLAN_TICKET_QR
	public static final String   TKT_GU_WLAN_TICKET_QR = JT_EOP_BASE_PATH+ "/TktGuWlanTicketQr/TktGuWlanTicketQr";
	
	//余额变动明细查询U_GET_EX_DET
	public static final String  CON_U_GET_EX_DET = JT_EOP_BASE_PATH+ "/QryBalanceRecordDetail/QryBalanceRecordDetail";
	
	//话费返还记录查询U_GET_BAL_RTN
	//一致性话费返还记录查询CON_U_GET_VAL_RTN
	public static final String  CON_U_GET_BAL_RTN = JT_EOP_BASE_PATH+ "/QryReturnBalanceDetail/QryReturnBalanceDetail";
	
	//话费返还记录明细查询U_GET_BAL_RTN_D
	public static final String  CON_U_GET_BAL_RTN_D = JT_EOP_BASE_PATH+ "/QryReturnBalanceInfoDetail/QryReturnBalanceInfoDetail";
	
	//新客户话账单查询地址
	public static final String QRY_NEW_CUST_BILL = "http://jsjteop.telecomjs.com:8764/jsbillcx/ECQryCustBill";
	
	//实时话费明细查询地址
	public static final String  QRY_INSTANT_FEE_LIST = "http://jsjteop.telecomjs.com:8764/jseop/billingjf/ECQryInstantFeeList_DQ";
	
	//消费趋势查询接口
	public static final String  QRY_FEE_TREND = "http://jsjteop.telecomjs.com:8764/jseop/billingjf/FeeTrend";
	
	//拆机号码资料信息、欠费信息查询
	//http://132.254.8.140:8881/jsjf/CecServices/qry/othBackoutFeeInfo
	public static final String  BACK_OUT_FEE_INFO_URL = "http://jsjteop.telecomjs.com:8764/jsjf/CecServices/qry/othBackoutFeeInfo";
	
	/**
	 * 线上销户计费-余额查询接口
	 * test:132.228.119.42:8097
	 * 正式：132.254.13.41:9009
	 */
	public static final String QUERY_BALANCE_URL = "http://132.254.13.41:9009/adjust/account/thirdBalanceMove_queryBlanceToThird.do";
	
	
	/**
	 * 线上销户计费-余额提取或结转进度查询接口
	 * test:132.254.13.41:9009
	 * 正式：132.254.13.41:9009
	 */
	public static final String QUERY_BALANCE_PROGRESS_URL = "http://132.254.13.41:9009/adjust/account/thirdBalanceMove_queryBalanceProgress.do";
	
	//政企门户业务进度查询
	public static final String  CURRENT_TACH_INFO_TO_CRM = "http://jsjteop.telecomjs.com:8764/jseop/oss/getCurrentTachInfoToCrm";
	
	//根据购物车判断是否分解成功
	public static final String  GET_OD_BYCUSTORDERCODE = "http://jsjteop.telecomjs.com:8764/jseop/oss/getODByCustOrderCode";
	
	//充值缴费查询(新)
	public static final String GET_NEW_QR_PAY_NEW_URL = "http://jsjteop.telecomjs.com:8764/jseop/billing_zw/getNewQrPay/getNewQrPay";
	
	//费用查询
	public static final String GET_ACCT_ITEM_FOR_DIAG_URL = "http://jsjteop.telecomjs.com:8764/jseop/billing_zw/getAcctItemForDiag/getAcctItemForDiag";
	
	//往月拆机用户建议充值金额
	public static final String QRY_DEPOSIT_AMOUNT = "http://jsjteop.telecomjs.com:8764/jseop/billing_cx/QryDepositAmount";
	//EOP通用测试地址
	public static final String EOP_TEST_URL = "http://132.252.128.217:32516/eop/test_crmservices/http_common";

	//查询账户的地址
	public static final String EOP_QRY_ACCOUNT = "https://jseop.telecomjs.com/eop/crmservices/js.intf.qryAccount?appkey=js_dzqd";
	//查询客户的地址
	public static final String EOP_QRY_CUSTOMER = "https://jseop.telecomjs.com/eop/crmservices/js.intf.qryCustomer?appkey=js_dzqd";
	//查询客户订单
	public static final String EOP_QRY_CUSTOMER_ORDER = "https://jseop.telecomjs.com/eop/crmservices/js.intf.qryCustomerOrder?appkey=js_dzqd";
	//查询订单项详情
	public static final String EOP_QRY_ORDERITEM_DETAIL = "https://jseop.telecomjs.com/eop/crmservices/js.intf.qryOrderItemDetail?appkey=js_dzqd";
	//查询组合产品内部成员关系
	public static final String EOP_QRY_MEMBERINFO = "https://jseop.telecomjs.com/eop/crmservices/js.intf.qryMemberInfo?appkey=js_dzqd";

	public static final String NEW_EOP_URL = "http://132.224.197.27:10901/serviceAgent/http/dfsGetData";//无用

	//查询销售品实例接口地址
	public static final String EOP_QRY_OFFERINST = "https://jseop.telecomjs.com/eop/crmservices/js.intf.qryOfferInst?appkey=js_dzqd";

	//查询产品实例
	public static final String EOP_QRY_PRODINST = "https://jseop.telecomjs.com/eop/crmservices/js.intf.qryProdInst?appkey=js_dzqd";

	//查询产品实例
	public static final String EOP_QRY_PRODINSTREL = "https://jseop.telecomjs.com/eop/crmservices/js.intf.qryProdInstRel?appkey=js_dzqd";

	//政企验证接口
	public static final String EOP_CHECK_GOVCUSTOMER = "https://jseop.telecomjs.com/eop/crmservices/js.intf.checkGovCustomer?appkey=js_dzqd";

	//PUK码查询
	public static final String EOP_QRY_PUKCODE = "https://jseop.telecomjs.com/eop/crmservices/js.intf.qryPUKCode?appkey=js_dzqd";

	//根据partyId,startDt,endDt查询电子签名信息
	public static final String EOP_QRY_CUSTORDERANDPDFFLAG = "https://jseop.telecomjs.com/eop/crmservices/js.intf.qryCustOrderAndPdfFlag?appkey=js_dzqd";

	//查询无纸化标识信息详情
	public static final String EOP_QRY_PDFBYOLNBR = "https://jseop.telecomjs.com/eop/crmservices/js.intf.qryAppointAgreement?appkey=js_dzqd";
	//实名认证
	public static final String EOP_CHECK_REALNAME = "https://jseop.telecomjs.com/eop/crmservices/js.intf.checkRealName?appkey=js_dzqd";

	//查询可订购【功能/服务】产品
	public static final String QRY_OPTIONALPROD = "http://jseop.telecomjs.com:18888/eop/crm_cpcp/cpc_ppm_qryOptionalProd?appkey=js_dzqd";

	//查询服务参数信息及分组规则信息(没用到)
	public static final String QRY_PROD_ATTRLIMITRULS = "http://132.252.129.147/serviceAgent/http/cpc_ppm_qryProdAttrLimitRuls";

    //发票抬头增改查地址
    // 7996745668740972614  GET
    // 7996745671093977101  PATCH请求
    // 7996745681953030215 POST
    public static final String INVOICE_INFORMATION_URL = "http://jsjteop.telecomjs.com:8764/jseop/crm_saop/invoiceInformation/invoiceInformation";
    
    public static final String GET_NUM_INFO_BY_CARD_NO_URL ="http://jsjteop.telecomjs.com:8764/jseop/oss/O3Interface/crm/getNumInfoByCardNo";

	//查询产品规格id
	public static final String EOP_QRY_PRODID = "https://jseop.telecomjs.com/eop/crmservices/js.intf.qryProdId?appkey=js_dzqd";

	//2.59.查询已经订购的提醒定制实例
	public static final String EOP_QRY_REMINDPRODINST = "https://jseop.telecomjs.com/eop/crmservices/js.intf.qryRemindProdInst?appkey=js_dzqd";

	//查询pdf详情
	public static final String EOP_DFSGETDATA = "http://jseop.telecomjs.com:18888/eop/crmservices/dfsGetData/CRM?appkey=js_dzqd";
	//eop密钥
	public static final String EOP_KEY = "aiweb123";

	// 一证五卡(新)
	public static final String EOP_QRY_CRTNBR = "http://jseop.telecomjs.com:12556/eop/crmservices/new/so_queryCrtfctNmbrRltn?appkey=js_dzqd";
	//一张五卡预校验(不含物联网卡)
	public static final String EOP_QRY_CRTNBR_PRE_CHECK = "http://openapi.telecomjs.com:80/eop/jtsaop/so.precheckCrtfctNmbrRltn";
	//一证多卡校验
	public static final String ONE_CERT_MUL_CARD = "http://jsjteop.telecomjs.com:8764/jseop/crm_saop/oneCertMulCard";
	
	//查询本自然年内强制复机订单的接口
	public static final String EOP_QRY_PHONENUMINFO = "http://openapi.telecomjs.com:80/eop/jtsaop/so.qryPhoneNumInfo";


	// ID5_信息校验查询（无用）
	public static final String EOP_CHECK_ID5 = "https://eop.telecomjs.com/serviceAgent/http/getCheckResultId5";
	//查询销售品规格信息
	public static final String EOP_QRY_OFFER_DETAIL = "http://jseop.telecomjs.com:12556/eop/crm_cpcp/cpc_ppm_qryOfferDetail?appkey=js_dzqd";
	//查询纳税人信息
	public static final String EOP_QRY_TAX_PLAYER = "https://jseop.telecomjs.com/eop/crmservices/js.intf.qryGeneralTaxpayer?appkey=js_dzqd";
	//查询黑名单接口
	public static final String EOP_QRY_SPECIAL_LIST = "https://jseop.telecomjs.com/eop/crmservices/js.intf.qryProvincialBlackList?appkey=js_dzqd";
	//天翼十年接口(不用了)
	public static final String EOP_QRY_ASDL_INFO = "https://eop.telecomjs.com/serviceAgent/http/JS00000001/js.intf.qryAdslProdInst";

	public static final String EOP_CHECK_MNP_ABILITY = "https://jseop.telecomjs.com/eop/crmservices/js.intf.portOutAbility?appkey=js_dzqd";

	public static final String EOP_CHECK_MNP = "https://jseop.telecomjs.com/eop/crmservices/js.intf.authcodeReq?appkey=js_dzqd";

	public static final String EOP_MNP_RM_OUTABLE = "https://jseop.telecomjs.com/eop/crmservices/js.intf.removeOutAbility?appkey=js_dzqd";

	public static final String EOP_QRY_SUPERB_NUMBER_TEST = "http://132.252.128.217:32516/eop/test_crmservices/http_common";

	public static final String EOP_QRY_SUPERB_NUMBER = "https://jseop.telecomjs.com/eop/crmservices/js.intf.pottomConsumerLogo?appkey=js_dzqd";
	//增值服务
	public static final String EOP_QRY_VALUEADDEDSERVICE = "https://jseop.telecomjs.com/eop/crmservices/js.intf.qryMyValueAddedService?appkey=js_dzqd";

	// 查询员工信息
	public static final String EOP_QRY_STAFF = "https://jseop.telecomjs.com/eop/crmservices/js.intf.qryStaff?appkey=js_dzqd";
	// 查询渠道信息
	public static final String EOP_QRY_CHANNEL = "https://jseop.telecomjs.com/eop/crmservices/js.intf.qryChannel?appkey=js_dzqd";
	// 用户证件敏感信息校验
	public static final String EOP_CHECK_PARTYCERT = "https://jseop.telecomjs.com/eop/crmservices/js.intf.checkPartyCert?appkey=js_dzqd";
	// 查询特殊名单列表-企信灰名单
	public static final String EOP_QRY_SPECIAL_H_LIST = "https://jseop.telecomjs.com/eop/crmservices/js.intf.qrySpecialList?appkey=js_dzqd";
	//违约金试算
	public static final String EOP_RPEHANDLEHTTPREQ = "https://jseop.telecomjs.com/eop/crmservices/js.intf.rpeHandleHttpReq?appkey=js_dzqd";
	//订单算费（费用明细）
	public static final String EOP_CALCHARGEDETAILINFO = "https://jseop.telecomjs.com/eop/crmservices/js.intf.calChargeDetailInfo?appkey=js_dzqd";
	//查询客户联系方式
	public static final String EOP_QRY_CONTACTINFOBYCERTNUM = "https://jseop.telecomjs.com/eop/crmservices/js.intf.qryContactInfoByCertNum?appkey=js_dzqd";
	// 查询一年内办卡数量
	public static final String EOP_QRY_PHONENUMCOUNT = "https://jseop.telecomjs.com/eop/crmservices/js.intf.qryPhoneNumCount?appkey=js_dzqd";
	// 查询客户一年内风险复机次数，个人使用
	public static final String EOP_QRY_PHONERESETNUMCOUNT = "https://jseop.telecomjs.com/eop/crmservices/js.intf.qryPhoneResetNumCount?appkey=js_dzqd";
	// 查询用户一年内风险复机次数，政企使用
	public static final String EOP_QRY_USERPHONERESETNUMCOUNT = "https://jseop.telecomjs.com/eop/crmservices/js.intf.qryUserPhoneResetNumCount?appkey=js_dzqd";
	// 2.28. 查询发送停机通知短信结果
	public static final String EOP_QRY_SENDTJMESSAGERESULT = "https://jseop.telecomjs.com/eop/crmservices/js.intf.qrySendTJMessageResult?appkey=js_dzqd";
	// 生成副卡风险停机后复机订单
	public static final String EOP_CREATE_FKFXDTFJORDER = "https://jseop.telecomjs.com/eop/crmservices/js.intf.createFKFXDTFJOrder?appkey=js_dzqd";
	//根据OFFER_SYS_TYPE和OFFER_TYPE查询销售品集合
	public static final String EOP_CPC_PPM_QRYOFFERLIST = "http://jseop.telecomjs.com:18888/eop/crm_cpc/cpc_ppm_qryOfferList?appkey=js_dzqd";
	//查询销售品集合
	public static final String EOP_CPC_PPM_QRYOFFERGRPMEMBERS = "http://openapi.telecomjs.com:80/eop/CPCP/qryOfferGrpMembers/cpc_ppm_qryOfferGrpMembers";

	public static final String EOP_CPC_PPM_QRY_OFFER_GRO_RELA = "http://openapi.telecomjs.com:80/eop/CPCP/cpc_ppm_qryOfferGrpRela/cpc_ppm_qryOfferGrpRela";

	//查询客户未签名购物车
	public static final String EOP_QRY_CUSTOMER_ORDER_TYPE = "https://jseop.telecomjs.com/eop/crmservices/js.intf.qryCustomerOrderByTypeOld?appkey=js_dzqd";
	//远程签名信息查询
	public static final String EOP_QRY_REMOTE_SIGN_INFO = "https://jseop.telecomjs.com/eop/crmservices/js.intf.onl.qryRemoteSignInfo?appkey=js_dzqd";//新地址

	//营业厅门店ID与BSS渠道ID对应关系查询
	public static final String EOP_QRY_GET_STORE_MSM = "http://openapi.telecomjs.com:80/eop/xsqd/getStoreMsg/getStoreMsg";

	//鉴权地址
	public static final String GET_TOKEN = "http://132.252.34.146:9009/gisIntf/account/gettoken";
	//经纬度查询所在网格接口地址
	public static final String QRY_GRID_BY_LON_LAT = "http://132.252.34.146:9009/gisintf/gis/grid/GridController/queryGridByLonLat.do";

	public static final String QRY_NEAR_BY_GRID_AND_LON_LAT = "http://132.252.34.146:9009/gisintf/gis/grid/GridController/queryNearbyGridByLonLat.do";

	public static final String EOP_QRY_CUST_LEVEL = "https://jseop.telecomjs.com/eop/crmservices/js.intf.qryCustlevelAndKeyPerson?appkey=js_dzqd";

	//落地网格查询接口地址
	public static final String QRY_REAL_GRID_BY_SERV_ID = "http://jsjteop.telecomjs.com:8764/jseop/css_api/marketPlace/getRealGridListByServId.do";

	//Bss专线在途判断
	public static final String OSS_GET_BSS_INFO = "http://132.254.14.185:8082/oss_report/login/getBssInfo.do";

	// 查询我的套餐
	public static final String EOP_QRY_MYPRODUCTOFFER = "https://jseop.telecomjs.com/eop/crmservices/js.intf.qryMyProductOffer?appkey=js_dzqd";

	// 预约信息提交
	public static final String EOP_ZHYX_ADDACTRES = "http://openapi.telecomjs.com:80/eop/ZHYX/addActivityReservation/addActivityReservation";

	// 查询订单接入产品
	public static final String EOP_QRY_ACC_PROD_INST = "https://jseop.telecomjs.com/eop/crmservices/js.intf.qryAccProdInstsByCsNbr?appkey=js_dzqd";

	//UIM卡是否支持指定服务校验
	public static final String OSS3_CHECK_SERV_SPEC_BY_DEV_MODEL_URL = "http://jseop.telecomjs.com:18888/serviceAgent/http/checkServSpecByDevModel?appkey=js_dzqd";
	
	//查询账户合并关系
	public static final String EOP_QRYACCTRELBYPRODINSTID = "https://jseop.telecomjs.com/eop/crmservices/js.intf.qryAcctRelByProdInstId?appkey=js_dzqd";
	
	//根据号码查询订单列表
	public static final String EOP_QRYORDERBYACCNUMLIST = "http://jsjteop.telecomjs.com:8764/jseop/crm_saop/js_intf_qryOrderByAccNumList";

	//查询60天内无理由退货纪录
	public static final String EOP_QRYREDUCTIONINFORECORDS = "http://jsjteop.telecomjs.com:8764/jseop/crm_saop/js_intf_qryReductionInfoRecords";

	//销售品扩展属性查询
	public static final String EOP_QRYOFFEREXTATTRS = "http://jsjteop.telecomjs.com:8764/jseop/crm_saop/js_intf_qryOfferExtAttrs";
	
	//产品拆机记录查询
	public static final String EOP_QRYHISACCPRODINSTLISTLOCAL = "http://jsjteop.telecomjs.com:8764/jseop/crm_saop/js_intf_qryHisAccProdInstListLocal";
//	public static final String EOP_QRYHISACCPRODINSTLISTLOCAL = "http://132.252.129.160/jseop/crm_saop/js_intf_qryHisAccProdInstListLocal";

	//江苏分发标识查询
//	public static final String EOP_XW_RY_DISPATCHSIGN = "http://132.252.129.160/jseop/crm_saop/js_xw_ry_dispatchSign";
	public static final String EOP_XW_RY_DISPATCHSIGN = "http://jsjteop.telecomjs.com:8764/jseop/crm_saop/js_xw_ry_dispatchSign";

	// 查询礼包/商品实例
	// public static final String  QRY_GOODS_AND_PKG="http://132.252.129.160/jseop/crm_saop/js_xw_ry_qryPkgAndGoods";
	public static final String  QRY_GOODS_AND_PKG="http://jsjteop.telecomjs.com:8764/jseop/crm_saop/js_xw_ry_qryPkgAndGoods";


}
