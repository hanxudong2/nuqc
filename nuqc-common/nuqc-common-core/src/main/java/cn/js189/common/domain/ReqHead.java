package cn.js189.common.domain;

import lombok.Data;

import java.io.Serializable;

/**
 * 请求结果封装
 */
@Data
public class ReqHead implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	/** 流水号 */
	private String tranId;
	/** 渠道id */
	private String channelId;
	/** 属地编号 */
	private String areaCode;
	/** 登录用户 */
	private String loginNbr;
	/** 登录类型 */
	private String loginType;
	/** 用户ip */
	private String userIp;
	/** 请求时间 */
	private String requestTime;
	/** 认证 */
	private String auth;
	/** 请求信息 */
	private String msg;
	/** 请求状态 */
	private String status;
	/** 接口编号 */
	private String actionCode;
	
}
