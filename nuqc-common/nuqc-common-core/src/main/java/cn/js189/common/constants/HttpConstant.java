/**************************************************************************************** 

 ****************************************************************************************/
package cn.js189.common.constants;

/** 
 * <Description> <br> 
 *  
 * @author zhangpzh<br>
 * @version 1.0<br>
 * @taskId <br>
 * @CreateDate 2016年4月9日 <br>
 */

public class HttpConstant {
	
	private HttpConstant(){}
    
    /** The Constant UTF_ENCODING. */
    public static final String UTF_ENCODING = "UTF-8";
    
    /** The Constant GB2312_ENCODING. */
    public static final String GB2312_ENCODING = "GB2312";
    
    /** The Constant CONNECT_TIMEOUT. */
    public static final int CONNECT_TIMEOUT = 10000;
    
    /** The Constant SOCKET_TIMEOUT. */
    public static final int SOCKET_TIMEOUT = 30000;
    
    /** The Constant REQUEST_TIMEOUT. */
    public static final int REQUEST_TIMEOUT = 10000;
    
    /** The Constant ROUTE_SIZE. */
    public static final int ROUTE_SIZE = 1000;
    
    /** The Constant MAX_TOTAL. */
    public static final int MAX_TOTAL = 500;
    
}
