package cn.js189.common.constants;

/**
 * 错误码常量定义
 * */
public class ExpConstant {
	
	private ExpConstant(){}

    /**
     * 返回成功结果码
     * */
    public static final String ERROR_SUCCESS = "0";
    
    /**
     * 入参有误
     * */
    public static final String ERROR_PARAMS= "6666";
    
    /**
     * 请求为空异常码
     * */
    public static final String ERROR_INPUT_NULL = "7777";
    
    /**
     * 返回为空异常码
     * */
    public static final String ERROR_OUTPUT_NULL = "9999";
    
    /**
     * 内部异常
     * */
    public static final String ERROR_INNER_EXCEPTION = "8888";
    
}
