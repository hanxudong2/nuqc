/**    
 * @{#} JSonHelper.java Create on 2011-9-22 下午04:03:26    
 *    
 * Copyright (c) 2011 by 创发科技.    
 */    
package cn.js189.common.util.helper;

import cn.js189.common.enums.CodeEnum;
import cn.js189.common.enums.SystemEnum;
import cn.js189.common.util.MainUtils;
import cn.js189.common.util.exception.ToolException;
import com.alibaba.fastjson.JSONObject;

/**    
 * @author <a href="h124436797@yahoo.com.cn">hrg</a>   
 * @version 1.0    
 */

public class JSonResultHelper {
	
	private JSonResultHelper(){}
	
	/**
	 * 
	 * 描述: 出现异常的时候统一返回的json格式
	 * @param result
	 * @param code
	 * @param msg
	 * @return
	 * @author     huangrougang
	 * date        2011-9-22
	 * --------------------------------------------------
	 * 修改人    	      修改日期       修改描述
	 * huangrougang        2011-9-22       创建
	 * --------------------------------------------------
	 * @Version  Ver1.0
	 */
	public static String exportResult(String result,String code,String msg){
		return "{\"" +
				SystemEnum.TSR_RESULT.name() +
				"\":\"" +
				MainUtils.getNullEmpty(result) +
				"\",\"" +
				SystemEnum.TSR_CODE.name() +
				"\":\"" +
				MainUtils.getNullEmpty(code) +
				"\",\"" +
				SystemEnum.TSR_MSG.name() +
				"\":\"" +
				MainUtils.getNullEmpty(msg) +
				"\"}";
		
		
	}
	
	/**
	 * 
	 * 描述: 为空返回json格式
	 * @param name
	 * @return
	 * @author     huangrougang
	 * date        2011-9-23
	 * --------------------------------------------------
	 * 修改人    	      修改日期       修改描述
	 * huangrougang        2011-9-23       创建
	 * --------------------------------------------------
	 * @Version  Ver1.0
	 */
	public static String exportResultBlank(String name){
		return JSonResultHelper.exportResult(CodeEnum.CT_11111.name(), CodeEnum.CT_11111.name(), name+" is null");
		
	}
	
	
	/**
	 * 
	 * 描述: 调用接口返回值为空
	 * @return
	 * @author     huangrougang
	 * date        2011-9-26
	 * --------------------------------------------------
	 * 修改人    	      修改日期       修改描述
	 * huangrougang        2011-9-26       创建
	 * --------------------------------------------------
	 * @Version  Ver1.0
	 */
	public static String exportReturnResultBlank(){
		return JSonResultHelper.exportResult(CodeEnum.CT_66666.name(), CodeEnum.CT_66666.name(),"return value is null");
		
	}
	
	/**
	 * 
	 * 描述: 返回空值的json对象
	 * @return
	 * @author     huangrougang
	 * date        2011-9-26
	 * --------------------------------------------------
	 * 修改人    	      修改日期       修改描述
	 * huangrougang        2011-9-26       创建
	 * --------------------------------------------------
	 * @Version  Ver1.0
	 */
	public static JSONObject exportJSONObjectReturnResultBlank(){
		try{
		JSONObject json=new JSONObject();
		json.put(SystemEnum.TSR_RESULT.name(), CodeEnum.CT_66666.name());
		json.put(SystemEnum.TSR_CODE.name(), CodeEnum.CT_66666.name());
		json.put(SystemEnum.TSR_MSG.name(), CodeEnum.CT_66666.getMsg());
		return json;
		}catch(Exception e){
			throw new ToolException(e);
		}
		
	}
	
	/**
	 * 
	 * 描述: 返回其它错误的json对象
	 * @return
	 * @author     huangrougang
	 * date        2011-9-26
	 * --------------------------------------------------
	 * 修改人    	      修改日期       修改描述
	 * huangrougang        2011-9-26       创建
	 * --------------------------------------------------
	 * @Version  Ver1.0
	 */
	public static JSONObject exportJSONObjectOtherError(){
		try{
			JSONObject json=new JSONObject();
			json.put(SystemEnum.TSR_RESULT.name(), CodeEnum.CT_60005.name());
			json.put(SystemEnum.TSR_CODE.name(), CodeEnum.CT_60005.name());
			json.put(SystemEnum.TSR_MSG.name(), CodeEnum.CT_60005.getMsg());
			return json;
			}catch(Exception e){
				throw new ToolException(e);
			}
	}
	
	
	/**
	 * 
	 * 描述: 返回其它错误
	 * @return
	 * @author     huangrougang
	 * date        2011-9-26
	 * --------------------------------------------------
	 * 修改人    	      修改日期       修改描述
	 * huangrougang        2011-9-26       创建
	 * --------------------------------------------------
	 * @Version  Ver1.0
	 */
	public static String exportOtherError(){
		return JSonResultHelper.exportResult(CodeEnum.CT_60005.name(), CodeEnum.CT_60005.name(), CodeEnum.CT_60005.getMsg());
	}
	
}
  
