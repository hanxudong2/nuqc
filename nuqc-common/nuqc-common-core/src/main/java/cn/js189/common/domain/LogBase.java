package cn.js189.common.domain;

import lombok.Data;

import java.io.Serializable;

/**
 * 保存日志基础信息
 */
@Data
public class LogBase implements Serializable {

    private static final long serialVersionUID = 1L;

    /** 参数 */
    private String params;

    /** 请求时间 */
    private String requestTime;

    /** 接口耗时 */
    private Long useTime;

    /** 节点类型 */
    private String type;

    /** 接口路径 */
    private String path;

    /** 环境 */
    private String isTest;

    /** 方法名 */
    private String method;

    /** 接口编码 */
    private String name;

    /** 接口提供方 */
    private String owner;

    /** 第三方接口地址 */
    private String url;

    /** 结果 */
    private String result;

    /** 响应时间 */
    private String responseTime;

}
