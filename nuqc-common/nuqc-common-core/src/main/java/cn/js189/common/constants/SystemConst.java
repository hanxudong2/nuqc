package cn.js189.common.constants;

import cn.js189.common.domain.ContextHelper;
import org.springframework.context.ApplicationContext;

import java.util.Locale;


/**
 * <Description> 系统常量 <br>
 * 
 * @author liuzg<br>
 * @version 1.0<br>
 * @taskId <br>
 * @CreateDate 2016年8月25日 下午3:19:55 <br>
 */
public class SystemConst {
    /**
     * 货币符号
     */
    public static final String SYS_CHARSET_DOLLAR = "$";

    /**
     * 字符集编码UTF-8
     */
    public static final String SYS_CHARSET_UTF8 = "UTF-8";

    /**
     * 字符冒号
     */
    public static final String SYS_CHARSET_COLON = ":";

    /**
     * 字符问号
     */
    public static final String SYS_CHARSET_QUESTION = "?";

    /**
     * 字符"|"
     */
    public static final String SYS_CHARSET_VERTICAL = "|";

    /**
     * 字符">"
     */
    public static final String SYS_CHARSET_GREATE = ">";

    /**
     * 字符"<"
     */
    public static final String SYS_CHARSET_LESS = "<";

    /**
     * 字符"="
     */
    public static final String SYS_CHARSET_EQUAL = "=";
    
    /**
     * 字符"[" <br>
     */
    public static final String SYS_LEFT_SQUARE_BRACKET = "[";
    
    /**
     * 字符"]" <br>
     */
    public static final String SYS_RIGHT_SQUARE_BRACKET = "]";

    /**
     * 系统错误异常编码
     */
    public static final String SYS_ERROR_CODE = "999999";
    
    /**
     * ERP回参错误编码
     */
    public static final String ERP_ERROR_CODE = "1";

    /**
     * 系统错误异常描述
     */
    public static final String SYS_ERROR_MSG = "系统开小差，请稍后再试~";

    /**
     * 接口成功编码
     */
    public static final String SUCCESS = "000000";

    /**
     * 商品域编码
     */
    public static final String USC = "USC";

    /**
     * 订单域编码
     */
    public static final String UBC = "UBC";
    
    /**
     * 电子协议域渠道
     */
    public static final String UEC="uec";
    
    /**
     * 数字0
     */
    public static final String NUMBER_STR_ZERO = "0";

    /**
     * 数字1
     */
    public static final String NUMBER_STR_ONE = "1";

    /**
     * 数字2
     */
    public static final String NUMBER_STR_TWO = "2";

    /**
     * 数字3
     */
    public static final String NUMBER_STR_THREE = "3";

    /**
     * 数字4
     */
    public static final String NUMBER_STR_FOUR = "4";

    /**
     * 数字5
     */
    public static final String NUMBER_STR_FIVE = "5";
    
    /**
     * 数字6
     */
    public static final String NUMBER_STR_SIX = "6";
    
    /**
     * 数字7
     */
    public static final String NUMBER_STR_SEVEN = "7";
    
    /**
     * 数字8
     */
    public static final String NUMBER_STR_EIGHT = "8";
    
    /**
     * 数字9
     */
    public static final String NUMBER_STR_NINE = "9";
    
    /**
     * 操作成功
     */
    public static final String OPER_SUCCESS = "0000";
    
    /**
     * 时间格式 <br>
     */
    public static final String TIME_STR = "yyyyMMddHHmmssSSS";
    
    /**
     * BSS_CHANNEL_ID <br>
     */
    public static final String BSS_CHANNEL_ID = "-10012";
    
    /**
     * 订单分账信息为空
     */
    public static final String ORDER_SPLIT_NULL = "订单分账信息为空";
    
    /**
     * 成功 <br>
     */
    public static final String INTERFACE_SUCCESS = "0";
    
    /**
     * 失败 <br>
     */
    public static final String INTERFACE_FAIL = "1";
    
    /**
     * 逗号 <br>
     */
    public static final String COMMA = ",";

    /**
     * Description:获取当前域名称：usc、ubc <br>
     * 
     * @author liuzg<br>
     * @taskId <br>
     * @return <br>
     */
    public static String getSysName() {
        ApplicationContext ctx = ContextHelper.getApplicationContext();
        return ctx.getMessage("sys_name", null, "", Locale.CHINA);
    }
}
