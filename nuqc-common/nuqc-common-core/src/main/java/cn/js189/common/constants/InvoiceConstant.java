package cn.js189.common.constants;

public class InvoiceConstant {

	private InvoiceConstant() {
		throw new IllegalStateException("Utility class");
	}
	
	//已开具
	public static final String INVOICE_FINISH = "1";
	
	//未开具
	public static final String INVOICE_NOT = "2";
	
	//开具中申请成功
	public static final String INVOICE_ING = "3";

	//开具申请失败
	public static final String INVOICE_FAIL = "4";
	
	
	
	
	
	//pdf未下载
	public static final String PDF_EMPTY = "0";
	
	//下载成功
	public static final String PDF_SUCCESS = "1";
		
	//下载失败
	public static final String PDF_FAIL = "2";

	/**
	 * 微信插卡锁前缀
	 */
	public static final String WX_CARD_INSERT_PREFIX = "WX_CARD_INSERT_";

	/**
	 * 支付宝插卡锁前缀
	 */
	public static final String ALI_CARD_INSERT_PREFIX = "ALI_CARD_INSERT_";

	public static final String WX_UPDATE_AUTH_INFO_PREFIX = "UPDATE_INSERT_AUTH_";

	public static final String INIT_USER_INFO_PREFIX = "INIT_USER_INFO_";


	/**
	 * 企信侧同步发票信息锁
	 */
	public static final String QX_SYNC_INVOICE_DETAIL_PREFIX = "QX_SYNC_INVOICE_DETAIL_";

	/**
	 * 用户提交开票申请
	 */
	public static final String USER_SUBMIT_INVOICE_ = "USER_SUBMIT_INVOICE_";

	/**
	 * 查询用户授权状态
	 */
	public static final String QUERY_USER_AUTHTYPE_INSERT_ = "QUERY_USER_AUTHTYPE_INSERT_";
	
	
	public static final String APPID = "2019071865816965";
	
	public static final String ALIPAY_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkt1iLFrIgRYAFHXGaXLjV//L6ovMV1KkTGM+P6ia6NPeMIIphfzVajgMCDwaKLRXetOm7bCyhINxDpJH5WkOpvMtqTuva5j3SH8RnC0cPirKpvsmvunASSt6uu+7rT+GgDqzN/a1zAw+IHFjTzSLI1x1iakKwhJ8mZCQmUaoAhcw+FBSSXH2EzifW6ap285iyBiabSoPME+95FsiTguFAWoNeD/yIFhVXdPLDnZF7OeVqWuM+xWRFu0SqOKFkeE/Z/nXkvRm0YEGIDDcPgDP9W02kWaoX/xskSs5xhxu35cIvzU9q87UW1lm9oJw5fA81yxTJ42iT+KJFJzE2/nRVQIDAQAB";
	
	public static final String APP_PRIVATE_KEY = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQC16Hb6GWGW9QP9lEgdsvFfFy+fYtzXTwg+CY9AFQiUxflOGBnaKToOBLqXsWIh6E5ISsbW2AU+q+MMVHGUqmKuie9zuyNns0Jdtpri5WItmLqF+avBJJu1HklXIuqSC6PBDS7hLoWM8ay/VDpNBl+3ZaNEn2RfMLJmTscy9O9QsHO/85KwJ5B/Q0U08fXUVk6400ruMnj16fsIVG44Ds6o905eqpxGSM+N20yCPp9iKbkryNLzRW/jWWo962aqs4khUNu32jw7y5mIrCaSKGF52A/VrEIYoi3ecVUNHLs86ic1LYBO8BVA7BP6+USawc1x2xxTv/ImWJPk8odJVRVFAgMBAAECggEATSbvb1vcfD4nFyIRBCelIAOheyRfLiKjCEzdqPxCy3a8Klcmf9UEfnWa/kOJr3/r09WDSBQPBWm5vr0B/1/UwtpV6OvyNEVfXIfaYWmQnm4sji46nox8TfEugfqZKqAtL+/7dh5ZHlJSRkPfBlQxvIMrICvKUmVBBQtciUxBRCMbER3p0ARgVFmUyV/L+oqzG2uEINA/FJ1bN7+LyZyhjoRWKrhTkgRk9ZijN3vVx0onnhQbCEp+KMNH1M/a5N01BSMAFcO/pJLNePlDpr9boRIqP+H8R12Z0Q1FpMb6gIIEaiFH/Izc/tW1qBmSY+UEXICX9E5tFZ3EP45LMyu+cQKBgQDpw8jTdm8eRVgEHejzH16T+cSk3vIuYIQahyIKmwsIY9W4kl/ZxddZm4ELPy3EyNGrrfKBayaCDqb0S7deR3SyV2EE10R5dvjpCbR6WbmwU7P1hmWK1r83rtL8OaioD7iPNtO6COR4RklsPQ1ju2qjh6iuww+sWoPoFiPLaFmDMwKBgQDHNfWrrr2QUVTdFFcsF+USxiSlbiWoOPZ5kGLf5ngyLofrfwQUcuLv1NRCzuG5YKXnw/IovxJhg8iYkx9eSqKxx7CAHMhnEk+96GC783DFHLr32HcyEaVaxWruOr8f0TyZr9RSNhX84Duu8iFrq+F1Rm9kyf1Ku3wEbqtEwx2FpwKBgFiRAv+LOaJgZ2533pBubCjoUqaciWkkzCg41p7qCw4MtSaO3p3lpSegEQ4vy6lo5HeFGgIKlLZl0r9wCHNB42bGUE3DOYBDoR65vJU2dXHm5KG7X7Fwl1YDeJBzUAEMo8Iu5ZzwTY98IclOgEou8cIvXauI3+FPEfbWnI0uZeudAoGBAIESBxCrcQr2h0OOkERzXuphExFUYnWyIo/06vsiujHZUBtW+m+9WCgH/1ch78JKSnP10wpvchtYZTnl6OcQ+ISzWzpGa9nMVXo3+6ULP7+Ws9UtnV0Fbdt4GaYszfRxfxAcijaR8IEdT84z909p1/RCJJ5q+P/YpavBgJpWyOP1AoGALJH0ePRwIIgxeDiInQnv5nvb9EPDNv7/mGy/9zO+54K9qqW2+l/rjghJhqHVc8NWmkcTmxheGk41T4i5ZDifcfM1ghH9DIlSZbpXDyYJ3TuOgwfCirdQ/cPos6OFiYQ3Wpbc4DKHx8+0ib+FUPKChIDvblGUcZm5E9M8+pOtbEs=";
	
	public static final String WX_USER_INFO_TABLENAME = "wx_user_info";

	public static final String WX_USER_INFO_COLUMN_TAX_NUM = "tax_num";

}
