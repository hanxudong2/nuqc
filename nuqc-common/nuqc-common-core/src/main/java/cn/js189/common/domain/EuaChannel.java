package cn.js189.common.domain;

import lombok.Data;

import java.io.Serializable;

/**
 * 渠道权限信息
 */
@Data
public class EuaChannel implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	/** 渠道id */
	private String channelId;
	
	/** 渠道编号 */
	private String channelCode;
	
	/** 渠道key */
	private String channelKey;
	
	/** 状态 */
	private String status;

}
