package cn.js189.common.enums;

/**
 * 计费接口错误码
 */
public enum BSErrorEnum {


	BS_RESPONSE_EMPTY("bs_response_empty","计费接口返回空");


	private String errorCode;

	private String errorMsg;

	BSErrorEnum(String errorCode, String errorMsg) {
		this.errorCode = errorCode;
		this.errorMsg = errorMsg;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

}
