package cn.js189.common.util.exception;

import cn.js189.common.constants.SystemConst;
import cn.js189.common.domain.ContextHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.Locale;


/**
 * <Description> 异常处理类<br>
 * 
 * @author liuzg<br>
 * @version 1.0<br>
 * @taskId <br>
 * @CreateDate 2016年1月10日 <br>
 */
public class ExceptionUtil {

    /**
     * 日志对象
     */
    private static final Logger LOG = LoggerFactory.getLogger(ExceptionHandler.class);

    /**
     * 日志是否发送kafka
     */
    private static String sendKafkaFlag;

    /**
     * 系统名称
     */
    private static String sysName;

    /**
     * 静态模块
     */
    static {
        init();
    }

    /**
     * Description: 封装框架core提供的异常处理能力。传入系统运行异常，记录日志，不抛出异常 <br>
     * 
     * @author liuzg <br>
     * @taskId <br>
     * @param params <br>
     * @param t <br>
     * @CreateDate 2016年2月26日 下午12:25:13 <br>
     */
    public static void noThrowException(Throwable t, String... params) {
        try {
            String detail = getThrowableTrace(t);
            LOG.error(detail);
            String errorCode = SystemConst.SYS_ERROR_CODE;
            String errorDesc = getErrorMsg(errorCode, params);
//            if (SystemConst.NUMBER_STR_ONE.equals(sendKafkaFlag)) {
//                ExceptionLog.sendErrorLog(detail, errorCode, errorDesc, "", sysName);
//            }
        } catch (Exception e) {
            LOG.info("ExceptionUtil.noThrowException() error!!");
        }
    }

    /**
     * Description: : 封装框架core提供的异常处理能力。 传入自定义异常编码，抛出BaseAppException <br>
     * 
     * @author liuzg <br>
     * @taskId <br>
     * @param errorCode <br>
     * @param params <br>
     * @CreateDate 2016年2月18日 上午9:22:50 <br>
     */
    public static void throwBusiException(String errorCode, String... params) throws BaseAppException {
        String errorDesc = getErrorMsg(errorCode, params);
//        if (SystemConst.NUMBER_STR_ONE.equals(sendKafkaFlag)) {
//            ExceptionLog.sendErrorLog("", errorCode, errorDesc, "", sysName);
//        }
        throw new BaseAppException(errorCode, errorDesc, null, null);
    }

    /**
     * Description: 不知道谁加的方法，CheckStyle检查 加注释<br>
     * 
     * @author liuzg<br>
     * @taskId <br>
     * @param errorCode <br>
     * @param params <br>
     * @throws BaseAppException <br>
     */
    public static void throwBusiExceptionWithMsg(String errorCode, String params) throws BaseAppException {
        String errorDesc = getErrorMsg(errorCode, params);
//        if (SystemConst.NUMBER_STR_ONE.equals(sendKafkaFlag)) {
//            ExceptionLog.sendErrorLog("", errorCode, errorDesc, "", sysName);
//        }
        throw new BaseAppException(errorCode, errorDesc, null, null);
    }

    /**
     * Description: 自定义错误信息<br>
     * 
     * @author liutian<br>
     * @taskId <br>
     * @param errorCode <br>
     * @param params <br>
     * @throws BaseAppException <br>
     */
    public static void throwBusiExceptionMsg(String errorCode, String params) throws BaseAppException {
//        if (SystemConst.NUMBER_STR_ONE.equals(sendKafkaFlag)) {
//            ExceptionLog.sendErrorLog("", errorCode, params, "", sysName);
//        }
        throw new BaseAppException(errorCode, params, null, null);
    }
    
    /**
     * Description: 初始化信息<br>
     * 
     * @author liuzg<br>
     * @taskId <br>
     */
    private static void init() {
        ApplicationContext ctx = ContextHelper.getApplicationContext();
        String sendKafkaFlagStr = ctx.getMessage("sendKafkaFlag", null, SystemConst.NUMBER_STR_ZERO, Locale.CHINA);
        String sysNameStr = ctx.getMessage("sys_name", null, "", Locale.CHINA);
        sysName = sysNameStr;
        sendKafkaFlag = sendKafkaFlagStr;
    }

    /**
     * Description: 根据异常编码获取异常信息 <br>
     * 
     * @author liuzg <br>
     * @taskId <br>
     * @param errorCode <br>
     * @param params <br>
     * @return <br>
     * @CreateDate 2016年2月22日 下午1:55:17 <br>
     */
    public static String getErrorMsg(String errorCode, String... params) {
        ApplicationContext ctx = ContextHelper.getApplicationContext();
        String msg = ctx.getMessage(errorCode, params, SystemConst.SYS_ERROR_MSG, Locale.CHINA);
        return msg;
    }

    /**
     * Description: 获取异常堆栈<br>
     * 
     * @author liuzg<br>
     * @taskId <br>
     * @param t
     * @return <br>
     */
    private static String getThrowableTrace(Throwable t) {
        StringBuffer sb = new StringBuffer();
        if (t != null) {
            sb.append("\r\nCause by: ");
            sb.append(t.toString());
            for (int i = 0; i < t.getStackTrace().length; i++) {
                sb.append("\r\n\tat ");
                sb.append(t.getStackTrace()[i]);
            }
        }
        return sb.toString();
    }
}
