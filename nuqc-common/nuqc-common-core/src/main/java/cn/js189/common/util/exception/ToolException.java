/**    
 * @{#} ToolException.java Create on 2011-7-17 下午1:40:39    
 *    
 * Copyright (c)  by xiaogang.    
 */    
package cn.js189.common.util.exception;
   
/**    
 * @author <a href="h124436797@yahoo.com.cn">hrg</a>   
 * @version 1.0
 * Description:所有工具来继承的异常    
 */

public class ToolException extends RuntimeException {
	
	public ToolException(Throwable e){
		super(e);
	}
	
	public ToolException(String msg, Throwable e){
		super(msg,e);
	}
	
	public ToolException(String msg){
		super(msg);
	}

}
  