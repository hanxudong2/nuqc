package cn.js189.common.domain;

import lombok.Data;
import java.io.Serializable;

@Data
public class RespHead implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	/** 响应结果信息 */
	private String msg;
	
	/** 响应时间 */
	private String responseTime;
	
	/** 响应状态码 */
	private String status;
	
	/** 响应id */
	private String responseId;

	/**
	 * Description: <br>
	 *
	 * @author liuzg<br>
	 * @taskId <br>
	 * @param statusStr <br>
	 * @param message <br>
	 */
	public void setStatusAndMsg(String statusStr, String message) {
		this.status = statusStr;
		this.msg = message;
	}
	
}
