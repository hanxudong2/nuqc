package cn.js189.common.domain;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import java.io.Serializable;

/**
 * 响应结果封装
 */
@Data
public class RespInfo implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * 响应head
	 */
	private RespHead head;
	
	/**
	 *响应body
	 */
	private JSONObject body;
	
	/**
	 * 成功响应
	 * @param status 状态码
	 * @param msg 响应信息
	 * @return 响应对象
	 */
	public static RespInfo result(String status,String msg){
		RespInfo respInfo = new RespInfo();
		// 响应头部信息
		RespHead heads = new RespHead();
		heads.setResponseId(IdUtil.fastSimpleUUID());
		heads.setResponseTime(DateUtil.formatDateTime(DateUtil.date()));
		heads.setStatus(status);
		heads.setMsg(msg);
		respInfo.setHead(heads);
		
		respInfo.setBody(new JSONObject());
		return respInfo;
	}
	
}
