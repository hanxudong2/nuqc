package cn.js189.common.enums;

/**
 * 查询销售品范围的枚举类型
 * 
 * @author Mid
 */
public enum QryOfferInstScopeInfoEnum {
	// 销售品实例
	OFFER_INST("offerInst", 1),
	// 销售品产品实例关系
	OFFER_PROD_INST_REL("offerProdInstRel", 2),
	// 销售品实例属性
	OFFER_INST_ATTR("offerInstAttr", 3),
	// 销售品实例包含对象
	OFFER_OBJ_INST_REL("offerObjInstRel", 4),
	// 销售品实例关系
	OFFER_INST_REL("offerInstRel", 5),
	// 销售品发展人信息
	DEV_STAFF_INFO("devStaffInfo", 6);

	// 范围编码
	private String scopeCode;
	// 范围索引
	private int scopeIndex;

	private QryOfferInstScopeInfoEnum(String scopeCode, int scopeIndex) {
		this.scopeCode = scopeCode;
		this.scopeIndex = scopeIndex;
	}

	public String getScopeCode() {
		return scopeCode;
	}

	public int getScopeIndex() {
		return scopeIndex;
	}
}
