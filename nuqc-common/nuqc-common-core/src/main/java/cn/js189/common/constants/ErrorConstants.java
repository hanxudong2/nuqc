package cn.js189.common.constants;

/**
 * 异常日志常量
 */
public class ErrorConstants {

    private ErrorConstants(){}

    /** ctg-cache连接池获取链接失败 */
    public static final String REDIS_CONN_ERROR = "ctg-cache连接池获取链接失败";

    /** ctg-cache执行保存失败 */
    public static final String REDIS_SAVE_ERROR = "ctg-cache执行保存失败";

    /** ctg-cache执行删除失败 */
    public static final String REDIS_REMOVE_ERROR = "ctg-cache执行删除失败";
    /**
     * 获取微信access_token锁失败
     */
    public static final String WX_LOCK_ACCESS_TOKEN_FAIL = "lock_failed_100001";
    
    /**
     * 提交订单获取锁失败
     */
    public static final String ORDER_LOCK_FAIL = "lock_failed_100002";
    
    /**
     * 获取授权页ticket锁失败
     */
    public static final String WX_LOCK_TICKET_FAIL = "lock_failed_100003";
    
    /**
     * 自有服务获取微信access_token失败
     */
    public static final String WX_GET_ACCESS_TOKEN_FAIL = "wx_failed_200002";
    
    /**
     * 自有服务获取token所需的ticket失败
     */
    public static final String WX_GET_ACCESS_TOKEN_TICKT_FAL = "wx_failed_200001";
    
    /**
     * 获取微信自身的开票平台识别码失败
     */
    public static final String WX_GET_S_PAPPID_FAIL = "wx_failed_200003";
    
    /**
     * 微信插卡失败
     */
    public static final String WX_INVOICE_CARDINSERT_FALIL = "wx_faliled_200012";
    
    /**
     * 发票信息不完整 无法插卡
     */
    public static final String WX_INVOICE_INFO_EMPTY_ERROR = "wx_faliled_200016";
    
    /**
     * 获取微信一次授权页失败
     */
    public static final String WX_GET_BATCHAUTH_URL_FALIL = "wx_faliled_2000017";
    
    /**
     * 获取用户一次授权信息
     */
    public static final String WX_GET_BATCHAUTH_INFO_FALIL = "wx_faliled_2000018";
    
    /**
     * 获取授权页ticket失败
     */
    public static final String WX_GET_TICKET_FALIL = "wx_failed_200005";
    
    /**
     * 设置商户联系方式失败
     */
    public static final String WX_SET_CONTACT_FAIL = "wx_failed_200004";
    
    /**
     * 获取微信授权页失败
     */
    public static final String WX_GET_AUTH_URL_FALIL = "wx_faliled_200006";
    
    /**
     * 查询授权完成状态失败
     */
    public static final String WX_GET_TICKET_AUTH_FALIL = "wx_faliled_200007";
    
    /**
     * 微信创建卡券模板失败
     */
    public static final String WX_INVOICE_CREATECARD_FALIL = "wx_faliled_200010";
    
    /**
     * 发票pdf下载地址为空
     */
    public static final String WX_INVOICE_PDFURL_EMPTY = "wx_faliled_200015";
    
    /**
     * 本地pdf下载失败
     */
    public static final String WX_INVOICE_PDFDOWNLOAD_FALIL = "wx_faliled_200013";
    
    /**
     * 微信上传PDF失败
     */
    public static final String WX_INVOICE_UPLOADPDF_FALIL = "wx_faliled_200011";
    
    /**
     * 发票用户信息不完整
     */
    public static final String PARAM_EMPTY_USERTAIL = "user_error_300003";
    
    /**
     * 微信插卡参数错误
     */
    public static final String WX_INVOICE_CARDINSERT_PARAM_ERROR = "wx_faliled_200014";
    
    /**
     * 企信同步发票详情错误
     */
    public static final String QX_SYNC_INVOICE_ERROR = "qx_sync_invoice_400003";
    
    /**
     * 订单信息入库失败
     */
    public static final String ORDER_INSERT_ERROR = "invoice_error_300002";
    
    /**
     * id不能为空
     */
    public static final String DOWN_ID_NULL = "down_error_300011";
    
    /**
     * 入参信息不完整
     */
    public static final String INVOICE_PARAM_EMPTY = "param_empty_300013";
    
    /**
     *没有对应用户信息
     */
    public static final String USER_NULL_BYPARTYID = "user_error_300009";
    
    /**
     * url不能为空
     */
    public static final String DOWN_URL_NULL = "down_error_300012";
    
    /**
     * 发票信息不完整
     */
    public static final String PARAM_EMPTY_INVOICEDETAIL = "invoice_empty_300001";
    
    /**
     * 发票type错误
     */
    public static final String INVOICE_TYPE_ERROR = "invoice_error_300014";
    
    /**
     * 参数为空或者记录已存在
     */
    public static final String PARAM_EMPTY_EXIST_INVOICEFAIL = "param_empty_exist_300016";
    
    /**
     * 税号为空
     */
    public static final String INVOICE_TAX_NUM_EMPTY = "param_empty_300020";
    
    /**
     * 重复提交开票申请
     */
    public static final String SUBMIT_INVOICE_ERROR = "submit_invoice_error_300018";
    
    /**
     * 发票已存在
     */
    public static final String PARAM_EXISTS_INVOICEFAIL = "invoice_error_300005";
    
    /**
     * 发票开具失败
     */
    public static final String ISSUE_INVOICE_FAIL = "invoice_error_300004";
    
    /**
     * 公众号
     * 获取开票授权页参数错误
     */
    public static final String WX_GET_AUTH_URL_PARAM_ERROR = "wx_error_200008";
    
    /**
     * 发票信息不存在
     */
    public static final String INVOICE_NOT_EXISTS = "invoice_error_300006";
    
    /**
     * 发票金额不一致
     */
    public static final String INVOICE_MONEY_ERROR = "invoice_error_300007";
    
    /**
     * 公众号
     * 开票授权类型不支持
     */
    public static final String WX_GET_AUTH_TYPE_NOT_SUPPORT = "wx_error_200009";

}
