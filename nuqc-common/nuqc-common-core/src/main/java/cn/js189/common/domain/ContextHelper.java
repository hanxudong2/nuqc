package cn.js189.common.domain;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * 获取容器中的ApplicationContext
 * @author zhangai
 *
 */

public class ContextHelper implements ApplicationContextAware {
    /**
     * appCtx
     */
    private static ApplicationContext appCtx;
   
   /**
    * 
    * Description: 此方法可以把ApplicationContext对象inject到当前类中作为一个静态成员变量。<br> 
    *  
    * @author lenovo<br>
    * @taskId <br>
    * @param applicationContext ApplicationContext 对象. <br>
    */
    public void setApplicationContext(ApplicationContext applicationContext)  {
        appCtx = applicationContext;  
    } 
    
    /**
     * 
     * Description: 获取ApplicationContext<br> 
     *  
     * @author zhangai<br>
     * @taskId <br>
     * @return <br>
     */
    public static ApplicationContext getApplicationContext() {
        return appCtx;
    }
    
    /**
     *  
     * Description:这是一个便利的方法，帮助我们快速得到一个BEAN  <br> 
     *  
     * @author zhangai<br>
     * @taskId <br>
     * @param beanName beanName
     * @return <br>
     */
    public static Object getBean(String beanName) {
        return appCtx.getBean(beanName);  
    } 
}
