package cn.js189.common.util;


public class TransactionContext {
    private static final ThreadLocal<String> tranIdHolder = new ThreadLocal<>();

    public static void setTranId(String tranId) {
        tranIdHolder.set(tranId);
    }

    public static String getTranId() {
        return tranIdHolder.get();
    }

    public static void clear() {
        tranIdHolder.remove();
    }
}




