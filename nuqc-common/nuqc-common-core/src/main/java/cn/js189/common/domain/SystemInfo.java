package cn.js189.common.domain;

import lombok.Data;

@Data
public class SystemInfo {
	
	public static final int DEBUG = 1;
	
	public static final int INFO = 2;
	
	public static final int WARN = 3;
	
	public static final int ERROR = 4;
	
	public static final int FATAL = 5;
	
	public static final int SYS_ERROR_TYPE = 1;
	
	public static final int BUSI_ERROR_TYPE = 2;
	
	private int logLevel;
	
	private String systemName;
	
	private String moduleName;
	
	private int type;
	
}
