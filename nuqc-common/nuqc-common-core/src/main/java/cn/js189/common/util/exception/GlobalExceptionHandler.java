package cn.js189.common.util.exception;

import cn.js189.common.constants.Constants;
import cn.js189.common.domain.RespHead;
import cn.js189.common.domain.RespInfo;
import cn.js189.common.domain.Result;
import cn.js189.common.util.TransactionContext;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.Date;

@ControllerAdvice(annotations = RestController.class)
public class GlobalExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);
    @ExceptionHandler(GlobalException.class)
    @ResponseBody
    public Result handleGlobalException(GlobalException e) {
        recordLog(e);
        return Result.fail(e.getMessage());
    }

    @ExceptionHandler(Exception.class)
    @ResponseBody
    // Utils line 391
    public String handleException(Exception e) {
        recordLog(e);
        RespInfo response = new RespInfo();
        RespHead responseHead = new RespHead();
        responseHead.setResponseId(Constants.RSP + TransactionContext.getTranId());
        responseHead.setResponseTime(getCurrentTime());
        responseHead.setMsg("服务内部错误");
        responseHead.setStatus("99");
        response.setHead(responseHead);
        TransactionContext.clear();
        return response.toString();
    }

    @ExceptionHandler(BSException.class)
    @ResponseBody
    // Utils line 365
    public String handleBSException(BSException e) {
        recordLog(e);
        JSONObject response = new JSONObject();
        JSONObject head = new JSONObject();
        head.put("msg", e.getErrorMsg());
        head.put("status", e.getErrorCode());
        head.put("responseId", Constants.RSP + TransactionContext.getTranId());
        head.put("responseTime", getCurrentTime());
        response.put("head", head);
        TransactionContext.clear();
        return response.toString();
    }

    @ExceptionHandler(BaseAppException.class)
    @ResponseBody
    // Utils line 347
    public String handleBaseAppException(BaseAppException e) {
        recordLog(e);
        JSONObject response = new JSONObject();
        JSONObject head = new JSONObject();
        head.put("msg", "入参校验失败");
        head.put("status", "03");
        head.put("responseId", Constants.RSP + TransactionContext.getTranId());
        head.put("responseTime", getCurrentTime());
        response.put("head", head);
        TransactionContext.clear();
        return response.toString();
    }
    
    @ExceptionHandler(GlobalException.class)
    @ResponseBody
    // Utils line 347
    public String handleBaseAppException(GlobalException e) {
        recordLog(e);
        JSONObject response = new JSONObject();
        JSONObject head = new JSONObject();
        head.put("msg", e.getMessage());
        head.put("status", e.getCode());
        head.put("responseId", Constants.RSP + TransactionContext.getTranId());
        head.put("responseTime", getCurrentTime());
        response.put("head", head);
        TransactionContext.clear();
        return response.toString();
    }
    
    private String getCurrentTime(){
        Date time = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(time);
    }

    private void recordLog(Exception e) {
        StackTraceElement info = e.getStackTrace()[0];
        logger.error("{}---{}---{},异常信息：{}",info.getClassName(), info.getMethodName(), info.getLineNumber() , e.getMessage());
    }
}
