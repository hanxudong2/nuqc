/**************************************************************************************** 

 ****************************************************************************************/
package cn.js189.common.constants;

/**
 * <Description>常量 <br>
 *
 * @author zhangpzh<br>
 * @version 1.0<br>
 * @taskId <br>
 * @CreateDate 2015-10-15 <br>
 */

public class Constant {

    public static final String js_intf_qryCustomer = "js.intf.qryCustomer";
    public static final String js_intf_qryOfferInst = "js.intf.qryOfferInst";
    public static final String js_intf_ProdInst = "js.intf.ProdInst";
    public static final String js_intf_qryCustomerOrder = "js.intf.qryCustomerOrder";
    public static final String JS_INTF_QRYPUKCODE = "js.intf.qryPUKCode";
    public static final String JS_INTF_QRYCUSTORDERANDPDFFLAG = "js.intf.qryCustOrderAndPdfFlag";
    public static final String js_intf_qryPdfByOlNbr = "js.intf.qryAppointAgreement";
    public static final String JS_INTF_QRYACCOUNT = "js.intf.qryAccount";
    public static final String JS_INTF_QRYREMINDPRODINST = "js.intf.qryRemindProdInst";
    public static final String JS_INTF_CPC_PPM_QRYOPTIONALPROD = "js.intf.cpc_ppm_qryOptionalProd";
    public static final String js_intf_qryProdInstRel = "js.intf.qryProdInstRel";
    public static final String js_intf_qryProdId = "js.intf.qryProdId";
    public static final String JS_INTF_CHECKGOVECUSTOMER = "js.intf.checkGovCustomer";
    public static final String SO_QUERYCRTFCTNMBRRLTN = "so.queryCrtfctNmbrRltn";
    public static final String DFS_GET_DATA = "dfsGetData";//获取无纸化pdf
    public static final String JS_INTF_CHECKREALNAME = "js.intf.checkRealName";
    public static final String JS_INTF_QRYMEMBERINFO = "js.intf.qryMemberInfo";
    // 查询员工信息
    public static final String JS_INTF_QRYSTAFF = "js.intf.qryStaff";
    // 查询渠道信息
    public static final String JS_INTF_QRYCHANNEL = "js.intf.qryChannel";
    // 校验证件敏感信息
    public static final String JS_INTF_CHECKPARTCERT = "js.intf.checkPartyCert";
    // 查询特殊名单列表-企信灰名单
    public static final String JS_INTF_QRYSPECIALLIST = "js.intf.qrySpecialList";
    //违约金试算
    public static final String JS_INTF_RPEHANDLEHTTPREQ = "js.intf.rpeHandleHttpReq";
    //订单算费（费用明细）
    public static final String JS_INTF_CALCHARGEDETAILINFO = "js.intf.calChargeDetailInfo";

    //查询客户联系方式
    public static final String JS_INTF_QRYCONTACTINFOBYCERTNUM = "js.intf.qryContactInfoByCertNum";
    
    //查询账户合并关系
    public static final String JS_INTF_QRYACCTRELBYPRODINSTID = "js.intf.qryAcctRelByProdInstId";
    
    //查询60天内无理由退货纪录
    public static final String JS_INTF_QRYREDUTIONINFORECORDS = "js.intf.qryReductionInfoRecords";

    //销售品扩展属性查询
    public static final String JS_INTF_QRYOFFEREXTATTRS = "js.intf.qryOfferExtAttrs";
    
    //根据号码查询订单列表
    public static final String JS_INTF_QRYORDERBYACCNUMLIST = "js.intf.qryOrderByAccNumList";

    // 查询我的套餐
    public static final String JS_INTF_QRYMYPRODUCTOFFER = "js.intf.qryMyProductOffer";


//    public static final String[] METHODS = new String[]{js_intf_qryCustomer, js_intf_qryOfferInst, js_intf_ProdInst, js_intf_qryCustomerOrder,
//            js_intf_qryPUKCode, js_intf_qryCustOrderAndPdfFlag, js_intf_qryPdfByOlNbr, js_intf_qryAccount, js_intf_qryRemindProdInst,
//            cpc_ppm_qryOptionalProd, js_intf_qryProdInstRel, js_intf_qryProdId, js_intf_checkGovCustomer, so_queryCrtfctNmbrRltn,
//            dfsGetData, js_intf_checkRealName, js_intf_qryMemberInfo
//    };
//    public static final String[] AREA_CODES = new String[]{"025", "0510", "0514", "0517", "0523", "0511", "0512", "0513", "0515", "0516", "0518", "0519", "0527"};


    public static final String ZT_SECRET = "123456";
    /**
     * XML文档风格<br>
     * 0:节点属性值方式
     */
    public static final String XML_ATTIRBUTE = "0";

    /**
     * XML文档风格<br>
     * 1:节点元素值方式
     */
    public static final String XML_NODE = "1";

    /**
     * 点
     */
    public static final String DOT = ".";

    /**
     * date 格式字符串
     */
    public static final String STRING_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    /**
     * 斜杠
     */
    public static final String SLASH = "/";

    /**
     * XML标志
     */
    public static final String XML_FLAG = "<?xml";

    /**
     * XML头
     */
    public static final String XML_HEADER = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";

    /**
     * XML根节点
     */
    public static final String XML_ROOT = "root";

    /**
     * 默认JSONList
     */
    public static final String DEFAULT_ALIST = "defaultAList";

    /**
     * 默认JSONString
     */
    public static final String DEFAULT_STRING = "defaultJsonString";

    /**
     * 结果
     */
    public static final String RESULT = "result";

    /**
     * 结果描述
     */
    public static final String RESULT_MSG = "resultMsg";

    /**
     * 结果码
     */
    public static final String RESULT_CODE = "resultCode";

    /**
     * 返回编码
     */
    public static final String RETURNCODE = "returnCode";

    /**
     * 返回信息
     */
    public static final String RETURN_INFO = "returnInfo";

    /**
     * 地区编码
     */
    public static final String AREACODE = "areaCode";

    // 天翼24小时流量包
    public static final String SERVICE_24HOUR_NAME = "天翼24小时流量包5元包1G省内流量";

    public static final String SERVICE_24HOUR_NAME2 = "天翼24小时流量包10元包1G省内流量";

    public static final String SERVICE_4HOUR_NAME = "天翼4小时流量包5元包1G省内流量";

    //爱电视定向流量包2小时不限量
    public static final String SERVICE_2HOUR_NAME = "爱电视定向流量包2小时不限量";
    //天翼7天流量包30元包1G全国流量
    public static final String SERVICE_7DAY_NAME = "天翼7天流量包30元包1G全国流量";
    //天翼7天流量包30元包10G省内流量
    public static final String SERVICE_7DAY_NAME3 = "天翼7天流量包30元包10G省内流量";
    //天翼7天流量包50元包10G省内流量
    public static final String SERVICE_7DAY_NAME4 = "天翼7天流量包50元包10G省内流量";
    //天翼7天流量包20元包1G省内流量
    public static final String SERVICE_7DAY_NAME2 = "天翼7天流量包20元包1G省内流量";
    //天翼3天流量包15元包1G省内流量
    public static final String SERVICE_3DAY_NAME = "天翼3天流量包15元包1G省内流量";
    //天翼3天流量包20元包5G省内流量
    public static final String SERVICE_3DAY_NAME1 = "天翼3天流量包20元包5G省内流量";


    //“抢流量、用天翼”赠送7天500M省内流量
    public static final String TIANYILIULIANG_7DAY500M = "“抢流量、用天翼”赠送7天500M省内流量";
    //“抢流量、用天翼”赠送7天300M省内流量
    public static final String TIANYILIULIANG_7DAY300M = "“抢流量、用天翼”赠送7天300M省内流量";
    //“抢流量、用天翼”赠送7天100M省内流量
    public static final String TIANYILIULIANG_7DAY100M = "“抢流量、用天翼”赠送7天100M省内流量";
    //“抢流量、用天翼”赠送7天1G省内流量
    public static final String TIANYILIULIANG_7DAY1G = "“抢流量、用天翼”赠送7天1G省内流量";
//    public static final  Set<String> remoteRetTypes = new HashSet<>();
//    static{
//        remoteRetTypes.add("2262");
//        remoteRetTypes.add("3111");
//        remoteRetTypes.add("3304");
//    }


//    public static final String[] ratableResourceArray = new String[]{"131100"};
//    public static final Set<String> ratableResourceId = new HashSet<>(Arrays.asList(ratableResourceArray));
    // static{
    //     ratableResourceId.add("131100");
    // };
    public static final String INVALID_PARA = "10001";
    public static final String PARA_ERROR = "对不起，请检查输入的信息是否有误或咨询10000号";

    /**
     * <Description> <br>
     *
     * @author 'lixu'<br>
     * @version 1.0<br>
     * @taskId <br>
     * @CreateDate 2015年10月26日 <br>
     */
    public enum magicStringEnum {
        /**
         * 魔鬼数字格式化类型
         */
        STRING_3("3"), STRING_4("4"), STRING_15("15"), STRING_20("20"), STRING_26("26"), STRING_33("33"), STRING_39("39"), STRING_48("48"), STRING_60("60"), STRING_63("63"), STRING_69("69"), STRING_79("79"), STRING_84("84")
        /**
         * 魔鬼数字格式化类型
         */
        , STRING_1000("1000"), STRING_30("30");
        /**
         * value
         */
        private final String value;

        /**
         * 类型
         *
         * @param type
         */
        magicStringEnum(final String type) {
            this.value = type;
        }

        /**
         * Description: <br>
         *
         * @return <br>
         * @author 'lixu'<br>
         * @taskId <br>
         */
        public String getValue() {
            return this.value;
        }
    }

    /**
     * <Description> <br>
     *
     * @author 'lixu'<br>
     * @version 1.0<br>
     * @taskId <br>
     * @CreateDate 2015年10月26日 <br>
     */
    public enum magicTypeEnum {
        /**
         * 魔鬼数字格式化类型
         */
        NUMBER_3(3), NUMBER_4(4), NUMBER_15(15), NUMBER_20(20), NUMBER_26(26), NUMBER_30(30), NUMBER_33(33), NUMBER_39(39), NUMBER_48(48), NUMBER_60(60), NUMBER_63(63),
        /**
         * 魔鬼数字格式化类型
         */
        NUMBER_69(69), NUMBER_79(79), NUMBER_84(84), NUMBER_200(200), NUMBER_5000(5000), NUMBER_1000(1000),
        ;
        /**
         * value
         */
        private final int value;

        /**
         * 类型
         *
         * @param type
         */
        magicTypeEnum(final int type) {
            this.value = type;
        }

        /**
         * Description: <br>
         *
         * @return <br>
         * @author 'lixu'<br>
         * @taskId <br>
         */
        public int getValue() {
            return this.value;
        }

    }
}
