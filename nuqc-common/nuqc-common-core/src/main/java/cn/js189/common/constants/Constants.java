package cn.js189.common.constants;

import cn.js189.common.domain.LogBase;
import com.alibaba.fastjson.JSONObject;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 公共常量
 */
public class Constants {
    
    private Constants(){
        /* empty */
    }
    
    public static final String CONTENT_TYPE = "Content-Type";
    
    public static final String DES_STR = "DES";
    
    public static final String RSP = "RSP";
    public static final String mobile = "mobile";
    public static final String UNKNOWN = "unknown";
    public static final String IP_STR = "127.0.0.1";
    
    
    
    
    
    /**
     * 简单查询模式
     */
    public static final String EASY_QRY_MOD = "easyQryMod";
    /**
     * 发票实例编码
     */
    public static final String INVOICE_ID = "invoiceId";
    /**
     * COUNT
     */
    public static final String COUNT = "count";
    /**
     * 创建时间
     */
    public static final String CREATE_TIME = "createTime";
    
    /**
     * 渠道id
     */
    public static final String CHANNEL_ID = "channelId";

    /**
     * UTF-8 字符集
     */
    public static final String UTF8 = "UTF-8";

    /**
     * GBK 字符集
     */
    public static final String GBK = "GBK";

    /**
     * 成功标记
     */
    public static final String SUCCESS = "00";
    
    /**
     * 状态
     */
    public static final String STATUS = "status";
    
    /**
     * 查询模式
     */
    public static final String QUERY_MODE = "queryMode";

    /**
     * 失败标记
     */
    public static final String FAIL = "500";
    
    /**
     * 删除密码标识
     */
    public static final String DEL_PWD_FLAG= "delPWDFlag";

    /** 存储日志 */
    public static Map<String, Map<String,LogBase>> cLog = new LinkedHashMap<>(1);
    
    /** body参数解密键值 */
    public static final String DES_BODY_KEY = "DES_BODY";
    
    /** 接口次数统计key */
    public static final String LOG_INTER_COUNT_KEY = "LOG_INTER_COUNT_KEY_";
    
    /** 请求签名字符串名称 */
    public static final String AUTH_STR_NAME = "sign";
    
    /** 请求签名字符串名称 */
    public static final String UQC_ENC_FLAG = "uqcEncFlag";

    /** 主方法 */
    public static final String MASTER = "MASTER";

    /** 子方法 */
    public static final String SLAVE = "SLAVE";
    
    /** 渠道key存储后缀标识 */
    public static final String EUA_KEY_SUFFIX = "_key";
    
    /** 渠道权限字符-0000 */
    public static final String EUA_AUTH_0000 = "0000";
    
    /** 渠道权限字符-0001 */
    public static final String EUA_AUTH_0001 = "0001";
    
    /** 渠道权限字符-0002 */
    public static final String EUA_AUTH_0002 = "0002";
    
    /** 渠道权限字符-0003 */
    public static final String EUA_AUTH_0003 = "0003";
    
    /** 参数错误 */
    public static final String PARAM_ERROR = "uqc_common_10000";
    
    /*** 接口统计暂存数据 */
    public static final Map<String, Map<String,JSONObject>> INTER_COUNT_MAP = new HashMap<>(1);

    public static final String X_CTG_REQUEST_ID = "X-CTG-Request-ID";

    /**
     * 企信签名key
     */
    public static final String EOP_SIGN = "EOP_SIGN";

    /**
     * 未竣工标识
     */
    public static final String NO_FINISH = "N";

    /**
     * 竣工标识
     */
    public static final String YES_FINISH = "Y";



    /**
     * 错误类型，返回为空
     */
    public static final String ERR_NULL = "返回数据为空，无法解析";

    /**
     * 错误类型，返回对象为空
     */
    public static final String ERR_OBJ_NULL = "查询结果对象为空";

    /**
     * 错误类型，内部接口为空
     */
    public static final String ERR_INF_NULL = "内部接口返回空";

    /**
     * 查询产品返回空
     */
    public static final String ERR_QRY_PRODINST_NULL= "产品返回数据为空，无法解析";

    /**
     * 查询产品限流
     */
    public static final String QRY_PRODINSTANT_LIMIT= "查询产品限流";

    /**
     * 数字-1
     */
    public static final String NUM_MINUS_ONE = "-1";

    /**
     * 数字1
     */
    public static final String NUM_ONE = "1";
    /**
     * 数字200
     */
    public static final String NUM_TWO_HUNDRED = "200";

    /**
     * 数字0
     */
    public static final String NUM_ZERO = "0";

    /**
     * 数字-2
     */
    public static final String NUM_MINUS_TWO = "-2";

    /**
     * 数字2
     */
    public static final String NUM_TWO = "2";

    /**
     * 数字-3
     */
    public static final String NUM_MINUS_THREE = "-3";
    /**
     * 数字-4
     */
    public static final String NUM_MINUS_FOUR = "-4";
    /**
     * 数字-5
     */
    public static final String NUM_MINUS_FIVE = "-5";

    /**
     * 数字3
     */
    public static final String NUM_THREE = "3";
    /**
     * 数字4
     */
    public static final String NUM_FOUR = "4";
    /**
     * 数字5
     */
    public static final String NUM_FIVE = "5";

    /**
     * 数字-6
     */
    public static final String NUM_MINUS_SIX = "-6";
    /**
     * 数字-7
     */
    public static final String NUM_MINUS_SEVEN = "-7";
    /**
     * 数字-8
     */
    public static final String NUM_MINUS_EIGHT = "-8";
    /**
     * 数字-12
     */
    public static final String NUM_MINUS_TWELVE = "-12";

    /**
     * 独立订购标识
     */
    public static final String NO_INDEPENDENT = "1";

    /**
     * 非独立订购标识
     */
    public static final String YES_INDEPENDENT = "0";

    
    public static final String UQCENCFLAG ="uqcEncFlag";
    
    public static final String ACC_NBR = "accNbr";
    
    public static final java.lang.String UBC = "UBC";
    
    /**
     * 活动编码
     */
    public static final String ACTIVITY_ID = "activityId";
    /**
     * 本地网编码
     */
    public static final String LAN_TN_ID = "latnId";
    
    /** 客运增加的单宽查询联系人和反馈联系人接口的key */
    public static final String DK_KEY="1KF9y196mjks9012";
    
    /**
     * 返回结果
     */
    public static final String RESULT_OBJECT = "resultObject";
    
    
    /**
     * 状态
     */
    public static final String STATUS_CD = "statusCd";
    /**
     * 订单号
     */
    public static final String ORDER_ID = "orderId";
    /**
     * 页码数量
     */
    public static final String PAGE_SIZE = "pageSize";
    /** 查询归属地 */
    public static final String NEW_AREA_CODE = "newAreaCode";
    /** 账单查询 */
    public static final String NEW_BILL_REQ= "NewBillReq";
    /** 欠费查询 */
    public static final String NEW_OTH_GET_OWE = "NewOthGetOwe";
    /** 账户查询 */
    public static final String NEW_OTH_GET_ACCT = "NewOthGetAcct";
    /** 手机使用量查询 */
    public static final String POCKET_SUM_QR = "PocketSumQr";
    
    /**
     * 应用编码
     */
    public static final String APP_ID = "appId";
    
    /**
     *来源
     */
    public static final String SOURCE = "source";
    
    
    /**
     *开票编码
     */
    public static final String OPEN_ID = "openId";
    
    public static final String DESCRIPTION = "description";
}
