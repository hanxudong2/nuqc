package cn.js189.common.domain;

import lombok.Data;

import java.io.Serializable;

/**
 * 请求日志子节点
 */
@Data
public class SysLogNodes implements Serializable {

    private static final long serialVersionUID = 1L;

    /** 生产or测试*/
    private String isTest;

    /** 方法名称 */
    private String method;

    /** 接口编码 */
    private String name;

    /** 接口提供方 */
    private String owner;

    /** 参数 */
    private String param;

    /** 请求时间 */
    private String requestTime;

    /** 响应时间*/
    private String responseTime;

    /** 结果 */
    private String result;

    /** 接口地址 */
    private String url;

    /** 接口执行时长*/
    private String useTime;


}
