package cn.js189.common.enums;


/**
 * @author : yangjian
 * @date : 2023/1/11
 */
public enum BSSConstants {

    JS_INTF_PROD_INST("查询产品实例","js.intf.qryProdInst","https://jseop.telecomjs.com/eop/crmservices/js.intf.qryProdInst?appkey=js_dzqd");

    private String name;
    private String method;
    private String url;

    BSSConstants(String name, String method, String url){
        this.name = name;
        this.method = method;
        this.url = url;
    }
    public String getName() {
        return name;
    }

    public String getMethod() {
        return method;
    }

    public String getUrl() {
        return url;
    }

    public String getInParam() {
        return this.name + "入参：";
    }

    public String getOutParam() {
        return this.name + "出参：";
    }

    public String getResultObjectMsg(){
        return this.name + "resultObject节点：";
    }

    public String getLimitMsg(){
        return this.name + "限流";
    }

    public String getResultNullMsg(){
        return this.name + "返回数据为空，无法解析";
    }

    public String getSystemError(){
        return this.name + "服务器内部错误";
    }


}
