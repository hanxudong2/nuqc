package cn.js189.common.constants;

public class CouponUrlConstant {
	
	private CouponUrlConstant(){}


//	private static final String BASE_PATH = "https://jseop.telecomjs.com/eop/crmservices";

	/**
	 * 集团接口-翼券平台优惠券基础接口地址
	 */
	//private static final String E_COUPON_BASE_URL = "https://partners.189.cn:31038/osa/interface";

	/**
	 * 集团接口-翼券平台优惠券基础接口地址-测试地址
	 */
	public static final String E_COUPON_BASE_URL_TEST = "http://10.251.27.227:31038/osa/interface";

	/**
	 * 翼券平台由于生成sign签名的url
	 */
	public static final String E_COUPON_GENERATOR_SIGN_URL = "/osa/interface";




	/**
	 * 根据客户查询业务体验券
	 */
	public static final String QUERY_BUS_EXP_COUPON = "https://jseop.telecomjs.com/eop/crmservices/js.intf.qryBusinessExperienceCoupons?appkey=js_dzqd";

	/**
	 * 根据适用系统编码查询可用券池
	 */
	public static final String QUERY_COUPON_POOLS = "https://jseop.telecomjs.com/eop/crmservices/js.intf.qryCouponPools?appkey=js_dzqd";

	/**
	 * 业务体验券绑定客户接口
	 */
	public static final String BIND_BUS_EXP_COUPON = "https://jseop.telecomjs.com/eop/crmservices/js.intf.bindBusinessExperienceCoupon?appkey=js_dzqd";



	/**
	 * 业务体验券绑定对象接口
	 */
	public static final String BIND_FREE_BUS_EXP_COUPON = "https://jseop.telecomjs.com/eop/crmservices/js.intf.bindFreeBusExpCoupon?appkey=js_dzqd";

	/**
	 * 根据绑定对象查询券信息接口
	 */
	public static final String QUERY_BIND_BUS_EXP_COUPON = "https://jseop.telecomjs.com/eop/crmservices/js.intf.qryBindBusExpCoupon?appkey=js_dzqd";

	/**
	 * 查询可用优惠券列表接口
	 */
	//public static final String QUERY_E_COUPON_RECORD = E_COUPON_BASE_URL_TEST+"/api/ecoupon/queryCoupon";

	/**
	 * 查询优惠券详情接口
	 */
	//public static final String QUERY_E_COUPON_DETAIL = E_COUPON_BASE_URL_TEST+"/api/ecoupon/queryCouponDetail";

	/**
	 * 我的卡包查询接口
	 */
	//public static final String QUERY_MY_E_COUPON_PACKAGE = E_COUPON_BASE_URL_TEST+"/api/ecoupon/queryMyCouponPackage";

	/**
	 * 集团翼券平台文件同步接口
	 */
	public static final String GROUP_E_COUPON_SYNC_URL = "http://10.251.27.221:31090/api/ecoupon/couponExt/couponExtUpload";

}
