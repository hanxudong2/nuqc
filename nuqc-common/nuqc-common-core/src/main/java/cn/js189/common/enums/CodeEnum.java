/**    
 * @{#} CodeEnum.java Create on 2011-8-26 上午11:28:40    
 *    
 * Copyright (c) 2010 by 创发科技.    
 */    
   package cn.js189.common.enums;
/**    
 * @author <a href="h124436797@yahoo.com.cn">hrg</a>   
 * @version 1.0 
 * Description:参数结果编码   
 */

public enum CodeEnum {
	/**
	 * 表示成功  
	 */
	CT_00000("success"),//
	/**
	 * 非空值
	 */
	CT_11111("{} is blank !"), //
	/**
	 * 加密传不对
	 */
	CT_22222("{} is not authorization!"), 
	/**
	 * 该ip不能访问
	 */
	CT_33333("{} is not allowed's ip!"),
	
	//计费错误码
	CT_50001("{} tuxedo wsnaddr connection refuesed!"),//tuxedo地址连接拒绝
	CT_50002("{} assign sendbuff buffer cache error!"),//分配发送缓冲区错误
	CT_50003("{} assign recvbuff buffer cache error!"), //分配接受缓冲区错误
	CT_50004("{} tuxedo call service error!"), //tuxedo 调用服务错误
	CT_50005("{} tuxedo wsnaddr  time out !"),//连接超时
	CT_50006("{} tuxedo call service return null!"),//tuxedo服务调用返回值为空
	CT_50007("{} tuxedo invoke again  return value is max RECEIVEBUFFSIZE "),//重新调用一次后返回值大余RECEIVEBUFFSIZE 
	CT_50008("{} tuxedo invoke after return value is max  MAXBUFFSIZE"),//调用后返回值大于最大缓冲值
	

	CT_60001("{} 500错误---对方服务挂死，无法正常访问对方服务！"),
	CT_60002("{} 超时错误---读取响应数据超时！"),
	CT_60003("{} 网络连接错误---连接被拒绝！"),
	CT_60004("{} 网络连接错误---连接超时！"),
	CT_60005("{} 其它未知异常错误!"),
	CT_77777("{} is xml parse errir "),//xml解析错误
	
	CT_404("{}  is xml parse error"),//找不到路径
	
	

	//webservice 错误码
	CT_66666("url:{} return value is null"),//返回值为空
    CT_99999("system other error!"),//其它错误
	
	CT_99998("选号接口异常"),
	
	
	//新定义的错误码
	T0000("查询成功！"),
	T0001("接口超时！"),
	S0001("系统异常，请重试！"),
	B0001("接口入参错误，请检查！");
	
	private String msg;
	
	private CodeEnum(String msg){
		this.msg=msg;
	}
	
	public String getMsg(){
		return this.msg;
	}
	
	protected void setMsg(String msg){
		this.msg=msg;
	}

	
}
  
