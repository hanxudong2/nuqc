package cn.js189.common.util;

import cn.js189.common.util.exception.ToolException;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author 黄柔刚
 * @time 2010-3-17-下午10:09:18
 */
public class MainUtils {
	
	private MainUtils() {
	}
	
	public static final String DEFAULT_DATE_PATTERN = "yyyy-MM-dd'T'HH:mm:ss";
	
	/**
	 * Byte.
	 */
	public static Byte getByte(String value, Byte defaultValue) {
		try {
			return Byte.valueOf(value);
		} catch (Exception ex) {
			return defaultValue;
		}
	}
	
	public static Byte getByte(String value) {
		return getByte(value, null);
	}
	
	/**
	 * Short.
	 */
	public static Short getShort(String value, Short defaultValue) {
		try {
			return Short.valueOf(value);
		} catch (Exception ex) {
			return defaultValue;
		}
	}
	
	public static Short getShort(String value) {
		return getShort(value, null);
	}
	
	/**
	 * Integer.
	 */
	public static Integer getInt(String value, Integer defaultValue) {
		try {
			return Integer.valueOf(value);
		} catch (Exception ex) {
			return defaultValue;
		}
	}
	
	public static Integer getInt(String value) {
		return getInt(value, null);
	}
	
	/**
	 * Long.
	 */
	public static Long getLong(String value, Long defaultValue) {
		try {
			return Long.valueOf(value);
		} catch (Exception ex) {
			return defaultValue;
		}
	}
	
	public static Long getLong(String value) {
		return getLong(value, null);
	}
	
	public static long[] getLongArray(JSONArray array, String name) {
		try {
			long[] result = new long[0];
			if (array != null) {
				for (Object o : array) {
					JSONObject obj = (JSONObject) o;
					long value = obj.getLong(name);
					result = ArrayUtils.add(result, value);
					
				}
			}
			return result;
		} catch (Exception e) {
			throw new ToolException(e);
		}
	}
	
	
	public static long[] getLongArray(List<JSONObject> list, String name) {
		try {
			long[] result = new long[0];
			if (list != null) {
				for (int i = 0, len = list.size(); i < len; i++) {
					JSONObject obj = list.get(i);
					long value = obj.getLong(name);
					result = ArrayUtils.add(result, value);
					
				}
			}
			return result;
		} catch (Exception e) {
			throw new ToolException(e);
		}
	}
	
	
	public static String[] getStringArray(List<JSONObject> list, String name) {
		try {
			String[] result = new String[0];
			if (list != null) {
				for (int i = 0, len = list.size(); i < len; i++) {
					JSONObject obj = list.get(i);
					String value = obj.getString(name);
					result = (String[]) ArrayUtils.add(result, value);
					
				}
			}
			return result;
		} catch (Exception e) {
			throw new ToolException(e);
		}
	}
	
	public static String[] getStringArray(JSONArray array, String name) {
		try {
			String[] result = new String[0];
			if (array != null) {
				for (int i = 0, len = array.size(); i < len; i++) {
					JSONObject obj = (JSONObject) array.get(i);
					String value = obj.getString(name);
					result = (String[]) ArrayUtils.add(result, value);
					
				}
			}
			return result;
		} catch (Exception e) {
			throw new ToolException(e);
		}
	}
	
	
	/**
	 * Float.
	 */
	public static Float getFloat(String value, Float defaultValue) {
		try {
			return Float.valueOf(value);
		} catch (Exception ex) {
			return defaultValue;
		}
	}
	
	public static Float getFloat(String value) {
		return getFloat(value, null);
	}
	
	/**
	 * Double.
	 */
	public static Double getDouble(String value, Double defaultValue) {
		try {
			return Double.valueOf(value);
		} catch (Exception ex) {
			return defaultValue;
		}
	}
	
	public static Double getDouble(String value) {
		return getDouble(value, null);
	}
	
	/**
	 * Character.
	 */
	public static Character getChar(String value, Character defaultValue) {
		try {
			return value.charAt(0);
		} catch (Exception ex) {
			return defaultValue;
		}
	}
	
	public static Character getChar(String value) {
		return getChar(value, null);
	}
	
	/**
	 * Boolean.
	 */
	public static Boolean getBoolean(String value, Boolean defaultValue) {
		try {
			return Boolean.valueOf(value);
		} catch (Exception ex) {
			return defaultValue;
		}
	}
	
	public static Boolean getBoolean(String value) {
		return getBoolean(value, Boolean.FALSE);
	}
	
	
	/**
	 * Description:如果值为空，则转换为空字符串
	 *
	 * @param value
	 * @return
	 */
	public static String getNullEmpty(String value) {
		if (value == null || "null".equals(value)) {
			value = "";
		}
		if ("{}".equals(value)) {
			value = "";
		}
		return value.trim();
	}
	
	/**
	 * Description:计费入参转换
	 *
	 * @param value
	 * @return
	 */
	public static String getNullEmptyForReq(String value) {
		if (value == null || "null".equals(value) || "".equals(value.trim()) || "undefined".equals(value) || " ".equals(value)) {
			value = "-1";
		}
		return value.trim();
	}
	
	public static long[] getLongArray(String[] str) {
		int len = str.length;
		long[] result = new long[len];
		for (int i = 0; i < len; i++) {
			result[i] = getLong(str[i]);
		}
		return result;
	}
	
	
	public static String getString(String name, HttpServletRequest req) {
		return req.getParameter(name) == null ? "" : req
				.getParameter(name);
	}
	
	
}
