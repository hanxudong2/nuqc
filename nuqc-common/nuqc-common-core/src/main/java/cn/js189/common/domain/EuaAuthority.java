package cn.js189.common.domain;

import lombok.Data;
import java.io.Serializable;

/**
 * 请求校验
 */

@Data
public class EuaAuthority implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	/** 接口编号 */
	private String actionCode;
	
	/** 渠道编号 */
	private String channelId;
	
	/** 区域编号 */
	private String areaCode;
	
}
