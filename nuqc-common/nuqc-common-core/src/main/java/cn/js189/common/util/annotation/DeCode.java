package cn.js189.common.util.annotation;

import cn.js189.common.domain.ReqInfo;

import java.lang.annotation.*;

/**
 * body参数解密注解
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface DeCode {
	
	/** 参数类型 */
	Class<?> cls() default ReqInfo.class;
	
	/** 加密的字段 */
	String field() default "body";

}
