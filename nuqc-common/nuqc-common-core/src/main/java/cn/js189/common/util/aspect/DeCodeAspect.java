package cn.js189.common.util.aspect;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.text.CharSequenceUtil;
import cn.js189.common.constants.Constants;
import cn.js189.common.domain.ReqInfo;
import cn.js189.common.util.ServletUtils;
import cn.js189.common.util.annotation.DeCode;
import cn.js189.common.util.sign.DesUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;


/**
 * 参数解密切面处理
 */
@Slf4j
@Aspect
@Component
public class DeCodeAspect {
	
	/**
	 * 环绕通知 进入方法前执行
	 *
	 * @param point  织入点
	 * @param deCode body参数解密注解标识
	 * @return 结果
	 * @throws Throwable 异常
	 */
	@Around("@annotation(deCode)")
	public Object around(ProceedingJoinPoint point, DeCode deCode) throws Throwable {
		try{
			// 参数类型
			Object[] args = point.getArgs();
			for (int i = 0; i < args.length; i++) {
				JSONObject bean = JSON.parseObject(String.valueOf(args[i]));
				args[i] = DesUtils.desBody(bean);
			}
			return point.proceed(args);
		}catch (Exception e){
			log.error(e.getMessage(),e);
			return point.proceed();
		}
	}
	
}
