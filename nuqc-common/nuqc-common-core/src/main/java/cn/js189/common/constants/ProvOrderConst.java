package cn.js189.common.constants;

public class ProvOrderConst {
    private ProvOrderConst() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * APP-ID
     */
    public static final String APP_ID  = "b8c2e5dcfe54af717e669739e5790478";

    /**
     * APP-KEY
     */
    public static final String APP_KEY = "467a04de96967045d75f8d06bec20e8c";

    public static final String X_CTG_PROVINCE_ID = "8320000";
    public static final String X_CTG_LAN_ID = "8320000";

    /**
     * 密钥
     */
    public static final String SECRET_KEY = "74bdd67055e611ee92f1bef758bef040";

    /**
     * 系统渠道编码
     */
    public static final String SYSCODE = "S10300";

    /**
     * 应用编码
     */
    public static final String APPCODE = "P208020601";

    /**
     * 接口版本
     */
    public static final String VERSION_PORT = "0000";

    /**
     * 密钥版本
     */
    public static final String VERSION_KEY = "00";

    /**
     * 数据收集method
     */
    public static final String METHOD_ORDER = "FS0101";


    /**
     * 接口名称
     */
    public static final String INTERFACE_NAME = "/osa/interface";


    /**
     * 推送订单至集团接口地址
     */
    public static final String PUSH_ORDER_TO_JT_URL = "http://10.128.86.64:8000/serviceAgent/rest/dianqu/nanjing/interface";

}
