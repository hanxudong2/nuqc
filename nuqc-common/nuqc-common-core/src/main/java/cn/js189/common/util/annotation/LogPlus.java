package cn.js189.common.util.annotation;

import cn.js189.common.constants.HttpStatus;
import cn.js189.common.constants.OwnerConstants;

import java.lang.annotation.*;

/**
 * 记录接口调用相关日志
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface LogPlus {

    /** 请求地址,调用第三方接口时必填
     *  el表达式格式 例如：#user.userName(对象参数名点字段名) 单个参数时传 #name(name为字段名称) */
    String url() default "";

    /** 请求方式 */
    String type() default HttpStatus.POST;

    /** 接口调用方 调用第三方接口时必填 常量 OwnerConstants */
    String owner() default OwnerConstants.UNKNOWN;

    /** 接口编码 el 格式 */
    String interCode() default "";

}
