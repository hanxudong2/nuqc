package cn.js189.common.constants;

/**
 * 内部服务常量
 */
public class ServerNameConstants
{
    
    private ServerNameConstants(){
	    /* empty */
    }

    /*系统服务名称*/
    public static final String UQC_SYSTEM = "nuqc-system";
    
    /*查询域服务名称*/
    public static final String NUQC_UQC = "nuqc-uqc";
    

}

