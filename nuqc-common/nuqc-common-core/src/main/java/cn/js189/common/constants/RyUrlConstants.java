package cn.js189.common.constants;

import java.io.Serializable;

/**
 * @program: xxb
 * @description: 如意接口url公共参数
 * @author: jzd
 * @create: 2023/11/19 15:29
 **/
public class RyUrlConstants implements Serializable {
    private static final long serialVersionUID = 7792186696132395858L;

    public static final String X_APP_ID = "068e5f0f10cf880011facc0aa2cfcaf5";
    public static final String X_APP_KEY = "1aed1c6e816260ec6df1b3f1940dc6bd";
    public static final String SYSTEMID = "1002";
    public static final String APPKEY = "JS00000001";

    //客服dcoos
    public static final String KF_X_APP_ID = "a428c500e545b21142cf71385e551eac";
    public static final String KF_X_APP_KEY = "c254a5b589312a9713b1cbfcb86eeb11";
    public static final String KFAPPKEY = "JS00000009";
    //如意-查询客户接口
    public static final String XW_RY_QRY_CUSTOMER = "http://jsjteop.telecomjs.com:8764/jseop/crm_saop/js_xw_ry_qryCustomer";

    //如意-查询产品实例
    public static final String XW_RY_QRY_PRODINST = "http://jsjteop.telecomjs.com:8764/jseop/crm_saop/js_xw_ry_qryProdInst";

    //如意-查询客户订单
    public static final String XW_RY_QRY_CUSTOMERORDER = "http://jsjteop.telecomjs.com:8764/jseop/crm_saop/js_xw_ry_qryCustomerOrder";

    //如意-订单算费（总费用）
    public static final String XW_RY_SCENE_CHARGE_URL = "http://jsjteop.telecomjs.com:8764/jseop/crm_saop/js_xw_ry_sceneCharge";

    /** 获取礼包详细信息【受理前端】 */
    public static final String XW_RY_PACK_DETAIL_FRONT_URL = "http://jsjteop.telecomjs.com:8764/jseop/crm_saop/js_xw_sr_getPkgInfo";

    /** 礼包列表 */
    public static final String XW_RY_PACK_LIST_URL = "http://jsjteop.telecomjs.com:8764/jseop/crm_saop/js_xw_sr_listPkgSpec";

    /** 获取礼包详细信息【受理后端】*/
    public static final String XW_RY_PACK_DETAIL_AFTER_URL = "http://jsjteop.telecomjs.com:8764/jseop/crm_saop/js_xw_sr_getPkgInfoForBehind";
}
