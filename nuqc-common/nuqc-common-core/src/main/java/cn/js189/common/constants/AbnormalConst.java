/**************************************************************************************** 
        湖南创发科技有限责任公司
 ****************************************************************************************/
package cn.js189.common.constants;

/**
 * 订单域业务异常编码定义 <Description> <br>
 * 
 * @author liuzg<br>
 * @version 1.0<br>
 * @taskId <br>
 * @CreateDate 2016年1月11日 <br>
 */
public class AbnormalConst {

    /**
     * 成功
     */
    public static final String SUCCESS = "000000";
    
    

    /* =======================公共异常定义============================= */
    /**
     * 参数错误
     */
    public static final String PARAM_ERROR = "uqc_common_10000";
    
    /**
     * 参数安全验证错误
     */
    public static final String PARAM_SECURITY_ERROR = "uqc_common_10099";

    /**
     * 渠道信息不存在
     */
    public static final String CHANL_NOT_EXISTS = "uqc_common_10001";

    /**
     * 渠道状态异常
     */
    public static final String CHANL_STATE_ERROR = "uqc_common_10002";

    /**
     * 验签失败
     */
    public static final String AUTH_ERROR = "uqc_common_10003";

    /**
     * 请求url为空
     */
    public static final String RQUEST_URL_IS_NULL = "uqc_common_10004";
    
    /**
     * actionCode为空
     */
    public static final String ACTION_IS_NULL = "uqc_common_10005";
    
    /**
     * 渠道不再访问范围为空
     */
    public static final String EUAA_CAHHENL_ERRO = "euaa_10001";
    
   
    /**
     *  actionCode不在访问范围内
     */
    public static final String EUAA_ACTIONCODE_ERRO = "euaa_10002";
    
    /**
     *  区号不在访问范围内
     */
    public static final String EUAA_AREACODE_ERRO = "euaa_10003";
    
    /**
     * 渠道密钥为空
     */
    public static final String EUAA_CAHHENLKEY_ERRO = "euaa_10004";
    
    /* =======================订单异常定义============================= */

}