package cn.js189.common.util;

import cn.js189.common.constants.Constants;

import java.util.HashMap;

public class CodeConvertUtil {
	private CodeConvertUtil(){
		/*empty*/
	}
	
	private static HashMap<String, String> PRODUCT_ID_MAP =  new HashMap<>();
	private static HashMap<String, String> accNbreTypeMap =  new HashMap<>();
	
	private static HashMap<String, String> loginAccNbreTypeMap =  new HashMap<>();
	
	private static HashMap<String, String> scoreNbreTypeMap =  new HashMap<>();
	
	private static HashMap<String, String> threeAreaCodeMap =  new HashMap<>();
	
	private static HashMap<String, String> areaCodeMap =  new HashMap<>();
	
	private static HashMap<String, String> prodSpecIdMap =  new HashMap<>();
	
	private static HashMap<String, String> gjmyMap =  new HashMap<>();
	
	private static HashMap<String, String> areaCodeToStaffMap =  new HashMap<>();
	
	private static HashMap<String, String> packageConditionsMap =  new HashMap<>();
	
	private static HashMap<String, String> areaImCode =  new HashMap<>();
	
	private static HashMap<String, String> areaNameCode =  new HashMap<>();
	
	//七月宽带对应的地址编码
	private static HashMap<String, String> areaCodeApi =  new HashMap<>();
	
	static {
		String str = "kuandai";
		// -1 未指定 如果传入的产品类别参数是-1，那么程序将取号码唯一对应的主产品实例，或者其中的产品类别是固话的主产品实例。
		// 0 固话 1 小灵通 2 移动 3 ADSL 4 智能公话 5 互联星空 11 WLAN账户 12 无线e卡
		// 51 201B类卡  52 南京96998卡 53 WLAN充值卡 54 学子易行卡 55 C+W
		// 56 IPTV 61 扬州一网通充值 995 网元数据 999 未知
		PRODUCT_ID_MAP.put("2", Constants.mobile);
		PRODUCT_ID_MAP.put("0", "tel");
		PRODUCT_ID_MAP.put("3", str);
		PRODUCT_ID_MAP.put("56", "itv");
		PRODUCT_ID_MAP.put("-1", "other");
//		PRODUCT_ID_MAP.put("1", "id")
		PRODUCT_ID_MAP.put("1", "xlt");
		PRODUCT_ID_MAP.put("55", "c+w");
		
		// 1电话，小灵通，2宽带帐号、C+W上网账号；
		// 3合同号
		// 4.高校信息卡; 5客户卡
		// 9 CDMA号码; 805：C+W; 842：百事通家庭医生；
		// 843：个人法律顾问；
		// 845：政企法律顾问；
		// 846：理财顾问
		accNbreTypeMap.put("tel", "1");
		accNbreTypeMap.put("xlt", "1");
		accNbreTypeMap.put(str, "2");
		accNbreTypeMap.put(Constants.mobile, "9");
		accNbreTypeMap.put("id", "9");
		accNbreTypeMap.put("c+w", "805");
		
		// 0000000 客户类型 0000001 客户卡
		// 1000000 帐户类型  2000001 固话（产品类型） 2000002 宽带（产品类型）
		// 2000003 小灵通（产品类型） 2000004 手机（产品类型） 2000005 网厅注册帐号 2000006 学子E行
		// 2000008 无线宽带 3110001 身份证类型 4110001 未知产品类型
		
		loginAccNbreTypeMap.put("2000001", "tel");
		loginAccNbreTypeMap.put("2000002", str);
		loginAccNbreTypeMap.put("2000003", "xlt");
		loginAccNbreTypeMap.put("2000004", Constants.mobile);
		loginAccNbreTypeMap.put("2000008", "c+w");
		
		// 1客户ID 2固话(报文头已含客户区号，因此此处不带区号) 3小灵通
		// 4移动号码 5宽带账号 0身份证
		scoreNbreTypeMap.put("tel", "2");
		scoreNbreTypeMap.put("xlt", "3");
		scoreNbreTypeMap.put(str, "5");
		scoreNbreTypeMap.put(Constants.mobile, "4");
		scoreNbreTypeMap.put("id", "1");
		
		threeAreaCodeMap.put("025", "3");// 南京：025
		threeAreaCodeMap.put("0511", "4");// 镇江：0511
		threeAreaCodeMap.put("0510", "15");// 无锡
		threeAreaCodeMap.put("0512", "20");// 苏州：0512
		threeAreaCodeMap.put("0513", "26");// 南通：0513
		threeAreaCodeMap.put("0514", "33");// 扬州：0514
		threeAreaCodeMap.put("0515", "39");// 盐城：0515
		threeAreaCodeMap.put("0516", "48");// 徐州：0516
		threeAreaCodeMap.put("0517", "60");// 淮安：0517
		threeAreaCodeMap.put("0518", "63");// 连云港：0518
		threeAreaCodeMap.put("0519", "69");// 常州：0519
		threeAreaCodeMap.put("0523", "79");// 泰州：0523
		threeAreaCodeMap.put("0527", "84");// 宿迁：0527
		
		areaCodeMap.put("3", "025");// 南京：025
		areaCodeMap.put("4", "0511");// 镇江：0511
		areaCodeMap.put("15", "0510");// 无锡
		areaCodeMap.put("20", "0512");// 苏州：0512
		areaCodeMap.put("26", "0513");// 南通：0513
		areaCodeMap.put("33", "0514");// 扬州：0514
		areaCodeMap.put("39", "0515");// 盐城：0515
		areaCodeMap.put("48", "0516");// 徐州：0516
		areaCodeMap.put("60", "0517");// 淮安：0517
		areaCodeMap.put("63", "0518");// 连云港：0518
		areaCodeMap.put("69", "0519");// 常州：0519
		areaCodeMap.put("79", "0523");// 泰州：0523
		areaCodeMap.put("84", "0527");// 宿迁：0527
		
		areaCodeMap.put("集团公司", "1");
		areaCodeMap.put("江苏省公司", "2");
		areaCodeMap.put("南京市", "3");
		areaCodeMap.put("镇江全区", "4");
		areaCodeMap.put("丹阳", "5");
		areaCodeMap.put("句容", "6");
		areaCodeMap.put("扬中", "7");
		areaCodeMap.put("镇江市区", "8");
		areaCodeMap.put("南京市区", "9");
		areaCodeMap.put("江宁", "10");
		areaCodeMap.put("溧水", "11");
		areaCodeMap.put("高淳", "12");
		areaCodeMap.put("六合", "13");
		areaCodeMap.put("江浦", "14");
		areaCodeMap.put("无锡", "15");
		areaCodeMap.put("无锡市区", "16");
		areaCodeMap.put("盐都", "17");
		areaCodeMap.put("宜兴", "18");
		areaCodeMap.put("江阴", "19");
		areaCodeMap.put("苏州", "20");
		areaCodeMap.put("苏州市区", "21");
		areaCodeMap.put("胥浦", "22");
		areaCodeMap.put("吴江", "23");
		areaCodeMap.put("海门", "24");
		areaCodeMap.put("启东", "25");
		areaCodeMap.put("南通", "26");
		areaCodeMap.put("通州", "27");
		areaCodeMap.put("南通市区", "28");
		areaCodeMap.put("如东", "29");
		areaCodeMap.put("如皋", "30");
		areaCodeMap.put("海安", "31");
		areaCodeMap.put("仪征", "32");
		areaCodeMap.put("扬州", "33");
		areaCodeMap.put("扬州市区", "34");
		areaCodeMap.put("邗江", "35");
		areaCodeMap.put("江都", "36");
		areaCodeMap.put("高邮", "37");
		areaCodeMap.put("宝应", "38");
		areaCodeMap.put("盐城", "39");
		areaCodeMap.put("盐城市区", "40");
		areaCodeMap.put("大丰", "41");
		areaCodeMap.put("东台", "42");
		areaCodeMap.put("射阳", "43");
		areaCodeMap.put("阜宁", "44");
		areaCodeMap.put("滨海", "45");
		areaCodeMap.put("响水", "46");
		areaCodeMap.put("建湖", "47");
		areaCodeMap.put("徐州", "48");
		areaCodeMap.put("徐州市区", "49");
		areaCodeMap.put("铜山", "50");
		areaCodeMap.put("睢宁", "51");
		areaCodeMap.put("邳县", "52");
		areaCodeMap.put("新沂", "53");
		areaCodeMap.put("沛县", "54");
		areaCodeMap.put("丰县", "55");
		areaCodeMap.put("金湖", "56");
		areaCodeMap.put("盱眙", "57");
		areaCodeMap.put("淮阴", "58");
		areaCodeMap.put("洪泽", "59");
		areaCodeMap.put("淮安", "60");
		areaCodeMap.put("淮安市区", "61");
		areaCodeMap.put("涟水", "62");
		areaCodeMap.put("连云港", "63");
		areaCodeMap.put("连云港市区", "64");
		areaCodeMap.put("赣榆", "65");
		areaCodeMap.put("灌云", "66");
		areaCodeMap.put("东海", "67");
		areaCodeMap.put("灌南", "68");
		areaCodeMap.put("常州", "69");
		areaCodeMap.put("常州市区", "70");
		areaCodeMap.put("宿豫", "71");
		areaCodeMap.put("金坛", "72");
		areaCodeMap.put("溧阳", "73");
		areaCodeMap.put("昆山", "74");
		areaCodeMap.put("太仓", "75");
		areaCodeMap.put("常熟", "76");
		areaCodeMap.put("张家港", "77");
		areaCodeMap.put("靖江", "78");
		areaCodeMap.put("泰州", "79");
		areaCodeMap.put("泰州市区", "80");
		areaCodeMap.put("泰兴", "81");
		areaCodeMap.put("姜堰", "82");
		areaCodeMap.put("兴化", "83");
		areaCodeMap.put("宿迁", "84");
		areaCodeMap.put("宿迁市区", "85");
		areaCodeMap.put("泗洪", "86");
		areaCodeMap.put("沭阳", "87");
		areaCodeMap.put("泗阳", "88");
		areaCodeMap.put("楚州", "89");
		// areaCodeMap.put("沐阳", "287")
		areaCodeMap.put("外地", "1000");
		areaCodeMap.put("测试地区", "1001");
		
		areaNameCode.put("025", "南京");
		areaNameCode.put("0511", "镇江");
		areaNameCode.put("0510", "无锡");
		areaNameCode.put("0512", "苏州");
		areaNameCode.put("0513", "南通");
		areaNameCode.put("0514", "扬州");
		areaNameCode.put("0515", "盐城");
		areaNameCode.put("0516", "徐州");
		areaNameCode.put("0517", "淮安");
		areaNameCode.put("0518", "连云港");
		areaNameCode.put("0519", "常州");
		areaNameCode.put("0523", "泰州");
		areaNameCode.put("0527", "宿迁");
		
		// 1 PHS
		// 2 普通电话
		// 9 ADSL（动态）
		// 10 LAN（动态）
		// 11 光纤宽带（动态）
		// 12 专线ADSL(静态)
		// 13 专线LAN(静态)
		// 14 专线光纤宽带(静态)
		// 120000030 双号双机
		// 372 个人通讯助理
		// 28 电子邮箱
		// 90000000 e6-1套餐
		// 90000001 e8-1套餐
		// 90000002 e8-2套餐
		// 610 无线自由宽带帐号
		// 788 家校e通
		// 789 学子e行
		// 372 个人通讯助理
		// 364 商务领航
		// 416 客户卡
		// 802 商信通
		// 803 网信
		// 379 CDMA
		// 805 C+W
		// 842 百事通家庭医生
		// 843 个人法律顾问
		// 852 会易通
		// 845 政企法律顾问
		// 846 理财顾问
		// 863 无线宽带组合套餐/T2
		// 874 天翼圈子
		// 864 协同通信
		// 892 天翼校园自助VPN
		
		prodSpecIdMap.put("tel", "2");
		prodSpecIdMap.put(str, "5");
		prodSpecIdMap.put(Constants.mobile, "379");
		
		gjmyMap = new HashMap<>();
		gjmyMap.put("300006000800", "300006000602");
		gjmyMap.put("300006000890", "300006001280");
		gjmyMap.put("999999999999", "300006000602");
		
		// 地区码转换对应工号
		areaCodeToStaffMap.put("025", "934901");
		areaCodeToStaffMap.put("0510", "7579");
		areaCodeToStaffMap.put("0511", "11246");
		areaCodeToStaffMap.put("0512", "12978");
		areaCodeToStaffMap.put("0513", "5");
		areaCodeToStaffMap.put("0514", "36");
		areaCodeToStaffMap.put("0515", "98");
		areaCodeToStaffMap.put("0516", "30");
		areaCodeToStaffMap.put("0517", "50");
		areaCodeToStaffMap.put("0518", "18126");
		areaCodeToStaffMap.put("0519", "100001");
		areaCodeToStaffMap.put("0523", "8");
		areaCodeToStaffMap.put("0527", "27081");
		
		// 礼包查询条件
		packageConditionsMap.put("1", "ATT_TC_TYPE");
		packageConditionsMap.put("2", "ATT_TCP_AREA");
		packageConditionsMap.put("3", "ATT_BRAND");
		packageConditionsMap.put("4", "ATT_P_AREA");
		packageConditionsMap.put("5", "ATT_SHAPE");
		packageConditionsMap.put("6", "ATT_SYSTEM");
		
		// IM客户地区码
		areaImCode.put("025", "3201");
		areaImCode.put("0510", "3202");
		areaImCode.put("0516", "3203");
		areaImCode.put("0519", "3204");
		areaImCode.put("0513", "3206");
		areaImCode.put("0518", "3207");
		areaImCode.put("0517", "3208");
		areaImCode.put("0515", "3209");
		areaImCode.put("0514", "3210");
		areaImCode.put("0511", "3211");
		areaImCode.put("0523", "3212");
		areaImCode.put("0527", "3213");
		
		
		//七月宽带对应的地址编码
		// A 南京 025
		areaCodeApi.put("025", "A");
		// B 无锡 0510
		areaCodeApi.put("0510", "B");
		// C 镇江 0511
		areaCodeApi.put("0511", "C");
		// D 苏州 0512
		areaCodeApi.put("0512", "D");
		// E 南通 0513
		areaCodeApi.put("0513", "E");
		// F 扬州 0514
		areaCodeApi.put("0514", "F");
		// G 盐城 0515
		areaCodeApi.put("0515", "G");
		// H 徐州 0516
		areaCodeApi.put("0516", "H");
		// I 淮安 0517
		areaCodeApi.put("0517", "I");
		// J 连云港 0518
		areaCodeApi.put("0518", "J");
		// K 常州 0519
		areaCodeApi.put("0519", "K");
		// L 泰州 0523
		areaCodeApi.put("0523", "L");
		// M 宿迁 0527
		areaCodeApi.put("0527", "M");
	}
	
	/**
	 * 类型转换
	 * @param areaCode 区划
	 * @return 属地名称
	 */
	public static String getThreeAreaCode(String areaCode) {
		return threeAreaCodeMap.get(areaCode);
	}

}
