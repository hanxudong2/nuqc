package cn.js189.common.domain;

/**
 * 客户端-请求消息封装类
 * 
 * @author zhangpz
 * 
 */
public class Request {

    // 字符集
    private String charset;

    // 请求内容格式:xml, json, obj
    private String contentType;

    // 请求对象实例
    private Object entity;

    public Request() {
        charset = "UTF-8";
        contentType = "json";
    }

    public void setEntityAsObject(Object obj) {
        this.entity = obj;
        this.contentType = "object";
    }

    public void setEntityAsXml(String xml) {
        this.entity = xml;
        this.contentType = "xml";
    }

    public void setEntityAsJson(String json) {
        this.entity = json;
        this.contentType = "json";

    }

    public String getCharset() {
        return charset;
    }

    public String getContentType() {
        return contentType;
    }

    public Object getEntity() {
        return entity;
    }

}
