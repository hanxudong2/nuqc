package cn.js189.common.util.exception;

import cn.js189.common.domain.SystemInfo;
import lombok.Data;
import lombok.Getter;

import java.util.Date;

@Getter
public class BaseAppException extends Exception{
	
	private static final long serialVersionUID = -1187516993124229948L;
	
	private final SystemInfo systemInfo;
	
	private final String code;
	
	private final String msg;
	
	private final Date time;
	
	private final Throwable throwable;
	
	public BaseAppException(String code, String msg, Throwable throwable, SystemInfo systemInfo) {
		this.code = code;
		this.msg = msg;
		this.throwable = throwable;
		this.time = new Date();
		this.systemInfo = systemInfo;
	}
	
}
