package cn.js189.common.util;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <Description> 日期时间工具类 <br>
 * 
 * @author liuzg<br>
 * @version 1.0<br>
 */
public class DateUtil {
    
    private DateUtil(){/*empty*/}

    /**
     * 日期格式 yyyyMMddHHmmss
     */
    public static final String FORMAT_1 = "yyyyMMddHHmmss";

    /**
     * 日期格式 yyyy-MM-dd
     */
    public static final String FORMAT_2 = "yyyy-MM-dd";

    /**
     * 日期格式 yyyy-MM-dd HH:mm:ss
     */
    public static final String FORMAT_3 = "yyyy-MM-dd HH:mm:ss";

    /**
     * 日期格式 yyyyMMdd
     */
    public static final String FORMAT_4 = "yyyyMMdd";
    
    /**
     * 日期格式 yyyy-MM-dd HH:mm:ss:SS
     */
    public static final String FORMAT_5 = "yyyy-MM-dd HH:mm:ss:SS";

    /**
     * 日期格式 yyyyMMddHHmm
     */
    public static final String FORMAT_6 = "yyyyMMddHHmm";
    
    /**
     * 日期格式 HH:mm:ss
     */
    public static final String FORMAT_7 = "HH:mm:ss";
    
    /**
     * 日期格式 yyyyMM
     */
    public static final String FORMAT_8 = "yyyyMM";

    /**
     * 按指定格式获取当前系统时间 , 注意传入的参数为时间格式
     * @param format <br>
     * @return <br>
     */
    public static String getNowDate(String format) {
        return new SimpleDateFormat(format).format(new Date());
    }

    /**
     * 根据日期获取yyyy-MM-dd格式的字符串
     * @return <br>
     */
    public static String getStrByDate(Date date) {
        try {
            SimpleDateFormat format = new SimpleDateFormat(FORMAT_2);
            return format.format(date);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 获取时间对象
     */
    public static Date getDate(String strTime, String pattern) {
        Date date = null;
        SimpleDateFormat sdf = new SimpleDateFormat(pattern, Locale.US);
        try {
            date = sdf.parse(strTime);
        } catch (ParseException e) {
            e.getMessage();
        }
        return date;
    }

    /**
     * 间隔天数
     */
    public static int getCompareDate(Date startDate, Date endDate) {
        int days = -6;
        long oneDay = 24L * 60 * 60 * 1000L;
        if (startDate != null && endDate != null) {
            long timeInt = (endDate.getTime() - startDate.getTime());
            days = (int) (timeInt / oneDay);
        }
        return days;
    }

    /**
     * 获取分钟差距
     */
    public static long getCompareMin(Date date1, Date date2) {
        SimpleDateFormat format = new SimpleDateFormat(FORMAT_6);
        long dateLong = Long.parseLong(format.format(date1));
        long dateLong2 = Long.parseLong(format.format(date2));
        return dateLong - dateLong2;
    }

    /**
     * Description: <br>
     * 
     * @author zhpp<br>
     * @taskId <br>
     * @param timeSpace <br>
     * @return <br>
     */
    public static String getTime(int timeSpace) {
        Date d = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat(FORMAT_1);
        return formatter.format(new Date(d.getTime() + (long) timeSpace * 60 * 1000));
    }

    /**
     * 傳入小時分鐘得到日期
     */
    public static Date getDateByHoursAddMinutes(String s){
        SimpleDateFormat simpledateformat = new SimpleDateFormat("HH:mm");
        ParsePosition parseposition = new ParsePosition(0);
        return simpledateformat.parse(s, parseposition);
    }

    /**
     * Description: 定義輸出整數的格式﹐將輸入整數按照指定的格式輸出﹐如果輸入整數原來的位數大于或等于要格式化的位數﹐ 則返回原來輸入整數的字符串形式﹐如果不足位﹐則在前面補0 使用方法 <br>
     * 
     * @author zhpp<br>
     * @taskId <br>
     * @param number <br>
     * @param decimal <br>
     * @return <br>
     */
    public static String numberFomat(int number, int decimal) {
        String str = "";
        DecimalFormat df = new DecimalFormat();
        df.setMinimumIntegerDigits(decimal);
        str = df.format(number);
        return str;
    }

    /**
     * 功能﹕就是傳入一個字符串﹐然后把其轉化為時間類型
     * 
     * @param s <br>
     * @return <br>
     */
    public static Date strToDate(String s) {
        SimpleDateFormat simpledateformat = new SimpleDateFormat(
                FORMAT_3);
        ParsePosition parseposition = new ParsePosition(0);
        Date date = simpledateformat.parse(s, parseposition);
        return date;
    }

    /**
     * Description:取得系統當前日期，日期格式為:yyyy-MM-dd <br>
     * 
     * @author zhpp<br>
     * @taskId <br>
     * @return <br>
     */
    public static String getNowDate() {
        Date date = new Date();
        SimpleDateFormat simpledateformat = new SimpleDateFormat(FORMAT_2);
        return simpledateformat.format(date);
    }
    
    /**
     * Description:取得系統當前日期，日期格式為:yyyyMM<br>
     * 
     * @author liutian<br>
     * @taskId <br>
     * @return <br>
     */
    public static String getNowMonth() {
        Date date = new Date();
        SimpleDateFormat simpledateformat = new SimpleDateFormat(FORMAT_8);
        return simpledateformat.format(date);
    }
    
    /**
     * Description:取得系統當前日期，日期格式為:yyyyMMdd<br>
     * 
     * @author liutian<br>
     * @taskId <br>
     * @return <br>
     */
    public static String getNowDay() {
        Date date = new Date();
        SimpleDateFormat simpledateformat = new SimpleDateFormat(FORMAT_4);
        return simpledateformat.format(date);
    }
    
    /**
     * Description: <br>
     * 
     * @author zhpp<br>
     * @taskId <br>
     * @return <br>
     */
    public static String getNowYear() {
        Date date = new Date();
        SimpleDateFormat simpledateformat = new SimpleDateFormat("yyyy");
        return simpledateformat.format(date);
    }

    /**
     * Description: <br>
     * 
     * @author zhpp<br>
     * @taskId <br>
     * @return <br>
     */
    public static String getCurrentTimeSS() {
        // 设置日期格式
        SimpleDateFormat df = new SimpleDateFormat(FORMAT_5);
        // new Date()为获取当前系统时间
        return df.format(new Date());
    }

    /**
     * Description:获取今天日期 <br>
     * 
     * @author zhpp<br>
     * @taskId <br>
     * @return <br>
     */
    public static String getCurrentTime() {
        // 设置日期格式
        SimpleDateFormat df = new SimpleDateFormat(FORMAT_3);
        // new Date()为获取当前系统时间
        return df.format(new Date());
    }

    /**
     * Description:根据规则串格式化获取今天日期 <br>
     * 
     * @author wuxin<br>
     * @taskId <br>
     * @param formt <br>
     * @return <br>
     */
    public static String getCurrentTime(String formt) {
        // 设置日期格式
        SimpleDateFormat df = new SimpleDateFormat(formt);
        // new Date()为获取当前系统时间
        return df.format(new Date());
    }

    /**
     * Description:与当前时间比较 ，返回差值 秒数 <br>
     * 
     * @author zhpp<br>
     * @taskId <br>
     * @param time <br>
     * @return <br>
     */
    public static int getSecondsCompared(String time) {
        if (null == time) {
            return 0;
        }
        // 转化为long类型 计算出 分钟差值
        return (int) ((System.currentTimeMillis() - Long.parseLong(time) * 1000L) / (1000));
    }

    /**
     * Description: 获取前一天日期 <br>
     * 
     * @author liuzg <br>
     * @taskId <br>
     * @return 时间<br>
     * @CreateDate 2016年1月21日 上午3:51:41 <br>
     */
    public static String getLastDay() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        return new SimpleDateFormat(FORMAT_4).format(cal.getTime());

    }

    
    /**
     * Description:根据传入的指定时间值 获取前后日期，传0则获取当天日期，-1则为前一天日子，1则为后一天日期 <br>
     * 
     * @author liutian <br>
     * @taskId <br>
     * @return 时间<br>
     * @CreateDate 2016年1月21日 上午3:51:41 <br>
     */
    public static String getLastNextDay(String dateStr,int time) {
        Calendar cal = Calendar.getInstance();
        Date date=null; 
        try { 
    	date = new SimpleDateFormat(FORMAT_3).parse(dateStr); 
    	} catch (ParseException e) { 
    	e.printStackTrace(); 
    	} 
        cal.setTime(date); 
        cal.add(Calendar.DATE, time);
        return new SimpleDateFormat(FORMAT_3).format(cal.getTime());

    }
    
    /**
     * Description:根据传入的指定时间和时间格式，反对对应时间格式的时间str<br>
     * 
     * @author liutian <br>
     * @taskId <br>
     * @return 时间<br>
     * @CreateDate 2016年1月21日 上午3:51:41 <br>
     */
    public static String getformatDate(String dateStr,String format) {
        Calendar cal = Calendar.getInstance();
        Date date=null; 
        try { 
    	date = new SimpleDateFormat(format).parse(dateStr); 
    	} catch (ParseException e) { 
    	e.printStackTrace(); 
    	} 
        cal.setTime(date); 
        return new SimpleDateFormat(format).format(cal.getTime());

    }
    
    /**
     * Description: 获取前两天日期 <br>
     * 
     * @author liuzg <br>
     * @taskId <br>
     * @return 时间<br>
     * @CreateDate 2016年1月21日 上午3:51:41 <br>
     */
    public static String getBeforeDay() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -2);
        return new SimpleDateFormat(FORMAT_4).format(cal.getTime());

    }

    /**
     * Description: 获取当天某个时间的Date对象 传入小时 <br>
     * 
     * @author liuzg <br>
     * @taskId <br>
     * @param hour <br>
     * @return Date <br>
     * @CreateDate 2016年4月8日 下午2:34:23 <br>
     */
    public static Date getDateByHour(int hour) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.set(Calendar.HOUR_OF_DAY, hour);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        return cal.getTime();
    }
    
    /** 
     * 得到年月日，中文的
     */  
    public static String getDateForChinese() {
    	 Calendar now = Calendar.getInstance();
         StringBuilder dateFor = new StringBuilder();
         dateFor.append(now.get(Calendar.YEAR)).append("年");
         dateFor.append((now.get(Calendar.MONTH) + 1)).append("月");
         dateFor.append(now.get(Calendar.DAY_OF_MONTH)).append("日");
         return dateFor.toString();
    }
    
    /** 
     * 得到几天前的时间 
     *  
     * @param d 
     * @param day 
     * @return 
     */  
   public static Date getDateBefore(Date d, int day) {  
       Calendar now = Calendar.getInstance();  
       now.setTime(d);  
       now.set(Calendar.DATE, now.get(Calendar.DATE) - day);  
       return now.getTime();  
   }    
   /** 
    * 得到几天后的时间 
    *  
    * @param d 
    * @param day 
    * @return 
    */  
   public static Date getDateAfter(Date d, int day) {  
       Calendar now = Calendar.getInstance();  
       now.setTime(d);  
       now.set(Calendar.DATE, now.get(Calendar.DATE) + day);  
       return now.getTime();  
   }
   
   /**
    * Description: 对时间的格式和有效性做校验<br> 
    *  
    * @author liutian<br>
    * @taskId <br>
    * @param timeStr
    * @return <br>
    */
   public static boolean valiDateTimeWithLongFormat(String timeStr) {
		String format = "((19|20)[0-9]{2})-(0?[1-9]|1[012])-(0?[1-9]|[12][0-9]|3[01]) "
				+ "([01]?[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]";
		Pattern pattern = Pattern.compile(format);
		Matcher matcher = pattern.matcher(timeStr);
		if (matcher.matches()) {
			pattern = Pattern.compile("(\\d{4})-(\\d+)-(\\d+).*");
			matcher = pattern.matcher(timeStr);
			if (matcher.matches()) {
				int y = Integer.parseInt(matcher.group(1));
				int m = Integer.parseInt(matcher.group(2));
				int d = Integer.parseInt(matcher.group(3));
				if (d > 28) {
					Calendar c = Calendar.getInstance();
					c.set(y, m-1, 1);
					int lastDay = c.getActualMaximum(Calendar.DAY_OF_MONTH);
					return (lastDay >= d);
				}
			}
			return true;
		}
		return false;
	}

}
