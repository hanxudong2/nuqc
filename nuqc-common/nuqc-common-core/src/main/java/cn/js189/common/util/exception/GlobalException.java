package cn.js189.common.util.exception;

/**
 * 全局异常
 * 
 * @author hxd
 */
public class GlobalException extends RuntimeException
{
    private static final long serialVersionUID = 1L;

    /**
     * 错误提示
     */
    private String message;
    
    /**
     * 编号
     */
    private String code;

    /**
     * 错误明细，内部调试错误
     *
     */
    private String detailMessage;

    /**
     * 空构造方法，避免反序列化问题
     */
    public GlobalException()
    {
    }

    public GlobalException(String message)
    {
        this.message = message;
    }
    
    public GlobalException(String message,String code)
    {
        this.code = code;
        this.message = message;
    }

    public void setCode(String code){
        this.code = code;
    }
    
    public String getCode(){
        return this.code;
    }
    
    public String getDetailMessage()
    {
        return detailMessage;
    }

    public GlobalException setDetailMessage(String detailMessage)
    {
        this.detailMessage = detailMessage;
        return this;
    }

    @Override
    public String getMessage()
    {
        return message;
    }

    public GlobalException setMessage(String message)
    {
        this.message = message;
        return this;
    }
}