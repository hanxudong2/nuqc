package cn.js189.common.enums;

/**
 * 老接口actionCode对应的转换类枚举
 * 
 * @author Mid
 */
public enum ResponseBusinessTypeEnum {
 // 通用处理返回的类，以后如果没有变更不再生成新的
    COMMONTRANSFORM(1, "commonTransform"),
    // 根据证件号码查询客户信息转换类
    QRY_CUSTOMER_BY_CERTNUM(61011, "qryCustomerByCertNumTransform"),

    // 根据证件号码查询客户信息转换类
    QRY_PRODINST_BY_CERTNUM(19037, "qryProdInstByCertNumTransform"),

    // 根据partyId查询客户订单转换类
    QRY_ORDER_BY_PARTYID(61002, "qryOrderByPartyIdTransform"),
    // 根据partyId查询用户套餐转换类
    QRY_OFFERINST_BY_OWERCUSTID(17007, "qryOfferInstByOwnerCustIdTransform"),
    // 查询附属销售品
    QRY_OFFERINST_BY_ACCPRODINSTID(19009, "qryOfferInstByAccProdInstIdTransform"),
    // 查询客户订单
    QRY_CUSTOMER_ORDER(191202, "qryCustomerOrderTransform"),
    // 查询客户订单项
    QRY_ORDERITEM_DETAIL(200923, "qryOrderItemDetailTransform"),
 // 查询一年内办卡数量
    QRY_PHONENUMCOUNT(210325, "qryPhoneNumCountTransform"),
 // 查询客户订单
    QRY_VALUEADDEDSERVICE(191206, "qryValueAddedServiceTransform"),

    // 根据证件号码查询客户信息转换类
    QRY_OFFERINST(190091, "qryOfferInstTransform"),
    // 根据接入号码查询PUK码
    QRY_PUK_BY_ACCNUM(10011,"qryPukByAccNumTransform"),

    //根据接入号查询账户信息
    QRY_ACCOUNT_BY_ACCNUM(150503, "qryAccountByAccNumTransform"),

    //根据账号查询产品规格信息
    QRY_PRODID_BY_ACCNUM(150504, "qryProdIdByAccountTransform"),

    //根据接入号查询产品规格信息
    QRY_MAINSALE_BY_CUSTID(19015, "qryMainSaleByCustIdTransform"),

    //    19015  转换
    QRY_MAINSALE_BY_ID(190151, "qryMainSaleByIdTransform"),  

    //根据接入号查询查询客户信息->(转换)查询证件信息参数
    QRY_CUSTOMER_BY_ACCNUM_1(10023, "qryCustomerByAccNumTransform"),

    //查询销售品到期时间
    QRY_OFFER_INST_ENDTIME(19013, "qryOfferInstByOfferInstIdTransform"),

    //根据接入号查询查询客户信息->(转换)根据身份证查询partyId
    QRY_CUSTOMER_BY_ACCNUM_2(61011, "qryPartyIdByIdTransform"),

    //主销售品查询
    QRY_MAINOFFER_INST(15051, "qryMainOfferInstTransform"),

    //查询产品下在用的服务
    QRY_PROINST_BY_ACCPRODINSTID(19113, "qryProdInstByAccProdInstIdTransform"),
    //查询产品下在用的服务
    QRY_CUSTORDERANDPDFFLAG(30000, "qryCustOrderAndPdfFlagTransform"),
    //查询停机状态
    QRY_ACCOUNNTSTATE(22, "qryAccountUnderStateTransform"),
    //号码名下欠费状态
    QRY_ACCNBRUNDERSTATE(23, "qryAccNbrUnderStateTransform"),
    //查询客户信息
    QRY_CUSTOMER(24, "qryCustomerTransform"),
    //查询产品实例
    QRY_PRODINST(25, "qryProdInstTransform"),
  //查询产品实例
    QRY_ADSLINFO(181120, "qryAdslInfoTransform"),
    //查询产品规格
    QRY_PRODID(33, "qryProdIdTransform"),
    //查询产品规格
    QRY_OFFERDETAIL(45, "qryOfferDetailTransform"),
  //查询一般纳税人
    QRY_GENERALTAXPAYER(190718, "qryGeneralTaxpayerTransform"),
  //查询黑名单
    QRY_SPECIAL_LIST(190801, "qrySpecialListTransform"),
    //获取用户信息
    QRY_INUSEPROD(26,"qryInUseProdTransform"),
    //根据产品id查询合账付费的产品列表
    QRY_PRODS(27, "qryProdstTransform"),
    //根据号码查询亲情网内所有成员长号短号资料
    QRY_MEMERINFO(29, "qryMemberInfoTransform"),
    //新用户信息查询
    NEW_QURUSSERINFOS(40, "newQurUserInfos"),
    //销售品信息查询
    QRY_OFFINST(19500, "qryOffInstTransform"),
    //销售品信息查询
    QRY_OFFINSTER(19501, "qryOffInstErTransform"),
    // 根据接入号查询产品实例 
    QRY_PRODINST_BY_ACCNUM(17086, "qryProdInstByAccNumTransform"),
    // 查询可订购提醒定制
    QRY_ABLE_REMINDPRODINST(19017, "qryAbleRemindProdInstTransform"),
    //查询已订购提醒定制
    QRY_REMINDPRODINST(19016,"qryRemindProdInstTransform");
    // 业务类型
    private int businessType;
    // 实现类bean名称
    private String beanName;

    private ResponseBusinessTypeEnum(int businessType, String beanName) {
        this.businessType = businessType;
        this.beanName = beanName;
    }

    public static String getBeanName(int businessType) {
        for (ResponseBusinessTypeEnum responseBusinessType : ResponseBusinessTypeEnum.values()) {
            if (responseBusinessType.businessType == businessType) {
                return responseBusinessType.beanName;
            }
        }
        return null;
    }

    public int getBusinessType() {
        return businessType;
    }

    public String getBeanName() {
        return beanName;
    }

}
