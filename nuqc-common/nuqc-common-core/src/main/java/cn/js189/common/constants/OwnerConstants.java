package cn.js189.common.constants;

/**
 * 接口来源常量
 */
public class OwnerConstants {

    private OwnerConstants(){}

    public static final String UNIT_CREDIT = "企信";

    public static final String UNKNOWN = "未知";

}

