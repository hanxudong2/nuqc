package cn.js189.common.domain;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import java.io.Serializable;

/**
 * 请求信息
 */
@Data
public class ReqInfo implements Serializable {

	private static final long serialVersionUID = 1L;
	
	/** 请求头部信息 */
	private ReqHead head;
	
	/** 请求体 */
	private JSONObject body;

}
