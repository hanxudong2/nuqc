package cn.js189.log.service;

import cn.js189.system.api.RemoteSysLogApi;
import cn.js189.system.api.domain.SysLogPlus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SysLogService {

    @Autowired
    private RemoteSysLogApi remoteSysLogApi;

    public void saveLog(SysLogPlus sysLog){
        remoteSysLogApi.saveLog(sysLog);
    }

}
