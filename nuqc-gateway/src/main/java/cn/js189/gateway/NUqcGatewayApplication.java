package cn.js189.gateway;

import cn.js189.common.util.annotation.EnableNFeignClients;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.scheduling.annotation.EnableScheduling;

@Slf4j
@EnableNFeignClients
@SpringBootApplication(exclude= {DataSourceAutoConfiguration.class})
@EnableDiscoveryClient
@EnableScheduling
public class NUqcGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(NUqcGatewayApplication.class, args);
        log.info("网关服务启动成功......`");
    }
}
