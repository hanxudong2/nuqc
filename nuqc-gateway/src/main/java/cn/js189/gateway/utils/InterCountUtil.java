package cn.js189.gateway.utils;

import cn.hutool.core.date.DateUtil;
import cn.hutool.json.JSONUtil;
import cn.js189.common.constants.Constants;
import cn.js189.system.api.RemoteSysLogApi;
import cn.js189.system.api.domain.SysInterCount;
import cn.js189.system.api.domain.SysInterCountDto;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 接口访问数量统计工具类
 */
@Slf4j
@Component
public class InterCountUtil {
	
	@Lazy
	@Resource
	private RemoteSysLogApi remoteSysLogApi;
	
	/**
	 * 保存接口访问次数统计
	 * @param url 请求地址
	 * @param bodyString 参数
	 */
	public static void saveInterCount(String url,String bodyString){
		cn.hutool.json.JSONObject obj = JSONUtil.parseObj(bodyString);
		cn.hutool.json.JSONObject head = obj.getJSONObject("head");
		String channelId = head.getStr("channelId");
		
		String interKey = Constants.LOG_INTER_COUNT_KEY + DateUtil.format(DateUtil.date(),"yyyyMMddHH");
		Map<String, JSONObject> json = Constants.INTER_COUNT_MAP.getOrDefault(interKey,new HashMap<>());
		JSONObject object = json.getOrDefault(url,new JSONObject());
		Long count1 = object.getLong(Constants.COUNT);
		long count = (count1 == null ? 1L : count1 + 1L);
		object.put(Constants.COUNT,count);
		object.put(Constants.CHANNEL_ID,channelId);
		object.put(Constants.CREATE_TIME,DateUtil.now());
		json.put(url,object);
		Constants.INTER_COUNT_MAP.put(interKey,json);
		log.info("接口访问次数:{}",count);
		log.info("请求接口参数{}",json);
	}
	
	/**
	 * 接口访问次数持久化
	 */
	@Scheduled(cron = "59 59 11 * * ? ")
	public void saveInter(){
		
		log.info("存储接口调用次数统计开始......");
		long time = Long.parseLong(DateUtil.format(DateUtil.date(),"yyyyMMddHH")) - 1;
		List<SysInterCount> interCountList = new ArrayList<>(1);
		List<String> interKeys = new ArrayList<>(1);
		for (Map.Entry<String, Map<String, JSONObject>> interMap : Constants.INTER_COUNT_MAP.entrySet()) {
			long interKeyTime = Long.parseLong(interMap.getKey().replace(Constants.LOG_INTER_COUNT_KEY,""));
			if (interKeyTime <= time) {
				for (Map.Entry<String, JSONObject> map : interMap.getValue().entrySet()) {
					JSONObject value = map.getValue();
					SysInterCount sysInterCount = new SysInterCount();
					sysInterCount.setUrl(map.getKey());
					sysInterCount.setChannelId(value.getString(Constants.CHANNEL_ID));
					sysInterCount.setCreateTime(value.getString(Constants.CREATE_TIME));
					sysInterCount.setInterCount(value.getLong(Constants.COUNT));
					interCountList.add(sysInterCount);
				}
				interKeys.add(interMap.getKey());
			}
		}
		
		//异步调用feign服务接口 　　
		remoteSysLogApi.saveInterfaceCount(new SysInterCountDto(interCountList));
		interKeys.forEach(Constants.INTER_COUNT_MAP::remove);
		log.info("存储接口调用次数统计结束......");
	}

}
