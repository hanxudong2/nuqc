package cn.js189.uqc.api;

import cn.js189.common.constants.ServerNameConstants;
import cn.js189.uqc.api.factory.RemoteAreaBillFallBackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(contextId = "remoteAreaBillApi",value = ServerNameConstants.NUQC_UQC,fallback = RemoteAreaBillFallBackFactory.class)
public interface RemoteAreaBillApi {
	
	/**
	 *将mysql的HCode数据导入到redis
	 */
	@PostMapping("/iAreaBill/areaInfoMysqlToRedis")
	String areaInfoMysqlToRedis();
	
	/**
	 *将arpu信息从数据库刷入缓存
	 */
	@PostMapping("/iAreaBill/arpuInfoMysqlToRedis")
	String arpuInfoMysqlToRedis();
}
