package cn.js189.uqc.api.factory;

import cn.js189.uqc.api.RemoteAreaBillApi;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class RemoteAreaBillFallBackFactory implements FallbackFactory<RemoteAreaBillApi> {
	
	@Override
	public RemoteAreaBillApi create(Throwable cause) {
		log.error(cause.getMessage(),cause);
		return new RemoteAreaBillApi() {
			@Override
			public String areaInfoMysqlToRedis() {
				return "error";
			}
			
			@Override
			public String arpuInfoMysqlToRedis() {
				return "error";
			}
		};
	}
	
}
