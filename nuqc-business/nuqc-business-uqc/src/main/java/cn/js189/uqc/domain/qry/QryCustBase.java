/**************************************************************************************** 
        湖南创发科技有限责任公司
 ****************************************************************************************/
package cn.js189.uqc.domain.qry;

import cn.js189.uqc.common.RequestParamBase;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 
 * <Description>  智慧BSS 查询订单列表及无纸化标识信息<br> 
 *  
 * @author 刘添<br>
 * @version 1.0<br>
 * @taskId <br>
 * @CreateDate 2018年4月2日 <br>
 */

public abstract class QryCustBase extends RequestParamBase {

	/**
	 *  日志组件
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(QryCustBase.class);

	/**
	 *  svcCode 提供的API接口名称
	 */
	public static final String BSS_TCP_CONT_SVC_CODE = "js.intf.qryCustOrderAndPdfFlag";
	
	/**
	 *  地区标识
	 */
	protected String regionId;
	
	/**
	 * 查询订单列表及无纸化标识信息构造器
	 * 
	 * @param regionId:地区标识
	 */
	public QryCustBase(String regionId) {
		super();
		this.regionId = regionId;
	}
		
	/**
	 * Description: <br> 
	 *  
	 * @author 刘添<br>
	 * @taskId <br>
	 * @return <br>
	 */ 
	@Override
	protected JSONObject generateTcpCont() {
		return super.generateTcpCont(BSS_TCP_CONT_APP_KEY, BSS_TCP_CONT_SVC_CODE, null);
	}

	/**
	 * Description: <br> 
	 *  
	 * @author 刘添<br>
	 * @taskId <br>
	 * @return <br>
	 */ 
	@Override
	protected JSONObject generateSvcCont() {
		JSONObject authenticationInfo = generateAuthenticationInfo();
		if (null == authenticationInfo) {
			LOGGER.error("generate authenticationInfo failure");
			return null;
		}

		JSONObject requestObject = generateRequestObject();
		if (null == requestObject) {
			LOGGER.error("generate requestObject failure");
			return null;
		}

		JSONObject svcCont = new JSONObject();
		svcCont.put("authenticationInfo", authenticationInfo);
		svcCont.put("requestObject", requestObject);
		return svcCont;
	}
	
	/**
	 * 生成 svcCont 下的认证信息
	 * 
	 * @return
	 */
	protected JSONObject generateAuthenticationInfo() {
		return super.generateSvcContAuthenticationInfo(regionId, null, null, null);
	}
	
	
	/**
	 * 生成 svcCont 下的请求对象
	 * 
	 * @return
	 */
	protected JSONObject generateRequestObject() {
		JSONObject requestObject = new JSONObject();
		requestObject.put("regionId", regionId);
		return improveRequestObject(requestObject);
	}

	/**
	 * 补齐 svcCont 下的请求对象
	 * 
	 * @param requestObject:svcCont下的请求对象
	 * @return
	 */
	protected abstract JSONObject improveRequestObject(JSONObject requestObject);

}
