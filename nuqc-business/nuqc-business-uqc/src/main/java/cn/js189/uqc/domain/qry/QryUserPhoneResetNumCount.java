package cn.js189.uqc.domain.qry;

import com.alibaba.fastjson.JSONObject;


public class QryUserPhoneResetNumCount extends QryUserPhoneResetNumCountBase {

    private String accNum;
    private String regionId;

    public QryUserPhoneResetNumCount(String regionId, String accNum) {
        super(regionId);
        this.accNum = accNum;
    }


    protected JSONObject improveRequestObject(JSONObject requestObject) {
        requestObject.put("accNum", accNum);
        requestObject.put("lanId", regionId);
        return requestObject;
    }

}
