package cn.js189.uqc.domain;

import lombok.Data;

import java.io.Serializable;

@Data
public class ArpuInfo implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String phoneNbr;
	
	private String arpu;
	
	private String arpu_kefu;
	
}
