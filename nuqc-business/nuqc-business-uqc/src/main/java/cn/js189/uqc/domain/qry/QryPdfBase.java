package cn.js189.uqc.domain.qry;

import cn.js189.uqc.common.RequestParamBase;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 智慧BSS，查询无纸化信息
 * 
 * @author Mid
 */
public abstract class QryPdfBase extends RequestParamBase {
	// 日志组件
	private static final Logger LOGGER = LoggerFactory.getLogger(QryPdfBase.class);

	// svcCode 提供的API接口名称
//	public static final String BSS_TCP_CONT_SVC_CODE = "js.intf.qryPdfByOlNbr";
	public static final String BSS_TCP_CONT_SVC_CODE = "js.intf.qryAppointAgreement";

	// 地区标识
	protected String regionId;

	/**
	 * 查询无纸化信息构造器
	 * 
	 * @param regionId:地区标识
	 */
	public QryPdfBase(String regionId) {
		super();
		this.regionId = regionId;
	}

	@Override
	protected JSONObject generateTcpCont() {
		JSONObject a=  super.generateTcpCont(BSS_TCP_CONT_APP_KEY, BSS_TCP_CONT_SVC_CODE, null);
//		a.put("appKey", "JS_DEFAULT");
//		a.put("sign", "123456");
		return a;
	}

	@Override
	protected JSONObject generateSvcCont() {
		JSONObject authenticationInfo = generateAuthenticationInfo();
		authenticationInfo.put("centerUrlFlag", "1");

		JSONObject requestObject = generateRequestObject();
		if (null == requestObject) {
			LOGGER.error("generate requestObject failure");
			return null;
		}

		JSONObject svcCont = new JSONObject();
		svcCont.put("authenticationInfo", authenticationInfo);
		svcCont.put("requestObject", requestObject);
		return svcCont;
	}

	/**
	 * 生成 svcCont 下的认证信息
	 * 
	 * @return
	 */
	protected JSONObject generateAuthenticationInfo() {
		return super.generateSvcContAuthenticationInfo(regionId, null, null, null);
	}

	/**
	 * 生成 svcCont 下的请求对象
	 * 
	 * @return
	 */
	protected JSONObject generateRequestObject() {
		JSONObject requestObject = new JSONObject();
		requestObject.put("regionId", regionId);
		return improveRequestObject(requestObject);
	}

	/**
	 * 补齐 svcCont 下的请求对象
	 * 
	 * @param requestObject:svcCont下的请求对象
	 * @return
	 */
	protected abstract JSONObject improveRequestObject(JSONObject requestObject);

}
