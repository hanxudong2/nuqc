package cn.js189.uqc.domain;

import com.alibaba.fastjson.JSONObject;

/**
 * 卡券模板
 */
public class BaseInvoiceTemplate {

	private String payee;

	private String type;

	private JSONObject baseInfo;

	private String logoUrl;

	private String title;

	private String customUrlName;

	private String customUrl;

	private String customUrlSubTitle;

	private String promotionUrlName;

	private String promotionUrl;

	private String promotionUrlSubTitle;

	public BaseInvoiceTemplate() {
	}

	public BaseInvoiceTemplate(String payee, String type, String logoUrl, String title) {
		this.payee = payee;
		this.type = type;
		this.logoUrl = logoUrl;
		this.title = title;
	}

	public String getPayee() {
		return payee;
	}

	public void setPayee(String payee) {
		this.payee = payee;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public JSONObject getBaseInfo() {
		return baseInfo;
	}

	public void setBaseInfo(JSONObject baseInfo) {
		this.baseInfo = baseInfo;
	}

	public String getLogoUrl() {
		return logoUrl;
	}

	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCustomUrlName() {
		return customUrlName;
	}

	public void setCustomUrlName(String customUrlName) {
		this.customUrlName = customUrlName;
	}

	public String getCustomUrl() {
		return customUrl;
	}

	public void setCustomUrl(String customUrl) {
		this.customUrl = customUrl;
	}

	public String getCustomUrlSubTitle() {
		return customUrlSubTitle;
	}

	public void setCustomUrlSubTitle(String customUrlSubTitle) {
		this.customUrlSubTitle = customUrlSubTitle;
	}

	public String getPromotionUrlName() {
		return promotionUrlName;
	}

	public void setPromotionUrlName(String promotionUrlName) {
		this.promotionUrlName = promotionUrlName;
	}

	public String getPromotionUrl() {
		return promotionUrl;
	}

	public void setPromotionUrl(String promotionUrl) {
		this.promotionUrl = promotionUrl;
	}

	public String getPromotionUrlSubTitle() {
		return promotionUrlSubTitle;
	}

	public void setPromotionUrlSubTitle(String promotionUrlSubTitle) {
		this.promotionUrlSubTitle = promotionUrlSubTitle;
	}

	@Override
	public String toString() {
		JSONObject param = new JSONObject();
		JSONObject invoiceInfo = new JSONObject();

		baseInfo = new JSONObject();
		baseInfo.put("logo_url", this.getLogoUrl());

		baseInfo.put("title", this.getTitle());
		baseInfo.put("custom_url_name", this.getCustomUrlName());
		baseInfo.put("custom_url", this.getCustomUrl());
		baseInfo.put("custom_url_sub_title", this.getCustomUrlSubTitle());
		baseInfo.put("promotion_url_name", this.getPromotionUrlName());
		baseInfo.put("promotion_url", this.getPromotionUrl());
		baseInfo.put("promotion_url_sub_title", this.getPromotionUrlSubTitle());

		invoiceInfo.put("base_info", baseInfo);
		invoiceInfo.put("type", this.getType());
		invoiceInfo.put("payee", this.getPayee());

		param.put("invoice_info", invoiceInfo);

		return param.toString();
	}
}
