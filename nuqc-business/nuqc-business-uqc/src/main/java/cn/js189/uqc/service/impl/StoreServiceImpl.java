package cn.js189.uqc.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.js189.common.constants.Constants;
import cn.js189.common.domain.ReqHead;
import cn.js189.common.domain.ReqInfo;
import cn.js189.common.domain.RespHead;
import cn.js189.common.domain.RespInfo;
import cn.js189.common.util.Utils;
import cn.js189.uqc.service.IStoreService;
import cn.js189.uqc.service.PubAddressIpervice;
import cn.js189.uqc.util.BSSHelper;
import cn.js189.uqc.util.HttpClientUtils;
import cn.js189.uqc.util.UqcAddressCodeConstants;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
public class StoreServiceImpl implements IStoreService {
	
	@Resource
	private PubAddressIpervice pubAddressIpervice;
	
	@Override
	public String getStoreMsg(String reqParams) {
		JSONObject response = null;
		log.info("BssSystemImpl.getStoreMsg input:{}" , reqParams);
		RespInfo responseJson = new RespInfo();
		RespHead responseHead = new RespHead();
		responseJson.setHead(responseHead);
		//  获得接口号流水
		String tranId = "";
		try {
			ReqInfo reqInfo = BeanUtil.toBean(reqParams, ReqInfo.class);
			ReqHead head = reqInfo.getHead();
			JSONObject body = reqInfo.getBody();
			tranId = head.getTranId();
			Map<String,String> headMap = new HashMap<>();
			headMap.put("X-APP-ID","944d8bf998dd426e8b198de2f07c9c80");
			headMap.put("X-APP-KEY","8fc30c17998f4b609fd1ce814b9f2963");
			
			log.info("BssSystemImpl.getStoreMsg body：{}" , body);
			String actionUrl = pubAddressIpervice.getActionUrl(UqcAddressCodeConstants.EOP_QRY_GET_STORE_MSM, head.getLoginNbr());
			String bssRes = HttpClientUtils.sendPostWithHead(actionUrl, body.toString(),false,headMap);
			log.info("BssSystemImpl.getStoreMsg bssRes：{}" , bssRes);
			if (StringUtils.isBlank(bssRes)) {
				String resultMsg = Constants.ERR_NULL;
				return BSSHelper.getErrInfo("99", resultMsg).toString();
			}
			return Utils.builderResponse(bssRes, responseHead, responseJson, tranId,"code","","err").toString();
		}catch (Exception e) {
			response = Utils.dealExceptionResp(responseHead, tranId, responseJson);
			return response.toString();
		}
	}

}
