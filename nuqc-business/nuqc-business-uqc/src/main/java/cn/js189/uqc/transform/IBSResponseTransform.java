package cn.js189.uqc.transform;

import com.alibaba.fastjson.JSONObject;

public interface IBSResponseTransform {

	JSONObject transform(String responseString, String tranId);
}
