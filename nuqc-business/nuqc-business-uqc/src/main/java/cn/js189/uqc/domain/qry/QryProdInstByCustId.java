package cn.js189.uqc.domain.qry;

import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 通过接入号的方式查询产品信息，生成请求报文
 * 
 * @author Mid
 */
public class QryProdInstByCustId extends QryProdInstBase {
	// 日志组件
	private static final Logger LOGGER = LoggerFactory.getLogger(QryProdInstByCustId.class);

	// 客户标识custId
	private String custId;

	public QryProdInstByCustId(String regionId, String uncompletedFlag, String custId) {
		super(regionId, uncompletedFlag);
		this.custId = custId;
	}

	@Override
	protected JSONObject improveRequestObject(JSONObject requestObject) {
		requestObject.put("custId", custId);
		LOGGER.debug("requestObject:{}", requestObject.toString());
		return requestObject;
	}
}
