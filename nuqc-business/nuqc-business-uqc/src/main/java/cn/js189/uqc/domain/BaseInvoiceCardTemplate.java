package cn.js189.uqc.domain;

import cn.js189.common.util.helper.UUIDHelper;
import com.alibaba.fastjson.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class BaseInvoiceCardTemplate {
	
	/**
	 * 商户授权开票订单号 invoiceId
	 */
	private String orderId;
	
	/**
	 * 发票cardId
	 */
	private String cardId;
	
	/**
	 * 该订单号授权时使用的appid，一般为商户appid
	 */
	private String appid;
	
	/**
	 * 发票具体内容
	 */
	private JSONObject cardExt;
	
	/**
	 * 随机字符串，防止重复
	 */
	private String nonceStr;
	
	/**
	 * 用户信息结构体
	 */
	private JSONObject userCard;
	
	private JSONObject invoiceUserData;
	
	/**
	 * 发票金额
	 * 单位:分
	 */
	private int fee;
	
	/**
	 * 发票抬头
	 */
	private String title;
	
	/**
	 * 发票开票时间
	 * 10位时间戳
	 */
	private int billingTime;
	
	/**
	 * 发票代码
	 */
	private String billingNo;
	
	/**
	 * 发票号码
	 */
	private String billingCode;
	
	/**
	 * 商品详情结构
	 */
	private List<Goods> info;
	
	/**
	 * 不含税金额
	 * 单位:分
	 */
	private int feeWithoutTax;
	
	/**
	 * 税额,以分为单位
	 */
	private int tax;
	
	/**
	 * 上传pdf返回的sMediaId
	 */
	private String sPdfMediaId;
	
	/**
	 * 其他消费附件
	 */
	private String sTripPdfMediaId;
	
	/**
	 * 校验码，发票pdf右上角，开票日期下的校验码
	 */
	private String checkCode;
	
	/**
	 * 购买方纳税人识别号
	 */
	private String buyerNumber;
	
	/**
	 * 购买方地址、电话
	 */
	private String buyerAddressAndPhone;
	
	/**
	 * 购买方开户行及账号
	 */
	private String buyerBankAccount;
	
	/**
	 * 销售方纳税人识别号
	 */
	private String sellerNumber;
	
	/**
	 * 销售方地址、电话
	 */
	private String sellerAddressAndPhone;
	
	/**
	 * 销售方开户行及账号
	 */
	private String sellerBankAccount;
	
	/**
	 * 备注，发票右下角初
	 */
	private String remarks;
	
	/**
	 * 收款人，发票左下角处
	 */
	private String cashier;
	
	/**
	 * 开票人，发票下方处
	 */
	private String maker;
	
	/**
	 * 微信openid
	 */
	private String wxOpenId;
	
	/**
	 * 商户用户唯一标识
	 */
	private String partyId;
	
	public String getPartyId() {
		return partyId;
	}
	
	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}
	
	public String getWxOpenId() {
		return wxOpenId;
	}
	
	public void setWxOpenId(String wxOpenId) {
		this.wxOpenId = wxOpenId;
	}
	
	public String getOrderId() {
		return orderId;
	}
	
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	
	public String getCardId() {
		return cardId;
	}
	
	public void setCardId(String cardId) {
		this.cardId = cardId;
	}
	
	public String getAppid() {
		return appid;
	}
	
	public void setAppid(String appid) {
		this.appid = appid;
	}
	
	public JSONObject getCardExt() {
		return cardExt;
	}
	
	public void setCardExt(JSONObject cardExt) {
		this.cardExt = cardExt;
	}
	
	public String getNonceStr() {
		return nonceStr;
	}
	
	public void setNonceStr(String nonceStr) {
		this.nonceStr = nonceStr;
	}
	
	public JSONObject getUserCard() {
		return userCard;
	}
	
	public void setUserCard(JSONObject userCard) {
		this.userCard = userCard;
	}
	
	public int getFee() {
		return fee;
	}
	
	public void setFee(int fee) {
		this.fee = fee;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public int getBillingTime() {
		return billingTime;
	}
	
	public void setBillingTime(int billingTime) {
		this.billingTime = billingTime;
	}
	
	public String getBillingNo() {
		return billingNo;
	}
	
	public void setBillingNo(String billingNo) {
		this.billingNo = billingNo;
	}
	
	public String getBillingCode() {
		return billingCode;
	}
	
	public void setBillingCode(String billingCode) {
		this.billingCode = billingCode;
	}
	
	public List<Goods> getInfo() {
		return info;
	}
	
	public void setInfo(List<Goods> info) {
		this.info = info;
	}
	
	public int getFeeWithoutTax() {
		return feeWithoutTax;
	}
	
	public void setFeeWithoutTax(int feeWithoutTax) {
		this.feeWithoutTax = feeWithoutTax;
	}
	
	public int getTax() {
		return tax;
	}
	
	public void setTax(int tax) {
		this.tax = tax;
	}
	
	public String getsPdfMediaId() {
		return sPdfMediaId;
	}
	
	public void setsPdfMediaId(String sPdfMediaId) {
		this.sPdfMediaId = sPdfMediaId;
	}
	
	public String getsTripPdfMediaId() {
		return sTripPdfMediaId;
	}
	
	public void setsTripPdfMediaId(String sTripPdfMediaId) {
		this.sTripPdfMediaId = sTripPdfMediaId;
	}
	
	public String getCheckCode() {
		return checkCode;
	}
	
	public void setCheckCode(String checkCode) {
		this.checkCode = checkCode;
	}
	
	public String getBuyerNumber() {
		return buyerNumber;
	}
	
	public void setBuyerNumber(String buyerNumber) {
		this.buyerNumber = buyerNumber;
	}
	
	public String getBuyerAddressAndPhone() {
		return buyerAddressAndPhone;
	}
	
	public void setBuyerAddressAndPhone(String buyerAddressAndPhone) {
		this.buyerAddressAndPhone = buyerAddressAndPhone;
	}
	
	public String getBuyerBankAccount() {
		return buyerBankAccount;
	}
	
	public void setBuyerBankAccount(String buyerBankAccount) {
		this.buyerBankAccount = buyerBankAccount;
	}
	
	public String getSellerNumber() {
		return sellerNumber;
	}
	
	public void setSellerNumber(String sellerNumber) {
		this.sellerNumber = sellerNumber;
	}
	
	public String getSellerAddressAndPhone() {
		return sellerAddressAndPhone;
	}
	
	public void setSellerAddressAndPhone(String sellerAddressAndPhone) {
		this.sellerAddressAndPhone = sellerAddressAndPhone;
	}
	
	public String getSellerBankAccount() {
		return sellerBankAccount;
	}
	
	public void setSellerBankAccount(String sellerBankAccount) {
		this.sellerBankAccount = sellerBankAccount;
	}
	
	public String getRemarks() {
		return remarks;
	}
	
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
	public String getCashier() {
		return cashier;
	}
	
	public void setCashier(String cashier) {
		this.cashier = cashier;
	}
	
	public String getMaker() {
		return maker;
	}
	
	public void setMaker(String maker) {
		this.maker = maker;
	}
	
	public JSONObject getInvoiceUserData() {
		return invoiceUserData;
	}
	
	public void setInvoiceUserData(JSONObject invoiceUserData) {
		this.invoiceUserData = invoiceUserData;
	}
	
	public void addGoodsInfo(Goods goods) {
		if (info == null) {
			info = new ArrayList<>();
		}
		info.add(goods);
	}
	
	@Override
	public String toString() {
		JSONObject param = new JSONObject();
		param.put("order_id", this.getOrderId());
		param.put("card_id", this.getCardId());
		param.put("appid", this.getAppid());
		param.put("openid", this.getWxOpenId());
		param.put("out_user_id", this.getPartyId());
		
		cardExt = new JSONObject();
		userCard = new JSONObject();
		invoiceUserData = new JSONObject();
		
		invoiceUserData.put("fee", this.getFee());
		invoiceUserData.put("title", this.getTitle());
		invoiceUserData.put("billing_time", this.getBillingTime());
		invoiceUserData.put("billing_no", this.getBillingNo());
		invoiceUserData.put("billing_code", this.getBillingCode());
		invoiceUserData.put("info", this.getInfo());
		invoiceUserData.put("fee_without_tax", this.getFeeWithoutTax());
		invoiceUserData.put("tax", this.getTax());
		invoiceUserData.put("s_pdf_media_id", this.getsPdfMediaId());
		invoiceUserData.put("check_code", this.getCheckCode());
		
		cardExt.put("nonce_str", UUIDHelper.getRandomStr());
		cardExt.put("user_card", userCard);
		
		userCard.put("invoice_user_data", invoiceUserData);
		
		param.put("card_ext", cardExt);
		return param.toString();
	}
}