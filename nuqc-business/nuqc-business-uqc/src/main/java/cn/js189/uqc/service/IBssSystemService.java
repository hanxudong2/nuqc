package cn.js189.uqc.service;

/**
 * @Author yxl
 * @Date 2024/3/7
 */

public interface IBssSystemService {
    String qryOfferSpecDetail(String requestJson);
}
