/**************************************************************************************** 

 ****************************************************************************************/
package cn.js189.uqc.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * <Description> <br>
 * .
 * 
 * @author zhangpzh<br>
 * @version 1.0<br>
 */

public final class DateUtils {

	private DateUtils(){}
    
    public static final String NEW_DATE_FORMAT = "yyyyMMdd";
    
    public static final Locale DEFAULT_LOCALE = Locale.CHINA;
	
    private static final Logger LOGGER = LoggerFactory.getLogger(DateUtils.class);

    /** The Constant DEFAULT_DATE_FORMAT. */
    public static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    /** The Constant STRING_DATE_FORMAT. */
    public static final String STRING_DATE_FORMAT = "yyyyMMddHHmmss";
    
    public static final String MKT_DATE_FORMAT = "yyyy.MM.dd HH:mm:ss";

    public static final String YYMMDDHHMM = "yyMMddHHmm";

    public static final String YYYYMMDD = "yyyyMMdd";
	
    /**
     * 
     * Description: <br> 
     *  
     * @author 'lixu'<br>
     * @param dateString 日期字符串
     * @param format 类型
     * @return <br>
     */
    public static Date formateDateString(String dateString, String format) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        Date date = null;
        try {
            date = simpleDateFormat.parse(dateString);
        } catch (ParseException e) {
            LOGGER.debug(e.getMessage());
        }
        return date;
    }

    /**
     * Description: 获取当前时间的字符串 <br>
     * .
     * 
     * @author JSDQ<br>
     * @return <br>
     */
    public static String getNowDateString() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DEFAULT_DATE_FORMAT);
        return simpleDateFormat.format(new Date());
    }

    /**
     * Description: 根据时间输出符合时间格式的字符串<br>
     * 
     * @param date date
     * @param format format
     * @return <br>
     */
    public static String getDateString(Date date, String format) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        return simpleDateFormat.format(date);
    }
    
   public static String getNOWDate(){
       SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
       return sdf.format(new Date());
   }

    /**
     * 获取某天的结束时间
     */
    public static Date getDayEnd(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        return cal.getTime();
    }

    /**
     * 获取指定时间
     */
    public static Date getSpecifiedDate(int dayOffset, int hour, int minute, int second) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH) + dayOffset);
        cal.set(Calendar.HOUR_OF_DAY, hour);
        cal.set(Calendar.MINUTE, minute);
        cal.set(Calendar.SECOND, second);
        return cal.getTime();
    }

    /**
     * 获取指定日期时间
     */
    public static Date getSpecifiedDate(int dayOffset) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH) + dayOffset);
        return cal.getTime();
    }

    public static String formatDate(Date date, String formatter) {
        SimpleDateFormat sdf = new SimpleDateFormat(formatter);
        return sdf.format(date);
    }

    public static long random(long startDate, long endDate) {
        long randomMiles = startDate + (long) (Math.random() * (endDate - startDate));
        if (randomMiles == startDate || randomMiles == endDate) {
            return random(startDate, endDate);
        }
        return randomMiles;
    }

    /**
     * 获取当天结束时间 和 第二天开始1分钟时间的随机时间
     * 两个时间之间取个随机时间作为失效时间 防止所有服务器同时失效
     *
     * @return 返回当前时间距失效时间的秒数
     */
    public static int getExpireSecond(){
        // 当天结束时间
        Date endDate = getDayEnd(new Date());
        // 下一天开始1分钟时间
        Date startDate = getSpecifiedDate(1, 0, 1, 0);
        long endDateTimes = endDate.getTime();
        long startDateTimes = startDate.getTime();
        long randomTimes = random(endDateTimes, startDateTimes);
        // 随机时间
        Date randomDate = new Date(randomTimes);
        // 当前时间距失效时间的秒数
        return (int) ((randomDate.getTime() / 1000) - (new Date().getTime() / 1000));
    }

    /**
     * Date类型转换为10位时间戳
     * @param time 时间
     */
    public static Integer dateToTimestamp(Date time){
        Timestamp ts = new Timestamp(time.getTime());

        return (int) ((ts.getTime())/1000);
    }

    /**
     * @return 当前时间至当天结束时间的秒数
     */
    public static int nowToEndSecond() {
        Date date = new Date();
        long end = getDayEnd(date).getTime();
        long now = date.getTime();
        return (int) ((end - now) / 1000);
    }
    
    /**
     * 开始时间等于月初，结束时间等于月末
     * @param begDate 开始时间
     * @param endDate 结束时间
     * @return true是  false否
     */
    public static boolean isAllMonthFlag(String begDate,String endDate){
        Date date = parseDateTime(NEW_DATE_FORMAT, getDefaultLocal(), begDate);
        String getMonthStartDate = getMonthStart(date, NEW_DATE_FORMAT);
        String getMonthEndDate = getMonthEnd(date, NEW_DATE_FORMAT);
        return (getMonthStartDate.equals(begDate) && getMonthEndDate.equals(endDate));
    }
    
    /**
     * 使用指定的格式和地域解析字段串为时间
     *
     * @param format
     *            格式
     * @param locale
     *            地域
     * @param str
     *            字段串
     */
    public static Date parseDateTime(String format, Locale locale, String str) {
        SimpleDateFormat sdf = new SimpleDateFormat(format, locale);
        try {
            return sdf.parse(str);
        } catch (ParseException e) {
            LOGGER.debug(e.getMessage());
            return null;
        }
    }
    
    /**
     * 使用指定格式取指定时间的月首
     */
    public static String getMonthStart(Date date, String format) {
        Calendar localTime = Calendar.getInstance(getDefaultLocal());
        localTime.setTime(date);
        localTime.set(Calendar.DAY_OF_MONTH, 1);
        return formatDateTime(localTime, format, getDefaultLocal());
    }
    
    
    /**
     * 格式化日期时间
     *
     * @param calendar
     *            时间
     * @param format
     *            格式
     * @param locale
     *            地域
     */
    public static String formatDateTime(Calendar calendar, String format,
                                        Locale locale) {
        SimpleDateFormat sdf = new SimpleDateFormat(format, locale);
        return sdf.format(calendar.getTime());
    }
    
    public static Locale getDefaultLocal() {
        return DEFAULT_LOCALE;
    }
    
    /**
     * 使用指定格式取指定时间的月末
     */
    public static String getMonthEnd(Date date, String format) {
        Calendar localTime = Calendar.getInstance(getDefaultLocal());
        localTime.setTime(date);
        localTime.add(Calendar.MONTH, 1);
        localTime.set(Calendar.DAY_OF_MONTH, 1);
        localTime.add(Calendar.DAY_OF_MONTH, -1);
        return formatDateTime(localTime, format, getDefaultLocal());
    }
    
    
}
