package cn.js189.uqc.transform.impl;

import cn.js189.uqc.transform.IResponseParamTransform;
import cn.js189.uqc.util.BSSHelper;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class QryCustomerTransform implements IResponseParamTransform {

	// 日志组件
		private static final Logger LOGGER = LoggerFactory.getLogger(QryCustomerTransform.class);
		
		@Override
		public String transform(JSONObject responseJSON) {
			JSONObject res = new JSONObject();
			JSONObject body = new JSONObject();
			body.put("resultMsg", "成功");
			body.put("resultCode", "0");
			body.put("resultObject", responseJSON);
			
			JSONObject head = BSSHelper.getRspInfo("00","success");
			res.put("head", head);
			res.put("body", body);
			LOGGER.debug("查询客户信息,参数转换:"+res);
			return res.toString();
		}

		//日期转换
		public String dateChange(String date) {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			SimpleDateFormat formats = new SimpleDateFormat("yyyy-MM-dd");
			String newDate = "";
			try {
				Date dateTime = format.parse(date);
				newDate = formats.format(dateTime);
				System.out.println(newDate);
			} catch (ParseException e) {
				LOGGER.debug("日期转换失败");
			}
			return newDate;
		}
		
}
