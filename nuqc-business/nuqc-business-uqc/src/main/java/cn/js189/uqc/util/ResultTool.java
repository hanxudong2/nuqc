/**    
 * @{#} ResultTool.java Create on 2011-9-26 上午09:34:48    
 *    
 * Copyright (c) 2011 by 创发科技.    
 */
package cn.js189.uqc.util;

import cn.js189.common.enums.CodeEnum;
import org.apache.commons.lang.StringUtils;


/**
 * @author <a href="h124436797@yahoo.com.cn">hrg</a>
 * @version 1.0
 */

public class ResultTool {
	private ResultTool(){}
	
	public static String transfarBsnResult(String result) {
		String code = "";
		if (StringUtils.isNotBlank(result)) {
			if ("-99999".equals(result)) {
				code = CodeEnum.CT_50001.name();
				// "连接不上";
			} else if ("-11111".equals(result)) {
				code = CodeEnum.CT_50005.name();
				// 超时;
			} else if ("-22222".equals(result)) {
				code = CodeEnum.CT_60005.name();
			} else if ("-1".equals(result)) {
				code = CodeEnum.CT_50004.name();
			} else if ("-90001".equals(result)) {
				code = CodeEnum.CT_50007.name();
			} else if ("-90002".equals(result)) {
				code = CodeEnum.CT_50008.name();
			} else if ("-88888".equals(result)) {
				code = CodeEnum.CT_50006.name();
			} else {
				code = result;
			}

		} else {
			code = CodeEnum.CT_60005.name();
		}

		return code;
	}
}
