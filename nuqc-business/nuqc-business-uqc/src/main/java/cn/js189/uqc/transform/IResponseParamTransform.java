package cn.js189.uqc.transform;

import com.alibaba.fastjson.JSONObject;

/**
 * 智慧BSS，转换老接口出参
 * 
 * @author Mid
 */
public interface IResponseParamTransform {
	
	/**
	 * 参数转换
	 * @param respParam:智慧BSS返回JSON
	 * @return 结果
	 */
	String transform(JSONObject respParam);
}
