/**************************************************************************************** 
        湖南创发科技有限责任公司
 ****************************************************************************************/
package cn.js189.uqc.common;

import cn.js189.uqc.domain.ChannelInfo;
import cn.js189.uqc.mapper.ChannelInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**
 *订单域公共服务类
 */
@Service("commonService")
public class CommonService {

    /**
     * 渠道信息Mapper
     */
    @Resource
    private ChannelInfoMapper channelInfoMapper;

    /**
     * Description: 根据渠道id查询渠道信息<br> 
     *  
     * @author liuzg<br>
     * @taskId <br>
     * @param channelId <br>
     * @return <br>
     */
    public ChannelInfo queryChannelInfo(String channelId) {
        return channelInfoMapper.selectByChannelId(channelId);
    }
   
}
