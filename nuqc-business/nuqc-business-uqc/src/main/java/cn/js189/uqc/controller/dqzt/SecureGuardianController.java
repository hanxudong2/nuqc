package cn.js189.uqc.controller.dqzt;

import cn.js189.uqc.service.ISecureGuardian;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: xxb
 * @description:
 * @author: jzd
 * @create: 2024/3/4 16:41
 **/
@RestController
public class SecureGuardianController {

    @Autowired
    ISecureGuardian secureGuardian;


    /**
     * @Description uuid=3.1 查询订购信息
     * @Param requestJSON
     * @Return {@link JSONObject}
     * @Author jzd
     * @Date 2024/3/4 16:42
     */
    @PostMapping(value = "/iSecureGuardian/queryOrderInfo", produces = "application/json;charset=UTF-8")
    JSONObject queryOrderInfo(@RequestBody JSONObject requestJSON){
        return secureGuardian.queryOrderInfo(requestJSON);
    }

    /**
     * Description:uuid=3.3 绑定申请，解绑 <br>
     *
     * @author shenfan<br>
     * @taskId <br>
     * @param requestJSON 入参
     * @return <br>
     */
    @PostMapping(value = "/iSecureGuardian/bindAndUnbund", produces = "application/json;charset=UTF-8")
    public JSONObject bindAndUnbund(@RequestBody JSONObject requestJSON){
        return secureGuardian.bindAndUnbund(requestJSON);

    }

    /**
     * Description:uuid=3.6 上网管理开关设置 <br>
     *
     * @author shenfan<br>
     * @taskId <br>
     * @param requestJSON 入参
     * @return <br>
     */
    @PostMapping(value = "/iSecureGuardian/onlineManageSetup", produces = "application/json;charset=UTF-8")
    public JSONObject onlineManageSetup(@RequestBody JSONObject requestJSON){
        return secureGuardian.queryOrderInfo(requestJSON);
    }

    /**
     * Description:uuid=3.7 上网管理开关查询 <br>
     *
     * @author shenfan<br>
     * @taskId <br>
     * @param requestJSON 入参
     * @return <br>
     */
    @PostMapping(value = "/iSecureGuardian/qryOnlineSetupState", produces = "application/json;charset=UTF-8")
    public JSONObject qryOnlineSetupState(@RequestBody JSONObject requestJSON){
        return secureGuardian.queryOrderInfo(requestJSON);
    }

    /**
     * Description:uuid=3.8 阻断上网时间段设置 <br>
     *
     * @author shenfan<br>
     * @taskId <br>
     * @param requestJSON 入参
     * @return <br>
     */
    @PostMapping(value = "/iSecureGuardian/blkOnlineTimeSetup", produces = "application/json;charset=UTF-8")
    public JSONObject blkOnlineTimeSetup(@RequestBody JSONObject requestJSON){
        return secureGuardian.queryOrderInfo(requestJSON);
    }

    /**
     * Description:uuid=3.9 阻断上网时间段查询 <br>
     *
     * @author shenfan<br>
     * @taskId <br>
     * @param requestJSON 入参
     * @return <br>
     */
    @PostMapping(value = "/iSecureGuardian/qryBlkOnlineTimeSetup", produces = "application/json;charset=UTF-8")
    public JSONObject qryBlkOnlineTimeSetup(@RequestBody JSONObject requestJSON){
        return secureGuardian.queryOrderInfo(requestJSON);
    }


    /**
     * Description:uuid=3.16 防护日志查询 <br>
     *
     * @author shenfan<br>
     * @taskId <br>
     * @param requestJSON 入参
     * @return <br>
     */
    @PostMapping(value = "/iSecureGuardian/queryGuardLog", produces = "application/json;charset=UTF-8")
    public JSONObject queryGuardLog(@RequestBody JSONObject requestJSON){
        return secureGuardian.queryOrderInfo(requestJSON);
    }
}
