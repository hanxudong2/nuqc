package cn.js189.uqc.common;

import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 通过custId的方式查询客户信息，生成请求报文
 * 
 * @author Mid
 */
public class QryCustomerByCustId extends QryCustomerBase {
	// 日志组件
	private static final Logger LOGGER = LoggerFactory.getLogger(QryCustomerByCustId.class);

	//接入类产品实例标识custId
	private String custId;

	public QryCustomerByCustId(String regionId, String uncompletedFlag, String custId) {
		super(regionId, uncompletedFlag);
		this.custId = custId;
	}

	@Override
	protected JSONObject improveRequestObject(JSONObject requestObject) {
		requestObject.put("custId", custId);
		LOGGER.debug("requestObject:{}", requestObject);
		return requestObject;
	}
	
}
