package cn.js189.uqc.domain.qry;

import cn.js189.uqc.common.RequestParamBase;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class QryChannelBase extends RequestParamBase {

    private static final Logger LOGGER = LoggerFactory.getLogger(QryChannelBase.class);

    private static final String BSS_TCP_CONT_SVC_CODE = "js.intf.qryChannel";

    // 地区标识
    protected String regionId;

    public QryChannelBase(String regionId) {
        super();
        this.regionId = regionId;
    }

    @Override
    protected JSONObject generateTcpCont() {
        return super.generateTcpCont(BSS_TCP_CONT_APP_KEY, BSS_TCP_CONT_SVC_CODE, null);
    }

    @Override
    protected JSONObject generateSvcCont() {
        JSONObject authenticationInfo = generateAuthenticationInfo();
        if (null == authenticationInfo) {
            LOGGER.error("generate authenticationInfo failure");
            return null;
        }

        JSONObject requestObject = generateRequestObject();
        if (null == requestObject) {
            LOGGER.error("generate requestObject failure");
            return null;
        }

        JSONObject svcCont = new JSONObject();
        svcCont.put("authenticationInfo", authenticationInfo);
        svcCont.put("requestObject", requestObject);
        return svcCont;
    }

    /**
     * 生成 svcCont 下的认证信息
     *
     * @return
     */
    protected JSONObject generateAuthenticationInfo() {
        return super.generateSvcContAuthenticationInfo(regionId, null, null, null);
    }

    /**
     * 生成 svcCont 下的请求对象
     *
     * @return
     */
    protected JSONObject generateRequestObject() {
        JSONObject requestObject = new JSONObject();
        requestObject.put("regionId", regionId);
        return improveRequestObject(requestObject);
    }
    
    /**
     * 补齐 svcCont 下的请求对象
     *
     * @param requestObject:svcCont下的请求对象
     * @return
     */
    protected abstract JSONObject improveRequestObject(JSONObject requestObject);
}
