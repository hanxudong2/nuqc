package cn.js189.uqc.mapper;


import cn.js189.uqc.domain.PubAddressInfo;

public interface PubAddressIpMapper {

    /**
     * Description: 根据编码，查询http，入访地址<br>
     *
     */
    public String getActionUrl(String actionCode);

    /**
     * Description: 根据编码，查询http，无端口号的入访地址<br>
     *
     */
    public String getActionUrlNew(String actionCode);

    public String selectSwitchInfo();


    /**
     * Description: 根据编码查询配置信息<br>
     */
    public PubAddressInfo getPubAddressInfo(String actionCode);


}
