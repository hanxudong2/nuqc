package cn.js189.uqc.common;

import cn.js189.common.util.StringUtils;
import com.alibaba.fastjson.JSONObject;

public class CheckServSpecByDevModel extends CheckServSpecByDevModelBase{
	
	//地区CODE
	private String areaCode;
	//CRM服务规格ID
	private String servSpecId;
	//UIM卡号
	private String terminalCode;

	
	public CheckServSpecByDevModel(String areaCode, String servSpecId, String terminalCode) {
		super();
		this.areaCode = areaCode;
		this.servSpecId = servSpecId;
		this.terminalCode = terminalCode;
	}
	
	@Override
	protected JSONObject improveRequestObject(JSONObject requestObject) {
		if(!StringUtils.isEmpty(areaCode)){
    		requestObject.put("areaCode", areaCode);
    	}
		if(!StringUtils.isEmpty(servSpecId)){
    		requestObject.put("servSpecId", servSpecId);
    	}
		if(!StringUtils.isEmpty(terminalCode)){
    		requestObject.put("terminalCode", terminalCode);
    	}
		return requestObject;
	}
}
