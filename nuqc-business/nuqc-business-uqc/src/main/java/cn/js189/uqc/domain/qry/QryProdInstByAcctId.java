package cn.js189.uqc.domain.qry;

import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 通过接入号的方式查询产品信息，生成请求报文
 * 
 * @author Mid
 */
public class QryProdInstByAcctId extends QryProdInstBase {
	// 日志组件
	private static final Logger LOGGER = LoggerFactory.getLogger(QryProdInstByAcctId.class);

	// 账户id
	private String acctId;


	public QryProdInstByAcctId(String regionId, String uncompletedFlag, String acctId) {
		super(regionId, uncompletedFlag);
		this.acctId = acctId;
	}

	@Override
	protected JSONObject improveRequestObject(JSONObject requestObject) {
		requestObject.put("acctId", acctId);
		LOGGER.debug("requestObject:{}", requestObject.toString());
		return requestObject;
	}
}
