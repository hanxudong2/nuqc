package cn.js189.uqc.controller.css;

import cn.js189.uqc.service.IAreaBillService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;

@RestController
@RequestMapping
public class CssController {
	@Resource
	private IAreaBillService areaBillService;
	
	/**
	 * 根据产品实例查询归属信息接口协议
	 * @param reqParams 参数
	 * @return 结果
	 */
	@PostMapping("/iAreaBill/getProdExtendInfo")
	public String getProdExtendInfo(@RequestBody String reqParams){
		return areaBillService.getProdExtendInfo(reqParams);
	}

}
