package cn.js189.uqc.mapper;

import cn.js189.uqc.domain.ArpuInfo;
import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import java.util.Map;

/**
 * 查询arpu信息
 */
@Mapper
public interface ArpuInfoMapper {
	
	/**
	 *查询总数
	 */
	int getArpuInfoCount();
	
	
	/**
	 * 分页获取数据
	 */
	List<ArpuInfo> getArpuInfo(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize);
	
	/**
	 * 根据号码查arpu
	 */
	@MapKey("arpu")
	Map<String, String> getArpuInfoByPhoneNbr(String phoneNbr);
	
}
