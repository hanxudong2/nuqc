package cn.js189.uqc.domain.qry;

import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 通过接入号查询Puk码，生成请求报文
 * 
 * @author Mid
 */
public class QryPukByAccNum extends QryPukBase{

	// 日志组件
	private static final Logger LOGGER = LoggerFactory.getLogger(QryPukByAccNum.class);

	// 接入号码
	private String accNum;
	
	// 接入号码类型
	private String prodId;

	public QryPukByAccNum(String regionId, String accNum, String prodId) {
		super(regionId);
		this.accNum = accNum;
		this.prodId = prodId;
	}

	@Override
	protected JSONObject improveRequestObject(JSONObject requestObject) {
		requestObject.put("accNum", accNum);
		requestObject.put("prodId", prodId);
		LOGGER.debug("requestObject:{}", requestObject.toString());
		return requestObject;
	}
}
