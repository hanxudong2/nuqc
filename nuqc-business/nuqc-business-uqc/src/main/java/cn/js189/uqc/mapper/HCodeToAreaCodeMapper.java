package cn.js189.uqc.mapper;

import cn.js189.uqc.domain.HCode;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface HCodeToAreaCodeMapper {
	
	/**
	 *
	 * 查询总数
	 */
	int getHCodeCount();
	
	/**
	 * 分页获取HCode数据库数据
	 */
	List<HCode> getHCodeInfo(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize);

}
