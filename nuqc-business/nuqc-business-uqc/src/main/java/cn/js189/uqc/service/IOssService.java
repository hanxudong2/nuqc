package cn.js189.uqc.service;

/**
 * @program: xxb
 * @description:
 * @author: jzd
 * @create: 2024/3/4 14:20
 **/
public interface IOssService {

    /**
     * JSOSS政企门户业务进度透明化查询
     * @param requestJSON
     * @return
     */

    String getCurrentTachInfoToCrm(String requestJSON);


    /**
     * JSOSS根据购物车判断是否分解成功
     * @param requestJSON
     * @return
     */

    String getODByCustOrderCode(String requestJSON);
}
