/**    
 * @{#} MsgTool.java Create on 2011-4-8 上午09:57:00    
 *    
 * Copyright (c) 2010 by 创发科技.    
 */    
package cn.js189.uqc.util;

import cn.js189.common.util.MainUtils;
import org.apache.commons.lang.StringUtils;


/**
 * @author <a href="h124436797@yahoo.com.cn">hrg</a>   
 * @version 1.0
 * Description:消息工具    
 */

public final class MsgTool {
	
	private MsgTool(){}
		
	/**
	 * Description:返回提示语
	 * @param code
	 * @return
	 */
	public static String getMsg(String code){
		String msg="";
		if(StringUtils.isNotBlank(code)){
			int c= MainUtils.getInt(code);
			switch (c) {
			case 0:
				msg="成功！";
				break;
			case -99999:
				msg="您好！通讯连接出现未知错误，请稍后再试...("+code+")";
				break;
			case -11111:
				msg="您好！通讯连接出现未知错误，请稍后再试...("+code+")";
				break;
			case 1:
				msg="您好！未查到信息。";
				break;
			case 100000:
				msg="您好！系统忙，请稍后再试...("+code+")";
		    break;
			case 100001:
				msg="您好！未查到信息，请您选择查询条件...("+code+")";
		    break;
			case 900807:
			     msg="您好！查询话单失败，请稍后再试...("+code+")";
			break;
			case 201016:
			   msg="您好！未查到用户信息...("+code+")";
			break;
			case 7000005:
				   msg="您好！未查到话单信息...("+code+")";
				break;
			case 602004:
				   msg="您好！未查到付款记录...("+code+")";
				break;
			case 901407:
				   msg="您好！查询操作失败,查询付款信息失败...("+code+")";
				break;
			case 905012:
				   msg="您好！非预付费用户不允许查询实时详单("+code+")";
				break;
			case 903016:
				   msg="您好！查询日志记录出错...("+code+")";
				break;
			case 201013:
				   msg="您好！无帐单信息...("+code+")";
				break;
			case 903017:
				   msg="您好！无可用套餐信息...("+code+")";
				break;
			case 903018:
				   msg="您好！查询操作失败,套餐信息查询出错...("+code+")";
				break;
			case 910000:
				   msg="您好！未查到帐单信息...("+code+")";
				break;
			case 910001:
				   msg="您好！查询操作失败,查询成员号码时出错...("+code+")";
				break;
			case 910002:
				   msg="您好！查询操作失败，缺少成员信息...("+code+")";
				break;
			case 910003:
				   msg="您好！查询操作失败...("+code+")";
				break;
			case 910004:
				   msg="您好！查询操作失败,检查套餐用户时出错...("+code+")";
				break;
			case 910005:
				   msg="您好！查询操作失败,找不到套餐用户...("+code+")";
				break;
			case 910006:
				msg="您好！查询操作失败,查询系统时间出错...("+code+")";
				break;
			case 910007:
				msg="您好！查询操作失败,查询积分信息出错...("+code+")";
				break;
			case 910008:
				msg="您好！查询操作失败,找不到积分信息...("+code+")";
				break;
			case 910009:
				 msg="您好！查询操作失败,查询帐单信息出错...("+code+")";
				 break;
			case -22222:
				msg="您好！通讯连接出现未知错误，请稍后再试...("+code+")";
				break;
			case -1:
				msg="您好！通讯连接出现未知错误，请稍后再试...("+code+")";
				break;
			case -317067262:
			    msg="没有话费";
			    break;
			default:
				msg="您好！通讯连接出现未知错误，请稍后再试...("+code+")";
			
				break;
			}
		}else{
			msg="您好！系统错误，请稍后再试...";
		}
		
		return msg;
	}
	
	
	
	/**
	 * Description:充值消息
	 * @param code
	 * @return
	 */
	public static String getOthMsg(String code){
		String msg="";
		if(StringUtils.isNotBlank(code)){
			int c=MainUtils.getInt(code);
			switch (c) {
			case 0:
				msg="成功！";
				break;
				case -11111:
				msg="您好！通讯连接出现未知错误，请稍后再试...("+code+")";
				break;
			case 905071:
				msg="您好！未知充值来源。";
				break;
			case 905072:
				msg="您好！根据充值来源、服务名取充值来源信息错误...("+code+")";
		    break;
			case 905073:
				msg="您好！充值来源认证口令错误...("+code+")";
		    break;
			case 905074:
			     msg="您好！充值来源为不在用状态...("+code+")";
			break;
			case 905081:
			   msg="您好！根据订单号取相关交易记录信息出错...("+code+")";
			break;
			case 905082:
				   msg="您好！无销帐流水、付款流水相关交易记录...("+code+")";
				break;
			case 905083:
				   msg="您好！根据销帐流水、付款流水取相关交易记录信息出错...("+code+")";
				break;
			case 905084:
				   msg="您好！修改交易记录信息错误...("+code+")";
				break;
			case 905085:
				   msg="您好！插入交易记录信息记录表出错("+code+")";
				break;
			case 905086:
				   msg="您好！传入的销帐流水或付款流水错误...("+code+")";
				break;
			case 905087:
				   msg="您好！错误的交易记录状态...("+code+")";
				break;
			case 905088:
				   msg="您好！无订单号相关交易记录...("+code+")";
				break;
			case 905089:
				   msg="您好！订单交易类型错误...("+code+")";
				break;
			case 905090:
				   msg="您好！交易来源不符合，无法对非本来源交易做操作...("+code+")";
				break;
			case 905091:
				   msg="您好！充值来源的交易流水在本地已经有对应的交易记录...("+code+")";
				break;
			case 905092:
				   msg="您好！检查充值来源的交易流水出错...("+code+")";
				break;
			case -22222:
				msg="您好！通讯连接出现未知错误，请稍后再试...("+code+")";
				break;
			case -1:
				msg="您好！通讯连接出现未知错误，请稍后再试...("+code+")";
				break;
				case -99999:
				default:
				msg="您好！通讯连接出现未知错误，请稍后再试...("+code+")";
			
				break;
			}
		}else{
			msg="您好！系统错误，请稍后再试...";
		}
		
		return msg;
	}
	
	
}
  