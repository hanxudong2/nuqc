package cn.js189.uqc.domain.qry;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang.StringUtils;

public class QryStaff extends QryStaffBase {

    private String staffCode;

    private String staffAccount;

    private JSONArray staffIds;

    public QryStaff(String regionId, String staffCode, String staffAccount, JSONArray staffIds) {
        super(regionId);
        this.staffCode = staffCode;
        this.staffAccount = staffAccount;
        this.staffIds = staffIds;
    }


    @Override
    protected JSONObject improveRequestObject(JSONObject requestObject) {
        if (!StringUtils.isEmpty(staffCode)) {
            requestObject.put("staffCode", staffCode);
            return requestObject;
        }
        if (!StringUtils.isEmpty(staffAccount)) {
            requestObject.put("staffAccount", staffAccount);
            return requestObject;
        }
        if (staffIds != null && !staffIds.isEmpty()) {
            requestObject.put("staffIds", staffIds);
            return requestObject;
        }
        return requestObject;
    }

}
