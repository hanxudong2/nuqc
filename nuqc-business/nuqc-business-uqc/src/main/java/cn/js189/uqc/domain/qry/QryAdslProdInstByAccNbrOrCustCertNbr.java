package cn.js189.uqc.domain.qry;

import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * 
 * 
 */
public class QryAdslProdInstByAccNbrOrCustCertNbr extends QryAdslProdInstBase {
	// 日志组件
	private static final Logger LOGGER = LoggerFactory.getLogger(QryAdslProdInstByAccNbrOrCustCertNbr.class);


	public QryAdslProdInstByAccNbrOrCustCertNbr(String regionId, String accNum, String custCertNbr) {
		super(regionId,accNum,custCertNbr);
	}

	@Override
	protected JSONObject improveRequestObject(JSONObject requestObject) {
		return requestObject;
	}
	
}
