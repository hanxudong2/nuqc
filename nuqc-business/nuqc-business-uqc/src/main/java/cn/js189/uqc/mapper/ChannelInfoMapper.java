package cn.js189.uqc.mapper;


import cn.js189.uqc.domain.ChannelInfo;

/**
 * <Description> <br> 
 *  
 * @author X<br>
 * @version 1.0<br>
 * @taskId <br>
 * @CreateDate 2017年2月9日 <br>
 */
public interface ChannelInfoMapper {
    
    /**
     * Description: 根据渠道编号查询渠道信息<br> 
     *  
     * @author X<br>
     * @taskId <br>
     * @param channelNbr String
     * @return <br>
     */ 
    ChannelInfo selectByChannelNbr(String channelNbr);

    /**
     * Description: 根据渠道id查询渠道信息<br> 
     *  
     * @author X<br>
     * @taskId <br>
     * @param channelId String
     * @return <br>
     */ 
    ChannelInfo selectByChannelId(String channelId);
    
    /**
     * Description: 根据渠道id查询渠道sign<br> 
     *  
     * @author zhouchao<br>
     * @taskId <br>
     * @param channelId String
     * @return <br>
     */ 
    String getSignByChannelId(String channelId);
    
    /**
     * Description: 更新key簽名<br> 
     *  
     * @author zhouchao<br>
     * @param channelId String
     * @return <br>
     */
    int updateAuthKeyByChannelId(String sign);
}