package cn.js189.uqc.service;

public interface IBillService {

    /**
     * 线上销户余额查询接口
     * @param jsonParam 入参
     * @author lj
     * @return
     */
    String queryBalance(String jsonParam);

    /**
     * 线上销户余额提取或者结转进度
     * @param jsonParam
     * @return
     */
    String queryBalanceProgress(String jsonParam);

    /**
     * @Description 三期一致性累积量
     * @Param
     * @Author xzy
     * @Date 2024/3/4 10:01
     */
    String accuUseQry(String jsonParam);


    /**
     *
     * Description:费用清单<br>
     * @param
     * @Author xzy
     * @Date 2024/3/4 14:01
     */
    String callFeeBillQuery(String jsonParam);

    /**
     *
     * Description:账单查询<br>
     * @param
     * @Author xzy
     * @Date 2024/3/4 14:01
     */
    String callNewBillReq(String jsonParam);

    /**
     *
     * Description:长话话单信息-10171 <br>
     * @author xzy<br>
     * @param jsonParam 入参
     * @Date 2024/3/4 15:40
     */
    String callNewCallTicket(String jsonParam);

    /**
     *
     * Description:无线宽带上网清单<br>
     * @param
     * @Author xzy
     * @Date 2024/3/4 14:01
     */
    String callNewDataTicketQrReq(String jsonParam);

    /**
     *
     * Description:新业务清单<br>
     * @param
     * @Author xzy
     * @Date 2024/3/4 14:01
     */
    String callNewHttpSvrTicQr(String jsonParam);


    /**
     *
     * Description:互联星空清单-16109<br>
     * @param
     * @Author xzy
     * @Date 2024/3/4 15:30
     */
    String callNewInfoDataQr(String jsonParam);

    /**
     *
     * Description:电话清单查询<br>
     * @param
     * @Author xzy
     * @Date 2024/3/4 15:30
     */
    String callNewMessageTicketQr(String jsonParam);

    /**
     *
     * Description:窄带清单<br>
     * @param
     * @Author xzy
     * @Date 2024/3/4 15:30
     */
    String callNewNarrowDataQr(String jsonParam);

    /**
     *
     * Description:欠费查询<br>
     * @param
     * @Author xzy
     * @Date 2024/3/4 15:30
     */
    String callNewOthGetOwe(String jsonParam);

    /**
     *
     * Description:C网短信/彩信增值详单<br>
     * @param
     * @Author xzy
     * @Date 2024/3/4 15:30
     */
    String callNewSmsValueTicketQr(String jsonParam);

    /**
     *
     * Description:bsn短信详单查询-10135<br>
     * @param
     * @Author xzy
     * @Date 2024/3/4 15:30
     */
    String callNewSmTicketQr(String jsonParam);

    /**
     *
     * Description:其他增值业务清单<br>
     * @param
     * @Author xzy
     * @Date 2024/3/4 15:30
     */
    String callNewValueTicketQr(String jsonParam);

    /**
     *
     * Description:手机语音清单<br>
     * @param
     * @Author xzy
     * @Date 2024/3/4 15:30
     */
    String callNewVoiceTicket(String jsonParam);

    /**
     *
     * Description:查询语音，短信，流量，增值清单<br>
     * @param
     * @Author xzy
     * @Date 2024/3/4 15:30
     */
    String queryBillItems(String jsonParam);

}
