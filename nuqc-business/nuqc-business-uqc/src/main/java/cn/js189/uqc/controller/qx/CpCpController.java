package cn.js189.uqc.controller.qx;

import cn.js189.uqc.service.ICpCpService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping
public class CpCpController {
	
	@Resource
	private ICpCpService iCpCpService;
	
	/**
	 * 企信销售品互斥依赖关系配置查询
	 * @param reqParams 参数
	 * @return 结果
	 */
	@PostMapping("/iBssSystem/cpcpQryOfferGrpRela")
	public String cpCpQryOfferGrpRela(@RequestBody String reqParams){
		return iCpCpService.cpCpQryOfferGrpRela(reqParams);
	}
	
	/**
	 * cpcp根据规格查询销售品列表
	 * @param reqParams 参数
	 * @return 结果
	 */
	@PostMapping("/iBssSystem/cpcpQryOfferList")
	public String cpCpQryOfferList(@RequestBody String reqParams){
		return iCpCpService.cpCpQryOfferList(reqParams);
	}
	
}
