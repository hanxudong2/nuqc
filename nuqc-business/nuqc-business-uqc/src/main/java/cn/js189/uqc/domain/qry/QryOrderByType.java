package cn.js189.uqc.domain.qry;

import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class QryOrderByType extends QryOrderByTypeBase {

    // 日志组件
    private static final Logger LOGGER = LoggerFactory.getLogger(QryOrderByType.class);

    private String custId;//客户标识
    private String prodInstId;//产品实例标识
    private String accNum;//远程签名业务号码
    private String isSendAutograph;//返回已发起远程签名，但未完成签名的购物车

    /**
     * 查询产品信息构造器
     *
     * @param regionId :地区标识
     */
    public QryOrderByType(String regionId) {
        super(regionId);
    }

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public String getProdInstId() {
        return prodInstId;
    }

    public void setProdInstId(String prodInstId) {
        this.prodInstId = prodInstId;
    }

    public String getAccNum() {
        return accNum;
    }

    public void setAccNum(String accNum) {
        this.accNum = accNum;
    }

    public String getIsSendAutograph() {
		return isSendAutograph;
	}

	public void setIsSendAutograph(String isSendAutograph) {
		this.isSendAutograph = isSendAutograph;
	}

	@Override
    protected JSONObject improveRequestObject(JSONObject requestObject) {
        requestObject.put("custId", custId);
        requestObject.put("prodInstId", prodInstId);
        requestObject.put("accNum", accNum);
        requestObject.put("isSendAutograph", isSendAutograph);
        LOGGER.debug("requestObject:{}", requestObject.toString());
        return requestObject;
    }
}
