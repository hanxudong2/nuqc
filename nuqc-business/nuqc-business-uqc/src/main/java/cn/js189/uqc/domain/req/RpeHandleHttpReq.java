package cn.js189.uqc.domain.req;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang.StringUtils;

public class RpeHandleHttpReq extends RpeHandleHttpReqBase{
	//流程id
	private String procId;
	//流程名
	private String procName;
	//工号
	private String staffCode;
	//时间
	private String createDate;
	//违约金试算信息
	private JSONObject data;
	
	public RpeHandleHttpReq(String regionId, String procId, String procName, String staffCode, String createDate, JSONObject data){
		super(regionId);
		this.procId = procId;
		this.procName = procName;
		this.staffCode = staffCode;
		this.createDate = createDate;
		this.data = data;
	}

	@Override
	protected JSONObject improveRequestObject(JSONObject requestObject) {
		if(!StringUtils.isEmpty(procId)){
    		requestObject.put("procId", procId);
    	}
		if(!StringUtils.isEmpty(procName)){
    		requestObject.put("procName", procName);
    	}
		if(!StringUtils.isEmpty(staffCode)){
    		requestObject.put("staffCode",staffCode );
    	}
		if(!StringUtils.isEmpty(createDate)){
    		requestObject.put("createDate",createDate );
    	}
		if(null != data && !data.isEmpty()){
    		requestObject.put("data", data);
    	}
		return requestObject;
	}
}
