package cn.js189.uqc.service;

import com.alibaba.fastjson.JSONObject;

/**
 * 查询用户相关信息
 */
public interface IBssUserQueryService {
	
	/**
	 * 限购策略校验
	 * @param reqParams 参数
	 * @return 结果
	 */
	JSONObject checkPurchaseRestrictions(String reqParams);
	
	/**
	 * 拦截校验接口
	 * @param reqParams 参数
	 * @return 结果
	 */
	JSONObject checkRcBlackList(String reqParams);
	
	/**
	 * 证件查询接口
	 * @param reqParams 入参
	 * @return 结果
	 */
	String documentQuery(String reqParams);
	
	/**
	 * 活动详情查询接口
	 * @param reqParams 参数
	 * @return 结果
	 */
	String getActivityInfo(String reqParams);
	
	/**
	 * http://132.252.111.235:15005/services/LcimsForUserInfo?wsdl
	 * 用户认证失败记录查询接口
	 * @param reqParams 参数
	 * @return 结果
	 */
	String getAuthFail(String reqParams);
	
	/**
	 * http://jsjteop.telecomjs.com:8764/jseop/manager/api/getCustManagerbyProdId
	 * 根据产品id获取客户京信息
	 * @param reqParams 参数
	 * @return 结果
	 */
	String getCustomManagerByProdId(String reqParams);
	
	/**
	 * http://10.251.27.227:31038/osa/interface
	 * 根据产品id查询客户经理信息
	 * @param reqParams 参数
	 * @return 结果
	 */
	String getGMPropertyRecords(String reqParams);
	
	/**
	 * https://jseop.telecomjs.com/eop/crmservices/js.intf.qryStaff?appkey=js_dzqd
	 * 查询员工信息
	 * @param reqParams 参数
	 * @return 结果
	 */
	String queryStaffInfo(String reqParams);
	
	/**
	 * https://jseop.telecomjs.com/eop/crmservices/js.intf.qryChannel?appkey=js_dzqd
	 * 查询渠道信息
	 * @param reqParams 参数
	 * @return 结果
	 */
	String qryChannel(String reqParams);
	
	/**
	 * https://jseop.telecomjs.com/eop/crmservices/js.intf.rpeHandleHttpReq?appkey=js_dzqd
	 * 违约金试算
	 * @param reqParams 参数
	 * @return 结果
	 */
	String rpeHandleHttpReq(String reqParams);
	
	/**
	 * https://jseop.telecomjs.com/eop/crmservices/js.intf.qryCustomer?appkey=js_dzqd
	 * 订单算费（费用明细）
	 * @param reqParams 参数
	 * @return 结果
	 */
	String calChargeDetailInfo(String reqParams);
	
	/**
	 * http://132.252.111.235:15005/services/LcimsForUserInfo?wsdl
	 * 获取用户信息
	 * @param reqParams 参数
	 * @return 结果
	 */
	String getUserInfo(String reqParams);
	
	/**
	 * 根据卡号查询用户号码接口
	 * @param reqParams 参数
	 * @return 结果
	 */
	String getNumInfoByCardNo(String reqParams);
	
	/**
	 * 获取签名
	 */
	String getSign();
	
	/**
	 * http://132.252.111.235:15005/services/LcimsForUserInfo?wsdl
	 * 用户上网记录查询接口
	 * @param reqParams 参数
	 * @return 结果
	 */
	String getUserDet(String reqParams);
	
	/**
	 * http://132.252.111.235:15005/services/LcimsForUserInfo?wsdl
	 * 用户在线查询接口
	 * @param reqParams 参数
	 * @return 结果
	 */
	String getUserOnline(String reqParams);
	
	/**
	 * https://jseop.telecomjs.com/eop/crmservices/js.intf.qryCustlevelAndKeyPerson?appkey=js_dzqd
	 * 查询政企客户等级、关键人联系人
	 * @param reqParams 参数
	 * @return 结果
	 */
	String qryCustlevelAndKeyPerson(String reqParams);
	
	/**
	 * https://jseop.telecomjs.com/eop/crmservices/js.intf.qryCustomer?appkey=js_dzqd
	 * 查询客户
	 * @param reqParams 参数
	 * @return 结果
	 */
	String qryCustomerWithNoFilter(String reqParams);
	
	/**
	 * https://jseop.telecomjs.com/eop/crmservices/js.intf.qryCustomerOrderByTypeOld?appkey=js_dzqd
	 * 查询客户未签名购物车
	 * @param reqParams 参数
	 * @return 结果
	 */
	String qryCustomerOrderByType(String reqParams);
	
	/**
	 * 智慧选址接口
	 * @param reqParams 参数
	 * @return 结果
	 */
	String qryFullAddressFromOSS(String reqParams);
	
}
