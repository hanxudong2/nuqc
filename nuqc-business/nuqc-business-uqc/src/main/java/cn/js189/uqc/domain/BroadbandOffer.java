package cn.js189.uqc.domain;

public class BroadbandOffer {

	private String offerId;

	private String offerName;

	private String netAccount;

	public BroadbandOffer() {

	}

	public BroadbandOffer(String offerId, String offerName) {
		this.offerId = offerId;
		this.offerName = offerName;
	}

	public BroadbandOffer(String offerId, String offerName, String netAccount) {
		this.offerId = offerId;
		this.offerName = offerName;
		this.netAccount = netAccount;
	}

	public String getOfferId() {
		return offerId;
	}

	public void setOfferId(String offerId) {
		this.offerId = offerId;
	}

	public String getOfferName() {
		return offerName;
	}

	public void setOfferName(String offerName) {
		this.offerName = offerName;
	}

	public String getNetAccount() {
		return netAccount;
	}

	public void setNetAccount(String netAccount) {
		this.netAccount = netAccount;
	}
}
