/**************************************************************************************** 

 ****************************************************************************************/
package cn.js189.uqc.util.thread;

import cn.hutool.extra.spring.SpringUtil;
import cn.js189.uqc.redis.RedisOperation;
import cn.js189.uqc.util.HttpClientUtils;
import cn.js189.uqc.util.MailUtil;
import com.alibaba.fastjson.JSONObject;


/**
 * Description: <br> 
 *  
 * @author yangyang<br>
 * 2020年11月27日
 */ 
public class SendMnpAlmThread extends Thread{

    String phone;
    
    /**
     * phone 手机号
     */ 
    public SendMnpAlmThread(String  phone) {
        super();
        this.phone = phone;
    }

    /**
     * Description: <br> 
     *  
     * @author yangyang<br>
     * @taskId <br> <br>
     */ 
    @Override
    public void run() {
        doSMS();
        doEmail();
    }

    /**
     * Description: <br> 
     *  
     * @author yangyang<br>
     * 2020年11月27日
     */ 
    private void doEmail() {
        RedisOperation redisOperation = SpringUtil.getBean("redisOperation");
        String  MNPEmailToUsers = redisOperation.getForString("MNPEmailToUsers");
        String [] users = MNPEmailToUsers.split(",");
	    for (String user : users) {
		    MailUtil.sendMail("查询域", user, "调用第三方短厅下发授权码告警", phone + "下发授权码超时或者网络波动", null);
	    }
    }

    /**
     * Description: <br> 
     *  
     * @author yangyang<br>
     * 2020年11月27日
     */ 
    private void doSMS() {
        RedisOperation redisOperation = SpringUtil.getBean("redisOperation");
        String  usersCache = redisOperation.getForString("MNPSMSToUsers");
        String [] users = usersCache.split(",");
        JSONObject sendJson = new JSONObject();
        sendJson.put("templateId", "167");
        sendJson.put("param1", phone);
        sendJson.put("param2", "下发短信授权码");
        sendJson.put("param3", "XXXX");
	    for (String user : users) {
		    sendJson.put("smsCalledNbr", user);
		    HttpClientUtils.doPostJSONold("https://jsdt3.telecomjs.com:8083/jsdt_services/IUVCService/realTimeSendInfo", sendJson.toString());
	    }
    }
    

}
