package cn.js189.uqc.domain.check;

import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CheckGovCustomerByAccNum extends CheckGovCustomerBase{
	// 日志组件
	private static final Logger LOGGER = LoggerFactory.getLogger(CheckGovCustomerByAccNum.class);
	
	// 接入号码
	private String accNum;
	// 账号
	// 接入号码类型
	private String prodId;
	
	public CheckGovCustomerByAccNum(String regionId, String uncompletedFlag, String accNum, String prodId) {
		super(regionId, uncompletedFlag);
		this.accNum = accNum;
		this.prodId = prodId;
	}

	@Override
	protected JSONObject improveRequestObject(JSONObject requestObject) {
		requestObject.put("accNum", accNum);
		requestObject.put("prodId", prodId);
		LOGGER.debug("requestObject:{}", requestObject.toString());
		return requestObject;
	}
}
