/**************************************************************************************** 

 ****************************************************************************************/
package cn.js189.uqc.domain.qry;

import cn.js189.uqc.common.RequestParamBase;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * <Description> <br> 
 *  
 * @author yangyang<br>
 * @version 1.0<br>
 * @taskId <br>
 * @CreateDate 2019年6月3日 <br>
 */

public  class QryMyValueAddedServiceBase extends RequestParamBase {

 // 日志组件
    private static final Logger LOGGER = LoggerFactory.getLogger(QryMyValueAddedServiceBase.class);

    // svcCode 提供的API接口名称
    public static final String BSS_TCP_CONT_SVC_CODE = "js.intf.qryMyValueAddedService";

    // 地区标识
    protected String regionId;
    protected String prodId;//接入号产品类型
    protected String accNum;//接入号
   

    /**
     * 查询销售品信息构造器
     * 
     * @param regionId:地区标识
     */
    public QryMyValueAddedServiceBase(String regionId, String prodId, String accNum) {
        super();
        this.regionId = regionId;
        this.prodId = prodId;
        this.accNum = accNum;
    }

    @Override
    protected JSONObject generateTcpCont() {
        JSONObject aa = super.generateTcpCont(BSS_TCP_CONT_APP_KEY, BSS_TCP_CONT_SVC_CODE, null);
//        aa.put("appKey", "JS_DEFAULT");
//        aa.put("sign", "123456");
        return aa;
    }

    @Override
    protected JSONObject generateSvcCont() {
        JSONObject authenticationInfo = generateAuthenticationInfo();
        if (null == authenticationInfo) {
            LOGGER.error("generate authenticationInfo failure");
            return null;
        }

        JSONObject requestObject = generateRequestObject();
        if (null == requestObject) {
            LOGGER.error("generate requestObject failure");
            return null;
        }

        JSONObject svcCont = new JSONObject();
        svcCont.put("authenticationInfo", authenticationInfo);
        svcCont.put("requestObject", requestObject);
        return svcCont;
    }

    /**
     * 生成 svcCont 下的认证信息
     * 
     * @return
     */
    protected JSONObject generateAuthenticationInfo() {
        return super.generateSvcContAuthenticationInfo(regionId, null, null, null);
    }

    /**
     * 生成 svcCont 下的请求对象
     * 
     * @return
     */
    protected JSONObject generateRequestObject() {
        JSONObject requestObject = new JSONObject();
        requestObject.put("regionId", regionId);
        requestObject.put("prodId", prodId);
        requestObject.put("accNum", accNum);
        return requestObject;
    }



   
}
