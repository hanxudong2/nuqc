package cn.js189.uqc.common;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 通过账号的方式查询产品关联关系信息，生成请求报文
 * 
 * @author Mid
 */
public class QryProdInstRelByAccount extends QryProdRelBase {
	// 日志组件
	private static final Logger LOGGER = LoggerFactory.getLogger(QryProdInstRelByAccount.class);

	// 账号
	private String account;
	
	//产品规格   100000002固话   10000009宽带 100000379手机号码
	private String prodId;
	
	//查询关系
	private JSONArray relTypes;
	
	public QryProdInstRelByAccount(String regionId, String uncompletedFlag, String account, String prodId, JSONArray relTypes) {
		super(regionId, uncompletedFlag);
		this.account = account;
		this.prodId = prodId;
		this.relTypes = relTypes;
	}

	@Override
	protected JSONObject improveRequestObject(JSONObject requestObject) {
		requestObject.put("account", account);
		requestObject.put("prodId", prodId);
		requestObject.put("relTypes", relTypes);
		LOGGER.debug("requestObject:{}", requestObject);
		return requestObject;
	}
}
