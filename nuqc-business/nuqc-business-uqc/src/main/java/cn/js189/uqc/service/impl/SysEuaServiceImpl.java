package cn.js189.uqc.service.impl;

import cn.js189.common.domain.EuaAuthority;
import cn.js189.common.domain.EuaChannel;
import cn.js189.uqc.mapper.SysEuaMapper;
import cn.js189.uqc.service.SysEuaService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class SysEuaServiceImpl implements SysEuaService {
	
	@Resource
	private SysEuaMapper sysEuaMapper;
	
	@Override
	public List<EuaAuthority> listAll(String channelId) {
		return sysEuaMapper.listAll(channelId);
	}
	
	@Override
	public Integer listCount() {
		return sysEuaMapper.listCount();
	}
	
	@Override
	public List<EuaChannel> listAllKey(String channelId) {
		return sysEuaMapper.listAllKey(channelId);
	}
}
