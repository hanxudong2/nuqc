package cn.js189.uqc.domain.qry;

import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class QryProdIdByAccount extends QryProdIdBase{
	// 日志组件
	private static final Logger LOGGER = LoggerFactory.getLogger(QryProdIdByAccount.class);

	// 接入号码
	private String accNum;
	// 账号
	private String account;
	
	public QryProdIdByAccount(String regionId, String uncompletedFlag, String accNum, String account) {
		super(regionId, uncompletedFlag);
		this.accNum = accNum;
		this.account = account;
	}

	@Override
	protected JSONObject improveRequestObject(JSONObject requestObject) {
		requestObject.put("accNum", accNum);
		requestObject.put("account", account);
		LOGGER.debug("requestObject:{}", requestObject.toString());
		return requestObject;
	}
}
