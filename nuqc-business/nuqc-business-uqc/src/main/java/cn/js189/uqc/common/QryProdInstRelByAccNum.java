package cn.js189.uqc.common;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 通过接入号的方式查询产品关联关系信息，生成请求报文
 * 
 * @author Mid
 */
public class QryProdInstRelByAccNum extends QryProdRelBase {
	// 日志组件
	private static final Logger LOGGER = LoggerFactory.getLogger(QryProdInstRelByAccNum.class);

	// 接入号
	private String accNum;
	
	//产品规格
	private String prodId;
	
	//查询关系
	private JSONArray relTypes;

	public QryProdInstRelByAccNum(String regionId, String uncompletedFlag, String accNum, String prodId, JSONArray relTypes) {
		super(regionId, uncompletedFlag);
		this.accNum = accNum;
		this.prodId = prodId;
		this.relTypes = relTypes;
	}

	@Override
	protected JSONObject improveRequestObject(JSONObject requestObject) {
		requestObject.put("accNum", accNum);
		requestObject.put("prodId", prodId);
		requestObject.put("relTypes", relTypes);
		LOGGER.debug("requestObject:{}", requestObject);
		return requestObject;
	}
}
