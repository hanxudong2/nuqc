package cn.js189.uqc.domain.qry;

import cn.js189.uqc.common.RequestParamBase;
import cn.js189.uqc.util.enums.QryStaffEnum;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public abstract class QrySendTJMessageResultBase extends RequestParamBase {

    private static final Logger LOGGER = LoggerFactory.getLogger(QrySendTJMessageResultBase.class);

    private static final String BSS_TCP_CONT_SVC_CODE = "js.intf.qrySendTJMessageResult";

    // 地区标识
    protected String regionId;

    // 查询范围信息的封装
    protected List<QryStaffEnum> scopeInfos;


    public QrySendTJMessageResultBase(String regionId) {
        super();
        this.regionId = regionId;
    }

    @Override
    protected JSONObject generateTcpCont() {
        JSONObject aa = super.generateTcpCont(BSS_TCP_CONT_APP_KEY, BSS_TCP_CONT_SVC_CODE, null);
//        aa.put("appKey", "JS_DEFAULT");
//        aa.put("sign", "123456");
        return aa;
    }

    @Override
    protected JSONObject generateSvcCont() {
        JSONObject authenticationInfo = generateAuthenticationInfo();
        if (null == authenticationInfo) {
            LOGGER.error("generate authenticationInfo failure");
            return null;
        }

        JSONObject requestObject = generateRequestObject();
        if (null == requestObject) {
            LOGGER.error("generate requestObject failure");
            return null;
        }

        JSONObject svcCont = new JSONObject();
        svcCont.put("authenticationInfo", authenticationInfo);
        svcCont.put("requestObject", requestObject);
        return svcCont;
    }

    /**
     * 生成 svcCont 下的认证信息
     *
     * @return
     */
    protected JSONObject generateAuthenticationInfo() {
        return super.generateSvcContAuthenticationInfo(regionId, null, null, null);
    }

    /**
     * 生成 svcCont 下的请求对象
     *
     * @return
     */
    protected JSONObject generateRequestObject() {
        JSONObject requestObject = new JSONObject();
        requestObject.put("regionId", regionId);
//        requestObject.put("scopeInfos", generateScopeInfos());
        return improveRequestObject(requestObject);
    }

    /**
     * 生成请求对象下的scope信息
     *
     * @return
     */
    private JSONArray generateScopeInfos() {
//        boolean scopeInfosIsEmpty = (null == scopeInfos || scopeInfos.isEmpty());
//        // 默认查询产品实例信息
//        if (scopeInfosIsEmpty) {
//            scopeInfos = new ArrayList<QryStaffEnum>();
//            scopeInfos.add(QryStaffEnum.STAFF);
//            scopeInfos.add(QryStaffEnum.STAFF_CONTACT_INFO);
//        }

        JSONArray scopeInfoJSONArray = new JSONArray();

//        for (QryStaffEnum qryStaffEnum : scopeInfos) {
//            JSONObject scopeInfo = new JSONObject();
//            scopeInfo.put("scope", qryStaffEnum.getScopeCode());
//            scopeInfoJSONArray.add(scopeInfo);
//        }

        return scopeInfoJSONArray;
    }

    /**
     * 添加查询scope信息
     */
    public void addScopeInfos(QryStaffEnum qryStaffEnum) {
        if (null == scopeInfos) {
            scopeInfos = new ArrayList<QryStaffEnum>();
        }
        scopeInfos.add(qryStaffEnum);
    }

    /**
     * 补齐 svcCont 下的请求对象
     *
     * @param requestObject:svcCont下的请求对象
     * @return
     */
    protected abstract JSONObject improveRequestObject(JSONObject requestObject);
}
