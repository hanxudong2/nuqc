package cn.js189.uqc.domain.check;


import cn.js189.uqc.common.RequestParamBase;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * 智慧BSS，校验政企用户请求报文基础类
 * 
 * @author Mid
 */
public abstract class CheckGovCustomerBase extends RequestParamBase {
	// 日志组件
	private static final Logger LOGGER = LoggerFactory.getLogger(CheckGovCustomerBase.class);

	// svcCode 提供的API接口名称
	public static final String BSS_TCP_CONT_SVC_CODE = "js.intf.checkGovCustomer";

	// 地区标识
	protected String regionId;
	// 是否查询未竣工相应的信息
	protected String uncompletedFlag;

	/**
	 * 查询产品信息构造器
	 * 
	 * @param regionId:地区标识
	 * @param uncompletedFlag:是否查询未竣工相应的信息
	 */
	public CheckGovCustomerBase(String regionId, String uncompletedFlag) {
		super();
		this.regionId = regionId;
		this.uncompletedFlag = uncompletedFlag;
	}

	@Override
	protected JSONObject generateTcpCont() {
		return super.generateTcpCont(BSS_TCP_CONT_APP_KEY, BSS_TCP_CONT_SVC_CODE, null);
	}

	@Override
	protected JSONObject generateSvcCont() {
		JSONObject authenticationInfo = generateAuthenticationInfo();
		if (null == authenticationInfo) {
			LOGGER.error("generate authenticationInfo failure");
			return null;
		}

		JSONObject requestObject = generateRequestObject();
		if (null == requestObject) {
			LOGGER.error("generate requestObject failure");
			return null;
		}

		JSONObject svcCont = new JSONObject();
		svcCont.put("authenticationInfo", authenticationInfo);
		svcCont.put("requestObject", requestObject);
		return svcCont;
	}

	/**
	 * 生成 svcCont 下的认证信息
	 * 
	 * @return
	 */
	protected JSONObject generateAuthenticationInfo() {
		return super.generateSvcContAuthenticationInfo(regionId, null, null, null);
	}

	/**
	 * 生成 svcCont 下的请求对象
	 * 
	 * @return
	 */
	protected JSONObject generateRequestObject() {
		JSONObject requestObject = new JSONObject();
		requestObject.put("regionId", regionId);
		requestObject.put("uncompletedFlag", uncompletedFlag);
		return improveRequestObject(requestObject);
	}

	/**
	 * 补齐 svcCont 下的请求对象
	 * 
	 * @param requestObject:svcCont下的请求对象
	 * @return
	 */
	protected abstract JSONObject improveRequestObject(JSONObject requestObject);


}
