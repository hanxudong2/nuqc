package cn.js189.uqc.domain.qry;

import cn.js189.common.enums.QryOrderScopeInfoEnum;
import cn.js189.uqc.service.IQryPageInfo;
import cn.js189.uqc.common.QryPageInfoDefaultImpl;
import cn.js189.uqc.common.RequestParamBase;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * 智慧BSS，生成查询订单信息请求报文基础类
 * 
 * @author Mid
 */
public abstract class QryOrderNbrByPartyIdBase extends RequestParamBase {
	// 日志组件
	private static final Logger LOGGER = LoggerFactory.getLogger(QryOrderNbrByPartyIdBase.class);

	// svcCode 提供的API接口名称
	public static final String BSS_TCP_CONT_SVC_CODE = "js.intf.qryCustomerOrder";

	// 地区标识
	protected String regionId;
	// 分页相关信息的封装
	protected IQryPageInfo qryPageInfo;
	// 查询范围信息的封装
	protected List<QryOrderScopeInfoEnum> scopeInfos;

	/**
	 * 查询产品信息构造器
	 * 
	 * @param regionId:地区标识
	 */
	public QryOrderNbrByPartyIdBase(String regionId) {
		super();
		this.regionId = regionId;
		this.qryPageInfo = new QryPageInfoDefaultImpl();
	}

	@Override
	protected JSONObject generateTcpCont() {
		return super.generateTcpCont(BSS_TCP_CONT_APP_KEY, BSS_TCP_CONT_SVC_CODE, null);
	}

	@Override
	protected JSONObject generateSvcCont() {
		JSONObject authenticationInfo = generateAuthenticationInfo();
		if (null == authenticationInfo) {
			LOGGER.error("generate authenticationInfo failure");
			return null;
		}

		JSONObject requestObject = generateRequestObject();
		if (null == requestObject) {
			LOGGER.error("generate requestObject failure");
			return null;
		}

		JSONObject svcCont = new JSONObject();
		svcCont.put("authenticationInfo", authenticationInfo);
		svcCont.put("requestObject", requestObject);
		return svcCont;
	}

	/**
	 * 生成 svcCont 下的认证信息
	 * 
	 * @return
	 */
	protected JSONObject generateAuthenticationInfo() {
		return super.generateSvcContAuthenticationInfo(regionId, null, null, null);
	}

	/**
	 * 生成 svcCont 下的请求对象
	 * 
	 * @return
	 */
	protected JSONObject generateRequestObject() {
		JSONObject requestObject = new JSONObject();
		requestObject.put("pageInfos", qryPageInfo.generatePageInfos());
		requestObject.put("scopeInfos", generateScopeInfos());
		requestObject.put("regionId", regionId);
		return improveRequestObject(requestObject);
	}

	/**
	 * 补齐 svcCont 下的请求对象
	 * 
	 * @param requestObject:svcCont下的请求对象
	 * @return
	 */
	protected abstract JSONObject improveRequestObject(JSONObject requestObject);

	/**
	 * 添加查询scope信息
	 * 
	 * @param qryOfferInstScopeInfo:查询销售品范围的枚举类型
	 */
	public void addScopeInfos(QryOrderScopeInfoEnum qryOfferInstScopeInfo) {
		if (null == scopeInfos) {
			scopeInfos = new ArrayList<>();
		}
		scopeInfos.add(qryOfferInstScopeInfo);
	}

	/**
	 * 生成请求对象下的scope信息
	 * 
	 * @return
	 */
	private JSONArray generateScopeInfos() {
		boolean scopeInfosIsEmpty = (null == scopeInfos || scopeInfos.isEmpty());
		// 默认查询产品实例信息
		if (scopeInfosIsEmpty) {
			scopeInfos = new ArrayList<>();
			scopeInfos.add(QryOrderScopeInfoEnum.CUSTOMER_ORDER);
		}

		JSONArray scopeInfoJSONArray = new JSONArray();
		for (QryOrderScopeInfoEnum qryOfferInstScopeInfo : scopeInfos) {
			JSONObject scopeInfo = new JSONObject();
			scopeInfo.put("scope", qryOfferInstScopeInfo.getScopeCode());
			scopeInfoJSONArray.add(scopeInfo);
		}
		return scopeInfoJSONArray;
	}
}
