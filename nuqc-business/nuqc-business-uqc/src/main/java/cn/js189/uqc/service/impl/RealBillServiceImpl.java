package cn.js189.uqc.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.js189.common.domain.ReqHead;
import cn.js189.common.domain.ReqInfo;
import cn.js189.common.domain.RespHead;
import cn.js189.common.domain.RespInfo;
import cn.js189.common.enums.SystemEnum;
import cn.js189.common.util.MainUtils;
import cn.js189.common.util.Utils;
import cn.js189.common.util.helper.UUIDHelper;
import cn.js189.uqc.service.IRealBillService;
import cn.js189.uqc.service.PubAddressIpervice;
import cn.js189.uqc.util.HandleParamForAccountingCenter;
import cn.js189.uqc.util.HttpClientHelperForEop;
import cn.js189.uqc.util.MD5Utils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
public class RealBillServiceImpl implements IRealBillService {

    private static final Logger logger = LoggerFactory.getLogger(RealBillServiceImpl.class);

    @Autowired
    private BSService bsService;

    @Autowired
    private PubAddressIpervice pubAddressIpervice;

    /**
     * 量本使用明细查询（累积量明细）
     * @Param
     * @Author xzy
     * @Date 2024/3/6 10:01
     */
    @Override
    public String accuUseDetailQry(String requestJSON1) {
        logger.debug("accuUseDetailQry input :{}" , requestJSON1);
        JSONObject response = null;
        RespInfo responseJson = new RespInfo();
        RespHead responseHead = new RespHead();
        responseJson.setHead(responseHead);
        String tranId = "";
        try {
            ReqInfo reqInfo = BeanUtil.toBean(requestJSON1,ReqInfo.class);
            String result = bsService.accuUseDetailQry(reqInfo);
            response = Utils.builderResponse(result, responseHead, responseJson, tranId, "resultCode", "", "resultMsg");
            return response.toString();
        } catch (Exception e) {
            response = Utils.dealExceptionResp(responseHead, tranId, responseJson);
            logger.debug("accuUseDetailQry Exception :{}" , e.getMessage());
            return response.toString();
        }
    }

    /**
     * Description: 余额查询<br>
     * @Param
     * @Author xzy
     * @Date 2024/3/6 10:01
     */
    @Override
    public String callBalanceQrReq(String requestJSON1) {
        logger.debug("callBalanceQrReq(余额查询) input :{}" , requestJSON1);
        JSONObject response = null;
        RespInfo responseJson = new RespInfo();
        RespHead responseHead = new RespHead();
        responseJson.setHead(responseHead);
        //  获得接口号流水
        String tranId = "";
        try {
            ReqInfo reqInfo = BeanUtil.toBean(requestJSON1,ReqInfo.class);
            JSONObject body = reqInfo.getBody();
            String result = bsService.callBalanceQrReqNew(reqInfo);
            ReqHead reqHead = reqInfo.getHead();
            return buildBillResponse(body, reqHead, result);
        } catch (Exception e) {
            response = Utils.dealExceptionResp(responseHead, tranId, responseJson);
            logger.debug("callBalanceQrReq(余额查询) Exception :{}" , e.getMessage());
            return response.toString();
        }
    }


    /**
     *
     * Description:缓存获取余额查询 <br>
     * @Param
     * @Author xzy
     * @Date 2024/3/6 10:01
     */
    @Override
    public String callBalanceReqWithCache(String requestJSON1) {
        logger.debug("callBalanceReqWithCache(余额缓存查询) input :{}" , requestJSON1);
        JSONObject response = null;
        RespInfo responseJson = new RespInfo();
        RespHead responseHead = new RespHead();
        responseJson.setHead(responseHead);
        //  获得接口号流水
        String tranId = "";
        try {
            ReqInfo reqInfo = BeanUtil.toBean(requestJSON1,ReqInfo.class);
            JSONObject requestJSON = JSON.parseObject(requestJSON1);
            JSONObject head = requestJSON.getJSONObject("head");
            JSONObject body = reqInfo.getBody();
            tranId = head.getString("tranId");
            Map<String,String> para = new HashMap<>();
            para.put("areaCode", head.getString("areaCode"));
            para.put("accNbr", body.getString("accNbr"));
            para.put("dccDestinationAttr", body.getString("dccDestinationAttr"));// // 0：固话 2：移动 3：宽带,宽带需要多加一遍区号
            para.put("dccQueryFlag", body.getString("dccQueryFlag"));
            String result = zTScreenDateDealer.searchBillArrears(para);
            response = Utils.builderResponse(result, responseHead, responseJson, tranId, "TSR_RESULT", "", "TSR_MSG");
            if (body.containsKey(MDA.UQCENCFLAG)) {
                response = getEncResponse(response, reqInfo.getHead());
            }
        } catch (Exception e) {
            response = Utils.dealExceptionResp(responseHead, tranId, responseJson);
            logger.debug("callBalanceReqWithCache(余额缓存查询) Exception :{}" , e.getMessage());
            return response.toString();
        }
        return response.toString();
    }


    /**
     *
     * Description:本月消费缓存查询 <br>
     * @Param
     * @Author xzy
     * @Date 2024/3/6 10:01
     */
    @Override
    public String callCurrMonthBillReqWithCache(String requestJSON1) {
        logger.debug("callCurrMonthBillReqWithCache(当月消费缓存查询) input :{}" , requestJSON1);
        JSONObject response = null;
        RespInfo responseJson = new RespInfo();
        RespHead responseHead = new RespHead();
        responseJson.setHead(responseHead);
        //  获得接口号流水
        String tranId = "";
        try {
            JSONObject requestJSON = JSON.parseObject(requestJSON1);
            JSONObject head = requestJSON.getJSONObject("head");
            JSONObject body = reqInfo.getBody();
            tranId = head.getString("tranId");
            Map<String,String> para = new HashMap<>();
            para.put("areaCode", head.getString("areaCode"));
            para.put("accNbr", body.getString("accNbr"));
            para.put("productId", body.getString("productId"));// // 0：固话 2：移动 3：宽带,宽带需要多加一遍区号
            String result = zTScreenDateDealer.searchMonthConsumption(para);
            response = Utils.builderResponse(result, responseHead, responseJson, tranId, "TSR_RESULT", "", "TSR_MSG");
            if (body.containsKey(MDA.UQCENCFLAG)) {
                response = getEncResponse(response, reqInfo.getHead());
            }
        } catch (Exception e) {
            response = Utils.dealExceptionResp(responseHead, tranId, responseJson);
            logger.debug("callCurrMonthBillReqWithCache(当月消费缓存查询) Exception :{}" , e.getMessage());
            return response.toString();
        }
        return response.toString();
    }

    /**
     * Description: 本期已付查询接口<br>
     * @Param
     * @Author xzy
     * @Date 2024/3/6 10:01
     */
    @Override
    public String callCustUseQry(String requestJSON1) {
        logger.debug("新计费接口统一接口参数（callCustUseQry-本期已付查询） input :{}" , requestJSON1);
        JSONObject response = null;
        RespInfo responseJson = new RespInfo();
        RespHead responseHead = new RespHead();
        responseJson.setHead(responseHead);
        //  获得接口号流水
        String tranId = "";
        try {
            JSONObject requestJSON = JSON.parseObject(requestJSON1);
            JSONObject head = requestJSON.getJSONObject("head");
            JSONObject body = requestJSON.getJSONObject("body");
            tranId =  head.getString("tranId");

            String sAreaCode = body.getString("areaCode");
            //-----------1126修改开始
//            http://jsjteop.telecomjs.com:8764/jseop/billing_zw/CustomerQry/CustomerQry
//            String url = BSUrlConstant.CUSTOMER_QRY;
            String url = pubAddressIpervice.getActionUrl("CUSTOMER_QRY","");
            logger.debug("新计费接口统一接口参数（callCustUseQry-本期已付查询）---url:{}" , url);

            String sAccNbr = body.getString("accNbr");
            String lFamilyId = body.getString("productId");
            String lBillingCycleId = body.getString("billingCycle");
            String lGetFlag = body.getString("queryFlag");
            JSONObject paramJson = CreateParamForAccountingCenter.createParamForCustomerQry(sAreaCode, sAccNbr, lFamilyId, lBillingCycleId, lGetFlag);
            String resultString = HttpClientHelperForEop.postWithMNP(url, paramJson.toJSONString());
            return HandleParamForAccountingCenter.builderResponseForCustomerQry(resultString);
        }catch (Exception e) {
            response = Utils.dealExceptionResp(responseHead, tranId, responseJson);
            logger.debug("callCustUseQry-本期已付查询 Exception :{}" , e.getMessage());
            return response.toString();
        }
    }

    /**
     * Description: 费用返还记录查询接口<br>
     * @Param
     * @Author xzy
     * @Date 2024/3/6 10:01
     */
    @Override
    public String callFeeReturnList(String requestJSON1) {
        logger.debug("新计费接口统一接口参数（callFeeReturnList-话费返还记录查询） input :{}",requestJSON1);
        JSONObject response = null;
        RespInfo responseJson = new RespInfo();
        RespHead responseHead = new RespHead();
        responseJson.setHead(responseHead);
        //  获得接口号流水
        String tranId = "";
        try {
            JSONObject requestJSON = JSON.parseObject(requestJSON1);
            JSONObject head = requestJSON.getJSONObject("head");
            JSONObject body = requestJSON.getJSONObject("body");
            tranId =  head.getString("tranId");
            String requestBillJson =  builderFeeReturnQr(body.toString());
            logger.debug(":new charge callFeeReturnList-话费返还记录查询 input : {}" , requestBillJson);
            String rsp = BillSmartSystemImpl.commonMethod(requestBillJson,"callFeeReturnList-话费返还记录查询",RealBillImpl.FEERETURNURL);
            logger.debug("callFeeReturnList-话费返还记录查询 output : {}" , rsp);
            return rsp;
        }catch (Exception e) {
            response = Utils.dealExceptionResp(responseHead, tranId, responseJson);
            logger.debug("callFeeReturnList-话费返还记录查询 Exception :{}" , e.getMessage());
            return response.toString();
        }
    }

    /**
     * Description:欠费查询 <br>
     * @Param
     * @Author xzy
     * @Date 2024/3/6 10:01
     */
    @Override
    public String callNewOthGetOwe(String requestJSON1) {
        logger.debug("callNewOthGetOwe(欠费查询 ) input :{}" , requestJSON1);
        JSONObject response = null;
        RespInfo responseJson = new RespInfo();
        RespHead responseHead = new RespHead();
        responseJson.setHead(responseHead);
        //  获得接口号流水
        String tranId = "";
        try {
            JSONObject requestJSON = JSON.parseObject(requestJSON1);
            JSONObject head = requestJSON.getJSONObject("head");
            tranId = head.getString("tranId");
            JSONObject body = reqInfo.getBody();

            String sAreaCode = body.getString("areaCode");
            JSONObject resultjson = null;
            //-----------1202修改开始
            //11 27 计费重新对接改动
//            欠费查询接口
//            http://jsjteop.telecomjs.com:8764/jseop/billing_zw/OthGetOwe/OthGetOwe
//            String newUrl = BSUrlConstant.OTH_GET_OWE;
            String newUrl = pubAddressIpervice.getActionUrl("OTH_GET_OWE","");
            logger.debug("callNewOthGetOwe(欠费查询 ) ---url:{}" , newUrl);
            String sRemoteSourceID = SystemEnum.SGW_ORG_ID.getValue();
            String sRemoteSourcePwd = SystemEnum.SGW_PASSWORD.getValue();
            String lGetFlag = body.getString("getFlag");
//                String lStaffID = body.getString("staffId");

            String  sAccNbr = body.getString("accNbr");

            JSONObject paramJson = CreateParamForAccountingCenter.createParamForOthGetOwe(sAreaCode, sAccNbr, lGetFlag, sRemoteSourceID, sRemoteSourcePwd);
            logger.info("callNewOthGetOwe 欠费查询入参:{}", paramJson);

            String resultString = HttpClientHelperForEop.postWithMNP(newUrl, paramJson.toJSONString());
            logger.info("callNewOthGetOwe 欠费查询 出参:{}", resultString);

            resultjson = HandleParamForAccountingCenter.builderResponseForOthGetOwe(resultString);
            logger.info("callNewOthGetOwe 欠费查询处理后出参:{}", resultjson);
            //-----------1202修改结束
            response = Utils.builderResponse(resultjson.toString(), responseHead, responseJson, tranId, "iresult", "", "smsg");
            if (response.getJSONObject("head").getString("status").equals("00")) {
                String jsonArrStr = response.getJSONObject("body").getString("othgetowelist");
                if (!jsonArrStr.startsWith("[")) {
                    JSONObject jsonObject = response.getJSONObject("body").getJSONObject("othgetowelist");
                    JSONArray jsonArr = new JSONArray();
                    jsonArr.add(jsonObject);
                    response.getJSONObject("body").remove("othgetowelist");
                    response.getJSONObject("body").put("othgetowelist", jsonArr);
                }
            }
            response = (JSONObject) JSON.toJSON(responseJson);
            if (body.containsKey(MDA.UQCENCFLAG)) {
                response = getEncResponse(response, reqInfo.getHead());
            }
        } catch (Exception e) {
            response = Utils.dealExceptionResp(responseHead, tranId, responseJson);
            logger.debug("callNewOthGetOwe(欠费查询 ) Exception :{}" , e.getMessage());
            return response.toString();
        }
        return response.toString();
    }

    /**
     * Description:充值-查询业务类型 <br>
     * @Param
     * @Author xzy
     * @Date 2024/3/6 10:01
     */
    @Override
    public String callNewUOthGetType(String requestJSON1) {
        logger.debug("callNewUOthGetType(充值-查询业务类型接口) input :{}" , requestJSON1);
        JSONObject response = null;
        RespInfo responseJson = new RespInfo();
        RespHead responseHead = new RespHead();
        responseJson.setHead(responseHead);
        //  获得接口号流水
        String tranId = "";
        try {
            JSONObject requestJSON = JSON.parseObject(requestJSON1);
            JSONObject head = requestJSON.getJSONObject("head");
            JSONObject body = requestJSON.getJSONObject("body");
            tranId = head.getString("tranId");


            String sAreaCode = body.getString("areaCode");
            JSONObject resultjson= null;
            //-----------1202修改开始
            //11 27 计费重新对接改动
//            查询业务接口类型
//            http://jsjteop.telecomjs.com:8764/jseop/billing_zw/OthGetServType/OthGetServType
//            String newUrl =BSUrlConstant.OTH_GET_TYPE;
            String newUrl = pubAddressIpervice.getActionUrl("OTH_GET_TYPE","");
            logger.debug("callNewUOthGetType(充值-查询业务类型接口) ---url:{}" , newUrl);

            String sRemoteSourceID = SystemEnum.SGW_ORG_ID.getValue();
            String sRemoteSourcePwd = SystemEnum.SGW_PASSWORD.getValue();
            String lFamilyId = MainUtils.getNullEmptyForReq(body.getString("family"));
            String  sAccNbr = body.getString("accNbr");
            JSONObject paramJson = CreateParamForAccountingCenter.createParamForOthGetServType(sRemoteSourceID, sRemoteSourcePwd, lFamilyId, sAreaCode, sAccNbr);
            String resultString = HttpClientHelperForEop.postWithMNP(newUrl, paramJson.toJSONString());
            resultjson = HandleParamForAccountingCenter.builderResponseForOthGetServType(resultString);
            //-----------1202修改结束
            response = Utils.builderResponse(resultjson.toJSONString(), responseHead, responseJson, tranId, "iresult");
        } catch (Exception e) {
            response = Utils.dealExceptionResp(responseHead, tranId, responseJson);
            logger.debug("callNewUOthGetType(充值-查询业务类型接口) Exception :{}" , e.getMessage());
            return response.toString();
        }
        return response.toString();
    }

    /**
     *
     * Description:积分商城通用接口 <br>
     * @Param
     * @Author xzy
     * @Date 2024/3/6 10:01
     */
    @Override
    public String callPointMarketCommonMethod(String requestJSON1) {
        logger.debug("callPointMarketCommonMethod(积分商城通用接口) input:{}" ,requestJSON1);
        JSONObject response = null;
        RespInfo responseJson = new RespInfo();
        RespHead responseHead = new RespHead();
        responseJson.setHead(responseHead);
        //  获得接口号流水
        String tranId = "";
        // 生产password
        String cipher = "8320999612345678";
        // 测试MD5 KEY
        // 生产MD5 KEY
        String md5Key = "tenkentJSDX";
        // 生产地址
//        String url = "https://wxjf.js118114.com/hbgl/zt/JSTMobileService/";
        NacosCommon nacosCommon = (NacosCommon) ContextHelper.getBean("nacosCommon");
        String url = nacosCommon.getProxyUrl() + "/hbgl/zt/JSTMobileService/";

        try {
            ReqInfo reqInfo = BeanUtil.toBean(jsonParam,ReqInfo.class);
            JSONObject requestJSON = JSON.parseObject(requestJSON1);
            JSONObject head = requestJSON.getJSONObject("head");
            JSONObject body = reqInfo.getBody();
            tranId = head.getString("tranId");
            JSONObject contentJSON = null;
            JSONObject requestParam = new JSONObject();
            String contentStr = "";
            if (!body.getString("content").equals("")) {
                contentJSON = body.getJSONObject("content");
                contentStr = contentJSON.toString();
            }
            String method = body.getString("method");
            url = url + method;
            byte[] data = AES.encrypt(contentStr, cipher);
            String content = Base64.encode(data);
            String noEncoderContent =content;
            content = URLEncoder.encode(content, "UTF-8");
            requestParam.put("content", content);
            requestParam.put("method", method);
            Long time = new Date().getTime();
            requestParam.put("timestamp", time);
            String phone = head.getString("loginNbr");
            requestParam.put("phone", phone);
            String needEncodeStr = noEncoderContent + time + phone + md5Key;
            String verify = MD5Utils.mD5(needEncodeStr).toUpperCase();
            requestParam.put("verify", verify);
            logger.debug("JSTMobileService接口具体方法为:{},入参:{}", method, requestParam);
            String result = NetUtil.send(url, "POST", requestParam.toJSONString(), "application/json");
            logger.debug("JSTMobileService接口出参：{}", result);

            JSONObject resultJson = JSON.parseObject(result);
            String resultContent = "";
            String cont = resultJson.getString("content");
            byte[] b = Base64.decode(cont);
            resultContent = new String(AES.decrypt(b, cipher), StandardCharsets.UTF_8);
            JSONObject resultContentJson = JSON.parseObject(resultContent);
            resultJson.put("content", resultContentJson == null ? "" : resultContentJson);

            if ((boolean) resultJson.get("result")) {
                resultJson.put("result", "0");
            }
            logger.debug("JSTMobileService接口解密出参:{}", resultJson);
            response = Utils.builderResponse(resultJson.toString(), responseHead, responseJson, tranId, "result", "", "content");
            if (body.containsKey(MDA.UQCENCFLAG)) {
                response = getEncResponse(response, reqInfo.getHead());
            }
        } catch (Exception e) {
            response = Utils.dealExceptionResp(responseHead, tranId, responseJson);
            logger.debug("callPointMarketCommonMethod(积分商城通用接口) Exception :{}" , e.getMessage());
            return response.toString();
        }
        return response.toString();
    }

    /**
     *
     * Description:政企账单汇总 <br>
     * @Param
     * @Author xzy
     * @Date 2024/3/6 10:01
     */
    @Override
    public String fundManageCustBill(String jsonParam) {
        logger.debug("政企账单汇总查询-21122001 input{}" , jsonParam);
        // 接口请求参数
        JSONObject response = null;
        RespInfo responseJson = new RespInfo();
        RespHead responseHead = new RespHead();
        responseJson.setHead(responseHead);
        String tranId = "";
        try {
            JSONObject requestJSON = JSON.parseObject(jsonParam);
            JSONObject head = requestJSON.getJSONObject("head");
            JSONObject body = requestJSON.getJSONObject("body");
            String areaCode = head.getString("areaCode");
            tranId = head.getString("tranId");//流水号
//            政企账单汇总
//            http://jsjteop.telecomjs.com:8764/jseop/billing_zw/FundManageCustBill
//            String url = BSUrlConstant.FUNDMANAGECUSTBILL;
            String url  = pubAddressIpervice.getActionUrl("FUNDMANAGECUSTBILL","");
            logger.debug("政企账单汇总查询-21122001 ---url:{}" , url);
            //拼装入参
            body.put("areaCode", areaCode);
            body.put("remoteSourceId", SystemEnum.SGW_ORG_ID.getValue());
            body.put("remoteSourcePwd", SystemEnum.SGW_PASSWORD.getValue());
            logger.debug("政企账单汇总查询-21122001 调用入参:{}" , body);
            String resultString = HttpClientHelperForEop.postWithMNP(url, body.toJSONString(),30000);
            logger.debug("政企账单汇总查询-21122001 output:{}" , resultString);
            response = Utils.builderResponse(resultString, responseHead, responseJson, tranId,"errorCode","","errorMsg");
        } catch (Exception e) {
            response = Utils.dealExceptionResp(responseHead, tranId, responseJson);
            logger.debug("政企账单汇总查询-21122001 Exception :{}" , e.getMessage());
            return response.toString();
        }
        return response.toString();
    }

    /**
     *
     * Description:政企账单详单 <br>
     * @Param
     * @Author xzy
     * @Date 2024/3/6 10:01
     */
    @Override
    public String fundManageCustBillDet(String jsonParam) {
        logger.debug("政企账单详单查询-21122002 input {}" , jsonParam);
        // 接口请求参数
        JSONObject response = null;
        RespInfo responseJson = new RespInfo();
        RespHead responseHead = new RespHead();
        responseJson.setHead(responseHead);
        String tranId = "";
        try {
            JSONObject requestJSON = JSON.parseObject(jsonParam);
            JSONObject head = requestJSON.getJSONObject("head");
            JSONObject body = requestJSON.getJSONObject("body");
            String areaCode = head.getString("areaCode");
            tranId = head.getString("tranId");//流水号
//            政企账单分项
//            http://jsjteop.telecomjs.com:8764/jseop/billing_zw/FundManageCustBillDet
//            String url = BSUrlConstant.FUNDMANAGECUSTBILLDET;
            String url  = pubAddressIpervice.getActionUrl("FUNDMANAGECUSTBILLDET","");
            logger.debug("政企账单详单查询-21122002 ---url:{}" , url);
            //拼装入参
            body.put("areaCode", areaCode);
            body.put("remoteSourceId", SystemEnum.SGW_ORG_ID.getValue());
            body.put("remoteSourcePwd", SystemEnum.SGW_PASSWORD.getValue());
            logger.debug("政企账单详单查询-21122002 调用入参:{}" , body);
            String resultString = HttpClientHelperForEop.postWithMNP(url, body.toJSONString(),30000);
            logger.debug("政企账单详单查询-21122002 output:{}" , resultString);
            response = Utils.builderResponse(resultString, responseHead, responseJson, tranId,"errorCode","","errorMsg");
        } catch (Exception e) {
            response = Utils.dealExceptionResp(responseHead, tranId, responseJson);
            logger.debug("政企账单详单查询-21122002 Exception :{}" , e.getMessage());
            return response.toString();
        }
        return response.toString();
    }

    /**
     * 费用查询
     * @Param
     * @Author xzy
     * @Date 2024/3/6 10:01
     */
    @Override
    public String getAcctItemForDiag(String jsonParam) {
        logger.debug("费用查询 input {}" , jsonParam);
        // 接口请求参数
        JSONObject response = null;
        RespInfo responseJson = new RespInfo();
        RespHead responseHead = new RespHead();
        responseJson.setHead(responseHead);
        String tranId = "";
        try {
            JSONObject requestJSON = JSON.parseObject(jsonParam);
            JSONObject head = requestJSON.getJSONObject("head");
            JSONObject body = requestJSON.getJSONObject("body");
            String areaCode = head.getString("areaCode");
            tranId = head.getString("tranId");//流水号
//            费用查询
//            http://jsjteop.telecomjs.com:8764/jseop/billing_zw/getAcctItemForDiag/getAcctItemForDiag
//            String url = BSUrlConstant.GET_ACCT_ITEM_FOR_DIAG_URL;
            String url  = pubAddressIpervice.getActionUrl("GET_ACCT_ITEM_FOR_DIAG_URL","");
            logger.debug("费用查询---url:{}" , url);
            //拼装入参
            JSONObject reqJson = new JSONObject();
            reqJson.put("lQuerySourceID", SystemEnum.SGW_ORG_ID.getValue());
            reqJson.put("sQuerySerial", UUIDHelper.currentTimeStampString());
            reqJson.put("sAreaCode", areaCode);
            reqJson.put("sAccNbr", body.getString("accNbr"));
            reqJson.put("sAcctNbr97", body.getString("acctNbr97"));
            reqJson.put("lGetFlag", body.getString("getFlag"));
            reqJson.put("lPrintFlag", body.getString("printFlag"));
            reqJson.put("sBeginDate", body.getString("beginDate"));
            reqJson.put("sEndDate", body.getString("endDate"));
            reqJson.put("lOnlyServ", body.getString("onlyServ"));
            reqJson.put("lFamilyId", body.getString("familyId"));
            logger.debug("费用查询调用入参:{}" , body);
            String resultString = HttpClientHelperForEop.postWithMNP(url, reqJson.toJSONString(),30000);
            logger.debug("费用查询 output:{}" , resultString);
            if(StringUtils.isNotEmpty(resultString)){
                JSONObject resultJson = JSON.parseObject(resultString);
                String errorCode = resultJson.getString("errorCode");
                String errorMsg = resultJson.getString("errorMsg");
                if("0".equals(errorCode)){
                    JSONObject retJson = new JSONObject();
                    JSONArray acctItemFeeList = new JSONArray();
                    retJson.put("errorCode", errorCode);
                    retJson.put("errorMsg", errorMsg);
                    JSONObject acctItemFeeJson = resultJson.getJSONObject("acctItemFee");
                    retJson.put("totalCharge", acctItemFeeJson.getString("lTotalCharge"));
                    JSONArray acctItemFeeDetailArr = acctItemFeeJson.getJSONArray("acctItemFeeDetail");
                    for(int i=0;i<acctItemFeeDetailArr.size();i++){
                        JSONObject acctItemFeeDetailItem = new JSONObject();
                        JSONObject acctItemFeeDetail = acctItemFeeDetailArr.getJSONObject(i);
                        acctItemFeeDetailItem.put("areaCode", acctItemFeeDetail.getString("sAreaCode"));
                        acctItemFeeDetailItem.put("accNbr", acctItemFeeDetail.getString("sAccNbr"));
                        acctItemFeeDetailItem.put("cycleEndDate", acctItemFeeDetail.getString("sCycleEndDate"));
                        acctItemFeeDetailItem.put("summaryItemID", acctItemFeeDetail.getString("lSummaryItemID"));
                        acctItemFeeDetailItem.put("summaryItemName", acctItemFeeDetail.getString("sSummaryItemName"));
                        acctItemFeeDetailItem.put("acctItemTypeId", acctItemFeeDetail.getString("lAcctItemTypeId"));
                        acctItemFeeDetailItem.put("acctItemTypeName", acctItemFeeDetail.getString("sAcctItemTypeName"));
                        acctItemFeeDetailItem.put("acctItemCharge", acctItemFeeDetail.getString("lAcctItemCharge"));
                        acctItemFeeDetailItem.put("offerId", acctItemFeeDetail.getString("lOfferId"));
                        acctItemFeeDetailItem.put("offerName", acctItemFeeDetail.getString("sOfferName"));
                        acctItemFeeDetailItem.put("status", acctItemFeeDetail.getString("sStatus"));
                        acctItemFeeDetailItem.put("statusDate", acctItemFeeDetail.getString("sStatusDate"));
                        acctItemFeeList.add(acctItemFeeDetailItem);
                    }
                    retJson.put("acctItemFeeList", acctItemFeeList);

                    response = Utils.dealSuccessResp(responseHead, tranId, retJson, responseJson);
                }else{
                    response = Utils.dealErrorResp(responseHead, tranId, resultJson, "02", "计费接口返回错误", responseJson);
                }
            }else{
                response = Utils.dealExceptionRespWithMsgAndStatus(responseHead, tranId, "计费接口返回为空", "01", responseJson);
            }
        } catch (Exception e) {
            response = Utils.dealExceptionResp(responseHead, tranId, responseJson);
            logger.debug("费用查询 Exception :{}" , e.getMessage());
            return response.toString();
        }
        return response.toString();
    }

    /**
     * 充值缴费记录查询
     * @Param
     * @Author xzy
     * @Date 2024/3/6 10:01
     */
    @Override
    public String getNewQrPay(String jsonParam) {
        logger.debug("充值缴费记录查询 input {}" , jsonParam);
        // 接口请求参数
        JSONObject response = null;
        RespInfo responseJson = new RespInfo();
        RespHead responseHead = new RespHead();
        responseJson.setHead(responseHead);
        String tranId = "";
        try {
            JSONObject requestJSON = JSON.parseObject(jsonParam);
            JSONObject head = requestJSON.getJSONObject("head");
            JSONObject body = requestJSON.getJSONObject("body");
            String areaCode = head.getString("areaCode");
            tranId = head.getString("tranId");//流水号
//            充值缴费查询(新)
//            http://jsjteop.telecomjs.com:8764/jseop/billing_zw/getNewQrPay/getNewQrPay
//            String url = BSUrlConstant.GET_NEW_QR_PAY_NEW_URL;
            String url  = pubAddressIpervice.getActionUrl("GET_NEW_QR_PAY_NEW_URL","");
            logger.debug("充值缴费记录查询 ---url{}" , url);
            //拼装入参
            JSONObject reqJson = new JSONObject();
            reqJson.put("lQuerySourceID", SystemEnum.SGW_ORG_ID.getValue());
            reqJson.put("sQuerySerial", UUIDHelper.currentTimeStampString());
            reqJson.put("sAreaCode", areaCode);
            reqJson.put("sAccNbr", body.getString("accNbr"));
            reqJson.put("sAcctNbr97", body.getString("acctNbr97"));
            reqJson.put("lGetFlag", body.getString("getFlag"));
            reqJson.put("sBeginDate", body.getString("beginDate"));
            reqJson.put("sEndDate", body.getString("endDate"));
            logger.debug("充值缴费记录查询 调用入参:{}" , body);
            String resultString = HttpClientHelperForEop.postWithMNP(url, reqJson.toJSONString(),30000);
            logger.debug("充值缴费记录查询 output:{}" , resultString);
            if(StringUtils.isNotEmpty(resultString)){
                JSONObject resultJson = JSON.parseObject(resultString);
                String errorCode = resultJson.getString("errorCode");
                String errorMsg = resultJson.getString("errorMsg");
                if("0".equals(errorCode)){
                    JSONObject retJson = new JSONObject();
                    JSONArray paymentInfoList = new JSONArray();
                    retJson.put("errorCode", errorCode);
                    retJson.put("errorMsg", errorMsg);
                    JSONObject paymentInfoLocalJson = resultJson.getJSONObject("paymentInfoLocal");
                    JSONArray paymentDetailArray = paymentInfoLocalJson.getJSONArray("paymentDetail");
                    for(int i=0;i<paymentDetailArray.size();i++){
                        JSONObject item = new JSONObject();
                        JSONObject paymentDetail = paymentDetailArray.getJSONObject(i);
                        item.put("paySerialNbr", paymentDetail.getString("lPaySerialNbr"));
                        item.put("sourceType", paymentDetail.getString("sSourceType"));
                        item.put("returnName", paymentDetail.getString("sReturnName"));
                        item.put("paymentMethodName", paymentDetail.getString("sPaymentMethodName"));
                        item.put("paymentCharge", paymentDetail.getString("lPaymentCharge"));
                        item.put("payDate", paymentDetail.getString("sPayDate"));
                        item.put("payTime", paymentDetail.getString("sPayTime"));
                        item.put("payLevel", paymentDetail.getString("lPayLevel"));
                        item.put("payNbr", paymentDetail.getString("sPayNbr"));
                        item.put("busRemark", paymentDetail.getString("sBusRemark"));
                        item.put("balanceType", paymentDetail.getString("sBalanceType"));
                        item.put("operType", paymentDetail.getString("sOperType"));
                        paymentInfoList.add(item);
                    }
                    retJson.put("paymentInfoList", paymentInfoList);

                    response = Utils.dealSuccessResp(responseHead, tranId, retJson, responseJson);
                }else{
                    response = Utils.dealErrorResp(responseHead, tranId, resultJson, "02", "计费接口返回错误", responseJson);
                }
            }else{
                response = Utils.dealExceptionRespWithMsgAndStatus(responseHead, tranId, "计费接口返回为空", "01", responseJson);
            }
        } catch (Exception e) {
            response = Utils.dealExceptionResp(responseHead, tranId, responseJson);
            logger.debug("充值缴费记录查询 Exception :{}" , e.getMessage());
            return response.toString();
        }
        return response.toString();
    }

}
