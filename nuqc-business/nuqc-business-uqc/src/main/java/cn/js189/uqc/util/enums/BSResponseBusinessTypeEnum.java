package cn.js189.uqc.util.enums;

import cn.js189.common.util.StringUtils;

public enum BSResponseBusinessTypeEnum {
    
  //通用业务查询
    QRY_COMMON("commonMethod","qryCommonTransform"),

	// bsn费用查询-10113 --> BS:费用查询
	GET_ACCT_ITEM_FQ_R("callFeeBillQuery", "getAcctItemFqrTransform"),

	// 一致性本月消费查询 --> BS:费用查询
	GET_ACCT_ITEM_FQ_R_CONSISTENCE("callAcctItemConsumqr", "getAcctItemFqrConsistenceTransform"),

	// 套餐使用情况查询 --> 量本使用查询 AccuUseQry-controller/AccuUseQry
	ACCU_USE_QRY("callCurrAcuReq","accuUseQryTransform"),

	// 套餐使用情况查询 --> 量本使用查询 AccuUseQry-controller/AccuUseQry 掌厅缓存用
	ACCU_USE_QRY_NEW("callCurrAcuReqNew","accuUseQryTransformNew"),

	// 流量卡有效期余额查询 - 流量卡余量查询
	GET_FLOW_ACCU_QR("callNewFlowCumulationQr","getFlowAccuQrTransform"),

	// 区域编码查询 - 号码归属地查询
	QRY_AREA_CD_BY_NBR("callNewAreaCode","qryAreaCdByNbrTransform"),

	// 20007余额查询 - 余额查询
	GET_BALANCE_SVR_BALANCE_QR("callBalanceQrReq","getBalanceSvrBalanceQrTransform"),

	// 宽带清单查询 - 宽带清单查询
	TKT_GU_BROAD_DATA_QR("callNewBroadDataQr","tktGuBroadDataQrTransform"),

	// 长话话单信息 - 长途清单查询
	TKT_GU_CALL_TICKET_QR("callNewCallTicket","tktGuCallTicketQrTransform"),
	
	//移动用户当月数据详单按天累计查询接口
	QRY_TKT_YI_VALUE_TICKET_QR_BY_DAYCOUNT("getTktYiValueTicketQrByDayCount","tktYiValueTicketQrByDayCountTransform"),

	// 16101-彩信E详单 - 彩E使用详单
	TKT_YI_CE_TICKET_QR("callCeTicketQr","tktYiCeTicketQrTransform"),

	// 流量卡充值记录查询 - 流量卡充值记录查询
	GET_FLOW_PAY_QR("callNewFlowPayQr","getFlowPayQrTransform"),

	// 流量卡使用详单查询 - 流量卡使用记录查询
	TGET_TKT_YI_F_FLOW_CARD_USERD_QR("callNewFlowDatauseQr","getTktYiFFlowCardUserdQrTransform"),

	// 互联星空清单 - 互联星空清单
	TKT_GU_INFO_DATA_QR("callNewInfoDataQr","tktGuInfoDataQrTransform"),

	// IPTV使用清单 - ITV清单
	TKT_GU_IPTV_DATA_QR("callNewIptvDataQr","tktGuIptvDataQrTransform"),

	// ITV点播清单 - ITV点播清单
	TKT_GU_IPTV_VALUE_ADDED_QR("callNewIptvValueAdderQr","tktGuIptvValueAddedQrTransform"),

	// 市话话单信息 - 市话清单
	TKT_GU_LS_TICKET_QR("callNewLsTicket","tktGuLsTicketQrTransform"),

	// 电话清单查询 - 电话信息清单
	TKT_GU_MESSAGE_TICKET_QR("callNewMessageTicketQr","tktGuMessageTicketQrTransform"),

	// 窄带清单查询 - 窄带清单
	TKT_GU_NARROW_DATA_QR("callNewNarrowDataQr","tktGuNarrowDataQrTransform"),

	// 账单查询 - 客户化帐单接口
	QRY_NEW_BILL_SVR("callNewBillReq","qryCustBillTransform"),

	// 移动用户上网及数据通信详单查询 - 移动用户上网及数据通信详单查询
	TKT_YI_NEW_DATA_TICKET_QR("callNewDataTicketQrReq","tktYiNewDataTicketQrTransform"),

	// 充值缴费查询 - 充值缴费记录查询
	GET_NEW_QR_PAY("callNewPayReq","getNewQrPayTransform"),

	// 一致性余额查询 - 余额查询
	QUERY_BALANCE("callConQueryBalance","queryBalanceTransform");


	// 原业务方法名
	private String oldMethod;
	// 实现类bean名称
	private String beanName;

	BSResponseBusinessTypeEnum(String oldMethod, String beanName) {
		this.oldMethod = oldMethod;
		this.beanName = beanName;
	}

	public static String getBeanName(String oldMethod) {
		for (BSResponseBusinessTypeEnum responseBusinessType : BSResponseBusinessTypeEnum.values()) {
			if (StringUtils.equals(responseBusinessType.oldMethod, oldMethod)) {
				return responseBusinessType.beanName;
			}
		}
		return null;
	}

}
