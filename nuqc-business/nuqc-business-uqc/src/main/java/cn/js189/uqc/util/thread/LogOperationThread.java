package cn.js189.uqc.util.thread;

import cn.hutool.extra.spring.SpringUtil;
import cn.js189.uqc.domain.LogOperation;
import cn.js189.uqc.mapper.CheckInfoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LogOperationThread implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger(LogOperationThread.class);

    private LogOperation logOperation;

    public LogOperationThread(LogOperation logOperation) {
        this.logOperation = logOperation;
    }

    @Override
    public void run() {

        try {

            logger.debug("业务操作数据库日志入库...");

            CheckInfoMapper checkInfoDao = SpringUtil.getBean("checkInfoDao");
            checkInfoDao.insertOperationLog(logOperation);

            logger.debug("业务操作数据库日志入库完成");

        } catch (Exception e) {
            logger.error("业务操作数据库入库插入异常：" + e.getMessage());
        }

    }

}
