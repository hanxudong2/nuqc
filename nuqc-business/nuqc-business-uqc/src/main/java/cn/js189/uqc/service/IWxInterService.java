package cn.js189.uqc.service;

/**
 * 微信接口相关
 */
public interface IWxInterService {
	
	/**
	 * 微信侧获取授权状态定时任务
	 * @param reqParams 参数
	 * @return 结果
	 */
	String getInvoiceAuthTask(String reqParams);
	
	/**
	 * 微信插卡定时任务
	 * @param reqParams 参数
	 * @return 结果
	 */
	String inserWxCardTask(String reqParams);
	
	/**
	 * 查询微信一次授权状态
	 * @param reqParams 参数
	 * @return 结果
	 */
	String checkBatchAuthInfo(String reqParams);
	
	/**
	 * 获取微信授权页url
	 * @param reqParams 参数
	 * @return 结果
	 */
	String getWxAuthUrl(String reqParams);
	
	/**
	 * 智能网查询
	 * @param reqParams 参数
	 * @return 结果
	 */
	String getZnwInfo(String reqParams);
	
}
