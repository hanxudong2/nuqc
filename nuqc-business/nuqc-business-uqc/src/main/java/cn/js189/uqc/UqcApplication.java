package cn.js189.uqc;

import cn.js189.common.util.annotation.EnableCustomConfig;
import cn.js189.common.util.annotation.EnableNFeignClients;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@Slf4j
@EnableCustomConfig
@ServletComponentScan
@EnableNFeignClients
@SpringBootApplication
public class UqcApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(UqcApplication.class,args);
		log.info("查询域服务启动成功......");
	}
	
}
