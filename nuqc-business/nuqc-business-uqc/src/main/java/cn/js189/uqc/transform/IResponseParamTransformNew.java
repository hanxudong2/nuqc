package cn.js189.uqc.transform;

import com.alibaba.fastjson.JSONObject;

/**
 * 智慧BSS，转换老接口出参
 * 
 */
public interface IResponseParamTransformNew {

	/**
	 * 参数转换
	 * 
	 * @param responseJSON: 智慧BSS返回JSON
	 * @param oldHead:老接口入参头信息
	 * @param oldBody：老接口入参body
	 * @return
	 */
	String transform(JSONObject responseJSON,JSONObject oldHead,JSONObject oldBody);
}
