package cn.js189.uqc.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.js189.common.constants.BSSUrlConstants;
import cn.js189.common.domain.ReqHead;
import cn.js189.common.domain.ReqInfo;
import cn.js189.common.domain.RespHead;
import cn.js189.common.domain.RespInfo;
import cn.js189.common.util.DateUtil;
import cn.js189.common.util.StringUtils;
import cn.js189.common.util.Utils;
import cn.js189.common.util.exception.BaseAppException;
import cn.js189.common.util.exception.ExceptionUtil;
import cn.js189.common.util.helper.DateHelper;
import cn.js189.common.util.helper.UUIDHelper;
import cn.js189.uqc.service.IBillService;
import cn.js189.uqc.service.PubAddressIpervice;
import cn.js189.uqc.util.*;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


@Service
public class BillServiceImpl implements IBillService {

    /**
     * 日志组件 <br>
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(BillServiceImpl.class);

    @Resource
    private PubAddressIpervice pubAddressIpervice;

    @Resource
    private BSService bsService;

    @Resource
    private BssBillService bssBillService;

    /**
     * 线上销户计费-余额查询接口
     * @param jsonParam 入参
     * @return
     */
    @Override
    public String queryBalance(String jsonParam) {
        // 接口请求参数
        JSONObject response = null;
        RespInfo responseJson = new RespInfo();
        RespHead responseHead = new RespHead();
        responseJson.setHead(responseHead);
        String tranId = "";
        String request = addRequestHead(jsonParam);
        try{
            JSONObject requestJson = JSON.parseObject(request);
            JSONObject head = requestJson.getJSONObject("head");
            JSONObject body = requestJson.getJSONObject("body");
            tranId = head.getString("tranId");
            LOGGER.info("线上销户计费-余额查询接口,入参:{}",body);
            /**
             * 线上销户计费-余额查询接口
             * test:132.228.119.42:8097
             * 正式：132.254.13.41:9009
             */
//            http://132.254.13.41:9009/adjust/account/thirdBalanceMove_queryBlanceToThird.do
            String url  = pubAddressIpervice.getActionUrl("QUERY_BALANCE_URL","");
            LOGGER.info("线上销户计费-余额查询接口,---url:{}",url);
            String result = HttpClientUtils.doPostJSONold(url, body.toJSONString());
            LOGGER.info("线上销户计费-余额查询接口,出参:{}",result);
            response = Utils.builderResponse(result, responseHead, responseJson, tranId,"resultCode","","resultMsg");
        }catch (Exception e){
            response = Utils.dealExceptionResp(responseHead, tranId, responseJson);
            LOGGER.info("线上销户计费-余额查询接口,出现异常:{}",e.getMessage());
        }
        return response.toString();
    }

    /**
     * 线上销户计费-余额提取或结转进度查询接口
     * @param jsonParam 入参
     * @return
     */
    @Override
    public String queryBalanceProgress(String jsonParam) {
        // 接口请求参数
        JSONObject response = null;
        RespInfo responseJson = new RespInfo();
        RespHead responseHead = new RespHead();
        responseJson.setHead(responseHead);
        String tranId = "";
        String request = addRequestHead(jsonParam);
        try{
            JSONObject requestJson = JSON.parseObject(request);
            JSONObject head = requestJson.getJSONObject("head");
            JSONObject body = requestJson.getJSONObject("body");
            tranId = head.getString("tranId");
            LOGGER.info("线上销户计费-余额提取或结转接口,入参:{}",body);
            /**
             * 线上销户计费-余额提取或结转进度查询接口
             * test:132.254.13.41:9009
             * 正式：132.254.13.41:9009
             */
//            http://132.254.13.41:9009/adjust/account/thirdBalanceMove_queryBalanceProgress.do
            String url  = pubAddressIpervice.getActionUrl("QUERY_BALANCE_PROGRESS_URL","");
            LOGGER.info("线上销户计费-余额提取或结转接口,---url:{}",url);
            String result = HttpClientUtils.doPostJSONold(url, body.toJSONString());
            LOGGER.info("线上销户计费-余额提取或结转接口,出参:{}",result);
            response = Utils.builderResponse(result, responseHead, responseJson, tranId,"resultCode","","resultMsg");
        }catch (Exception e){
            response = Utils.dealExceptionResp(responseHead, tranId, responseJson);
            LOGGER.info("线上销户计费-余额提取或结转接口,出现异常:{}",e.getMessage());
        }
        return response.toString();
    }

    private String addRequestHead(String body){
        JSONObject requestJson = new JSONObject();
        JSONObject headJson = new JSONObject();
        headJson.put("requestTime","2020082015125458052");
        headJson.put("tranId","20200820151254952673");
        headJson.put("auth","f0bad85e8523f1f3c9e28097f9333f23");
        headJson.put("actionCode","20007");
        headJson.put("channelId","ZT");
        requestJson.put("head",headJson);
        requestJson.put("body",body);
        return requestJson.toString();
    }

    @Override
    public String accuUseQry(String jsonParam) {
        // 接口请求参数
        JSONObject response = null;
        RespInfo responseJson = new RespInfo();
        RespHead responseHead = new RespHead();
        responseJson.setHead(responseHead);
        String tranId = "";
        try {
            JSONObject requestJSON = JSON.parseObject(jsonParam);

            JSONObject head = requestJSON.getJSONObject("head");
            JSONObject body = requestJSON.getJSONObject("body");

            String areaCode = head.getString("areaCode");
            String accNbr = body.getString("accNbr");
            String family = body.getString("family");
            String billingCycle = body.getString("billingCycle");
            tranId = head.getString("tranId");//流水号
            //调用地址：http://jsjteop.telecomjs.com:8764/jseop/billing_zw/ECAccuUseQry
            String url  = pubAddressIpervice.getActionUrl("accuUseQry","");
            //拼装入参
            JSONObject paramJson = CreateParamForAccountingCenter.createParamForQryAccuUse(accNbr, family ,areaCode,billingCycle);
            LOGGER.debug("三期一致性累积量-211109 调用--url:{}" , url);
            LOGGER.debug("三期一致性累积量-211109 调用入参:{}" , paramJson);
            String resultString = HttpClientHelperForEop.postWithMNP(url, paramJson.toJSONString());
            LOGGER.debug("三期一致性累积量-211109 output:{}" , resultString);
            response = Utils.builderResponse(resultString, responseHead, responseJson, tranId,"resultCode","","resultMsg");
        } catch (Exception e) {
            response = Utils.dealExceptionResp(responseHead, tranId, responseJson);
            LOGGER.debug("三期一致性累积量-211109 Exception :{}" , e.getMessage());
            return response.toString();
        }
        return response.toString();
    }


    @Override
    public String callFeeBillQuery(String jsonParam) {
        // 接口请求参数
        JSONObject response = null;
        RespInfo responseJson = new RespInfo();
        RespHead responseHead = new RespHead();
        responseJson.setHead(responseHead);
        String tranId = "";
        try {
            ReqInfo reqInfo = BeanUtil.toBean(jsonParam,ReqInfo.class);
            JSONObject body = reqInfo.getBody();
            String result = bsService.callFeeBillQueryNew(reqInfo);
            ReqHead reqHead = reqInfo.getHead();
            return EuaCacheUtil.buildBillResponse(body, reqHead, result);
        } catch (Exception e) {
            response = Utils.dealExceptionResp(responseHead, tranId, responseJson);
            LOGGER.debug("(callFeeBillQuery)bsn费用查询-10113 Exception :{}" , e.getMessage());
            return response.toString();
        }
    }



    protected RespInfo disposeRespJson(RespInfo respInfo, String reqTreanId) {
        respInfo.getHead().setResponseId("RSP" + reqTreanId);
        respInfo.getHead().setResponseTime(DateUtil.getCurrentTime());
        return respInfo;
    }

    @Override
    public String callNewBillReq(String jsonParam) {
        LOGGER.debug("账单查询接口原始入参={}",jsonParam);
        // 接口请求参数
        ReqInfo reqInfo = new ReqInfo();
        // 接口响应信息
        RespInfo respInfo = new RespInfo();
        String serialId = UUIDHelper.currentTimeStampString();// 此接口调用的唯一标识
        // 请求参数-body
        try {
            reqInfo = BeanUtil.toBean(jsonParam,ReqInfo.class);
            JSONObject body = reqInfo.getBody();
            body.put("areaCode", reqInfo.getHead().getAreaCode());
            body.put("serialId", serialId);
            body.put("accNbr", reqInfo.getHead().getLoginNbr());
            LOGGER.debug("账单查询接口入参={}",JSON.toJSON(body));
            // 余额查询参数组装
            JSONObject jsonObj = bssBillService.callNewBillReq(body);
            if(!"0".equals(jsonObj.getString("TSR_RESULT"))){
                respInfo.getHead().setStatusAndMsg(jsonObj.getString("TSR_CODE"), jsonObj.getString("TSR_MSG"));
            }else{
                respInfo.setBody(jsonObj);
            }
        } catch (Exception e) {
            LOGGER.debug("CallNewBillReq new charge Exception :{}" , e.getMessage());
            respInfo.getHead().setStatusAndMsg("99", "服务内部错误");
            ExceptionUtil.noThrowException(e);
        }finally{
            respInfo = disposeRespJson(respInfo, reqInfo.getHead().getTranId());
        }
        return JSON.toJSONString(respInfo);
    }


    /**
     * Description: 长话话单信息-10171<br>
     *
     * @author xzy<br>
     * @taskId <br>
     * @param jsonParam
     * @return <br>
     */
    @Override
    public String callNewCallTicket(String jsonParam) {
        // 接口请求参数
        JSONObject response = null;
        RespInfo responseJson = new RespInfo();
        RespHead responseHead = new RespHead();
        responseJson.setHead(responseHead);
        String tranId = "";
        try {
            ReqInfo reqInfo = BeanUtil.toBean(jsonParam,ReqInfo.class);
            return this.bsService.callNewCallTicketNew(reqInfo);
        } catch (Exception e) {
            response = Utils.dealExceptionResp(responseHead, tranId, responseJson);
            LOGGER.debug("长话话单信息-10171 Exception :{}" , e.getMessage());
            return response.toString();
        }
    }

    /**
     * Description: 无线宽带上网清单-16102<br>
     *
     * @author xzy<br>
     * @taskId <br>
     * @param jsonParam
     * @return <br>
     */
    @Override
    public String callNewDataTicketQrReq(String jsonParam) {
        JSONObject response = null;
        LOGGER.debug("无线宽带上网清单-16102 input:{}",jsonParam);
        RespInfo responseJson = new RespInfo();
        RespHead responseHead = new RespHead();
        responseJson.setHead(responseHead);
        //获得接口号流水
        String tranId = "";
        try {
            ReqInfo reqInfo = BeanUtil.toBean(jsonParam,ReqInfo.class);
            return this.bsService.callNewDataTicketQrReqNew(reqInfo);
        } catch (Exception e) {
            response = Utils.dealExceptionResp(responseHead, tranId, responseJson);
            LOGGER.debug("无线宽带上网清单-16102 Exception :{}",e.getMessage());
            return response.toString();
        }
    }

    /**
     * Description: 新业务清单查询-16110<br>
     *
     * @author xzy<br>
     * @taskId <br>
     * @param jsonParam
     * @return <br>
     */
    @Override
    public String callNewHttpSvrTicQr(String jsonParam) {
        JSONObject response = null;
        RespInfo responseJson = new RespInfo();
        RespHead responseHead = new RespHead();
        responseJson.setHead(responseHead);
        //获得接口号流水
        String tranId = "";
        try {
            JSONObject requestJson = JSON.parseObject(jsonParam);
            JSONObject head = requestJson.getJSONObject("head");// head体
            JSONObject body = requestJson.getJSONObject("body");// body体
            tranId = head.getString("tranId");//流水号
            String areaCode = head.getString("areaCode");//区号
            String begDate = body.getString("begDate");
            String endDate = body.getString("endDate");
            String getFlag = DateHelper.isAllMonthFlag(begDate, endDate)?"1":"3";
            String accNbr = body.getString("accNbr");
            String productId = body.getString("productId");
            //计费新接口对接
            //包含区号的完整电话号码
            String entireAccNbr = areaCode+accNbr;
            JSONObject parmaJson= CreateParamForAccountingCenter.createParamForNewSvrTic(entireAccNbr, getFlag, begDate, endDate, productId);
            //http://jsjteop.telecomjs.com:8764/jseop/billing_zw/TktGuNewSvrTicQr/TktGuNewSvrTicQr
            String  newUrl =  BSSUrlConstants.NEW_SVR_TIC_GU ;
            String resultString = HttpClientHelperForEop.postToBill(newUrl, parmaJson.toJSONString(),areaCode);
            LOGGER.info("callNewHttpSvrTicQr 新业务清单查询-16110查询 出参:{}", resultString);
            response = HandleParamForAccountingCenter.builderResponseForNewSvrTic(resultString, responseHead, responseJson, tranId);
            return response.toString();
        } catch (Exception e) {
            response = Utils.dealExceptionResp(responseHead, tranId, responseJson);
            LOGGER.debug("新业务清单查询-16110 Exception :{}" , e.getMessage());
            return response.toString();
        }
    }

    /**
     * Description: 互联星空清单查询-16109<br>
     *
     * @author xzy<br>
     * @taskId <br>
     * @param jsonParam
     * @return <br>
     */
    @Override
    public String callNewInfoDataQr(String jsonParam) {
        JSONObject response = null;
        RespInfo responseJson = new RespInfo();
        RespHead responseHead = new RespHead();
        responseJson.setHead(responseHead);
        //获得接口号流水
        String tranId = "";
        try {
            ReqInfo reqInfo = BeanUtil.toBean(jsonParam,ReqInfo.class);
            return this.bsService.callNewInfoDataQrNew(reqInfo);
        } catch (Exception e) {
            response = Utils.dealExceptionResp(responseHead, tranId, responseJson);
            LOGGER.debug("互联星空清单查询-16109 Exception :{}" , e.getMessage());
            return response.toString();
        }
    }

    /**
     * Description: 电话清单查询-16111<br>
     *
     * @author xzy<br>
     * @taskId <br>
     * @param jsonParam
     * @return <br>
     */
    @Override
    public String callNewMessageTicketQr(String jsonParam) {
        JSONObject response = null;
        RespInfo responseJson = new RespInfo();
        RespHead responseHead = new RespHead();
        responseJson.setHead(responseHead);
        //获得接口号流水
        String tranId = "";
        try {
            ReqInfo reqInfo = BeanUtil.toBean(jsonParam,ReqInfo.class);
            return this.bsService.callNewMessageTicketQrNew(reqInfo);
        } catch (Exception e) {
            response = Utils.dealExceptionResp(responseHead, tranId, responseJson);
            LOGGER.debug("电话清单查询-16111 Exception :{}" , e.getMessage());
            return response.toString();
        }
    }

    /**
     * Description: 窄带清单查询-16108<br>
     *
     * @author xzy<br>
     * @taskId <br>
     * @param jsonParam
     * @return <br>
     */
    @Override
    public String callNewNarrowDataQr(String jsonParam) {
        JSONObject response = null;
        LOGGER.debug("窄带清单查询-16108 input:{}",jsonParam);
        RespInfo responseJson = new RespInfo();
        RespHead responseHead = new RespHead();
        responseJson.setHead(responseHead);
        //获得接口号流水
        String tranId = "";
        try {
            ReqInfo reqInfo = BeanUtil.toBean(jsonParam,ReqInfo.class);
            return this.bsService.callNewNarrowDataQrNew(reqInfo);
        } catch (Exception e) {
            response = Utils.dealExceptionResp(responseHead, tranId, responseJson);
            LOGGER.debug("窄带清单查询-16108 Exception :{}" , e.getMessage());
            return response.toString();
        }
    }

    /**
     * Description: 账户欠费查询<br>
     *
     * @author xzy<br>
     * @taskId <br>
     * @param jsonParam
     * @return <br>
     */
    @Override
    public String callNewOthGetOwe(String jsonParam) {
        LOGGER.debug("欠费查询接口原始入参={}",jsonParam);
        // 接口请求参数
        ReqInfo reqInfo = new ReqInfo();
        // 接口响应信息
        RespInfo respInfo = new RespInfo();
        try {
            reqInfo = BeanUtil.toBean(jsonParam,ReqInfo.class);
            JSONObject body = reqInfo.getBody();
            body.put("areaCode", reqInfo.getHead().getAreaCode());
            body.put("accNbr", reqInfo.getHead().getLoginNbr());
            JSONObject jsonObj = bssBillService.callNewOthGetOwe(body);
            if(!"0".equals(jsonObj.getString("TSR_RESULT"))){
                respInfo.getHead().setStatusAndMsg(jsonObj.getString("TSR_CODE"), jsonObj.getString("TSR_MSG"));
            }else{
                respInfo.setBody(jsonObj);
            }
        }catch(BaseAppException bse){
            LOGGER.debug("业务异常编码={}，异常信息={}",bse.getCode(), bse.getMsg());
            respInfo.getHead().setStatusAndMsg(bse.getCode(), bse.getMsg());
        } catch (Exception e) {
            LOGGER.debug("callNewOthGetOwe new charge Exception :{}" , e.getMessage());
            respInfo.getHead().setStatusAndMsg("9999", "系统异常");
            ExceptionUtil.noThrowException(e);
        }finally{
            respInfo = disposeRespJson(respInfo, reqInfo.getHead().getTranId());
        }
        return JSON.toJSONString(respInfo);
    }

    /**
     * Description: C网短信/彩信增值详单-16107<br>
     *
     * @author xzy<br>
     * @taskId <br>
     * @param jsonParam
     * @return <br>
     */
    @Override
    public String callNewSmsValueTicketQr(String jsonParam) {
        LOGGER.debug("callNewSmsValueTicketQr C网短信/彩信增值详单查询接口原始入参={}",jsonParam);
        // 接口请求参数
        ReqInfo reqInfo = new ReqInfo();
        // 接口响应信息
        String serialId = UUIDHelper.currentTimeStampString();// 此接口调用的唯一标识
        RespInfo responseJson = new RespInfo();
        RespHead responseHead = new RespHead();
        responseJson.setHead(responseHead);
        JSONObject response = null;
        //  获得接口号流水
        String tranId = "";
        try {
            reqInfo = BeanUtil.toBean(jsonParam,ReqInfo.class);
            JSONObject body = reqInfo.getBody();
            tranId = reqInfo.getHead().getTranId();
            body.put("areaCode", reqInfo.getHead().getAreaCode());
            body.put("accNbr", reqInfo.getBody().getString("accNbr"));
            body.put("serialId", serialId);

            if (StringUtils.isBlank(body.getString("areaCode"))) {
                ExceptionUtil.throwBusiException("uqc_1000");
            }
            // 号码校验
            if (StringUtils.isBlank(body.getString("accNbr"))) {
                ExceptionUtil.throwBusiException("uqc_1001");
            }
//          http://jsjteop.telecomjs.com:8764/jseop/billing_zw/TktYiSmsValueTicketQr/TktYiSmsValueTicketQr
            String newUrl = BSSUrlConstants.TKT_YI_SMS_VALUE_TICKET_QR;
            String familyId =body.getString("familyId");
            String accNbr = body.getString("accNbr");
            String begDate = body.getString("begDate");
            String endDate = body.getString("endDate");
//          String getFlag = MainUtils.getNullEmptyForReq(body.getString("getFlag"))
            String getFlag = DateHelper.isAllMonthFlag(begDate, endDate)?"1":"3";
            JSONObject paramJson = CreateParamForAccountingCenter.createParamForSmTicketQr(accNbr, getFlag, begDate, endDate, familyId);
            String resultString = HttpClientHelperForEop.postWithMNP(newUrl, paramJson.toJSONString());
            LOGGER.info("callNewSmsValueTicketQr C网短信/彩信增值详单-16107查询 出参:{}", resultString);
            response = HandleParamForAccountingCenter.builderResponseForSmsValueTicketQr(resultString, responseHead, responseJson, tranId);
            return response.toString();
        }catch (Exception e) {
            response = Utils.dealExceptionResp(responseHead, tranId, responseJson);
            LOGGER.debug("callNewSmsValueTicketQr C网短信/彩信增值详单查询 Exception :{}" , e.getMessage());
            return response.toString();
        }
    }


    /**
     * callNewSmTicket
     * bsn短信详单查询-10135
     */
    @Override
    public String callNewSmTicketQr(String jsonParam) {
        // 接口请求参数
        JSONObject response = null;
        RespInfo responseJson = new RespInfo();
        RespHead responseHead = new RespHead();
        responseJson.setHead(responseHead);
        String tranId = "";
        try {
            JSONObject requestJSON = JSON.parseObject(jsonParam);
            JSONObject head = requestJSON.getJSONObject("head");
            JSONObject body = requestJSON.getJSONObject("body");
            tranId = head.getString("tranId");//流水号
            //11 25 计费重新对接改动
//            http://jsjteop.telecomjs.com:8764/jseop/billing_zw/TktYiVSmTicketQr/TktYiVSmTicketQr
            String newUrl = BSSUrlConstants.TKT_YI_V_SM_TICKET_QR ;
            String familyId =body.getString("familyId");
            String accNbr = body.getString("accNbr");
//            String getFlag = MainUtils.getNullEmptyForReq(body.getString("getFlag"))
            String begDate = body.getString("begDate");
            String endDate = body.getString("endDate");
            String getFlag = DateHelper.isAllMonthFlag(begDate, endDate)?"1":"3";
            JSONObject paramJson = CreateParamForAccountingCenter.createParamForSmTicketQr(accNbr, getFlag, begDate, endDate, familyId);
            String resultString = HttpClientHelperForEop.postWithMNP(newUrl, paramJson.toJSONString());
            LOGGER.info("callNewSmTicketQr bsn短信详单查询-10135查询出参:{}", resultString);
            response = HandleParamForAccountingCenter.builderResponseForSmTicketQr(resultString, responseHead, responseJson, tranId);
            return response.toString();
            //-----------1125修改结束
        } catch (Exception e) {
            response = Utils.dealExceptionResp(responseHead, tranId, responseJson);
            LOGGER.debug("(callNewSmTicketQr)bsn短信详单查询-10135 Exception :{}" , e.getMessage());
            return response.toString();
        }
    }

    /**
     * Description: 其他增值业务清单查询<br>
     *
     * @author xzy<br>
     * @taskId <br>
     * @param jsonParam
     * @return <br>
     */
    @Override
    public String callNewValueTicketQr(String jsonParam) {
        LOGGER.debug("其他增值业务清单查询接口原始入参={}",jsonParam);
        // 接口请求参数
        ReqInfo reqInfo = new ReqInfo();
        JSONObject response = null;
        // 接口响应信息
        RespInfo responseJson = new RespInfo();
        RespHead responseHead = new RespHead();
        responseJson.setHead(responseHead);
        //  获得接口号流水
        String tranId = "";
        try {
            reqInfo = BeanUtil.toBean(jsonParam,ReqInfo.class);
            JSONObject body = reqInfo.getBody();
            body.put("areaCode", reqInfo.getHead().getAreaCode());
            tranId = reqInfo.getHead().getTranId();


            JSONObject resultjson =null;
//            http://jsjteop.telecomjs.com:8764/jseop/billing_zw/TktYiValueTicketQr/TktYiValueTicketQr
            String newUrl = BSSUrlConstants.TKT_YI_VALUE_TICKET_QR;

            String accNbr = body.getString("accNbr");
            String begDate = body.getString("begDate");
            String endDate = body.getString("endDate");
            String familyId =  body.getString("familyId");
            String getFlag = DateHelper.isAllMonthFlag(begDate, endDate)?"1":"3";

            JSONObject paramJson = CreateParamForAccountingCenter.createParamForSmTicketQr(accNbr, getFlag, begDate, endDate,familyId);
            String resultString = HttpClientHelperForEop.postWithMNP(newUrl, paramJson.toJSONString());
            LOGGER.debug("callNewValueTicketQr(其他增值业务清单查询接口) output :{}" , resultString);
            resultjson = HandleParamForAccountingCenter.builderResponseForTktYiValueTicketQr(resultString);
            response = Utils.builderResponse(resultjson.toString(), responseHead, responseJson, tranId,"iresult","","smsg");
            if(response.getJSONObject("head").getString("status").equals("00")){
                String jsonArrStr = response.getJSONObject("body").getString("valueTicketgulist");
                if(!jsonArrStr.startsWith("[")){
                    JSONObject jsonObject = response.getJSONObject("body").getJSONObject("valueTicketgulist");
                    JSONArray jsonArr = new JSONArray();
                    jsonArr.add(jsonObject);
                    response.getJSONObject("body").remove("valueTicketgulist");
                    response.getJSONObject("body").put("valueTicketgulist", jsonArr);
                }
            }
        }catch (Exception e) {
            response = Utils.dealExceptionResp(responseHead, tranId, responseJson);
            LOGGER.debug("callNewValueTicketQr(其他增值业务清单查询接口) Exception :{}" , e.getMessage());
            return response.toString();
        }
        return response.toString();
    }

    /**
     * Description: 手机语音清单<br>
     *
     * @author xzy<br>
     * @taskId <br>
     * @param jsonParam
     * @return <br>
     */
    @Override
    public String callNewVoiceTicket(String jsonParam) {
        LOGGER.debug("callNewVoiceTicket 手机语音清单查询接口原始入参={}",jsonParam);
        // 接口请求参数
        ReqInfo reqInfo = new ReqInfo();
        // 接口响应信息
        JSONObject response = null;
        RespInfo responseJson = new RespInfo();
        RespHead responseHead = new RespHead();
        responseJson.setHead(responseHead);
        //  获得接口号流水
        String tranId = "";
        try {
            reqInfo = BeanUtil.toBean(jsonParam,ReqInfo.class);
            JSONObject body = reqInfo.getBody();
            body.put("areaCode", reqInfo.getHead().getAreaCode());
            tranId = reqInfo.getHead().getTranId();
            String rsp = bssBillService.callNewVoiceTicket(body);
            LOGGER.debug("callNewVoiceTicket(手机语音清单) output :{}" , rsp);
            response = JSON.parseObject(rsp);
            LOGGER.debug("callNewVoiceTicket(手机语音清单) output :{}" , response);
        }catch (Exception e) {
            response = Utils.dealExceptionResp(responseHead, tranId, responseJson);
            LOGGER.debug("callNewVoiceTicket(手机语音清单) Exception :{}" , e.getMessage());
            return response.toString();
        }
        return response.toString();
    }


    /**
     * Description: 查询语音，短信，流量，增值清单<br>
     *
     * @author xzy<br>
     * @taskId <br>
     * @param jsonParam
     * @return <br>
     */
    @Override
    public String queryBillItems(String jsonParam) {
        // 接口请求参数
        JSONObject response = null;
        RespInfo responseJson = new RespInfo();
        RespHead responseHead = new RespHead();
        responseJson.setHead(responseHead);
        String tranId = "";
        try {
            JSONObject requestJSON = JSON.parseObject(jsonParam);

            JSONObject head = requestJSON.getJSONObject("head");
            JSONObject body = requestJSON.getJSONObject("body");

            String areaCode = head.getString("areaCode");
            String accNbr = body.getString("accNbr");
            String family = body.getString("family");
            String billingCycle = body.getString("billingCycle");
            String startDate =  body.getString("startDate");
            String endDate = body.getString("endDate");
            String qryType = body.getString("qryType");
            String page = body.getString("page");
            String row = body.getString("row");
            tranId = head.getString("tranId");//流水号
            String dataArea = "1";
//            http://jsjteop.telecomjs.com:8764/jseop/billing_zw/ECQueryBillItems
            String url  = BSSUrlConstants.CON_QUERY_BILL_ITEMS;
            //拼装入参
            JSONObject paramJson = CreateParamForAccountingCenter.createParamForQryBillItems(accNbr, family ,billingCycle,areaCode,dataArea,startDate,endDate,qryType,page,row);
            LOGGER.debug("详单查询-2111041 调用入参:{}" , paramJson);
            String resultString = HttpClientHelperForEop.postWithMNP(url, paramJson.toJSONString(),15000);
            LOGGER.debug("详单查询-2111041 output:{}" , resultString);
            response = Utils.builderResponse(resultString, responseHead, responseJson, tranId,"resultCode","","resultMsg");
        } catch (Exception e) {
            response = Utils.dealExceptionResp(responseHead, tranId, responseJson);
            LOGGER.debug("详单查询-2111041 Exception :{}" , e.getMessage());
            return response.toString();
        }
        return response.toString();
    }

}
