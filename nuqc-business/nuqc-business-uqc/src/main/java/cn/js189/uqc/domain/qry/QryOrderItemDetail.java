package cn.js189.uqc.domain.qry;

import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * 通过证件号码的方式查询客户信息，生成请求报文
 * 
 * @author cx
 */
public class QryOrderItemDetail extends QryOrderItemDetailBase {
	// 日志组件
	private static final Logger LOGGER = LoggerFactory.getLogger(QryOrderItemDetail.class);

	private List<String> orderItemIds;
	
	public QryOrderItemDetail(String regionId, List<String> orderItemIds,List<String> scopeInfos ) {
		super(regionId, scopeInfos);
		this.orderItemIds=orderItemIds;
	}

	@Override
	protected JSONObject improveRequestObject(JSONObject requestObject) {
		requestObject.put("regionId", regionId);
		requestObject.put("orderItemIds", orderItemIds);
		LOGGER.debug("requestObject:{}", requestObject);
		return requestObject;
	}

}
