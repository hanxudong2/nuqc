package cn.js189.uqc.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.js189.common.constants.BSSUrlConstants;
import cn.js189.common.domain.ReqHead;
import cn.js189.common.domain.ReqInfo;
import cn.js189.common.domain.RespInfo;
import cn.js189.uqc.service.ICpCpService;
import cn.js189.uqc.service.PubAddressIpervice;
import cn.js189.uqc.util.HttpClientUtils;
import cn.js189.uqc.util.UqcAddressCodeConstants;
import cn.js189.common.util.Utils;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 企信cpcp接口
 */
@Slf4j
@Service
public class CpCpServiceImpl implements ICpCpService {
	
	@Resource
	private PubAddressIpervice pubAddressIpervice;
	
	
	@Override
	public String cpCpQryOfferGrpRela(String reqParams) {
		JSONObject response = null;
		RespInfo responseJson = new RespInfo();
		String tranId="";
		try{
			ReqInfo reqInfo = BeanUtil.toBean(reqParams, ReqInfo.class);
			ReqHead head = reqInfo.getHead();// head体
			tranId = head.getTranId();
			JSONObject body = reqInfo.getBody();
			log.debug("cpcpQryOfferGrpRela  body :{}" ,body);
			String actionUrl = pubAddressIpervice.getActionUrl(UqcAddressCodeConstants.EOP_CPC_PPM_QRY_OFFER, head.getLoginNbr());
			String returnResult = HttpClientUtils.doPostJSONHttpsNewEOP(actionUrl, body.toString());
			log.debug("cpcpQryOfferGrpRela  returnResult :{}" ,returnResult);
			return Utils.builderResponse(returnResult, responseJson.getHead(), responseJson, tranId,"resultCode","","").toString();
		} catch (Exception e) {
			response = Utils.dealExceptionResp(responseJson.getHead(), tranId, responseJson);
			log.debug("cpcpQryOfferGrpMembers Exception :{}",e.getMessage());
			return response.toString();
		}
	}
	
	@Override
	public String cpCpQryOfferList(String reqParams) {
		JSONObject response = null;
		RespInfo responseJson = new RespInfo();
		String tranId="";
		try{
			ReqInfo reqInfo = BeanUtil.toBean(reqParams, ReqInfo.class);
			ReqHead head = reqInfo.getHead();// head体
			tranId = head.getTranId();
			JSONObject body = reqInfo.getBody();
			JSONArray reqJSONArray = new JSONArray();
			reqJSONArray.add(body);
			String returnResult = HttpClientUtils.doPostJSONHttps(BSSUrlConstants.EOP_CPC_PPM_QRYOFFERLIST, reqJSONArray.toString());
			return Utils.builderResponse(returnResult, responseJson.getHead(), responseJson, tranId,"resultCode","","").toString();
		} catch (Exception e) {
			response = Utils.dealExceptionResp(responseJson.getHead(), tranId, responseJson);
			log.debug("cpcpQryOfferList Exception :{}",e.getMessage());
			return response.toString();
		}
	}
}
