package cn.js189.uqc.service;

public interface ICpCpService {
	
	/**
	 * 企信销售品互斥依赖关系配置查询
	 * @param reqParams 参数
	 * @return 结果
	 */
	String cpCpQryOfferGrpRela(String reqParams);
	
	/**
	 * cpcp根据规格查询销售品列表
	 * @param reqParams 参数
	 * @return 结果
	 */
	String cpCpQryOfferList(String reqParams);
	
}
