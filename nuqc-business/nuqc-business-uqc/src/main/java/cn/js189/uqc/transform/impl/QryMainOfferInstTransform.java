package cn.js189.uqc.transform.impl;

import cn.js189.uqc.transform.IResponseParamTransform;
import cn.js189.uqc.util.BSSHelper;
import cn.js189.uqc.util.StaticDataMappingUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class QryMainOfferInstTransform implements IResponseParamTransform {

    // 日志组件
    private static final Logger LOGGER = LoggerFactory.getLogger(QryMainOfferInstTransform.class);

    @Override
    public String transform(JSONObject responseJSON) {
        //15051根据accNbr或者accNum查询主销售品
        JSONObject res = new JSONObject();
        JSONObject body = new JSONObject();
        JSONObject offerrecords = new JSONObject();
        JSONArray offerrecord = new JSONArray();
        //拿到企信出参中业务节点 resultObject
        JSONArray arr = responseJSON.getJSONArray("offerInstDetailList");
        for(int i = 0;arr.size() > 0 && i < arr.size();i++){
            JSONObject offerInstDetail = arr.getJSONObject(i).getJSONObject("offerInstDetail");
            JSONArray offerProdInstRels = JSONArray.parseArray(offerInstDetail.getString("offerProdInstRels"));
            String statusCd = offerInstDetail.getString("statusCd");
            if(!"1100".equals(statusCd)){
                JSONObject offerrecordObj = new JSONObject();
                offerrecordObj.put("offertypecd", "待转换");//待确认
                offerrecordObj.put("partyname", "");
                offerrecordObj.put("offertypename", StaticDataMappingUtil.getStatucCdToNewMap(offerInstDetail.getString("statusCd")));
                offerrecordObj.put("offerstatename", "主销售品");
                offerrecordObj.put("startdt", dateChange(offerInstDetail.getString("createDate")));
                offerrecordObj.put("offerid", offerInstDetail.getString("offerInstId"));
                offerrecordObj.put("offerstatuscd", offerInstDetail.getString("offerType"));
                offerrecordObj.put("areaid", StaticDataMappingUtil.getBssToDubAreaMap(offerInstDetail.getString("regionId")));
                offerrecordObj.put("offerspecid", offerInstDetail.getString("offerId"));
                offerrecordObj.put("enddt", dateChange(offerInstDetail.getString("effDate")));
                offerrecordObj.put("offerspecname", offerInstDetail.getString("offerName"));
                offerrecordObj.put("partyid", offerInstDetail.getString("ownerCustId"));

                JSONObject offermembers = new JSONObject();
                JSONArray offermember = new JSONArray();
                //循环新接口的offerProdInstRels集合
                for (int j = 0; j < offerProdInstRels.size(); j++) {
                    JSONObject offermemberObj = new JSONObject();
                    //获取offerProdInstRels集合中的对象
                    JSONObject offerProdInstRelsObj = offerProdInstRels.getJSONObject(j);
                    JSONObject prodInst = offerProdInstRelsObj.getJSONObject("prodInst");
                    if(prodInst==null){//added by yangyang优化产品已失效，但是销售品和产品关联关系未失效的数据bug
                        continue;
                    }
                    offermemberObj.put("offermemberid", offerProdInstRelsObj.getString("offerProdInstRelId"));
                    offermemberObj.put("accessnumber", prodInst.getString("accNum"));
                    String prodUseType = offerProdInstRelsObj.getString("prodUseType");
                    offermemberObj.put("objtype", "待转换");//待转换
                    offermemberObj.put("offerroleid", "待转换");//待转换
                    offermemberObj.put("offermembstatuscd", "待转换");//待转换
                    offermemberObj.put("areaid", StaticDataMappingUtil.getBssToDubAreaMap(offerProdInstRelsObj.getString("regionId")));
                    offermemberObj.put("rolecd", offerProdInstRelsObj.getString("roleId"));
                    offermemberObj.put("prodid", offerProdInstRelsObj.getString("prodInstId"));
                    String prodId = offerProdInstRelsObj.getString("prodId");
                    offermemberObj.put("prodspecid", prodId.substring(prodId.lastIndexOf("0") + 1));
                    if(offermemberObj.getString("prodspecid").equals("9")){
                        //如果是宽带，需要带入netAccount
                        offermemberObj.put("netaccount", prodInst.getString("account"));
                    }
                    offermemberObj.put("feetypename", StaticDataMappingUtil.getPaymentModeCdNameMap(prodInst.getString("paymentModeCd")));
                    offermemberObj.put("prodspecname", prodInst.getString("prodName"));
                    offermemberObj.put("partyname", "");
                    offermemberObj.put("rolename", offerProdInstRelsObj.getString("roleName"));
                    offermemberObj.put("offermembstatename", StaticDataMappingUtil.getStatucCdToNewMap(offerProdInstRelsObj.getString("statusCd")));
                    offermemberObj.put("feetype", StaticDataMappingUtil.getPaymentModeCdMap(prodInst.getString("paymentModeCd")));
                    offermemberObj.put("objname", "待转换");//待转换
                    offermemberObj.put("partyid", prodInst.getString("ownerCustId"));

                    JSONObject prodstatustypes = new JSONObject();
                    JSONObject prodstatustype = new JSONObject();
                    prodstatustype.put("prodstatusname", StaticDataMappingUtil.getProdStatusNameMap(prodInst.getString("statusCd")));
                    String prodstatuscd = "";
                    if ("100000".equals(prodInst.getString("statusCd"))) {
                        prodstatuscd = "1";
                    } else if ("110000".equals(prodInst.getString("statusCd"))) {
                        prodstatuscd = "3";
                    } else if ("120000".equals(prodInst.getString("statusCd"))) {
                        prodstatuscd = "2";
                    } else if ("140000".equals(prodInst.getString("statusCd")) || "140001".equals(prodInst.getString("statusCd")) || "140002".equals(prodInst.getString("statusCd"))) {
                        prodstatuscd = "19";
                    }

                    prodstatustype.put("prodstatuscd", prodstatuscd);
                    prodstatustypes.put("prodstatustype", prodstatustype);
                    offermemberObj.put("prodstatustypes", prodstatustypes);

                    if (offerProdInstRels.size() > 1) {
                        offermember.add(offermemberObj);
                    } else {
                        offermembers.put("offermember", offermemberObj);
                    }


                }
                if (offerProdInstRels.size() > 1) {
                    offermembers.put("offermember", offermember);
                }

                offerrecordObj.put("offermembers", offermembers);
                offerrecord.add(offerrecordObj);
            }
        }



        offerrecords.put("offerrecord", offerrecord);
        body.put("offerrecords", offerrecords);
        body.put("resultMsg", "成功");
        body.put("resultCode", "0");

        JSONObject head = BSSHelper.getRspInfo("00","success");
        res.put("head", head);
        res.put("body", body);
        LOGGER.debug("根据accNbr或者accNum查询主销售品,参数转换:"+res);
        return res.toString();
    }

    //日期转换
    public String dateChange(String date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat formats = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
        String newDate = "";
        try {
            Date dateTime = format.parse(date);
            newDate = formats.format(dateTime);
        } catch (ParseException e) {
            LOGGER.debug("日期转换失败");
        }
        return newDate;
    }
}
