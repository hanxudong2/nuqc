package cn.js189.uqc.util.enums;

/**
 * 查询产品范围的枚举类型
 * 
 * @author Mid
 */
public enum QryProdInstScopeInfoEnum {
	// 接入类产品实例
	PROD_INST("accProdInst", 1),
	// 接入类号码
	PROD_INST_ACC_NUM("prodInstAccNum", 2),
	// 接入类产品实例属性
	PROD_INST_ATTR("prodInstAttr", 3),
	// 接入类产品实例关系
	PROD_INST_REL("prodInstRel", 4),
	// 接入类产品实例停机记录
	PROD_INST_STATE("prodInstState", 5),
	// 接入类产品帐务定制关系
	PROD_INST_ACCT_REL("prodInstAcctRel", 6),
	// 接入类产品实例联系人
	PROD_INST_CONTACT("prodInstContact", 7),
	// 接入类产品发展人信息
	DEV_STAFF_INFO("devStaffInfo", 8),
	// 功能类产品实例
	FUNC_PROD_INST("funcProdInst", 9),
	// 功能类产品实例属性
	FUNC_PROD_INST_ATTR("funcProdInstAttr", 10),
	
	INST("prodInst",11);

	// 范围编码
	private String scopeCode;
	// 范围索引
	private int scopeIndex;

	private QryProdInstScopeInfoEnum(String scopeCode, int scopeIndex) {
		this.scopeCode = scopeCode;
		this.scopeIndex = scopeIndex;
	}

	public String getScopeCode() {
		return scopeCode;
	}

	public int getScopeIndex() {
		return scopeIndex;
	}
}
