package cn.js189.uqc.domain;

import cn.js189.uqc.util.DateUtils;
import com.alibaba.fastjson.JSONObject;

import java.util.Date;

public class WxAuthUrlDTO {

	private String sPappid;

	private String orderId;

	private String money;

	private int timestamp;

	private String source;

	private String type;

	private String redirectUrl;

	private String ticket;

	public WxAuthUrlDTO() {
	}

	public WxAuthUrlDTO(String sPappid, String orderId, String money, String source, String type, String redirectUrl, String ticket) {
		this.sPappid = sPappid;
		this.orderId = orderId;
		this.money = money;
		this.source = source;
		this.type = type;
		this.redirectUrl = redirectUrl;
		this.ticket = ticket;
		this.timestamp = DateUtils.dateToTimestamp(new Date());
	}

	public String getsPappid() {
		return sPappid;
	}

	public void setsPappid(String sPappid) {
		this.sPappid = sPappid;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getMoney() {
		return money;
	}

	public void setMoney(String money) {
		this.money = money;
	}

	public int getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(int timestamp) {
		this.timestamp = timestamp;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getRedirectUrl() {
		return redirectUrl;
	}

	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}

	public String getTicket() {
		return ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

	@Override
	public String toString() {
		JSONObject param = new JSONObject();
		param.put("s_pappid", this.getsPappid());
		param.put("order_id", this.getOrderId());
		param.put("money", Integer.valueOf(this.getMoney()));
		param.put("timestamp", this.getTimestamp());
		param.put("source", this.getSource());
		param.put("redirect_url", this.getRedirectUrl());
		param.put("ticket", this.getTicket());
		param.put("type", Integer.valueOf(this.getType()));
		return param.toString();
	}
}
