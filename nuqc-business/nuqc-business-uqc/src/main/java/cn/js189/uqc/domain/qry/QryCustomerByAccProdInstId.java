package cn.js189.uqc.domain.qry;

import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 通过接入号的方式查询客户信息，生成请求报文
 * 
 * @author Mid
 */
public class QryCustomerByAccProdInstId extends QryCustomerBase {
	// 日志组件
	private static final Logger LOGGER = LoggerFactory.getLogger(QryCustomerByAccProdInstId.class);

	//接入类产品实例标识accProdInstId
	private String accProdInstId;

	public QryCustomerByAccProdInstId(String regionId, String uncompletedFlag, String accProdInstId) {
		super(regionId, uncompletedFlag);
		this.accProdInstId = accProdInstId;
	}

	@Override
	protected JSONObject improveRequestObject(JSONObject requestObject) {
		requestObject.put("accProdInstId", accProdInstId);
		LOGGER.debug("requestObject:{}", requestObject.toString());
		return requestObject;
	}
	
}
