package cn.js189.uqc.controller.qx;

import cn.js189.uqc.service.*;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: xxb
 * @description: 支付平台相关接口
 * @author: jzd
 * @create: 2024/3/1 10:44
 **/
@RestController
@RequestMapping
public class PayCenterController {

    @Autowired
    private IHistoryBillService historyBillService;

    @Autowired
    private IQueryConsistenceService queryConsistenceService;

    @Autowired
    private IRealBillService realBillService;

    @Autowired
    private IBillService billService;


    /**
     * @Description 三期一致性累积量
     * @Param
     * @Return {@link String}
     * @Author xzy
     * @Date 2024/3/4 10:32
     */
    //http://dqzt.telecomjs.com:8081/uqc_services/bss/accuUseQry
    @PostMapping("/bss/accuUseQry")
    public String accuUseQry(@RequestBody String jsonParam){
        return billService.accuUseQry(jsonParam);
    }

    /**
     *
     * Description:费用清单<br>
     * @param
     * @Author xzy
     * @Date 2024/3/4 14:01
     */
    //http://dqzt.telecomjs.com:8081/uqc_services/bss/callFeeBillQuery
    @PostMapping("/bss/callFeeBillQuery")
    public String callFeeBillQuery(@RequestBody String jsonParam){
        return billService.callFeeBillQuery(jsonParam);
    }

    /**
     *
     * Description:账单查询<br>
     * @param
     * @Author xzy
     * @Date 2024/3/4 14:01
     */
    //http://dqzt.telecomjs.com:8081/uqc_services/bss/callNewBillReq
    @PostMapping("/bss/callNewBillReq")
    public String callNewBillReq(@RequestBody String jsonParam){
        return billService.callNewBillReq(jsonParam);
    }


    /**
     *
     * Description:长话话单信息-10171<br>
     * @param
     * @Author xzy
     * @Date 2024/3/4 14:01
     */
    //http://dqzt.telecomjs.com:8081/uqc_services/bss/callNewCallTicket
    @PostMapping("/bss/callNewCallTicket")
    public String callNewCallTicket(@RequestBody String jsonParam){
        return billService.callNewCallTicket(jsonParam);
    }


    /**
     *
     * Description:无线宽带上网清单<br>
     * @param
     * @Author xzy
     * @Date 2024/3/4 14:01
     */
    //http://dqzt.telecomjs.com:8081/uqc_services/bss/callNewDataTicketQrReq
    @PostMapping("/bss/callNewDataTicketQrReq")
    public String callNewDataTicketQrReq(@RequestBody String jsonParam){
        return billService.callNewDataTicketQrReq(jsonParam);
    }


    /**
     *
     * Description:新业务清单<br>
     * @param
     * @Author xzy
     * @Date 2024/3/4 14:01
     */
    //http://dqzt.telecomjs.com:8081/uqc_services/bss/callNewHttpSvrTicQr
    @PostMapping("/bss/callNewHttpSvrTicQr")
    public String callNewHttpSvrTicQr(@RequestBody String jsonParam){
        return billService.callNewHttpSvrTicQr(jsonParam);
    }


    /**
     *
     * Description:互联星空清单-16109<br>
     * @param
     * @Author xzy
     * @Date 2024/3/4 14:01
     */
    //http://dqzt.telecomjs.com:8081/uqc_services/bss/callNewInfoDataQr
    @PostMapping("/bss/callNewInfoDataQr")
    public String callNewInfoDataQr(@RequestBody String jsonParam){
        return billService.callNewInfoDataQr(jsonParam);
    }


    /**
     *
     * Description:电话清单查询<br>
     * @param
     * @Author xzy
     * @Date 2024/3/4 14:01
     */
    //http://dqzt.telecomjs.com:8081/uqc_services/bss/callNewMessageTicketQr
    @PostMapping("/bss/callNewMessageTicketQr")
    public String callNewMessageTicketQr(@RequestBody String jsonParam){
        return billService.callNewMessageTicketQr(jsonParam);
    }


    /**
     *
     * Description:窄带清单<br>
     * @param
     * @Author xzy
     * @Date 2024/3/4 14:01
     */
    //http://dqzt.telecomjs.com:8081/uqc_services/bss/callNewNarrowDataQr
    @PostMapping("/bss/callNewNarrowDataQr")
    public String callNewNarrowDataQr(@RequestBody String jsonParam){
        return billService.callNewNarrowDataQr(jsonParam);
    }

    /**
     *
     * Description:欠费查询<br>
     * @param
     * @Author xzy
     * @Date 2024/3/4 14:01
     */
    //http://dqzt.telecomjs.com:8081/uqc_services/bss/callNewOthGetOwe
    @PostMapping("/bss/callNewOthGetOwe")
    public String callNewOthGetOwe(@RequestBody String jsonParam){
        return billService.callNewOthGetOwe(jsonParam);
    }

    /**
     *
     * Description:C网短信/彩信增值详单<br>
     * @param
     * @Author xzy
     * @Date 2024/3/4 14:01
     */
    //http://dqzt.telecomjs.com:8081/uqc_services/bss/callNewSmsValueTicketQr
    @PostMapping("/bss/callNewSmsValueTicketQr")
    public String callNewSmsValueTicketQr(@RequestBody String jsonParam){
        return billService.callNewSmsValueTicketQr(jsonParam);
    }

    /**
     *
     * Description:bsn短信详单查询-10135<br>
     * @param
     * @Author xzy
     * @Date 2024/3/4 14:01
     */
    //http://dqzt.telecomjs.com:8081/uqc_services/bss/callNewSmTicketQr
    @PostMapping("/bss/callNewSmTicketQr")
    public String callNewSmTicketQr(@RequestBody String jsonParam){
        return billService.callNewSmTicketQr(jsonParam);
    }

    /**
     *
     * Description:其他增值业务清单<br>
     * @param
     * @Author xzy
     * @Date 2024/3/4 14:01
     */
    //http://dqzt.telecomjs.com:8081/uqc_services/bss/callNewValueTicketQr
    @PostMapping("/bss/callNewValueTicketQr")
    public String callNewValueTicketQr(@RequestBody String jsonParam){
        return billService.callNewValueTicketQr(jsonParam);
    }


    /**
     *
     * Description:手机语音清单<br>
     * @param
     * @Author xzy
     * @Date 2024/3/4 14:01
     */
    //http://dqzt.telecomjs.com:8081/uqc_services/bss/callNewVoiceTicket
    @PostMapping("/bss/callNewVoiceTicket")
    public String callNewVoiceTicket(@RequestBody String jsonParam){
        return billService.callNewVoiceTicket(jsonParam);
    }

    /**
     *
     * Description:查询语音，短信，流量，增值清单<br>
     * @param
     * @Author xzy
     * @Date 2024/3/4 14:01
     */
    //http://dqzt.telecomjs.com:8081/uqc_services/bss/queryBillItems
    @PostMapping("/bss/queryBillItems")
    public String queryBillItems(@RequestBody String jsonParam){
        return billService.queryBillItems(jsonParam);
    }


    /**
     * 历史查询 账单查询
     * @param requestJSON
     * @return String
     */
    @PostMapping("/iHistoryBill/callNewBillReq")
    public String callNewHistoryBillReq(@RequestBody String requestJSON) {
        return historyBillService.callNewBillReq(requestJSON);
    }

    /**
     * 历史查询 消费趋势查询接口
     * @param requestJson
     * @return String
     */
    @PostMapping("/iHistoryBill/feeTrend")
    public String feeTrend(@RequestBody String requestJson) {
        return historyBillService.feeTrend(requestJson);
    }

    /**
     * 历史查询 移动用户当月数据详单按天累计查询接口
     * @param requestJSON
     * @return String
     */
    @PostMapping("/iHistoryBill/getTktYiValueTicketQrByDayCount")
    public String getTktYiValueTicketQrByDayCount(@RequestBody String requestJSON) {
        return historyBillService.getTktYiValueTicketQrByDayCount(requestJSON);
    }

    /**
     * 历史查询 新实时话费接口
     * @param requestJSON
     * @return String
     */
    @PostMapping("/iHistoryBill/qryInstantFeeList")
    public String qryInstantFeeList(@RequestBody String requestJSON) {
        return historyBillService.qryInstantFeeList(requestJSON);
    }

    /**
     * 历史查询 新历史账单查询
     * @param requestJSON
     * @return String
     */
    @PostMapping("/iHistoryBill/qryNewCustBill")
    public String qryNewCustBill(@RequestBody String requestJSON) {
        return historyBillService.qryNewCustBill(requestJSON);
    }

    /**
     * 历史查询 增值业务钻取
     * @param requestJSON
     * @return String
     */
    @PostMapping("/iHistoryBill/qrySpDetail")
    public String qrySpDetail(@RequestBody String requestJSON) {
        return historyBillService.qrySpDetail(requestJSON);
    }

    /**
     * 一致性-本月消费查询
     * @param requestJSON
     * @return JSONObject
     */
    @PostMapping("/iQueryConsistence/callAcctItemConsumqr")
    public JSONObject callAcctItemConsumqr(@RequestBody JSONObject requestJSON) {
        return queryConsistenceService.callAcctItemConsumqr(requestJSON);
    }

    /**
     * 一致性-余额查询
     * @param requestJSON
     * @return JSONObject
     */
    @PostMapping("/iQueryConsistence/callConQueryBalance")
    public JSONObject callConQueryBalance(@RequestBody JSONObject requestJSON) {
        return queryConsistenceService.callConQueryBalance(requestJSON);
    }

    /**
     * 一致性-欠费查询
     * @param requestJSON
     * @return JSONObject
     */
    @PostMapping("/iQueryConsistence/callQueryOwe")
    public JSONObject callQueryOwe(@RequestBody JSONObject requestJSON) {
        return queryConsistenceService.callQueryOwe(requestJSON);
    }

    /**
     * 一致性余额变动
     * @param requestJSON
     * @return JSONObject
     */
    @PostMapping("/iQueryConsistence/callUgetBalChg")
    public JSONObject callUgetBalChg(@RequestBody JSONObject requestJSON) {
        return queryConsistenceService.callUgetBalChg(requestJSON);
    }

    /**
     * 一致性-余额变动明细查询
     * @param requestJSON
     * @return JSONObject
     */
    @PostMapping("/iQueryConsistence/callUgetBalChgDetail")
    public JSONObject callUgetBalChgDetail(@RequestBody JSONObject requestJSON) {
        return queryConsistenceService.callUgetBalChgDetail(requestJSON);
    }

    /**
     * 一致性-话费返还记录查询
     * @param requestJSON
     * @return JSONObject
     */
    @PostMapping("/iQueryConsistence/callUgetBalRtn")
    public JSONObject callUgetBalRtn(@RequestBody JSONObject requestJSON) {
        return queryConsistenceService.callUgetBalRtn(requestJSON);
    }


    /**
     * 一致性-话费返还记录查询
     * @param requestJSON
     * @return JSONObject
     */
    @PostMapping("/iQueryConsistence/callUgetBalRtnDetail")
    public JSONObject callUgetBalRtnDetail(@RequestBody JSONObject requestJSON) {
        return queryConsistenceService.callUgetBalRtnDetail(requestJSON);
    }


    /**
     * 一致性-缴费记录查询
     * @param requestJSON
     * @return JSONObject
     */
    @PostMapping("/iQueryConsistence/callUgetPayment")
    public JSONObject callUgetPayment(@RequestBody JSONObject requestJSON) {
        return queryConsistenceService.callUgetPayment(requestJSON);
    }


    /**
     * 量本使用明细查询（累积量明细）
     */
    //http://dqzt.telecomjs.com:8081/uqc_services/iRealBill/accuUseDetailQry
    @PostMapping("/iRealBill/accuUseDetailQry")
    public String accuUseDetailQry(@RequestBody String jsonParam){
        return realBillService.accuUseDetailQry(jsonParam);
    }


    /**
     * 余额查询<br>
     */
    //http://dqzt.telecomjs.com:8081/uqc_services/iRealBill/callBalanceQrReq
    @PostMapping("/iRealBill/callBalanceQrReq")
    public String callBalanceQrReq(@RequestBody String jsonParam){
        return realBillService.callBalanceQrReq(jsonParam);
    }

    /**
     * 缓存获取余额查询
     */
    //http://dqzt.telecomjs.com:8081/uqc_services/iRealBill/callBalanceReqWithCache
    @PostMapping("/iRealBill/callBalanceReqWithCache")
    public String callBalanceReqWithCache(@RequestBody String jsonParam){
        return realBillService.callBalanceReqWithCache(jsonParam);
    }

    /**
     * 本月消费缓存查询
     */
    //http://dqzt.telecomjs.com:8081/uqc_services/iRealBill/callCurrMonthBillReqWithCache
    @PostMapping("/iRealBill/callCurrMonthBillReqWithCache")
    public String callCurrMonthBillReqWithCache(@RequestBody String jsonParam){
        return realBillService.callCurrMonthBillReqWithCache(jsonParam);
    }

    /**
     * 本期已付查询接口
     */
    //http://dqzt.telecomjs.com:8081/uqc_services/iRealBill/callCustUseQry
    @PostMapping("/iRealBill/callCustUseQry")
    public String callCustUseQry(@RequestBody String jsonParam){
        return realBillService.callCustUseQry(jsonParam);
    }

    /**
     * 费用返还记录查询接口
     */
    //http://dqzt.telecomjs.com:8081/uqc_services/iRealBill/callFeeReturnList
    @PostMapping("/iRealBill/callFeeReturnList")
    public String callFeeReturnList(@RequestBody String jsonParam){
        return realBillService.callFeeReturnList(jsonParam);
    }

    /**
     * 欠费查询
     */
    //http://dqzt.telecomjs.com:8081/uqc_services/iRealBill/callNewOthGetOwe
    @PostMapping("/iRealBill/callNewOthGetOwe")
    public String callNewOthGetOweByReal(@RequestBody String jsonParam){
        return realBillService.callNewOthGetOwe(jsonParam);
    }

    /**
     * 充值-查询业务类型
     */
    //http://dqzt.telecomjs.com:8081/uqc_services/iRealBill/callNewUOthGetType
    @PostMapping("/iRealBill/callNewUOthGetType")
    public String callNewUOthGetType(@RequestBody String jsonParam){
        return realBillService.callNewUOthGetType(jsonParam);
    }

    /**
     * 积分商城通用接口
     */
    //http://dqzt.telecomjs.com:8081/uqc_services/iRealBill/callPointMarketCommonMethod
    @PostMapping("/iRealBill/callPointMarketCommonMethod")
    public String callPointMarketCommonMethod(@RequestBody String jsonParam){
        return realBillService.callPointMarketCommonMethod(jsonParam);
    }

    /**
     * 政企账单汇总
     */
    //http://dqzt.telecomjs.com:8081/uqc_services/iRealBill/fundManageCustBill
    @PostMapping("/iRealBill/fundManageCustBill")
    public String fundManageCustBill(@RequestBody String jsonParam){
        return realBillService.fundManageCustBill(jsonParam);
    }

    /**
     * 政企账单详单
     */
    //http://dqzt.telecomjs.com:8081/uqc_services/iRealBill/fundManageCustBillDet
    @PostMapping("/iRealBill/fundManageCustBillDet")
    public String fundManageCustBillDet(@RequestBody String jsonParam){
        return realBillService.fundManageCustBillDet(jsonParam);
    }

    /**
     * 费用查询
     */
    //http://dqzt.telecomjs.com:8081/uqc_services/iRealBill/getAcctItemForDiag
    @PostMapping("/iRealBill/getAcctItemForDiag")
    public String getAcctItemForDiag(@RequestBody String jsonParam){
        return realBillService.getAcctItemForDiag(jsonParam);
    }

    /**
     * 充值缴费记录查询
     */
    //http://dqzt.telecomjs.com:8081/uqc_services/iRealBill/getNewQrPay
    @PostMapping("/iRealBill/getNewQrPay")
    public String getNewQrPay(@RequestBody String jsonParam){
        return realBillService.getNewQrPay(jsonParam);
    }

    /**
     * 线上销户余额查询接口
     */
    //http://dqzt.telecomjs.com:8081/uqc_services/bss/queryBalance
    @PostMapping("/bss/queryBalance")
    public String queryBalance(@RequestBody String jsonParam){
        return billService.queryBalance(jsonParam);
    }

    /**
     * 线上销户余额提取或者结转进度
     */
    //http://dqzt.telecomjs.com:8081/uqc_services/bss/queryBalanceProgress
    @PostMapping("/bss/queryBalanceProgress")
    public String queryBalanceProgress(@RequestBody String jsonParam){
        return billService.queryBalanceProgress(jsonParam);
    }


    /**
     * 一致性二期
     * @param requestJson
     * @return String
     */
    @PostMapping("/iQueryConsistence/queryConsistenceNew")
    public String queryConsistenceNew(@RequestBody String requestJson) {
        return queryConsistenceService.queryConsistenceNew(requestJson);
    }



}


