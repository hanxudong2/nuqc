package cn.js189.uqc.domain.qry;

import cn.js189.uqc.common.QryOfferInstBase;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * 通过产品实例ID的方式查询销售品信息，生成请求报文
 * 
 * @author ly
 */
public class QryInstForMainOffer extends QryOfferInstBase {

	// 日志组件
	private static final Logger LOGGER = LoggerFactory.getLogger(QryInstForMainOffer.class);

	//业务号码
	private String ownerCustId;
	
	//宽带账号
	private JSONArray accProdInstIds;
	
	//产品规格标识
	private String accNbrType;
	
	
	public QryInstForMainOffer(List<String> offerIds, String roleId, String regionId, String uncompletedFlag, String isIndependent, String ownerCustId, JSONArray accProdInstIds, String accNbrType) {
		super(regionId, uncompletedFlag, isIndependent,roleId,offerIds);
		this.ownerCustId = ownerCustId;
		this.accProdInstIds = accProdInstIds;
		this.accNbrType = accNbrType;
	}

	
	@Override
	protected JSONObject improveRequestObject(JSONObject requestObject) {
		if("1".equals(accNbrType)){
			requestObject.put("ownerCustId", ownerCustId);
		}else{
			requestObject.put("accProdInstIds", accProdInstIds);
		}
		LOGGER.debug("requestObject:{}", requestObject.toString());
		return requestObject;
	}

}
