package cn.js189.uqc.domain.qry;

import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 通过按合同号查询方式查询产品信息，生成请求报文
 * 
 * @author Mid
 */
public class QryProdInstByAcctCd extends QryProdInstBase {
	// 日志组件
	private static final Logger LOGGER = LoggerFactory.getLogger(QryProdInstByAcctCd.class);

	// 合同号
	private String acctCd;

	public QryProdInstByAcctCd(String regionId, String uncompletedFlag, String acctCd) {
		super(regionId, uncompletedFlag);
		this.acctCd = acctCd;
	}

	@Override
	protected JSONObject improveRequestObject(JSONObject requestObject) {
		requestObject.put("acctCd", acctCd);
		LOGGER.debug("requestObject:{}", requestObject);
		return requestObject;
	}
}
