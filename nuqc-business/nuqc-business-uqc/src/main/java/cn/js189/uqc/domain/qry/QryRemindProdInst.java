package cn.js189.uqc.domain.qry;

import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class QryRemindProdInst extends QryRemindProdInstBase{
	// 日志组件
	private static final Logger LOGGER = LoggerFactory.getLogger(QryRemindProdInst.class);

	private String accProdInstId;
	
	public QryRemindProdInst(String regionId, String accProdInstId) {
		super(regionId);
		this.accProdInstId = accProdInstId;
	}

	@Override
	protected JSONObject improveRequestObject(JSONObject requestObject) {
		requestObject.put("accProdInstId", accProdInstId);
		LOGGER.debug("requestObject:{}", requestObject.toString());
		return requestObject;
	}
}
