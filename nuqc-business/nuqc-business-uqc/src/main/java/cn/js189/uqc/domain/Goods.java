package cn.js189.uqc.domain;

public class Goods {
	private String name;

	private int num;

	private String unit;

	private int price;

	public Goods(String name, int num, String unit, int price) {
		this.name = name;
		this.num = num;
		this.unit = unit;
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}
}