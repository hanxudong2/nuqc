package cn.js189.uqc.domain.qry;

import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 查询无纸化信息，生成请求报文
 * 
 * @author Mid
 */
public class QryPdfByOrderNbr extends QryPdfBase{

	// 日志组件
	private static final Logger LOGGER = LoggerFactory.getLogger(QryPdfByOrderNbr.class);

	// 客户订单号
	private String custOrderNbr;
	
	//南京区县标识
	private String regionId;
	
	public QryPdfByOrderNbr(String regionId, String custOrderNbr, String regionId2) {
		super(regionId);
		this.custOrderNbr = custOrderNbr;
		this.regionId = regionId2;
	}

	@Override
	protected JSONObject improveRequestObject(JSONObject requestObject) {
		requestObject.put("custOrderNbr", custOrderNbr);
		requestObject.put("regionId", regionId);
		LOGGER.debug("requestObject:{}", requestObject.toString());
		return requestObject;
	}
}
