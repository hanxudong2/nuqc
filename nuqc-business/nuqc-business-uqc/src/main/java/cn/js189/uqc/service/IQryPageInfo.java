package cn.js189.uqc.service;

import com.alibaba.fastjson.JSONObject;

/**
 * 分页信息接口
 * 
 * @author Mid
 */
public interface IQryPageInfo {

	/**
	 * 生成分页信息
	 * 
	 * @return
	 */
	JSONObject generatePageInfos();
}
