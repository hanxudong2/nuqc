package cn.js189.uqc.util;

/**
 * uqc动态接口地址查村常量
 */
public class UqcAddressCodeConstants
{
	private UqcAddressCodeConstants(){
		/*empty*/
	}
	
	/**
	 * H码正式接口 code
	 */
	public static final String GET_NUMBER = "GET_H_NUMBER";
	
	/**
	 * 用户网格关系接口
	 */
	public static final String GET_PROD_EXTEND_INFO = "GET_PROD_EXTEND_INFO";
	
	/**
	 * 长话话单信息 - 长途清单查询
	 */
	public static final String TKT_GU_CALL_TICKET_QR = "TKT_GU_CALL_TICKET_QR";
	
	/**
	 * 限购校验地址
	 */
	public static final String PUR_REST_STRATEGY = "PUR_REST_STRATEGY";
	
	/**
	 * 拦截校验接口
	 */
	public static final String CHECK_RC_BLACK = "CHECK_RC_BLACK";
	
	/**
	 * 查询礼包信息
	 */
	public static final String EOP_CPC_PPM_QRY_OFFER = "EOP_CPC_PPM_QRY_OFFER";
	
	/**
	 * 查询客户的地址
	 */
	public static final String EOP_QRY_CUSTOMER = "EOP_QRY_CUSTOMER";
	
	/**
	 * 查询客户的地址
	 */
	public static final String CUSTOM_MANAGER_URL = "CUST_MANAGER_URL";
	
	/**
	 * 待补充
	 */
	public static final String GM_PROPERTIES_URL = "GM_PROPERTIES_URL";
	
	/**
	 * 用户认证失败记录查询接口
	 */
	public static final String AUTH_FAIL = "AUTH_FAIL";
	
	/**
	 * 用户认证失败记录查询接口
	 */
	public static final String EOP_CAL_CHARGE_DETAIL_INFO = "EOP_CAL_CHARGE_DETAIL_INFO";
	
	/**
	 * 违约金试算
	 */
	public static final String EOP_RPE_HANDLE_HTTP_REQ = "EOP_RPE_HANDLE_HTTP_REQ";
	
	/**
	 * 违约金试算
	 */
	public static final String EOP_QRY_CHANNEL = "EOP_QRY_CHANNEL";
	
	/**
	 * 根据卡号查询用户号码接口
	 */
	public static final String GET_NUM_INFO_BY_CARD_NO_URL = "GET_NUM_INFO_BY_CARD_NO_URL";
	
	/**
	 * 营业厅门店ID与BSS渠道ID对应关系查询
	 */
	public static final String EOP_QRY_GET_STORE_MSM = "EOP_QRY_GET_STORE_MSM";
	
	/**
	 * 查询用户信息
	 */
	public static final String GET_USER_INFO = "GET_USER_INFO";
	
	/**
	 * 用户上网记录查询接口
	 */
	public static final String GET_USER_DET = "GET_USER_DET";
	
	/**
	 * 用户在线查询接口
	 */
	public static final String GET_USER_ONLINE = "GET_USER_ONLINE";
	
	/**
	 * 智能网查询接口
	 */
	public static final String ZNW_INFO_URL = "ZNW_INFO_URL";
	
	/**
	 * 查询政企客户等级、关键人联系人
	 */
	public static final String EOP_QRY_CUST_LEVEL = "EOP_QRY_CUST_LEVEL";
	
	/**
	 * 查询政企客户等级、关键人联系人
	 */
	public static final String EOP_QRY_CUSTOMER_ORDER_TYPE = "EOP_QRY_CUSTOMER_ORDER_TYPE";
	
	/**
	 * 查询政企客户等级、关键人联系人
	 */
	public static final String QRY_FULL_ADDRESS_FORM_OSS = "QRY_FULL_ADDRESS_FORM_OSS";
	
	
}
