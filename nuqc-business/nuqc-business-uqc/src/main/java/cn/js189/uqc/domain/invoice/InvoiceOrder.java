package cn.js189.uqc.domain.invoice;

import java.util.Date;

/**
 * 电子发票订单信息
 */
public class InvoiceOrder {

	private Long id;

	/**
	 * 微信侧用户唯一标识
	 */
	private String wxOpenId;

	/**
	 * 支付宝侧用户唯一标识
	 */
	private String aliOpenId;

	/**
	 * 商户内部电子发票订单号
	 */
	private String orderId;

	/**
	 * 开票号码
	 */
	private String phone;

	/**
	 * 开票号码类型
	 */
	private String phoneType;

	/**
	 * 开票类型
	 * 1000月结 1100实缴 1200营业
	 */
	private String type;

	/**
	 * 区号
	 */
	private String areaCode;

	/**
	 * 发票抬头
	 */
	private String titleName;

	/**
	 * 抬头类型
	 * 1个人 2政企
	 */
	private String titleType;

	/**
	 * 发票内容
	 */
	private String description;

	/**
	 * 发票总金额
	 */
	private String money;

	/**
	 * 发票接收邮箱
	 */
	private String mail;

	/**
	 * 税号
	 * 政企用户填写
	 */
	private String taxNum;

	/**
	 * 开户行名称
	 * 政企用户填写
	 */
	private String bankName;

	/**
	 * 开户行银行账号
	 * 政企用户填写
	 */
	private String bankAccount;

	/**
	 * 企业地址
	 * 政企用户填写
	 */
	private String companyAddr;

	/**
	 * 企业联系电话
	 * 政企用户填写
	 */
	private String companyPhone;

	/**
	 * 备注说明
	 * 政企用户填写
	 */
	private String remark;

	/**
	 * 开票涞源 微信侧
	 * app：app开票，web：微信h5开票，wxa：小程序开发票，wap：普通网页开票
	 */
	private String source;

	/**
	 * 开票订单创建时间
	 */
	private Date gmtCreate;

	/**
	 * 修改时间
	 */
	private Date gmtUpdate;

	/**
	 * 登陆号码
	 */
	private String loginAccNbr;
	
	/**
	 * 是否合并开票
	 */
	private String isMerge;

	public String getLoginAccNbr() {
		return loginAccNbr;
	}

	public void setLoginAccNbr(String loginAccNbr) {
		this.loginAccNbr = loginAccNbr;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getWxOpenId() {
		return wxOpenId;
	}

	public void setWxOpenId(String wxOpenId) {
		this.wxOpenId = wxOpenId;
	}

	public String getAliOpenId() {
		return aliOpenId;
	}

	public void setAliOpenId(String aliOpenId) {
		this.aliOpenId = aliOpenId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPhoneType() {
		return phoneType;
	}

	public void setPhoneType(String phoneType) {
		this.phoneType = phoneType;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getTitleName() {
		return titleName;
	}

	public void setTitleName(String titleName) {
		this.titleName = titleName;
	}

	public String getTitleType() {
		return titleType;
	}

	public void setTitleType(String titleType) {
		this.titleType = titleType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getMoney() {
		return money;
	}

	public void setMoney(String money) {
		this.money = money;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getTaxNum() {
		return taxNum;
	}

	public void setTaxNum(String taxNum) {
		this.taxNum = taxNum;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankAccount() {
		return bankAccount;
	}

	public void setBankAccount(String bankAccount) {
		this.bankAccount = bankAccount;
	}

	public String getCompanyAddr() {
		return companyAddr;
	}

	public void setCompanyAddr(String companyAddr) {
		this.companyAddr = companyAddr;
	}

	public String getCompanyPhone() {
		return companyPhone;
	}

	public void setCompanyPhone(String companyPhone) {
		this.companyPhone = companyPhone;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public Date getGmtCreate() {
		return gmtCreate;
	}

	public void setGmtCreate(Date gmtCreate) {
		this.gmtCreate = gmtCreate;
	}

	public Date getGmtUpdate() {
		return gmtUpdate;
	}

	public void setGmtUpdate(Date gmtUpdate) {
		this.gmtUpdate = gmtUpdate;
	}

	public String getIsMerge() {
		return isMerge;
	}

	public void setIsMerge(String isMerge) {
		this.isMerge = isMerge;
	}
}
