package cn.js189.uqc.service;

import cn.js189.common.util.exception.BaseAppException;
import cn.js189.uqc.domain.*;
import cn.js189.uqc.domain.invoice.*;

import java.util.List;
import java.util.Map;


public interface InvoiceService {

	/**
	 * 提交开票信息
	 * 1.插入订单总信息
	 * 2.插入订单详情信息
	 * 3.更新/新增用户信息
	 */
	boolean commitInvoiceInfo(InvoiceOrder invoiceOrder, List<InvoiceOrderDetail> orderDetail, User user, String bizName) throws BaseAppException;

	/**
	 * 账单id和产品实例id查询是否有发票信息
	 */
	InvoiceOrderDetail selectDetailByIdAndProductId(String id, String productId);

	/**
	 * 更新发票信息
	 * 发票实例id作为唯一标识
	 */
	int updateInvoiceOrderDetail(InvoiceOrderDetail invoiceOrderDetail);

	InvoiceDetail selectInvoiceById(String invoiceId);

	InvoiceDetail selectInvoiceById(String invoiceId,String partyId);

    InvoiceDetail selectInvoiceType1000(InvoiceInfoDTO invoiceInfoDTO);
	

	InvoiceDetail selectInvoiceType1100(InvoiceInfoDTO invoiceInfoDTO);
	
	
	InvoiceDetail selectInvoiceType1200(InvoiceInfoDTO invoiceInfoDTO);
	
	/**
	 * 根据订单id更新开票状态信息
	 */
	int updateInvoiceDetailStatusByOrderId(String orderId, String status);

	/**
	 * 更新发票信息
	 * 主键id作为唯一标识
	 */
	int updateInvoiceOrderDetailByPK(InvoiceOrderDetail invoiceOrderDetail);

	/**
	 * 根据身份证查询用户信息
	 */
	User selectUserByIdCard(String idCard);

	/**
	 * 插入用户信息
	 */
	int insertUserInfos(User user);

	/**
	 * 更新用户信息
	 */
	int updateUserInfos(User user);

	List<InvoiceHisDto> selectMonthlyHis(String phone, String type, String startTime, String endTime, String phoneType);

	List<InvoiceHisDto> selectPaymentHis(String phone, String type, String startTime, String endTime, String phoneType);

	List<InvoiceHisDto> selectOrderOneItemHis(String phone, String type, String startTime, String endTime, String phoneType);
	
	List<Map<String, String>> selectListByPartyId(String partyId);
	
	List<QxError> initQxError();
	
	void insertData(String areaCode, String accNbr, String returnCode, String inputParam, String outParam, String serviceType);
	
}
