package cn.js189.uqc.service.impl;

import cn.js189.common.constants.ExpConstant;
import cn.js189.uqc.service.ISecureGuardian;
import cn.js189.uqc.service.PubAddressIpervice;
import cn.js189.uqc.util.HttpClientUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @program: xxb
 * @description:
 * @author: jzd
 * @create: 2024/3/4 16:44
 **/
@Service
public class SecureGuardianImpl implements ISecureGuardian {

    private static final Logger logger = LoggerFactory.getLogger(SecureGuardianImpl.class);

    @Autowired
    PubAddressIpervice pubAddressIpervice;

    @Override
    public JSONObject queryOrderInfo(JSONObject requestJSON) {
        return commonDealMethod(requestJSON, "queryOrderInfo(查询订购信息)", "3.1");
    }

    @Override
    public JSONObject bindAndUnbund(JSONObject requestJSON) {
        return commonDealMethod(requestJSON, "bindAndUnbund(绑定申请，解绑)", "3.3");
    }

    @Override
    public JSONObject onlineManageSetup(JSONObject requestJSON) {
        return commonDealMethod(requestJSON, "onlineManageSetup(上网管理开关设置)", "3.6");
    }

    @Override
    public JSONObject qryOnlineSetupState(JSONObject requestJSON) {
        return commonDealMethod(requestJSON, "qryOnlineSetupState(上网管理开关状态查询)", "3.7");
    }

    @Override
    public JSONObject blkOnlineTimeSetup(JSONObject requestJSON) {
        return commonDealMethod(requestJSON, "blkOnlineTimeSetup(阻断上网时间段设置)", "3.8");
    }

    @Override
    public JSONObject qryBlkOnlineTimeSetup(JSONObject requestJSON) {
        return commonDealMethod(requestJSON, "qryBlkOnlineTimeSetup(阻断上网时间段查询)", "3.9");
    }

    @Override
    public JSONObject queryGuardLog(JSONObject requestJSON) {
        return commonDealMethod(requestJSON, "queryGuardLog(防护日志查询)", "3.16");
    }

    /**
     * 请求入参参数校验
     * @param requestJSON 入参
     * @param intefaceIdentify 接口标识
     * @return response 参数校验结果
     * */
    public JSONObject validate(JSONObject requestJSON, String intefaceIdentify) {
        JSONObject response = null;
        if(null == requestJSON) {
            response = new JSONObject();
            response.put("errorCode", ExpConstant.ERROR_INPUT_NULL);
            response.put("errorMsg", "request params is null,plc check.");
            response.put("success", "OK");
            response.put("bodys", "");
            return response;
        }
        String uuid = requestJSON.getString("uuid");
        if(null == uuid || !intefaceIdentify.equals(uuid)) {
            response = new JSONObject();
            response.put("errorCode", ExpConstant.ERROR_PARAMS);
            response.put("errorMsg", "uuid is error,please enter the correct uuid.");
            response.put("success", "OK");
            response.put("bodys", "");
        }
        return response;
    }

    /**
     * 响应报文封装
     * @param result 响应string类型报文
     * @return response api返回结果报文
     * */
    public JSONObject builderResponse(String result) {
        JSONObject response = new JSONObject();
        if(StringUtils.isEmpty(result)){
            response.put("errorCode", ExpConstant.ERROR_OUTPUT_NULL);
            response.put("errorMsg", "unknown error，noc response is null,plc check.");
            response.put("success", "OK");
            response.put("bodys", result);
            return response;
        }
        JSONObject respJO = JSON.parseObject(result);
        String code = respJO.getString("code"); // noc返回结果码
        String msg = respJO.getString("msg"); // noc返回结果描述
        response.put("errorCode", code);
        response.put("errorMsg", msg);
        response.put("success", "OK");
        response.put("bodys", respJO);
        return response;
    }

    /**
     * 出现异常后返回报文封装
     * @param result noc响应结果报文
     * @param exceptionMsg 捕获异常信息描述
     * @return response 出现异常返回报文
     * */
    public JSONObject dealExceptionResp(String result, String exceptionMsg) {
        JSONObject response = new JSONObject();
        response.put("errorCode", ExpConstant.ERROR_INNER_EXCEPTION); // 接口异常，内部异常
        response.put("errorMsg", exceptionMsg); // 接口异常描述
        response.put("success", "");
        response.put("bodys", result);
        return response;
    }

    /**
     * 接口处理公共方法
     * @param requestJSON 请求入参
     * @param logInfo 日志信息
     * @return response response
     * */
    public JSONObject commonDealMethod(JSONObject requestJSON, String logInfo ,String intefaceIdentify){
        JSONObject response = null;
        logger.debug(logInfo +"input :" + requestJSON.toString());
        // 请求入参uuid校验，参数为空返回相应信息
        JSONObject jo = validate(requestJSON, intefaceIdentify);
        if(null != jo) {
            return jo;
        }
        String result = "";
        try {
            String url = pubAddressIpervice.getActionUrl("uqc_secure_guardian",null);

            result = HttpClientUtils.sendPost(url, requestJSON.toString());
            logger.debug(logInfo + " output :" + result);
            // 报文封装
            response = builderResponse(result);
            logger.debug(logInfo + " api报文出参" + response);
        } catch (Exception e) {
            String exceptionMsg = e.getMessage(); // 异常描述
            response = this.dealExceptionResp(result, exceptionMsg);
            logger.debug(logInfo + " Exception :" + exceptionMsg);
            return response;
        }
        return response;
    }


}
