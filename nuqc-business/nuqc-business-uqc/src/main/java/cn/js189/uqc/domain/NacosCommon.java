package cn.js189.uqc.domain;

/**
 * @program: xxb
 * @description:
 * @author: jzd
 * @create: 2023/3/15 11:02
 **/
public class NacosCommon {

    private String proxyUrl;//公网接口的代理地址，从nacos配置，公网nginx的vip地址

    private String zkAddress;

    public String getZkAddress() {
        return zkAddress;
    }

    public void setZkAddress(String zkAddress) {
        this.zkAddress = zkAddress;
    }

    public void setProxyUrl(String proxyUrl) {
        this.proxyUrl = proxyUrl;
    }

    public String getProxyUrl(){
        return proxyUrl;
    }
}
