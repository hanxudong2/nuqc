package cn.js189.uqc.service;

/**
 * 地址类查询
 */
public interface IAreaBillService {
	
	/**
	 * 查询全国号码H码，从而查到lanId
	 * @param reqInfo 请求参数
	 * @return 结果
	 */
	String callNumberCountry(String reqInfo);
	
	/**
	 * 根据手机号查询号段信息
	 * @param reqInfo 参数
	 * 结果
	 */
	String callNewAreaCodeBasic(String reqInfo);
	
	/**
	 * 区域编码查询-10112
	 * @param params 参数
	 * @return 结果
	 */
	String callNewAreaCode(String params);
	
	/**
	 * 根据号码查询对应产品ID
	 * @param reqParams 参数
	 * @return 结果
	 */
	String getArpuByPhoneNbr(String reqParams);
	
	/**
	 * 根据产品实例查询归属信息接口协议
	 * @param reqParams 参数
	 * @return 结果
	 */
	String getProdExtendInfo(String reqParams);
	
	/**
	 * getGoodsIdByAccNbrFK
	 * @param reqParams 参数
	 * @return 结果
	 */
	String getGoodsIdByAccNbrFK(String reqParams);
	
}
