package cn.js189.uqc.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.js189.common.domain.*;
import cn.js189.common.util.Utils;
import cn.js189.uqc.service.IHistoryBillService;
import cn.js189.uqc.util.EuaCacheUtil;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author yxl
 * @Date 2024/3/6
 */
@Service
@Slf4j
public class IHistoryBillServiceImpl implements IHistoryBillService {
    private static final Logger logger = LoggerFactory.getLogger(IHistoryBillServiceImpl.class);
    @Autowired
    private BSService bsService;

    @Override
    public String callNewBillReq(String requestJson1) {
        String tranId = "";
        JSONObject response = new JSONObject();
        RespInfo responseJson = new RespInfo();
        RespHead responseHead = new RespHead();
        responseJson.setHead(responseHead);
        try {
            ReqInfo reqInfo = BeanUtil.toBean(requestJson1, ReqInfo.class);
            JSONObject body = reqInfo.getBody();
            String result = this.bsService.callNewBillReqNew(reqInfo);
            ReqHead reqHead = reqInfo.getHead();
            return EuaCacheUtil.buildBillResponse(body, reqHead, result);
        } catch (Exception e) {
            String exceptionMsg = e.getMessage(); // 异常描述
            response = Utils.dealExceptionResp(responseHead, tranId, responseJson);
            logger.debug("callNewBillReq(账单查询) Exception :{}", exceptionMsg);
            return response.toString();
        }
    }

    @Override
    public String feeTrend(String requestJson) {
        logger.debug("(feeTrend)消费趋势查询接口 input:{}", requestJson);
        String tranId = "";
        JSONObject response = new JSONObject();
        RespInfo responseJson = new RespInfo();
        RespHead responseHead = new RespHead();
        responseJson.setHead(responseHead);
        try {
            ReqInfo reqInfo = BeanUtil.toBean(requestJson, ReqInfo.class);
            String result = this.bsService.feeTrend(reqInfo);
            return EuaCacheUtil.buildBillResponse(reqInfo.getBody(), reqInfo.getHead(), result);
        } catch (Exception e) {
            String exceptionMsg = e.getMessage(); // 异常描述
            response = Utils.dealExceptionResp(responseHead, tranId, responseJson);
            logger.debug("(feeTrend)消费趋势查询接口 Exception :{}", exceptionMsg);
            return response.toString();
        }
    }

    @Override
    public String getTktYiValueTicketQrByDayCount(String requestJson1) {
        String tranId = "";
        JSONObject response = new JSONObject();
        RespInfo responseJson = new RespInfo();
        RespHead responseHead = new RespHead();
        responseJson.setHead(responseHead);
        try {
            ReqInfo reqInfo = BeanUtil.toBean(requestJson1, ReqInfo.class);
            JSONObject body = reqInfo.getBody();
            String result = this.bsService.getTktYiValueTicketQrByDayCount(reqInfo);
            ReqHead reqHead = reqInfo.getHead();
            return EuaCacheUtil.buildBillResponse(body, reqHead, result);
        } catch (Exception e) {
            String exceptionMsg = e.getMessage(); // 异常描述
            response = Utils.dealExceptionResp(responseHead, tranId, responseJson);
            logger.debug("(qrySpDetail)移动用户当月数据详单按天累计查询接口 Exception :{}", exceptionMsg);
            return response.toString();
        }

    }

    @Override
    public String qryInstantFeeList(String requestJson) {
        logger.debug("(qryInstantFeeList)实时话费明细查询 input:{}", requestJson);
        String tranId = "";
        JSONObject response = new JSONObject();
        RespInfo responseJson = new RespInfo();
        RespHead responseHead = new RespHead();
        responseJson.setHead(responseHead);
        try {
            ReqInfo reqInfo = BeanUtil.toBean(requestJson, ReqInfo.class);
            String result = this.bsService.qryInstantFeeList(reqInfo);
            return EuaCacheUtil.buildBillResponse(reqInfo.getBody(), reqInfo.getHead(), result);
        } catch (Exception e) {
            String exceptionMsg = e.getMessage(); // 异常描述
            response = Utils.dealExceptionResp(responseHead, tranId, responseJson);
            logger.debug("qryInstantFeeList(实时话费明细查询) Exception :{}", exceptionMsg);
            return response.toString();
        }
    }

    @Override
    public String qryNewCustBill(String requestJson) {
        logger.debug("(qryNewCustBill)新账单查询 input:{}", requestJson);
        String tranId = "";
        JSONObject response = new JSONObject();
        RespInfo responseJson = new RespInfo();
        RespHead responseHead = new RespHead();
        responseJson.setHead(responseHead);
        try {
            ReqInfo reqInfo = BeanUtil.toBean(requestJson, ReqInfo.class);

            String result = this.bsService.qryNewCustBill(reqInfo);
            return EuaCacheUtil.buildBillResponse(reqInfo.getBody(), reqInfo.getHead(), result);
        } catch (Exception e) {
            String exceptionMsg = e.getMessage(); // 异常描述
            response = Utils.dealExceptionResp(responseHead, tranId, responseJson);
            logger.debug("qryNewCustBill(账单查询) Exception :{}", exceptionMsg);
            return response.toString();
        }
    }

    @Override
    public String qrySpDetail(String requestJson1) {
        String tranId="";
        JSONObject response = new JSONObject();
        RespInfo responseJson = new RespInfo();
        RespHead responseHead = new RespHead();
        responseJson.setHead(responseHead);
        try{
            ReqInfo reqInfo = BeanUtil.toBean(requestJson1, ReqInfo.class);
            JSONObject body = reqInfo.getBody();
            String result = this.bsService.qrySpDetail(reqInfo);
            ReqHead reqHead = reqInfo.getHead();
            return EuaCacheUtil.buildBillResponse(body, reqHead, result);
        } catch (Exception e) {
            String exceptionMsg = e.getMessage(); // 异常描述
            response = Utils.dealExceptionResp(responseHead, tranId, responseJson);
            logger.debug("(qrySpDetail)账单中增值业务钻取查询 增值业务明细 Exception :{}" , exceptionMsg);
            return response.toString();
        }
    }
}
