package cn.js189.uqc.domain.qry;

import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 通过接入号的方式查询客户信息，生成请求报文
 * 
 * @author Mid
 */
public class QryCustomer extends QryCustomerBase {
	// 日志组件
	private static final Logger LOGGER = LoggerFactory.getLogger(QryCustomer.class);

	// 接入号码
	private String accNum;
	// 账号
	private String account;
	// 接入号码类型
	private String prodId;
	//客户id
	private String custId;
	//账户id
	private String acctId;
	//证件id
	private String certNum;
	//证件名称
	private String certType;

	public QryCustomer(String regionId, String uncompletedFlag, String accNum, String account, String prodId, String custId, String acctId, String certNum, String certType) {
		super(regionId, uncompletedFlag);
		this.accNum = accNum;
		this.account = account;
		this.prodId = prodId;
		this.custId = custId;
		this.acctId = acctId;
		this.certNum = certNum;
		this.certType = certType;
	}

	@Override
	protected JSONObject improveRequestObject(JSONObject requestObject) {
		requestObject.put("accNum", accNum);
		requestObject.put("account", account);
		requestObject.put("prodId", prodId);
		requestObject.put("custId", custId);
		requestObject.put("acctId", acctId);
		requestObject.put("certNum", certNum);
		requestObject.put("certType", certType);
		LOGGER.debug("requestObject:{}", requestObject.toString());
		return requestObject;
	}
	
}
