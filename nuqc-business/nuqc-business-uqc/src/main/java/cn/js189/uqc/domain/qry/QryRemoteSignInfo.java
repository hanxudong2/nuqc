package cn.js189.uqc.domain.qry;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class QryRemoteSignInfo extends QryRemoteSignInfoBase {

    // 日志组件
    private static final Logger LOGGER = LoggerFactory.getLogger(QryRemoteSignInfo.class);

    private String sysSource;//客户标识
    private String sysSourceSerial;//产品实例标识
    private JSONArray queryDealSeriNum;//远程签名业务号码

    /**
     * 查询产品信息构造器
     *
     * @param regionId :地区标识
     */
    public QryRemoteSignInfo(String regionId, String channelId, String staffCode, String sysSource) {
        super(regionId,channelId,staffCode,sysSource);
    }

    public String getSysSource() {
        return sysSource;
    }

    public void setSysSource(String sysSource) {
        this.sysSource = sysSource;
    }

    public String getSysSourceSerial() {
        return sysSourceSerial;
    }

    public void setSysSourceSerial(String sysSourceSerial) {
        this.sysSourceSerial = sysSourceSerial;
    }

    public JSONArray getQueryDealSeriNum() {
        return queryDealSeriNum;
    }

    public void setQueryDealSeriNum(JSONArray queryDealSeriNum) {
        this.queryDealSeriNum = queryDealSeriNum;
    }

    @Override
    protected JSONObject improveRequestObject(JSONObject requestObject) {
        requestObject.put("sysSource", sysSource);
        requestObject.put("sysSourceSerial", sysSourceSerial);
        requestObject.put("queryDealSeriNum", queryDealSeriNum);
        LOGGER.debug("requestObject:{}", requestObject.toJSONString());
        return requestObject;
    }
}
