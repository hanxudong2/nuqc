package cn.js189.uqc.domain;

public class OfferInfo {

	private String offerInfoId;

	private String offerId;

	private String originalId;

	private String offerName;

	private String chargeMoney;

	private String offerAttrId;

	private String offerAttrValue;

	private String ruleName;

	private String ruleValue;

	public String getOfferInfoId() {
		return offerInfoId;
	}

	public void setOfferInfoId(String offerInfoId) {
		this.offerInfoId = offerInfoId;
	}

	public String getOfferId() {
		return offerId;
	}

	public void setOfferId(String offerId) {
		this.offerId = offerId;
	}

	public String getOfferName() {
		return offerName;
	}

	public void setOfferName(String offerName) {
		this.offerName = offerName;
	}

	public String getChargeMoney() {
		return chargeMoney;
	}

	public void setChargeMoney(String chargeMoney) {
		this.chargeMoney = chargeMoney;
	}

	public String getRuleName() {
		return ruleName;
	}

	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}

	public String getRuleValue() {
		return ruleValue;
	}

	public void setRuleValue(String ruleValue) {
		this.ruleValue = ruleValue;
	}

	public String getOfferAttrId() {
		return offerAttrId;
	}

	public void setOfferAttrId(String offerAttrId) {
		this.offerAttrId = offerAttrId;
	}

	public String getOfferAttrValue() {
		return offerAttrValue;
	}

	public void setOfferAttrValue(String offerAttrValue) {
		this.offerAttrValue = offerAttrValue;
	}

	public String getOriginalId() {
		return originalId;
	}

	public void setOriginalId(String originalId) {
		this.originalId = originalId;
	}

	@Override
	public String toString() {
		return "OfferInfo{" +
				"offerInfoId='" + offerInfoId + '\'' +
				", offerId='" + offerId + '\'' +
				", originalId='" + originalId + '\'' +
				", offerName='" + offerName + '\'' +
				", chargeMoney='" + chargeMoney + '\'' +
				", offerAttrId='" + offerAttrId + '\'' +
				", offerAttrValue='" + offerAttrValue + '\'' +
				", ruleName='" + ruleName + '\'' +
				", ruleValue='" + ruleValue + '\'' +
				'}';
	}
}
