/**************************************************************************************** 

 ****************************************************************************************/
package cn.js189.uqc.domain;

import cn.js189.uqc.common.RequestParamBase;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * <Description> <br> 
 *  
 * @author yangyang<br>
 * @version 1.0<br>
 * @taskId <br>
 * @CreateDate 2019年6月3日 <br>
 */

public  class PortOutAbilityBase extends RequestParamBase {

 // 日志组件
    private static final Logger LOGGER = LoggerFactory.getLogger(PortOutAbilityBase.class);

    // svcCode 提供的API接口名称
    public static final String BSS_TCP_CONT_SVC_CODE = "js.intf.portOutAbility";

    // 地区标识
    protected String regionId;
    protected String npCode;//号码
    protected String credNumber;//证件号码
    protected String custName;//客户名称
    protected String  staffId;//员工Id
    protected String  orgId;//渠道Id

    /**
     * 查询销售品信息构造器
     * 
     * @param regionId:地区标识
     */
    public PortOutAbilityBase(String regionId, String npCode, String credNumber, String custName, String staffId, String orgId) {
        super();
        this.regionId = regionId;
        this.npCode = npCode;
        this.credNumber = credNumber;
        this.custName= custName;
        this.staffId = staffId;
        this.orgId = orgId;
    }

    @Override
    protected JSONObject generateTcpCont() {
        JSONObject aa = super.generateTcpCont(BSS_TCP_CONT_APP_KEY, BSS_TCP_CONT_SVC_CODE, null);
//        aa.put("appKey", "JS_DEFAULT");
//        aa.put("sign", "123456");
        return aa;
    }

    @Override
    protected JSONObject generateSvcCont() {
        JSONObject authenticationInfo = generateAuthenticationInfo();
        if (null == authenticationInfo) {
            LOGGER.error("generate authenticationInfo failure");
            return null;
        }

        JSONObject requestObject = generateRequestObject();
        if (null == requestObject) {
            LOGGER.error("generate requestObject failure");
            return null;
        }

        JSONObject svcCont = new JSONObject();
        svcCont.put("authenticationInfo", authenticationInfo);
        svcCont.put("requestObject", requestObject);
        return svcCont;
    }

    /**
     * 生成 svcCont 下的认证信息
     * 
     * @return
     */
    protected JSONObject generateAuthenticationInfo() {
        return super.generateSvcContAuthenticationInfo(regionId, null, null, null);
    }

    /**
     * 生成 svcCont 下的请求对象
     * 
     * @return
     */
    protected JSONObject generateRequestObject() {
        JSONObject requestObject = new JSONObject();
        requestObject.put("npCode", npCode);
        requestObject.put("credNumber", credNumber);
        requestObject.put("staffId", staffId);
        requestObject.put("orgId", orgId);
        requestObject.put("custName", custName);
        return requestObject;
    }



   
}
