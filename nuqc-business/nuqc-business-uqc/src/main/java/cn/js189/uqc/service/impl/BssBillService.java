package cn.js189.uqc.service.impl;

import cn.js189.common.constants.HttpConstant;
import cn.js189.common.enums.SystemEnum;
import cn.js189.common.util.MainUtils;
import cn.js189.common.util.StringUtils;
import cn.js189.common.util.exception.BaseAppException;
import cn.js189.common.util.exception.ExceptionUtil;
import cn.js189.common.util.helper.DateHelper;
import cn.js189.uqc.service.PubAddressIpervice;
import cn.js189.uqc.util.*;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author Administrator
 */
@Service
public class BssBillService {

    /**
     * 日志框架 <br>
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(BssBillService.class);

    /**
     * xml文件解析组件 <br>
     */
    @Resource
    private ResolveService resolveService;

    @Resource
    private PubAddressIpervice pubAddressIpervice;

    /**
     * Description: 账单查询<br>
     * 
     * @author Administrator<br>
     * @taskId <br>
     * @param body
     * @return <br>
     */
    public JSONObject callNewBillReq(JSONObject body) throws BaseAppException {
        // 计算接口调用时间
        long startTime = System.currentTimeMillis();
        // 地市编码校验
        if (StringUtils.isBlank(body.getString("areaCode"))) {
            ExceptionUtil.throwBusiException("uqc_1000");
        }
        // 号码校验
        if (StringUtils.isBlank(body.getString("accNbr"))) {
            ExceptionUtil.throwBusiException("uqc_1001");
        }
        // 产品ID校验
        if (StringUtils.isBlank(body.getString("productId"))) {
            ExceptionUtil.throwBusiException("uqc_1002");
        }
        // 账单时间校验
        if (StringUtils.isBlank(body.getString("billingCycleId"))) {
            ExceptionUtil.throwBusiException("uqc_1003");
        }
        // 查询类型校验
        if (StringUtils.isBlank(body.getString("queryFlag"))) {
            ExceptionUtil.throwBusiException("uqc_1004");
        }
        StringBuffer sb = new StringBuffer();
        sb.append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:web='http://www.example.org/Webservice_zz/'>");
        sb.append("<soapenv:Body><web:NEW_BILL_Req>");
        sb.append("<ORG_ID>" + SystemEnum.SGW_ORG_ID.getValue() + "</ORG_ID><PASSWORD>" + SystemEnum.SGW_PASSWORD.getValue() + "</PASSWORD>");
        // <ORG_ID>80003</ORG_ID><PASSWORD>Dq123_qr</PASSWORD>");
        sb.append("<QUERY_SERIAL>").append(body.getString("serialId")).append("</QUERY_SERIAL>");
        sb.append("<AREA_CODE>").append(body.getString("areaCode")).append("</AREA_CODE>");
        sb.append("<ACC_NBR>").append(body.getString("accNbr")).append("</ACC_NBR>");
        sb.append("<FAMILY_ID>").append(MainUtils.getNullEmptyForReq(body.getString("productId"))).append("</FAMILY_ID>");
        sb.append("<BILLING_CYCLE_ID>").append(MainUtils.getNullEmpty(body.getString("billingCycleId"))).append("</BILLING_CYCLE_ID>");
        sb.append("<QUERY_FLAG>").append(MainUtils.getNullEmpty(body.getString("queryFlag"))).append("</QUERY_FLAG>");
        sb.append("</web:NEW_BILL_Req>").append("</soapenv:Body>").append("</soapenv:Envelope>");

        LOGGER.debug(body.getString("serialId") + ":new charge CallNewBillReq input :" + sb.toString());
        //http://jseop.telecomjs.com:12555/WebCumul/CumulServices?appkey=js_dzqd
//        String url = URLPropertyUtil.getContextProperty(URLKey.UQC_CALLBILLQUERY_URL);
        String url  = pubAddressIpervice.getActionUrl("UQC_CALLBILLQUERY_URL","");
        LOGGER.debug("UQC_CALLBILLQUERY_URL 接口url---{}",url);
        String rsp = HttpClientHelperForBsn.post(sb.toString(), url, 6000, HttpConstant.UTF_ENCODING);
        long endTime = System.currentTimeMillis();

        LOGGER.debug(body.getString("serialId") + ":new charge CallNewBillReq output :" + rsp);

        LOGGER.debug("invoke1 METHODS process time:{}", (endTime - startTime));
        LOGGER.debug("rsp:{}", rsp);
        // 解析回参
        return resolveService.resolveBillReqData(rsp);
    }

    /**
     * Description: 余额查询<br>
     * 
     * @author Administrator<br>
     * @taskId <br>
     * @param body
     * @return <br>
     */
    public JSONObject callBalanceQrReq(JSONObject body) throws BaseAppException {
        // 地市编码校验
        if (StringUtils.isBlank(body.getString("areaCode"))) {
            ExceptionUtil.throwBusiException("uqc_1000");
        }
        // 号码校验
        if (StringUtils.isBlank(body.getString("accNbr"))) {
            ExceptionUtil.throwBusiException("uqc_1001");
        }
        // 产品ID校验
        if (StringUtils.isBlank(body.getString("productId"))) {
            ExceptionUtil.throwBusiException("uqc_1005");
        }
        // 余额查询参数组装
        long begTime = System.currentTimeMillis();
        StringBuffer sb = new StringBuffer();
        sb.append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:web='http://www.example.org/Webservice_zz/'>");
        sb.append("<soapenv:Body><web:BALANCE_QRReq>");
        sb.append("<ORG_ID>" + SystemEnum.SGW_ORG_ID.getValue() + "</ORG_ID><PASSWORD>" + SystemEnum.SGW_PASSWORD.getValue() + "</PASSWORD>");
        sb.append("<QUERY_SERIAL>").append(body.getString("serialId")).append("</QUERY_SERIAL>");
        sb.append("<AREA_CODE>").append(body.getString("areaCode")).append("</AREA_CODE>");
        sb.append("<ACC_NBR>").append(body.getString("accNbr")).append("</ACC_NBR>");
        sb.append("<FAMILY_ID>").append(MainUtils.getNullEmptyForReq(body.getString("family"))).append("</FAMILY_ID>");
        sb.append("<ACCT_NBR_97>").append(body.getString("accNbr97")).append("</ACCT_NBR_97>");
        sb.append("</web:BALANCE_QRReq>").append("</soapenv:Body>").append("</soapenv:Envelope>");

        LOGGER.debug(body.getString("serialId") + ":new charge CallBalanceQrReq input :" + sb.toString());
//        String url = URLPropertyUtil.getContextProperty(URLKey.UQC_CALLSGWCOMMON_UQL);
//        http://jseop.telecomjs.com:12555/WebCumul/CumulServices?appkey=js_dzqd
        String url  = pubAddressIpervice.getActionUrl("UQC_CALLSGWCOMMON_UQL","");
        LOGGER.debug("UQC_CALLSGWCOMMON_UQL 接口url---{}",url);
        String rsp = HttpClientHelperForBsn.post(sb.toString(), url, 6000, HttpConstant.UTF_ENCODING);
        long endTime = System.currentTimeMillis();
        LOGGER.debug(body.getString("serialId") + ":new charge CallBalanceQrReq output :" + rsp);
        LOGGER.debug("invoke2 METHODS process time:{}", (endTime - begTime));
        // 解析回参
        return resolveService.resolveBalanceQrReqData(rsp);
    }

    /**
     * Description: 实时话费查询<br>
     * 
     * @author Administrator<br>
     * @taskId <br>
     * @param body
     * @return
     * @throws BaseAppException <br>
     */
    public JSONObject callAcctItemfqReq(JSONObject body) throws BaseAppException {
        // 号码校验
        if (StringUtils.isBlank(body.getString("accNbr"))) {
            ExceptionUtil.throwBusiException("uqc_1001");
        }
        // 产品ID校验
        if (StringUtils.isBlank(body.getString("productId"))) {
            ExceptionUtil.throwBusiException("uqc_1002");
        }
        // 实时话费查询入参组装
        long begTime = System.currentTimeMillis();
        StringBuffer sf = new StringBuffer();
        sf.append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:web='http://www.example.org/Webservice_zz/'>");
        sf.append("<soapenv:Body>");
        sf.append("<web:ACCT_ITEM_F_QReq>");
        sf.append("<ORG_ID>" + SystemEnum.SGW_ORG_ID.getValue() + "</ORG_ID><PASSWORD>" + SystemEnum.SGW_PASSWORD.getValue() + "</PASSWORD>");
        sf.append("<AREA_CODE></AREA_CODE><ACC_NBR>" + body.getString("accNbr") + "</ACC_NBR><ACCT_NBR_97>" + body.getString("accNbr97")
                + "</ACCT_NBR_97>");
        sf.append("<GET_FLAG>" + MainUtils.getNullEmptyForReq(body.getString("getFlag")) + "</GET_FLAG><BEGIN_DATE>" + body.getString("begDate")
                + "</BEGIN_DATE>");
        sf.append("<END_DATE>" + body.getString("endDate") + "</END_DATE><PRODUCT_ID>" + MainUtils.getNullEmptyForReq(body.getString("productId"))
                + "</PRODUCT_ID>");
        sf.append("<QUERY_SERIAL>" + body.getString("serialId") + "</QUERY_SERIAL>");
        sf.append("<ONLY_SERV>" + MainUtils.getNullEmpty(body.getString("onlyServ")) + "</ONLY_SERV>");
        sf.append("<PRINT_FLAG>" + MainUtils.getNullEmpty(body.getString("printFlag")) + "</PRINT_FLAG>");
        sf.append("</web:ACCT_ITEM_F_QReq>");
        sf.append("</soapenv:Body>");
        sf.append("</soapenv:Envelope>");
        LOGGER.debug(body.getString("serialId") + ":new charge CallAcctItemfqReq input :" + sf.toString());
//        String url = URLPropertyUtil.getContextProperty(URLKey.UQC_CALLACCTITEMFQREQ_URL);
//        http://jseop.telecomjs.com:12555/WebCumul/CumulServices?appkey=js_dzqd
        String url  = pubAddressIpervice.getActionUrl("UQC_CALLACCTITEMFQREQ_URL","");
        LOGGER.debug("UQC_CALLACCTITEMFQREQ_URL 接口url---{}",url);
        String rsp = HttpClientHelperForBsn.post(sf.toString(), url, 6000, HttpConstant.UTF_ENCODING);
        long endTime = System.currentTimeMillis();
        LOGGER.debug(body.getString("serialId") + ":new charge CallAcctItemfqReq output ={}，耗时={}", rsp, (endTime - begTime));
        return resolveService.resolveAcctItemfqReq(rsp);
    }

    /**
     * Description: 账户欠费查询<br>
     * 
     * @author Administrator<br>
     * @taskId <br>
     * @param body
     * @return
     * @throws BaseAppException <br>
     */
    public JSONObject callNewOthGetOwe(JSONObject body) throws BaseAppException {
        long begTime = System.currentTimeMillis();
        // 地市编码校验
        if (StringUtils.isBlank(body.getString("areaCode"))) {
            ExceptionUtil.throwBusiException("uqc_1000");
        }
        // 号码校验
        if (StringUtils.isBlank(body.getString("accNbr"))) {
            ExceptionUtil.throwBusiException("uqc_1001");
        }
        // 产品ID校验
        if (StringUtils.isBlank(body.getString("productId"))) {
            ExceptionUtil.throwBusiException("uqc_1005");
        }
        StringBuffer sb = new StringBuffer();
        sb.append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:web='http://www.example.org/Webservice_zz/'>");
        sb.append("<soapenv:Body><web:OTH_GET_OWEReq>");
        sb.append("<REMOTE_SOURCE>" + SystemEnum.SGW_ORG_ID.getValue() + "</REMOTE_SOURCE><REMOTE_PASSWD>" + SystemEnum.SGW_PASSWORD.getValue()
                + "</REMOTE_PASSWD>");
        sb.append("<REMOTE_ACCT>" + SystemEnum.SGW_ORG_ID.getValue() + "</REMOTE_ACCT>");
        sb.append("<AREA_CODE>").append(body.getString("areaCode")).append("</AREA_CODE>");
        sb.append("<GET_FLAG>").append(MainUtils.getLong(body.getString("getFlag"))).append("</GET_FLAG>");
        sb.append("<STAFF_ID>").append(MainUtils.getLong(body.getString("staffId"))).append("</STAFF_ID>");
        sb.append("<ACC_NBR>").append(body.getString("accNbr")).append("</ACC_NBR>");
        sb.append("<CIPHER_STRING>").append("9527.js").append("</CIPHER_STRING>");
        sb.append("</web:OTH_GET_OWEReq>").append("</soapenv:Body>").append("</soapenv:Envelope>");
        LOGGER.debug(":new charge callNewDataTicketQrReq input :" + sb.toString());
//        String url = URLPropertyUtil.getContextProperty(URLKey.UQC_CALLSGWCOMMON_UQL);
//        http://jseop.telecomjs.com:12555/WebCumul/CumulServices?appkey=js_dzqd
        String url  = pubAddressIpervice.getActionUrl("UQC_CALLSGWCOMMON_UQL","");
        LOGGER.debug("UQC_CALLSGWCOMMON_UQL 接口url---{}",url);
        String rsp = HttpClientHelperForBsn.post(sb.toString(), url, 6000, HttpConstant.UTF_ENCODING);
        long endTime = System.currentTimeMillis();
        LOGGER.debug(":new charge callNewFlowDatauseQr output ={}，耗时={}", rsp, (endTime - begTime));
        // 解析回参
        return resolveService.resolveOthGetOweData(rsp);
    }

    /**
     * Description: 短信详单查询 <br>
     * 
     * @author Administrator<br>
     * @taskId <br>
     * @param body
     * @return
     * @throws BaseAppException <br>
     */
    public JSONObject callNewSmTicket(JSONObject body) throws BaseAppException {
        // 地市编码校验
        if (StringUtils.isBlank(body.getString("areaCode"))) {
        	ExceptionUtil.throwBusiException("uqc_1000");
        }
        // 号码校验
        if (StringUtils.isBlank(body.getString("accNbr"))) {
            ExceptionUtil.throwBusiException("uqc_1001");
        }
        StringBuffer sb = new StringBuffer();
        sb.append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:web='http://www.example.org/Webservice_zz/'>");
        sb.append("<soapenv:Body><web:SM_TICKET_GUReq>");
        sb.append("<ORG_ID>" + SystemEnum.SGW_ORG_ID.getValue() + "</ORG_ID><PASSWORD>" + SystemEnum.SGW_PASSWORD.getValue() + "</PASSWORD>");
        sb.append("<ACC_NBR>").append(body.getString("accNbr")).append("</ACC_NBR>");
        sb.append("<ACCT_NBR_97>").append(body.getString("accNbr97")).append("</ACCT_NBR_97>");
        sb.append("<GET_FLAG>").append(MainUtils.getLong(body.getString("getFlag"))).append("</GET_FLAG>");
        sb.append("<BEGIN_DATE>").append(body.getString("begDate")).append("</BEGIN_DATE>");
        sb.append("<END_DATE>").append(body.getString("endDate")).append("</END_DATE>");
        sb.append("<AREA_CODE>").append(body.getString("areaCode")).append("</AREA_CODE>");
        sb.append("</web:SM_TICKET_GUReq>").append("</soapenv:Body>").append("</soapenv:Envelope>");

        LOGGER.debug(":new charge callNewSmTicket input :" + sb.toString());
        long begTime = System.currentTimeMillis();
//        String url = URLPropertyUtil.getContextProperty(URLKey.UQC_COMMONBILLSERVICE_URL);
//        http://jseop.telecomjs.com:12555/WebCumul/CumulServices?appkey=js_dzqd
        String url  = pubAddressIpervice.getActionUrl("UQC_COMMONBILLSERVICE_URL","");
        LOGGER.debug("UQC_COMMONBILLSERVICE_URL 接口url---{}",url);
        String rsp = HttpClientHelperForBsn.post(sb.toString(), url, HttpConstant.CONNECT_TIMEOUT, HttpConstant.GB2312_ENCODING);
        long endTime = System.currentTimeMillis();
        LOGGER.debug(":new charge callNewFlowDatauseQr output={}，耗时={}", rsp, (endTime - begTime));

        // 解析回参
        return resolveService.resolveCallNewSmTicket(rsp);
    }

    /**
     * Description: 语音详单查询<br>
     * @author Administrator<br>
     * @taskId <br>
     * @param body
     * @return <br>
     */
    public String callNewVoiceTicket(JSONObject body) throws BaseAppException {
        // 地市编码校验
        if (StringUtils.isBlank(body.getString("areaCode"))) {
            ExceptionUtil.throwBusiException("uqc_1000");
        }
        // 号码校验
        if (StringUtils.isBlank(body.getString("accNbr"))) {
            ExceptionUtil.throwBusiException("uqc_1001");
        }
//        String newUrl =BSUrlConstant.TKT_YI_VOICE_TICKET_QR;
//        http://jsjteop.telecomjs.com:8764/jseop/billing_zw/TktYiVoiceTicketQr/TktYiVoiceTicketQr
        String newUrl  = pubAddressIpervice.getActionUrl("TKT_YI_VOICE_TICKET_QR","");
        LOGGER.info("callNewVoiceTicket 语音详单查询 ---url:{}", newUrl);
		String accNbr = body.getString("accNbr");
		String begDate = body.getString("begDate"); 
		String endDate = body.getString("endDate"); 
		String getFlag = DateHelper.isAllMonthFlag(begDate, endDate)?"1":"3";
		//新语音详单查询 不能跨月
        JSONObject paramJson = CreateParamForAccountingCenter.createParamForTktYiVoiceTicketQr(accNbr, getFlag, begDate, endDate);
        String resultString = HttpClientHelperForEop.postWithMNP(newUrl, paramJson.toJSONString());
        LOGGER.info("callNewVoiceTicket 语音详单查询 企信出参:{}", resultString);
        resultString = HandleParamForAccountingCenter.builderQueryResponse(resultString);
        LOGGER.info("callNewVoiceTicket 语音详单查询 builderQueryResponse出参:{}", resultString);
        return  HandleParamForAccountingCenter.builderResponseForTktYiVoiceTicketQr(resultString);
    }
    
    /**
     * Description: 无线宽带上网清单<br> 
     *  
     * @author Administrator<br>
     * @taskId <br>
     * @param body
     * @return
     * @throws BaseAppException <br>
     */ 
    public JSONObject callNewDataTicketQrReq(JSONObject body) throws BaseAppException{
        // 地市编码校验
        if (StringUtils.isBlank(body.getString("areaCode"))) {
            ExceptionUtil.throwBusiException("uqc_1000");
        }
        // 号码校验
        if (StringUtils.isBlank(body.getString("accNbr"))) {
            ExceptionUtil.throwBusiException("uqc_1001");
        }
        try {
            StringBuffer sb = new StringBuffer();
            sb.append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:web='http://www.example.org/Webservice_zz/'>");
            sb.append("<soapenv:Body><web:NEW_DATA_TICKET_QReq>");
            sb.append("<ORG_ID>"+SystemEnum.SGW_ORG_ID.getValue()+"</ORG_ID><PASSWORD>"+SystemEnum.SGW_PASSWORD.getValue()+"</PASSWORD>");
            sb.append("<ACC_NBR>").append(body.getString("accNbr")).append("</ACC_NBR>");
            sb.append("<ACCT_NBR_97>").append(body.getString("accNbr97")).append("</ACCT_NBR_97>");
            sb.append("<GET_FLAG>").append(MainUtils.getLong(body.getString("getFlag"))).append("</GET_FLAG>");
            sb.append("<BEGIN_DATE>").append(body.getString("begDate")).append("</BEGIN_DATE>");
            sb.append("<END_DATE>").append(body.getString("endDate")).append("</END_DATE>");
            sb.append("<AREA_CODE>").append(body.getString("areaCode")).append("</AREA_CODE>");
            sb.append("<FAMILY_ID>").append(MainUtils.getLong(body.getString("family"))).append("</FAMILY_ID>");
            sb.append("</web:NEW_DATA_TICKET_QReq>").append("</soapenv:Body>").append("</soapenv:Envelope>");
            
            LOGGER.debug(":new charge callNewDataTicketQrReq input :"+sb.toString());
            // long begTime = System.currentTimeMillis();
//            String url = URLPropertyUtil.getContextProperty(URLKey.UQC_CALLNEWDATATICKETQRREQ_URL);
//            http://jseop.telecomjs.com:12555/WebCumul/CumulServices?appkey=js_dzqd
            String url  = pubAddressIpervice.getActionUrl("UQC_CALLNEWDATATICKETQRREQ_URL","");
            LOGGER.debug("UQC_CALLNEWDATATICKETQRREQ_URL ---url:{}", url);
            String rsp = HttpClientHelperForBsn.post(sb.toString(), url, 6000, HttpConstant.GB2312_ENCODING);
            // long endTime = System.currentTimeMillis();
            LOGGER.debug(":new charge callNewFlowDatauseQr output :"+rsp);
            // LOGGER.debug("invoke3 METHODS process time:{}",(endTime-begTime));
            
            //解析回参
            JSONObject jsonObj = resolveService.resolveCallNewDataTicketQrReq(rsp);
            return jsonObj;
        } catch (Exception e) {
            LOGGER.debug("callNewDataTicketQrReq new charge Exception :"+e.getMessage());
        }
        return null;
    }
    
    /**
     * Description: 其他增值业务详单<br> 
     *  
     * @author Administrator<br>
     * @taskId <br>
     * @param body
     * @return
     * @throws BaseAppException <br>
     */ 
    public String callNewValueTicketQr(JSONObject body) throws BaseAppException{
        // 地市编码校验
        if (StringUtils.isBlank(body.getString("areaCode"))) {
            ExceptionUtil.throwBusiException("uqc_1000");
        }
        // 号码校验
        if (StringUtils.isBlank(body.getString("accNbr"))) {
            ExceptionUtil.throwBusiException("uqc_1001");
        }
        try {
            StringBuffer sb = new StringBuffer();
            sb.append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:web='http://www.example.org/Webservice_zz/'>");
            sb.append("<soapenv:Body><web:VALUE_TICKETGUReq>");
            sb.append("<ORG_ID>"+SystemEnum.SGW_ORG_ID.getValue()+"</ORG_ID><PASSWORD>"+SystemEnum.SGW_PASSWORD.getValue()+"</PASSWORD>");
            sb.append("<ACC_NBR>").append(body.getString("accNbr")).append("</ACC_NBR>");
            sb.append("<ACCT_NBR_97>").append(body.getString("accNbr97")).append("</ACCT_NBR_97>");
            sb.append("<GET_FLAG>").append(MainUtils.getNullEmpty(body.getString("getFlag"))).append("</GET_FLAG>");
            sb.append("<BEGIN_DATE>").append(body.getString("begDate")).append("</BEGIN_DATE>");
            sb.append("<END_DATE>").append(body.getString("endDate")).append("</END_DATE>");
            sb.append("<AREA_CODE>").append(body.getString("areaCode")).append("</AREA_CODE>");
            sb.append("</web:VALUE_TICKETGUReq>").append("</soapenv:Body>").append("</soapenv:Envelope>");
            
            LOGGER.debug(":new charge callNewValueTicketQr input :"+sb.toString());
            long begTime = System.currentTimeMillis();
//            String url = URLPropertyUtil.getContextProperty(URLKey.UQC_CALLNEWVALUETICKETQR_URL);
//            http://jseop.telecomjs.com:12555/WebCumul/CumulServices?appkey=js_dzqd
            String url  = pubAddressIpervice.getActionUrl("UQC_CALLNEWVALUETICKETQR_URL","");
            LOGGER.debug("UQC_CALLNEWVALUETICKETQR_URL ---url:{}", url);
            String rsp = HttpClientHelperForBsn.post(sb.toString(), url, 6000, HttpConstant.GB2312_ENCODING);
            long endTime=System.currentTimeMillis();
            LOGGER.debug(":new charge callNewFlowDatauseQr output :"+rsp);
            LOGGER.debug("新计费接口统一接口参数（callNewValueTicketQr-其它增值业务查询）:"+endTime+"#16105#"+"#"+body.getString("accNbr")+"#"+(endTime-begTime));
            
            //解析回参
//            JSONObject jsonObj = resolveService.resolveCallNewValueTicketQr(rsp);
            return rsp;
        } catch (Exception e) {
            LOGGER.debug("CallAcctItemfqReq new charge Exception :"+e.getMessage());
        }
        return null; 
    }
    
    /**
     * Description:wlan一次一密 上网查询 <br> 
     *  
     * @author Administrator<br>
     * @taskId <br>
     * @param body
     * @return <br>
     */ 
    public String callNewWifiTicketQr(JSONObject body) throws BaseAppException{
        // 地市编码校验
        if (StringUtils.isBlank(body.getString("areaCode"))) {
            ExceptionUtil.throwBusiException("uqc_1000");
        }
        // 号码校验
        if (StringUtils.isBlank(body.getString("accNbr"))) {
            ExceptionUtil.throwBusiException("uqc_1001");
        }
        try {
            StringBuffer sf = new StringBuffer();
            sf.append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:web='http://www.example.org/Webservice_zz/'>");
            sf.append("<soapenv:Body>");
            sf.append("<web:WIFI_TICKETReq>");
            sf.append("<ORG_ID>"+SystemEnum.SGW_ORG_ID.getValue()+"</ORG_ID><PASSWORD>"+SystemEnum.SGW_PASSWORD.getValue()+"</PASSWORD>");
            sf.append("<AREA_CODE>").append(body.getString("areaCode")).append("</AREA_CODE>");
            sf.append("<ACC_NBR>").append(body.getString("accNbr")).append("</ACC_NBR>");
            sf.append("<ACCT_NBR_97>").append(body.getString("accNbr97")).append("</ACCT_NBR_97>");
            sf.append("<GET_FLAG>").append(MainUtils.getNullEmpty(body.getString("getFlag"))).append("</GET_FLAG>");
            sf.append("<BEGIN_DATE>").append(body.getString("begDate")).append("</BEGIN_DATE>");
            sf.append("<END_DATE>").append(body.getString("endDate")).append("</END_DATE>");
            sf.append("<FAMILY_ID>").append(MainUtils.getNullEmpty(body.getString("family"))).append("</FAMILY_ID>");
            sf.append("</web:WIFI_TICKETReq>").append("</soapenv:Body>").append("</soapenv:Envelope>");
            
            LOGGER.debug(":new charge callNewWifiTicketQr input :"+sf.toString());
            // long begTime = System.currentTimeMillis();
//            String url = URLPropertyUtil.getContextProperty(URLKey.UQC_CALLNEWDATATICKETQRREQ_URL);
//            http://jseop.telecomjs.com:12555/WebCumul/CumulServices?appkey=js_dzqd
            String url  = pubAddressIpervice.getActionUrl("UQC_CALLNEWDATATICKETQRREQ_URL","");
            LOGGER.debug("UQC_CALLNEWDATATICKETQRREQ_URL ---url:{}", url);
            String rsp = HttpClientHelperForBsn.post(sf.toString(), url, 6000, HttpConstant.GB2312_ENCODING);
            // long endTime = System.currentTimeMillis();
            // LOGGER.debug(":new charge callNewWifiTicketQr output={},耗时={}",rsp,(endTime - begTime));
            //解析回参
            JSONObject jsonObj = resolveService.resolveCallNewWifiTicketQr(rsp);
            return jsonObj.toString();
        } catch (Exception e) {
            LOGGER.debug("callNewWifiTicketQr new charge Exception :"+e.getMessage());
        }
        return "";
    }
    
    /**
     * Description: C网短信/彩信增值详单<br> 
     *  
     * @author Administrator<br>
     * @taskId <br>
     * @param body
     * @return
     * @throws BaseAppException <br>
     */ 
    public JSONObject callNewSmsValueTicketQr(JSONObject body) throws BaseAppException{
        // 地市编码校验
        if (StringUtils.isBlank(body.getString("areaCode"))) {
            ExceptionUtil.throwBusiException("uqc_1000");
        }
        // 号码校验
        if (StringUtils.isBlank(body.getString("accNbr"))) {
            ExceptionUtil.throwBusiException("uqc_1001");
        }
        try {
    
        	
        	
            StringBuffer sf = new StringBuffer();
            sf.append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:web='http://www.example.org/Webservice_zz/'>");
            sf.append("<soapenv:Body>");
            sf.append("<web:SMS_VALUE_TICKET_QReq>");
            sf.append("<ORG_ID>"+SystemEnum.SGW_ORG_ID.getValue()+"</ORG_ID><PASSWORD>"+SystemEnum.SGW_PASSWORD.getValue()+"</PASSWORD>");
            sf.append("<AREA_CODE>").append(body.getString("areaCode")).append("</AREA_CODE>");
            sf.append("<ACC_NBR>").append(body.getString("accNbr")).append("</ACC_NBR>");
            sf.append("<ACCT_NBR_97>").append(body.getString("accNbr97")).append("</ACCT_NBR_97>");
            sf.append("<GET_FLAG>").append(MainUtils.getNullEmpty(body.getString("getFlag"))).append("</GET_FLAG>");
            sf.append("<BEGIN_DATE>").append(body.getString("begDate")).append("</BEGIN_DATE>");
            sf.append("<END_DATE>").append(body.getString("endDate")).append("</END_DATE>");
            sf.append("</web:SMS_VALUE_TICKET_QReq>").append("</soapenv:Body>").append("</soapenv:Envelope>");
            
            LOGGER.debug(":new charge callNewSmsValueTicketQr input :"+sf.toString());
            long begTime = System.currentTimeMillis();
//            String url = URLPropertyUtil.getContextProperty(URLKey.UQC_CALLNEWDATATICKETQRREQ_URL);
//            http://jseop.telecomjs.com:12555/WebCumul/CumulServices?appkey=js_dzqd
            String url  = pubAddressIpervice.getActionUrl("UQC_CALLNEWDATATICKETQRREQ_URL","");
            LOGGER.debug("UQC_CALLNEWDATATICKETQRREQ_URL ---url:{}", url);
            String rsp = HttpClientHelperForBsn.post(sf.toString(), url, 6000, HttpConstant.GB2312_ENCODING);
            long endTime = System.currentTimeMillis();
            
            LOGGER.debug("新计费接口统一接口参数（callNewSmsValueTicketQr-C网短信/彩信增值清单），入参={}，回参={}，耗时={}",sf,rsp,(endTime - begTime));
            //解析回参
            JSONObject jsonObj = resolveService.resolveCallNewSmsValueTicketQr(rsp);
            return jsonObj;
        } catch (Exception e) {
            LOGGER.debug("C网短信/彩信增值清单异常信息 :"+e);
        }
        return null;
    }
    
    /**
     * Description: 窄带清单查询<br> 
     *  
     * @author Administrator<br>
     * @taskId <br>
     * @param body
     * @return
     * @throws BaseAppException <br>
     */ 
    public JSONObject callNewNarrowDataQr(JSONObject body) throws BaseAppException{
        // 地市编码校验
        if (StringUtils.isBlank(body.getString("areaCode"))) {
            ExceptionUtil.throwBusiException("uqc_1000");
        }
        // 号码校验
        if (StringUtils.isBlank(body.getString("accNbr"))) {
            ExceptionUtil.throwBusiException("uqc_1001");
        }
        try {
            StringBuffer sf = new StringBuffer();
            sf.append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:web='http://www.example.org/Webservice_zz/'>");
            sf.append("<soapenv:Body>");
            sf.append("<web:NARROW_DATA_GUReq>");
            sf.append("<ORG_ID>"+SystemEnum.SGW_ORG_ID.getValue()+"</ORG_ID><PASSWORD>"+SystemEnum.SGW_PASSWORD.getValue()+"</PASSWORD>");
            sf.append("<AREA_CODE>").append(body.getString("areaCode")).append("</AREA_CODE>");
            sf.append("<QUERY_SERIAL>"+body.getString("serialId")+"</QUERY_SERIAL>");
            sf.append("<ACC_NBR>").append(body.getString("accNbr")).append("</ACC_NBR>");
            sf.append("<ACCT_NAME>").append(body.getString("acctName")).append("</ACCT_NAME>");
            sf.append("<GET_FLAG>").append(MainUtils.getNullEmpty(body.getString("getFlag"))).append("</GET_FLAG>");
            sf.append("<BEGIN_DATE>").append(body.getString("begDate")).append("</BEGIN_DATE>");
            sf.append("<END_DATE>").append(body.getString("endDate")).append("</END_DATE>");
            sf.append("<TICKET_ID>").append(0).append("</TICKET_ID>");
            sf.append("<PRODUCT_ID>").append(0).append("</PRODUCT_ID>");
            sf.append("</web:NARROW_DATA_GUReq>").append("</soapenv:Body>").append("</soapenv:Envelope>");
            
            LOGGER.debug(":new charge CallNewNarrowDataQr input :"+sf.toString());
            long begTime = System.currentTimeMillis();
//            String url = URLPropertyUtil.getContextProperty(URLKey.UQC_CALLNEWNARROWDATAQR_URL);
//            http://jseop.telecomjs.com:12555/WebCumul/CumulServices?appkey=js_dzqd
            String url  = pubAddressIpervice.getActionUrl("UQC_CALLNEWNARROWDATAQR_URL","");
            LOGGER.debug("UQC_CALLNEWNARROWDATAQR_URL ---url:{}", url);
            String rsp = HttpClientHelperForBsn.post(sf.toString(), url, 6000, HttpConstant.GB2312_ENCODING);
            long endTime = System.currentTimeMillis();
            LOGGER.debug("新计费接口统一接口参数（CallNewNarrowDataQr-窄带清单）入参={}，出参={}，耗时={}",sf,rsp,(endTime - begTime));
            
            //解析回参
            JSONObject jsonObj = resolveService.resolveCallNewNarrowDataQr(rsp);
            return jsonObj;
        } catch (Exception e) {
            LOGGER.debug("新计费接口统一接口参数（CallNewNarrowDataQr-窄带清单）:"+e);
        }
        return null;
    }
    
    /**
     * Description:互联星空清单查询 <br> 
     *  
     * @author Administrator<br>
     * @taskId <br>
     * @param body
     * @return
     * @throws BaseAppException <br>
     */ 
    public JSONObject callNewInfoDataQr(JSONObject body)throws BaseAppException{
        // 地市编码校验
        if (StringUtils.isBlank(body.getString("areaCode"))) {
            ExceptionUtil.throwBusiException("uqc_1000");
        }
        // 号码校验
        if (StringUtils.isBlank(body.getString("accNbr"))) {
            ExceptionUtil.throwBusiException("uqc_1001");
        }
        try {
            StringBuffer sf = new StringBuffer();
            sf.append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:web='http://www.example.org/Webservice_zz/'>");
            sf.append("<soapenv:Body>");
            sf.append("<web:INFO_DATA_GUReq>");
            sf.append("<ORG_ID>"+SystemEnum.SGW_ORG_ID.getValue()+"</ORG_ID><PASSWORD>"+SystemEnum.SGW_PASSWORD.getValue()+"</PASSWORD>");
            sf.append("<AREA_CODE>").append(body.getString("areaCode")).append("</AREA_CODE>");
            sf.append("<QUERY_SERIAL>"+body.getString("serialId")+"</QUERY_SERIAL>");
            sf.append("<ACC_NBR>").append(body.getString("accNbr")).append("</ACC_NBR>");
            sf.append("<ACCT_NAME>").append(body.getString("acctName")).append("</ACCT_NAME>");
            sf.append("<GET_FLAG>").append(MainUtils.getNullEmpty(body.getString("getFlag"))).append("</GET_FLAG>");
            sf.append("<BEGIN_DATE>").append(body.getString("begDate")).append("</BEGIN_DATE>");
            sf.append("<END_DATE>").append(body.getString("endDate")).append("</END_DATE>");
            sf.append("<TICKET_ID>").append(0).append("</TICKET_ID>");
            sf.append("<PRODUCT_ID>").append(MainUtils.getNullEmpty(body.getString("productId"))).append("</PRODUCT_ID>");
            sf.append("</web:INFO_DATA_GUReq>").append("</soapenv:Body>").append("</soapenv:Envelope>");
            
            LOGGER.debug("互联星空请求参数:"+sf.toString());
            long begTime = System.currentTimeMillis();
//            String url = URLPropertyUtil.getContextProperty(URLKey.UQC_CALLNEWINFODATAQR_URL);
//            http://jseop.telecomjs.com:12555/WebCumul/CumulServices?appkey=js_dzqd
            String url  = pubAddressIpervice.getActionUrl("UQC_CALLNEWINFODATAQR_URL","");
            LOGGER.debug("UQC_CALLNEWINFODATAQR_URL ---url:{}", url);
            String rsp = HttpClientHelperForBsn.post(sf.toString(), url, 6000, HttpConstant.GB2312_ENCODING);
            long endTime = System.currentTimeMillis();
            LOGGER.debug("新计费接口统一接口参数（callNewInfoDataQr-互联星空清单），请求入参={}，回参={}，耗时={}",sf.toString(),rsp,(endTime - begTime));
            //解析回参
            JSONObject jsonObj = resolveService.resolveCallNewInfoDataQr(rsp);
            return jsonObj;
        } catch (Exception e) {
            LOGGER.debug("新计费接口统一接口参数（CallNewMessageTicketQr-互联星空清单）:"+e);
        }
        return null;
    }
    
    /**
     * Description:新业务查询 <br> 
     *  
     * @author Administrator<br>
     * @taskId <br>
     * @return
     * @throws BaseAppException <br>
     */ 
    public JSONObject callNewHttpSvrTicQr(JSONObject body) throws BaseAppException{
        // 地市编码校验
        if (StringUtils.isBlank(body.getString("areaCode"))) {
            ExceptionUtil.throwBusiException("uqc_1000");
        }
        // 号码校验
        if (StringUtils.isBlank(body.getString("accNbr"))) {
            ExceptionUtil.throwBusiException("uqc_1001");
        }
        try {
            StringBuffer sf = new StringBuffer();
            sf.append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:web='http://www.example.org/Webservice_zz/'>");
            sf.append("<soapenv:Body>");
            sf.append("<web:NEW_SVR_TIC_GUReq>");
            sf.append("<ORG_ID>"+SystemEnum.SGW_ORG_ID.getValue()+"</ORG_ID><PASSWORD>"+SystemEnum.SGW_PASSWORD.getValue()+"</PASSWORD>");
            sf.append("<AREA_CODE>").append(body.getString("areaCode")).append("</AREA_CODE>");
            sf.append("<QUERY_SERIAL>"+body.getString("serialId")+"</QUERY_SERIAL>");
            sf.append("<ACC_NBR>").append(body.getString("accNbr")).append("</ACC_NBR>");
            sf.append("<ACCT_NBR_97>").append(body.getString("accNbr97")).append("</ACCT_NBR_97>");
            sf.append("<GET_FLAG>").append(MainUtils.getLong(body.getString("getFlag"))).append("</GET_FLAG>");
            sf.append("<BEGIN_DATE>").append(body.getString("begDate")).append("</BEGIN_DATE>");
            sf.append("<END_DATE>").append(body.getString("endDate")).append("</END_DATE>");
            sf.append("<TICKET_ID>").append(0).append("</TICKET_ID>");
            sf.append("<PRODUCT_ID>").append(MainUtils.getLong(body.getString("productId"))).append("</PRODUCT_ID>");
            sf.append("</web:NEW_SVR_TIC_GUReq>").append("</soapenv:Body>").append("</soapenv:Envelope>");
            
            LOGGER.debug("新业务查询入参={}"+sf.toString());
            long begTime = System.currentTimeMillis();
//            String url = URLPropertyUtil.getContextProperty(URLKey.UQC_CALLNEWHTTPSVRTICQR_URL);
//            http://jseop.telecomjs.com:12555/WebCumul/CumulServices?appkey=js_dzqd
            String url  = pubAddressIpervice.getActionUrl("UQC_CALLNEWHTTPSVRTICQR_URL","");
            LOGGER.debug("UQC_CALLNEWHTTPSVRTICQR_URL ---url:{}", url);
            String rsp = HttpClientHelperForBsn.post(sf.toString(), url, 6000, HttpConstant.GB2312_ENCODING);
            long endTime=System.currentTimeMillis();
            
            LOGGER.debug("新计费接口统一接口参数（callNewHttpSvrTicQr-新业务清单），入参={}，出参={}，耗时={}",sf.toString(),rsp,(endTime - begTime));
            
            //解析回参
            JSONObject jsonObj = resolveService.resolveCallNewHttpSvrTicQr(rsp);
            return jsonObj;
        } catch (Exception e) {
            LOGGER.debug("新计费接口统一接口参数（callNewHttpSvrTicQr-新业务清单）异常="+e);
        }
        return null;
    }
    
    /**
     * Description: 电话清单查询<br> 
     *  
     * @author Administrator<br>
     * @taskId <br>
     * @param body
     * @return
     * @throws BaseAppException <br>
     */ 
    public JSONObject callNewMessageTicketQr(JSONObject body) throws BaseAppException{
        // 地市编码校验
        if (StringUtils.isBlank(body.getString("areaCode"))) {
            ExceptionUtil.throwBusiException("uqc_1000");
        }
        // 号码校验
        if (StringUtils.isBlank(body.getString("accNbr"))) {
            ExceptionUtil.throwBusiException("uqc_1001");
        }
        try {
            StringBuffer sf = new StringBuffer();
            sf.append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:web='http://www.example.org/Webservice_zz/'>");
            sf.append("<soapenv:Body>");
            sf.append("<web:MESSAGE_TICKET_GUReq>");
            sf.append("<ORG_ID>"+SystemEnum.SGW_ORG_ID.getValue()+"</ORG_ID><PASSWORD>"+SystemEnum.SGW_PASSWORD.getValue()+"</PASSWORD>");
            sf.append("<AREA_CODE>").append(body.getString("areaCode")).append("</AREA_CODE>");
            sf.append("<QUERY_SERIAL>"+body.getString("serialId")+"</QUERY_SERIAL>");
            sf.append("<ACC_NBR>").append(body.getString("accNbr")).append("</ACC_NBR>");
            sf.append("<ACCT_NBR_97>").append(body.getString("accNbr97")).append("</ACCT_NBR_97>");
            sf.append("<GET_FLAG>").append(MainUtils.getNullEmpty(body.getString("getFlag"))).append("</GET_FLAG>");
            sf.append("<BEGIN_DATE>").append(body.getString("begDate")).append("</BEGIN_DATE>");
            sf.append("<END_DATE>").append(body.getString("endDate")).append("</END_DATE>");
            sf.append("<TICKET_ID>").append(0).append("</TICKET_ID>");
            sf.append("<PRODUCT_ID>").append(MainUtils.getNullEmpty(body.getString("productId"))).append("</PRODUCT_ID>");
            sf.append("</web:MESSAGE_TICKET_GUReq>").append("</soapenv:Body>").append("</soapenv:Envelope>");
            
            LOGGER.debug("电话清单查询入参={}",sf.toString());
            long begTime = System.currentTimeMillis();
//            String url = URLPropertyUtil.getContextProperty(URLKey.UQC_CALLNEWMESSAGETICKETQR_URL);
//            http://jseop.telecomjs.com:12555/WebCumul/CumulServices?appkey=js_dzqd
            String url  = pubAddressIpervice.getActionUrl("UQC_CALLNEWMESSAGETICKETQR_URL","");
            LOGGER.debug("UQC_CALLNEWMESSAGETICKETQR_URL ---url:{}", url);
            String rsp = HttpClientHelperForBsn.post(sf.toString(), url, 6000, HttpConstant.GB2312_ENCODING);
            long endTime = System.currentTimeMillis();
            
            LOGGER.debug("新计费接口统一接口参数（CallNewMessageTicketQr-电话信息清单）,入参={}，出参={}，耗时={}",sf,rsp,(endTime - begTime));
            //解析回参
            JSONObject jsonObj = resolveService.resolveCallNewMessageTicketQr(rsp);
            return jsonObj;
        } catch (Exception e) {
            LOGGER.debug("新计费接口统一接口参数（CallNewMessageTicketQr-电话信息清单）异常={}",e);
        }
        return null;
    }
    
    /**
     * 
     * Description: 余额结转-20152<br> 
     *  
     * @author zhpp<br>
     * @taskId <br>
     * @param body
     * @return
     * @throws BaseAppException <br>
     */
    public JSONObject callNewBlanceSwitch(JSONObject body) throws BaseAppException{
        try {
            StringBuffer sf = new StringBuffer();
            sf.append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:web='http://www.example.org/Webservice_zz/'>");
            sf.append("<soapenv:Body>");
            sf.append("<web:A_EPAY_TRANSFERReq>");
            sf.append("<ACC_NBR>").append(body.getString("accNbr")).append("</ACC_NBR>");
            sf.append("<BALANCE>").append(body.getString("balance")).append("</BALANCE>");
            sf.append("<CALLED_NBR>").append(body.getString("calledNbr")).append("</CALLED_NBR>");
            sf.append("<REMOTE_SOURCE>").append("80003").append("</REMOTE_SOURCE>");
            sf.append("<REMOTE_PASSWD>").append("Dq123_qr").append("</REMOTE_PASSWD>");
            sf.append("</web:A_EPAY_TRANSFERReq>").append("</soapenv:Body>").append("</soapenv:Envelope>");
            
            LOGGER.debug("余额结转-20152 查询入参={}",sf);
            // long begTime = System.currentTimeMillis();
//            String url = "http://jseop.telecomjs.com:12555/WebCumul/CumulServices?appkey=js_dzqd";
//            String url = URLPropertyUtil.getContextProperty("uqc.callSgwCommon.uql");
            String url  = pubAddressIpervice.getActionUrl("UQC_CALLSGWCOMMON_UQL","");
            LOGGER.debug("UQC_CALLSGWCOMMON_UQL ---url:{}", url);
            String rsp = HttpClientHelperForBsn.post(sf.toString(), url, HttpConstant.SOCKET_TIMEOUT, HttpConstant.UTF_ENCODING);
            // long endTime = System.currentTimeMillis();
            
            LOGGER.debug("新计费接口统一接口参数（余额结转-20152）output:", rsp);
            // LOGGER.debug("新计费接口统一接口参数（余额结转-20152） METHODS time:" + (endTime - begTime));
            //解析回参
            JSONObject jsonObj = resolveService.resolveCallNewMessageTicketQr(rsp);
            return jsonObj;
        } catch (Exception e) {
            LOGGER.debug("新计费接口统一接口参数（CallNewMessageTicketQr-电话信息清单）异常={}",e);
        }
        return null;
    }
}
