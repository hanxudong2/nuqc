package cn.js189.uqc.domain.qry;

import cn.js189.uqc.common.RequestParamBase;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 智慧BSS，生成查询客户信息请求报文基础类
 * 
 * @author Mid
 */
public abstract class QryOfferDetailBase extends RequestParamBase {
	// 日志组件
	private static final Logger LOGGER = LoggerFactory.getLogger(QryOfferDetailBase.class);

	// svcCode 提供的API接口名称
	public static final String BSS_TCP_CONT_SVC_CODE = "cpc_ppm_qryOfferDetail";

	// 地区标识
	protected String regionId;

	/**
	 * 查询销售品信息构造器
	 * 
	 * @param regionId:地区标识
	 */
	public QryOfferDetailBase(String regionId) {
		super();
		this.regionId = regionId;
	}

	@Override
	protected JSONObject generateTcpCont() {
		return super.generateTcpCont(BSS_TCP_CONT_APP_KEY, BSS_TCP_CONT_SVC_CODE, null);
	}

	@Override
	protected JSONObject generateSvcCont() {
		JSONObject authenticationInfo = generateAuthenticationInfo();
		if (null == authenticationInfo) {
			LOGGER.error("generate authenticationInfo failure");
			return null;
		}

		JSONObject requestObject = generateRequestObject();
		if (null == requestObject) {
			LOGGER.error("generate requestObject failure");
			return null;
		}

		JSONObject svcCont = new JSONObject();
		svcCont.put("authenticationInfo", authenticationInfo);
		svcCont.put("requestObject", requestObject);
		return svcCont;
	}

	/**
	 * 生成 svcCont 下的认证信息
	 * 
	 * @return
	 */
	protected JSONObject generateAuthenticationInfo() {
		return super.generateSvcContAuthenticationInfo(regionId, null, null, null);
	}

	/**
	 * 生成 svcCont 下的请求对象
	 * 
	 * @return
	 */
	protected JSONObject generateRequestObject() {
		JSONObject requestObject = new JSONObject();
		requestObject.put("scopeInfos", generateScopeInfos());
		requestObject.put("regionId", regionId);
		return improveRequestObject(requestObject);
	}

	/**
	 * 补齐 svcCont 下的请求对象
	 * 
	 * @param requestObject:svcCont下的请求对象
	 * @return
	 */
	protected abstract JSONObject improveRequestObject(JSONObject requestObject);


	/**
	 * 生成请求对象下的scope信息
	 * 
	 * @return
	 */
	private JSONArray generateScopeInfos() {

		JSONArray scopeInfoJSONArray = new JSONArray();
		//这边直接写死，因为接口入参格式跟其他的不太一样
		JSONObject offer = new JSONObject();
		offer.put("scope", "offer");
		
		JSONObject offerProdRels = new JSONObject();
		offerProdRels.put("scope", "offerProdRels");
		JSONObject params = new JSONObject();
		JSONArray scopeInfos =  new JSONArray();
		JSONObject scope1= new JSONObject();
		scope1.put("scope", "prodAttrValueRestrict");
		JSONObject scope2= new JSONObject();
        scope2.put("scope", "offerCompRelRstrCfg");
        scopeInfos.add(scope1);
        scopeInfos.add(scope2);
        params.put("scopeInfos",scopeInfos);
		offerProdRels.put("params", params);
		
		JSONObject offerObjRels = new JSONObject();
		offerObjRels.put("scope", "offerObjRels");
		
		JSONObject objCountLimitRuls = new JSONObject();
		objCountLimitRuls.put("scope", "objCountLimitRuls");
		
		scopeInfoJSONArray.add(offer);
		scopeInfoJSONArray.add(offerProdRels);
		scopeInfoJSONArray.add(offerObjRels);
		scopeInfoJSONArray.add(objCountLimitRuls);
		return scopeInfoJSONArray;
	}
}
