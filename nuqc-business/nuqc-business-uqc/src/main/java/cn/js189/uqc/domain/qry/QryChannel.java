package cn.js189.uqc.domain.qry;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang.StringUtils;

public class QryChannel extends QryChannelBase {

	private JSONArray orgIds;
	
    private String channelNbr;

    private String channelName;
    
    private String channelClass;
    
    private String statusCd;
    
    private JSONObject pageInfo;

    public QryChannel(String regionId, JSONArray orgIds, String channelNbr, String channelName, String channelClass, String statusCd, JSONObject pageInfo) {
        super(regionId);
        this.orgIds = orgIds;
        this.channelNbr = channelNbr;
        this.channelName = channelName;
        this.channelClass = channelClass;
        this.statusCd = statusCd;
        this.pageInfo = pageInfo;
    }

    @Override
    protected JSONObject improveRequestObject(JSONObject requestObject) {
    	if(!StringUtils.isEmpty(channelClass)  ){
    		requestObject.put("channelClass", channelClass);
    	}
    	if(!StringUtils.isEmpty(statusCd) ){
    		requestObject.put("statusCd", statusCd);
    	}
    	
    	if(null!= pageInfo && !pageInfo.isEmpty()){
    		requestObject.put("pageInfo", pageInfo);
    	}
    	
    	if (null != orgIds && !orgIds.isEmpty()) {
            requestObject.put("orgIds", orgIds);
            return requestObject;
        }
        if (!StringUtils.isEmpty(channelNbr)) {
            requestObject.put("channelNbr", channelNbr);
            return requestObject;
        }
        if (!StringUtils.isEmpty(channelName)) {
            requestObject.put("channelName", channelName);
            return requestObject;
        }
        return requestObject;
    }
}
