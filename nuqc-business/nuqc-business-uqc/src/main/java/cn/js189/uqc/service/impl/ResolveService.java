/**************************************************************************************** 
        湖南创发科技有限责任公司
 ****************************************************************************************/
package cn.js189.uqc.service.impl;

import cn.js189.common.enums.SystemEnum;
import cn.js189.common.util.MainUtils;
import cn.js189.common.util.helper.JSonResultHelper;
import cn.js189.uqc.util.MsgTool;
import cn.js189.uqc.util.ResultTool;
import cn.js189.uqc.util.XML2JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * <Description> <br>
 * 
 * @author Administrator<br>
 * @version 1.0<br>
 * @taskId <br>
 * @CreateDate 2017年6月7日 <br>
 */

@Service
public class ResolveService {

    /**
     * 日志组件 <br>
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(ResolveService.class);

    /**
     * Description: 欠费查询xml解析<br>
     * 
     * @author Administrator<br>
     * @taskId <br>
     * @param rsp
     */
    public JSONObject resolveOthGetOweData(String rsp) {
        try {
            JSONObject toservice = new JSONObject();// 返回给业务层
            JSONObject root = XML2JSON.xmlToJson(rsp);
            JSONObject jsonObject = root.getJSONObject("Body").getJSONObject("OTH_GET_OWERsp");
            if (jsonObject != null && (!jsonObject.isEmpty())) {
                String isresult = jsonObject.getString("IRESULT");
                String smsg = jsonObject.getString("SMSG");
                toservice.put(SystemEnum.TSR_RESULT.name(), isresult);
                toservice.put(SystemEnum.TSR_CODE.name(), ResultTool.transfarBsnResult(isresult));
                toservice.put(SystemEnum.TSR_MSG.name(), MsgTool.getMsg(isresult));
                toservice.put(SystemEnum.TUXEDO_MSG.name(), MainUtils.getNullEmpty(smsg));

                toservice.put("blance", jsonObject.getString("BALANCE"));// 使用时长合计
                toservice.put("preFlag", jsonObject.getString("PRE_FLAG"));// 话费合计
                toservice.put("realBlance", jsonObject.getString("REAL_BALANCE")); // 流量合计

                JSONArray blances = new JSONArray();
                String json = jsonObject.getString("OTHGETOWElist");
                if (StringUtils.isNotBlank(json)) {
                    if (json.startsWith("[")) {
                    	JSONArray array = jsonObject.getJSONArray("OTHGETOWElist");
                        for (int i = 0; i < array.size(); i++) {
                            JSONObject obj = new JSONObject();
                            JSONObject jsonBlance = (JSONObject) array.get(i);
                            obj.put("cycleBeginDate",
                                    MainUtils.getNullEmpty(jsonBlance.getString("CYCLE_BEGIN_DATE")));
                            obj.put("cycleEndDate",
                                    MainUtils.getNullEmpty(jsonBlance.getString("CYCLE_END_DATE")));//
                            obj.put("acctItemCharge",
                                    MainUtils.getNullEmpty(jsonBlance.getString("ACCT_ITEM_CHARGE")));//
                            obj.put("due",
                                    MainUtils.getNullEmpty(jsonBlance.getString("DUE"))); //
                            obj.put("billed",
                                    MainUtils.getNullEmpty(jsonBlance.getString("BILLED"))); //
                            blances.add(obj);
                        }
                    } else {
                        JSONObject jsonObj = jsonObject.getJSONObject("OTHGETOWElist");
                        JSONObject obj = new JSONObject();
                        obj.put("cycleBeginDate",
                                MainUtils.getNullEmpty(jsonObj.getString("CYCLE_BEGIN_DATE")));
                        obj.put("cycleEndDate",
                                MainUtils.getNullEmpty(jsonObj.getString("CYCLE_END_DATE")));//
                        obj.put("acctItemCharge",
                                MainUtils.getNullEmpty(jsonObj.getString("ACCT_ITEM_CHARGE")));//
                        obj.put("due",
                                MainUtils.getNullEmpty(jsonObj.getString("DUE"))); //
                        obj.put("billed",
                                MainUtils.getNullEmpty(jsonObj.getString("BILLED"))); //
                        blances.add(obj);
                    }
                }
                toservice.put("items", blances);
                return toservice;

            } else {
                return JSonResultHelper.exportJSONObjectReturnResultBlank();
            }
        } catch (Exception e) {
            LOGGER.debug("出参实际处理类出错啦!{}", e.getMessage());
            return JSonResultHelper.exportJSONObjectOtherError();
        }
    }

    /**
     * Description: 账单结果返回xml解析<br>
     * 
     * @author Administrator<br>
     * @taskId <br>
     * @param rsp
     */
    public JSONObject resolveBillReqData(String rsp) {
        try {
            JSONObject toservice = new JSONObject();// 返回给业务层
            JSONObject root = XML2JSON.xmlToJson(rsp);
            JSONObject jsonObject = root.getJSONObject("Body").getJSONObject("NEW_BILL_Rsp");
            if (jsonObject != null && (!jsonObject.isEmpty())) {
                String isresult = jsonObject.getString("IRESULT");
                String smsg = jsonObject.getString("SMSG");
                toservice.put(SystemEnum.TSR_RESULT.name(), isresult);
                toservice.put(SystemEnum.TSR_CODE.name(), ResultTool.transfarBsnResult(isresult));
                toservice.put(SystemEnum.TSR_MSG.name(), MsgTool.getMsg(isresult));
                toservice.put(SystemEnum.TUXEDO_MSG.name(), MainUtils.getNullEmpty(smsg));

                int startIndex = rsp.indexOf("<CustBillQuery");
                int endIndex = rsp.indexOf("</XML_OUT>");
                String result = rsp.substring(startIndex, endIndex);
                result = result.trim();
                result = result.replace("\t", "").replace("\n", "");
                StringBuilder buffer = new StringBuilder();
                buffer.append("<?xml version='1.0' encoding='GBK'?>");
                buffer.append(result);
                toservice.put("custBillQuery", buffer.toString());
                return toservice;
            } else {
                return JSonResultHelper.exportJSONObjectReturnResultBlank();
            }
        } catch (Exception e) {
            LOGGER.debug("出参实际处理类出错啦!{}", e.getMessage());
            return JSonResultHelper.exportJSONObjectOtherError();
        }
    }

    /**
     * Description: 余额查询返回报文解析<br>
     * 
     * @author Administrator<br>
     * @taskId <br>
     * @param rsp
     */
    public JSONObject resolveBalanceQrReqData(String rsp) {
        try {
            JSONObject toservice = new JSONObject();// 返回给业务层
            JSONObject root = XML2JSON.xmlToJson(rsp);
            JSONObject jsonObject = root.getJSONObject("Body").getJSONObject("BALANCE_QRRsp");
            if (jsonObject != null && (!jsonObject.isEmpty())) {
                String isresult = jsonObject.getString("IRESULT");
                String smsg = jsonObject.getString("SMSG");
                String groupBalance = jsonObject.getString("GROUP_BALANCE");
                String returnedBlance = jsonObject.getString("RETURNED_BALANCE");
                toservice.put(SystemEnum.TSR_RESULT.name(), isresult);
                toservice.put(SystemEnum.TSR_CODE.name(), ResultTool.transfarBsnResult(isresult));
                toservice.put(SystemEnum.TSR_MSG.name(), MsgTool.getMsg(isresult));
                toservice.put(SystemEnum.TUXEDO_MSG.name(), MainUtils.getNullEmpty(smsg));
                toservice.put("groupBlance", groupBalance);
                toservice.put("returnedBlance", returnedBlance);
                JSONArray blances = new JSONArray();
                 JSONArray array = jsonObject.getJSONArray("BALANCElist");
                for (int i = 0; i < array.size(); i++) {
                    JSONObject blanceJson = new JSONObject();
                    JSONObject jsonBlance = (JSONObject) array.get(i);
                    String blance = jsonBlance.getString("BALANCE");
                    String expdate = "{}".equals(jsonBlance.getString("EXP_DATE")) ? "" : jsonBlance.getString("EXP_DATE");
                    blanceJson.put("blance", blance);
                    blanceJson.put("expDate", expdate);
                    blances.add(blanceJson);
                }
                toservice.put("blances", blances);
                return toservice;
            } else {
                return JSonResultHelper.exportJSONObjectReturnResultBlank();
            }

        } catch (Exception e) {
            LOGGER.debug("出参实际处理类出错啦!{}", e.getMessage());
            return JSonResultHelper.exportJSONObjectOtherError();
        }
    }

    /**
     * Description: 实时查询返回报文解析<br>
     * 
     * @author Administrator<br>
     * @taskId <br>
     * @param rsp
     */
    public JSONObject resolveAcctItemfqReq(String rsp) {
        try {
            // 开始解析回参
            JSONObject roots = XML2JSON.xmlToJson(rsp);
            JSONObject jsonObject = roots.getJSONObject("Body").getJSONObject("ACCT_ITEM_F_QRsp");
            JSONObject root = new JSONObject();
            root.put(SystemEnum.TSR_RESULT.name(), jsonObject.getString("IRESULT"));
            root.put(SystemEnum.TSR_CODE.name(), ResultTool.transfarBsnResult(jsonObject.getString("IRESULT")));
            root.put(SystemEnum.TSR_MSG.name(), MsgTool.getMsg(jsonObject.getString("IRESULT")));
            root.put(SystemEnum.TUXEDO_MSG.name(), MainUtils.getNullEmpty(jsonObject.getString("SMSG")));
            if ("0".equals(jsonObject.getString("IRESULT"))) {
                root.put("chargeCnt", MainUtils.getNullEmpty(jsonObject.getString("CHARGE_CNT")));// 费用总和
                String json = jsonObject.getString("ACCT_ITEM_F_QRlist");
                JSONArray items = new JSONArray();
                if (json.startsWith("[")) {
                    JSONArray itemsThis = jsonObject.getJSONArray("ACCT_ITEM_F_QRlist");

                    for (int i = 0; i < itemsThis.size(); i++) {
                        if (!itemsThis.isEmpty()) {
                            JSONObject obj = new JSONObject();
                            if (!itemsThis.getJSONObject(i).isEmpty()) {
                                obj.put("accNbr", MainUtils.getNullEmpty(itemsThis.getJSONObject(i).getString("ACC_NBR")));
                                obj.put("areaCode", ("{}".equals(itemsThis.getJSONObject(i).getString("AREA_CODE")) ? "" : itemsThis.getJSONObject(i)
                                        .getString("AREA_CODE")));// 电话号码
                                obj.put("cycleEndDate", MainUtils.getNullEmpty(itemsThis.getJSONObject(i).getString("CYCLE_END_DATE")));// 帐务周期结束日期
                                obj.put("sumaryItemId", MainUtils.getNullEmpty(itemsThis.getJSONObject(i).getString("SUMMARY_ITEM_ID")));// 帐目归并项标识
                                obj.put("groupName", MainUtils.getNullEmpty(itemsThis.getJSONObject(i).getString("GROUP_NAME"))); // 帐目归并项名称
                                obj.put("acctItemTypeId", MainUtils.getNullEmpty(itemsThis.getJSONObject(i).getString("ACCT_ITEM_TYPE_ID")));// 帐目类型标识
                                obj.put("acctItemTypeName", MainUtils.getNullEmpty(itemsThis.getJSONObject(i).getString("ACCT_ITEM_TYPE_NAME")));// 帐目类型名称
                                obj.put("acctItemCharge", MainUtils.getNullEmpty(itemsThis.getJSONObject(i).getString("ACCT_ITEM_CHARGE"))); // 帐目金额
                            }
                            items.add(obj);
                        }
                    }
                } else {
                    JSONObject jsonObj = jsonObject.getJSONObject("ACCT_ITEM_F_QRlist");
                    JSONObject obj = new JSONObject();
                    obj.put("accNbr", MainUtils.getNullEmpty(jsonObj.getString("ACC_NBR")));
                    obj.put("areaCode", ("{}".equals(jsonObj.getString("AREA_CODE")) ? "" : jsonObj.getString("AREA_CODE")));// 电话号码
                    obj.put("cycleEndDate", MainUtils.getNullEmpty(jsonObj.getString("CYCLE_END_DATE")));// 帐务周期结束日期
                    obj.put("sumaryItemId", MainUtils.getNullEmpty(jsonObj.getString("SUMMARY_ITEM_ID")));// 帐目归并项标识
                    obj.put("groupName", MainUtils.getNullEmpty(jsonObj.getString("GROUP_NAME"))); // 帐目归并项名称
                    obj.put("acctItemTypeId", MainUtils.getNullEmpty(jsonObj.getString("ACCT_ITEM_TYPE_ID")));// 帐目类型标识
                    obj.put("acctItemTypeName", MainUtils.getNullEmpty(jsonObj.getString("ACCT_ITEM_TYPE_NAME")));// 帐目类型名称
                    obj.put("acctItemCharge", MainUtils.getNullEmpty(jsonObj.getString("ACCT_ITEM_CHARGE"))); // 帐目金额
                    items.add(obj);
                }
                root.put("items", items);
            }
            return root;

        } catch (Exception e) {
            return JSonResultHelper.exportJSONObjectOtherError();
        }
    }

    /**
     * Description:短信详单查询 <br>
     * 
     * @author Administrator<br>
     * @taskId <br>
     * @param rsp
     * @return <br>
     */
    public JSONObject resolveCallNewSmTicket(String rsp) {
        try {
            JSONObject toservice = new JSONObject();// 返回给业务层
            JSONObject root = XML2JSON.xmlToJson(rsp);
            JSONObject jsonObject = root.getJSONObject("Body").getJSONObject("SM_TICKET_GURsp");
            if (jsonObject != null && (!jsonObject.isEmpty())) {
                String isresult = jsonObject.getString("IRESULT");
                String smsg = jsonObject.getString("SMSG");
                toservice.put(SystemEnum.TSR_RESULT.name(), isresult);
                toservice.put(SystemEnum.TSR_CODE.name(), ResultTool.transfarBsnResult(isresult));
                toservice.put(SystemEnum.TSR_MSG.name(), MsgTool.getMsg(isresult));
                toservice.put(SystemEnum.TUXEDO_MSG.name(), MainUtils.getNullEmpty(smsg));

                toservice.put("changeCntch", MainUtils.getNullEmpty(jsonObject.getString("CHARGE_CNT_CH")));// 话费合计

                JSONArray blances = new JSONArray();
                JSONArray array = new JSONArray();
                String json = jsonObject.getString("SM_TICKET_QRlist");
                if (json.startsWith("[")) {
                    array = jsonObject.getJSONArray("SM_TICKET_QRlist");
                    toservice.put("count", String.valueOf(array.size()));
                    for (int i = 0; i < array.size(); i++) {
                        JSONObject obj = new JSONObject();
                        JSONObject jsonBlance = (JSONObject) array.get(i);
                        obj.put("ticketNumber", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_NUMBER")));// 序号
                        obj.put("ticketType", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_TYPE")));// 呼叫类型
                        obj.put("nbr", MainUtils.getNullEmpty(jsonBlance.getString("ACC_NBR")));// 呼叫号码
                        obj.put("startDateNew", MainUtils.getNullEmpty(jsonBlance.getString("START_DATE_NEW")));// 呼叫日期
                        obj.put("startTimeNew", MainUtils.getNullEmpty(jsonBlance.getString("START_TIME_NEW")));// 呼叫时间
                        obj.put("ticketChargeCh", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_CHARGE_CH")));// 话费
                        obj.put("productName", MainUtils.getNullEmpty(jsonBlance.getString("PRODUCT_NAME")));// 产品名称
                        blances.add(obj);
                    }
                } else {
                    JSONObject jsonBlance = jsonObject.getJSONObject("SM_TICKET_QRlist");
                    JSONObject obj = new JSONObject();
                    obj.put("ticketNumber", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_NUMBER")));// 序号
                    obj.put("ticketType", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_TYPE")));// 呼叫类型
                    obj.put("nbr", MainUtils.getNullEmpty(jsonBlance.getString("ACC_NBR")));// 呼叫号码
                    obj.put("startDateNew", MainUtils.getNullEmpty(jsonBlance.getString("START_DATE_NEW")));// 呼叫日期
                    obj.put("startTimeNew", MainUtils.getNullEmpty(jsonBlance.getString("START_TIME_NEW")));// 呼叫时间
                    obj.put("ticketChargeCh", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_CHARGE_CH")));// 话费
                    obj.put("productName", MainUtils.getNullEmpty(jsonBlance.getString("PRODUCT_NAME")));// 产品名称
                    blances.add(obj);
                }

                toservice.put("items", blances);
                return toservice;
            } else {
                return JSonResultHelper.exportJSONObjectReturnResultBlank();
            }
        } catch (Exception e) {
            LOGGER.debug("出参实际处理类出错啦!{}", e.getMessage());
            return JSonResultHelper.exportJSONObjectOtherError();
        }
    }

    /**
     * Description: 手机语音清单回参解析<br>
     * 
     * @author Administrator<br>
     * @taskId <br>
     * @param rsp
     * @return <br>
     */
    public JSONObject resolveCallNewVoiceTicket(String rsp) {
        try {
            JSONObject toservice = new JSONObject();// 返回给业务层
            JSONObject root = XML2JSON.xmlToJson(rsp);
            JSONObject jsonObject = root.getJSONObject("Body").getJSONObject("VOICE_TICKETGURsp");
            if (jsonObject != null && (!jsonObject.isEmpty())) {
                String isresult = jsonObject.getString("IRESULT");
                String smsg = jsonObject.getString("SMSG");
                String changeCntch = jsonObject.getString("CHARGE_CNT_CH");// 话费合计
                String duartionCntCh = jsonObject.getString("DURATION_CNT_CH");// 通话时长合计
                toservice.put(SystemEnum.TSR_RESULT.name(), isresult);
                toservice.put(SystemEnum.TSR_CODE.name(), ResultTool.transfarBsnResult(isresult));
                toservice.put(SystemEnum.TSR_MSG.name(), MsgTool.getMsg(isresult));
                toservice.put(SystemEnum.TUXEDO_MSG.name(), MainUtils.getNullEmpty(smsg));
                toservice.put("duartionCntCh", duartionCntCh);
                toservice.put("changeCntch", changeCntch);
                JSONArray blances = new JSONArray();
                JSONArray array = new JSONArray();
                String json = jsonObject.getString("VOICE_TICKETGUlist");
                if (json.startsWith("[")) {
                    array = jsonObject.getJSONArray("VOICE_TICKETGUlist");
                    for (int i = 0; i < array.size(); i++) {
                        JSONObject obj = new JSONObject();
                        JSONObject jsonBlance = (JSONObject) array.get(i);
                        obj.put("ticketNumber", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_NUMBER")));// 序号
                        obj.put("ticketType", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_TYPE")));// 呼叫类型
                        obj.put("nbr", MainUtils.getNullEmpty(jsonBlance.getString("ACC_NBR")));// 呼叫号码
                        obj.put("startDateNew", MainUtils.getNullEmpty(jsonBlance.getString("START_DATE_NEW")));//
                        obj.put("startTimeNew", MainUtils.getNullEmpty(jsonBlance.getString("START_TIME_NEW")));//
                        obj.put("duartionCh", MainUtils.getNullEmpty(jsonBlance.getString("DURATION_CH")));// 通话时长
                        obj.put("ticketChargeCh", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_CHARGE_CH")));// 话费
                        obj.put("areaCode", MainUtils.getNullEmpty(jsonBlance.getString("AREA_CODE")));
                        obj.put("productName", MainUtils.getNullEmpty(jsonBlance.getString("PRODUCT_NAME")));
                        blances.add(obj);
                    }
                } else {
                    JSONObject jsonBlance = jsonObject.getJSONObject("VOICE_TICKETGUlist");
                    JSONObject obj = new JSONObject();
                    obj.put("ticketNumber", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_NUMBER")));// 序号
                    obj.put("ticketType", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_TYPE")));// 呼叫类型
                    obj.put("nbr", MainUtils.getNullEmpty(jsonBlance.getString("ACC_NBR")));// 呼叫号码
                    obj.put("startDateNew", MainUtils.getNullEmpty(jsonBlance.getString("START_DATE_NEW")));//
                    obj.put("startTimeNew", MainUtils.getNullEmpty(jsonBlance.getString("START_TIME_NEW")));//
                    obj.put("duartionCh", MainUtils.getNullEmpty(jsonBlance.getString("DURATION_CH")));// 通话时长
                    obj.put("ticketChargeCh", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_CHARGE_CH")));// 话费
                    obj.put("areaCode", MainUtils.getNullEmpty(jsonBlance.getString("AREA_CODE")));
                    obj.put("productName", MainUtils.getNullEmpty(jsonBlance.getString("PRODUCT_NAME")));
                    blances.add(obj);
                }

                toservice.put("items", blances);
                return toservice;
            } else {
                return JSonResultHelper.exportJSONObjectReturnResultBlank();
            }
        } catch (Exception e) {
            LOGGER.debug("出参实际处理类出错啦!{}", e.getMessage());
            return JSonResultHelper.exportJSONObjectOtherError();
        }
    }

    /**
     * Description: 移动用户商务及数据通信详单回参解析<br>
     * 
     * @author Administrator<br>
     * @taskId <br>
     * @param rsp
     * @return <br>
     */
    public JSONObject resolveCallNewDataTicketQrReq(String rsp) {
        try {
            JSONObject toservice = new JSONObject();// 返回给业务层
            JSONObject root = XML2JSON.xmlToJson(rsp);
            JSONObject jsonObject = root.getJSONObject("Body").getJSONObject("NEW_DATA_TICKET_QRsp");
            if (jsonObject != null && (!jsonObject.isEmpty())) {
                String isresult = jsonObject.getString("IRESULT");
                String smsg = jsonObject.getString("SMSG");
                toservice.put(SystemEnum.TSR_RESULT.name(), isresult);
                toservice.put(SystemEnum.TSR_CODE.name(), ResultTool.transfarBsnResult(isresult));
                toservice.put(SystemEnum.TSR_MSG.name(), MsgTool.getMsg(isresult));
                toservice.put(SystemEnum.TUXEDO_MSG.name(), MainUtils.getNullEmpty(smsg));

                toservice.put("duartionCntCh", jsonObject.getString("DURATION_CNT_CH"));// 使用时长合计
                toservice.put("changeCntch", jsonObject.getString("CHARGE_CNT_CH"));// 话费合计
                toservice.put("totalBytesCnt", jsonObject.getString("TOTAL_BYTES_CNT")); // 流量合计

                JSONArray blances = new JSONArray();
                JSONArray array = new JSONArray();
                String json = jsonObject.getString("NEW_DATA_TICKET_QRlist");
                if (json.startsWith("[")) {
                    array = jsonObject.getJSONArray("NEW_DATA_TICKET_QRlist");
                    toservice.put("count", String.valueOf(array.size()));
                    for (int i = 0; i < array.size(); i++) {
                        JSONObject obj = new JSONObject();
                        JSONObject jsonBlance = (JSONObject) array.get(i);
                        // 0 TICKET_NUMBER 序号
                        obj.put("TICKET_NUMBER", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_NUMBER")));
                        // 1 START_TIME 上线时间 格式为YYYY-MM-DD HH24:MI
                        obj.put("START_TIME", MainUtils.getNullEmpty(jsonBlance.getString("START_TIME")));
                        // 2 DURATION_CH 使用时长 格式为HH24小时MI分SS秒
                        obj.put("DURATION_CH", MainUtils.getNullEmpty(jsonBlance.getString("DURATION_CH")));
                        // 3 BYTES_CNT 流量 格式为*GB*MB*KB
                        obj.put("BYTES_CNT", MainUtils.getNullEmpty(jsonBlance.getString("BYTES_CNT")));
                        // 4 TICKET_CHARGE_CH 话费 格式为0.00元
                        obj.put("TICKET_CHARGE_CH", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_CHARGE_CH")));
                        // 5 TICKET_TYPE 通信地点 形如：南京等
                        obj.put("TICKET_TYPE", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_TYPE")));
                        // 6 SERVICE_TYPE 承载网络 3G或者1X
                        obj.put("SERVICE_TYPE", MainUtils.getNullEmpty(jsonBlance.getString("SERVICE_TYPE")));
                        // 7 CCG_PRODUCT_NAME CCG业务类型名称 CCG_PRODUCT_DESC
                        obj.put("CCG_PRODUCT_NAME",
                                ("{}".equals(jsonBlance.getString("CCG_PRODUCT_NAME")) ? "" : jsonBlance.getString("CCG_PRODUCT_NAME")));
                        blances.add(obj);
                    }
                } else {
                    JSONObject jsonObj = jsonObject.getJSONObject("NEW_DATA_TICKET_QRlist");
                    JSONObject obj = new JSONObject();
                    toservice.put("count", "1");
                    // 0 TICKET_NUMBER 序号
                    obj.put("TICKET_NUMBER", MainUtils.getNullEmpty(jsonObj.getString("TICKET_NUMBER")));
                    // 1 START_TIME 上线时间 格式为YYYY-MM-DD HH24:MI
                    obj.put("START_TIME", MainUtils.getNullEmpty(jsonObj.getString("START_TIME")));
                    // 2 DURATION_CH 使用时长 格式为HH24小时MI分SS秒
                    obj.put("DURATION_CH", MainUtils.getNullEmpty(jsonObj.getString("DURATION_CH")));
                    // 3 BYTES_CNT 流量 格式为*GB*MB*KB
                    obj.put("BYTES_CNT", MainUtils.getNullEmpty(jsonObj.getString("BYTES_CNT")));
                    // 4 TICKET_CHARGE_CH 话费 格式为0.00元
                    obj.put("TICKET_CHARGE_CH", MainUtils.getNullEmpty(jsonObj.getString("TICKET_CHARGE_CH")));
                    // 5 TICKET_TYPE 通信地点 形如：南京等
                    obj.put("TICKET_TYPE", MainUtils.getNullEmpty(jsonObj.getString("TICKET_TYPE")));
                    // 6 SERVICE_TYPE 承载网络 3G或者1X
                    obj.put("SERVICE_TYPE", MainUtils.getNullEmpty(jsonObj.getString("SERVICE_TYPE")));
                    // 7 CCG_PRODUCT_NAME CCG业务类型名称 CCG_PRODUCT_DESC
                    obj.put("CCG_PRODUCT_NAME", ("{}".equals(jsonObj.getString("CCG_PRODUCT_NAME")) ? "" : jsonObj.getString("CCG_PRODUCT_NAME")));
                    blances.add(obj);
                }

                toservice.put("items", blances);
                return toservice;
            } else {
                return JSonResultHelper.exportJSONObjectReturnResultBlank();
            }
        } catch (Exception e) {
            LOGGER.debug("出参实际处理类出错啦!{}", e.getMessage());
            return JSonResultHelper.exportJSONObjectOtherError();
        }
    }

    /**
     * Description:其他增值业务详单回参解析 <br>
     * 
     * @author Administrator<br>
     * @taskId <br>
     * @param rsp
     * @return <br>
     */
    public JSONObject resolveCallNewValueTicketQr(String rsp) {
        try {
            JSONObject toservice = new JSONObject();// 返回给业务层
            JSONObject root = XML2JSON.xmlToJson(rsp);
            JSONObject jsonObject = root.getJSONObject("Body").getJSONObject("VALUE_TICKETGURsp");
            if (jsonObject != null && (!jsonObject.isEmpty())) {
                String isresult = jsonObject.getString("IRESULT");
                String smsg = jsonObject.getString("SMSG");
                toservice.put(SystemEnum.TSR_RESULT.name(), isresult);
                toservice.put(SystemEnum.TSR_CODE.name(), ResultTool.transfarBsnResult(isresult));
                toservice.put(SystemEnum.TSR_MSG.name(), MsgTool.getMsg(isresult));
                toservice.put(SystemEnum.TUXEDO_MSG.name(), MainUtils.getNullEmpty(smsg));

                toservice.put("totalTicketChargeCh", jsonObject.getString("TOTAL_TICKET_CHARGE_CH"));// 金额合计

                JSONArray blances = new JSONArray();
                String json = jsonObject.getString("VALUE_TICKETGUlist");
                if (json.startsWith("[")) {
                	JSONArray array = jsonObject.getJSONArray("VALUE_TICKETGUlist");
                    toservice.put("count", String.valueOf(array.size()));
                    for (int i = 0; i < array.size(); i++) {
                        JSONObject obj = new JSONObject();
                        JSONObject jsonBlance = (JSONObject) array.get(i);
                        obj.put("ticketNumber", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_NUMBER")));//
                        obj.put("ticketType", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_TYPE")));//
                        obj.put("nbr", MainUtils.getNullEmpty(jsonBlance.getString("ACC_NBR")));//
                        obj.put("startDateNew", MainUtils.getNullEmpty(jsonBlance.getString("START_DATE_NEW")));//
                        obj.put("startTimeNew", MainUtils.getNullEmpty(jsonBlance.getString("START_TIME_NEW")));//
                        obj.put("agentCode", MainUtils.getNullEmpty(jsonBlance.getString("AGENT_CODE")));//
                        obj.put("ticketChargeCh", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_CHARGE_CH")));
                        obj.put("spName", MainUtils.getNullEmpty(jsonBlance.getString("SP_NAME")));
                        obj.put("connectName", MainUtils.getNullEmpty(jsonBlance.getString("CONNECT_NAME")));
                        obj.put("productName", MainUtils.getNullEmpty(jsonBlance.getString("PRODUCT_NAME")));
                        blances.add(obj);
                    }
                } else {
                    JSONObject jsonBlance = jsonObject.getJSONObject("BALANCE_DETAIList");
                    JSONObject obj = new JSONObject();
                    obj.put("ticketNumber", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_NUMBER")));//
                    obj.put("ticketType", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_TYPE")));//
                    obj.put("nbr", MainUtils.getNullEmpty(jsonBlance.getString("ACC_NBR")));//
                    obj.put("startDateNew", MainUtils.getNullEmpty(jsonBlance.getString("START_DATE_NEW")));//
                    obj.put("startTimeNew", MainUtils.getNullEmpty(jsonBlance.getString("START_TIME_NEW")));//
                    obj.put("agentCode", MainUtils.getNullEmpty(jsonBlance.getString("AGENT_CODE")));//
                    obj.put("ticketChargeCh", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_CHARGE_CH")));
                    obj.put("spName", MainUtils.getNullEmpty(jsonBlance.getString("SP_NAME")));
                    obj.put("connectName", MainUtils.getNullEmpty(jsonBlance.getString("CONNECT_NAME")));
                    obj.put("productName", MainUtils.getNullEmpty(jsonBlance.getString("PRODUCT_NAME")));
                    blances.add(obj);
                }

                toservice.put("items", blances);
                return toservice;
            } else {
                return JSonResultHelper.exportJSONObjectReturnResultBlank();
            }
        } catch (Exception e) {
            LOGGER.debug("出参实际处理类出错啦!{}", e.getMessage());
            return JSonResultHelper.exportJSONObjectOtherError();
        }
    }

    /**
     * Description: wlan一次一密 上网查询返回报文解析<br>
     * 
     * @author Administrator<br>
     * @taskId <br>
     * @param rsp
     * @return <br>
     */
    public JSONObject resolveCallNewWifiTicketQr(String rsp) {
        try {
            JSONObject toservice = new JSONObject();// 返回给业务层
            JSONObject root = XML2JSON.xmlToJson(rsp);
            JSONObject jsonObject = root.getJSONObject("Body").getJSONObject("WIFI_TICKETRsp");
            if (jsonObject != null && (!jsonObject.isEmpty())) {
                String isresult = jsonObject.getString("IRESULT");
                String smsg = jsonObject.getString("SMSG");
                toservice.put("isresult", isresult);
                toservice.put("smsg", smsg);
                if (isresult.equals("0")) {
                    toservice.put("chargeChtCh",
                            MainUtils.getNullEmpty(jsonObject.getString("CHARGE_CNT_CH")));
                    toservice.put("durationCntCh",
                            MainUtils.getNullEmpty(jsonObject.getString("DURATION_CNT_CH")));
                    toservice.put("totalBytesCnt",
                            MainUtils.getNullEmpty(jsonObject.getString("TOTAL_BYTES_CNT")));

                    JSONArray blances = new JSONArray();
                    String json = jsonObject.getString("WIFI_TICKETlist");
                    if (json.startsWith("[")) {
                    	JSONArray array = jsonObject.getJSONArray("WIFI_TICKETlist");
                        toservice.put("count", String.valueOf(array.size()));
                        for (int i = 0; i < array.size(); i++) {
                            JSONObject obj = new JSONObject();
                            JSONObject jsonBlance = (JSONObject) array.get(i);
                            obj.put("ticketNumber", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_NUMBER")));// 序号
                            obj.put("payMentMethodName", MainUtils.getNullEmpty(jsonBlance.getString("PAYMENT_METHOD_NAME")));
                            obj.put("ticketType", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_TYPE")));
                            obj.put("startDateNew", MainUtils.getNullEmpty(jsonBlance.getString("START_DATE_NEW")));// 发送日期
                            obj.put("startTimeNew", MainUtils.getNullEmpty(jsonBlance.getString("START_TIME_NEW")));// 发送时间
                            obj.put("duartionCh", MainUtils.getNullEmpty(jsonBlance.getString("DURATION_CH"))); // 使用时长
                            obj.put("ticketChargeCh", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_CHARGE_CH")));// 费用
                            obj.put("bytesCnt", MainUtils.getNullEmpty(jsonBlance.getString("BYTES_CNT"))); // 使用流量
                            obj.put("productName", MainUtils.getNullEmpty(jsonBlance.getString("PRODUCT_NAME")));// 产品名称
                            blances.add(obj);
                        }
                    } else {
                        toservice.put("count", "1");
                        JSONObject jsonBlance = jsonObject.getJSONObject("WIFI_TICKETlist");
                        JSONObject obj = new JSONObject();
                        obj.put("ticketNumber", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_NUMBER")));// 序号
                        obj.put("payMentMethodName", MainUtils.getNullEmpty(jsonBlance.getString("PAYMENT_METHOD_NAME")));
                        obj.put("ticketType", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_TYPE")));
                        obj.put("startDateNew", MainUtils.getNullEmpty(jsonBlance.getString("START_DATE_NEW")));// 发送日期
                        obj.put("startTimeNew", MainUtils.getNullEmpty(jsonBlance.getString("START_TIME_NEW")));// 发送时间
                        obj.put("duartionCh", MainUtils.getNullEmpty(jsonBlance.getString("DURATION_CH"))); // 使用时长
                        obj.put("ticketChargeCh", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_CHARGE_CH")));// 费用
                        obj.put("bytesCnt", MainUtils.getNullEmpty(jsonBlance.getString("BYTES_CNT"))); // 使用流量
                        obj.put("productName", MainUtils.getNullEmpty(jsonBlance.getString("PRODUCT_NAME")));// 产品名称
                        blances.add(obj);
                    }
                    toservice.put("items", blances);
                }
                return toservice;
            } else {
                return JSonResultHelper.exportJSONObjectReturnResultBlank();
            }
        } catch (Exception e) {
            LOGGER.debug("出参实际处理类出错啦!{}", e.getMessage());
            return JSonResultHelper.exportJSONObjectOtherError();
        }
    }

    /**
     * Description: C网短信/彩信增值清单<br>
     * 
     * @author Administrator<br>
     * @taskId <br>
     * @param rsp
     * @return <br>
     */
    public JSONObject resolveCallNewSmsValueTicketQr(String rsp) {
        try {
            JSONObject toservice = new JSONObject();// 返回给业务层
            JSONObject root = XML2JSON.xmlToJson(rsp);
            JSONObject jsonObject = root.getJSONObject("Body").getJSONObject("SMS_VALUE_TICKET_QRsp");
            if (jsonObject != null && (!jsonObject.isEmpty())) {
                String isresult = jsonObject.getString("IRESULT");
                String smsg = jsonObject.getString("SMSG");
                if (isresult.equals("0")) {
                    toservice.put("totalTicketChargeCh", MainUtils.getNullEmpty(jsonObject.getString("TOTAL_TICKET_CHARGE_CH")));// 通信费用总计
                    JSONArray blances = new JSONArray();
                    String json = jsonObject.getString("SMS_VALUE_TICKETlist");
                    if (json.startsWith("[")) {
                    	JSONArray array = jsonObject.getJSONArray("SMS_VALUE_TICKETlist");
                        toservice.put("count", String.valueOf(array.size()));
                        for (int i = 0; i < array.size(); i++) {
                            JSONObject obj = new JSONObject();
                            JSONObject jsonBlance = (JSONObject) array.get(i);
                            obj.put("ticketNumber", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_NUMBER")));// 序号
                            obj.put("ticketType", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_TYPE")));// 使用方式
                            obj.put("nbr", MainUtils.getNullEmpty(jsonBlance.getString("ACC_NBR")));// 对方号码
                            obj.put("startDateNew", MainUtils.getNullEmpty(jsonBlance.getString("START_DATE_NEW")));// 发送日期
                            obj.put("startTimeNew", MainUtils.getNullEmpty(jsonBlance.getString("START_TIME_NEW")));// 发送时间
                            obj.put("agentCode", MainUtils.getNullEmpty(jsonBlance.getString("AGENT_CODE")));// 发送时间
                            obj.put("ticketChargeCh", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_CHARGE_CH")));// 话费
                            obj.put("spName", MainUtils.getNullEmpty(jsonBlance.getString("SP_NAME")));// 流量
                            obj.put("connectName", MainUtils.getNullEmpty(jsonBlance.getString("CONNECT_NAME")));
                            blances.add(obj);
                        }
                    } else {
                        toservice.put("count", "1");
                        JSONObject jsonBlance = jsonObject.getJSONObject("SMS_VALUE_TICKETlist");
                        JSONObject obj = new JSONObject();
                        obj.put("ticketNumber", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_NUMBER")));// 序号
                        obj.put("ticketType", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_TYPE")));// 使用方式
                        obj.put("nbr", MainUtils.getNullEmpty(jsonBlance.getString("ACC_NBR")));// 对方号码
                        obj.put("startDateNew", MainUtils.getNullEmpty(jsonBlance.getString("START_DATE_NEW")));// 发送日期
                        obj.put("startTimeNew", MainUtils.getNullEmpty(jsonBlance.getString("START_TIME_NEW")));// 发送时间
                        obj.put("agentCode", MainUtils.getNullEmpty(jsonBlance.getString("AGENT_CODE")));// 发送时间
                        obj.put("ticketChargeCh", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_CHARGE_CH")));// 话费
                        obj.put("spName", MainUtils.getNullEmpty(jsonBlance.getString("SP_NAME")));// 流量
                        obj.put("connectName", MainUtils.getNullEmpty(jsonBlance.getString("CONNECT_NAME")));
                        blances.add(obj);
                    }
                    toservice.put("items", blances);
                }
                toservice.put("isresult", isresult);
                toservice.put("smsg", smsg);
                return toservice;
            } else {
                return JSonResultHelper.exportJSONObjectReturnResultBlank();
            }
        } catch (Exception e) {
            LOGGER.debug("C网短信/彩信增值清单解析回参出现异常!", e);
            return JSonResultHelper.exportJSONObjectOtherError();
        }
    }

    /**
     * Description:窄带清单查询出参解析 <br>
     * 
     * @author Administrator<br>
     * @taskId <br>
     * @param rsp
     * @return <br>
     */
    public JSONObject resolveCallNewNarrowDataQr(String rsp) {
        try {
            JSONObject toservice = new JSONObject();// 返回给业务层
            JSONObject root = XML2JSON.xmlToJson(rsp);
            JSONObject jsonObject = root.getJSONObject("Body").getJSONObject("NARROW_DATA_GURsp");
            if (jsonObject != null && (!jsonObject.isEmpty())) {
                String isresult = jsonObject.getString("IRESULT");
                String smsg = jsonObject.getString("SMSG");
                toservice.put(SystemEnum.TSR_RESULT.name(), isresult);
                toservice.put(SystemEnum.TSR_CODE.name(), ResultTool.transfarBsnResult(isresult));
                toservice.put(SystemEnum.TSR_MSG.name(), MsgTool.getMsg(isresult));
                toservice.put(SystemEnum.TUXEDO_MSG.name(), MainUtils.getNullEmpty(smsg));

                toservice.put("chargeChtCh", MainUtils.getNullEmpty(jsonObject.getString("CHARGE_CNT_CH")));
                toservice.put("durationCntCh", MainUtils.getNullEmpty(jsonObject.getString("DURATION_CNT_CH")));
                toservice.put("totalBytesCnt", MainUtils.getNullEmpty(jsonObject.getString("TOTAL_BYTES_CNT")));

                JSONArray blances = new JSONArray();
                String json = jsonObject.getString("NARROW_DATA_GUlist");
                if (json.startsWith("[")) {
                	JSONArray array = jsonObject.getJSONArray("NARROW_DATA_GUlist");
                    toservice.put("count", String.valueOf(array.size()));
                    for (int i = 0; i < array.size(); i++) {
                        JSONObject obj = new JSONObject();
                        JSONObject jsonBlance = (JSONObject) array.get(i);
                        obj.put("ticketNumber", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_NUMBER")));// 序号
                        obj.put("ticketId", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_ID")));// 话单标识
                        obj.put("areaCode", MainUtils.getNullEmpty(jsonBlance.getString("AREA_CODE")));// 区域编码
                        obj.put("nbr", MainUtils.getNullEmpty(jsonBlance.getString("ACC_NBR")));// 对方号码
                        obj.put("accName", MainUtils.getNullEmpty(jsonBlance.getString("ACCT_NAME")));// 宽带账号
                        obj.put("calledNbr", MainUtils.getNullEmpty(jsonBlance.getString("CALLED_NBR")));// 被叫号码
                        obj.put("ticketType", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_TYPE"))); // 业务类型
                        obj.put("startDateNew", MainUtils.getNullEmpty(jsonBlance.getString("START_DATE_NEW")));// 上线日期
                        obj.put("startTimeNew", MainUtils.getNullEmpty(jsonBlance.getString("START_TIME_NEW")));// 上线时间
                        obj.put("endDateNew", MainUtils.getNullEmpty(jsonBlance.getString("END_DATE_NEW")));// 上线日期
                        obj.put("endTimeNew", MainUtils.getNullEmpty(jsonBlance.getString("END_TIME_NEW")));// 上线时间
                        obj.put("duartionCh", MainUtils.getNullEmpty(jsonBlance.getString("DURATION_CH")));// 使用时长
                        obj.put("acctItemChargeCh", MainUtils.getNullEmpty(jsonBlance.getString("ACCT_ITEM_CHARGE_CH")));// 话费
                        obj.put("inBytesCnt", MainUtils.getNullEmpty(jsonBlance.getString("IN_BYTES_CNT")));// 流量
                        obj.put("outBytesCnt", MainUtils.getNullEmpty(jsonBlance.getString("OUT_BYTES_CNT")));
                        obj.put("bytesCnt", MainUtils.getNullEmpty(jsonBlance.getString("BYTES_CNT")));
                        obj.put("cycleEndDate", MainUtils.getNullEmpty(jsonBlance.getString("CYCLE_END_DATE")));
                        obj.put("productName", MainUtils.getNullEmpty(jsonBlance.getString("PRODUCT_NAME")));
                        blances.add(obj);
                    }
                } else {
                    toservice.put("count", "1");
                    JSONObject jsonBlance = jsonObject.getJSONObject("NARROW_DATA_GUlist");
                    JSONObject obj = new JSONObject();
                    obj.put("ticketNumber", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_NUMBER")));// 序号
                    obj.put("ticketId", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_TYPE")));// 话单标识
                    obj.put("areaCode", MainUtils.getNullEmpty(jsonBlance.getString("AREA_CODE")));// 区域编码
                    obj.put("nbr", MainUtils.getNullEmpty(jsonBlance.getString("ACC_NBR")));// 对方号码
                    obj.put("accName", MainUtils.getNullEmpty(jsonBlance.getString("ACCT_NAME")));// 宽带账号
                    obj.put("calledNbr", MainUtils.getNullEmpty(jsonBlance.getString("CALLED_NBR")));// 被叫号码
                    obj.put("ticketType", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_TYPE"))); // 业务类型
                    obj.put("startDateNew", MainUtils.getNullEmpty(jsonBlance.getString("START_DATE_NEW")));// 上线日期
                    obj.put("startTimeNew", MainUtils.getNullEmpty(jsonBlance.getString("START_TIME_NEW")));// 上线时间
                    obj.put("endDateNew", MainUtils.getNullEmpty(jsonBlance.getString("END_DATE_NEW")));// 上线日期
                    obj.put("endTimeNew", MainUtils.getNullEmpty(jsonBlance.getString("END_TIME_NEW")));// 上线时间
                    obj.put("duartionCh", MainUtils.getNullEmpty(jsonBlance.getString("DURATION_CH")));// 使用时长
                    obj.put("acctItemChargeCh", MainUtils.getNullEmpty(jsonBlance.getString("ACCT_ITEM_CHARGE_CH")));// 话费
                    obj.put("inBytesCnt", MainUtils.getNullEmpty(jsonBlance.getString("IN_BYTES_CNT")));// 流量
                    obj.put("outBytesCnt", MainUtils.getNullEmpty(jsonBlance.getString("OUT_BYTES_CNT")));
                    obj.put("bytesCnt", MainUtils.getNullEmpty(jsonBlance.getString("BYTES_CNT")));
                    obj.put("cycleEndDate", MainUtils.getNullEmpty(jsonBlance.getString("CYCLE_END_DATE")));
                    obj.put("productName", MainUtils.getNullEmpty(jsonBlance.getString("PRODUCT_NAME")));
                    blances.add(obj);
                }
                toservice.put("items", blances);
                return toservice;
            } else {
                return JSonResultHelper.exportJSONObjectReturnResultBlank();
            }
        } catch (Exception e) {
            LOGGER.debug("窄带详单查询解析异常!", e);
            return JSonResultHelper.exportJSONObjectOtherError();
        }
    }

    /**
     * Description: 互联星空出参解析<br>
     * 
     * @author Administrator<br>
     * @taskId <br>
     * @param rsp
     * @return <br>
     */
    public JSONObject resolveCallNewInfoDataQr(String rsp) {
        try {
            JSONObject toservice = new JSONObject();// 返回给业务层
            JSONObject root = XML2JSON.xmlToJson(rsp);
            JSONObject jsonObject = root.getJSONObject("Body").getJSONObject("INFO_DATA_GURsp");
            if (jsonObject != null && (!jsonObject.isEmpty())) {
                String isresult = jsonObject.getString("IRESULT");
                String smsg = jsonObject.getString("SMSG");
                toservice.put(SystemEnum.TSR_RESULT.name(), isresult);
                toservice.put(SystemEnum.TSR_CODE.name(), ResultTool.transfarBsnResult(isresult));
                toservice.put(SystemEnum.TSR_MSG.name(), MsgTool.getMsg(isresult));
                toservice.put(SystemEnum.TUXEDO_MSG.name(), MainUtils.getNullEmpty(smsg));
                toservice.put("changeCntch", MainUtils.getNullEmpty(jsonObject.getString("CHARGE_CNT_CH")));// 费用合计
                toservice.put("duartionCntCh", MainUtils.getNullEmpty(jsonObject.getString("DURATION_CNT_CH")));// 使用时长合计

                JSONArray blances = new JSONArray();
                String json = jsonObject.getString("INFO_DATA_GUlist");
                if (json.startsWith("[")) {
                	JSONArray array = jsonObject.getJSONArray("INFO_DATA_GUlist");
                    toservice.put("count", String.valueOf(array.size()));
                    for (int i = 0; i < array.size(); i++) {
                        JSONObject obj = new JSONObject();
                        JSONObject jsonBlance = (JSONObject) array.get(i);
                        obj.put("ticketNumber", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_NUMBER")));// 序号
                        obj.put("ticketId", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_ID")));// 话单标识
                        obj.put("areaCode", MainUtils.getNullEmpty(jsonBlance.getString("AREA_CODE")));// 区域编码
                        obj.put("nbr", MainUtils.getNullEmpty(jsonBlance.getString("ACC_NBR")));// 对方号码
                        obj.put("acctName", MainUtils.getNullEmpty(jsonBlance.getString("ACCT_NAME")));// 宽带账号
                        obj.put("ticketType", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_TYPE"))); // 业务类型
                        obj.put("startDateNew", MainUtils.getNullEmpty(jsonBlance.getString("START_DATE_NEW")));// 上线日期
                        obj.put("startTimeNew", MainUtils.getNullEmpty(jsonBlance.getString("START_TIME_NEW")));// 上线时间
                        obj.put("endDateNew", MainUtils.getNullEmpty(jsonBlance.getString("END_DATE_NEW")));// 上线日期
                        obj.put("endTimeNew", MainUtils.getNullEmpty(jsonBlance.getString("END_TIME_NEW")));// 上线时间
                        obj.put("duartionCh", MainUtils.getNullEmpty(jsonBlance.getString("DURATION_CH")));// 使用时长
                        obj.put("acctItemChargeCh", MainUtils.getNullEmpty(jsonBlance.getString("ACCT_ITEM_CHARGE_CH")));// 话费
                        obj.put("cycleEndDate", MainUtils.getNullEmpty(jsonBlance.getString("CYCLE_END_DATE")));
                        obj.put("productName", MainUtils.getNullEmpty(jsonBlance.getString("PRODUCT_NAME")));
                        obj.put("switchItem", MainUtils.getNullEmpty(jsonBlance.getString("SWITCH_ITEM")));
                        obj.put("agentCode", MainUtils.getNullEmpty(jsonBlance.getString("AGENT_CODE")));
                        blances.add(obj);
                    }
                } else {
                    toservice.put("count", "1");
                    JSONObject jsonBlance = jsonObject.getJSONObject("INFO_DATA_GUlist");
                    JSONObject obj = new JSONObject();
                    obj.put("ticketNumber", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_NUMBER")));// 序号
                    obj.put("ticketId", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_ID")));// 话单标识
                    obj.put("areaCode", MainUtils.getNullEmpty(jsonBlance.getString("AREA_CODE")));// 区域编码
                    obj.put("nbr", MainUtils.getNullEmpty(jsonBlance.getString("ACC_NBR")));// 对方号码
                    obj.put("acctName", MainUtils.getNullEmpty(jsonBlance.getString("ACCT_NAME")));// 宽带账号
                    obj.put("ticketType", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_TYPE"))); // 业务类型
                    obj.put("startDateNew", MainUtils.getNullEmpty(jsonBlance.getString("START_DATE_NEW")));// 上线日期
                    obj.put("startTimeNew", MainUtils.getNullEmpty(jsonBlance.getString("START_TIME_NEW")));// 上线时间
                    obj.put("endDateNew", MainUtils.getNullEmpty(jsonBlance.getString("END_DATE_NEW")));// 上线日期
                    obj.put("endTimeNew", MainUtils.getNullEmpty(jsonBlance.getString("END_TIME_NEW")));// 上线时间
                    obj.put("duartionCh", MainUtils.getNullEmpty(jsonBlance.getString("DURATION_CH")));// 使用时长
                    obj.put("acctItemChargeCh", MainUtils.getNullEmpty(jsonBlance.getString("ACCT_ITEM_CHARGE_CH")));// 话费
                    obj.put("cycleEndDate", MainUtils.getNullEmpty(jsonBlance.getString("CYCLE_END_DATE")));
                    obj.put("productName", MainUtils.getNullEmpty(jsonBlance.getString("PRODUCT_NAME")));
                    obj.put("switchItem", MainUtils.getNullEmpty(jsonBlance.getString("SWITCH_ITEM")));
                    obj.put("agentCode", MainUtils.getNullEmpty(jsonBlance.getString("AGENT_CODE")));
                    blances.add(obj);
                }
                toservice.put("items", blances);
                return toservice;
            } else {
                return JSonResultHelper.exportJSONObjectReturnResultBlank();
            }
        } catch (Exception e) {
            LOGGER.debug("互联星空回参解析异常!", e);
            return JSonResultHelper.exportJSONObjectOtherError();
        }
    }

    /**
     * Description: 新业务回参解析<br>
     * 
     * @author Administrator<br>
     * @taskId <br>
     * @param rsp
     * @return <br>
     */
    public JSONObject resolveCallNewHttpSvrTicQr(String rsp) {
        try {
            JSONObject toservice = new JSONObject();// 返回给业务层
            JSONObject root = XML2JSON.xmlToJson(rsp);
            JSONObject jsonObject = root.getJSONObject("Body").getJSONObject("NEW_SVR_TIC_GURsp");
            if (jsonObject != null && (!jsonObject.isEmpty())) {
                String isresult = jsonObject.getString("IRESULT");
                String smsg = jsonObject.getString("SMSG");
                toservice.put(SystemEnum.TSR_RESULT.name(), isresult);
                toservice.put(SystemEnum.TSR_CODE.name(), ResultTool.transfarBsnResult(isresult));
                toservice.put(SystemEnum.TSR_MSG.name(), MsgTool.getMsg(isresult));
                toservice.put(SystemEnum.TUXEDO_MSG.name(), MainUtils.getNullEmpty(smsg));

                toservice.put("changeCntch", MainUtils.getNullEmpty(jsonObject.getString("CHARGE_CNT_CH")));// 费用合计
                toservice.put("totalTicketChargeCh", MainUtils.getNullEmpty(jsonObject.getString("TOTAL_TICKET_CHARGE_CH")));// 通信费用总计
                toservice.put("totalDisctChargeCh", MainUtils.getNullEmpty(jsonObject.getString("TOTAL_DISCT_CHARGE_CH")));// 优惠费用总计

                toservice.put("duartionCntCh", MainUtils.getNullEmpty(jsonObject.getString("DURATION_CNT_CH")));// 使用时长合计

                JSONArray blances = new JSONArray();
                String json = jsonObject.getString("NEW_SVR_TICGUlist");
                if (json.startsWith("[")) {
                	JSONArray array = jsonObject.getJSONArray("NEW_SVR_TICGUlist");
                    toservice.put("count", String.valueOf(array.size()));
                    for (int i = 0; i < array.size(); i++) {
                        JSONObject obj = new JSONObject();
                        JSONObject jsonBlance = (JSONObject) array.get(i);
                        obj.put("ticketNumber", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_NUMBER")));// 序号
                        obj.put("ticketId", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_ID")));// 话单标识
                        obj.put("areaCode", MainUtils.getNullEmpty(jsonBlance.getString("AREA_CODE")));// 区域编码
                        obj.put("nbr", MainUtils.getNullEmpty(jsonBlance.getString("ACC_NBR")));// 接入号码
                        obj.put("calledNbr", MainUtils.getNullEmpty(jsonBlance.getString("CALLED_NBR")));// 被叫号码
                        obj.put("calledAeraCode", MainUtils.getNullEmpty(jsonBlance.getString("CALLED_AREA_CODE")));// 被叫地区
                        obj.put("ticketType", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_TYPE"))); // 业务类型
                        obj.put("startDateNew", MainUtils.getNullEmpty(jsonBlance.getString("START_DATE_NEW")));// 上线日期
                        obj.put("startTimeNew", MainUtils.getNullEmpty(jsonBlance.getString("START_TIME_NEW")));// 上线时间
                        obj.put("duartionCh", MainUtils.getNullEmpty(jsonBlance.getString("DURATION_CH")));// 使用时长
                        obj.put("acctItemChargeCh", MainUtils.getNullEmpty(jsonBlance.getString("ACCT_ITEM_CHARGE_CH")));// 话费
                        obj.put("ticketChargeCh", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_CHARGE_CH")));//
                        obj.put("disctChargeCh", MainUtils.getNullEmpty(jsonBlance.getString("DISCT_CHARGE_CH")));
                        obj.put("productName", MainUtils.getNullEmpty(jsonBlance.getString("PRODUCT_NAME")));
                        obj.put("agentCode", MainUtils.getNullEmpty(jsonBlance.getString("AGENT_CODE")));
                        blances.add(obj);
                    }
                } else {
                    toservice.put("count", "1");
                    JSONObject jsonBlance = jsonObject.getJSONObject("NEW_SVR_TICGUlist");
                    JSONObject obj = new JSONObject();
                    obj.put("ticketNumber", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_NUMBER")));// 序号
                    obj.put("ticketId", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_ID")));// 话单标识
                    obj.put("areaCode", MainUtils.getNullEmpty(jsonBlance.getString("AREA_CODE")));// 区域编码
                    obj.put("nbr", MainUtils.getNullEmpty(jsonBlance.getString("ACC_NBR")));// 接入号码
                    obj.put("calledNbr", MainUtils.getNullEmpty(jsonBlance.getString("CALLED_NBR")));// 被叫号码
                    obj.put("calledAeraCode", MainUtils.getNullEmpty(jsonBlance.getString("CALLED_AREA_CODE")));// 被叫地区
                    obj.put("ticketType", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_TYPE"))); // 业务类型
                    obj.put("startDateNew", MainUtils.getNullEmpty(jsonBlance.getString("START_DATE_NEW")));// 上线日期
                    obj.put("startTimeNew", MainUtils.getNullEmpty(jsonBlance.getString("START_TIME_NEW")));// 上线时间
                    obj.put("duartionCh", MainUtils.getNullEmpty(jsonBlance.getString("DURATION_CH")));// 使用时长
                    obj.put("acctItemChargeCh", MainUtils.getNullEmpty(jsonBlance.getString("ACCT_ITEM_CHARGE_CH")));// 话费
                    obj.put("ticketChargeCh", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_CHARGE_CH")));//
                    obj.put("disctChargeCh", MainUtils.getNullEmpty(jsonBlance.getString("DISCT_CHARGE_CH")));
                    obj.put("productName", MainUtils.getNullEmpty(jsonBlance.getString("PRODUCT_NAME")));
                    obj.put("agentCode", MainUtils.getNullEmpty(jsonBlance.getString("AGENT_CODE")));
                    blances.add(obj);
                }
                toservice.put("items", blances);
                return toservice;
            } else {
                return JSonResultHelper.exportJSONObjectReturnResultBlank();
            }
        } catch (Exception e) {
            LOGGER.debug("新业务回参解析异常={}!", e.getMessage());
            return JSonResultHelper.exportJSONObjectOtherError();
        }
    }

    /**
     * Description: 电话清单查询回参解析<br>
     * 
     * @author Administrator<br>
     * @taskId <br>
     * @param rsp
     * @return <br>
     */
    public JSONObject resolveCallNewMessageTicketQr(String rsp) {
        try {
            JSONObject toservice = new JSONObject();// 返回给业务层
            JSONObject root = XML2JSON.xmlToJson(rsp);
            JSONObject jsonObject = root.getJSONObject("Body").getJSONObject("MESSAGE_TICKET_GURsp");
            if (jsonObject != null && (!jsonObject.isEmpty())) {
                String isresult = jsonObject.getString("IRESULT");
                String smsg = jsonObject.getString("SMSG");
                toservice.put(SystemEnum.TSR_RESULT.name(), isresult);
                toservice.put(SystemEnum.TSR_CODE.name(), ResultTool.transfarBsnResult(isresult));
                toservice.put(SystemEnum.TSR_MSG.name(), MsgTool.getMsg(isresult));
                toservice.put(SystemEnum.TUXEDO_MSG.name(), MainUtils.getNullEmpty(smsg));

                toservice.put("changeCntch", MainUtils.getNullEmpty(jsonObject.getString("CHARGE_CNT_CH")));// 费用合计
                toservice.put("totalTicketChargeCh", MainUtils.getNullEmpty(jsonObject.getString("TOTAL_TICKET_CHARGE_CH")));// 通信费用总计
                toservice.put("totalDisctChargeCh", MainUtils.getNullEmpty(jsonObject.getString("TOTAL_DISCT_CHARGE_CH")));// 优惠费用总计

                toservice.put("duartionCntCh", MainUtils.getNullEmpty(jsonObject.getString("DURATION_CNT_CH")));// 使用时长合计

                JSONArray blances = new JSONArray();
                String json = jsonObject.getString("MESSAGETICKETGUlist");
                if (json.startsWith("[")) {
                	JSONArray array = jsonObject.getJSONArray("MESSAGETICKETGUlist");
                    toservice.put("count", String.valueOf(array.size()));
                    for (int i = 0; i < array.size(); i++) {
                        JSONObject obj = new JSONObject();
                        JSONObject jsonBlance = (JSONObject) array.get(i);
                        obj.put("ticketNumber", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_NUMBER")));// 序号
                        obj.put("ticketId", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_ID")));// 话单标识
                        obj.put("areaCode", MainUtils.getNullEmpty(jsonBlance.getString("AREA_CODE")));// 区域编码
                        obj.put("nbr", MainUtils.getNullEmpty(jsonBlance.getString("ACC_NBR")));// 接入号码
                        obj.put("calledNbr", MainUtils.getNullEmpty(jsonBlance.getString("CALLED_NBR")));// 被叫号码
                        obj.put("calledAeraCode", MainUtils.getNullEmpty(jsonBlance.getString("CALLED_AREA_CODE")));// 被叫地区
                        obj.put("ticketType", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_TYPE"))); // 业务类型
                        obj.put("startDateNew", MainUtils.getNullEmpty(jsonBlance.getString("START_DATE_NEW")));// 上线日期
                        obj.put("startTimeNew", MainUtils.getNullEmpty(jsonBlance.getString("START_TIME_NEW")));// 上线时间
                        obj.put("duartionCh", MainUtils.getNullEmpty(jsonBlance.getString("DURATION_CH")));// 使用时长
                        obj.put("acctItemChargeCh", MainUtils.getNullEmpty(jsonBlance.getString("ACCT_ITEM_CHARGE_CH")));// 话费
                        obj.put("ticketChargeCh", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_CHARGE_CH")));//
                        obj.put("disctChargeCh", MainUtils.getNullEmpty(jsonBlance.getString("DISCT_CHARGE_CH")));
                        obj.put("productName", MainUtils.getNullEmpty(jsonBlance.getString("PRODUCT_NAME")));
                        obj.put("agentCode", MainUtils.getNullEmpty(jsonBlance.getString("AGENT_CODE")));
                        blances.add(obj);
                    }
                } else {
                    toservice.put("count", "1");
                    JSONObject jsonBlance = jsonObject.getJSONObject("MESSAGETICKETGUlist");
                    JSONObject obj = new JSONObject();
                    obj.put("ticketNumber", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_NUMBER")));// 序号
                    obj.put("ticketId", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_ID")));// 话单标识
                    obj.put("areaCode", MainUtils.getNullEmpty(jsonBlance.getString("AREA_CODE")));// 区域编码
                    obj.put("nbr", MainUtils.getNullEmpty(jsonBlance.getString("ACC_NBR")));// 接入号码
                    obj.put("calledNbr", MainUtils.getNullEmpty(jsonBlance.getString("CALLED_NBR")));// 被叫号码
                    obj.put("calledAeraCode", MainUtils.getNullEmpty(jsonBlance.getString("CALLED_AREA_CODE")));// 被叫地区
                    obj.put("ticketType", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_TYPE"))); // 业务类型
                    obj.put("startDateNew", MainUtils.getNullEmpty(jsonBlance.getString("START_DATE_NEW")));// 上线日期
                    obj.put("startTimeNew", MainUtils.getNullEmpty(jsonBlance.getString("START_TIME_NEW")));// 上线时间
                    obj.put("duartionCh", MainUtils.getNullEmpty(jsonBlance.getString("DURATION_CH")));// 使用时长
                    obj.put("acctItemChargeCh", MainUtils.getNullEmpty(jsonBlance.getString("ACCT_ITEM_CHARGE_CH")));// 话费
                    obj.put("ticketChargeCh", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_CHARGE_CH")));//
                    obj.put("disctChargeCh", MainUtils.getNullEmpty(jsonBlance.getString("DISCT_CHARGE_CH")));
                    obj.put("productName", MainUtils.getNullEmpty(jsonBlance.getString("PRODUCT_NAME")));
                    obj.put("agentCode", MainUtils.getNullEmpty(jsonBlance.getString("AGENT_CODE")));
                    blances.add(obj);
                }
                toservice.put("items", blances);
                return toservice;
            } else {
                return JSonResultHelper.exportJSONObjectReturnResultBlank();
            }
        } catch (Exception e) {
            LOGGER.debug("电话清单详情解析异常!", e);
            return JSonResultHelper.exportJSONObjectOtherError();
        }
    }
    
    /**
     * Description: 余额结转回参解析<br>
     * 
     * @author Administrator<br>
     * @taskId <br>
     * @param rsp
     * @return <br>
     */
    public JSONObject resolveCallNewBlanceSwitch(String rsp) {
        try {
            JSONObject toservice = new JSONObject();// 返回给业务层
            JSONObject root = XML2JSON.xmlToJson(rsp);
            JSONObject jsonObject = root.getJSONObject("soapenv:Envelope").getJSONObject("soapenv:Body").getJSONObject("web:A_EPAY_TRANSFERRsp");
            if (jsonObject != null && (!jsonObject.isEmpty())) {
                String isresult = jsonObject.getString("IRESULT");
                String smsg = jsonObject.getString("SMSG");
                toservice.put(SystemEnum.TSR_RESULT.name(),isresult);
                toservice.put(SystemEnum.TSR_CODE.name(), ResultTool.transfarBsnResult(isresult));
                toservice.put(SystemEnum.TSR_MSG.name(), MsgTool.getMsg(isresult));
                toservice.put(SystemEnum.TUXEDO_MSG.name(), MainUtils.getNullEmpty(smsg)); 
                
                return toservice;
            } else {
                return JSonResultHelper.exportJSONObjectReturnResultBlank();
            }
        } catch (Exception e) {
            LOGGER.debug("余额结转-20152情解析异常!", e);
            return JSonResultHelper.exportJSONObjectOtherError();
        }
    }
    
    /**
     * Description: 信用度查询接口-30340回参解析<br>
     * 
     * @author Administrator<br>
     * @taskId <br>
     * @param rsp
     * @return <br>
     */
    public JSONObject resolvecallAcctCreditQuery(String rsp) {
        try {
            JSONObject toservice = new JSONObject();// 返回给业务层
            JSONObject root = XML2JSON.xmlToJson(rsp);
            JSONObject jsonObject = root.getJSONObject("soapenv:Envelope").getJSONObject("soapenv:Body").getJSONObject("web:A_EPAY_TRANSFERRsp");
            if (jsonObject != null && (!jsonObject.isEmpty())) {
                String isresult = jsonObject.getString("IRESULT");
                String smsg = jsonObject.getString("SMSG");
                String acctCreditAmout = jsonObject.getString("ACCT_CREDIT_AMOUNT");
                toservice.put(SystemEnum.TSR_RESULT.name(),isresult);
                toservice.put(SystemEnum.TSR_CODE.name(), ResultTool.transfarBsnResult(isresult));
                toservice.put(SystemEnum.TSR_MSG.name(), MsgTool.getMsg(isresult));
                toservice.put(SystemEnum.TUXEDO_MSG.name(), MainUtils.getNullEmpty(smsg)); 
                if("0".equals(isresult)){
                    toservice.put("ACCT_CREDIT_AMOUNT", acctCreditAmout);
                    JSONArray acctCreditList = new JSONArray();
                    if (jsonObject.get("ACCTCREDITQUERYList") instanceof JSONArray) {
                    	acctCreditList = (JSONArray) jsonObject.get("ACCTCREDITQUERYList");
                    } else if (jsonObject.get("ACCTCREDITQUERYList") instanceof JSONObject){
                        JSONObject obj = (JSONObject) jsonObject.get("ACCTCREDITQUERYList");
                        acctCreditList.add(obj);
                    }
                    
                    toservice.put("ACCTCREDITQUERYList", acctCreditList);
                }
                return toservice;
            } else {
                return JSonResultHelper.exportJSONObjectReturnResultBlank();
            }
        } catch (Exception e) {
            LOGGER.debug("信用度查询接口-30340解析异常!", e);
            return JSonResultHelper.exportJSONObjectOtherError();
        }
    }
    
}
