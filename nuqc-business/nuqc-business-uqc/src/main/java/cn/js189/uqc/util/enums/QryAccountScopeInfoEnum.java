package cn.js189.uqc.util.enums;

public enum QryAccountScopeInfoEnum {
		// 客户
		ACCOUNT("account", 1),
		// 客户属性
		ACC_ATTR("paymentPlan", 2),
		//账户属性
        ACCT_ATTR("acctAttr",3),

		EXT_ACCT("extAcct",4);
		/*// 客户标签
		CUST_LABEL("custLabel", 3),
		// 客户特殊名单
		SPECIAL_LIST("specialList", 4),
		// 客户服务等级
		SERVICE_GRADE("serviceGrade", 5),
		// 客户联系信息
		CUST_CONTACT_INFO_REL("custContactsInfo", 6),
		// 参与人信息
		PARTY("party", 7),
		// 参与人证件
		PARTY_CERT("partyCert", 8);*/

		// 范围编码
		private String scopeCode;
		// 范围索引
		private int scopeIndex;

		private QryAccountScopeInfoEnum(String scopeCode, int scopeIndex) {
			this.scopeCode = scopeCode;
			this.scopeIndex = scopeIndex;
		}

		public String getScopeCode() {
			return scopeCode;
		}

		public int getScopeIndex() {
			return scopeIndex;
		}
}
