package cn.js189.uqc.domain.qry;

import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class QryMemberInfo extends QryMemberInfoBase{
	// 日志组件
	private static final Logger LOGGER = LoggerFactory.getLogger(QryMemberInfo.class);
	// 业务号码 产品实例的业务号码。
	private String accNum;
	// 产品规格标识
	private String prodId;


	public QryMemberInfo(String regionId, String uncompletedFlag,
	                     String accNum, String prodId) {
		super( regionId, uncompletedFlag);
		this.accNum = accNum;
		this.uncompletedFlag = uncompletedFlag;
		this.prodId = prodId;
	}
	
	
	
	@Override
	protected JSONObject improveRequestObject(JSONObject requestObject) {
		requestObject.put("accNum", accNum);
		requestObject.put("prodId", prodId);
		requestObject.put("compProdId", "100000966");
		LOGGER.debug("requestObject:{}", requestObject.toString());
		return requestObject;
	}

	public String getAccNum() {
		return accNum;
	}

	public void setAccNum(String accNum) {
		this.accNum = accNum;
	}

	public String getProdId() {
		return prodId;
	}

	public void setProdId(String prodId) {
		this.prodId = prodId;
	}

	public static Logger getLogger() {
		return LOGGER;
	}
	
}
