package cn.js189.uqc.domain.qry;

import com.alibaba.fastjson.JSONObject;

import java.util.List;

/**
 * 
 * <Description> 2.27.查询政企客户等级、关键人联系人 入参拼接<br>
 * 
 * @author zhy<br>
 * @version 1.0<br>
 * @taskId <br>
 * @CreateDate 2022年6月30日 下午3:20:00 <br>
 */
public class QryCustlevelAndKeyPerson extends QryCustlevelAndKeyPersonBase {
	
	

	// 接入号码
	private String accNum;
	
	//证件id
	private String certNum;
	
	//客户id
	private String custId;
	
	//查询内容
	private List<Integer> queryContent;
	
	
	
	public QryCustlevelAndKeyPerson(String accNum, String certNum, String custId, List<Integer> queryContent, String  regionId) {
		super(regionId);
		this.accNum=accNum;
		this.certNum=certNum;
		this.custId=custId;
		this.queryContent=queryContent;
	}

	@Override
	protected JSONObject improveRequestObject(JSONObject requestObject) {
		requestObject.put("accNum", accNum);
		requestObject.put("certNum", certNum);
		requestObject.put("custId", custId);
		requestObject.put("queryContent", queryContent);
		return requestObject;
		
	}

}
