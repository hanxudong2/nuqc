package cn.js189.uqc.common;

import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 通过证件号码的方式查询客户信息，生成请求报文
 * 
 * @author Mid
 */
public class QryCustomerByCertNum extends QryCustomerBase {
	// 日志组件
	private static final Logger LOGGER = LoggerFactory.getLogger(QryCustomerByCertNum.class);

	// 证件号码
	private String certNum;
	// 证件号码类型
	private String certType;

	public QryCustomerByCertNum(String regionId, String uncompletedFlag, String certNum, String certType) {
		super(regionId, uncompletedFlag);
		this.certNum = certNum;
		this.certType = certType;
	}

	@Override
	protected JSONObject improveRequestObject(JSONObject requestObject) {
		requestObject.put("certNum", certNum);
		requestObject.put("certType", certType);
		LOGGER.debug("requestObject:{}", requestObject);
		return requestObject;
	}

}
