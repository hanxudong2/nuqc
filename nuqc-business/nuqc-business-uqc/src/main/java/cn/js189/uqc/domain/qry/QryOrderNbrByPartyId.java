package cn.js189.uqc.domain.qry;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * 通过证件号码的方式查询客户信息，生成请求报文
 * 
 * @author cx
 */
public class QryOrderNbrByPartyId extends QryOrderNbrByPartyIdBase {
	// 日志组件
	private static final Logger LOGGER = LoggerFactory.getLogger(QryOrderNbrByPartyId.class);

	// 客户belongCustId
	private String belongCustId;
	// 受理员工id标识
	private List<String> createStaffs;
	// 受理员工四级区域标识
	private String acceptRegionId;

	public QryOrderNbrByPartyId(String regionId, String belongCustId,List<String> createStaffs,String acceptRegionId) {
		super(regionId);
		this.belongCustId = belongCustId;
		this.createStaffs = createStaffs;
		this.acceptRegionId = acceptRegionId;
	}
	public QryOrderNbrByPartyId(String regionId, String belongCustId) {
		super(regionId);
		this.belongCustId = belongCustId;
	}

	@Override
	protected JSONObject improveRequestObject(JSONObject requestObject) {
		requestObject.put("belongCustId", belongCustId);
//		requestObject.put("createStaffs", createStaffs);
		requestObject.put("acceptRegionId", acceptRegionId);
		//订单项状态（除了竣工状态，基本是未竣工状态，可选取客户需求确认，在途，开通中，竣工中）
		JSONArray orderItemStatusCds = new JSONArray();
		orderItemStatusCds.add("301100");
		requestObject.put("orderItemStatusCds", orderItemStatusCds);
		LOGGER.debug("requestObject:{}", requestObject.toString());
		return requestObject;
	}

}
