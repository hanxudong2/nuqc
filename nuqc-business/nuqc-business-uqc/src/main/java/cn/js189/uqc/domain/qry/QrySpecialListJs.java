package cn.js189.uqc.domain.qry;

import cn.js189.common.util.StringUtils;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * 通过接入号的方式查询客户信息，生成请求报文
 * 
 * @author Mid
 */
public class QrySpecialListJs extends QrySpecialListJsBase {
	// 日志组件
	private static final Logger LOGGER = LoggerFactory.getLogger(QrySpecialListJs.class);

	//两个条件只能有一个
	// 查询custId
	private String partyId;
	//查询身份证
	private String certNum;
	//证件类型
	private String certType;
	

	public QrySpecialListJs(String regionId, String partyId, String certNum, String certType) {
		super(regionId);
		this.partyId = partyId;
		this.certNum= certNum;
		this.certType = certType;
	}

	@Override
    protected JSONObject improveRequestObject(JSONObject requestObject) {
        requestObject.put("regionId", regionId);
        if (!StringUtils.isEmpty(partyId)) {
            requestObject.put("partyId", partyId);
        } else {
            requestObject.put("certNum", certNum);
//            requestObject.put("certType", "1");//电渠默认只有身份证
        }
        if (!StringUtils.isEmpty(certType)) {
            requestObject.put("certType", certType);
        } 
        LOGGER.debug("requestObject:{}", requestObject);
        return requestObject;
    }
	
}
