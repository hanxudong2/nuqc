package cn.js189.uqc.controller.noc;

import cn.js189.uqc.service.INocBillService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @program: xxb
 * @description: Noc相关接口
 * @author: jzd
 * @create: 2024/3/1 10:43
 **/
@RestController
public class NocController {
    @Resource
    private INocBillService nocBillService;

    /**
     * 新增发票抬头信息接口
     * @param requestJson
     * @return
     */
    @PostMapping("/iNocBill/addInvoiceInformation")
    public String addInvoiceInformation(@RequestBody String requestJson) {
        return nocBillService.addInvoiceInformation(requestJson);
    }

    /**
     * 查询手机的在线网络类型
     * @param requestJSON
     * @return String<br>
     */
    @PostMapping("/iNocBill/askUserSessionType")
    public String askUserSessionType(@RequestBody String requestJSON){
        return nocBillService.askUserSessionType(requestJSON);
    }

    /**
     * 营业执照识别
     * @param requestJSON
     * @return String
     */
    @PostMapping("/iNocBill/bussinessOcr")
    public String bussinessOcr(@RequestBody String requestJSON) {
        return nocBillService.bussinessOcr(requestJSON);
    }

    /**
     * 根据宽带接入号查询宽带在线（查ip和端口）
     * @param param
     * @return String
     */
    @PostMapping("/iNocBill/callAdslOrLanOnline")
    public String callAdslOrLanOnline(@RequestBody String param) {
        return nocBillService.callAdslOrLanOnline(param);
    }

    /**
     * 查询发票抬头信息接口
     * @param requestJson
     * @return String
     */
    @PostMapping("/iNocBill/checkInvoiceInformation")
    public String checkInvoiceInformation(@RequestBody String requestJson) {
        return nocBillService.checkInvoiceInformation(requestJson);
    }

    /**
     * UIM卡是否支持指定服务校验
     * @param requestJSON
     * @return String
     */
    @PostMapping("/iNocBill/checkServSpecByDevModel")
    public String checkServSpecByDevModel(@RequestBody String requestJSON) {
        return nocBillService.checkServSpecByDevModel(requestJSON);
    }

    /**
     * 获取检测结果接口
     * @param request
     * @return String
     */
    @PostMapping("/iNocBill/getAutoCheckResult")
    public String getAutoCheckResult(@RequestBody String request) {
        return nocBillService.getAutoCheckResult(request);
    }

    /**
     * 根据号码查询卡类型
     * @param requestJSON
     * @return String
     */
    @PostMapping("/iNocBill/getCardInfoByNum")
    public String getCardInfoByNum(@RequestBody String requestJSON) {
        return nocBillService.getCardInfoByNum(requestJSON);
    }

    /**
     * 查询终端设备序列号
     * @param requestJSON
     * @return String
     */
    @PostMapping("/iNocBill/queryDevSN")
    public String queryDevSN(@RequestBody String requestJSON) {
        return nocBillService.queryDevSN(requestJSON);
    }

    /**
     * 政支透明工单查询
     * @param requestJSON
     * @return String
     */
    @PostMapping("/iNocBill/queryOrderDetail")
    public String queryOrderDetail(@RequestBody String requestJSON) {
        return nocBillService.queryOrderDetail(requestJSON);
    }

    /**
     * noc宽带提速速率是否能达到
     * @param param
     * @return String
     */
    @PostMapping("/iNocBill/queryResAbility")
    public String queryResAbility(@RequestBody String param) {
        return nocBillService.queryResAbility(param);
    }

    /**
     * 装维人员服务号码查询
     * @param requestJSON
     * @return String
     */
    @PostMapping("/iNocBill/queryServiceNum")
    public String queryServiceNum(@RequestBody String requestJSON) {
        return nocBillService.queryServiceNum(requestJSON);
    }

    /**
     * itv终端信息
     * @param requestJSON
     * @return String
     */
    @PostMapping("/iNocBill/queryUserDevInfo")
    public String queryUserDevInfo(@RequestBody String requestJSON) {
        return nocBillService.queryUserDevInfo(requestJSON);
    }

    /**
     * 印章识别接口
     * @param requestJSON
     * @return String
     */
    @PostMapping("/iNocBill/stampDect")
    public String stampDect(@RequestBody String requestJSON) {
        return nocBillService.stampDect(requestJSON);
    }

    /**
     * 触发一键检测接口
     * @param request
     * @return String
     */
    @PostMapping("/iNocBill/startAutoCheck")
    public String startAutoCheck(@RequestBody String request) {
        return nocBillService.startAutoCheck(request);
    }

    /**
     * 手机故障诊断接口
     * @param request
     * @return String
     */
    @PostMapping("/iNocBill/toIvrCheck")
    public String toIvrCheck(@RequestBody String request) {
        return nocBillService.toIvrCheck(request);
    }

    /**
     * 修改发票抬头信息接口
     * @param requestJson
     * @return String
     */
    @PostMapping("/iNocBill/updateInvoiceInformation")
    public String updateInvoiceInformation(@RequestBody String requestJson) {
        return nocBillService.updateInvoiceInformation(requestJson);
    }

    /**
     * 上传营业执照和单位授权书至企信FTP接口
     * @param requestJSON
     * @return String
     */
    @PostMapping("/iNocBill/uploadOcrFtp")
    public String uploadOcrFtp(@RequestBody String requestJSON) {
        return nocBillService.uploadOcrFtp(requestJSON);
    }
}
