package cn.js189.uqc.common;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 通过产品实例的方式查询产品关联关系信息，生成请求报文
 * 
 * @author Mid
 */
public class QryProdInstRelByProdId extends QryProdRelBase {
	// 日志组件
	private static final Logger LOGGER = LoggerFactory.getLogger(QryProdInstRelByProdId.class);

	// 产品实例id
	private String prodInstId;
	
	//查询关系
	private JSONArray relTypes;

	public QryProdInstRelByProdId(String regionId, String uncompletedFlag, String prodInstId, JSONArray relTypes) {
		super(regionId, uncompletedFlag);
		this.prodInstId = prodInstId;
		this.relTypes = relTypes;
	}

	@Override
	protected JSONObject improveRequestObject(JSONObject requestObject) {
		requestObject.put("prodInstId", prodInstId);
		requestObject.put("relTypes", relTypes);
		LOGGER.debug("requestObject:{}", requestObject);
		return requestObject;
	}
}
