package cn.js189.uqc.domain;

import java.util.Date;

public class User {

	private Long id;

	/**
	 * 用户身份证号码
	 */
	private String idCard;

	/**
	 * 客户id
	 */
	private String partyId;

	/**
	 * 用户类型
	 * 1个人 2政企
	 */
	private String partyType;

	/**
	 * 微信侧用户唯一标识
	 */
	private String wxOpenId;

	/**
	 * 支付宝侧用户唯一标识
	 */
	private String aliOpenId;

	/**
	 * 用户号码
	 * 当前登陆号码
	 */
	private String loginPhone;

	/**
	 * 号码类型
	 */
	private String phoneType;

	/**
	 * 区号
	 */
	private String areaCode;

	/**
	 * 用户邮箱
	 * 接收电子发票
	 */
	private String mail;

	/**
	 * 微信册授权标识 0未授权 1已授权
	 */
	private String wxAuth;

	/**
	 * 支付宝册授权标识 0未授权 1已授权
	 */
	private String aliAuth;

	/**
	 * 创建时间
	 */
	private Date gmtCreate;

	/**
	 * 修改时间
	 */
	private Date gmtUpdate;

	/**
	 * 政企用户税号
	 */
	private String taxNum;

	public User() {
	}

	public User(String partyId, String mail, String taxNum) {
		this.partyId = partyId;
		this.mail = mail;
		this.taxNum = taxNum;
	}

	public User(String partyId, String wxAuth, Date gmtUpdate) {
		this.partyId = partyId;
		this.wxAuth = wxAuth;
		this.gmtUpdate = gmtUpdate;
	}

	public User(String partyId, String idCard, String loginPhone, String phoneType, Date gmtUpdate) {
		this.partyId = partyId;
		this.idCard = idCard;
		this.loginPhone = loginPhone;
		this.phoneType = phoneType;
		this.gmtUpdate = gmtUpdate;
	}

	public String getTaxNum() {
		return taxNum;
	}

	public void setTaxNum(String taxNum) {
		this.taxNum = taxNum;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	public String getPartyId() {
		return partyId;
	}

	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}

	public String getPartyType() {
		return partyType;
	}

	public void setPartyType(String partyType) {
		this.partyType = partyType;
	}

	public String getWxOpenId() {
		return wxOpenId;
	}

	public void setWxOpenId(String wxOpenId) {
		this.wxOpenId = wxOpenId;
	}

	public String getAliOpenId() {
		return aliOpenId;
	}

	public void setAliOpenId(String aliOpenId) {
		this.aliOpenId = aliOpenId;
	}

	public String getLoginPhone() {
		return loginPhone;
	}

	public void setLoginPhone(String loginPhone) {
		this.loginPhone = loginPhone;
	}

	public String getPhoneType() {
		return phoneType;
	}

	public void setPhoneType(String phoneType) {
		this.phoneType = phoneType;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getWxAuth() {
		return wxAuth;
	}

	public void setWxAuth(String wxAuth) {
		this.wxAuth = wxAuth;
	}

	public String getAliAuth() {
		return aliAuth;
	}

	public void setAliAuth(String aliAuth) {
		this.aliAuth = aliAuth;
	}

	public Date getGmtCreate() {
		return gmtCreate;
	}

	public void setGmtCreate(Date gmtCreate) {
		this.gmtCreate = gmtCreate;
	}

	public Date getGmtUpdate() {
		return gmtUpdate;
	}

	public void setGmtUpdate(Date gmtUpdate) {
		this.gmtUpdate = gmtUpdate;
	}
}
