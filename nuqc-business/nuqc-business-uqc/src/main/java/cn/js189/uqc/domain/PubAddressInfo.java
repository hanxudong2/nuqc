package cn.js189.uqc.domain;

public class PubAddressInfo {

    private String addreCode;

    private String url;

    private String headParam;

    private String timeOut;

    public String getAddreCode() {
        return addreCode;
    }

    public void setAddreCode(String addreCode) {
        this.addreCode = addreCode;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getHeadParam() {
        return headParam;
    }

    public void setHeadParam(String headParam) {
        this.headParam = headParam;
    }

    public String getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(String timeOut) {
        this.timeOut = timeOut;
    }
}
