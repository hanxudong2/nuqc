package cn.js189.uqc.domain.check;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CheckGovCustomer extends CheckGovCustomerBase{
	// 日志组件
	private static final Logger LOGGER = LoggerFactory.getLogger(CheckGovCustomer.class);
	// 业务号码 产品实例的业务号码。
	private String accNum;
	// 帐号 记录宽带的帐号，如：XXX@adsl。
	private String account;
	// 产品规格标识
	private String prodId;


	public CheckGovCustomer(String regionId, String uncompletedFlag,
	                        String accNum, String account, String prodId) {
		super( regionId, uncompletedFlag);
		this.accNum = accNum;
		this.account = account;
		this.uncompletedFlag = uncompletedFlag;
		this.prodId = prodId;
		if(StringUtils.isNotBlank(accNum)){
			this.account="";
		}
	}
	
	
	
	@Override
	protected JSONObject improveRequestObject(JSONObject requestObject) {
		requestObject.put("accNum", accNum);
		requestObject.put("account", account);
		requestObject.put("prodId", prodId);
		LOGGER.debug("requestObject:{}", requestObject.toString());
		return requestObject;
	}

	public String getAccNum() {
		return accNum;
	}

	public void setAccNum(String accNum) {
		this.accNum = accNum;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getProdId() {
		return prodId;
	}

	public void setProdId(String prodId) {
		this.prodId = prodId;
	}

	public static Logger getLogger() {
		return LOGGER;
	}
	
}
