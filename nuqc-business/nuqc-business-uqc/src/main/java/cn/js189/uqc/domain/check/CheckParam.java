package cn.js189.uqc.domain.check;


import cn.js189.uqc.domain.req.JTRequestParamBase;

public class CheckParam extends JTRequestParamBase {

    /**
     * 系统编码
     */
    public static final String SYSCODE = "S10300";
    /**
     * 应用编码
     */
    public static final String APPCODE = "P107020201";
    /**
     * 密钥
     */
    public static final String KEY = "fbb5979019f249209fed04290e402140";

    @Override
    protected String generateXAuthDq(String requestBody,String method) {
        return super.generateXAuthDq(requestBody,KEY,SYSCODE,APPCODE,method);
    }

}
