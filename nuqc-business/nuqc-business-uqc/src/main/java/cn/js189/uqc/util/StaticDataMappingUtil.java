package cn.js189.uqc.util;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <Description> <br>
 *
 * @author xuzhiyuan<br>
 * @version 1.0<br>
 */
public class StaticDataMappingUtil {
	// 日志组件
	private static final Logger LOGGER = LoggerFactory.getLogger(StaticDataMappingUtil.class);
	
	public static final List<String> ususalProdId = Arrays.asList("100000379","100000009","100000002","100000881");

	// 区号和二级地区编码对应关系
	private static Map<String, String> areaCodeRegionRelMap = new HashMap<String, String>();
	// 二级地区编码和区号对应关系
	private static Map<String, String> regionAreaCodeRelMap = new HashMap<String, String>();

	// 产品规格转换关系
	private static Map<String, String> prodIdCodeMap = new HashMap<String, String>();
	private static Map<String, String> prodIdReverseMap = new HashMap<String, String>();

	//江苏区县与bss区县对应关系
	private static Map<String, String> dubAreaToBssMap = new HashMap<String, String>();

	//bss区县与江苏区县对应关系
	private static Map<String, String> bssToDubAreaMap = new HashMap<String, String>();

	//bss区县与江苏区县中文对应关系
	private static Map<String, String> bssToDubAreaNameMap = new HashMap<String, String>();

	//statucCd转换
	private static Map<String, String> statucCdToNewMap = new HashMap<String, String>();

	private static Map<String, String> statucCdToOldMap = new HashMap<String, String>();

	//prodStatusNameMap产品状态转换
	private static Map<String, String> prodStatusNameMap = new HashMap<String, String>();
	//付费方式转换
	private static Map<String, String> paymentModeCdMap = new HashMap<String, String>();
	private static Map<String, String> paymentModeCdNameMap = new HashMap<String, String>();

	private static Map<String, String> prodstatusCdMap = new HashMap<String, String>();

	// 宽带上行最高可达速率
	private static Map<String, String> kdUpSpeedMap = new HashMap<String, String>();
	// 下行最高可达速率
	private static Map<String, String> kdDownSpeedMap = new HashMap<String, String>();

	//bss证件类型与现网证件类型对应关系
	private static Map<String, String> bssToIndentNbrTypeMap = new HashMap<String, String>();

	// 老证件类型 --> bss证件类型
	private static Map<String, String> indentNbrTypeToBssTypeMap = new HashMap<String,String>();

	private static Map<String,String> identNbrTypeToMNPTypeMap = new HashMap<String,String>();
	static{
	    identNbrTypeToMNPTypeMap.put("1", "CIVIC_ID");//身份证
	    identNbrTypeToMNPTypeMap.put("3", "PASSPORT");//护照
	    identNbrTypeToMNPTypeMap.put("14", "SERVICEMAN_ID");//武装警察身份证件
	    identNbrTypeToMNPTypeMap.put("22", "SERVICEMAN_ID");//警官证
	    identNbrTypeToMNPTypeMap.put("2", "SERVICEMAN_ID");//现役军人居民身份证
	    identNbrTypeToMNPTypeMap.put("15", "COMPANY_ID");//组织机构代码证
	    identNbrTypeToMNPTypeMap.put("6", "BUSINESSLICENSE");//营业执照
	    identNbrTypeToMNPTypeMap.put("4", "HK_MACAU_PASS");//港澳居民来往内地通行证
	    identNbrTypeToMNPTypeMap.put("36", "TOMAINLAND_PASS");
	    identNbrTypeToMNPTypeMap.put("37", "TOMAINLAND_PASS");
	    identNbrTypeToMNPTypeMap.put("42", "TOMAINLAND_PASS");
	    identNbrTypeToMNPTypeMap.put("54", "TOMAINLAND_PASS");
	    identNbrTypeToMNPTypeMap.put("9", "DRIVERLICENSE");//驾驶证
	    identNbrTypeToMNPTypeMap.put("12", "REGISTEREDRESIDENCE");//户口本
	}
	static {
		LOGGER.debug("loading areaCodeRegionRel...");
		/*
		 * 区号和二级地区编码对应关系
		 */
		// 南京
		areaCodeRegionRelMap.put("025", "8320100");
		// 无锡
		areaCodeRegionRelMap.put("0510", "8320200");
		// 镇江
		areaCodeRegionRelMap.put("0511", "8321100");
		// 苏州
		areaCodeRegionRelMap.put("0512", "8320500");
		// 南通
		areaCodeRegionRelMap.put("0513", "8320600");
		// 扬州
		areaCodeRegionRelMap.put("0514", "8321000");
		// 盐城
		areaCodeRegionRelMap.put("0515", "8320900");
		// 徐州
		areaCodeRegionRelMap.put("0516", "8320300");
		// 淮安
		areaCodeRegionRelMap.put("0517", "8320800");
		// 连云港
		areaCodeRegionRelMap.put("0518", "8320700");
		// 常州
		areaCodeRegionRelMap.put("0519", "8320400");
		// 泰州
		areaCodeRegionRelMap.put("0523", "8321200");
		// 宿迁
		areaCodeRegionRelMap.put("0527", "8321300");

		LOGGER.debug("loading regionAreaCodeRel...");
		/*
		 * 二级地区编码和区号对应关系
		 */
		// 南京
		regionAreaCodeRelMap.put("8320100", "025");
		// 无锡
		regionAreaCodeRelMap.put("8320200", "0510");
		// 镇江
		regionAreaCodeRelMap.put("8321100", "0511");
		// 苏州
		regionAreaCodeRelMap.put("8320500", "0512");
		// 南通
		regionAreaCodeRelMap.put("8320600", "0513");
		// 扬州
		regionAreaCodeRelMap.put("8321000", "0514");
		// 盐城
		regionAreaCodeRelMap.put("8320900", "0515");
		// 徐州
		regionAreaCodeRelMap.put("8320300", "0516");
		// 淮安
		regionAreaCodeRelMap.put("8320800", "0517");
		// 连云港
		regionAreaCodeRelMap.put("8320700", "0518");
		// 常州
		regionAreaCodeRelMap.put("8320400", "0519");
		// 泰州
		regionAreaCodeRelMap.put("8321200", "0523");
		// 宿迁
		regionAreaCodeRelMap.put("8321300", "0527");

		LOGGER.debug("loading dubAreaToBssMap...");
		/*
		 * 江苏区县与bss区县对应关系
		 */
		//江宁                  
		dubAreaToBssMap.put("10", "8320115");
		//六合                  
		dubAreaToBssMap.put("13", "8320116");
		//南京市                  
		dubAreaToBssMap.put("3", "8320100");
		//南京市区                  
		dubAreaToBssMap.put("9", "8320101");
		//高淳                  
		dubAreaToBssMap.put("12", "8320125");
		//江浦                  
		dubAreaToBssMap.put("14", "8320111");
		//溧水                  
		dubAreaToBssMap.put("11", "8320124");
		//无锡市                  
		dubAreaToBssMap.put("15", "8320200");
		//无锡市区                  
		dubAreaToBssMap.put("16", "8320201");
		//江阴                  
		dubAreaToBssMap.put("19", "8320281");
		//宜兴                  
		dubAreaToBssMap.put("18", "8320282");
		//镇江市                  
		dubAreaToBssMap.put("4", "8321100");
		//镇江市区                  
		dubAreaToBssMap.put("8", "8321101");
		//丹阳                  
		dubAreaToBssMap.put("5", "8321181");
		//扬中                  
		dubAreaToBssMap.put("7", "8321182");
		//句容                  
		dubAreaToBssMap.put("6", "8321183");
		//苏州市                  
		dubAreaToBssMap.put("20", "8320500");
		//苏州市区                  
		dubAreaToBssMap.put("21", "8320501");
		//常熟                  
		dubAreaToBssMap.put("76", "8320581");
		//张家港                  
		dubAreaToBssMap.put("77", "8320582");
		//昆山                  
		dubAreaToBssMap.put("74", "8320583");
		//吴江                  
		dubAreaToBssMap.put("23", "8320584");
		//太仓                  
		dubAreaToBssMap.put("75", "8320585");
		//海安                  
		dubAreaToBssMap.put("31", "8320621");
		//如东                  
		dubAreaToBssMap.put("29", "8320623");
		//启东                  
		dubAreaToBssMap.put("25", "8320681");
		//如皋                  
		dubAreaToBssMap.put("30", "8320682");
		//海门                  
		dubAreaToBssMap.put("24", "8320684");
		//南通市                  
		dubAreaToBssMap.put("26", "8320600");
		//南通市区                  
		dubAreaToBssMap.put("28", "8320601");
		//通州                  
		dubAreaToBssMap.put("27", "8320612");
		//宝应                  
		dubAreaToBssMap.put("38", "8321023");
		//仪征                  
		dubAreaToBssMap.put("32", "8321081");
		//高邮                  
		dubAreaToBssMap.put("37", "8321084");
		//江都                  
		dubAreaToBssMap.put("36", "8321088");
		//扬州市                  
		dubAreaToBssMap.put("33", "8321000");
		//扬州市区                  
		dubAreaToBssMap.put("34", "8321001");
		//邗江                  
		dubAreaToBssMap.put("35", "8321003");
		//建湖                  
		dubAreaToBssMap.put("47", "8320925");
		//东台                  
		dubAreaToBssMap.put("42", "8320981");
		//大丰                  
		dubAreaToBssMap.put("41", "8320982");
		//盐城市                  
		dubAreaToBssMap.put("39", "8320900");
		//盐城市区                  
		dubAreaToBssMap.put("40", "8320901");
		//响水                  
		dubAreaToBssMap.put("46", "8320921");
		//滨海                  
		dubAreaToBssMap.put("45", "8320922");
		//阜宁                  
		dubAreaToBssMap.put("44", "8320923");
		//射阳                  
		dubAreaToBssMap.put("43", "8320924");
		//徐州市                  
		dubAreaToBssMap.put("48", "8320300");
		//徐州市区                  
		dubAreaToBssMap.put("49", "8320301");
		//贾汪                  
		dubAreaToBssMap.put("90", "8320305");
		//铜山                  
		dubAreaToBssMap.put("50", "8320312");
		//丰县                  
		dubAreaToBssMap.put("55", "8320321");
		//睢宁                  
		dubAreaToBssMap.put("51", "8320324");
		//新沂                  
		dubAreaToBssMap.put("53", "8320381");
		//邳州                  
		dubAreaToBssMap.put("52", "8320382");
		//沛县                  
		dubAreaToBssMap.put("54", "8320322");
		//淮安市                  
		dubAreaToBssMap.put("60", "8320800");
		//淮安市区                  
		dubAreaToBssMap.put("61", "8320801");
		//改为淮安区                  
		dubAreaToBssMap.put("89", "8320803");
		//淮阴                  
		dubAreaToBssMap.put("58", "8320804");
		//涟水                  
		dubAreaToBssMap.put("62", "8320826");
		//洪泽                  
		dubAreaToBssMap.put("59", "8320829");
		//盱眙                  
		dubAreaToBssMap.put("57", "8320830");
		//金湖                  
		dubAreaToBssMap.put("56", "8320831");
		//连云港市                  
		dubAreaToBssMap.put("63", "8320700");
		//连云港市区                  
		dubAreaToBssMap.put("64", "8320701");
		//赣榆                  
		dubAreaToBssMap.put("65", "8320721");
		//东海                  
		dubAreaToBssMap.put("67", "8320722");
		//灌云                  
		dubAreaToBssMap.put("66", "8320723");
		//灌南                  
		dubAreaToBssMap.put("68", "8320724");
		//常州市                  
		dubAreaToBssMap.put("69", "8320400");
		//常州市区                  
		dubAreaToBssMap.put("70", "8320401");
		//溧阳                  
		dubAreaToBssMap.put("73", "8320481");
		//金坛                  
		dubAreaToBssMap.put("72", "8320482");
		//泰州市                  
		dubAreaToBssMap.put("79", "8321200");
		//泰州市区                  
		dubAreaToBssMap.put("80", "8321201");
		//兴化                  
		dubAreaToBssMap.put("83", "8321281");
		//靖江                  
		dubAreaToBssMap.put("78", "8321282");
		//泰兴                  
		dubAreaToBssMap.put("81", "8321283");
		//姜堰                  
		dubAreaToBssMap.put("82", "8321284");
		//宿豫                  
		dubAreaToBssMap.put("71", "8321311");
		//沭阳                  
		dubAreaToBssMap.put("87", "8321322");
		//泗阳                  
		dubAreaToBssMap.put("88", "8321323");
		//泗洪                  
		dubAreaToBssMap.put("86", "8321324");
		//宿迁市                  
		dubAreaToBssMap.put("84", "8321300");
		//宿迁市区                      
		dubAreaToBssMap.put("85", "8321301");


		LOGGER.debug("loading bssToDubAreaMap...");
		//bss区县与江苏区县对应关系
		//江宁        
		bssToDubAreaMap.put("8320115", "10");
		//六合        
		bssToDubAreaMap.put("8320116", "13");
		//南京市      
		bssToDubAreaMap.put("8320100", "3");
		//南京市区    
		bssToDubAreaMap.put("8320101", "9");
		//高淳        
		bssToDubAreaMap.put("8320125", "12");
		//江浦        
		bssToDubAreaMap.put("8320111", "14");
		//溧水        
		bssToDubAreaMap.put("8320124", "11");
		//无锡市      
		bssToDubAreaMap.put("8320200", "15");
		//无锡市区    
		bssToDubAreaMap.put("8320201", "16");
		//江阴        
		bssToDubAreaMap.put("8320281", "19");
		//宜兴        
		bssToDubAreaMap.put("8320282", "18");
		//镇江市      
		bssToDubAreaMap.put("8321100", "4");
		//镇江市区    
		bssToDubAreaMap.put("8321101", "8");
		//丹阳        
		bssToDubAreaMap.put("8321181", "5");
		//扬中        
		bssToDubAreaMap.put("8321182", "7");
		//句容        
		bssToDubAreaMap.put("8321183", "6");
		//苏州市      
		bssToDubAreaMap.put("8320500", "20");
		//苏州市区    
		bssToDubAreaMap.put("8320501", "21");
		//常熟        
		bssToDubAreaMap.put("8320581", "76");
		//张家港      
		bssToDubAreaMap.put("8320582", "77");
		//昆山        
		bssToDubAreaMap.put("8320583", "74");
		//吴江        
		bssToDubAreaMap.put("8320584", "23");
		//太仓        
		bssToDubAreaMap.put("8320585", "75");
		//海安        
		bssToDubAreaMap.put("8320621", "31");
		//如东        
		bssToDubAreaMap.put("8320623", "29");
		//启东        
		bssToDubAreaMap.put("8320681", "25");
		//如皋        
		bssToDubAreaMap.put("8320682", "30");
		//海门        
		bssToDubAreaMap.put("8320684", "24");
		//南通市      
		bssToDubAreaMap.put("8320600", "26");
		//南通市区    
		bssToDubAreaMap.put("8320601", "28");
		//通州        
		bssToDubAreaMap.put("8320612", "27");
		//宝应        
		bssToDubAreaMap.put("8321023", "38");
		//仪征        
		bssToDubAreaMap.put("8321081", "32");
		//高邮        
		bssToDubAreaMap.put("8321084", "37");
		//江都        
		bssToDubAreaMap.put("8321088", "36");//22胥浦没了，划到仪征
		//扬州市      
		bssToDubAreaMap.put("8321000", "33");
		//扬州市区    
		bssToDubAreaMap.put("8321001", "34");
		//邗江        
		bssToDubAreaMap.put("8321003", "35");
		//建湖        
		bssToDubAreaMap.put("8320925", "47");
		//东台        
		bssToDubAreaMap.put("8320981", "42");
		//大丰        
		bssToDubAreaMap.put("8320982", "41");
		//盐城市      
		bssToDubAreaMap.put("8320900", "39");
		//盐城市区    
		bssToDubAreaMap.put("8320901", "40");
		//响水        
		bssToDubAreaMap.put("8320921", "46");
		//滨海        
		bssToDubAreaMap.put("8320922", "45");
		//阜宁        
		bssToDubAreaMap.put("8320923", "44");
		//射阳        
		bssToDubAreaMap.put("8320924", "43");
		//徐州市      
		bssToDubAreaMap.put("8320300", "48");
		//徐州市区    
		bssToDubAreaMap.put("8320301", "49");
		//贾汪        
		bssToDubAreaMap.put("8320305", "90");
		//铜山        
		bssToDubAreaMap.put("8320312", "50");
		//丰县        
		bssToDubAreaMap.put("8320321", "55");
		//睢宁        
		bssToDubAreaMap.put("8320324", "51");
		//新沂        
		bssToDubAreaMap.put("8320381", "53");
		//邳州        
		bssToDubAreaMap.put("8320382", "52");
		//沛县        
		bssToDubAreaMap.put("8320322", "54");
		//淮安市      
		bssToDubAreaMap.put("8320800", "60");
		//淮安市区    
		bssToDubAreaMap.put("8320801", "61");
		//改为淮安区  
		bssToDubAreaMap.put("8320803", "89");
		//淮阴        
		bssToDubAreaMap.put("8320804", "58");
		//涟水        
		bssToDubAreaMap.put("8320826", "62");
		//洪泽        
		bssToDubAreaMap.put("8320829", "59");
		//盱眙        
		bssToDubAreaMap.put("8320830", "57");
		//金湖        
		bssToDubAreaMap.put("8320831", "56");
		//连云港市    
		bssToDubAreaMap.put("8320700", "63");
		//连云港市区  
		bssToDubAreaMap.put("8320701", "64");
		//赣榆        
		bssToDubAreaMap.put("8320721", "65");
		//东海        
		bssToDubAreaMap.put("8320722", "67");
		//灌云        
		bssToDubAreaMap.put("8320723", "66");
		//灌南        
		bssToDubAreaMap.put("8320724", "68");
		//常州市      
		bssToDubAreaMap.put("8320400", "69");
		//常州市区    
		bssToDubAreaMap.put("8320401", "70");
		//溧阳        
		bssToDubAreaMap.put("8320481", "73");
		//金坛        
		bssToDubAreaMap.put("8320482", "72");
		//泰州市      
		bssToDubAreaMap.put("8321200", "79");
		//泰州市区    
		bssToDubAreaMap.put("8321201", "80");
		//兴化        
		bssToDubAreaMap.put("8321281", "83");
		//靖江        
		bssToDubAreaMap.put("8321282", "78");
		//泰兴        
		bssToDubAreaMap.put("8321283", "81");
		//姜堰        
		bssToDubAreaMap.put("8321284", "82");
		//宿豫        
		bssToDubAreaMap.put("8321311", "71");
		//沭阳        
		bssToDubAreaMap.put("8321322", "87");
		//泗阳        
		bssToDubAreaMap.put("8321323", "88");
		//泗洪        
		bssToDubAreaMap.put("8321324", "86");
		//宿迁市      
		bssToDubAreaMap.put("8321300", "84");
		//宿迁市区    
		bssToDubAreaMap.put("8321301", "85");


		//bss区县与江苏区县中文名对应关系
		//江宁        
		bssToDubAreaNameMap.put("8320115", "江宁");
		//六合        
		bssToDubAreaNameMap.put("8320116", "六合");
		//南京市      
		bssToDubAreaNameMap.put("8320100", "南京市");
		//南京市区    
		bssToDubAreaNameMap.put("8320101", "南京市区");
		//高淳        
		bssToDubAreaNameMap.put("8320125", "高淳");
		//江浦        
		bssToDubAreaNameMap.put("8320111", "江浦");
		//溧水        
		bssToDubAreaNameMap.put("8320124", "溧水");
		//无锡市      
		bssToDubAreaNameMap.put("8320200", "无锡市");
		//无锡市区    
		bssToDubAreaNameMap.put("8320201", "无锡市区");
		//江阴        
		bssToDubAreaNameMap.put("8320281", "江阴");
		//宜兴        
		bssToDubAreaNameMap.put("8320282", "宜兴");
		//镇江市      
		bssToDubAreaNameMap.put("8321100", "镇江市");
		//镇江市区    
		bssToDubAreaNameMap.put("8321101", "镇江市区");
		//丹阳        
		bssToDubAreaNameMap.put("8321181", "丹阳");
		//扬中        
		bssToDubAreaNameMap.put("8321182", "扬中");
		//句容        
		bssToDubAreaNameMap.put("8321183", "句容");
		//苏州市      
		bssToDubAreaNameMap.put("8320500", "苏州市");
		//苏州市区    
		bssToDubAreaNameMap.put("8320501", "苏州市区");
		//常熟        
		bssToDubAreaNameMap.put("8320581", "常熟");
		//张家港      
		bssToDubAreaNameMap.put("8320582", "张家港");
		//昆山        
		bssToDubAreaNameMap.put("8320583", "昆山");
		//吴江        
		bssToDubAreaNameMap.put("8320584", "吴江");
		//太仓        
		bssToDubAreaNameMap.put("8320585", "太仓");
		//海安        
		bssToDubAreaNameMap.put("8320621", "海安");
		//如东        
		bssToDubAreaNameMap.put("8320623", "如东");
		//启东        
		bssToDubAreaNameMap.put("8320681", "启东");
		//如皋        
		bssToDubAreaNameMap.put("8320682", "如皋");
		//海门        
		bssToDubAreaNameMap.put("8320684", "海门");
		//南通市      
		bssToDubAreaNameMap.put("8320600", "南通市");
		//南通市区    
		bssToDubAreaNameMap.put("8320601", "南通市区");
		//通州        
		bssToDubAreaNameMap.put("8320612", "通州");
		//宝应        
		bssToDubAreaNameMap.put("8321023", "宝应");
		//仪征        
		bssToDubAreaNameMap.put("8321081", "仪征");
		//高邮        
		bssToDubAreaNameMap.put("8321084", "高邮");
		//江都        
		bssToDubAreaNameMap.put("8321088", "江都");
		//扬州市      
		bssToDubAreaNameMap.put("8321000", "扬州市");
		//扬州市区    
		bssToDubAreaNameMap.put("8321001", "扬州市区");
		//邗江        
		bssToDubAreaNameMap.put("8321003", "邗江");
		//建湖        
		bssToDubAreaNameMap.put("8320925", "建湖");
		//东台        
		bssToDubAreaNameMap.put("8320981", "东台");
		//大丰        
		bssToDubAreaNameMap.put("8320982", "大丰");
		//盐城市      
		bssToDubAreaNameMap.put("8320900", "盐城市");
		//盐城市区    
		bssToDubAreaNameMap.put("8320901", "盐城市区");
		//响水        
		bssToDubAreaNameMap.put("8320921", "响水");
		//滨海        
		bssToDubAreaNameMap.put("8320922", "滨海");
		//阜宁        
		bssToDubAreaNameMap.put("8320923", "阜宁");
		//射阳        
		bssToDubAreaNameMap.put("8320924", "射阳");
		//徐州市      
		bssToDubAreaNameMap.put("8320300", "徐州市");
		//徐州市区    
		bssToDubAreaNameMap.put("8320301", "徐州市区");
		//贾汪        
		bssToDubAreaNameMap.put("8320305", "贾汪");
		//铜山        
		bssToDubAreaNameMap.put("8320312", "铜山");
		//丰县        
		bssToDubAreaNameMap.put("8320321", "丰县");
		//睢宁        
		bssToDubAreaNameMap.put("8320324", "睢宁");
		//新沂        
		bssToDubAreaNameMap.put("8320381", "新沂");
		//邳州        
		bssToDubAreaNameMap.put("8320382", "邳州");
		//沛县        
		bssToDubAreaNameMap.put("8320322", "沛县");
		//淮安市      
		bssToDubAreaNameMap.put("8320800", "淮安市");
		//淮安市区    
		bssToDubAreaNameMap.put("8320801", "淮安市区");
		//改为淮安区  
		bssToDubAreaNameMap.put("8320803", "淮安区");
		//淮阴        
		bssToDubAreaNameMap.put("8320804", "淮阴");
		//涟水        
		bssToDubAreaNameMap.put("8320826", "涟水");
		//洪泽        
		bssToDubAreaNameMap.put("8320829", "洪泽");
		//盱眙        
		bssToDubAreaNameMap.put("8320830", "盱眙");
		//金湖        
		bssToDubAreaNameMap.put("8320831", "金湖");
		//连云港市    
		bssToDubAreaNameMap.put("8320700", "连云港市");
		//连云港市区  
		bssToDubAreaNameMap.put("8320701", "连云港市区");
		//赣榆        
		bssToDubAreaNameMap.put("8320721", "赣榆");
		//东海        
		bssToDubAreaNameMap.put("8320722", "东海");
		//灌云        
		bssToDubAreaNameMap.put("8320723", "灌云");
		//灌南        
		bssToDubAreaNameMap.put("8320724", "灌南");
		//常州市      
		bssToDubAreaNameMap.put("8320400", "常州市");
		//常州市区    
		bssToDubAreaNameMap.put("8320401", "常州市区");
		//溧阳        
		bssToDubAreaNameMap.put("8320481", "溧阳");
		//金坛        
		bssToDubAreaNameMap.put("8320482", "金坛");
		//泰州市      
		bssToDubAreaNameMap.put("8321200", "泰州市");
		//泰州市区    
		bssToDubAreaNameMap.put("8321201", "泰州市区");
		//兴化        
		bssToDubAreaNameMap.put("8321281", "兴化");
		//靖江        
		bssToDubAreaNameMap.put("8321282", "靖江");
		//泰兴        
		bssToDubAreaNameMap.put("8321283", "泰兴");
		//姜堰        
		bssToDubAreaNameMap.put("8321284", "姜堰");
		//宿豫        
		bssToDubAreaNameMap.put("8321311", "宿豫");
		//沭阳        
		bssToDubAreaNameMap.put("8321322", "沭阳");
		//泗阳        
		bssToDubAreaNameMap.put("8321323", "泗阳");
		//泗洪        
		bssToDubAreaNameMap.put("8321324", "泗洪");
		//宿迁市      
		bssToDubAreaNameMap.put("8321300", "宿迁市");
		//宿迁市区    
		bssToDubAreaNameMap.put("8321301", "宿迁市区");
		/*
		 * 产品规格
		 */
		prodIdCodeMap.put("2", "100000002");
		prodIdCodeMap.put("9", "100000009");
		prodIdCodeMap.put("379", "100000379");
		prodIdCodeMap.put("881", "100000881");
		/*
		 * 反向产品规格
		 */
		prodIdReverseMap.put("100000002", "2");
		prodIdReverseMap.put("100000009", "9");
		prodIdReverseMap.put("100000379", "379");
		prodIdReverseMap.put("100000881", "881");

		/*
		 * statucCd转化
		 */
		statucCdToNewMap.put("1101", "将失效");
		statucCdToNewMap.put("1000", "有效");
		statucCdToNewMap.put("1100", "无效");
		statucCdToNewMap.put("1500", "过渡期");
		statucCdToNewMap.put("1300", "已归档");
		statucCdToNewMap.put("1400", "异常失效");
		statucCdToNewMap.put("1200", "未生效");

		statucCdToOldMap.put("1100", "11");
		statucCdToOldMap.put("1000", "10");
		statucCdToOldMap.put("1200", "12");

		prodStatusNameMap.put("110000", "拆机");
		prodStatusNameMap.put("150000", "撤销");
		prodStatusNameMap.put("110001", "主动拆机");
		prodStatusNameMap.put("110002", "被动拆机");
		prodStatusNameMap.put("140002", "未激活");
		prodStatusNameMap.put("140001", "预开通");
		prodStatusNameMap.put("120000", "停机");
		prodStatusNameMap.put("140000", "未激活");
		prodStatusNameMap.put("100000", "在用");
		prodStatusNameMap.put("130000", "未竣工");

		paymentModeCdMap.put("1200", "1");
		paymentModeCdMap.put("2100", "2");

		paymentModeCdNameMap.put("1200", "后付费");
		paymentModeCdNameMap.put("2100", "预付费");

		//在用
		prodstatusCdMap.put("100000", "1");
		//停机
		prodstatusCdMap.put("120000", "2");
		//拆机
		prodstatusCdMap.put("110000", "3");
		//未竣工
		prodstatusCdMap.put("130000", "11");
		//未激活（预开通）
		prodstatusCdMap.put("140000", "19");
		//预开通
		prodstatusCdMap.put("140001", "19");
		//未激活
		prodstatusCdMap.put("140002", "19");

		//证件类型转换         智慧-》现网
		bssToIndentNbrTypeMap.put("39", "3");
		bssToIndentNbrTypeMap.put("99", "21");
		bssToIndentNbrTypeMap.put("2", "26");
		bssToIndentNbrTypeMap.put("1", "1");
		bssToIndentNbrTypeMap.put("2", "2");
		bssToIndentNbrTypeMap.put("3", "9");
		bssToIndentNbrTypeMap.put("4", "49");
		bssToIndentNbrTypeMap.put("5", "23");
		bssToIndentNbrTypeMap.put("6", "52");
		bssToIndentNbrTypeMap.put("7", "24");
		bssToIndentNbrTypeMap.put("9", "6");
		bssToIndentNbrTypeMap.put("10", "8");
		bssToIndentNbrTypeMap.put("11", "7");
		bssToIndentNbrTypeMap.put("12", "25");
		bssToIndentNbrTypeMap.put("13", "17");
		bssToIndentNbrTypeMap.put("14", "47");
		bssToIndentNbrTypeMap.put("15", "11");
		bssToIndentNbrTypeMap.put("17", "27");
		bssToIndentNbrTypeMap.put("18", "28");
		bssToIndentNbrTypeMap.put("22", "29");
		bssToIndentNbrTypeMap.put("23", "30");
		bssToIndentNbrTypeMap.put("24", "31");
		bssToIndentNbrTypeMap.put("25", "10");
		bssToIndentNbrTypeMap.put("26", "32");
		bssToIndentNbrTypeMap.put("27", "33");
		bssToIndentNbrTypeMap.put("28", "34");
		bssToIndentNbrTypeMap.put("29", "35");
		bssToIndentNbrTypeMap.put("30", "36");
		bssToIndentNbrTypeMap.put("31", "37");
		bssToIndentNbrTypeMap.put("32", "38");
		bssToIndentNbrTypeMap.put("33", "39");
		bssToIndentNbrTypeMap.put("34", "40");
		bssToIndentNbrTypeMap.put("35", "41");
		bssToIndentNbrTypeMap.put("36", "42");
		bssToIndentNbrTypeMap.put("37", "43");
		bssToIndentNbrTypeMap.put("38", "44");
		bssToIndentNbrTypeMap.put("39", "");
		bssToIndentNbrTypeMap.put("40", "");
		bssToIndentNbrTypeMap.put("41", "51");
		bssToIndentNbrTypeMap.put("42", "50");
		bssToIndentNbrTypeMap.put("43", "45");
		bssToIndentNbrTypeMap.put("45", "");
		bssToIndentNbrTypeMap.put("46", "");
		bssToIndentNbrTypeMap.put("47", "");
		bssToIndentNbrTypeMap.put("48", "");
		// bssToIndentNbrTypeMap.put("99", "21");

		// 老证件类型 --> bss证件类型
		indentNbrTypeToBssTypeMap.put("41","35");
		indentNbrTypeToBssTypeMap.put("27","17");
		indentNbrTypeToBssTypeMap.put("42","36");
		indentNbrTypeToBssTypeMap.put("28","18");
		indentNbrTypeToBssTypeMap.put("39","33");
		indentNbrTypeToBssTypeMap.put("11","15");
		indentNbrTypeToBssTypeMap.put("40","34");
		indentNbrTypeToBssTypeMap.put("17","13");
		indentNbrTypeToBssTypeMap.put("47","14");
		indentNbrTypeToBssTypeMap.put("43","37");
		indentNbrTypeToBssTypeMap.put("7","11");
		indentNbrTypeToBssTypeMap.put("44","38");
		indentNbrTypeToBssTypeMap.put("25","12");
		indentNbrTypeToBssTypeMap.put("45","43");
		indentNbrTypeToBssTypeMap.put("50","42");
		indentNbrTypeToBssTypeMap.put("51","41");
		indentNbrTypeToBssTypeMap.put("21","99");
		indentNbrTypeToBssTypeMap.put("29","22");
		indentNbrTypeToBssTypeMap.put("30","23");
		indentNbrTypeToBssTypeMap.put("31","24");
		indentNbrTypeToBssTypeMap.put("10","25");
		indentNbrTypeToBssTypeMap.put("32","26");
		indentNbrTypeToBssTypeMap.put("33","27");
		indentNbrTypeToBssTypeMap.put("34","28");
		indentNbrTypeToBssTypeMap.put("35","29");
		indentNbrTypeToBssTypeMap.put("9","3");
		indentNbrTypeToBssTypeMap.put("2","2");
		indentNbrTypeToBssTypeMap.put("8","10");
		indentNbrTypeToBssTypeMap.put("1","1");
		indentNbrTypeToBssTypeMap.put("36","30");
		indentNbrTypeToBssTypeMap.put("24","7");
		indentNbrTypeToBssTypeMap.put("52","6");
		indentNbrTypeToBssTypeMap.put("38","32");
		indentNbrTypeToBssTypeMap.put("23","5");
		indentNbrTypeToBssTypeMap.put("37","31");
		indentNbrTypeToBssTypeMap.put("49","4");
		indentNbrTypeToBssTypeMap.put("6","9");

		// 下行最高可达速率
		kdDownSpeedMap.put("26", "15M");
		kdDownSpeedMap.put("28", "500M");
		kdDownSpeedMap.put("29", "11M");
		kdDownSpeedMap.put("30", "13M");
		kdDownSpeedMap.put("31", "17M");
		kdDownSpeedMap.put("32", "19M");
		kdDownSpeedMap.put("33", "21M");
		kdDownSpeedMap.put("34", "22M");
		kdDownSpeedMap.put("35", "23M");
		kdDownSpeedMap.put("36", "26M");
		kdDownSpeedMap.put("37", "27M");
		kdDownSpeedMap.put("38", "28M");
		kdDownSpeedMap.put("39", "29M");
		kdDownSpeedMap.put("40", "31M");
		kdDownSpeedMap.put("41", "32M");
		kdDownSpeedMap.put("42", "33M");
		kdDownSpeedMap.put("43", "35M");
		kdDownSpeedMap.put("44", "36M");
		kdDownSpeedMap.put("45", "37M");
		kdDownSpeedMap.put("46", "38M");
		kdDownSpeedMap.put("47", "39M");
		kdDownSpeedMap.put("48", "40M");
		kdDownSpeedMap.put("49", "41M");
		kdDownSpeedMap.put("50", "42M");
		kdDownSpeedMap.put("51", "43M");
		kdDownSpeedMap.put("52", "44M");
		kdDownSpeedMap.put("53", "45M");
		kdDownSpeedMap.put("54", "46M");
		kdDownSpeedMap.put("55", "47M");
		kdDownSpeedMap.put("56", "48M");
		kdDownSpeedMap.put("57", "49M");
		kdDownSpeedMap.put("99", "9600B");
		kdDownSpeedMap.put("1", "3M");
		kdDownSpeedMap.put("2", "2M");
		kdDownSpeedMap.put("3", "1M");
		kdDownSpeedMap.put("4", "512K");
		kdDownSpeedMap.put("5", "5M");
		kdDownSpeedMap.put("6", "10M");
		kdDownSpeedMap.put("7", "4M");
		kdDownSpeedMap.put("8", "6M");
		kdDownSpeedMap.put("9", "8M");
		kdDownSpeedMap.put("10", "100M");
		kdDownSpeedMap.put("11", "7M");
		kdDownSpeedMap.put("12", "9M");
		kdDownSpeedMap.put("13", "128K");
		kdDownSpeedMap.put("14", "256K");
		kdDownSpeedMap.put("15", "12M");
		kdDownSpeedMap.put("16", "25M");
		kdDownSpeedMap.put("17", "50M");
		kdDownSpeedMap.put("18", "20M");
		kdDownSpeedMap.put("19", "34M");
		kdDownSpeedMap.put("20", "1000M");
		kdDownSpeedMap.put("21", "30M");
		kdDownSpeedMap.put("22", "200M");
		kdDownSpeedMap.put("23", "14M");
		kdDownSpeedMap.put("24", "16M");
		kdDownSpeedMap.put("25", "18M");
		kdDownSpeedMap.put("27", "24M");
		kdDownSpeedMap.put("67", "10G");
		kdDownSpeedMap.put("58", "55M");
		kdDownSpeedMap.put("59", "60M");
		kdDownSpeedMap.put("60", "65M");
		kdDownSpeedMap.put("61", "70M");
		kdDownSpeedMap.put("62", "75M");
		kdDownSpeedMap.put("63", "80M");
		kdDownSpeedMap.put("64", "85M");
		kdDownSpeedMap.put("65", "90M");
		kdDownSpeedMap.put("66", "95M");
		// 上行最高可达速率
		kdUpSpeedMap.put("1", "256K");
		kdUpSpeedMap.put("8", "3M");
		kdUpSpeedMap.put("9", "5M");
		kdUpSpeedMap.put("10", "10M");
		kdUpSpeedMap.put("11", "8M");
		kdUpSpeedMap.put("12", "100M");
		kdUpSpeedMap.put("13", "7M");
		kdUpSpeedMap.put("14", "9M");
		kdUpSpeedMap.put("15", "128K");
		kdUpSpeedMap.put("16", "12M");
		kdUpSpeedMap.put("17", "25M");
		kdUpSpeedMap.put("18", "50M");
		kdUpSpeedMap.put("19", "20M");
		kdUpSpeedMap.put("20", "34M");
		kdUpSpeedMap.put("21", "1000M");
		kdUpSpeedMap.put("22", "30M");
		kdUpSpeedMap.put("23", "200M");
		kdUpSpeedMap.put("24", "14M");
		kdUpSpeedMap.put("25", "16M");
		kdUpSpeedMap.put("5", "1M");
		kdUpSpeedMap.put("6", "4M");
		kdUpSpeedMap.put("7", "6M");
		kdUpSpeedMap.put("2", "384K");
		kdUpSpeedMap.put("26", "768K");
		kdUpSpeedMap.put("0", "512K");
		kdUpSpeedMap.put("3", "800K");
		kdUpSpeedMap.put("4", "2M");
		kdUpSpeedMap.put("27", "18M");
		kdUpSpeedMap.put("28", "24M");
		kdUpSpeedMap.put("100", "6M");


	}

	/**
	 * bss区县与江苏区县中文名对应关系
	 *
	 * @param code
	 * @return
	 */
	public static String getBssToDubAreaNameMap(String code) {
		return bssToDubAreaNameMap.get(code);
	}


	/**
	 * 根据地市区号获取二级地区编码
	 *
	 * @param areaCode
	 * @return
	 */
	public static String getRegionByAreaCode(String areaCode) {
		return areaCodeRegionRelMap.get(areaCode);
	}

	/**
	 * 根据二级地区编码获取地市区号
	 *
	 * @param region
	 * @return
	 */
	public static String getAreaCodeByRegion(String region) {
		return regionAreaCodeRelMap.get(region);
	}

	/**
	 * 查询产品规格
	 *
	 * @param code
	 * @return
	 */
	public static String getProdId(String code) {
		return prodIdCodeMap.get(code);
	}

	/**
	 * 反向查询产品规格
	 *
	 * @param code
	 * @return
	 */
	public static String getProdIdReverse(String code) {
		return prodIdReverseMap.get(code);
	}


	/**
	 * Description: 根据江苏区县编码获取bss区县编码<br>
	 *
	 * @param bssInnerAreaId
	 * @return <br>
	 * @author xuzhiyuan<br>
	 * @taskId <br>
	 */
	public static String getDubAreaToBss(String bssInnerAreaId) {
		return dubAreaToBssMap.get(bssInnerAreaId);
	}

	/**
	 * Description: 智慧证件类型对应现网证件类型<br>
	 *
	 * @param id
	 * @return <br>
	 * @author cx<br>
	 * @taskId <br>
	 */
	public static String getBssToIndentNbrTypeMap(String id) {
		return bssToIndentNbrTypeMap.get(id);
	}

	/**
	 * @param @param  bssInnerAreaId
	 * @param @return 设定文件
	 * @return String    返回类型
	 * @throws
	 * @Title: getDubAreaToBss
	 * @Description: TODO(根据bss区县编码获取areaId)
	 * @author fulei
	 */
	public static String getBssToDubAreaMap(String bssInnerAreaId) {
		return bssToDubAreaMap.get(bssInnerAreaId);
	}

	public static String getStatucCdToNewMap(String id) {
		return statucCdToNewMap.get(id);
	}

	public static String getStatucCdToOldMap(String id) {
		return statucCdToOldMap.get(id);
	}

	public static String getProdStatusNameMap(String id) {
		return prodStatusNameMap.get(id);
	}

	public static String getPaymentModeCdMap(String id) {
		return paymentModeCdMap.get(id);
	}

	public static String getPaymentModeCdNameMap(String id) {
		return paymentModeCdNameMap.get(id);
	}

	public static String getProdstatusCdMap(String id) {
		return prodstatusCdMap.get(id);
	}

	/**
	 * Description: 根据valueId获取上行最高可达速率<br>
	 *
	 * @param id
	 * @return <br>
	 * @author 刘添<br>
	 * @taskId <br>
	 */
	public static String getKdUpSpeedValue(String id) {
		return kdUpSpeedMap.get(id);
	}

	/**
	 * Description: 根据valueId获取下行最高可达速率<br>
	 *
	 * @param id
	 * @return <br>
	 * @author 刘添<br>
	 * @taskId <br>
	 */
	public static String getKdDownSpeedValue(String id) {
		return kdDownSpeedMap.get(id);
	}


	/**
	 * 根据老证件类型获取bss证件类型
	 * @param oldIndentNbrType 老证件类型
	 * @return bss证件类型
	 */
	public static String getBssIndentNbrType(String oldIndentNbrType) {
		return indentNbrTypeToBssTypeMap.get(oldIndentNbrType);
	}
	public static String getMNPCertType(String certType){
	    String mnpCertType = identNbrTypeToMNPTypeMap.get(certType);
	    if(StringUtils.isEmpty(mnpCertType)){
	        return "OTHERTYPE";
	    }else{
	        return mnpCertType;
	    }
	}


	public static void main(String[] args) {
		for (Map.Entry<String, String> entry : bssToIndentNbrTypeMap.entrySet()) {
			String key = entry.getKey();
			String value = entry.getValue();
			if (!StringUtils.isEmpty(key) && !StringUtils.isEmpty(value)) {
				System.out.println("indentNbrTypeToBssTypeMap.put(\"" + value + "\",\"" + key + "\");");
			}
		}
	}

}
