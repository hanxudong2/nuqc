package cn.js189.uqc.domain;

import com.alibaba.fastjson.JSONObject;

public class CreateFKFXDTFJOrder extends CreateFKFXDTFJOrderBase {

    //主卡
    private String masterAccNum;
    //副卡
    private String accNum;

    public CreateFKFXDTFJOrder(String regionId, String masterAccNum, String accNum) {
        super(regionId);
        this.masterAccNum = masterAccNum;
        this.accNum = accNum;
    }


    @Override
    protected JSONObject improveRequestObject(JSONObject requestObject) {
        requestObject.put("masterAccNum", masterAccNum);
        requestObject.put("accNum", accNum);
        requestObject.put("createOrgId", -10053);
        requestObject.put("createStaff", -10053);
        return requestObject;
    }

}
