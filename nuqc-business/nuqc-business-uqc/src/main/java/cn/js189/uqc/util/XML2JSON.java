package cn.js189.uqc.util;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.dom4j.*;

import java.util.List;

public class XML2JSON {
	public static final char UNDERLINE='_';
    /**
     * xml转json
     * 
     * @param xmlStr
     * @return
     * @throws DocumentException
     */
    public static JSONObject xml2Json(String xmlStr) throws DocumentException {
        Document doc = DocumentHelper.parseText(xmlStr);
        JSONObject json = new JSONObject();
        dom4j2Json(doc.getRootElement(), json);
        return json;
    }
    
    public static JSONObject xmlToJson(String xmlStr) throws DocumentException {
        Document doc = DocumentHelper.parseText(xmlStr);
        JSONObject json = new JSONObject();
        dom4jToJson(doc.getRootElement(), json);
        return json;
    }

    public static String underlineToCamel(String param){  
        if (param==null|| param.trim().isEmpty()){
            return "";  
        }  
        param = param.toLowerCase();
        int len=param.length();  
        StringBuilder sb=new StringBuilder(len);  
        for (int i = 0; i < len; i++) {  
            char c=param.charAt(i);  
            if (c==UNDERLINE){  
               if (++i<len){  
                   sb.append(Character.toUpperCase(param.charAt(i)));  
               }  
            }else{  
                sb.append(c);  
            }  
        }  
        return sb.toString();  
    }

    /**
     * xml转json
     * 
     * @param element
     * @param json
     */
    public static void dom4j2Json(Element element, JSONObject json) {
        // 如果是属性
        for (Object o : element.attributes()) {
            Attribute attr = (Attribute) o;
            if (!isEmpty(attr.getValue())) {
                json.put(underlineToCamel(attr.getName()), attr.getValue());
            }
        }
        List<Element> chdEl = element.elements();
        if (chdEl.isEmpty() && !isEmpty(element.getText())) {// 如果没有子元素,只有一个值
            json.put(underlineToCamel(element.getName()), element.getText());
        }
        for (Element e : chdEl) {// 有子元素
            if (!e.elements().isEmpty()) {// 子元素也有子元素
                JSONObject chdjson = new JSONObject();
                dom4j2Json(e, chdjson);
                Object o = json.get(underlineToCamel(e.getName()));
                if (o != null) {
                    JSONArray jsona = null;
                    if (o instanceof JSONObject) {// 如果此元素已存在,则转为jsonArray
                        JSONObject jsono = (JSONObject) o;
                        json.remove(underlineToCamel(e.getName()));
                        jsona = new JSONArray();
                        jsona.add(jsono);
                        jsona.add(chdjson);
                    }
                    if (o instanceof JSONArray) {
                        jsona = (JSONArray) o;
                        jsona.add(chdjson);
                    }
                    json.put(underlineToCamel(e.getName()), jsona);
                } else {
                    if (!chdjson.isEmpty()) {
                        json.put(underlineToCamel(e.getName()), chdjson);
                    }
                }

            } else {// 子元素没有子元素
                for (Object o : element.attributes()) {
                    Attribute attr = (Attribute) o;
                    if (!isEmpty(attr.getValue())) {
                        json.put(underlineToCamel(attr.getName()), attr.getValue());
                    }
                }
                if (!e.getText().isEmpty()) {
                    json.put(underlineToCamel(e.getName()), e.getText());
                }
            }
        }
    }
    
    
    public static void dom4jToJson(Element element, JSONObject json) {
        // 如果是属性
        for (Object o : element.attributes()) {
            Attribute attr = (Attribute) o;
            if (!isEmpty(attr.getValue())) {
                json.put(attr.getName(), attr.getValue());
            }
        }
        List<Element> chdEl = element.elements();
        if (chdEl.isEmpty() && !isEmpty(element.getText())) {// 如果没有子元素,只有一个值
            json.put(element.getName(), element.getText());
        }
        for (Element e : chdEl) {// 有子元素
            if (!e.elements().isEmpty()) {// 子元素也有子元素
                JSONObject chdjson = new JSONObject();
                dom4jToJson(e, chdjson);
                Object o = json.get(e.getName());
                if (o != null) {
                    JSONArray jsona = null;
                    if (o instanceof JSONObject) {// 如果此元素已存在,则转为jsonArray
                        JSONObject jsono = (JSONObject) o;
                        json.remove(e.getName());
                        jsona = new JSONArray();
                        jsona.add(jsono);
                        jsona.add(chdjson);
                    }
                    if (o instanceof JSONArray) {
                        jsona = (JSONArray) o;
                        jsona.add(chdjson);
                    }
                    json.put(e.getName(), jsona);
                } else {
                    if (!chdjson.isEmpty()) {
                        json.put(e.getName(), chdjson);
                    }
                }

            } else {// 子元素没有子元素
                for (Object o : element.attributes()) {
                    Attribute attr = (Attribute) o;
                    if (!isEmpty(attr.getValue())) {
                        json.put(attr.getName(), attr.getValue());
                    }
                }
                if (!e.getText().isEmpty()) {
                    json.put(e.getName(), e.getText());
                }
            }
        }
    }

    public static boolean isEmpty(String str) {

        if (str == null || str.trim().isEmpty() || "null".equals(str)) {
            return true;
        }
        return false;
    }
    public static void main(String[] args) {
//        String xmlStr = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><req><tel>18612341234</tel><timestamp>1514736000</timestamp><channel>WXTX</channel><signmsg>XXX</signmsg></req>";
////        String xmlStr ="<?xml version=\"1.0\" encoding=\"UTF-8\"?><rtn><code>0</code><msg>查询成功</msg><tel>18612341234</tel><channel>WXTX</channel><activitylist><activity><activityid>10001</activityid><name>活动1</name><begintime>1483200000</begintime><endtime>1514736000</endtime></activity><activity><activityid>10002</activityid><name>活动2</name><begintime>1483200000</begintime><endtime>1514736000</endtime></activity></activitylist></rtn>";
//        JSONObject json= new JSONObject();
//        try {
//            json = Utils.parseSoapXML(xmlStr, "req");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        System.out.println(json);
    }
    
    public static JSONObject xml2JsonNew(String xmlStr) throws DocumentException {
        Document doc = DocumentHelper.parseText(xmlStr);
        JSONObject json = new JSONObject();
        dom4j2JsonNew(doc.getRootElement(), json);
        return json;
    }
    
    public static void dom4j2JsonNew(Element element, JSONObject json) {
        // 如果是属性
        for (Object o : element.attributes()) {
            Attribute attr = (Attribute) o;
            if (!isEmpty(attr.getValue())) {
                json.put(underlineToCamelNew(attr.getName()), attr.getValue());
            }
        }
        List<Element> chdEl = element.elements();
        if (chdEl.isEmpty() && !isEmpty(element.getText())) {// 如果没有子元素,只有一个值
            json.put(underlineToCamelNew(element.getName()), element.getText());
        }
        for (Element e : chdEl) {// 有子元素
            if (!e.elements().isEmpty()) {// 子元素也有子元素
                JSONObject chdjson = new JSONObject();
                dom4j2JsonNew(e, chdjson);
                Object o = json.get(underlineToCamelNew(e.getName()));
                if (o != null) {
                    JSONArray jsona = null;
                    if (o instanceof JSONObject) {// 如果此元素已存在,则转为jsonArray
                        JSONObject jsono = (JSONObject) o;
                        json.remove(underlineToCamelNew(e.getName()));
                        jsona = new JSONArray();
                        jsona.add(jsono);
                        jsona.add(chdjson);
                    }
                    if (o instanceof JSONArray) {
                        jsona = (JSONArray) o;
                        jsona.add(chdjson);
                    }
                    json.put(underlineToCamelNew(e.getName()), jsona);
                } else {
                    if (!chdjson.isEmpty()) {
                        json.put(underlineToCamelNew(e.getName()), chdjson);
                    }
                }

            } else {// 子元素没有子元素
                for (Object o : element.attributes()) {
                    Attribute attr = (Attribute) o;
                    if (!isEmpty(attr.getValue())) {
                        json.put(underlineToCamelNew(attr.getName()), attr.getValue());
                    }
                }
                if (!e.getText().isEmpty()) {
                    json.put(underlineToCamelNew(e.getName()), e.getText());
                }
            }
        }
    }
    
    public static String underlineToCamelNew(String param){  
        if (param==null||"".equals(param.trim())){  
            return "";  
        }  
        int len=param.length();  
        StringBuilder sb=new StringBuilder(len);  
        for (int i = 0; i < len; i++) {  
            char c=param.charAt(i);  
            if (c==UNDERLINE){  
               if (++i<len){  
                   sb.append(Character.toUpperCase(param.charAt(i)));  
               }  
            }else{  
                sb.append(c);  
            }  
        }  
        return sb.toString();  
    }
}
