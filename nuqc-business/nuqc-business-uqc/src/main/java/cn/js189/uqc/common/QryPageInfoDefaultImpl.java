package cn.js189.uqc.common;

import cn.js189.uqc.service.IQryPageInfo;
import com.alibaba.fastjson.JSONObject;

/**
 * 默认的分页信息
 * 
 * @author Mid
 */
public class QryPageInfoDefaultImpl implements IQryPageInfo {
	
	/**
	 * 生成分页信息
	 */
	@Override
	public JSONObject generatePageInfos() {
		JSONObject pageInfos = new JSONObject();
		// 当前页
		pageInfos.put("pageIndex", 1);
		// 每页数量
		pageInfos.put("pageSize", 100);
		return pageInfos;
	}
}
