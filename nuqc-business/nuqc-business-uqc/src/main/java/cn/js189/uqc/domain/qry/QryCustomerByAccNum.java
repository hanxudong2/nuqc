package cn.js189.uqc.domain.qry;

import cn.js189.common.constants.Constants;
import cn.js189.common.enums.QryCustomerScopeInfoEnum;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 通过接入号的方式查询客户信息，生成请求报文
 * 
 * @author Mid
 */
public class QryCustomerByAccNum extends QryCustomerBase {
	// 日志组件
	private static final Logger LOGGER = LoggerFactory.getLogger(QryCustomerByAccNum.class);

	// 接入号码
	private String accNum;
	// 接入号码类型
	private String prodId;

	public QryCustomerByAccNum(String regionId, String uncompletedFlag, String accNum, String prodId) {
		super(regionId, uncompletedFlag);
		this.accNum = accNum;
		this.prodId = prodId;
	}

	@Override
	protected JSONObject improveRequestObject(JSONObject requestObject) {
		requestObject.put("accNum", accNum);
		requestObject.put("prodId", prodId);
		LOGGER.debug("requestObject:{}", requestObject.toString());
		return requestObject;
	}
}
