package cn.js189.uqc.controller.qx;

import cn.js189.uqc.service.IBssUserQueryService;
import cn.js189.uqc.service.IOssService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @program: xxb
 * @description: OSS相关接口
 * @author: jzd
 * @create: 2024/3/1 10:44
 **/
@RestController
@RequestMapping
public class OssController {

    @Autowired
    IOssService ossService;
    @Resource
    private IBssUserQueryService bssUserQueryService;

    /**
     * JSOSS政企门户业务进度透明化查询
     * @param requestJSON
     * @return
     */
    @PostMapping("/iOss/getCurrentTachInfoToCrm")
    public String getCurrentTachInfoToCrm(@RequestBody String requestJSON){
        return ossService.getCurrentTachInfoToCrm(requestJSON);
    }
    
    /**
     * 根据卡号查询用户号码接口
     * @param reqParams 参数
     * @return 结果
     */
    @PostMapping("/iOss/getCurrentTachInfoToCrm")
    public String getNumInfoByCardNo(@RequestBody String reqParams){
        return bssUserQueryService.getNumInfoByCardNo(reqParams);
    }
    
    /**
     * 智慧选址接口
     * @param reqParams 参数
     * @return 结果
     */
    @PostMapping("/iBssSystem/qryFullAddressFromOSS")
    public String qryFullAddressFromOSS(@RequestBody String reqParams){
       return bssUserQueryService.qryFullAddressFromOSS(reqParams);
    }
}
