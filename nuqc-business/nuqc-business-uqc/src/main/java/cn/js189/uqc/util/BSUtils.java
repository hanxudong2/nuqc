package cn.js189.uqc.util;

import cn.hutool.extra.spring.SpringUtil;
import cn.js189.common.constants.BSSUrlConstants;
import cn.js189.common.domain.ReqInfo;
import cn.js189.common.domain.RespInfo;
import cn.js189.common.util.Utils;
import cn.js189.uqc.transform.IBSResponseTransform;
import cn.js189.uqc.util.enums.BSResponseBusinessTypeEnum;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class BSUtils {
	
	private BSUtils(){/*empty*/}
	
	/**
	 * UQC: 区域编码查询 callNewAreaCode
	 * BS: 号码归属地查询 qryAreaCdByNbr
	 */
	public static String callNewAreaCodeNew(ReqInfo reqInfo) {
		
		RespInfo responseJson = new RespInfo();
		String tranId = "";
		
		try {
			tranId = reqInfo.getHead().getTranId();
			
			// 新区号查询入参与老入参body一致,直接用body当作入参,无需转换
			JSONObject body = reqInfo.getBody();
			
			String requestJson = body.toString();
			
			log.info("号码归属地查询 qryAreaCdByNbr 入参:{}", requestJson);
			String result = HttpClientHelperForEop.postToBill(BSSUrlConstants.QRY_AREA_CD_BY_NBR, requestJson, null);
			log.info("号码归属地查询 qryAreaCdByNbr 出参:{}", result);
			
			IBSResponseTransform transform = SpringUtil.getBean(BSResponseBusinessTypeEnum.getBeanName("callNewAreaCode"));
			return transform.transform(result, tranId).toString();
			
		} catch (Exception e) {
			log.error("号码归属地查询 qryAreaCdByNbr Exception: {}", e.getMessage());
			return Utils.dealExceptionResp(responseJson.getHead(), tranId, responseJson).toString();
		}
	}
	
	/**
	 * UQC: 10171长话话单信息 callNewCallTicket
	 * BS: 长途清单查询 TktGuCallTicketQr
	 */
	public static String callNewCallTicketNew(String url,ReqInfo reqInfo) {
		
		RespInfo respInfo = new RespInfo();
		String tranId = "";
		
		try {
			tranId = reqInfo.getHead().getTranId();
			
			String areaCode = reqInfo.getHead().getAreaCode();
			
			JSONObject body = reqInfo.getBody();
			
			log.info("长途清单查询 TktGuCallTicketQr 开始入参转换");
			String requestJson = getTktGuCallTicketQr(body, areaCode);
			
			log.info("长途清单查询 TktGuCallTicketQr 入参:{}", requestJson);
			String result = HttpClientHelperForEop.postToBill(url, requestJson, areaCode);
			log.info("长途清单查询 TktGuCallTicketQr 出参:{}", result);
			
			IBSResponseTransform transform = SpringUtil.getBean(BSResponseBusinessTypeEnum.getBeanName("callNewCallTicket"));
			return transform.transform(result, tranId).toString();
			
		} catch (Exception e) {
			log.error("长途清单查询 TktGuCallTicketQr Exception: {}", e.getMessage());
			return Utils.dealExceptionResp(respInfo.getHead(), tranId, respInfo).toString();
		}
	}
	
	/**
	 * 长途清单查询 TktGuCallTicketQr 入参转换
	 */
	private static String getTktGuCallTicketQr(JSONObject body, String areaCode) {
		String begDate = body.getString("begDate");
		String endDate = body.getString("endDate");
		String getFlag = DateUtils.isAllMonthFlag(begDate, endDate)?"1":"3";
		JSONObject requestObj = new JSONObject();
		requestObj.put("ACC_NBR", body.getString("accNbr"));
		requestObj.put("AREA_CODE", areaCode);
		requestObj.put("GET_FLAG", getFlag);
		requestObj.put("BEGIN_DATE", begDate);
		requestObj.put("END_DATE", endDate);
		requestObj.put("TICKET_ID", "0");
		requestObj.put("FAMILY_ID", body.getString("productId"));
		return requestObj.toString();
	}
	
}
