package cn.js189.uqc.mapper;

import cn.js189.common.domain.EuaAuthority;
import cn.js189.common.domain.EuaChannel;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SysEuaMapper {

	List<EuaAuthority> listAll(@Param("channelId") String channelId);
	
	Integer listCount();
	
	List<EuaChannel> listAllKey(@Param("channelId") String channelId);
	
}
