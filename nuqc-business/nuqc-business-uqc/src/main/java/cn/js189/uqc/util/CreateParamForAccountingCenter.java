package cn.js189.uqc.util;

import cn.js189.common.util.MainUtils;
import cn.js189.common.util.helper.DateHelper;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 计费对接账务中心出入参拼装
 *
 */
public class CreateParamForAccountingCenter {
	
	private CreateParamForAccountingCenter(){}
	
	/** 
	* 生成操作人属性
	*/ 
	public static JSONObject  createOperAttrStruct(String routeRuleValue){
		JSONObject operAttrStructObject = new JSONObject();
		operAttrStructObject.put("operOrgId", routeRuleValue);
		operAttrStructObject.put("staffId","10001");
		return operAttrStructObject;	
	}
	
	
	/** 
	* 生成服务对象条件属性
	*/ 
	public static JSONObject  createSvcObjectStruct(String dccAccNbr,String dccDestinationAttr){
		JSONObject svcObjectStruct = new JSONObject();
		//1-按账户范围 2-按用户范围 3-按客户范围 4 按销售品范围
		svcObjectStruct.put("dataArea", "1");
		
		//用户属性 0-固话; 2-移动；  3-宽带
		svcObjectStruct.put("objAttr",dccDestinationAttr);
		
		//1-账户标识 2-用户标识 3-用户号码 4-客户标识 5-销售品实例 
		svcObjectStruct.put("objType", "3");
		//如果是用户号码且用户号码属性为固话、宽带时，此值要求带区号，含0
		svcObjectStruct.put("objValue", dccAccNbr);
		return svcObjectStruct;	
	}
	
	
	/** 
	*  根据查询范围生成服务对象条件属性
	*/ 
	public static JSONObject  createSvcObjectStructByDataArea(String dccAccNbr,String dccDestinationAttr,String dataArea){
		JSONObject svcObjectStruct = new JSONObject();
		if("".equals(dataArea)|| null==dataArea){
			dataArea = "1";
		}
		
		//1-按账户范围 2-按用户范围 3-按客户范围 4 按销售品范围
		svcObjectStruct.put("dataArea", dataArea);
		
		//用户属性 0-固话; 2-移动；  3-宽带
		svcObjectStruct.put("objAttr",dccDestinationAttr);
		
		//1-账户标识 2-用户标识 3-用户号码 4-客户标识 5-销售品实例 
		svcObjectStruct.put("objType", "3");
		//如果是用户号码且用户号码属性为固话、宽带时，此值要求带区号，含0
		svcObjectStruct.put("objValue", dccAccNbr);
		return svcObjectStruct;	
	}
	
	/** 
	*  新业务清单入参拼接
	*/ 
	public static JSONObject  createParamForNewSvrTic(String accNbr, String getFlag ,String begDate,String  endDate,String productId){
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("ACC_NBR", accNbr);
		jsonObject.put("GET_FLAG", getFlag);
		jsonObject.put("BEGIN_DATE", begDate);
		jsonObject.put("END_DATE", endDate);
		jsonObject.put("TICKET_ID", "0");		
		jsonObject.put("FAMILY_ID", MainUtils.getNullEmptyForReq(productId));
		return jsonObject;
	}
	
	
	/** 
	* 短信通信清单入参拼接     参数与 C网短信_彩信增值详单_移动    短信订制清单  TKT_YI_UNI_MSG_TICKET_QR       其他增值业务清单查询<一致
	*/ 
	public static JSONObject  createParamForSmTicketQr(String accNbr, String getFlag ,String begDate,String  endDate,String familyId){
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("ACC_NBR", accNbr);
		jsonObject.put("GET_FLAG", getFlag);
		jsonObject.put("BEGIN_DATE", begDate);
		jsonObject.put("END_DATE", endDate);		
		jsonObject.put("FAMILY_ID", MainUtils.getNullEmptyForReq(familyId));
		return jsonObject;
	}
	
	
	/** 
	* 本金、赠款销账金额查询CUSTOMER_QRY(cust_use_qry) 入参拼接
	*/ 
	public static JSONObject  createParamForCustomerQry(String sAreaCode, String sAccNbr ,String lFamilyId,String  lBillingCycleId,String lGetFlag){
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("sAreaCode", sAreaCode);
		jsonObject.put("sAccNbr", sAccNbr);
		jsonObject.put("lGetFlag", lGetFlag);
		jsonObject.put("lBillingCycleId", lBillingCycleId);		
		jsonObject.put("lFamilyId", MainUtils.getNullEmptyForReq(lFamilyId));
		return jsonObject;
	}
	
	
	/**
	* 账户信息查询OTH_GET_ACCT（OthGetAcct）
	*/ 
	public static JSONObject  createParamForOthGetAcct(String sAreaCode, String sAccNbr ,String lFamilyId,String sAccNbr97  ,String sRemoteSourceID,String sRemoteSourcePwd){
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("sAreaCode", sAreaCode);
		jsonObject.put("sAccNbr", sAccNbr);
		jsonObject.put("sAccNbr97", sAccNbr97);
		jsonObject.put("sRemoteSourceID", sRemoteSourceID);	
		jsonObject.put("sRemoteSourcePwd", sRemoteSourcePwd);		
		jsonObject.put("lFamilyId", MainUtils.getNullEmptyForReq(lFamilyId));
		return jsonObject;
	}
	
	
	/** 
	* 8 宽带清单查询 TKT_GU_BROAD_DATA_QR
	*/ 
	public static String  createParamForTktGuBroadDataQr(String param){
		JSONObject requestParam = JSON.parseObject(param);
		String begDate = requestParam.getString("begDate");
		String endDate = requestParam.getString("endDate");
		String getFlag = DateHelper.isAllMonthFlag(begDate, endDate)?"1":"3";
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("ACC_NBR",  requestParam.getString("accNbr"));
		jsonObject.put("ACCT_NAME", "");
		jsonObject.put("GET_FLAG", getFlag);
		jsonObject.put("BEGIN_DATE", begDate);
		jsonObject.put("END_DATE", endDate);
		jsonObject.put("TICKET_ID", "0");
		jsonObject.put("FAMILY_ID", requestParam.getString("productId"));
		
		return jsonObject.toJSONString();
	}
	
	
	/** 
	* 语音详单查询 TKT_YI_VOICE_TICKET_QR
	*/ 
	public static JSONObject  createParamForTktYiVoiceTicketQr(String accNbr, String getFlag ,String begDate,String  endDate){
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("ACC_NBR", accNbr);
		jsonObject.put("GET_FLAG", getFlag);
		jsonObject.put("BEGIN_DATE", begDate);
		jsonObject.put("END_DATE", endDate);	
        //原查询域接口callNewVoiceTicket 为手机语音清单查询，故此处产品类别为2 移动号码，后改为-1，不限产品规格
		jsonObject.put("DESTINATION_ATTR", "-1");
		return jsonObject;
	}
	
	
	/** 
	* 账单查询入参校验
	*/ 
	public static JSONObject  createParamForOthGetOwe(String sAreaCode, String sAccNbr,String lGetFlag,String sRemoteSourceID,String sRemoteSourcePwd){
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("sAreaCode", sAreaCode);
		jsonObject.put("sAccNbr", sAccNbr);
		jsonObject.put("sRemoteSourceID", sRemoteSourceID);	
		jsonObject.put("sRemoteSourcePwd", sRemoteSourcePwd);		
		jsonObject.put("lGetFlag", lGetFlag);
		return jsonObject;
	}
	
	public static JSONObject  createParamForQryCustBill(String dccAccNbr, String dccDestinationAttr ,String dccBillingCycle,String  dccQueryFlag,String routeRuleValue){
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("accNbr", dccAccNbr);
		jsonObject.put("destinationAttr", dccDestinationAttr);
		jsonObject.put("billingCycle", dccBillingCycle);
		jsonObject.put("queryFlag", dccQueryFlag);	
		
		//操作人属性
		jsonObject.put("operAttrStruct", createOperAttrStruct(routeRuleValue));
		return jsonObject;
	}
	
	/** 
	* 缴费销账记录查询U_GET_PAY_BILL   余额变动明细查询API
	*/ 
	public static JSONObject  createParamForQryPayment(String dccAccNbr, String dccDestinationAttr ,String dccBillingCycle,String routeRuleValue,String dataArea){
		JSONObject jsonObject = new JSONObject();	
		jsonObject.put("billingCycleId", dccBillingCycle);
		//操作人属性
		jsonObject.put("operAttrStruct", createOperAttrStruct(routeRuleValue));
		jsonObject.put("svcObjectStruct", createSvcObjectStructByDataArea(dccAccNbr, dccDestinationAttr,dataArea));
		
		return jsonObject;
	}
	
	public static JSONObject  createParamForQryCredit(String dccAccNbr, String dccDestinationAttr ,String routeRuleValue){
        JSONObject jsonObject = new JSONObject();   
        //操作人属性
        jsonObject.put("operAttrStruct", createOperAttrStruct(routeRuleValue));
        jsonObject.put("accNbr", dccAccNbr);
        jsonObject.put("destinationAttr", dccDestinationAttr);
        return jsonObject;
    }
	
	public static JSONObject  createParamForQryBillItems(String dccAccNbr, String dccDestinationAttr ,String dccBillingCycle,String routeRuleValue,String dataArea,String startDate,String endDate,String qryType,String page,String row){
        JSONObject jsonObject = new JSONObject();   
        jsonObject.put("billingCycleId", dccBillingCycle);
        jsonObject.put("startDate", startDate);
        jsonObject.put("endDate", endDate);
        jsonObject.put("qryType", qryType);
        jsonObject.put("page", page);
        jsonObject.put("row", row);
        jsonObject.put("custId", "");
        jsonObject.put("effDate ", "");
        jsonObject.put("expDate ", "");
        jsonObject.put("provinceCode", "025");
        String nowMonth = new SimpleDateFormat("yyyyMM").format(new Date());
        if(nowMonth.equals(dccBillingCycle)){//本月
            jsonObject.put("realTimeFlag", "1");
        }else{//历史
            jsonObject.put("realTimeFlag", "2");
        }
        //操作人属性
        jsonObject.put("operAttrStruct", createOperAttrStruct(routeRuleValue));
        jsonObject.put("svcObjectStruct", createSvcObjectStructByDataArea(dccAccNbr, dccDestinationAttr,dataArea));
        
        return jsonObject;
    }
	
	public static JSONObject  createParamForQryAccuUse(String dccAccNbr, String dccDestinationAttr ,String routeRuleValue,String billingCycle){
        JSONObject jsonObject = new JSONObject();   
        //操作人属性
        jsonObject.put("operAttrStruct", createOperAttrStruct(routeRuleValue));
        jsonObject.put("accNbr", dccAccNbr);
        jsonObject.put("destinationAttr", dccDestinationAttr);
        jsonObject.put("billingCycle", billingCycle);
        return jsonObject;
    }
	
	/** 
	* 余额变动汇总查询U_GET_EX_BAL
	*/ 
	public static JSONObject  createParamForQryBalanceRecord(String dccAccNbr, String dccDestinationAttr ,String dccBillingCycle,String routeRuleValue,String balanceTypeFlag){
		JSONObject jsonObject = new JSONObject();	
		jsonObject.put("billingCycleId", dccBillingCycle);
		jsonObject.put("balanceTypeFlag ", balanceTypeFlag);
		//操作人属性
		String dataArea = "1";
		jsonObject.put("operAttrStruct", createOperAttrStruct(routeRuleValue));
		jsonObject.put("svcObjectStruct", createSvcObjectStructByDataArea(dccAccNbr, dccDestinationAttr,dataArea));
		
		return jsonObject;
	}
	
	/** 
	* 账户欠费查询
	*/ 
	public static JSONObject  createParamForQryBill(String billQueryType, String destinationAccount ,String destinationAttr,String queryFlag,String feeQueryFlag,String routeRuleValue){
		JSONObject jsonObject = new JSONObject();	
		jsonObject.put("billQueryType", billQueryType);
		jsonObject.put("destinationAccount", destinationAccount);
		jsonObject.put("destinationAttr", destinationAttr);
		jsonObject.put("queryFlag", queryFlag);
		jsonObject.put("feeQueryFlag", feeQueryFlag);
		//操作人属性
		
		jsonObject.put("operAttrStruct", createOperAttrStruct(routeRuleValue));
		
		return jsonObject;
	}
	
	
	/**
	 * 生成只有 operAttrStruct  svcObjectStruct的请求参数
	*/ 
	public static JSONObject  createParamForBasicStruct(String dccAccNbr, String dccDestinationAttr ,String routeRuleValue,String dataArea ){
		JSONObject jsonObject = new JSONObject();	
		//操作人属性
		jsonObject.put("operAttrStruct", createOperAttrStruct(routeRuleValue));
		jsonObject.put("svcObjectStruct", createSvcObjectStructByDataArea(dccAccNbr, dccDestinationAttr,dataArea));
		
		return jsonObject;
	}
	
	/** 
	* 33.话费返还记录明细查询U_GET_BAL_RTN_D
	*/ 
	public static JSONObject  createParamForQryReturnBalanceInfoDetail(String dccAccNbr, String dccDestinationAttr ,String routeRuleValue,String returnPlanId,String dataArea ){
		JSONObject jsonObject = new JSONObject();	
		jsonObject.put("returnPlanId", returnPlanId);
		//操作人属性
		jsonObject.put("operAttrStruct", createOperAttrStruct(routeRuleValue));
		jsonObject.put("svcObjectStruct", createSvcObjectStructByDataArea(dccAccNbr, dccDestinationAttr,dataArea));
		
		return jsonObject;
	}
	
	public static JSONObject  createParamForOthGetServType(String sRemoteSourceID, String sRemoteSourcePwd ,String lFamilyId,String sAreaCode,String sAccNbr){
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("sAreaCode", sAreaCode);
		jsonObject.put("sAccNbr", sAccNbr);
		jsonObject.put("sRemoteSourceID", sRemoteSourceID);	
		jsonObject.put("sRemoteSourcePwd", sRemoteSourcePwd);		
		jsonObject.put("lFamilyId", lFamilyId);
		return jsonObject;
	}
	
	
	/** 
	* WLAN清单查询
	*/ 
	public static JSONObject  createParamForTktGuWlanTicketQr(String param){
		JSONObject requestParam = JSON.parseObject(param);
		String begDate = requestParam.getString("begDate");
		String endDate = requestParam.getString("endDate");
		String getFlag = DateHelper.isAllMonthFlag(begDate, endDate)?"1":"3";
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("ACC_NBR",  requestParam.getString("accNbr"));
		jsonObject.put("ACCT_NAME", requestParam.getString("accName"));
		jsonObject.put("GET_FLAG", getFlag);
		jsonObject.put("BEGIN_DATE", begDate);
		jsonObject.put("END_DATE", endDate);
		jsonObject.put("TICKET_ID", "0");
		jsonObject.put("FAMILY_ID", MainUtils.getNullEmptyForReq(requestParam.getString("productId")));
		
		return jsonObject;
	}
	
}
