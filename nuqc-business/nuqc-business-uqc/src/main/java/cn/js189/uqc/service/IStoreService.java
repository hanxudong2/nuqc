package cn.js189.uqc.service;

public interface IStoreService {
	
	/**
	 * http://openapi.telecomjs.com:80/eop/xsqd/getStoreMsg/getStoreMsg
	 * 营业厅门店ID与BSS渠道ID对应关系
	 * @param reqParams 参数
	 * @return 结果
	 */
	String getStoreMsg(String reqParams);
	
}
