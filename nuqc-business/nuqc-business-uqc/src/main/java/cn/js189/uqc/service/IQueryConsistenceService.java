package cn.js189.uqc.service;

import com.alibaba.fastjson.JSONObject;

/**
 * @Author yxl
 * @Date 2024/3/6
 */

public interface IQueryConsistenceService {
    JSONObject callAcctItemConsumqr(JSONObject requestJSON);

    JSONObject callConQueryBalance(JSONObject requestJSON);

    JSONObject callQueryOwe(JSONObject requestJSON);

    JSONObject callUgetBalChg(JSONObject requestJSON);

    JSONObject callUgetBalChgDetail(JSONObject requestJSON);

    JSONObject callUgetBalRtn(JSONObject requestJSON);

    JSONObject callUgetBalRtnDetail(JSONObject requestJSON);

    JSONObject callUgetPayment(JSONObject requestJSON);

    String queryConsistenceNew(String requestJson);

}
