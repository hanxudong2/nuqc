package cn.js189.uqc.service;

import com.alibaba.fastjson.JSONObject;

/**
 * @program: xxb
 * @description:
 * @author: jzd
 * @create: 2024/3/4 16:43
 **/
public interface ISecureGuardian {

    /**
     * Description:uuid=3.1 查询订购信息 <br>
     *
     * @author shenfan<br>
     * @taskId <br>
     * @param requestJSON 入参
     * @return <br>
     */
    JSONObject queryOrderInfo(JSONObject requestJSON);

    /**
     * Description:uuid=3.3 绑定申请，解绑 <br>
     *
     * @author shenfan<br>
     * @taskId <br>
     * @param requestJSON 入参
     * @return <br>
     */
    JSONObject bindAndUnbund(JSONObject requestJSON);

    /**
     * Description:uuid=3.6 上网管理开关设置 <br>
     *
     * @author shenfan<br>
     * @taskId <br>
     * @param requestJSON 入参
     * @return <br>
     */
    JSONObject onlineManageSetup(JSONObject requestJSON);

    /**
     * Description:uuid=3.7 上网管理开关查询 <br>
     *
     * @author shenfan<br>
     * @taskId <br>
     * @param requestJSON 入参
     * @return <br>
     */
    JSONObject qryOnlineSetupState(JSONObject requestJSON);

    /**
     * Description:uuid=3.8 阻断上网时间段设置 <br>
     *
     * @author shenfan<br>
     * @taskId <br>
     * @param requestJSON 入参
     * @return <br>
     */
    JSONObject blkOnlineTimeSetup(JSONObject requestJSON);

    /**
     * Description:uuid=3.9 阻断上网时间段查询 <br>
     *
     * @author shenfan<br>
     * @taskId <br>
     * @param requestJSON 入参
     * @return <br>
     */
    JSONObject qryBlkOnlineTimeSetup(JSONObject requestJSON);


    /**
     * Description:uuid=3.16 防护日志查询 <br>
     *
     * @author shenfan<br>
     * @taskId <br>
     * @param requestJSON 入参
     * @return <br>
     */
    JSONObject queryGuardLog(JSONObject requestJSON);
}
