package cn.js189.uqc.domain.qry;

import com.alibaba.fastjson.JSONObject;


public class QrySendTJMessageResult extends QrySendTJMessageResultBase {

    private String accNum;
    private String regionId;

    public QrySendTJMessageResult(String regionId, String accNum) {
        super(regionId);
        this.accNum = accNum;
    }


    protected JSONObject improveRequestObject(JSONObject requestObject) {
        requestObject.put("accNum", accNum);
        requestObject.put("lanId", regionId);
        return requestObject;
    }

}
