package cn.js189.uqc.domain.qry;

import cn.js189.uqc.common.RequestParamBase;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class QryOrderByTypeBase extends RequestParamBase {

    // 日志组件
    private static final Logger LOGGER = LoggerFactory.getLogger(QryOrderByTypeBase.class);

    // svcCode 提供的API接口名称
    public static final String BSS_TCP_CONT_SVC_CODE = "js.intf.qryCustomerOrderByTypeOld";

    // appKey
//    public static final String BSS_TCP_CONT_APP_KEY = "JS_DEFAULT";//JS00000001,80003,JS_DEFAULT,DEFAULT_TEST

    // 地区标识
    protected String regionId;

    /**
     * 查询产品信息构造器
     *
     * @param regionId:地区标识
     */
    public QryOrderByTypeBase(String regionId) {
        super();
        this.regionId = regionId;
    }

    @Override
    protected JSONObject generateTcpCont() {
        return super.generateTcpCont(BSS_TCP_CONT_APP_KEY, BSS_TCP_CONT_SVC_CODE, null);
    }

    @Override
    protected JSONObject generateSvcCont() {
        JSONObject authenticationInfo = generateAuthenticationInfo();
        if (null == authenticationInfo) {
            LOGGER.error("generate authenticationInfo failure");
            return null;
        }

        JSONObject requestObject = generateRequestObject();
        if (null == requestObject) {
            LOGGER.error("generate requestObject failure");
            return null;
        }

        JSONObject svcCont = new JSONObject();
        svcCont.put("authenticationInfo", authenticationInfo);
        svcCont.put("requestObject", requestObject);
        return svcCont;
    }

    /**
     * 生成 svcCont 下的认证信息
     *
     * @return
     */
    protected JSONObject generateAuthenticationInfo() {
        return super.generateSvcContAuthenticationInfo(regionId, null, null, null);
    }

    /**
     * 生成 svcCont 下的请求对象
     *
     * @return
     */
    protected JSONObject generateRequestObject() {
        JSONObject requestObject = new JSONObject();
        requestObject.put("regionId", regionId);
        return improveRequestObject(requestObject);
    }

    /**
     * 补齐 svcCont 下的请求对象
     *
     * @param requestObject:svcCont下的请求对象
     * @return
     */
    protected abstract JSONObject improveRequestObject(JSONObject requestObject);

    /**
     * 生成 svcCont 下的认证信息
     *
     * @param regionId:地区标识
     * @param staffId:营业员标识
     * @param staffCode:营业员工号
     * @param channelId:渠道标识
     * @return
     */
    protected JSONObject generateSvcContAuthenticationInfo(String regionId, String staffId, String staffCode,
                                                           String channelId) {
        if (StringUtils.isBlank(regionId)) {
            LOGGER.error("regionId is blank");
            return null;
        }

        JSONObject authenticationInfo = new JSONObject();
        authenticationInfo.put("regionId", regionId);
        if (StringUtils.isNotBlank(staffId)) {
            authenticationInfo.put("staffId", staffId);
        }
        if (StringUtils.isNotBlank(staffCode)) {
            authenticationInfo.put("staffCode", staffCode);
        }
        if (StringUtils.isNotBlank(channelId)) {
            authenticationInfo.put("channelId", channelId);
        }
        return authenticationInfo;
    }
}
