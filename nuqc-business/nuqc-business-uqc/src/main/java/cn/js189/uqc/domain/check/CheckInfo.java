package cn.js189.uqc.domain.check;

import java.util.Date;

public class CheckInfo {

    // 身份证号
    private String id;

    // 姓名
    private String name;

    // 是否需要照片
    private String needPhoto;

    // 请求结果code
    private String resultCode;

    private String channelId;

    // eop入参
    private String toEopParam;

    // eop出参
    private String fromEopParam;

    // 请求时间
    private Date gmtCreate;

    // 校验标记
    private String checkFlag;

    private Date gmtUpdate;

    public CheckInfo(String id, String name, String needPhoto, String resultCode, String channelId, Date gmtCreate, Date gmtUpdate, String checkFlag) {
        this.id = id;
        this.name = name;
        this.needPhoto = needPhoto;
        this.resultCode = resultCode;
        this.channelId = channelId;
        this.gmtCreate = gmtCreate;
        this.checkFlag = checkFlag;
        this.gmtUpdate = gmtUpdate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNeedPhoto() {
        return needPhoto;
    }

    public void setNeedPhoto(String needPhoto) {
        this.needPhoto = needPhoto;
    }

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public String getToEopParam() {
        return toEopParam;
    }

    public void setToEopParam(String toEopParam) {
        this.toEopParam = toEopParam;
    }

    public String getFromEopParam() {
        return fromEopParam;
    }

    public void setFromEopParam(String fromEopParam) {
        this.fromEopParam = fromEopParam;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getCheckFlag() {
        return checkFlag;
    }

    public void setCheckFlag(String checkFlag) {
        this.checkFlag = checkFlag;
    }

    public Date getGmtUpdate() {
        return gmtUpdate;
    }

    public void setGmtUpdate(Date gmtUpdate) {
        this.gmtUpdate = gmtUpdate;
    }

    @Override
    public String toString() {
        return "CheckInfo{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", needPhoto='" + needPhoto + '\'' +
                ", resultCode='" + resultCode + '\'' +
                ", channelId='" + channelId + '\'' +
                ", toEopParam='" + toEopParam + '\'' +
                ", fromEopParam='" + fromEopParam + '\'' +
                ", gmtCreate=" + gmtCreate +
                ", checkFlag='" + checkFlag + '\'' +
                ", gmtUpdate=" + gmtUpdate +
                '}';
    }
}
