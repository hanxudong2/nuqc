package cn.js189.uqc.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.js189.common.domain.ReqInfo;
import cn.js189.common.domain.RespHead;
import cn.js189.common.domain.RespInfo;
import cn.js189.common.domain.ResponseJson;
import cn.js189.common.util.HttpUtil;
import cn.js189.common.util.Utils;
import cn.js189.common.util.exception.BaseAppException;
import cn.js189.uqc.service.IQueryStatusService;
import cn.js189.uqc.util.StaticDataMappingUtil;
import cn.js189.uqc.util.XML2JSON;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * @Author yxl
 * @Date 2024/3/7
 */
@Service
public class IQueryStatusServiceImpl implements IQueryStatusService {

    private static final Logger logger = LoggerFactory.getLogger(IQueryStatusServiceImpl.class);

    @Override
    public String qryStaffLocation(String requestJSON) {
        logger.debug("查询装维最新经纬度入参: {}", requestJSON);
        JSONObject response = null;
        RespInfo responseJson = new RespInfo();
        RespHead responseHead = new RespHead();
        responseJson.setHead(responseHead);
        String tranId = "";
        try{
            ReqInfo reqInfo = BeanUtil.toBean(requestJSON,ReqInfo.class);
            JSONObject requestJson = JSON.parseObject(requestJSON);
            JSONObject head = requestJson.getJSONObject("head");// head体
            tranId = head.getString("tranId");
            String areaCode = head.getString("areaCode");
            JSONObject body = reqInfo.getBody();
            String zdNo = body.getString("zdNo");
            String areaId = StaticDataMappingUtil.getBssToDubAreaMap(StaticDataMappingUtil.getRegionByAreaCode(areaCode));
            //新地址
            String url = "http://132.254.19.12/zwqd_platform/services/RescheduleWebService";
            String requestXml="<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:ser='http://service.reschedule.graborders.com'>"+
                    "<soapenv:Header/>"+
                    "<soapenv:Body>"+
                    "<ser:qryStaffLocation>"+
                    "<ser:in0>"+
                    "<![CDATA[<?xml version='1.0' encoding='UTF-8'?>"+
                    "<root>"+
                    "<msgHead>"+
                    "<target>AYW@js.cn</target>"+
                    "</msgHead>"+
                    "<msgBody>"+
                    "<zdNo>"+zdNo+"</zdNo>"+
                    "<areaId>"+areaId+"</areaId>"+
                    "</msgBody>"+
                    "</root>]]>"+
                    "</ser:in0>"+
                    "</ser:qryStaffLocation>"+
                    "</soapenv:Body>"+
                    "</soapenv:Envelope>";
            int timeout = 40000;
            String switchStr = "N";
            String encodeStr = "utf-8";
            logger.info("调用oss装维最新经纬度url,{}",url);
            logger.info("调用oss装维最新经纬度入参,{}",requestXml);
            String xmlStr = HttpUtil.postWebServiceHttpProxy(url,requestXml,timeout,switchStr,encodeStr);
            logger.info("调用oss装维最新经纬度出参,{}",xmlStr);
            if(StringUtils.isNotEmpty(xmlStr)){
                String result = XML2JSON.xmlToJson(xmlStr).toString();
                String out = JSON.parseObject(result).getJSONObject("Body").getJSONObject("qryStaffLocationResponse").getString("out");
                String jsonString = XML2JSON.xmlToJson(out).toString();
                JSONObject res = JSON.parseObject(jsonString).getJSONObject("msgBody");
                if("000".equals(res.getString("resultCode"))){
                    res.put("resultCode","0");
                }
                response = Utils.builderResponse(res.toString(), responseHead, responseJson, tranId,"resultCode","","err");
            }else {
                response = Utils.dealExceptionResp(responseHead, tranId, responseJson);
            }
            return response.toString();
        } catch (Exception e) {
            response = Utils.dealExceptionResp(responseHead, tranId, responseJson);
            logger.error("查询装维最新经纬度 Exception :{}",e.getMessage());
            return response.toString();
        }
    }
}
