package cn.js189.uqc.util;

import cn.hutool.core.text.CharSequenceUtil;
import cn.hutool.extra.spring.SpringUtil;
import cn.js189.common.constants.*;
import cn.js189.common.domain.ReqHead;
import cn.js189.common.domain.RespHead;
import cn.js189.common.domain.RespInfo;
import cn.js189.common.enums.*;
import cn.js189.common.util.Utils;
import cn.js189.uqc.common.*;
import cn.js189.uqc.domain.*;
import cn.js189.uqc.domain.check.CheckGovCustomer;
import cn.js189.uqc.domain.check.CheckGovCustomerByAccNum;
import cn.js189.uqc.domain.qry.*;
import cn.js189.uqc.domain.req.AuthCodeReqBase;
import cn.js189.uqc.domain.req.RemoveOutAbilityReqBase;
import cn.js189.uqc.mapper.MnpCheckMapper;
import cn.js189.uqc.redis.RedisOperation;
import cn.js189.uqc.transform.IResponseParamTransform;
import cn.js189.uqc.transform.IResponseParamTransformNew;
import cn.js189.uqc.util.enums.QryAccountScopeInfoEnum;
import cn.js189.uqc.util.enums.QryProdInstScopeInfoEnum;
import cn.js189.uqc.util.thread.SendMnpAlmThread;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.*;

@Component
public class SmartBssSystemUtil {
	// 日志组件
	private static final Logger LOGGER = LoggerFactory.getLogger(SmartBssSystemUtil.class);
	// 企信新接口测试地址
	public static final String NEW_QIXIN_URL = "http://132.252.4.57:9088/saop-service/exchangeService";
	
	//eop密钥
	public static final String EOP_KEY = "aiweb123";
	public static final String CEILING_CODE = "11302";
	
	@Resource
	private RedisOperation redisOperation;
	@Resource
	private MnpCheckMapper mnpCheckMapper;
	private static final String DXURL = "https://jsdt3.telecomjs.com:8083/jsdt_services/IUVCService/transferNetwork";
	
	
	/**
	 * 智慧BSS,actionCode=61011,根据证件号码查询客户信息
	 *
	 * @param head
	 * @param body
	 * @return
	 */
	public String queryCustomerById(JSONObject head, JSONObject body) {
		
		
		try {
			// 映射企信测地市
			String areaCode = head.getString("areaCode");
			//		String regionId = StaticDataMappingUtil.getRegionByAreaCode(head.getString("areaCode"))
			String regionId2 = StaticDataMappingUtil.getDubAreaToBss(body.getJSONObject("reqMsg").getString("bssInnerAreaId"));
			// 身份证号码
			String identityNumber = body.getJSONObject("reqMsg").getString("identityNum");
			// 证件类型
			String identidiesTypeCd = body.getJSONObject("reqMsg").getString("identityType");
			//			identidiesTypeCd = StaticDataMappingUtil.getBssIndentNbrType(identidiesTypeCd)
			QryCustomerByCertNum qryCustomerByCertNum = new QryCustomerByCertNum(regionId2, Constants.NO_FINISH,
					identityNumber, identidiesTypeCd);
			qryCustomerByCertNum.addScopeInfos(QryCustomerScopeInfoEnum.CUSTOMER);
			JSONObject custObj = qryCustomerByCertNum.generateContractRoot();
			LOGGER.debug("queryProdInstByCertNum中查询企信的入参：{}", custObj);
			redisOperation.incr(Constant.js_intf_qryCustomer + areaCode + DateUtils.getNOWDate());
			String res = HttpClientUtils.doPostJSONHttps(BSSUrlConstants.EOP_QRY_CUSTOMER, custObj.toString());
			LOGGER.debug("queryProdInstByCertNum中查询企信的出参：{}", res);
			// 根据证件号码查询客户信息出现异常，直接返回
			JSONObject resObj = JSON.parseObject(res);
			if (StringUtils.isBlank(res)) {
				String resultMsg = Constants.ERR_NULL;
				return BSSHelper.getErrInfo("99", resultMsg).toString();
			}
			String resCode = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultCode");
			JSONObject resultObject = null;
			if (Constants.NUM_ZERO.equals(resCode)) {
				resultObject = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getJSONObject("resultObject");
			} else {
				String resultMsg = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultMsg");
				return BSSHelper.getErrInfo(resCode, resultMsg).toString();
			}
			LOGGER.debug("根据custId查询产品实例resultObject节点: {}", resultObject);
			IResponseParamTransform transformBean = (IResponseParamTransform) SpringUtil.getBean(ResponseBusinessTypeEnum.getBeanName(61011));
			return transformBean.transform(resultObject);
		} catch (Exception e) {
			return BSSHelper.getErrInfo("99", "服务内部错误").toString();
		}
	}
	
	
	/**
	 * 智慧BSS,actionCode=???,查询产品实例关联关系
	 *
	 * @param head
	 * @param body
	 * @return
	 */
	public String queryProdInstRel(JSONObject head, JSONObject body) {
		try {
			// 映射企信测地市
			String regionId = StaticDataMappingUtil.getRegionByAreaCode(head.getString("areaCode"));
			String res = "";
			String type = body.getString("type");
			String uncompletedFlag = "N";
			if (body.containsKey("uncompletedFlag") && body.getString("uncompletedFlag").equals("Y")) {
				uncompletedFlag = "Y";
			}
			redisOperation.incr(Constant.js_intf_qryProdInstRel + head.getString("areaCode") + DateUtils.getNOWDate());
			//根据产品实例id
			if ("1".equals(type)) {
				String prodInstId = body.getString("prodInstId");
				JSONArray relTypes = body.getJSONArray("relTypes");//产品实例关系类型
				QryProdInstRelByProdId qryProdInstRelByProdId = new QryProdInstRelByProdId(regionId, uncompletedFlag, prodInstId, relTypes);
				JSONObject req = qryProdInstRelByProdId.generateContractRoot();
				LOGGER.debug("queryProdInstRel中根据产品实例id的入参：{}", req);
				res = HttpClientUtils.doPostJSONHttps(BSSUrlConstants.EOP_QRY_PRODINSTREL, req.toString());
				LOGGER.debug("queryProdInstRel中根据产品实例id的出参：{}", res);
			} else if ("2".equals(type)) {
				//根据接入号
				String accnum = body.getString("accnum");
				String prodId = body.getString("prodId");//产品规格
				JSONArray relTypes = body.getJSONArray("relTypes");//产品实例关系类型
				QryProdInstRelByAccNum qryProdInstRelByAccNum = new QryProdInstRelByAccNum(regionId, uncompletedFlag, accnum, prodId, relTypes);
				JSONObject req = qryProdInstRelByAccNum.generateContractRoot();
				LOGGER.debug("queryProdInstRel中根据accnum的入参：{}", req);
				res = HttpClientUtils.doPostJSONHttps(BSSUrlConstants.EOP_QRY_PRODINSTREL, req.toString());
				LOGGER.debug("queryProdInstRel中根据accnum的出参：{}", res);
			} else {
				//根据宽带账号
				String account = body.getString("account");
				String prodId = body.getString("prodId");//产品规格
				JSONArray relTypes = body.getJSONArray("relTypes");//产品实例关系类型
				QryProdInstRelByAccount qryProdInstRelByAccount = new QryProdInstRelByAccount(regionId, uncompletedFlag, account, prodId, relTypes);
				JSONObject req = qryProdInstRelByAccount.generateContractRoot();
				LOGGER.debug("queryProdInstRel中根据account的入参：{}", req);
				res = HttpClientUtils.doPostJSONHttps(BSSUrlConstants.EOP_QRY_PRODINSTREL, req.toString());
				LOGGER.debug("queryProdInstRel中根据account的出参：{}", res);
			}
			//处理企信出参，获取svcCont节点
			return JSON.parseObject(res).getJSONObject("contractRoot").getJSONObject("svcCont").toString();
		} catch (Exception e) {
			return BSSHelper.getErrInfo("99", "服务内部错误").toString();
		}
		
	}
	
	/**
	 * 智慧BSS,actionCode=17007,根据客户id查询用户套餐信息
	 *
	 * @param head
	 * @param body
	 * @return
	 */
	public String queryOfferInstByOwnerCustId(JSONObject head, JSONObject body) {
		// 映射企信测地市
		JSONObject bodyObj = body.getJSONObject("reqMsg");
		//String regionId = StaticDataMappingUtil.getRegionByAreaCode(head.getString("areaCode"))
		String regionId = StaticDataMappingUtil.getDubAreaToBss(body.getJSONObject("reqMsg").getString("bssInnerAreaId"));
		// 产权客户标识
		String ownerCustId = bodyObj.getString("partyId").toString();
		
		QryOfferInstByOwnerCustId qryOfferInstByAccProdInstId = new QryOfferInstByOwnerCustId(null, null, regionId,
				Constants.NO_FINISH, Constants.NO_INDEPENDENT, ownerCustId);
		qryOfferInstByAccProdInstId.addScopeInfos(QryOfferInstScopeInfoEnum.OFFER_INST);
		qryOfferInstByAccProdInstId.addScopeInfos(QryOfferInstScopeInfoEnum.OFFER_PROD_INST_REL);
		qryOfferInstByAccProdInstId.addScopeInfos(QryOfferInstScopeInfoEnum.OFFER_INST_ATTR);
		
		JSONObject offerInstObj = qryOfferInstByAccProdInstId.generateContractRoot();
		redisOperation.incr(Constant.js_intf_qryOfferInst + head.getString("areaCode") + DateUtils.getNOWDate());
		String res = HttpClientUtils.doPostJSONHttps(BSSUrlConstants.EOP_QRY_OFFERINST, offerInstObj.toString());
		LOGGER.debug("根据客户id查询用户套餐信息:{}", res);
		
		
		// 查询产品实例出现异常，直接返回
		if (StringUtils.isBlank(res)) {
			String resultMsg = Constants.ERR_NULL;
			return BSSHelper.getErrInfo("99", resultMsg).toString();
		}
		JSONObject resObj = JSON.parseObject(res);
		String resCode = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultCode");
		JSONObject resultObject = null;
		if (Constants.NUM_ZERO.equals(resCode)) {
			resultObject = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getJSONObject("resultObject");
		} else {
			String resultMsg = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultMsg");
			return BSSHelper.getErrInfo(resCode, resultMsg).toString();
		}
		LOGGER.debug("根据客户id查询用户套餐信息resultObject节点:{}", resultObject);
		
		// 调用查询客户信息接口
		QryCustomerByCustId qryCustomerByCustId = new QryCustomerByCustId(regionId, Constants.NO_FINISH, ownerCustId);
		qryCustomerByCustId.addScopeInfos(QryCustomerScopeInfoEnum.CUSTOMER);
		JSONObject custInfoReq = qryCustomerByCustId.generateContractRoot();
		LOGGER.debug("根据客户id查询客户信息请求入参:{}", custInfoReq);
		redisOperation.incr(Constant.js_intf_qryCustomer + head.getString("areaCode") + DateUtils.getNOWDate());
		String custInfoStr = HttpClientUtils.doPostJSONHttps(BSSUrlConstants.EOP_QRY_CUSTOMER, custInfoReq.toString());
		LOGGER.debug("根据客户id查询客户信息请求回参:{}", custInfoReq);
		if (StringUtils.isBlank(custInfoStr)) {
			String resultMsg = Constants.ERR_NULL;
			return BSSHelper.getErrInfo("99", resultMsg).toString();
		}
		JSONObject custInfoObj = JSON.parseObject(custInfoStr);
		
		String custInfoCode = custInfoObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultCode");
		JSONObject resultCustInfo = null;
		if (Constants.NUM_ZERO.equals(custInfoCode)) {
			resultCustInfo = custInfoObj.getJSONObject("contractRoot").getJSONObject("svcCont").getJSONObject("resultObject");
		} else {
			String resultMsg = custInfoObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultMsg");
			return BSSHelper.getErrInfo(resCode, resultMsg).toString();
		}
		resultObject.put("resultCustInfo", resultCustInfo);
		resultObject.put("partyId", ownerCustId);
		IResponseParamTransform transformBean = (IResponseParamTransform) SpringUtil
				.getBean(ResponseBusinessTypeEnum.getBeanName(17007));
		return transformBean.transform(resultObject);
	}
	
	/**
	 * 目前企信bss这边对于号码类型进行了具体细分，对于新15050，查询产品实例，客户信息，账户信息都需要对产品类型做具体查询后再处理
	 * 目前的方案是，对于如果直接传9位新bss接口的产品规格id的直接使用，对于9的直接转为100000379，对于881直接转为100000881，其他的去查询产品规格接口
	 * 查询产品规格对于如果传2的，即以前的宽带的，用账号做查询，其他的用接入号查询
	 * 如果查出来的规格大于1，需要去排除手机类型，然后取第一个作为其真实类型
	 * Description: <br>
	 *
	 * @param oldNbrType
	 * @return String<br>
	 * @author yangyang<br>
	 * 2018年8月10日
	 */
	public String getGetExactAccNbrType(String oldNbrType, String accNbr, JSONObject head) {
		if (oldNbrType.length() == 9) {
			return oldNbrType;
		} else {
			if (oldNbrType.equals("9")) {
				return "100000379";
			} else if (oldNbrType.equals("881")) {
				return "100000881";
			} else {
				JSONObject body = new JSONObject();
				if (oldNbrType.equals("2")) {//宽带需要传account
					body.put("account", accNbr);
				} else {//其他传accNbr
					body.put("accNbr", accNbr);
				}
				String prodIdStr = qryProdId(head, body);
				JSONObject prodIdJson = JSON.parseObject(prodIdStr);
				if (!prodIdJson.getJSONObject("head").getString("status").equals("00")) {
					return "";
				} else {
					JSONArray prodInstList = prodIdJson.getJSONObject("body").getJSONObject("resultObject").getJSONArray("prodInstList");
					if (prodInstList.isEmpty()) {
						return null;
					} else {
						List<String> loadList = new ArrayList<>();
						for (int i = 0; i < prodInstList.size(); i++) {
							String prodId = prodInstList.getJSONObject(i).getString("prodId");
							if (prodId.equals("100000379") || prodId.equals("100000881")) {//手机和itv已经排除
								continue;
							}
							if (oldNbrType.equals("2") && "100000009".equals(prodId)) {//传2代表宽带，查询规格中有100000009直接传出
								return prodId;
							}
							if (oldNbrType.equals("1") && "100000002".equals(prodId)) {//传1代表固话，查询规格中有100000002直接传出
								return prodId;
							}
							loadList.add(prodId);
						}
						if (!loadList.isEmpty()) {//如果上述不满足，直接吐出第一个
							return loadList.get(0);
						} else {
							return null;
						}
						
					}
				}
			}
		}
	}
	
	/**
	 * 智慧BSS,actionCode=19009,根据产品实例id查询出已订购的附属销售品
	 *
	 * @param head
	 * @param body
	 * @return
	 */
	public String queryOfferInstByAccProdInstId(JSONObject head, JSONObject body) {
		String areaCode = head.getString("areaCode");
		// 映射企信测地市
		String regionId = StaticDataMappingUtil.getRegionByAreaCode(head.getString("areaCode"));
		JSONObject bodyObj = body.getJSONObject("reqMsg");
		// 产品实例ID
		String prodId = bodyObj.getString("prodId");
		// 产品实例ID
		JSONArray accProdInstIds = new JSONArray();
		accProdInstIds.add(prodId);
		QryOfferInstByAccProdInstId qryOfferInstByAccProdInstId = new QryOfferInstByAccProdInstId(null, null, regionId,
				Constants.NO_FINISH, Constants.YES_INDEPENDENT, accProdInstIds);
		qryOfferInstByAccProdInstId.addScopeInfos(QryOfferInstScopeInfoEnum.OFFER_INST);
		qryOfferInstByAccProdInstId.addScopeInfos(QryOfferInstScopeInfoEnum.OFFER_PROD_INST_REL);
		qryOfferInstByAccProdInstId.addScopeInfos(QryOfferInstScopeInfoEnum.OFFER_INST_ATTR);
		
		JSONObject offerInstObj = qryOfferInstByAccProdInstId.generateContractRoot();
		redisOperation.incr(Constant.js_intf_qryOfferInst + areaCode + DateUtils.getNOWDate());
		String res = HttpClientUtils.doPostJSONHttps(BSSUrlConstants.EOP_QRY_OFFERINST, offerInstObj.toString());
		LOGGER.debug("根据产品实例id查询出已订购的附属销售品:{}", res);
		// 查询产品实例出现异常，直接返回
		if (StringUtils.isBlank(res)) {
			String resultMsg = Constants.ERR_NULL;
			return BSSHelper.getErrInfo("99", resultMsg).toString();
		}
		JSONObject resObj = JSON.parseObject(res);
		String resCode = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultCode");
		JSONObject resultObject = null;
		if (Constants.NUM_ZERO.equals(resCode)) {
			resultObject = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getJSONObject("resultObject");
			if (resultObject == null) {
				String resultMsg = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultMsg");
				JSONObject resBody = new JSONObject();
				resBody.put("param", new JSONArray());
				return BSSHelper.getErrInfoWithNullResObj("1", resultMsg, resBody).toString();
			}
		} else {
			String resultMsg = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultMsg");
			return BSSHelper.getErrInfo(resCode, resultMsg).toString();
		}
		LOGGER.debug("据产品实例id查询出已订购的附属销售品resultObject节点:{}", resultObject);
		IResponseParamTransform transformBean = (IResponseParamTransform) SpringUtil
				.getBean(ResponseBusinessTypeEnum.getBeanName(19009));
		return transformBean.transform(resultObject);
	}
	
	public String queryCustomerOrder(JSONObject head, JSONObject body) {
		String beginDate = body.getString("beginDate");
		String endDate = body.getString("endDate");
		String belongCustId = body.getString("belongCustId");
		String uncompletedFlag = Constants.YES_FINISH;
		String regionId = StaticDataMappingUtil.getRegionByAreaCode(head.getString("areaCode"));
		String custOrderNbr = body.getString("custOrderNbr");
		String custOrderId = body.getString("custOrderId");
		String accNbr = body.getString("accNbr");
		String applyObjSpecs = body.getString("applyObjSpecs");
		String objId = body.getString("objId");
		JSONArray scopeInfos = body.getJSONArray("scopeInfos");
		
		QryCustomerOrderNew qryCustomerOrderNew = new QryCustomerOrderNew(regionId, uncompletedFlag, beginDate,
				endDate, belongCustId, custOrderNbr, custOrderId, accNbr, applyObjSpecs, objId);
		qryCustomerOrderNew.addScopeInfos(QryOrderScopeInfoEnum.CUSTOMER_ORDER);
		qryCustomerOrderNew.addScopeInfos(QryOrderScopeInfoEnum.ORDER_ITEM);
		if (scopeInfos != null) {
			for (int i = 0; i < scopeInfos.size(); i++) {
				qryCustomerOrderNew.addScopeInfos(QryOrderScopeInfoEnum.getQryOrderScopeInfoEnum(scopeInfos.get(i).toString()));
			}
		}
		JSONObject custObj = qryCustomerOrderNew.generateContractRoot();
		String res = HttpClientUtils.doPostJSONHttps(BSSUrlConstants.EOP_QRY_CUSTOMER_ORDER, custObj.toString());
		LOGGER.debug("queryCustomerOrder中查询客户订单的出参：{}", res);
		// custId查询出现异常，直接返回
		if (StringUtils.isBlank(res)) {
			String resultMsg = Constants.ERR_NULL;
			return BSSHelper.getErrInfo("99", resultMsg).toString();
		}
		JSONObject resObj = JSON.parseObject(res);
		String resCode = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultCode");
		if (Constants.NUM_ZERO.equals(resCode)) {
			IResponseParamTransform transformBean = (IResponseParamTransform) SpringUtil.getBean(ResponseBusinessTypeEnum.getBeanName(191202));
			return transformBean.transform(resObj);
		} else {
			String resultMsg = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultMsg");
			return BSSHelper.getErrInfo(resCode, resultMsg).toString();
		}
	}
	
	public String qryOrderItemDetail(String regionId, List<String> orderItemIds, List<String> scopeInfos) {
		QryOrderItemDetail qryOrderItemDetail = new QryOrderItemDetail(regionId, orderItemIds, scopeInfos);
		JSONObject custObj = qryOrderItemDetail.generateContractRoot();
		String res = HttpClientUtils.doPostJSONHttps(BSSUrlConstants.EOP_QRY_ORDERITEM_DETAIL, custObj.toString());
		LOGGER.debug("qryOrderItemDetail中查询订单项的出参：{}", res);
		if (StringUtils.isBlank(res)) {
			String resultMsg = Constants.ERR_NULL;
			return BSSHelper.getErrInfo("99", resultMsg).toString();
		}
		JSONObject resObj = JSON.parseObject(res);
		String resCode = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultCode");
		if (Constants.NUM_ZERO.equals(resCode)) {
			IResponseParamTransform transformBean = (IResponseParamTransform) SpringUtil.getBean(ResponseBusinessTypeEnum.getBeanName(200923));
			return transformBean.transform(resObj);
		} else {
			String resultMsg = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultMsg");
			return BSSHelper.getErrInfo(resCode, resultMsg).toString();
		}
	}
	
	public String qryPhoneResetNumCount(String regionId, String certNum) {
		QryPhoneResetNumCount qryPhoneResetNumCount = new QryPhoneResetNumCount(regionId, certNum);
		JSONObject custObj2 = qryPhoneResetNumCount.generateContractRoot();
		LOGGER.debug("qryPhoneResetNumCount查询复机次数的入参：{}", custObj2);
		String res2 = HttpClientUtils.doPostJSONHttps(BSSUrlConstants.EOP_QRY_PHONERESETNUMCOUNT, custObj2.toString());
		LOGGER.debug("qryPhoneResetNumCount查询复机次数的出参：{}", res2);
		if (StringUtils.isBlank(res2)) {
			String resultMsg = Constants.ERR_NULL;
			return BSSHelper.getErrInfo("99", resultMsg).toString();
		}
		JSONObject resObj2 = JSON.parseObject(res2);
		String resCode2 = resObj2.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultCode");
		if (Constants.NUM_ZERO.equals(resCode2)) {
			IResponseParamTransform transformBean = (IResponseParamTransform) SpringUtil.getBean(ResponseBusinessTypeEnum.getBeanName(1));
			return transformBean.transform(resObj2);
		} else {
			String resultMsg = resObj2.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultMsg");
			return BSSHelper.getErrInfo(resCode2, resultMsg).toString();
		}
	}
	
	public String qryUserPhoneResetNumCount(String regionId, String accNum) {
		QryUserPhoneResetNumCount qryUserPhoneResetNumCount = new QryUserPhoneResetNumCount(regionId, accNum);
		JSONObject custObj2 = qryUserPhoneResetNumCount.generateContractRoot();
		LOGGER.debug("qryUserPhoneResetNumCount查询复机次数的入参：{}", custObj2);
		String res2 = HttpClientUtils.doPostJSONHttps(BSSUrlConstants.EOP_QRY_USERPHONERESETNUMCOUNT, custObj2.toString());
		LOGGER.debug("qryUserPhoneResetNumCount查询复机次数的出参：{}", res2);
		if (StringUtils.isBlank(res2)) {
			String resultMsg = Constants.ERR_NULL;
			return BSSHelper.getErrInfo("99", resultMsg).toString();
		}
		JSONObject resObj2 = JSON.parseObject(res2);
		String resCode2 = resObj2.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultCode");
		if (Constants.NUM_ZERO.equals(resCode2)) {
			IResponseParamTransform transformBean = (IResponseParamTransform) SpringUtil.getBean(ResponseBusinessTypeEnum.getBeanName(1));
			return transformBean.transform(resObj2);
		} else {
			String resultMsg = resObj2.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultMsg");
			return BSSHelper.getErrInfo(resCode2, resultMsg).toString();
		}
	}
	
	public String qrySendTJMessageResult(String regionId, String accNum) {
		QrySendTJMessageResult qrySendTJMessageResult = new QrySendTJMessageResult(regionId, accNum);
		JSONObject custObj2 = qrySendTJMessageResult.generateContractRoot();
		LOGGER.debug("qrySendTJMessageResult查询发送停机短信的入参：{}", custObj2);
		String res2 = HttpClientUtils.doPostJSONHttps(BSSUrlConstants.EOP_QRY_SENDTJMESSAGERESULT, custObj2.toString());
		LOGGER.debug("qrySendTJMessageResult查询发送停机短信的出参：{}", res2);
		if (StringUtils.isBlank(res2)) {
			String resultMsg = Constants.ERR_NULL;
			return BSSHelper.getErrInfo("99", resultMsg).toString();
		}
		JSONObject resObj2 = JSON.parseObject(res2);
		String resCode2 = resObj2.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultCode");
		if (Constants.NUM_ZERO.equals(resCode2)) {
			IResponseParamTransform transformBean = (IResponseParamTransform) SpringUtil.getBean(ResponseBusinessTypeEnum.getBeanName(1));
			return transformBean.transform(resObj2);
		} else {
			String resultMsg = resObj2.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultMsg");
			return BSSHelper.getErrInfo(resCode2, resultMsg).toString();
		}
	}
	
	public String createFKFXDTFJOrder(String regionId, String masterAccNum, String accNum) {
		CreateFKFXDTFJOrder createFKFXDTFJOrder = new CreateFKFXDTFJOrder(regionId, masterAccNum, accNum);
		JSONObject custObj2 = createFKFXDTFJOrder.generateContractRoot();
		LOGGER.debug("createFKFXDTFJOrder生成副卡风险停机的复机入参：{}", custObj2);
		String res2 = HttpClientUtils.doPostJSONHttps(BSSUrlConstants.EOP_CREATE_FKFXDTFJORDER, custObj2.toString());
		LOGGER.debug("createFKFXDTFJOrder生成副卡风险停机的复机出参：{}", res2);
		if (StringUtils.isBlank(res2)) {
			String resultMsg = Constants.ERR_NULL;
			return BSSHelper.getErrInfo("99", resultMsg).toString();
		}
		
		JSONObject resObj2 = JSON.parseObject(res2);
		String resCode2 = resObj2.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultCode");
		if (Constants.NUM_ZERO.equals(resCode2)) {
			IResponseParamTransform transformBean = (IResponseParamTransform) SpringUtil.getBean(ResponseBusinessTypeEnum.getBeanName(1));
			return transformBean.transform(resObj2);
		} else {
			String resultMsg = resObj2.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultMsg");
			return BSSHelper.getErrInfo(resCode2, resultMsg).toString();
		}
	}
	
	public String qryMyValueAddedService(String regionId, String prodId, String accNum) {
		QryMyValueAddedServiceBase qryMyValueAddedServiceBase = new QryMyValueAddedServiceBase(regionId, prodId, accNum);
		JSONObject requestObj = qryMyValueAddedServiceBase.generateContractRoot();
		String res2 = HttpClientUtils.doPostJSONHttps(BSSUrlConstants.EOP_QRY_VALUEADDEDSERVICE, requestObj.toString());
		LOGGER.debug("qryMyValueAddedService中查询增值服务的出参：{}", res2);
		// custId查询出现异常，直接返回
		if (StringUtils.isBlank(res2)) {
			String resultMsg = Constants.ERR_NULL;
			return BSSHelper.getErrInfo("99", resultMsg).toString();
		}
		
		JSONObject resObj2 = JSON.parseObject(res2);
		String resCode2 = resObj2.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultCode");
		if (Constants.NUM_ZERO.equals(resCode2)) {
			IResponseParamTransform transformBean = (IResponseParamTransform) SpringUtil.getBean(ResponseBusinessTypeEnum.getBeanName(191206));
			return transformBean.transform(resObj2);
		} else {
			String resultMsg = resObj2.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultMsg");
			return BSSHelper.getErrInfo(resCode2, resultMsg).toString();
		}
	}
	
	
	/**
	 * 智慧BSS,actionCode=190091,根据产品实例id查询出已订购的附属销售品过滤
	 *
	 * @param head
	 * @param body
	 * @return
	 */
	public String queryOfferInstByAccProdInstId2(JSONObject head, JSONObject body) {
		// 映射企信测地市
		String regionId = StaticDataMappingUtil.getRegionByAreaCode(head.getString("areaCode"));
		JSONObject bodyObj = body.getJSONObject("reqMsg");
		// 产品实例ID
		String prodId = bodyObj.getString("prodId");
		// 产品实例ID
		JSONArray accProdInstIds = new JSONArray();
		accProdInstIds.add(prodId);
		QryOfferInstByAccProdInstId qryOfferInstByAccProdInstId = new QryOfferInstByAccProdInstId(null, null, regionId,
				Constants.NO_FINISH, Constants.YES_INDEPENDENT, accProdInstIds);
		qryOfferInstByAccProdInstId.addScopeInfos(QryOfferInstScopeInfoEnum.OFFER_INST);
		qryOfferInstByAccProdInstId.addScopeInfos(QryOfferInstScopeInfoEnum.OFFER_PROD_INST_REL);
		qryOfferInstByAccProdInstId.addScopeInfos(QryOfferInstScopeInfoEnum.OFFER_INST_ATTR);
		
		JSONObject offerInstObj = qryOfferInstByAccProdInstId.generateContractRoot();
		String res = "";
		redisOperation.incr(Constant.js_intf_qryOfferInst + head.getString("areaCode") + DateUtils.getNOWDate());
		res = HttpClientUtils.doPostJSONHttps(BSSUrlConstants.EOP_QRY_OFFERINST, offerInstObj.toString());
		LOGGER.debug("根据产品实例id查询出已订购的附属销售品:{}", res);
		// 查询产品实例出现异常，直接返回
		if (StringUtils.isBlank(res)) {
			String resultMsg = Constants.ERR_NULL;
			return BSSHelper.getErrInfo("99", resultMsg).toString();
		}
		JSONObject resObj = JSON.parseObject(res);
		String resCode = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultCode");
		JSONObject resultObject = null;
		if (Constants.NUM_ZERO.equals(resCode)) {
			resultObject = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getJSONObject("resultObject");
			if (resultObject == null) {
				String resultMsg = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultMsg");
				return BSSHelper.getErrInfo("1", resultMsg).toString();
			}
		} else {
			String resultMsg = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultMsg");
			return BSSHelper.getErrInfo(resCode, resultMsg).toString();
		}
		LOGGER.debug("据产品实例id查询出已订购的附属销售品resultObject节点:{}", resultObject);
		IResponseParamTransform transformBean = (IResponseParamTransform) SpringUtil
				.getBean(ResponseBusinessTypeEnum.getBeanName(190091));
		return transformBean.transform(resultObject);
	}
	
	/**
	 * 智慧BSS,actionCode=10011,查询PUK码
	 *
	 * @param head
	 * @param body
	 * @return
	 */
	public String queryPUKByAccNum(JSONObject head, JSONObject body) {
		try {
			//映射企信测地市
			String regionId = StaticDataMappingUtil.getRegionByAreaCode(head.getString("areaCode"));
			//要查询的号码
			String accNbr = body.getString("accNbr");
			//号码类型
			String prodId = "100000379";
			//拼接新接口入参
			QryPukByAccNum qryPukByAccNum = new QryPukByAccNum(regionId, accNbr, prodId);
			JSONObject inObj = qryPukByAccNum.generateContractRoot();
			LOGGER.debug("==========查询puk码入参=========={}", inObj);
			redisOperation.incr(Constant.JS_INTF_QRYPUKCODE + head.getString("areaCode") + DateUtils.getNOWDate());
			String res = HttpClientUtils.doPostJSONHttps(BSSUrlConstants.EOP_QRY_PUKCODE, inObj.toString());
			LOGGER.debug("==========查询puk码出参=========={}", res);
			//查询puk码出现异常，直接返回
			JSONObject resObj = JSON.parseObject(res);
			if (StringUtils.isBlank(res)) {
				String resultMsg = Constants.ERR_NULL;
				return BSSHelper.getErrInfo("99", resultMsg).toString();
			}
			String resCode = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultCode");
			JSONObject svcCont = null;
			if (Constants.NUM_ZERO.equals(resCode)) {
				svcCont = resObj.getJSONObject("contractRoot").getJSONObject("svcCont");
			} else {
				String resultMsg = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultMsg");
				return BSSHelper.getErrInfo(resCode, resultMsg).toString();
			}
			LOGGER.debug("根据接入号查询puk码svcCont节点:{}", svcCont);
			IResponseParamTransformNew transformBean = SpringUtil.getBean(ResponseBusinessTypeEnum.getBeanName(10011));
			return transformBean.transform(svcCont, head, body);
		} catch (Exception e) {
			return BSSHelper.getErrInfo("99", "服务内部错误").toString();
		}
	}
	
	/**
	 * 智慧BSS,actionCode=15050,查询账户
	 *
	 * @param head
	 * @param body
	 * @return
	 */
	public String queryAccount(JSONObject head, JSONObject body) {
		try {
			// 映射企信测地市
			String regionId = StaticDataMappingUtil.getRegionByAreaCode(head.getString("areaCode"));
			String accNum = null;
			String account = null;
			String prodId = null;
			String res = "";
			String accNbrType = body.getString("accNbrType");
			//accNbrType为1则为普通号码
			/**
			 if("1".equals(accNbrType)){
			 accNum = body.getString("accNbr");
			 prodId = "100000002";
			 account = null;
			 //accNbrType为2则为宽带
			 } else if("2".equals(accNbrType)){
			 account = body.getString("accNbr");
			 prodId = "100000009";
			 accNum = null;
			 //accNbrType为3则为移动业务
			 } else if("9".equals(accNbrType)){
			 accNum = body.getString("accNbr");
			 prodId = "100000379";
			 account = null;
			 }else if("881".endsWith(accNbrType)){
			 accNum = body.getString("accNbr");
			 prodId = "100000881";
			 account = null;
			 }
			 */
			prodId = getGetExactAccNbrType(accNbrType, body.getString("accNbr"), head);
			if (prodId == null) {
				return BSSHelper.getErrInfo("01", "用户信息没查到，请重试").toString();
			}
			if ("2".equals(accNbrType) || (body.containsKey("oldAccNbrType") && body.getString("oldAccNbrType").equals("2"))) {//转前是宽带的和直接就传2的
				account = body.getString("accNbr");
				accNum = null;
			} else {
				accNum = body.getString("accNbr");
				account = null;
			}
			String uncompletedFlag = "N";
			if (body.containsKey("uncompletedFlag") && body.getString("uncompletedFlag").equals("Y")) {
				uncompletedFlag = "Y";
			}
			//调用查询账户接口      暂时都置N，代表未竣工
			QryAccountByAccNum qryAccountByAccNum = new QryAccountByAccNum(regionId, uncompletedFlag, accNum, account, prodId);
			qryAccountByAccNum.addScopeInfos(QryAccountScopeInfoEnum.ACCOUNT);
			qryAccountByAccNum.addScopeInfos(QryAccountScopeInfoEnum.ACC_ATTR);
			qryAccountByAccNum.addScopeInfos(QryAccountScopeInfoEnum.ACCT_ATTR);
			qryAccountByAccNum.addScopeInfos(QryAccountScopeInfoEnum.EXT_ACCT);
			JSONObject acctObj = qryAccountByAccNum.generateContractRoot();
			LOGGER.debug("查询账户入参:{}", acctObj);
			redisOperation.incr(Constant.JS_INTF_QRYACCOUNT + head.getString("areaCode") + DateUtils.getNOWDate());
			res = HttpClientUtils.doPostJSONHttps(BSSUrlConstants.EOP_QRY_ACCOUNT, acctObj.toString());
			LOGGER.debug("查询账户出参：{}", res);
			//custId查询出现异常，直接返回
			JSONObject resObj = JSON.parseObject(res);
			if (StringUtils.isBlank(res)) {
				LOGGER.debug("查询账户接口出参=========> is null");
				String resultMsg = Constants.ERR_NULL;
				return BSSHelper.getErrInfo("99", resultMsg).toString();
			}
			String resCode = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultCode");
			JSONObject resultObject = null;
			if (Constants.NUM_ZERO.equals(resCode)) {
				resultObject = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getJSONObject("resultObject");
			} else {
				String resultMsg = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultMsg");
				return BSSHelper.getErrInfo(resCode, resultMsg).toString();
			}
			LOGGER.debug("查询账户resultObject节点: {}", resultObject);
			IResponseParamTransform transformBean = (IResponseParamTransform) SpringUtil.getBean(ResponseBusinessTypeEnum.getBeanName(150503));
			return transformBean.transform(resultObject);
		} catch (Exception e) {
			return BSSHelper.getErrInfo("99", "服务内部错误").toString();
		}
	}
	
	/**
	 * 智慧BSS,actionCode=15020,校验政企用户
	 *
	 * @param head
	 * @param body
	 * @return
	 */
	public String checkGovCustomer(JSONObject head, JSONObject body, JSONObject oldOutParam) {
		JSONObject res2 = new JSONObject();
		JSONObject body2 = new JSONObject();
		try {
			//映射企信测地市
			String regionId = StaticDataMappingUtil.getRegionByAreaCode(head.getString("areaCode"));
			String validateInfo = body.getString("validateInfo");//验证信息
			
			//号码
			String accNum = validateInfo.substring(validateInfo.indexOf("<accessNumber>") + 14, validateInfo.lastIndexOf("</accessNumber>"));
			String prodId = validateInfo.substring(validateInfo.indexOf("<prodSpecId>") + 12, validateInfo.lastIndexOf("</prodSpecId>"));
			Map<String, String> map = new HashMap<>();
			map.put("2", "100000002");
			map.put("9", "100000009");
			map.put("379", "100000379");
			prodId = map.get(prodId);
			
			//调用企信证件信息获取用户custId       暂时都置N，代表未竣工
			CheckGovCustomerByAccNum checkGovCustomerByAccNum = new CheckGovCustomerByAccNum(regionId, Constants.NO_FINISH, accNum, prodId);
			JSONObject custObj = checkGovCustomerByAccNum.generateContractRoot();
			redisOperation.incr(Constant.JS_INTF_CHECKGOVECUSTOMER + head.getString("areaCode") + DateUtils.getNOWDate());
			String res = HttpClientUtils.doPostJSONHttps(BSSUrlConstants.EOP_CHECK_GOVCUSTOMER, custObj.toString());
			LOGGER.debug("校验政企用户出参:{} ", res);
			//查询产品实例出现异常，直接返回
			if (StringUtils.isBlank(res)) {
				LOGGER.debug("校验政企用户接口出参=========> is null");
				return oldOutParam.toString();
			}
			JSONObject resObj = JSON.parseObject(res);
			String resCode = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultCode");
			JSONObject resultObject = null;
			resultObject = resObj.getJSONObject("contractRoot").getJSONObject("svcCont");
			LOGGER.debug("校验svcCont节点:{}", resultObject);
			
			//校验通过
			if (Constants.NUM_ZERO.equals(resCode)) {
				body2.put("resultMsg", resultObject.getString("resultMsg"));
				body2.put("result", resCode);
				JSONObject head2 = BSSHelper.getErrInfo("00", "success");
				res2.put("head", head2);
				res2.put("body", body2);
			} else {
				JSONObject head2 = BSSHelper.getErrInfo("01", resultObject.getString("resultMsg"));
				res2.put("head", head2);
				res2.put("body", body2);
			}
		} catch (Exception e) {
			LOGGER.debug("checkGovCustomer Exception!");
		}
		return res2.toString();
	}
	
	
	/**
	 * @param @param  head
	 * @param @param  body
	 * @param @return 设定文件
	 * @return String    返回类型
	 * @throws
	 * @Title: queryCustomerByAccNum 10023
	 * @Description: 1
	 * @author fulei
	 */
	public String queryCustomerByAccNum(ReqHead head, JSONObject body, String url) {
		try {
			// 映射企信测地市
			String regionId = StaticDataMappingUtil.getRegionByAreaCode(head.getAreaCode());
			// 身份证号码
			String accessNumber = body.getString("accessNumber");
			// 产品规格类型
			String prodSpecId = body.getString("prodSpecId");
			//产品规格类型转换
//			prodSpecId=BSSHelper.accNbrTypeToProdId(prodSpecId)
			if (prodSpecId.length() < 9) {//
				prodSpecId = BSSHelper.fillStringByTwoChar(prodSpecId, "0", "1");
			}
			QryCustomerByAccNum qryCustomerByCertNum = new QryCustomerByAccNum(regionId, Constants.NO_FINISH, accessNumber, prodSpecId);
			qryCustomerByCertNum.addScopeInfos(QryCustomerScopeInfoEnum.CUSTOMER);
			qryCustomerByCertNum.addScopeInfos(QryCustomerScopeInfoEnum.PARTY_CERT);
			JSONObject qryCustomerInParam = qryCustomerByCertNum.generateContractRoot();
			LOGGER.debug("queryCustomerByAccNum中台查询客户信息的入参：{}", qryCustomerInParam);
			redisOperation.incr(Constant.js_intf_qryCustomer + head.getAreaCode() + DateUtils.getNOWDate());
			String res = HttpClientUtils.doPostJSONHttps(url, qryCustomerInParam.toString());
			LOGGER.debug("queryCustomerByAccNum中台查询客户信息的出参：{}", res);
			// custId查询出现异常，直接返回
			JSONObject resultObject = JSON.parseObject(res);
			if (StringUtils.isBlank(res)) {
				String resultMsg = Constants.ERR_NULL;
				return BSSHelper.getErrInfo("99", resultMsg).toString();
			}
			String resCode = resultObject.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultCode");
			if (!Constants.NUM_ZERO.equals(resCode)) {
				String resultMsg = resultObject.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultMsg");
				return BSSHelper.getErrInfo(resCode, resultMsg).toString();
			}
			IResponseParamTransform transformBean = SpringUtil.getBean(ResponseBusinessTypeEnum.getBeanName(10023));
			return transformBean.transform(resultObject);
		} catch (Exception e) {
			return BSSHelper.getErrInfo("99", "服务内部错误").toString();
		}
		
	}
	
	/**
	 * 智慧BSS,actionCode=15051,主销售品查询接口
	 *
	 * @param head
	 * @param body
	 * @return
	 */
	public String queryMainOfferInst(JSONObject head, JSONObject body) {
		// 映射企信测地市
		String regionId = StaticDataMappingUtil.getRegionByAreaCode(head.getString("areaCode"));
		String ownerCustId = "";
		JSONArray accProdInstIds = new JSONArray();
		String accNbrType = body.getString("accNbrType");
		//accNbrType为1则为客户id
		if ("1".equals(accNbrType)) {
			ownerCustId = body.getString("accNbr");
			//accNbrType为2则为接入产品实例id
		} else if ("2".equals(accNbrType)) {
			// 产品实例ID
			String prodId = body.getString("accNbr");
			// 产品实例ID
			accProdInstIds.add(prodId);
		}
		// 产品实例ID
		QryInstForMainOffer qryMainOfferInst = new QryInstForMainOffer(null, null, regionId,
				Constants.NO_FINISH, Constants.NO_INDEPENDENT, ownerCustId, accProdInstIds, accNbrType);
		qryMainOfferInst.addScopeInfos(QryOfferInstScopeInfoEnum.OFFER_INST);
		qryMainOfferInst.addScopeInfos(QryOfferInstScopeInfoEnum.OFFER_PROD_INST_REL);
		qryMainOfferInst.addScopeInfos(QryOfferInstScopeInfoEnum.OFFER_INST_ATTR);
		
		JSONObject offerInstObj = qryMainOfferInst.generateContractRoot();
		redisOperation.incr(Constant.js_intf_qryOfferInst + head.getString("areaCode") + DateUtils.getNOWDate());
		String res = HttpClientUtils.doPostJSONHttps(BSSUrlConstants.EOP_QRY_OFFERINST, offerInstObj.toString());
		LOGGER.debug("根据产品实例id查询出已订购的附属销售品:{}", res);
		// 查询产品实例出现异常，直接返回
		if (StringUtils.isBlank(res)) {
			String resultMsg = Constants.ERR_NULL;
			return BSSHelper.getErrInfo("99", resultMsg).toString();
		}
		JSONObject resObj = JSON.parseObject(res);
		String resCode = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultCode");
		JSONObject resultObject = null;
		if (Constants.NUM_ZERO.equals(resCode)) {
			resultObject = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getJSONObject("resultObject");
		} else {
			String resultMsg = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultMsg");
			return BSSHelper.getErrInfo(resCode, resultMsg).toString();
		}
		LOGGER.debug("据产品实例id查询出已订购的附属销售品resultObject节点:{}", resultObject);
		IResponseParamTransform transformBean = (IResponseParamTransform) SpringUtil.getBean(ResponseBusinessTypeEnum.getBeanName(15051));
		return transformBean.transform(resultObject);
	}
	
	/**
	 * 查询无纸化标识信息详情 qryPdfByOlNbr
	 *
	 * @param head
	 * @param body
	 * @return
	 */
	public String qryPdfByOrderNbr(JSONObject head, JSONObject body) {
		String custOrderNbr = body.getString("olNbr");
		String mustFlag = body.getString("mustFlag");
		
		// 映射企信测地市
		String regionId = StaticDataMappingUtil.getRegionByAreaCode(head.getString("areaCode"));
		QryPdfByOrderNbr qryPdfByOrderNbr = new QryPdfByOrderNbr(regionId, custOrderNbr, regionId);
		JSONObject orderObj = qryPdfByOrderNbr.generateContractRoot();
		String res = "";
		try {
			redisOperation.incr(Constant.js_intf_qryPdfByOlNbr + head.getString("areaCode") + DateUtils.getNOWDate());
			res = HttpClientUtils.doPostJSONHttps(BSSUrlConstants.EOP_QRY_PDFBYOLNBR, orderObj.toString());
			LOGGER.debug("查询无纸化标识信息详情:{}", res);
			// 查询puk码出现异常，直接返回
			JSONObject resObj = JSON.parseObject(res);
			if (StringUtils.isBlank(res)) {
				String resultMsg = Constants.ERR_NULL;
				return BSSHelper.getErrInfo("99", resultMsg).toString();
			}
			String resCode = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultCode");
			JSONObject svcCont = null;
			if (Constants.NUM_ZERO.equals(resCode)) {
				svcCont = resObj.getJSONObject("contractRoot").getJSONObject("svcCont");
			} else {
				String resultMsg = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultMsg");
				return BSSHelper.getErrInfo(resCode, resultMsg).toString();
			}
			LOGGER.debug("查询无纸化标识信息详情svcCont节点:{}", svcCont);
			JSONArray pdfInfos = svcCont.getJSONObject("resultObject").getJSONArray("pdfInfos");
			JSONObject returnBody = new JSONObject();
			JSONArray docArray = new JSONArray();
			if (pdfInfos != null && !pdfInfos.isEmpty()) {
				for (int i = 0; i < pdfInfos.size(); i++) {
					String busiCode = pdfInfos.getJSONObject(i).getString("busiCode");
					String docKey = pdfInfos.getJSONObject(i).getString("docKey");
					String routeCode = pdfInfos.getJSONObject(i).getString("routeCode");
					String type = pdfInfos.getJSONObject(i).getString("type");//文档类型01代表PDF02代表图片
					/**
					 * 各种协议类型，比如靓号协议pdf
					 docType 03   docItem 95
					 靓号协议照片
					 docType 03   docItem 90  91   92   93  94
					 入网协议pdf
					 docType 01   docItem 35
					 入网协议照片
					 docType 01   docItem 30
					 携入协议pdf
					 docType 04   docItem 70
					 携入协议照片
					 docType 02   docItem 30  31  32  33  34
					 橙分期协议pdf
					 docType 04   docItem 65
					 橙分期协议照片
					 docType 04   docItem 60
					 */
					String docType = pdfInfos.getJSONObject(i).getString("docType");
					String docItem = pdfInfos.getJSONObject(i).getString("docItem");
					if (StringUtils.isEmpty(mustFlag) || type.equals(mustFlag)) {//如果控制文档类型，需要类型一致，如果不控制，则都可以
						QryPdfDetail qryPdfDetail = new QryPdfDetail(regionId, "CRM", busiCode, routeCode, docKey, "XRvSFruAAMY=");// 正式
						// QryPdfDetail qryPdfDetail = new QryPdfDetail(regionId, "CRMTEST", "TEST", routeCode, docKey, "XRvSFruAAMY=");//测试
						JSONObject pdfObj = qryPdfDetail.generateContractRoot();
						redisOperation.incr(Constant.DFS_GET_DATA + head.getString("areaCode") + DateUtils.getNOWDate());
						String result = HttpClientUtils.doPostJSONold(BSSUrlConstants.EOP_DFSGETDATA, pdfObj.toString());
						JSONObject resultObj = JSON.parseObject(result);
						String flag = resultObj.getString("flag");
						String storeValue = "";
						if (Constants.NUM_ZERO.equals(flag)) {
							storeValue = resultObj.getString("storeValue");
							String[] str = storeValue.split(",");
							JSONObject single = new JSONObject();
							single.put("storeValue", str[str.length - 1]);
							single.put("type", type);
							single.put("docType", docType);
							single.put("docItem", docItem);
							docArray.add(single);
						}
					}
				}
				JSONObject returnRes = new JSONObject();
				if (!docArray.isEmpty()) {
					returnBody.put("result", "0");
					returnBody.put("docArray", docArray);
					JSONObject returnHead = BSSHelper.getRspInfo("00", "success");
					returnRes.put("head", returnHead);
				} else {
					returnBody.put("result", "1");
					returnBody.put("docArray", docArray);
					JSONObject returnHead = BSSHelper.getRspInfo("01", "查询失败");
					returnRes.put("head", returnHead);
				}
				returnRes.put("body", returnBody);
				return returnRes.toString();
			}
			return BSSHelper.getErrInfo("01", "查询失败").toString();
		} catch (Exception e) {
			return BSSHelper.getErrInfo("99", "服务内部错误").toString();
		}
	}
	
	/**
	 * Description: 查询订单列表及无纸化标识信息<br>
	 *
	 * @param head
	 * @param body
	 * @return <br>
	 * @author 刘添<br>
	 * @taskId <br>
	 */
	public String qryCustOrderAndPdfFlag(JSONObject head, JSONObject body) {
		// 映射企信测地市
		String regionId = StaticDataMappingUtil.getRegionByAreaCode(head.getString("areaCode"));
		// regionId
		String regionId2 = StaticDataMappingUtil.getRegionByAreaCode(head.getString("areaCode"));
		// 客户id
		String custId = body.getString("partyId");
		// 开始时间
		String startDt = body.getString("startDt");
		// 结束时间
		String endDt = body.getString("endDt");
		
		QryCustOrderAndPdfFlag qryCustOrderAndPdfFlag = new QryCustOrderAndPdfFlag(regionId, regionId2, custId, startDt, endDt);
		JSONObject orderObj = qryCustOrderAndPdfFlag.generateContractRoot();
		String res = "";
		try {
			redisOperation.incr(Constant.JS_INTF_QRYCUSTORDERANDPDFFLAG + head.getString("areaCode") + DateUtils.getNOWDate());
			res = HttpClientUtils.doPostJSONHttps(BSSUrlConstants.EOP_QRY_CUSTORDERANDPDFFLAG, orderObj.toString());
			LOGGER.debug("查询订单列表及无纸化标识信息:{}", res);
			//查询puk码出现异常，直接返回
			JSONObject resObj = JSON.parseObject(res);
			if (StringUtils.isBlank(res)) {
				String resultMsg = Constants.ERR_NULL;
				return BSSHelper.getErrInfo("99", resultMsg).toString();
			}
			String resCode = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultCode");
			JSONObject svcCont = null;
			if (Constants.NUM_ZERO.equals(resCode)) {
				svcCont = resObj.getJSONObject("contractRoot").getJSONObject("svcCont");
			} else {
				String resultMsg = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultMsg");
				return BSSHelper.getErrInfo(resCode, resultMsg).toString();
			}
			LOGGER.debug("查询订单列表及无纸化标识信息svcCont节点:{}", svcCont);
			IResponseParamTransform transformBean = (IResponseParamTransform) SpringUtil
					.getBean(ResponseBusinessTypeEnum.getBeanName(30000));
			return transformBean.transform(resObj);
		} catch (Exception e) {
			return BSSHelper.getErrInfo("99", "服务内部错误").toString();
		}
	}
	
	/**
	 * 智慧BSS,actionCode=19013,查询销售品的详细信息，到期时间
	 *
	 * @param head
	 * @param body
	 * @return
	 */
	public String queryOfferInstExample(JSONObject head, JSONObject body) {
		// 映射企信测地市
		String regionId = StaticDataMappingUtil.getRegionByAreaCode(head.getString("areaCode"));
		JSONObject bodyObj = body.getJSONObject("reqMsg");
		//销售品id
		String offerInstId = bodyObj.getString("offerId");
		// 产品实例ID
		QryOfferInstByOfferInstId qryOfferInstByOfferInstId = new QryOfferInstByOfferInstId(null, null, regionId,
				Constants.NO_FINISH, Constants.NO_INDEPENDENT, offerInstId);
		qryOfferInstByOfferInstId.addScopeInfos(QryOfferInstScopeInfoEnum.OFFER_INST);
		qryOfferInstByOfferInstId.addScopeInfos(QryOfferInstScopeInfoEnum.OFFER_PROD_INST_REL);
		qryOfferInstByOfferInstId.addScopeInfos(QryOfferInstScopeInfoEnum.OFFER_INST_ATTR);
		
		JSONObject offerInstObj = qryOfferInstByOfferInstId.generateContractRoot();
		
		String res = "";
		try {
			LOGGER.debug("根据产品实例id查询出已订购的附属销售品: {}", offerInstObj);
			redisOperation.incr(Constant.js_intf_qryOfferInst + head.getString("areaCode") + DateUtils.getNOWDate());
			res = HttpClientUtils.doPostJSONHttps(BSSUrlConstants.EOP_QRY_OFFERINST, offerInstObj.toString());
			LOGGER.debug("根据产品实例id查询出已订购的附属销售品: {}", res);
			// 查询产品实例出现异常，直接返回
			JSONObject resObj = JSON.parseObject(res);
			if (StringUtils.isBlank(res)) {
				String resultMsg = Constants.ERR_NULL;
				return BSSHelper.getErrInfo("99", resultMsg).toString();
			}
			String resCode = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultCode");
			JSONObject resultObject = null;
			if (Constants.NUM_ZERO.equals(resCode)) {
				resultObject = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getJSONObject("resultObject");
				if (StringUtils.isBlank(resultObject.toString())) {
					String resultMsg = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultMsg");
					return BSSHelper.getErrInfo(resCode, resultMsg).toString();
				}
			} else {
				String resultMsg = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultMsg");
				return BSSHelper.getErrInfo(resCode, resultMsg).toString();
			}
			LOGGER.debug("据产品实例id查询出已订购的附属销售品resultObject节点: {}", resultObject.toString());
			IResponseParamTransform transformBean = (IResponseParamTransform) SpringUtil
					.getBean(ResponseBusinessTypeEnum.getBeanName(19013));
			return transformBean.transform(resultObject);
		} catch (Exception e) {
			return BSSHelper.getErrInfo("99", "服务内部错误").toString();
		}
	}
	
	/**
	 * 分布式文件存储通用接口(无用)
	 *
	 * @param head
	 * @param body
	 * @return
	 */
	public String dfsGetData(JSONObject head, JSONObject body) {
		String res = null;
		try {
			String sysCode = body.getString("sysCode");
			String busCode = body.getString("busCode");
			String storeKey = body.getString("storeKey");
			DfsGetData ddb = new DfsGetData(sysCode, busCode, storeKey);
			String paramString = ddb.generateContractRoot().toJSONString();
			LOGGER.debug("分布式文件存储通用接口入参=========>{}", paramString);
			res = HttpClientUtils.doPostJSONold(BSSUrlConstants.NEW_EOP_URL, paramString);
			LOGGER.debug("分布式文件存储通用接口出参=========>{}", res);
		} catch (Exception e) {
			LOGGER.debug("dfsGetData Exception!");
		}
		return res;
	}
	
	/**
	 * 智慧BSS,actionCode=19113,查询产品下在用的服务
	 *
	 * @param head
	 * @param body
	 * @return
	 */
	
	
	public String qryProdInstByAccProdInstId(JSONObject head, JSONObject body) {
		try {
			// 映射企信测地市
			String regionId = StaticDataMappingUtil.getRegionByAreaCode(head.getString("areaCode"));
			// 接入类产品实例标识
			String accProdInstId = body.getJSONObject("reqMsg").getString("prodId");
			// 产品规格标识列表
			JSONArray prodIds = new JSONArray();
			prodIds.add("20000" + body.getJSONObject("reqMsg").getString("servSpecId"));
			// 状态列表  100000表示在用
			JSONArray statusCds = new JSONArray();
			statusCds.add("100000");
			String res = "";
			
			// 调用企信根据AccProdInstId查询产品实例接口
			QryProdInstByAccProdInstId qryProdInstByAccProdInstId = new QryProdInstByAccProdInstId(regionId, Constants.NO_FINISH, accProdInstId, "1");
			qryProdInstByAccProdInstId.addScopeInfos(QryProdInstScopeInfoEnum.FUNC_PROD_INST, prodIds, statusCds);
			JSONObject inObj = qryProdInstByAccProdInstId.generateContractRoot();
			redisOperation.incr(Constant.js_intf_ProdInst + head.getString("areaCode") + DateUtils.getNOWDate());
			res = HttpClientUtils.doPostJSONHttps(BSSUrlConstants.EOP_QRY_PRODINST, inObj.toString());
			LOGGER.debug("根据accProdInstId查询产品实例接口出参: {}", res);
			// 查询产品实例出现异常，直接返回
			JSONObject resObj = JSON.parseObject(res);
			if (StringUtils.isBlank(res)) {
				String resultMsg = Constants.ERR_NULL;
				return BSSHelper.getErrInfo("99", resultMsg).toString();
			}
			String resCode = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultCode");
			JSONObject resultObject = null;
			if (Constants.NUM_ZERO.equals(resCode)) {
				resultObject = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getJSONObject("resultObject");
			} else {
				String resultMsg = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultMsg");
				return BSSHelper.getErrInfo(resCode, resultMsg).toString();
			}
			LOGGER.debug("根据accProdInstId查询产品实例resultObject节点: {}", resultObject);
			IResponseParamTransform transformBean = (IResponseParamTransform) SpringUtil
					.getBean(ResponseBusinessTypeEnum.getBeanName(19113));
			return transformBean.transform(resultObject);
		} catch (Exception e) {
			return BSSHelper.getErrInfo("99", "服务内部错误").toString();
		}
		
	}
	
	
	/**
	 * @param @param  head
	 * @param @param  body
	 * @param @return 设定文件
	 * @return String    返回类型
	 * @throws
	 * @Title: 校验政企客户
	 * @author cx
	 */
	public String checkGovCustomer(JSONObject head, JSONObject body) {
		try {
			// 映射企信测地市
			String regionId = StaticDataMappingUtil.getRegionByAreaCode(head.getString("areaCode"));
			// 业务号码
			String accNum = body.getString("accNum");
			// 帐号
			String account = body.getString("account");
			// 校验类型
//			String uncompletedFlag = body.getString("uncompletedFlag")
			// 产品规格标识
			String prodId = body.getString("prodId");
			
			//产品规格类型转换
			prodId = BSSHelper.accNbrTypeToProdId(prodId);
			CheckGovCustomer checkGovCustomer = new CheckGovCustomer(regionId, Constants.NO_FINISH, accNum, account, prodId);
			JSONObject checkGovCustomerParam = checkGovCustomer.generateContractRoot();
			LOGGER.debug("checkGovCustomer(校验政企客户接口)入参：{}", checkGovCustomerParam);
			redisOperation.incr(Constant.JS_INTF_CHECKGOVECUSTOMER + head.getString("areaCode") + DateUtils.getNOWDate());
			String res = HttpClientUtils.doPostJSONHttps(BSSUrlConstants.EOP_CHECK_GOVCUSTOMER, checkGovCustomerParam.toString());
			LOGGER.debug("checkGovCustomer(校验政企客户口)出参：{}", res);
			// custId查询出现异常，直接返回
			JSONObject resultObject = JSON.parseObject(res);
			if (StringUtils.isBlank(res)) {
				String resultMsg = Constants.ERR_NULL;
				return BSSHelper.getErrInfo("99", resultMsg).toString();
			}
			String resCode = resultObject.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultCode");
			if (!Constants.NUM_ZERO.equals(resCode)) {
				String resultMsg = resultObject.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultMsg");
				return BSSHelper.getErrInfo(resCode, resultMsg).toString();
			}
			String resultMsg = resultObject.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultMsg");
			JSONObject resObj = new JSONObject();
			JSONObject bodyObj = new JSONObject();
			bodyObj.put("resultMsg", resultMsg);
			bodyObj.put("resultCode", resCode);
			
			JSONObject headObj = BSSHelper.getErrInfo("00", "success");
			resObj.put("head", headObj);
			resObj.put("body", bodyObj);
			return resObj.toString();
		} catch (Exception e) {
			return BSSHelper.getErrInfo("99", "服务内部错误").toString();
		}
		
	}
	
	/**
	 * @param @param  head
	 * @param @param  body
	 * @param @return 设定文件
	 * @return String    返回类型
	 * @throws
	 * @Title: 根据号码查询亲情网内所有成员长号短号资料
	 * @author chenxiang
	 */
	public String qryMemberInfo(JSONObject head, JSONObject body) {
		try {
			// 映射企信测地市
			String regionId = StaticDataMappingUtil.getRegionByAreaCode(head.getString("areaCode"));
			// 业务号码
			String accNum = body.getString("accessNumber");
			// 产品规格标识
			String prodId = body.getString("prodId");
			QryMemberInfo qryMemberInfo = new QryMemberInfo(regionId, Constants.YES_FINISH, accNum, prodId);
			JSONObject qryMemberInfoParam = qryMemberInfo.generateContractRoot();
			LOGGER.debug("qryMemberInfo(根据号码查询亲情网内所有成员长号短号资料)入参：{}", qryMemberInfoParam.toString());
			redisOperation.incr(Constant.JS_INTF_QRYMEMBERINFO + head.getString("areaCode") + DateUtils.getNOWDate());
			String res = HttpClientUtils.doPostJSONHttps(BSSUrlConstants.EOP_QRY_MEMBERINFO, qryMemberInfoParam.toString());
			LOGGER.debug("(根据号码查询亲情网内所有成员长号短号资料)出参：{}", res);
			// custId查询出现异常，直接返回
			JSONObject resObj = JSON.parseObject(res);
			if (StringUtils.isBlank(res)) {
				String resultMsg = Constants.ERR_NULL;
				return BSSHelper.getErrInfo("99", resultMsg).toString();
			}
			String resCode = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultCode");
			JSONObject resultObject = null;
			if (Constants.NUM_ZERO.equals(resCode)) {
				resultObject = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getJSONObject("resultObject");
				if (resultObject == null) {
					String resultMsg = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultMsg");
					return BSSHelper.getErrInfo("1", resultMsg).toString();
				}
			} else {
				String resultMsg = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultMsg");
				return BSSHelper.getErrInfo(resCode, resultMsg).toString();
			}
			LOGGER.debug("根据号码查询亲情网内所有成员长号短号资料)resultObject节点: {}", resultObject.toString());
			IResponseParamTransform transformBean = (IResponseParamTransform) SpringUtil.getBean(ResponseBusinessTypeEnum.getBeanName(29));
			return transformBean.transform(resultObject);
		} catch (Exception e) {
			return BSSHelper.getErrInfo("99", "服务内部错误").toString();
		}
		
	}
	
	
	/**
	 * 智慧BSS,15050查询客户
	 *
	 * @param head
	 * @param body
	 * @return
	 */
	public String qryCustomer(JSONObject head, JSONObject body) {
		try {
			// 映射企信测地市
			String regionId = StaticDataMappingUtil.getRegionByAreaCode(head.getString("areaCode"));
			String accNum = null;
			String account = null;
			String prodId = null;
			String custId = null;
			String acctId = null;
			String certNum = null;
			String certType = null;
			String accNbrType = body.getString("accNbrType");
			//accNbrType为1则为普通号码
			/**
			 *
			 if("1".equals(accNbrType)){
			 accNum = body.getString("accNbr");
			 prodId = "100000002";
			 //accNbrType为2则为宽带
			 } else if("2".equals(accNbrType)){
			 account = body.getString("accNbr");
			 prodId = "100000009";
			 //accNbrType为3则为移动业务
			 } else if("9".equals(accNbrType)){
			 accNum = body.getString("accNbr");
			 prodId = "100000379";
			 }else if("881".endsWith(accNbrType)){
			 accNum = body.getString("accNbr");
			 prodId = "100000881";
			 }
			 */
			Map<String, String> map = new HashMap<>();
			if (body.containsKey("accNbr")) {
				prodId = getGetExactAccNbrType(accNbrType, body.getString("accNbr"), head);
				if (prodId == null) {
					map.put("resultCode", "001");
					return BSSHelper.getErrInfo("01", "没有查到对应号码类型，请确认号码是否存在", map).toString();
				} else if (prodId.equals("")) {
					map.put("resultCode", "002");
					return BSSHelper.getErrInfo("02", "查询号码规格接口失败", map).toString();
				}
				if ("2".equals(accNbrType) || (body.containsKey("oldAccNbrType") && body.getString("oldAccNbrType").equals("2"))) {// 转前是宽带的和直接就传2的
					account = body.getString("accNbr");
					accNum = null;
				} else {
					accNum = body.getString("accNbr");
					account = null;
				}
			} else if (body.containsKey("custId")) {
				custId = body.getString("custId");
			} else if (body.containsKey("acctId")) {
				acctId = body.getString("acctId");
			} else if (body.containsKey("certNum")) {
				certNum = body.getString("certNum");
				certType = body.getString("certType");
			}
			String uncompletedFlag = "N";
			if (body.containsKey("uncompletedFlag") && body.getString("uncompletedFlag").equals("Y")) {
				uncompletedFlag = "Y";
			}
			QryCustomer qryCustomer = new QryCustomer(regionId, uncompletedFlag, accNum, account, prodId, custId, acctId, certNum, certType);
			qryCustomer.addScopeInfos(QryCustomerScopeInfoEnum.CUSTOMER);
			qryCustomer.addScopeInfos(QryCustomerScopeInfoEnum.PARTY_CERT);
			qryCustomer.addScopeInfos(QryCustomerScopeInfoEnum.SPECIAL_LIST);
			qryCustomer.addScopeInfos(QryCustomerScopeInfoEnum.CUST_ATTR);
			JSONObject acctObj = qryCustomer.generateContractRoot();
			LOGGER.debug("查询客户入参:{}", acctObj.toString());
			redisOperation.incr(Constant.js_intf_qryCustomer + head.getString("areaCode") + DateUtils.getNOWDate());
			String res = HttpClientUtils.doPostJSONHttps(BSSUrlConstants.EOP_QRY_CUSTOMER, acctObj.toString());
			LOGGER.debug("查询客户出参：{}", res);
			//custId查询出现异常，直接返回
			if (StringUtils.isBlank(res)) {
				String resultMsg = "客户返回数据为空，无法解析";
				return BSSHelper.getErrInfo("02", resultMsg).toString();
			}
			JSONObject resObj = JSON.parseObject(res);
			if ((resObj.containsKey("IRESULT")) && (CEILING_CODE.equals(resObj.getString("IRESULT")))) {
				map.put("resultCode", "003");
				return BSSHelper.getErrInfo("02", "查询客户限流", map).toString();
			}
			String resCode = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultCode");
			JSONObject resultObject = null;
			if (Constants.NUM_ZERO.equals(resCode)) {
				resultObject = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getJSONObject("resultObject");
			} else {
				String resultMsg = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultMsg");
				map.put("resultCode", "004");//返回报错
				return BSSHelper.getErrInfo("01", "客户:" + resCode + "," + resultMsg, map).toString();
			}
			LOGGER.debug("查询客户resultObject节点: {} ", resultObject.toString());
			IResponseParamTransform transformBean = (IResponseParamTransform) SpringUtil.getBean(ResponseBusinessTypeEnum.getBeanName(24));
			return transformBean.transform(resultObject);
		} catch (Exception e) {
			return BSSHelper.getErrInfo("99", "服务内部错误").toString();
		}
		
	}
	
	public static String qryCustomerWithNoFilter(ReqHead head, JSONObject body, String url) {
		try {
			// 映射企信测地市
			String regionId = StaticDataMappingUtil.getRegionByAreaCode(head.getAreaCode());
			String accNum = null;
			String account = null;
			String prodId = null;
			String accNbrType = body.getString("accNbrType");
			//accNbrType为1则为普通号码
			if ("1".equals(accNbrType)) {
				accNum = body.getString("accNbr");
				prodId = "100000002";
				//accNbrType为2则为宽带
			} else if ("2".equals(accNbrType)) {
				account = body.getString("accNbr");
				prodId = "100000009";
				//accNbrType为3则为移动业务
			} else if ("9".equals(accNbrType)) {
				accNum = body.getString("accNbr");
				prodId = "100000379";
			} else if ("881".endsWith(accNbrType)) {
				accNum = body.getString("accNbr");
				prodId = "100000881";
			}
			Map<String, String> map = new HashMap<>();
			String uncompletedFlag = "N";
			if (body.containsKey("uncompletedFlag") && body.getString("uncompletedFlag").equals("Y")) {
				uncompletedFlag = "Y";
			}
			QryCustomer qryCustomer = new QryCustomer(regionId, uncompletedFlag, accNum, account, prodId, null, null, null, null);
			qryCustomer.addScopeInfos(QryCustomerScopeInfoEnum.CUSTOMER);
			qryCustomer.addScopeInfos(QryCustomerScopeInfoEnum.PARTY_CERT);
			qryCustomer.addScopeInfos(QryCustomerScopeInfoEnum.SPECIAL_LIST);
			JSONObject acctObj = qryCustomer.generateContractRoot();
			LOGGER.debug("查询客户入参:{} ", acctObj);
			RedisOperation redis = SpringUtil.getBean(RedisOperation.class);
			redis.incr(Constant.js_intf_qryCustomer + head.getAreaCode() + DateUtils.getNOWDate());
			String res = HttpClientUtils.doPostJSONHttps(url, acctObj.toString());
			LOGGER.debug("查询客户出参：{}", res);
			//custId查询出现异常，直接返回
			if (StringUtils.isBlank(res)) {
				String resultMsg = "客户返回数据为空，无法解析";
				return BSSHelper.getErrInfo("02", resultMsg).toString();
			}
			JSONObject resObj = JSON.parseObject(res);
			if ((resObj.containsKey("IRESULT")) && (CEILING_CODE.equals(resObj.getString("IRESULT")))) {
				map.put("resultCode", "003");
				return BSSHelper.getErrInfo("02", "查询客户限流", map).toString();
			}
			String resCode = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultCode");
			JSONObject resultObject = null;
			if (Constants.NUM_ZERO.equals(resCode)) {
				resultObject = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getJSONObject("resultObject");
			} else {
				String resultMsg = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultMsg");
				map.put("resultCode", "004");//返回报错
				return BSSHelper.getErrInfo("01", "客户:" + resCode + "," + resultMsg, map).toString();
			}
			LOGGER.debug("查询客户resultObject节点: {}", resultObject);
			IResponseParamTransform transformBean = SpringUtil.getBean(ResponseBusinessTypeEnum.getBeanName(24));
			return transformBean.transform(resultObject);
		} catch (Exception e) {
			return BSSHelper.getErrInfo("99", "服务内部错误").toString();
		}
		
	}
	
	/**
	 * 智慧BSS,15050查询产品实例
	 *
	 * @param head
	 * @param body
	 * @return
	 */
	public String qryProdInst(JSONObject head, JSONObject body) {
		try {
			// 映射企信测地市
			String regionId = StaticDataMappingUtil.getRegionByAreaCode(head.getString("areaCode"));
			String prodId = null;
			String custId = null;
			String accNbrType = body.getString("accNbrType");
			String qryMode = body.getString(Constants.QUERY_MODE);
			String delPWDFlag = body.getString(Constants.DEL_PWD_FLAG) == null ? "Y" : body.getString(Constants.DEL_PWD_FLAG);
			JSONObject inObj = null;
			Map<String, String> map = new HashMap<>();
			if ("20".equals(accNbrType)) {
				//根据客户查
				custId = body.getString("accNbr");
				QryProdInstByCustId qryProdInstByCustId = new QryProdInstByCustId(regionId, Constants.NO_FINISH, custId);
				qryProdInstByCustId.addScopeInfos(QryProdInstScopeInfoEnum.PROD_INST);
				boolean easyQryMod = body.containsKey(Constants.EASY_QRY_MOD);    // partyId查询产品实例判断有无此节点,有此节点无需查实例属性和关系
				if (!easyQryMod) {
					qryProdInstByCustId.addScopeInfos(QryProdInstScopeInfoEnum.PROD_INST_REL);
					qryProdInstByCustId.addScopeInfos(QryProdInstScopeInfoEnum.PROD_INST_STATE);
				}
				qryProdInstByCustId.addScopeInfos(QryProdInstScopeInfoEnum.PROD_INST_ATTR);
				inObj = qryProdInstByCustId.generateContractRoot();
				LOGGER.debug("SmartBssSystemImpl.qryProdInst 根据客户查询产品: {}", inObj);
			} else if ("21".equals(accNbrType)) {
				//根据产品实例查
				String accProdInstId = body.getString("accNbr");
				QryProdInstByAccProdInstId qryProdInstByAccProdInstId = new QryProdInstByAccProdInstId(regionId, Constants.NO_FINISH, accProdInstId, qryMode);
				qryProdInstByAccProdInstId.addScopeInfos(QryProdInstScopeInfoEnum.PROD_INST);
				boolean easyQryMod = body.containsKey("easyQryMod");    // partyId查询产品实例判断有无此节点,有此节点无需查实例属性和关系
				if (!easyQryMod) {
					qryProdInstByAccProdInstId.addScopeInfos(QryProdInstScopeInfoEnum.PROD_INST_REL);
					qryProdInstByAccProdInstId.addScopeInfos(QryProdInstScopeInfoEnum.PROD_INST_STATE);
				}
				qryProdInstByAccProdInstId.addScopeInfos(QryProdInstScopeInfoEnum.PROD_INST_ATTR);
				inObj = qryProdInstByAccProdInstId.generateContractRoot();
				LOGGER.debug("SmartBssSystemImpl.qryProdInst 根据产品实例查询产品: {}", inObj);
			} else if ("3".equals(accNbrType)) {
				//根据合同号查询
				String acctCd = body.getString("acctCd");
				QryProdInstByAcctCd qryProdInstByAccCd = new QryProdInstByAcctCd(regionId, Constants.NO_FINISH, acctCd);
				qryProdInstByAccCd.addScopeInfos(QryProdInstScopeInfoEnum.PROD_INST);
				qryProdInstByAccCd.addScopeInfos(QryProdInstScopeInfoEnum.PROD_INST_ATTR);
				qryProdInstByAccCd.addScopeInfos(QryProdInstScopeInfoEnum.PROD_INST_REL);
				qryProdInstByAccCd.addScopeInfos(QryProdInstScopeInfoEnum.FUNC_PROD_INST);
				qryProdInstByAccCd.addScopeInfos(QryProdInstScopeInfoEnum.FUNC_PROD_INST_ATTR);
				qryProdInstByAccCd.addScopeInfos(QryProdInstScopeInfoEnum.PROD_INST_ACCT_REL);
				qryProdInstByAccCd.addScopeInfos(QryProdInstScopeInfoEnum.PROD_INST_STATE);
				inObj = qryProdInstByAccCd.generateContractRoot();
				LOGGER.debug("SmartBssSystemImpl.qryProdInst 根据合同号查询产品: {}", inObj);
			} else {
				//根据宽带账号或者产品的接入号查
				prodId = getGetExactAccNbrType(accNbrType, body.getString("accNbr"), head);
				if (prodId == null) {
					map.put("resultCode", "001");
					return BSSHelper.getErrInfo("01", "没有查到对应号码类型，请确认号码是否存在", map).toString();
				} else if (prodId.equals("")) {
					map.put("resultCode", "002");
					return BSSHelper.getErrInfo("02", "查询号码规格接口失败", map).toString();
				}
				
				QryProdInstByAcc qryProdInstByAcc = getQryProdInstByAcc(head, body, prodId);
				qryProdInstByAcc.addScopeInfos(QryProdInstScopeInfoEnum.PROD_INST);
				qryProdInstByAcc.addScopeInfos(QryProdInstScopeInfoEnum.PROD_INST_ATTR);
				qryProdInstByAcc.addScopeInfos(QryProdInstScopeInfoEnum.PROD_INST_REL);
				qryProdInstByAcc.addScopeInfos(QryProdInstScopeInfoEnum.FUNC_PROD_INST);
				qryProdInstByAcc.addScopeInfos(QryProdInstScopeInfoEnum.FUNC_PROD_INST_ATTR);
				qryProdInstByAcc.addScopeInfos(QryProdInstScopeInfoEnum.PROD_INST_ACCT_REL);
				qryProdInstByAcc.addScopeInfos(QryProdInstScopeInfoEnum.PROD_INST_STATE);
				inObj = qryProdInstByAcc.generateContractRoot();
				LOGGER.debug("SmartBssSystemImpl.qryProdInst 根据宽带账号或者产品的接入号查产品: {}", inObj);
				
			}
			redisOperation.incr(Constant.js_intf_ProdInst + head.getString("areaCode") + DateUtils.getNOWDate());
			String res = HttpClientUtils.doPostJSONHttps(BSSUrlConstants.EOP_QRY_PRODINST, inObj.toString());
			LOGGER.debug("SmartBssSystemImpl.qryProdInst res ：{}", res);
			return parseQryProdInstResultObject(res, delPWDFlag);
		} catch (Exception e) {
			return BSSHelper.getErrInfo("99", "服务内部错误").toString();
		}
	}
	
	private QryProdInstByAcc getQryProdInstByAcc(JSONObject head, JSONObject body, String prodId) {
		String regionId = StaticDataMappingUtil.getRegionByAreaCode(head.getString("areaCode"));
		String accNum = null;
		String account = null;
		String qryMode = body.getString(Constants.QUERY_MODE);
		String accNbrType = body.getString("accNbrType");
		
		if ("2".equals(accNbrType) || (body.containsKey("oldAccNbrType") && "2".equals(body.getString("oldAccNbrType")))) {//转前是宽带的和直接就传2的
			account = body.getString("accNbr");
		} else {
			accNum = body.getString("accNbr");
		}
		String uncompletedFlag = "N";
		if (body.containsKey("uncompletedFlag") && "Y".equals(body.getString("uncompletedFlag"))) {
			uncompletedFlag = "Y";
		}
		
		return new QryProdInstByAcc(regionId, uncompletedFlag, accNum, account, prodId, qryMode);
	}
	
	private String parseQryProdInstResultObject(String res, String delPWDFlag) {
		Map<String, String> map = new HashMap<>();
		if (StringUtils.isBlank(res)) {
			String resultMsg = "产品接口返回数据为空，无法解析";
			return BSSHelper.getErrInfo("02", resultMsg).toString();
		}
		JSONObject resObj = JSON.parseObject(res);
		if ((resObj.containsKey("IRESULT")) && (CEILING_CODE.equals(resObj.getString("IRESULT")))) {
			map.put("resultCode", "003");
			return BSSHelper.getErrInfo("02", Constants.QRY_PRODINSTANT_LIMIT, map).toString();
		}
		String resCode = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultCode");
		JSONObject resultObject = null;
		if (Constants.NUM_ZERO.equals(resCode)) {
			resultObject = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getJSONObject("resultObject");
			resultObject.put(Constants.DEL_PWD_FLAG, delPWDFlag);
		} else {
			String resultMsg = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultMsg");
			map.put("resultCode", "004");//返回报错
			return BSSHelper.getErrInfo("01", resCode + "," + resultMsg, map).toString();
		}
		LOGGER.debug("查询产品实例resultObject节点 : {}", resultObject);
		
		IResponseParamTransform transformBean = (IResponseParamTransform) SpringUtil.getBean(ResponseBusinessTypeEnum.getBeanName(25));
		return transformBean.transform(resultObject);
	}
	
	public String qryProdInstWithNoFilter(JSONObject head, JSONObject body) {
		try {
			// 映射企信测地市
			String regionId = StaticDataMappingUtil.getRegionByAreaCode(head.getString("areaCode"));
			String accNum = null;
			String account = null;
			String prodId = null;
			String custId = body.getString("custId");
			String accProdInstId = body.getString("prodInstId");
			String accNbrType = body.getString("accNbrType");
			String qryMode = body.getString("queryMode");
			String delPWDFlag = body.getString("delPWDFlag");
			if (delPWDFlag == null) {
				delPWDFlag = "Y";
			}
			
			if (Objects.isNull(custId) && Objects.isNull(accProdInstId)) {
				if (Objects.isNull(accNbrType)) {
					return BSSHelper.getErrInfo("01", "产品类型为空").toString();
				}
				//accNbrType为1则为普通号码
				if ("1".equals(accNbrType)) {
					accNum = body.getString("accNbr");
					prodId = "100000002";
					//accNbrType为2则为宽带
				} else if ("2".equals(accNbrType)) {
					account = body.getString("accNbr");
					prodId = "100000009";
					//accNbrType为3则为移动业务
				} else if ("9".equals(accNbrType)) {
					accNum = body.getString("accNbr");
					prodId = "100000379";
				} else if ("881".endsWith(accNbrType)) {
					accNum = body.getString("accNbr");
					prodId = "100000881";
				} else {
					accNum = body.getString("accNbr");
					prodId = accNbrType;
				}
			}
			
			if ((Objects.isNull(accNum) || accNum.isEmpty()) && (Objects.isNull(account) || account.isEmpty())) {
				accNum = body.getString("accNum");
				if (Objects.isNull(accNum) || accNum.isEmpty()) {
					account = body.getString("account");
				}
			}
			
			String uncompletedFlag = "N";
			if (body.containsKey("uncompletedFlag") && body.getString("uncompletedFlag").equals("Y")) {
				uncompletedFlag = "Y";
			}
			
			Map<String, String> map = new HashMap<>();
			JSONObject acctObj = null;
			if (Objects.nonNull(custId) || (Objects.nonNull(accNbrType) && "20".equals(accNbrType))) {//根据客户查
				if (Objects.isNull(custId)) {
					custId = body.getString("accNbr");
				}
				QryProdInstByCustId qryProdInstByCustId = new QryProdInstByCustId(regionId, Constants.NO_FINISH, custId);
				qryProdInstByCustId.addScopeInfos(QryProdInstScopeInfoEnum.PROD_INST);
				boolean easyQryMod = body.containsKey("easyQryMod");    // partyId查询产品实例判断有无此节点,有此节点无需查实例属性和关系
				if (!easyQryMod) {
					qryProdInstByCustId.addScopeInfos(QryProdInstScopeInfoEnum.PROD_INST_REL);
				}
				qryProdInstByCustId.addScopeInfos(QryProdInstScopeInfoEnum.PROD_INST_ATTR);
				acctObj = qryProdInstByCustId.generateContractRoot();
			} else if (Objects.nonNull(accProdInstId) || (Objects.nonNull(accNbrType) && "21".equals(accNbrType))) {
				if (Objects.isNull(accProdInstId)) {
					accProdInstId = body.getString("accNbr");
				}
				QryProdInstByAccProdInstId qryProdInstByAccProdInstId = new QryProdInstByAccProdInstId(regionId, uncompletedFlag, accProdInstId, qryMode);
				qryProdInstByAccProdInstId.addScopeInfos(QryProdInstScopeInfoEnum.PROD_INST);
				qryProdInstByAccProdInstId.addScopeInfos(QryProdInstScopeInfoEnum.PROD_INST_ATTR);
				qryProdInstByAccProdInstId.addScopeInfos(QryProdInstScopeInfoEnum.PROD_INST_REL);
				qryProdInstByAccProdInstId.addScopeInfos(QryProdInstScopeInfoEnum.FUNC_PROD_INST);
				qryProdInstByAccProdInstId.addScopeInfos(QryProdInstScopeInfoEnum.FUNC_PROD_INST_ATTR);
				qryProdInstByAccProdInstId.addScopeInfos(QryProdInstScopeInfoEnum.PROD_INST_ACCT_REL);
				acctObj = qryProdInstByAccProdInstId.generateContractRoot();
			} else {
				QryProdInstByAcc qryProdInstByAcc = new QryProdInstByAcc(regionId, uncompletedFlag, accNum, account, prodId, qryMode);
				qryProdInstByAcc.addScopeInfos(QryProdInstScopeInfoEnum.PROD_INST);
				qryProdInstByAcc.addScopeInfos(QryProdInstScopeInfoEnum.PROD_INST_ATTR);
				qryProdInstByAcc.addScopeInfos(QryProdInstScopeInfoEnum.PROD_INST_REL);
				qryProdInstByAcc.addScopeInfos(QryProdInstScopeInfoEnum.FUNC_PROD_INST);
				qryProdInstByAcc.addScopeInfos(QryProdInstScopeInfoEnum.FUNC_PROD_INST_ATTR);
				qryProdInstByAcc.addScopeInfos(QryProdInstScopeInfoEnum.PROD_INST_ACCT_REL);
				acctObj = qryProdInstByAcc.generateContractRoot();
			}
			LOGGER.debug(BSSConstants.JS_INTF_PROD_INST.getInParam(), acctObj);
			redisOperation.incr(Constant.js_intf_ProdInst + head.getString("areaCode") + DateUtils.getNOWDate());
			String res = HttpClientUtils.doPostJSONHttps(BSSUrlConstants.EOP_QRY_PRODINST, acctObj.toString());
			LOGGER.debug(BSSConstants.JS_INTF_PROD_INST.getOutParam(), res);
			//custId查询出现异常，直接返回
			if (StringUtils.isBlank(res)) {
				return BSSHelper.getErrInfo("02", BSSConstants.JS_INTF_PROD_INST.getResultNullMsg()).toString();
			}
			JSONObject resObj = JSON.parseObject(res);
			if ((resObj.containsKey("IRESULT")) && (CEILING_CODE.equals(resObj.getString("IRESULT")))) {
				map.put("resultCode", "003");
				return BSSHelper.getErrInfo("02", BSSConstants.JS_INTF_PROD_INST.getLimitMsg(), map).toString();
			}
			String resCode = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultCode");
			JSONObject resultObject = null;
			if (Constants.NUM_ZERO.equals(resCode)) {
				resultObject = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getJSONObject("resultObject");
				resultObject.put("delPWDFlag", delPWDFlag);
			} else {
				String resultMsg = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultMsg");
				map.put("resultCode", "004");//返回报错
				return BSSHelper.getErrInfo("01", resCode + "," + resultMsg, map).toString();
			}
			LOGGER.debug(BSSConstants.JS_INTF_PROD_INST.getResultObjectMsg(), resultObject);
			IResponseParamTransform transformBean = (IResponseParamTransform) SpringUtil.getBean(ResponseBusinessTypeEnum.getBeanName(25));
			return transformBean.transform(resultObject);
		} catch (Exception e) {
			return BSSHelper.getErrInfo("99", "服务内部错误").toString();
		}
	}
	
	/**
	 * 根据接入号或者账号查询prodId
	 * Description: <br>
	 *
	 * @param head
	 * @param body
	 * @return String<br>
	 * @author yangyang<br>
	 * 2018年8月9日
	 */
	public String qryProdId(JSONObject head, JSONObject body) {
		try {
			// 映射企信测地市
			String regionId = StaticDataMappingUtil.getRegionByAreaCode(head.getString("areaCode"));
			String accNum = body.getString("accNbr");
			String account = body.getString("account");
			String uncompletedFlag = "N";
			if (body.containsKey("uncompletedFlag") && body.getString("uncompletedFlag").equals("Y")) {
				uncompletedFlag = "Y";
			}
			QryProdIdByAccount qryProdIdByAccount = new QryProdIdByAccount(regionId, uncompletedFlag, accNum, account);
			JSONObject acctObj = qryProdIdByAccount.generateContractRoot();
			LOGGER.debug("查询产品实例入参:{}", acctObj);
			redisOperation.incr(Constant.js_intf_qryProdId + head.getString("areaCode") + DateUtils.getNOWDate());
			String res = HttpClientUtils.doPostJSONHttps(BSSUrlConstants.EOP_QRY_PRODID, acctObj.toString());
			LOGGER.debug("查询产品实例出参：{}", res);
			//custId查询出现异常，直接返回
			JSONObject resObj = JSON.parseObject(res);
			if (StringUtils.isBlank(res)) {
				String resultMsg = Constants.ERR_NULL;
				return BSSHelper.getErrInfo("99", resultMsg).toString();
			}
			String resCode = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultCode");
			JSONObject resultObject = null;
			if (Constants.NUM_ZERO.equals(resCode)) {
				resultObject = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getJSONObject("resultObject");
			} else {
				String resultMsg = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultMsg");
				return BSSHelper.getErrInfo(resCode, resultMsg).toString();
			}
			LOGGER.debug("查询产品规格resultObject节点: {}", resultObject);
			IResponseParamTransform transformBean = (IResponseParamTransform) SpringUtil.getBean(ResponseBusinessTypeEnum.getBeanName(33));
			return transformBean.transform(resultObject);
		} catch (Exception e) {
			return BSSHelper.getErrInfo("99", "服务内部错误").toString();
		}
	}
	
	/**
	 * 智慧BSS,19353获取用户信息
	 *
	 * @param head
	 * @param body
	 * @return
	 */
	public String queryInUseProd(JSONObject head, JSONObject body) {
		try {
			// 映射企信测地市
			String regionId = StaticDataMappingUtil.getDubAreaToBss(body.getJSONObject("reqMsg").getString("bssInnerAreaId"));
			String accProdInstId = body.getJSONObject("reqMsg").getString("prodId");
			//-----------查询客户开始----------
			QryCustomerByAccProdInstId qryCustomerByAccProdInstId = new QryCustomerByAccProdInstId(regionId, "N", accProdInstId);
			qryCustomerByAccProdInstId.addScopeInfos(QryCustomerScopeInfoEnum.CUSTOMER);
			qryCustomerByAccProdInstId.addScopeInfos(QryCustomerScopeInfoEnum.PARTY_CERT);
			JSONObject custObj = qryCustomerByAccProdInstId.generateContractRoot();
			LOGGER.debug("查询客户入参:{}", custObj);
			redisOperation.incr(Constant.js_intf_qryCustomer + head.getString("areaCode") + DateUtils.getNOWDate());
			String resCust = HttpClientUtils.doPostJSONHttps(BSSUrlConstants.EOP_QRY_CUSTOMER, custObj.toString());
			LOGGER.debug("查询客户出参：{}", resCust);
			//custId查询出现异常，直接返回
			JSONObject resCustObj = JSON.parseObject(resCust);
			if (StringUtils.isBlank(resCust)) {
				String resultMsg = Constants.ERR_NULL;
				return BSSHelper.getErrInfo("99", resultMsg).toString();
			}
			String resCustCode = resCustObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultCode");
			JSONObject custObject = null;
			if (Constants.NUM_ZERO.equals(resCustCode)) {
				custObject = resCustObj.getJSONObject("contractRoot").getJSONObject("svcCont").getJSONObject("resultObject");
			} else {
				String resultMsg = resCustObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultMsg");
				return BSSHelper.getErrInfo(resCustCode, resultMsg).toString();
			}
			LOGGER.debug("查询客户resultObject节点: {}", custObject);
			//-----------查询客户结束----------
			
			//-----------查询产品实例开始----------
			QryProdInstByAccProdInstId qryProdInstByAccProdInstId = new QryProdInstByAccProdInstId(regionId, "N", accProdInstId, "1");
			qryProdInstByAccProdInstId.addScopeInfos(QryProdInstScopeInfoEnum.PROD_INST);
			qryProdInstByAccProdInstId.addScopeInfos(QryProdInstScopeInfoEnum.PROD_INST_ATTR);
			qryProdInstByAccProdInstId.addScopeInfos(QryProdInstScopeInfoEnum.FUNC_PROD_INST);
			qryProdInstByAccProdInstId.addScopeInfos(QryProdInstScopeInfoEnum.FUNC_PROD_INST_ATTR);
			qryProdInstByAccProdInstId.addScopeInfos(QryProdInstScopeInfoEnum.PROD_INST_ACC_NUM);
			JSONObject prodObj = qryProdInstByAccProdInstId.generateContractRoot();
			LOGGER.debug("查询产品实例入参: {}", prodObj);
			redisOperation.incr(Constant.js_intf_ProdInst + head.getString("areaCode") + DateUtils.getNOWDate());
			String resProd = HttpClientUtils.doPostJSONHttps(BSSUrlConstants.EOP_QRY_PRODINST, prodObj.toString());
			LOGGER.debug("查询产品实例出参：{}", resProd);
			//custId查询出现异常，直接返回
			JSONObject resProdObj = JSON.parseObject(resProd);
			if (StringUtils.isBlank(resProd)) {
				String resultMsg = resProdObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultMsg");
				return BSSHelper.getErrInfo("99", resultMsg).toString();
			}
			String resProdCode = resProdObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultCode");
			JSONObject prodObject = null;
			if (Constants.NUM_ZERO.equals(resProdCode)) {
				prodObject = resProdObj.getJSONObject("contractRoot").getJSONObject("svcCont").getJSONObject("resultObject");
			} else {
				String resultMsg = resProdObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultMsg");
				return BSSHelper.getErrInfo(resProdCode, resultMsg).toString();
			}
			LOGGER.debug("查询产品实例resultObject节点: {}", prodObject);
			
			//-----------查询产品实例结束----------
			
			//-----------查询账户开始----------
			//调用查询账户接口      暂时都置N，代表未竣工
			QryAccountByAccProdInstId qryAccountByAccProdInstId = new QryAccountByAccProdInstId(regionId, Constants.NO_FINISH, accProdInstId);
			qryAccountByAccProdInstId.addScopeInfos(QryAccountScopeInfoEnum.ACCOUNT);
			qryAccountByAccProdInstId.addScopeInfos(QryAccountScopeInfoEnum.ACC_ATTR);
			JSONObject acctObj = qryAccountByAccProdInstId.generateContractRoot();
			LOGGER.debug("查询账户入参: {}", acctObj);
			redisOperation.incr(Constant.JS_INTF_QRYACCOUNT + head.getString("areaCode") + DateUtils.getNOWDate());
			String res = HttpClientUtils.doPostJSONHttps(BSSUrlConstants.EOP_QRY_ACCOUNT, acctObj.toString());
			LOGGER.debug("查询账户出参：{}", res);
			//custId查询出现异常，直接返回
			JSONObject resObj = JSON.parseObject(res);
			if (StringUtils.isBlank(res)) {
				String resultMsg = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultMsg");
				return BSSHelper.getErrInfo("99", resultMsg).toString();
			}
			String resCode = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultCode");
			JSONObject accountObject = null;
			if (Constants.NUM_ZERO.equals(resCode)) {
				accountObject = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getJSONObject("resultObject");
			} else {
				String resultMsg = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultMsg");
				return BSSHelper.getErrInfo(resCode, resultMsg).toString();
			}
			LOGGER.debug("查询账户resultObject节点: {}", accountObject);
			//-----------查询账户结束----------
			IResponseParamTransform transformBean = (IResponseParamTransform) SpringUtil
					.getBean(ResponseBusinessTypeEnum.getBeanName(26));
			//把三个出参放入一个jsonobject
			JSONObject allObj = new JSONObject();
			allObj.put("custObj", custObject.toJSONString());
			allObj.put("prodObject", prodObject.toJSONString());
			allObj.put("accountObject", accountObject.toJSONString());
			return transformBean.transform(allObj);
		} catch (Exception e) {
			return BSSHelper.getErrInfo("99", "服务内部错误").toString();
		}
	}
	
	
	/**
	 * 定制提醒实例默认不开通服务
	 *
	 * @return
	 */
	public JSONArray specsList() {
		JSONArray specsList = new JSONArray();
		specsList.add("200000907");
		specsList.add("200000912");
		return specsList;
	}
	
	
	/**
	 * 查询已经订购的提醒定制实例 (提供给查询已经订购的提醒定制实例/  查询可订购的销售品实例 使用 )
	 *
	 * @param head
	 * @param body
	 * @return
	 */
	public String qryRemindProdInstTool(JSONObject head, JSONObject body) {
		// 映射企信测地市
		String regionId = StaticDataMappingUtil.getRegionByAreaCode(head.getString("areaCode"));
		String accProdInstId = body.getString("prodId");
		
		QryRemindProdInst qryRemindProdInst = new QryRemindProdInst(regionId, accProdInstId);
		qryRemindProdInst.addScopeInfos(QryProdInstScopeInfoEnum.INST);
		qryRemindProdInst.addScopeInfos(QryProdInstScopeInfoEnum.PROD_INST_ATTR);
		JSONObject orderObj = qryRemindProdInst.generateContractRoot();
		String res = "";
		try {
			redisOperation.incr(Constant.JS_INTF_QRYREMINDPRODINST + head.getString("areaCode") + DateUtils.getNOWDate());
			res = HttpClientUtils.doPostJSONHttps(BSSUrlConstants.EOP_QRY_REMINDPRODINST, orderObj.toString());
			LOGGER.debug("查询已经订购的提醒定制信息:{}", res);
			//查询已经订购的提醒定制信息
			JSONObject resObj = JSON.parseObject(res);
			if (StringUtils.isBlank(res)) {
				String resultMsg = Constants.ERR_NULL;
				return BSSHelper.getErrInfo("99", resultMsg).toString();
			}
			String resCode = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultCode");
			JSONObject resultObject = null;
			if (Constants.NUM_ZERO.equals(resCode)) {
				resultObject = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getJSONObject("resultObject");
				if (null == resultObject) {
					resultObject = new JSONObject();
					resultObject.put("prodInsts", new JSONArray());
				}
			} else {
				String resultMsg = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultMsg");
				return BSSHelper.getErrInfo(resCode, resultMsg).toString();
			}
			LOGGER.debug("查询已经订购的提醒定制svcCont节点:{}", resultObject);
			return resultObject.toString();
		} catch (Exception e) {
			return BSSHelper.getErrInfo("99", "服务内部错误").toString();
		}
		
	}
	
	
	/**
	 * 查询已订购【功能/服务】产品
	 *
	 * @param head
	 * @param body
	 * @return
	 */
	public String qryOptionalProd(JSONObject head, JSONObject body) {
		// 映射企信测地市
		String regionId = StaticDataMappingUtil.getRegionByAreaCode(head.getString("areaCode"));
		
		QryAllCanBeOrderedOfferInst qryAllCanBeOrderedOfferInst = new QryAllCanBeOrderedOfferInst(regionId, Constants.NO_FINISH, Constants.NO_INDEPENDENT, "105", "100000379");
		JSONObject orderObj = qryAllCanBeOrderedOfferInst.generateContractRoot();
		String res = "";
		try {
			redisOperation.incr(Constant.JS_INTF_CPC_PPM_QRYOPTIONALPROD + head.getString("areaCode") + DateUtils.getNOWDate());
			res = HttpClientUtils.doPostJSONold(BSSUrlConstants.QRY_OPTIONALPROD, orderObj.toString());
			LOGGER.debug("查询可订购的销售品信息:{}", res);
			//查询已订购【功能/服务】产品
			JSONObject resObj = JSON.parseObject(res);
			if (StringUtils.isBlank(res)) {
				String resultMsg = Constants.ERR_NULL;
				return BSSHelper.getErrInfo("99", resultMsg).toString();
			}
			String resCode = resObj.getString("resultCode");
			JSONObject resultObject = null;
			if (Constants.NUM_ZERO.equals(resCode)) {
				resultObject = resObj.getJSONObject("resultObject");
			} else {
				String resultMsg = resObj.getString("resultType");
				return BSSHelper.getErrInfo(resCode, resultMsg).toString();
			}
			LOGGER.debug("查询可订购的销售品信息svcCont节点: {}", resultObject);
			return resultObject.toString();
		} catch (Exception e) {
			return BSSHelper.getErrInfo("99", "服务内部错误").toString();
		}
		
	}
	
	/**
	 * 查询已经订购的提醒定制实例
	 *
	 * @param head
	 * @param body
	 * @return
	 */
	public String qryRemindProdInst(JSONObject head, JSONObject body) {
		try {
			//已订购
			String remindProdInst = qryRemindProdInstTool(head, body);
			//可订购
			String optionalProd = qryOptionalProd(head, body);
			JSONObject resultObject = new JSONObject();
			resultObject.put("remindProdInst", remindProdInst);
			resultObject.put("optionalProd", optionalProd);
			resultObject.put("specsList", specsList());
			
			IResponseParamTransform transformBean = (IResponseParamTransform) SpringUtil
					.getBean(ResponseBusinessTypeEnum.getBeanName(19016));
			
			return transformBean.transform(resultObject);
		} catch (Exception e) {
			return BSSHelper.getErrInfo("99", "服务内部错误").toString();
		}
	}
	
	/**
	 * 查询可订购的提醒定制实例
	 *
	 * @param head
	 * @param body
	 * @return
	 */
	public String qryAbleRemindProdInst(JSONObject head, JSONObject body) {
		try {
			//已订购
			String remindProdInst = qryRemindProdInst(head, body);
			LOGGER.debug("--------remindProdInst--------{}", remindProdInst);
			JSONObject resultJSON = JSON.parseObject(remindProdInst);
			JSONObject headJSON = resultJSON.getJSONObject("head");
			JSONObject bodyJSON = resultJSON.getJSONObject("body");
			JSONObject resultObj = null;
			String status = headJSON.getString("status");
			LOGGER.debug("--------status--------{}", status);
			if ("00".equals(status)) {
				resultObj = bodyJSON.getJSONObject("resultObject");
			}
			//可订购
			String optionalProd = qryOptionalProd(head, body);
			JSONObject resultObject = new JSONObject();
			resultObject.put("remindProdInst", resultObj);
			resultObject.put("optionalProd", optionalProd);
			resultObject.put("specsList", specsList());
			
			IResponseParamTransform transformBean = (IResponseParamTransform) SpringUtil.getBean(ResponseBusinessTypeEnum.getBeanName(19017));
			return transformBean.transform(resultObject);
		} catch (Exception e) {
			return BSSHelper.getErrInfo("99", "服务内部错误").toString();
		}
	}
	
	/**
	 * 智慧BSS,19018根据产品id查询合账付费的产品列表
	 */
	public String queryProds(JSONObject head, JSONObject body) {
		try {
			// 映射企信测地市
			String regionId = StaticDataMappingUtil.getDubAreaToBss(body.getJSONObject("reqMsg").getString("bssInnerAreaId"));
			String accProdInstId = body.getJSONObject("reqMsg").getString("prodId");
			//-----------查询账户开始----------
			//调用查询账户接口      暂时都置N，代表未竣工
			QryAccountByAccProdInstId qryAccountByAccProdInstId = new QryAccountByAccProdInstId(regionId, Constants.NO_FINISH, accProdInstId);
			qryAccountByAccProdInstId.addScopeInfos(QryAccountScopeInfoEnum.ACCOUNT);
			qryAccountByAccProdInstId.addScopeInfos(QryAccountScopeInfoEnum.ACC_ATTR);
			JSONObject acctObj = qryAccountByAccProdInstId.generateContractRoot();
			LOGGER.debug("查询账户入参: {}", acctObj);
			redisOperation.incr(Constant.JS_INTF_QRYACCOUNT + head.getString("areaCode") + DateUtils.getNOWDate());
			String res = HttpClientUtils.doPostJSONHttps(BSSUrlConstants.EOP_QRY_ACCOUNT, acctObj.toString());
			LOGGER.debug("查询账户出参：{}", res);
			//custId查询出现异常，直接返回
			JSONObject resObj = JSON.parseObject(res);
			String acctId = "";
			if (StringUtils.isBlank(res)) {
				String resultMsg = Constants.ERR_NULL;
				return BSSHelper.getErrInfo("99", resultMsg).toString();
			}
			String resCode = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultCode");
			JSONObject accountObject = null;
			if (Constants.NUM_ZERO.equals(resCode)) {
				accountObject = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getJSONObject("resultObject");
				if (accountObject != null) {
					acctId = accountObject.getJSONArray("accountDetailInfos").getJSONObject(0).getJSONObject("accountDetail").getString("acctId");
				} else {
					return BSSHelper.getErrInfo(resCode, "查询账户id为空").toString();
				}
			} else {
				String resultMsg = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultMsg");
				return BSSHelper.getErrInfo(resCode, resultMsg).toString();
			}
			LOGGER.debug("查询账户resultObject节点:{}", accountObject);
			//-----------查询账户结束----------
			
			//-----------查询产品实例开始----------
			QryProdInstByAcctId qryProdInstByAcctId = new QryProdInstByAcctId(regionId, "N", acctId);
			qryProdInstByAcctId.addScopeInfos(QryProdInstScopeInfoEnum.PROD_INST);
			qryProdInstByAcctId.addScopeInfos(QryProdInstScopeInfoEnum.PROD_INST_ATTR);
			qryProdInstByAcctId.addScopeInfos(QryProdInstScopeInfoEnum.FUNC_PROD_INST);
			qryProdInstByAcctId.addScopeInfos(QryProdInstScopeInfoEnum.FUNC_PROD_INST_ATTR);
			JSONObject prodObj = qryProdInstByAcctId.generateContractRoot();
			LOGGER.debug("查询产品实例入参:{}", prodObj);
			redisOperation.incr(Constant.js_intf_ProdInst + head.getString("areaCode") + DateUtils.getNOWDate());
			String resProd = HttpClientUtils.doPostJSONHttps(BSSUrlConstants.EOP_QRY_PRODINST, prodObj.toString());
			LOGGER.debug("查询产品实例出参：{}", resProd);
			//custId查询出现异常，直接返回
			JSONObject resProdObj = JSON.parseObject(resProd);
			if (StringUtils.isBlank(resProd)) {
				String resultMsg = resProdObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultMsg");
				return BSSHelper.getErrInfo("99", resultMsg).toString();
			}
			String resProdCode = resProdObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultCode");
			JSONObject prodObject = null;
			if (Constants.NUM_ZERO.equals(resProdCode)) {
				prodObject = resProdObj.getJSONObject("contractRoot").getJSONObject("svcCont").getJSONObject("resultObject");
			} else {
				String resultMsg = resProdObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultMsg");
				return BSSHelper.getErrInfo(resProdCode, resultMsg).toString();
			}
			LOGGER.debug("查询产品实例resultObject节点: {}", prodObject);
			
			//-----------查询产品实例结束----------
			
			IResponseParamTransform transformBean = (IResponseParamTransform) SpringUtil
					.getBean(ResponseBusinessTypeEnum.getBeanName(27));
			//把2个出参放入一个jsonobject
			JSONObject allObj = new JSONObject();
			allObj.put("prodObject", prodObject.toJSONString());
			allObj.put("accountObject", accountObject.toJSONString());
			return transformBean.transform(allObj);
			
		} catch (Exception e) {
			return BSSHelper.getErrInfo("99", "服务内部错误").toString();
		}
	}
	
	
	/**
	 * Description: 接入号查询宽带号码及装机地址 <br>
	 *
	 * @param head
	 * @param body
	 * @return <br>
	 * @author 刘添<br>
	 * @taskId <br>
	 */
	public String qryProdInstByAccNum(JSONObject head, JSONObject body) {
		try {
			// 映射企信测地市
			String regionId = StaticDataMappingUtil.getRegionByAreaCode(head.getString("areaCode"));
			
			String paramInfo = body.getString("paramInfo");
			// 接入号
			String accNbr = BSSHelper.getNodeByXml(paramInfo, "accNbr");
			// 服务规格
			String prodspecId = BSSHelper.getNodeByXml(paramInfo, "prodspecId");
			String prodId = BSSHelper.prodspecIdToProdId(prodspecId);
			//----------------查询产品开始-------------------
			
			QryProdInstByAccNum qryProdInstByAccNum = new QryProdInstByAccNum(regionId, Constants.NO_FINISH, accNbr, prodId, "1");
			qryProdInstByAccNum.addScopeInfos(QryProdInstScopeInfoEnum.FUNC_PROD_INST);
			qryProdInstByAccNum.addScopeInfos(QryProdInstScopeInfoEnum.PROD_INST_ATTR);
			qryProdInstByAccNum.addScopeInfos(QryProdInstScopeInfoEnum.PROD_INST);
			qryProdInstByAccNum.addScopeInfos(QryProdInstScopeInfoEnum.PROD_INST_REL);
			JSONObject inObj = qryProdInstByAccNum.generateContractRoot();
			redisOperation.incr(Constant.js_intf_ProdInst + head.getString("areaCode") + DateUtils.getNOWDate());
			String res = HttpClientUtils.doPostJSONHttps(BSSUrlConstants.EOP_QRY_PRODINST, inObj.toString());
			LOGGER.debug("接入号查询宽带号码及装机地址回参：{}", res);
			// ---------------开始解析返回参数--------------------
			JSONObject resJson = JSON.parseObject(res);
			JSONObject contractRootJson = resJson.getJSONObject("contractRoot");
			JSONObject svcContJson = contractRootJson.getJSONObject("svcCont");
			// 成功标识
			String resultCode = svcContJson.getString("resultCode");
			// 结果信息
			String resultMsg = svcContJson.getString("resultMsg");
			// 需要关系转换的入参
			JSONObject responseJSON = new JSONObject();
			// 如果调用企信接口成功，则进入下一步解析，否则返回异常
			if (SystemConst.NUMBER_STR_ZERO.equals(resultCode)) {
				responseJSON.put("resultObject", svcContJson.getJSONObject("resultObject"));
			} else {
				return BSSHelper.getErrInfo(resultCode, resultMsg).toString();
			}
			// 获取关系转换的BeanName
			IResponseParamTransform transformBean = SpringUtil.getBean(ResponseBusinessTypeEnum.getBeanName(17086));
			if (body.containsKey(Constants.UQCENCFLAG)) {
				responseJSON.put(Constants.UQCENCFLAG, "Y");
			}
			return transformBean.transform(responseJSON);
		} catch (Exception e) {
			return BSSHelper.getErrInfo("99", "服务内部错误").toString();
		}
	}
	
	
	/**
	 * @param @param  head
	 * @param @param  body
	 * @param @return 设定文件
	 * @return String    返回类型
	 * @throws
	 * @Title: 智慧各种查询销售品    19500
	 * @author chenxiang
	 */
	public String callQryOfferInst(JSONObject head, JSONObject body) {
		try {
			// 映射企信测地市
			String regionId = StaticDataMappingUtil.getRegionByAreaCode(head.getString("areaCode"));
			// 业务号码
			String accNum = "";
			//产品客户id
			String ownerCustId = "";
			// 产品实例ID
			String accProdInstId = "";
			JSONArray accProdInstIds = new JSONArray();
			//销售品规格id
			String offerId = "";
			// 查询类型  1 : 产品接入号码（宽带也是接入号码不能用宽带号码） 2：接入产品实例  3:客户id  4 ：销售品标识
			String accNbrType = body.getString("accNbrType");
			String isDep = body.getString("idDep");
			if (StringUtils.isEmpty(isDep)) {//默认空为主销售品
				isDep = Constants.NO_INDEPENDENT;
			} else if ("0".equals(isDep)) {//附属销售品
				isDep = Constants.YES_INDEPENDENT;
			} else if ("all".equals(isDep)) {
				isDep = null;
			}
			
			String roleId = body.getString("roleId");
			List<String> offerIds = null;
			JSONArray offerIdArr = body.getJSONArray("offerIds");
			if (offerIdArr != null && !offerIdArr.isEmpty()) {
				offerIds = JSON.parseArray(offerIdArr.toString(), String.class);
			}
			Map<String, String> returnNoResultMap = new HashMap<>();
			returnNoResultMap.put("returnCode", "1");
			if ("1".equals(accNbrType)) {
				accNum = body.getString("accNbr");
				QryProdIdByAccount qryProdIdByAccount = new QryProdIdByAccount(regionId, Constants.NO_FINISH, accNum, null);
				JSONObject qryProd = qryProdIdByAccount.generateContractRoot();
				redisOperation.incr(Constant.js_intf_qryProdId + head.getString("areaCode") + DateUtils.getNOWDate());
				String res = HttpClientUtils.doPostJSONHttps(BSSUrlConstants.EOP_QRY_PRODID, qryProd.toString());
				LOGGER.debug("根据接入号查询产品规格信息:{}", res);
				if (StringUtils.isBlank(res)) {
					String resultMsg = Constants.ERR_NULL;
					return BSSHelper.getErrInfo("99", resultMsg).toString();
				}
				JSONObject resObj = JSON.parseObject(res);
				String resCode = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultCode");
				JSONObject allobj = new JSONObject();
				JSONArray all = new JSONArray();
				if (Constants.NUM_ZERO.equals(resCode)) {
					JSONObject resultObject = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getJSONObject("resultObject");
					JSONArray prodInstList = resultObject.getJSONArray("prodInstList");
					if (!prodInstList.isEmpty()) {
						String prodId = "";
						for (int i = 0; i < prodInstList.size(); i++) {
							String singleProdId = prodInstList.getJSONObject(i).getString("prodId");
							if (StaticDataMappingUtil.ususalProdId.contains(singleProdId)) {
								prodId = singleProdId;
							}
						}
						if ("".equals(prodId)) {
							prodId = prodInstList.getJSONObject(0).getString("prodId");
						}
						QryOfferInstByAccNum qryOfferInstByAccNum = new QryOfferInstByAccNum(offerIds, roleId, regionId,
								Constants.NO_FINISH, isDep, accNum, prodId);
						qryOfferInstByAccNum.addScopeInfos(QryOfferInstScopeInfoEnum.OFFER_INST);
						qryOfferInstByAccNum.addScopeInfos(QryOfferInstScopeInfoEnum.OFFER_PROD_INST_REL);
						JSONObject offerInstObj = qryOfferInstByAccNum.generateContractRoot();
						redisOperation.incr(Constant.js_intf_qryOfferInst + head.getString("areaCode") + DateUtils.getNOWDate());
						res = HttpClientUtils.doPostJSONHttps(BSSUrlConstants.EOP_QRY_OFFERINST, offerInstObj.toString());
						LOGGER.debug("根据接入号查询销售品: {}", res);
						JSONObject obj = JSON.parseObject(res);
						if (StringUtils.isBlank(res)) {
							String resultMsg = Constants.ERR_NULL;
							return BSSHelper.getErrInfo("99", resultMsg).toString();
						}
						String resCode2 = obj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultCode");
						if (Constants.NUM_ZERO.equals(resCode2)) {
							resultObject = obj.getJSONObject("contractRoot").getJSONObject("svcCont").getJSONObject("resultObject");
							if (resultObject == null) {
								return BSSHelper.getErrInfo("01", "未查到销售品", returnNoResultMap).toString();
							}
							all.add(resultObject);
						} else {
							String resultMsg = obj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultMsg");
							return BSSHelper.getErrInfo(resCode, resultMsg).toString();
						}
						allobj.put("all", all);
						LOGGER.debug("根据接入号查询销售品resultObject节点:{}", allobj);
						IResponseParamTransform transformBean = (IResponseParamTransform) SpringUtil
								.getBean(ResponseBusinessTypeEnum.getBeanName(19501));
						return transformBean.transform(allobj);
					} else {
						return BSSHelper.getErrInfo("99", "未查询到接入号的服务规格，无法查到接入号的套餐信息").toString();
					}
				} else {
					String resultMsg = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultMsg");
					return BSSHelper.getErrInfo(resCode, resultMsg).toString();
				}
				
				
			} else if ("2".equals(accNbrType)) {//根据产品实例id查询
				accProdInstId = body.getString("accNbr");
				accProdInstIds.add(accProdInstId);
				QryOfferInstByAccProdInstId qryOfferInstByAccProdInstId = new QryOfferInstByAccProdInstId(offerIds, roleId, regionId, Constants.NO_FINISH, isDep, accProdInstIds);
				qryOfferInstByAccProdInstId.addScopeInfos(QryOfferInstScopeInfoEnum.OFFER_INST);
				qryOfferInstByAccProdInstId.addScopeInfos(QryOfferInstScopeInfoEnum.OFFER_PROD_INST_REL);
				qryOfferInstByAccProdInstId.addScopeInfos(QryOfferInstScopeInfoEnum.OFFER_INST_ATTR);
				
				JSONObject offerInstObj = qryOfferInstByAccProdInstId.generateContractRoot();
				redisOperation.incr(Constant.js_intf_qryOfferInst + head.getString("areaCode") + DateUtils.getNOWDate());
				String res = HttpClientUtils.doPostJSONHttps(BSSUrlConstants.EOP_QRY_OFFERINST, offerInstObj.toString());
				LOGGER.debug("根据产品实例id查询销售品.:{}", res);
				// 查询产品实例出现异常，直接返回
				if (StringUtils.isBlank(res)) {
					String resultMsg = Constants.ERR_NULL;
					return BSSHelper.getErrInfo("99", resultMsg).toString();
				}
				JSONObject resObj = JSON.parseObject(res);
				String resCode = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultCode");
				JSONObject resultObject = null;
				if (Constants.NUM_ZERO.equals(resCode)) {
					resultObject = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getJSONObject("resultObject");
					if (resultObject == null) {
						return BSSHelper.getErrInfo("01", "未查到销售品", returnNoResultMap).toString();
					}
				} else {
					String resultMsg = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultMsg");
					return BSSHelper.getErrInfo(resCode, resultMsg).toString();
				}
				LOGGER.debug("据产品实例id查询销售品resultObject节点: {}", resultObject);
				IResponseParamTransform transformBean = (IResponseParamTransform) SpringUtil.getBean(ResponseBusinessTypeEnum.getBeanName(19500));
				return transformBean.transform(resultObject);
				
				
			} else if ("3".equals(accNbrType)) {//根据客户id查询
				ownerCustId = body.getString("accNbr");
				QryOfferInstByOwnerCustId qryOfferInstByAccProdInstId = new QryOfferInstByOwnerCustId(offerIds, roleId, regionId,
						Constants.NO_FINISH, isDep, ownerCustId);
				qryOfferInstByAccProdInstId.addScopeInfos(QryOfferInstScopeInfoEnum.OFFER_INST);
				qryOfferInstByAccProdInstId.addScopeInfos(QryOfferInstScopeInfoEnum.OFFER_PROD_INST_REL);
				qryOfferInstByAccProdInstId.addScopeInfos(QryOfferInstScopeInfoEnum.OFFER_INST_ATTR);
				
				JSONObject offerInstObj = qryOfferInstByAccProdInstId.generateContractRoot();
				redisOperation.incr(Constant.js_intf_qryOfferInst + head.getString("areaCode") + DateUtils.getNOWDate());
				String res = HttpClientUtils.doPostJSONHttps(BSSUrlConstants.EOP_QRY_OFFERINST, offerInstObj.toString());
				LOGGER.debug("根据客户id查询销售品: {}", res);
				// 查询产品实例出现异常，直接返回
				if (StringUtils.isBlank(res)) {
					String resultMsg = Constants.ERR_NULL;
					return BSSHelper.getErrInfo("99", resultMsg).toString();
				}
				JSONObject resObj = JSON.parseObject(res);
				String resCode = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultCode");
				JSONObject resultObject = null;
				if (Constants.NUM_ZERO.equals(resCode)) {
					resultObject = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getJSONObject("resultObject");
					if (resultObject == null) {
						return BSSHelper.getErrInfo("01", "未查到销售品", returnNoResultMap).toString();
					}
				} else {
					String resultMsg = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultMsg");
					return BSSHelper.getErrInfo(resCode, resultMsg).toString();
				}
				LOGGER.debug("根据客户id查询销售品resultObject节点:{}", resultObject);
				IResponseParamTransform transformBean = (IResponseParamTransform) SpringUtil
						.getBean(ResponseBusinessTypeEnum.getBeanName(19500));
				return transformBean.transform(resultObject);
			} else if ("4".equals(accNbrType)) {//根据销售品实例查询
				offerId = body.getString("accNbr");
				QryOfferInstByOfferInstId qryOfferInstByOfferInstId = new QryOfferInstByOfferInstId(offerIds, roleId, regionId, Constants.NO_FINISH, isDep, offerId);
				qryOfferInstByOfferInstId.addScopeInfos(QryOfferInstScopeInfoEnum.OFFER_INST);
				qryOfferInstByOfferInstId.addScopeInfos(QryOfferInstScopeInfoEnum.OFFER_PROD_INST_REL);
				qryOfferInstByOfferInstId.addScopeInfos(QryOfferInstScopeInfoEnum.OFFER_INST_ATTR);
				
				JSONObject offerInstObj = qryOfferInstByOfferInstId.generateContractRoot();
				
				String res = "";
				LOGGER.debug("根据产品实例id查询销售品:{}", offerInstObj);
				redisOperation.incr(Constant.js_intf_qryOfferInst + head.getString("areaCode") + DateUtils.getNOWDate());
				res = HttpClientUtils.doPostJSONHttps(BSSUrlConstants.EOP_QRY_OFFERINST, offerInstObj.toString());
				LOGGER.debug("根据产品实例id查询销售品:{}", res);
				// 查询产品实例出现异常，直接返回
				JSONObject resObj = JSON.parseObject(res);
				if (StringUtils.isBlank(res)) {
					String resultMsg = Constants.ERR_NULL;
					return BSSHelper.getErrInfo("99", resultMsg).toString();
				}
				String resCode = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultCode");
				JSONObject resultObject = null;
				if (Constants.NUM_ZERO.equals(resCode)) {
					resultObject = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getJSONObject("resultObject");
					if (resultObject == null) {
						return BSSHelper.getErrInfo("01", "未查到销售品", returnNoResultMap).toString();
					}
					if (StringUtils.isBlank(resultObject.toString())) {
						String resultMsg = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultMsg");
						return BSSHelper.getErrInfo(resCode, resultMsg).toString();
					}
				} else {
					String resultMsg = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultMsg");
					return BSSHelper.getErrInfo(resCode, resultMsg).toString();
				}
				LOGGER.debug("据产品实例id查询销售品resultObject节点:{}", resultObject);
				IResponseParamTransform transformBean = (IResponseParamTransform) SpringUtil
						.getBean(ResponseBusinessTypeEnum.getBeanName(19500));
				return transformBean.transform(resultObject);
			} else if ("5".equals(accNbrType)) {// 专为已办业务所做，传接入号和接入类型
				LOGGER.debug("进入已办业务专用分支");
				accNum = body.getString("accNbr");
				String prodId = body.getString("prodId");
				JSONObject allobj = new JSONObject();
				JSONArray all = new JSONArray();
				QryOfferInstByAccNum qryOfferInstByAccNum = new QryOfferInstByAccNum(offerIds, roleId, regionId, Constants.NO_FINISH, isDep, accNum, prodId);
				qryOfferInstByAccNum.addScopeInfos(QryOfferInstScopeInfoEnum.OFFER_INST);
				qryOfferInstByAccNum.addScopeInfos(QryOfferInstScopeInfoEnum.OFFER_PROD_INST_REL);
				JSONObject offerInstObj = qryOfferInstByAccNum.generateContractRoot();
				redisOperation.incr(Constant.js_intf_qryOfferInst + head.getString("areaCode") + DateUtils.getNOWDate());
				String res = HttpClientUtils.doPostJSONHttps(BSSUrlConstants.EOP_QRY_OFFERINST, offerInstObj.toString());
				LOGGER.debug("根据接入号查询销售品:{}", res);
				JSONObject obj = JSON.parseObject(res);
				if (StringUtils.isBlank(res)) {
					String resultMsg = Constants.ERR_NULL;
					return BSSHelper.getErrInfo("99", resultMsg).toString();
				}
				String resCode2 = obj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultCode");
				if (Constants.NUM_ZERO.equals(resCode2)) {
					JSONObject resultObject = obj.getJSONObject("contractRoot").getJSONObject("svcCont").getJSONObject("resultObject");
					if (resultObject == null) {
						return BSSHelper.getErrInfo("01", "未查到销售品", returnNoResultMap).toString();
					}
					all.add(resultObject);
				} else {
					String resultMsg = obj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultMsg");
					return BSSHelper.getErrInfo("01", resultMsg).toString();
				}
				allobj.put("all", all);
				LOGGER.debug("根据接入号查询销售品resultObject节点:{}", allobj);
				IResponseParamTransform transformBean = (IResponseParamTransform) SpringUtil
						.getBean(ResponseBusinessTypeEnum.getBeanName(19501));
				return transformBean.transform(allobj);
			}
		} catch (Exception e) {
			return BSSHelper.getErrInfo("99", "服务内部错误").toString();
		}
		return null;
		
	}
	
	/**
	 * Description:智慧查询销售品规格详情 195531 <br>
	 *
	 * @param head
	 * @param body
	 * @return String<br>
	 * @author yangyang<br>
	 * 2018年8月27日
	 */
	public String qryOfferSpecDetail(JSONObject head, JSONObject body) {
		try {
			// 映射企信测地市
			String regionId = StaticDataMappingUtil.getRegionByAreaCode(head.getString("areaCode"));
			String offerSpecId = body.getString("offerSpecId");
			String offerNbr = body.getString("offerNbr");
			List<String> statusCds = null;
			if (body.containsKey("statusCds")) {
				statusCds = JSON.parseArray(body.getJSONArray("statusCds").toString(), String.class);
			}
			QryOfferDetailByOfferId qryOfferDetailByOfferId = new QryOfferDetailByOfferId(regionId, offerSpecId, offerNbr, statusCds);
			JSONObject qryOfferDetail = qryOfferDetailByOfferId.generateContractRoot();
			String res = HttpClientUtils.doPostJSONold(BSSUrlConstants.EOP_QRY_OFFER_DETAIL, qryOfferDetail.toString());
			LOGGER.debug("智慧查询销售品规格详情 195531:{}", res);
			if (StringUtils.isBlank(res)) {
				String resultMsg = Constants.ERR_NULL;
				return BSSHelper.getErrInfo("99", resultMsg).toString();
			}
			JSONObject resObj = JSON.parseObject(res);
			String resCode = resObj.getString("resultCode");
			if (Constants.NUM_ZERO.equals(resCode)) {
				JSONObject resultObject = resObj.getJSONObject("resultObject");
				JSONObject offerDetail = resultObject.getJSONObject("offerDetail");
				
				if (offerDetail != null) {
					IResponseParamTransform transformBean = (IResponseParamTransform) SpringUtil.getBean(ResponseBusinessTypeEnum.getBeanName(45));
					return transformBean.transform(offerDetail);
				} else {
					return BSSHelper.getErrInfo("99", "未查询到接入号的服务规格，无法查到接入号的套餐信息").toString();
				}
			} else {
				String resultMsg = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultMsg");
				return BSSHelper.getErrInfo(resCode, resultMsg).toString();
			}
		} catch (Exception e) {
			return BSSHelper.getErrInfo("99", "服务内部错误").toString();
		}
		
	}
	
	public String qryGeneralTaxpayer(JSONObject head, JSONObject body) {
		try {
			// 映射企信测地市
			String regionId = StaticDataMappingUtil.getRegionByAreaCode(head.getString("areaCode"));
			String custId = body.getString("custId");
			QryGeneralTaxpayer qryGeneralTaxpayer = new QryGeneralTaxpayer(regionId, custId);
			JSONObject qryGeneralTaxpayerJson = qryGeneralTaxpayer.generateContractRoot();
			String res = HttpClientUtils.doPostJSONHttps(BSSUrlConstants.EOP_QRY_TAX_PLAYER, qryGeneralTaxpayerJson.toString());
			LOGGER.debug("查询纳税人信息: {}", res);
			if (StringUtils.isBlank(res)) {
				String resultMsg = Constants.ERR_NULL;
				return BSSHelper.getErrInfo("99", resultMsg).toString();
			}
			JSONObject svcCont = JSON.parseObject(res).getJSONObject("contractRoot").getJSONObject("svcCont");
			String resCode = svcCont.getString("resultCode");
			if (Constants.NUM_ZERO.equals(resCode)) {
				JSONObject resultObject = svcCont.getJSONObject("resultObject");
				if (resultObject != null) {
					IResponseParamTransform transformBean = (IResponseParamTransform) SpringUtil.getBean(ResponseBusinessTypeEnum.getBeanName(190718));
					return transformBean.transform(resultObject);
				} else {
					return BSSHelper.getErrInfo("99", "未查询到一般纳税人").toString();
				}
			} else {
				String resultMsg = svcCont.getString("resultMsg");
				return BSSHelper.getErrInfo(resCode, resultMsg).toString();
			}
		} catch (Exception e) {
			return BSSHelper.getErrInfo("99", "服务内部错误").toString();
		}
	}
	
	public String qrySpecialListJs(JSONObject head, JSONObject body) {
		try {
			// 映射企信测地市
			String regionId = StaticDataMappingUtil.getRegionByAreaCode(head.getString("areaCode"));
//            String custId = body.getString("custId")
			String partyId = body.getString("partyId");
			String certNum = body.getString("certNum");
			String certType = body.getString("certType");
			QrySpecialListJs qrySpecialListJs = new QrySpecialListJs(regionId, partyId, certNum, certType);
			JSONObject qrySpecialListJsJson = qrySpecialListJs.generateContractRoot();
			LOGGER.debug("查询黑名单qrySpecialListJsJson:{}", qrySpecialListJsJson);
			String res = HttpClientUtils.doPostJSONHttps(BSSUrlConstants.EOP_QRY_SPECIAL_LIST, qrySpecialListJsJson.toString());
			LOGGER.debug("查询黑名单rest:{}", res);
			if (StringUtils.isBlank(res)) {
				String resultMsg = Constants.ERR_NULL;
				return BSSHelper.getErrInfo("99", resultMsg).toString();
			}
			JSONObject svcCont = JSON.parseObject(res).getJSONObject("contractRoot").getJSONObject("svcCont");
			String resCode = svcCont.getString("resultCode");
			if (Constants.NUM_ZERO.equals(resCode)) {
				JSONObject resultObject = svcCont.getJSONObject("resultObject");
				if (resultObject != null) {
					IResponseParamTransform transformBean = (IResponseParamTransform) SpringUtil.getBean(ResponseBusinessTypeEnum.getBeanName(190801));
					return transformBean.transform(resultObject);
				} else {
					return BSSHelper.getErrInfo("99", "未查询到黑名单").toString();
				}
			} else {
				String resultMsg = svcCont.getString("resultMsg");
				return BSSHelper.getErrInfo(resCode, resultMsg).toString();
			}
		} catch (Exception e) {
			return BSSHelper.getErrInfo("99", "服务内部错误").toString();
		}
	}
	
	/**
	 * Description:查询宽带地址详情 <br>
	 *
	 * @param head
	 * @param body
	 * @return String<br>
	 * @author yangyang<br>
	 * 2018年11月20日
	 */
	public String qryAdslProdInst(JSONObject head, JSONObject body) {
		try {
			// 映射企信测地市
			String regionId = StaticDataMappingUtil.getRegionByAreaCode(head.getString("areaCode"));
			//body区入参，accNbrType=1代表是手机号，2代表身份证   accNbr是具体的手机号或者身份证
			String accNbrType = body.getString("accNbrType");
			String accNbr = body.getString("accNbr");
			String telephone = "";
			String identityId = "";
			if ("1".equals(accNbrType)) {
				telephone = accNbr;
			} else {
				identityId = accNbr;
			}
			String response = redisOperation.getForString("tenyear" + "_" + head.getString("areaCode") + "_" + accNbrType + "_" + accNbr);
			if (CharSequenceUtil.isNotBlank(response)) {
				return response;
			}
			QryAdslProdInstByAccNbrOrCustCertNbr qryAdslProdInstByAccNbrOrCustCertNbr = new QryAdslProdInstByAccNbrOrCustCertNbr(regionId, telephone, identityId);
			JSONObject qryAdslInfo = qryAdslProdInstByAccNbrOrCustCertNbr.generateContractRoot();
			
			String res = HttpClientUtils.doPostJSON(BSSUrlConstants.EOP_QRY_ASDL_INFO, qryAdslInfo.toString());
			LOGGER.debug("查询十年宽带信息:{}", res);
			if (StringUtils.isBlank(res)) {
				String resultMsg = Constants.ERR_NULL;
				return BSSHelper.getErrInfo("99", resultMsg).toString();
			}
			JSONObject resObj = JSON.parseObject(res);
			String resCode = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultCode");
			if (Constants.NUM_ZERO.equals(resCode)) {
				JSONObject resultObject = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getJSONObject("resultObject");
				JSONArray adslProdInsts = resultObject.getJSONArray("adslProdInsts");
				if (adslProdInsts != null && !adslProdInsts.isEmpty()) {
					IResponseParamTransform transformBean = (IResponseParamTransform) SpringUtil
							.getBean(ResponseBusinessTypeEnum.getBeanName(181120));
					String responseResult = transformBean.transform(resultObject);
					redisOperation.setEx("tenyear" + "_" + head.getString("areaCode") + "_" + accNbrType + "_" + accNbr, 6 * 60 * 60, responseResult);
					return responseResult;
				} else {
					return BSSHelper.getErrInfo("99", "未查询到宽带信息").toString();
				}
			} else {
				String resultMsg = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultMsg");
				return BSSHelper.getErrInfo(resCode, resultMsg).toString();
			}
		} catch (Exception e) {
			return BSSHelper.getErrInfo("99", "服务内部错误").toString();
		}
	}
	
	public String checkMNP(Map insertMap, String portOutNetId, String phoneNumber, String serviceType, String areaCode, Map certMap, String staffId, String channelId) {
		try {
			// 映射企信测地市
			String regionId = StaticDataMappingUtil.getRegionByAreaCode(areaCode);
			String credType = StaticDataMappingUtil.getMNPCertType(certMap.get("certType").toString());
			String credNumber = certMap.get("certNum").toString();
			String custName = certMap.get("custName").toString();
			insertMap.put("phoneNumber", phoneNumber);
			insertMap.put("portOutNetId", portOutNetId);
			insertMap.put("credType", credType);
			insertMap.put("credNumber", credNumber);
			insertMap.put("staffId", staffId);
			insertMap.put("channelId", channelId);
			AuthCodeReqBase authCodeReqBase = new AuthCodeReqBase(regionId, "AUTHCODE_REQ", serviceType, phoneNumber, portOutNetId, credType, credNumber, custName, staffId, channelId);
			JSONObject authCodeReq = authCodeReqBase.generateContractRoot();
			LOGGER.debug("authCodeReq:  {} ", authCodeReq);
			String res = HttpClientUtils.doPostJSONHttps(BSSUrlConstants.EOP_CHECK_MNP, authCodeReq.toString());
			LOGGER.debug(res);
			if (StringUtils.isBlank(res)) {
				insertMap.put("returnCode", "99");
				insertMNPRecord(insertMap);
				String resultMsg = Constants.ERR_NULL;
				return BSSHelper.getErrInfo("99", resultMsg).toString();
			}
			JSONObject resObj = JSON.parseObject(res);
			String resCode = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultCode");
			insertMap.put("resultCode", resCode);
			Map<String, String> map = new HashMap<>();
			String resultMsg = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultMsg");
			JSONArray ruleInfos = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getJSONArray("ruleInfos");
			if (ruleInfos != null) {
				insertMap.put("ruleInfos", ruleInfos.toString());
				map.put("ruleInfos", ruleInfos.toString());
			}
			if (resultMsg != null) {
				insertMap.put("resultMsg", resultMsg);
			}
			if (Constants.NUM_ZERO.equals(resCode)) {
				JSONObject resultObject = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getJSONObject("resultObject");
				//获取到正确的值，则存入数据库，并返回
				String messageId = resultObject.getString("messageId");
				insertMap.put("messageId", messageId);
				insertMap.put("returnCode", "00");
				mnpCheckMapper.insertMNPRecord(insertMap);
				map.put("messageId", messageId);
				return BSSHelper.getErrInfo("00", "获取成功", map).toString();
			} else {
				insertMap.put("returnCode", resCode);
				mnpCheckMapper.insertMNPRecord(insertMap);
				return BSSHelper.getErrInfo(resCode, resultMsg, map).toString();
			}
		} catch (Exception e) {
			try {
				LOGGER.info("SmartBssSystemImpl.check,phoneNumber={}, message={}", phoneNumber, e.getMessage());
				LOGGER.info("SmartBssSystemImpl.check,phoneNumber={}, insertMap={}", phoneNumber, insertMap);
				insertMap.put("returnCode", "99");
				LOGGER.info("SmartBssSystemImpl.check,phoneNumber={}, insertMap={}", phoneNumber, insertMap);
				insertMNPRecord(insertMap);
			} catch (Exception e1) {
				LOGGER.error(e.getMessage());
			}
			return BSSHelper.getErrInfo("99", "服务内部错误").toString();
		}
	}
	
	public void insertMNPRecord(Map insertMap) {
		mnpCheckMapper.insertMNPRecord(insertMap);
	}
	
	public String removeOutAbility(Map insertMap, String portOutNetId, String phoneNumber, String serviceType, String areaCode, Map certMap, String staffId, String channelId) {
		try {
			// 映射企信测地市
			String regionId = StaticDataMappingUtil.getRegionByAreaCode(areaCode);
			String credType = StaticDataMappingUtil.getMNPCertType(certMap.get("certType").toString());
			String credNumber = certMap.get("certNum").toString();
			String custName = certMap.get("custName").toString();
			insertMap.put("phoneNumber", phoneNumber);
			insertMap.put("portOutNetId", portOutNetId);
			insertMap.put("credType", credType);
			insertMap.put("credNumber", credNumber);
			insertMap.put("staffId", staffId);
			insertMap.put("channelId", channelId);
			RemoveOutAbilityReqBase removeOutAbilityReqBase = new RemoveOutAbilityReqBase(regionId, "AUTHCODE_REQ", serviceType, phoneNumber, portOutNetId, credType, credNumber, custName, staffId, channelId);
			JSONObject removeOutAbilityReq = removeOutAbilityReqBase.generateContractRoot();
			LOGGER.debug("removeOutAbilityReq:   {} ", removeOutAbilityReq);
			String res = HttpClientUtils.doPostJSONHttps(BSSUrlConstants.EOP_MNP_RM_OUTABLE, removeOutAbilityReq.toString());
			LOGGER.debug(res);
			if (StringUtils.isBlank(res)) {
				insertMap.put("returnCode", "99");
				insertMNPRmOutableRecord(insertMap);
				String resultMsg = Constants.ERR_NULL;
				return BSSHelper.getErrInfo("99", resultMsg).toString();
			}
			JSONObject resObj = JSON.parseObject(res);
			String resCode = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultCode");
			insertMap.put("resultCode", resCode);
			Map<String, String> map = new HashMap<>();
			String resultMsg = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultMsg");
			JSONArray ruleInfos = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getJSONArray("ruleInfos");
			if (ruleInfos != null) {
				insertMap.put("ruleInfos", ruleInfos.toString());
				map.put("ruleInfos", ruleInfos.toString());
			}
			if (resultMsg != null) {
				insertMap.put("resultMsg", resultMsg);
			}
			if (Constants.NUM_ZERO.equals(resCode)) {
				JSONObject resultObject = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getJSONObject("resultObject");
				//获取到正确的值，则存入数据库，并返回
				if (resultObject != null) {
					String messageId = resultObject.getString("messageId");
					
					insertMap.put("messageId", messageId);
					map.put("messageId", messageId);
				}
				insertMap.put("returnCode", "00");
				insertMNPRmOutableRecord(insertMap);
				return BSSHelper.getErrInfo("00", resultMsg, map).toString();
			} else {
				
				insertMap.put("returnCode", resCode);
				insertMNPRmOutableRecord(insertMap);
				return BSSHelper.getErrInfo(resCode, resultMsg, map).toString();
			}
		} catch (Exception e) {
			try {
				insertMap.put("returnCode", "99");
				insertMNPRmOutableRecord(insertMap);
			} catch (Exception e1) {
				LOGGER.error(e1.getMessage());
			}
			return BSSHelper.getErrInfo("99", "服务内部错误").toString();
		}
	}
	
	public void insertMNPRmOutableRecord(Map insertMap) {
		mnpCheckMapper.insertMNPRmOutableRecord(insertMap);
	}
	
	public void insertNewSaleMNPRecord(Map insertMap) {
		mnpCheckMapper.insertNewSaleMNPRecord(insertMap);
	}
	
	public void insertNewSaleMNPAbilityRecord(Map insertMap) {
		mnpCheckMapper.insertNewSaleMNPAbilityRecord(insertMap);
	}
	
	public String checkMNPAbility(String phoneNumber, String areaCode, String certNum, String custName, String staffId, String channelId) {
		try {
			// 映射企信测地市
			String regionId = StaticDataMappingUtil.getRegionByAreaCode(areaCode);
			PortOutAbilityBase portOutAbilityBase = new PortOutAbilityBase(regionId, phoneNumber, certNum, custName, staffId, channelId);
			JSONObject portOutAbility = portOutAbilityBase.generateContractRoot();
			
			String res = HttpClientUtils.doPostJSONHttps(BSSUrlConstants.EOP_CHECK_MNP_ABILITY, portOutAbility.toString());
			if (StringUtils.isBlank(res)) {
				String resultMsg = Constants.ERR_NULL;
				return BSSHelper.getErrInfo("99", resultMsg).toString();
			}
			JSONObject resObj = JSON.parseObject(res);
			String resCode = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultCode");
			Map<String, String> insertMap = new HashMap<>();
			insertMap.put("phoneNumber", phoneNumber);
			insertMap.put("credNumber", certNum);
			insertMap.put("staffId", staffId);
			insertMap.put("channelId", channelId);
			insertMap.put("resultCode", resCode);
			Map<String, String> map = new HashMap<>();
			String resultMsg = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultMsg");
			JSONArray ruleInfos = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getJSONArray("ruleInfos");
			if (ruleInfos != null) {
				insertMap.put("ruleInfos", ruleInfos.toString());
				map.put("ruleInfos", ruleInfos.toString());
			}
			if (resultMsg != null) {
				insertMap.put("resultMsg", resultMsg);
			}
			if (Constants.NUM_ZERO.equals(resCode)) {
				//获取到正确的值，则存入数据库，并返回
				mnpCheckMapper.insertMNPAbilityRecord(insertMap);
				return BSSHelper.getErrInfo("00", "获取成功", map).toString();
			} else {
				mnpCheckMapper.insertMNPAbilityRecord(insertMap);
				return BSSHelper.getErrInfo(resCode, resultMsg, map).toString();
			}
		} catch (Exception e) {
			return BSSHelper.getErrInfo("99", "服务内部错误").toString();
		}
	}
	
	public String postMNPAuthCode(JSONObject body) {
		JSONObject sendJson = new JSONObject();
		try {
			JSONObject requestObject = body.getJSONObject("requestObject");
			String npCode = requestObject.getString("npCode");
			String origMessageId = requestObject.getString("origMessageId");
			String authCode = requestObject.getString("authCode");
			String expired = requestObject.getString("expired");
			String signalledCode = requestObject.getString("signalledCode");
			String signalledMessage = requestObject.getString("signalledMessage");
			String newMessageId = requestObject.getString("messageId");
			Map<String, String> updateMap = new HashMap<>();
			updateMap.put("npCode", npCode);
			updateMap.put("origMessageId", origMessageId);
			updateMap.put("signalledCode", signalledCode);
			updateMap.put("newMessageId", newMessageId);
			sendJson.put("smsCalledNbr", npCode);
			sendJson.put("messageId", origMessageId);
			sendJson.put("newMessageId", newMessageId);
			if (Constants.NUM_TWO_HUNDRED.equals(signalledCode)) {
				//发送给号码
				sendJson.put("templateId", "87");
				sendJson.put("param1", authCode);
				sendJson.put("param2", expired);
				String responseStr = HttpClientUtils.doPostJSONold(DXURL, sendJson.toString());
				if (StringUtils.isBlank(responseStr)) {
					responseStr = HttpClientUtils.doPostJSONold(DXURL, sendJson.toString());
					if (StringUtils.isBlank(responseStr)) {
						(new SendMnpAlmThread(npCode)).start();
					}
				}
				if (!StringUtils.isBlank(responseStr)) {
					JSONObject responseDX = JSON.parseObject(responseStr);
					updateMap.put("dxStatus", responseDX.getString("result"));
					updateMap.put("dxMsg", responseDX.getString("resultMsg"));
				}
//              String message="尊敬的客户您好！您申请的携出授权码为【"+authCode+"】,有效期到【"+expired+"】。您需在有效期内携带有效身份证件前往拟携入运营企业当地营业厅办理，逾期办理需重新申请授权码。在授权码有效时间内您只能办理挂失和解挂业务，解挂之后需要重新申请授权码。携出后，原有客户积分可能受影响，部分电信业务可能无法继续使用，请您提前做好积分兑换与备份工作。"
//              RunExcelFile.sendDX(npCode,message)
				updateMap.put("authCode", authCode);
				updateMap.put("expired", expired);
				mnpCheckMapper.updateMnpRecord(updateMap);
				mnpCheckMapper.insertAuthCodeRecord(updateMap);
				/**
				 *
				 if(i==0){
				 return BSSHelper.getErrInfo("01", "没有找到对应的origMessageId").toString();
				 }else{
				 return BSSHelper.getErrInfo("00", "success").toString();
				 }
				 */
				return BSSHelper.getErrInfo("00", "success").toString();
			} else {
//                String message="尊敬的客户您好！因携号转网系统异常，暂无法受理业务，给您带来不便，敬请谅解。"
				String templateId = "88";
				//发送给号码
				if ("604".equals(signalledCode)) {
//                    message="尊敬的客户您好！因系统维护今日不受理此业务,详情请咨询10000。"
					templateId = "89";
				} else if ("648".equals(signalledCode)) {
//                    message="尊敬的客户您好！因系统故障您的申请未能通过，请联系10000处理。"
					templateId = "90";
				} else if ("694".equals(signalledCode)) {
//                    message="尊敬的客户您好！您已经在携入方提交办理携号转网服务，不需再申请授权码。"
					templateId = "91";
				} else if ("695".equals(signalledCode)) {
//                    message="尊敬的客户您好！您的申请正在处理中，请耐心等待。"
					templateId = "92";
				} else if ("600".equals(signalledCode)) {
					//尊敬的客户您好！您的号码入网时间未达规定值，暂无法办理携号转网，请您于X年X月X日后再查询。详情请咨询10000。
					templateId = "93";
					/**
					 //查询入网时间，然后组装120天
					 //                    body.clear();
					 //                    body.put("accNbr", npCode);
					 //                    body.put("queryType", "2");
					 //                    body.put("queryMode", "1");
					 //                    body.put("accNbrType", "9");//现在默认是9手机号码携转
					 //                    //待补充查询15050查询入网时间+120天，拼X年X月X日
					 //                    BaseFacadeImpl.getZTParamsWithNoHead(npCode, areaCode, body, actionCode)
					 */
				} else if ("603".equals(signalledCode)) {
					//尊敬的客户您好！根据携号转网管理政策，您的号码暂不支持携号转网。详情请咨询10000。
					templateId = "94";
				} else if ("605".equals(signalledCode)) {//快速携回不满足
					//尊敬的客户您好！您的号码暂不满足快速携回。详情请咨询10000。
					templateId = "95";
				}
				sendJson.put("templateId", templateId);
				String responseStr = HttpClientUtils.doPostJSONold(DXURL, sendJson.toString());
				if (StringUtils.isBlank(responseStr)) {
					responseStr = HttpClientUtils.doPostJSONold(DXURL, sendJson.toString());
					if (StringUtils.isBlank(responseStr)) {
						(new SendMnpAlmThread(npCode)).start();
					}
				}
				LOGGER.debug("短厅返回responseStr:{}", responseStr);
				if (!StringUtils.isBlank(responseStr)) {
					JSONObject responseDX = JSON.parseObject(responseStr);
					updateMap.put("dxStatus", responseDX.getString("result"));
					updateMap.put("dxMsg", responseDX.getString("resultMsg"));
				}
//                RunExcelFile.sendDX(npCode,message)
				updateMap.put("signalledMessage", signalledMessage);
				mnpCheckMapper.updateMnpRecord(updateMap);
				mnpCheckMapper.insertAuthCodeRecord(updateMap);
				/**
				 //                if(i==0){
				 //                    return BSSHelper.getErrInfo("01", "没有找到对应的origMessageId").toString();
				 //                }else{
				 //                    return BSSHelper.getErrInfo("00", "success").toString();
				 //                }
				 */
				return BSSHelper.getErrInfo("00", "success").toString();
			}
		} catch (Exception e) {
			return BSSHelper.getErrInfo("99", "服务内部错误").toString();
		}
	}
	
	
	/**
	 * Description: <br>
	 *
	 * @return String<br>
	 * @author yangyang<br>
	 * 2020年7月3日
	 */
	public String getsmartHMAccNbr(String netAccNbr, JSONObject head, Set smartSet) {
		JSONObject body = new JSONObject();
		body.put("accNbr", netAccNbr);
		body.put("queryMode", "1");
		body.put("accNbrType", "100000009");
		String result = qryProdInst(head, body);
		JSONObject resultJson = JSON.parseObject(result);
		JSONObject resultHead = resultJson.getJSONObject("head");
		String smartAccnbr = "";
		if ("00".equals(resultHead.getString("status"))) {
			JSONObject resultBody = resultJson.getJSONObject("body");
			JSONArray accProdInstDetailList = resultBody.getJSONObject("resultObject").getJSONArray("accProdInstDetailList");
			if (!accProdInstDetailList.isEmpty()) {
				LOGGER.debug("prodInstRels  size:" + 2);
				JSONObject accProdInstDetail = accProdInstDetailList.getJSONObject(0).getJSONObject("accProdInstDetail");
				LOGGER.debug("prodInstRels   :{}", accProdInstDetail.getJSONArray("prodInstRels"));
				if ((accProdInstDetail.containsKey("prodInstRels")) && (!accProdInstDetail.getJSONArray("prodInstRels").isEmpty())) {
					JSONArray prodInstRels = accProdInstDetail.getJSONArray("prodInstRels");
					LOGGER.debug("prodInstRels  size:{}", prodInstRels.size());
					for (int i = 0; i < prodInstRels.size(); i++) {
						if ("100001090".equals(prodInstRels.getJSONObject(i).getString("zProdId"))) {//智慧家庭的号
							LOGGER.debug("智慧家庭的号码找到了!!!");
							smartSet.add(prodInstRels.getJSONObject(i).getString("zAccNum"));
							smartAccnbr = prodInstRels.getJSONObject(i).getString("zAccNum");
						}
					}
				}
			}
		}
		return !smartSet.isEmpty() ? smartAccnbr : null;
	}
	
	/**
	 * 根据宽带账号和地市编码，查询宽带接入号
	 *
	 * @param netAccount 宽带账号
	 * @param areaCode   地市编码
	 * @return 宽带接入号
	 */
	public String getAccNumByNetAccount(String netAccount, String areaCode) {
		JSONObject head = new JSONObject();
		head.put("areaCode", areaCode);
		JSONObject body = new JSONObject();
		body.put("accNbr", netAccount);
		body.put("queryMode", "1");
		body.put("accNbrType", "2");
		//查询产品实例接口
		String result = qryProdInst(head, body);
		JSONObject proResJSON = JSON.parseObject(result);
		if ("00".equals(proResJSON.getJSONObject("head").getString("status"))) {
			JSONArray accProdInstDetailList = proResJSON.getJSONObject("body").getJSONObject("resultObject").getJSONArray("accProdInstDetailList");
			if (!accProdInstDetailList.isEmpty()) {
				return accProdInstDetailList.getJSONObject(0).getJSONObject("accProdInstDetail").getString("accNum");
			}
		} else {
			return BSSHelper.getErrInfo("01", "没有查到对应宽带接入号").toString();
		}
		return "";
	}
	
	public static String qryCustomerOrderByType(String regionId, String custId, String prodInstId, String accNum, String isSendAutograph, String url) {
		QryOrderByType qryOrderByType = new QryOrderByType(regionId);
		qryOrderByType.setCustId(custId);
		qryOrderByType.setProdInstId(prodInstId);
		qryOrderByType.setAccNum(accNum);
		qryOrderByType.setIsSendAutograph(isSendAutograph);
		JSONObject custObj = qryOrderByType.generateContractRoot();
		
		LOGGER.info("qryCustomerOrderByType中查询客户未签名购物车的入参：{}", custObj);
		
		String res = HttpClientUtils.doPostJSONold(url, custObj.toString());
		LOGGER.info("qryCustomerOrderByType中查询客户未签名购物车的出参：{}", res);
		// custId查询出现异常，直接返回
		JSONObject resObj = JSON.parseObject(res);
		if (StringUtils.isBlank(res)) {
			String resultMsg = Constants.ERR_NULL;
			return BSSHelper.getErrInfo("99", resultMsg).toString();
		}
		String resCode = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultCode");
		JSONObject resJson = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getJSONObject("resultObject");
		JSONObject resultObject2 = new JSONObject();
		if (Constants.NUM_ZERO.equals(resCode)) {
			IResponseParamTransform transformBean = (IResponseParamTransform) SpringUtil
					.getBean(ResponseBusinessTypeEnum.getBeanName(191202));
			return transformBean.transform(resJson);
		} else {
			String resultMsg = resultObject2.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultMsg");
			return BSSHelper.getErrInfo(resCode, resultMsg).toString();
		}
	}
	
	public String queryRemoteSignInfo(JSONObject object, String regionId) {
		String sysSource = object.getString("sysSource");
		String channelId = object.getString("channelId");
		String staffCode = object.getString("staffCode");
		String sysSourceSerial = object.getString("sysSourceSerial");
		JSONArray queryDealSeriNum = object.getJSONArray("queryDealSeriNum");
		
		QryRemoteSignInfo qryRemoteSignInfo = new QryRemoteSignInfo(regionId, channelId, staffCode, sysSource);
		qryRemoteSignInfo.setSysSource(sysSource);
		qryRemoteSignInfo.setSysSourceSerial(sysSourceSerial);
		qryRemoteSignInfo.setQueryDealSeriNum(queryDealSeriNum);
		JSONObject paramJson = qryRemoteSignInfo.generateContractRoot();
		
		LOGGER.info("queryRemoteSignInfo中查询客户未签名购物车的入参：{}", paramJson);
		String res = HttpClientUtils.doPostJSONHttpsNewEOP(BSSUrlConstants.EOP_QRY_REMOTE_SIGN_INFO, paramJson.toJSONString());
		LOGGER.info("queryRemoteSignInfo中查询客户未签名购物车的出参：{}", res);
		
		// 查询出现异常，直接返回
		JSONObject resObj = JSON.parseObject(res);
		if (StringUtils.isBlank(res)) {
			String resultMsg = Constants.ERR_NULL;
			return BSSHelper.getErrInfo("99", resultMsg).toString();
		}
		String resCode = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultCode");
		JSONObject resJson = resObj.getJSONObject("contractRoot").getJSONObject("svcCont").getJSONObject("resultObject");
		JSONObject resultObject2 = new JSONObject();
		if (Constants.NUM_ZERO.equals(resCode)) {
			IResponseParamTransform transformBean = (IResponseParamTransform) SpringUtil
					.getBean(ResponseBusinessTypeEnum.getBeanName(191202));
			return transformBean.transform(resJson);
		} else {
			String resultMsg = resultObject2.getJSONObject("contractRoot").getJSONObject("svcCont").getString("resultMsg");
			return BSSHelper.getErrInfo(resCode, resultMsg).toString();
		}
	}
	
	public String queryGridByLonLat(JSONObject object) {
		//获取参数
		String lonlat = object.getString("lonlat");
		String type = object.getString("type");
		String areaId = object.getString("areaId");
		String countyId = object.getString("countyId");
		
		//获取鉴权
		String userName = "JS_YWZT";
		String passwd = "Gislogin123456";
		String tokenUrl = BSSUrlConstants.GET_TOKEN + "?userName=" + userName + "&passwd=" + passwd;
		LOGGER.info("获取鉴权入参：{}", tokenUrl);
		String tokenRes = HttpClientUtils.doHttpPostMsg(tokenUrl, "", HttpConstant.UTF_ENCODING);
		LOGGER.info("获取鉴权出参：{}", tokenRes);
		
		// 鉴权异常，直接返回
		if (StringUtils.isBlank(tokenRes)) {
			String resultMsg = "鉴权异常";
			return BSSHelper.getErrInfo("99", resultMsg).toString();
		}
		JSONObject tokeResObj = JSON.parseObject(tokenRes);
		String tokenResCode = tokeResObj.getString("resultcode");
		if (!Constants.NUM_ONE.equals(tokenResCode)) {
			String resultMsg = tokeResObj.getString("msg");
			return BSSHelper.getErrInfo(tokenResCode, resultMsg).toString();
		}
		String ak = tokeResObj.getString("ak");
		
		//获取区域网格
		StringBuilder url = new StringBuilder(BSSUrlConstants.QRY_GRID_BY_LON_LAT);
		url.append("?ak=").append(ak).append("&type=").append(type).append("&lonlat=").append(lonlat);
		if (StringUtils.isNotEmpty(areaId)) {
			url.append("&bss_area_id=").append(areaId);
		}
		if (StringUtils.isNotEmpty(countyId)) {
			url.append("&bss_area_id_4=").append(countyId);
		}
		
		LOGGER.info("获取坐标网格信息入参：{}", url);
		String res = HttpClientUtils.sendGet(url.toString(), EInvoiceUtil.getInvoiceHeadMap());
		LOGGER.info("获取坐标网格信息出参：{}", res);
		
		// 查询异常，直接返回
		if (StringUtils.isBlank(res)) {
			String resultMsg = "查询坐标网格信息异常";
			return BSSHelper.getErrInfo("99", resultMsg).toString();
		}
		JSONObject resObj = JSON.parseObject(res);
		String resCode = resObj.getString("status");
		if (Constants.NUM_ZERO.equals(resCode)) {
			resObj.put("status", "1");
		} else {
			resObj.put("status", "0");
		}
		return resObj.toString();
	}
	
	
	public String queryNearbyGridAndLonLat(JSONObject object, RespHead responseHead, String tranId, RespInfo responseJson) {
		//获取参数
		String lonlat = object.getString("lonlat");
		String distance = object.getString("distance");
		String areaId = object.getString("areaId");
		String countyId = object.getString("countyId");
		String type = object.getString("type");
		
		//获取鉴权
		String userName = "JS_YWZT";
		String passwd = "Gislogin123456";
		String tokenUrl = BSSUrlConstants.GET_TOKEN + "?userName=" + userName + "&passwd=" + passwd;
		LOGGER.info("获取鉴权入参:{}", tokenUrl);
		String tokenRes = HttpClientUtils.doHttpPostMsg(tokenUrl, "", HttpConstant.UTF_ENCODING);
		LOGGER.info("获取鉴权出参:{}", tokenRes);
		
		// 鉴权异常，直接返回
		if (StringUtils.isBlank(tokenRes)) {
			String resultMsg = "鉴权异常";
			return BSSHelper.getErrInfo("99", resultMsg).toString();
		}
		JSONObject tokeResObj = JSON.parseObject(tokenRes);
		String tokenResCode = tokeResObj.getString("resultcode");
		if (!Constants.NUM_ONE.equals(tokenResCode)) {
			String resultMsg = tokeResObj.getString("msg");
			return BSSHelper.getErrInfo(tokenResCode, resultMsg).toString();
		}
		String ak = tokeResObj.getString("ak");
		
		//获取区域网格
		StringBuilder url = new StringBuilder(BSSUrlConstants.QRY_NEAR_BY_GRID_AND_LON_LAT);
		url.append("?ak=").append(ak).append("&lonlat=").append(lonlat).append("&distance=").append(distance);
		if (StringUtils.isNotEmpty(type)) {
			url.append("&type=").append(type);
		}
		if (StringUtils.isNotEmpty(areaId)) {
			url.append("&bss_area_id=").append(areaId);
		}
		if (StringUtils.isNotEmpty(countyId)) {
			url.append("&bss_area_id_4=").append(countyId);
		}
		
		LOGGER.info("根据经纬度及指定距离查询范围内网格接口入参:{}", url);
		String res = HttpClientUtils.sendGet(url.toString(), EInvoiceUtil.getInvoiceHeadMap());
		LOGGER.info("根据经纬度及指定距离查询范围内网格接口出参:{}", res);
		
		// 查询异常，直接返回
		if (StringUtils.isBlank(res)) {
			return BSSHelper.getErrInfo("99", "查询范围内网格接口信息异常").toString();
		}
		JSONObject resObj = JSON.parseObject(res);
		String resCode = resObj.getString("status");
		if (StringUtils.isEmpty(resCode) || Constants.NUM_ZERO.equals(resCode)) {
			resObj.put("status", "1");
		} else {
			resObj.put("status", "0");
		}
		return Utils.builderResponse(resObj.toJSONString(), responseHead, responseJson, tranId, "status", "", "err").toString();
	}
	
	public String getOwnerCustIdByQryProdInst(String accNbr, String queryMode, String accNbrType, String delPWDFlag, JSONObject headJson) {
		String ownerCustId = "";
		JSONObject bodyJson = new JSONObject();
		bodyJson.put("accNbr", accNbr);
		bodyJson.put("queryMode", queryMode);
		bodyJson.put("accNbrType", accNbrType);
		bodyJson.put("delPWDFlag", delPWDFlag);
		String res = qryProdInst(headJson, bodyJson);
		JSONObject resJson = JSON.parseObject(res);
		if ("00".equals(resJson.getJSONObject("head").getString("status"))) {
			JSONObject resultObjectJson = resJson.getJSONObject("body").getJSONObject("resultObject");
			JSONObject accProdInstDetail = resultObjectJson.getJSONArray("accProdInstDetailList").getJSONObject(0).getJSONObject("accProdInstDetail");
			ownerCustId = accProdInstDetail.getString("ownerCustId");
		}
		return ownerCustId;
	}
}


