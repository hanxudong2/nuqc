package cn.js189.uqc.domain.invoice;

import java.util.Date;

public class InvoiceOrderDetail {

	/**
	 * 发票id 32位uuid
	 */
	private String id;

	/**
	 * 商户内部电子发票订单号
	 */
	private String orderId;

	/**
	 * 发票实例id
	 */
	private String invoiceId;

	/**
	 * 发票账单id
	 * 对应企信侧发票信息id字段
	 */
	private String billId;

	/**
	 * 产品实例id
	 */
	private String productId;

	/**
	 * 购物车id
	 * 发票类型是1200营业厅订单有值
	 */
	private String orderNumber;

	/**
	 * 发票代码
	 */
	private String invoiceCode;

	/**
	 * 发票号码
	 */
	private String invoiceNo;

	/**
	 * 开票类型
	 * 1000月结 1100实缴 1200营业
	 */
	private String type;

	/**
	 * 发票金额
	 * 单位:分
	 */
	private String money;

	/**
	 * 发票发送邮箱
	 * 重新发送更新此邮箱
	 */
	private String mail;

	/**
	 * 开票类型1000
	 * 账单日期 yyyyMM
	 */
	private String billMonth;

	/**
	 * 开票类型1100
	 * 实缴类型日期 yyyyMMDD HH:mm:ss
	 */
	private String paymentDate;

	/**
	 * 开票类型1200
	 * 营业厅订单日期 yyyyMMDD
 	 */
	private String orderDate;

	/**
	 * 开票时间
	 */
	private Date invoiceDate;

	/**
	 * 发票开具状态
	 * 1已开具 2未开具 3开具中
	 */
	private String status;

	/**
	 * pdf下载状态
	 * 0失败 1成功
	 */
	private String pdfDownload;

	/**
	 * 企信侧pdf链接地址
	 */
	private String pdfUrl;

	/**
	 * 本地pdf地址
	 */
	private String pdfLocalPath;

	/**
	 * 授权类型 微信侧
	 * 0：开票授权，1：填写字段开票授权，2：领票授权
	 */
	private String authType;

	/**
	 * 微信册授权状态
	 * 0否 1是
	 */
	private String wxAuth;

	/**
	 * 支付宝侧授权状态
	 * 0否 1是
	 */
	private String aliAuth;

	/**
	 * 微信册插卡状态
	 * 0否 1是
	 */
	private String wxCardInsert;

	/**
	 * 支付宝侧插卡状态
	 * 0否 1是
	 */
	private String aliCardInsert;

	/**
	 * 备注
	 */
	private String remark;

	/**
	 * 创建时间
	 */
	private Date gmtCreate;

	/**
	 * 修改时间
	 */
	private Date gmtUpdate;

	private String partyId;

	private String description;

	private String checkCode;

	public String getCheckCode() {
		return checkCode;
	}

	public void setCheckCode(String checkCode) {
		this.checkCode = checkCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPartyId() {
		return partyId;
	}

	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}
	
	public InvoiceOrderDetail() {
	}

	public InvoiceOrderDetail(String invoiceId, String mail, String wxAuth, String aliAuth) {
		this.invoiceId = invoiceId;
		this.mail = mail;
		this.wxAuth = wxAuth;
		this.aliAuth = aliAuth;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}

	public String getBillId() {
		return billId;
	}

	public void setBillId(String billId) {
		this.billId = billId;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getInvoiceCode() {
		return invoiceCode;
	}

	public void setInvoiceCode(String invoiceCode) {
		this.invoiceCode = invoiceCode;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getMoney() {
		return money;
	}

	public void setMoney(String money) {
		this.money = money;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getBillMonth() {
		return billMonth;
	}

	public void setBillMonth(String billMonth) {
		this.billMonth = billMonth;
	}

	public String getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}

	public String getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}

	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPdfDownload() {
		return pdfDownload;
	}

	public void setPdfDownload(String pdfDownload) {
		this.pdfDownload = pdfDownload;
	}

	public String getPdfUrl() {
		return pdfUrl;
	}

	public void setPdfUrl(String pdfUrl) {
		this.pdfUrl = pdfUrl;
	}

	public String getPdfLocalPath() {
		return pdfLocalPath;
	}

	public void setPdfLocalPath(String pdfLocalPath) {
		this.pdfLocalPath = pdfLocalPath;
	}

	public String getAuthType() {
		return authType;
	}

	public void setAuthType(String authType) {
		this.authType = authType;
	}

	public String getWxAuth() {
		return wxAuth;
	}

	public void setWxAuth(String wxAuth) {
		this.wxAuth = wxAuth;
	}

	public String getAliAuth() {
		return aliAuth;
	}

	public void setAliAuth(String aliAuth) {
		this.aliAuth = aliAuth;
	}

	public String getWxCardInsert() {
		return wxCardInsert;
	}

	public void setWxCardInsert(String wxCardInsert) {
		this.wxCardInsert = wxCardInsert;
	}

	public String getAliCardInsert() {
		return aliCardInsert;
	}

	public void setAliCardInsert(String aliCardInsert) {
		this.aliCardInsert = aliCardInsert;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Date getGmtCreate() {
		return gmtCreate;
	}

	public void setGmtCreate(Date gmtCreate) {
		this.gmtCreate = gmtCreate;
	}

	public Date getGmtUpdate() {
		return gmtUpdate;
	}

	public void setGmtUpdate(Date gmtUpdate) {
		this.gmtUpdate = gmtUpdate;
	}
}
