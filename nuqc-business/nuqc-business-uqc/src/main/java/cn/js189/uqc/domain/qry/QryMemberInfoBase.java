package cn.js189.uqc.domain.qry;

import cn.js189.uqc.service.IQryPageInfo;
import cn.js189.uqc.common.QryPageInfoDefaultImpl;
import cn.js189.uqc.common.RequestParamBase;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 实名制校验入参息请求报文基础类s
 * 
 * @ClassName: QryRealNameCheckInsBase
 * @author cx
 * @date 2018年3月24日 下午2:26:20
 *
 */
public abstract class QryMemberInfoBase extends RequestParamBase {

	// 日志组件
	private static final Logger LOGGER = LoggerFactory.getLogger(QryMemberInfoBase.class);

	// svcCode 提供的API接口名称
	private static final String BSS_TCP_CONT_SVC_CODE = "js.intf.qryMemberInfo";
	
	// 地区标识
	protected String regionId;
	// 是否查询未竣工相应的信息
	protected String uncompletedFlag;
	// 分页相关信息的封装
	protected IQryPageInfo qryPageInfo;
	
	
	/**
	 *构造器
	 * 
	 * @param regionId:地区标识
	 * @param uncompletedFlag:是否查询未竣工相应的信息
	 */
	public QryMemberInfoBase(String regionId, String uncompletedFlag) {
		super();
		this.regionId = regionId;
		this.uncompletedFlag = uncompletedFlag;
		this.qryPageInfo = new QryPageInfoDefaultImpl();
	}
	
	
	@Override
	protected JSONObject generateTcpCont() {
		return super.generateTcpCont(BSS_TCP_CONT_APP_KEY, BSS_TCP_CONT_SVC_CODE, null);
	}
	
	
	@Override
	protected JSONObject generateSvcCont() {
		JSONObject authenticationInfo =  super.generateSvcContAuthenticationInfo(regionId, null, null, null);
		if (null == authenticationInfo) {
			LOGGER.error("generate authenticationInfo failure");
			return null;
		}

		JSONObject requestObject = generateRequestObject();
		if (null == requestObject) {
			LOGGER.error("generate requestObject failure");
			return null;
		}

		JSONObject svcCont = new JSONObject();
		svcCont.put("authenticationInfo", authenticationInfo);
		svcCont.put("requestObject", requestObject);
		return svcCont;
	}
	
	/**
	 * 生成 svcCont 下的请求对象
	 * 
	 * @Title: generateRequestObject 
	 * @param @return    设定文件 
	 * @return JSONObject    返回类型 
	 * @author fulei
	 * @throws
	 */
	protected JSONObject generateRequestObject() {
		JSONObject requestObject = new JSONObject();
		requestObject.put("regionId", regionId);
		requestObject.put("uncompletedFlag", uncompletedFlag);
		requestObject.put("pageInfo", qryPageInfo.generatePageInfos());
		return improveRequestObject(requestObject);
	}
	
	/**
	 * 补齐 svcCont 下的请求对象
	 * 
	 * @param requestObject:svcCont下的请求对象
	 * @return
	 */
	protected abstract JSONObject improveRequestObject(JSONObject requestObject);
	
	
}
