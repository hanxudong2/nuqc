package cn.js189.uqc.common;

import cn.js189.uqc.service.IQryPageInfo;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 智慧BSS，生成查询产品关联关系报文基础类
 * 
 * @author Mid
 */
public abstract class QryProdRelBase extends RequestParamBase {
	// 日志组件
	private static final Logger LOGGER = LoggerFactory.getLogger(QryProdRelBase.class);

	// svcCode 提供的API接口名称
	public static final String BSS_TCP_CONT_SVC_CODE = "js.intf.qryProdInstRel";

	// 地区标识
	protected String regionId;
	// 是否查询未竣工相应的信息
	protected String uncompletedFlag;
	// 分页相关信息的封装
	protected IQryPageInfo qryPageInfo;

	/**
	 * 查询产品信息构造器
	 * 
	 * @param regionId:地区标识
	 * @param uncompletedFlag:是否查询未竣工相应的信息
	 */
	protected QryProdRelBase(String regionId, String uncompletedFlag) {
		super();
		this.regionId = regionId;
		this.uncompletedFlag = uncompletedFlag;
		this.qryPageInfo = new QryPageInfoDefaultImpl();
	}

	@Override
	protected JSONObject generateTcpCont() {
		return super.generateTcpCont(BSS_TCP_CONT_APP_KEY, BSS_TCP_CONT_SVC_CODE, null);
	}

	@Override
	protected JSONObject generateSvcCont() {
		JSONObject authenticationInfo = generateAuthenticationInfo();
		if (null == authenticationInfo) {
			LOGGER.error("generate authenticationInfo failure");
			return null;
		}

		JSONObject requestObject = generateRequestObject();
		if (null == requestObject) {
			LOGGER.error("generate requestObject failure");
			return null;
		}

		JSONObject svcCont = new JSONObject();
		svcCont.put("authenticationInfo", authenticationInfo);
		svcCont.put("requestObject", requestObject);
		return svcCont;
	}

	/**
	 * 生成 svcCont 下的认证信息
	 * 
	 * @return
	 */
	protected JSONObject generateAuthenticationInfo() {
		return super.generateSvcContAuthenticationInfo(regionId, null, null, null);
	}

	/**
	 * 生成 svcCont 下的请求对象
	 * 
	 * @return
	 */
	protected JSONObject generateRequestObject() {
		JSONObject requestObject = new JSONObject();
		requestObject.put("pageInfo", qryPageInfo.generatePageInfos());
		requestObject.put("regionId", regionId);
		requestObject.put("uncompletedFlag", uncompletedFlag);
		return improveRequestObject(requestObject);
	}

	/**
	 * 补齐 svcCont 下的请求对象
	 * 
	 * @param requestObject:svcCont下的请求对象
	 * @return
	 */
	protected abstract JSONObject improveRequestObject(JSONObject requestObject);
}
