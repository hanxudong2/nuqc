package cn.js189.uqc.domain.qry;

import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 通过接入类产品实例标识查询产品信息，生成请求报文
 * 
 * @author zhouchao
 */
public class QryProdInstByAccProdInstId extends QryProdInstBase {
	// 日志组件
	private static final Logger LOGGER = LoggerFactory.getLogger(QryProdInstByAccProdInstId.class);

	// 接入类产品实例标识accProdInstId
	private String accProdInstId;
	
	// qryMode
	private String qryMode;

	public QryProdInstByAccProdInstId(String regionId, String uncompletedFlag, String accProdInstId, String qryMode) {
		super(regionId, uncompletedFlag);
		this.accProdInstId = accProdInstId;
		this.qryMode = qryMode;
	}

	@Override
	protected JSONObject improveRequestObject(JSONObject requestObject) {
		requestObject.put("accProdInstId", accProdInstId);
		requestObject.put("qryMode", qryMode);
		LOGGER.debug("requestObject:{}", requestObject.toString());
		return requestObject;
	}
}
