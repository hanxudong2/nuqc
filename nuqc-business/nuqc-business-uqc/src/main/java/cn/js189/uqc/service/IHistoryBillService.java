package cn.js189.uqc.service;

/**
 * @Author yxl
 * @Date 2024/3/6
 */

public interface IHistoryBillService {
    String callNewBillReq(String requestJSON);

    String feeTrend(String requestJson);

    String getTktYiValueTicketQrByDayCount(String requestJSON);

    String qryInstantFeeList(String requestJSON);

    String qryNewCustBill(String requestJSON);

    String qrySpDetail(String requestJSON);
}
