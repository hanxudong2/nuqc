package cn.js189.uqc.common;


import cn.js189.common.util.StringUtils;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 通过证件号码的方式查询客户信息，生成请求报文
 * 
 * @author cx
 */
public class QryCustomerOrderNew extends QryOrderBase {
	// 日志组件
	private static final Logger LOGGER = LoggerFactory.getLogger(QryCustomerOrderNew.class);

	private String beginDate;//开始时间
	private String endDate;//结束时间
	private String belongCustId;//归属客户id
	private String regionId;//地区id
	private String custOrderNbr;//购物车流水号
	private String custOrderId;//购物车id
	private String accNbr;
	private String applyObjSpecs;
	private String objId;

	public QryCustomerOrderNew(String regionId, String uncompletedFlag, String beginDate, String endDate, String belongCustId, String custOrderNbr, String custOrderId, String accNbr, String applyObjSpecs, String objId) {
		super(regionId, uncompletedFlag);
		this.beginDate = beginDate;
		this.endDate = endDate;
		this.belongCustId = belongCustId;
		this.custOrderNbr = custOrderNbr;
		this.custOrderId = custOrderId;
		this.accNbr = accNbr;
		this.applyObjSpecs = applyObjSpecs;
		this.objId = objId;
	}

	@Override
	protected JSONObject improveRequestObject(JSONObject requestObject) {
		requestObject.put("regionId", regionId);
		requestObject.put("uncompletedFlag", uncompletedFlag);
		if((!StringUtils.isEmpty(beginDate))&&(!StringUtils.isEmpty(endDate))){
		    JSONObject acceptDateScope = new JSONObject();
		    acceptDateScope.put("beginDate", beginDate);
		    acceptDateScope.put("endDate", endDate);
		    requestObject.put("acceptDateScope", acceptDateScope);
		}
		requestObject.put("belongCustId", belongCustId);
		requestObject.put("custOrderNbr", custOrderNbr);
		requestObject.put("custOrderId", custOrderId);
		requestObject.put("accNbr", accNbr);
		requestObject.put("applyObjSpecs", applyObjSpecs);
		requestObject.put("objId", objId);
		LOGGER.debug("requestObject:{}", requestObject);
		return requestObject;
	}

}
