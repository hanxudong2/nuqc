/**************************************************************************************** 

 ****************************************************************************************/
package cn.js189.uqc.util;

import org.apache.http.client.config.RequestConfig;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.TrustStrategy;

import javax.net.ssl.SSLContext;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

/** 
 * <Description> <br> 
 *  
 * @author yangyang<br>
 * @version 1.0<br>
 */

public class SSLClientAF43 {
    private SSLClientAF43(){
        super();
    }
    protected static CloseableHttpClient createSSLClientDefault() {

        SSLContext sslContext;
        try {
	        //信任所有
	        sslContext = new SSLContextBuilder().loadTrustMaterial(null, (arg0, arg1) -> true).build();
          SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslContext);
          RequestConfig config = RequestConfig.custom().setConnectTimeout(30000).setSocketTimeout(30000).build();
          return HttpClients.custom().setSSLSocketFactory(sslsf).setDefaultRequestConfig(config).build();
        } catch (Exception ex) {
         // Do nothing because of X and Y.
        }
        return HttpClients.createDefault();
    }

}
