package cn.js189.uqc.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.js189.common.domain.ReqInfo;
import cn.js189.common.domain.RespHead;
import cn.js189.common.domain.RespInfo;
import cn.js189.common.util.Utils;
import cn.js189.uqc.redis.RedisOperation;
import cn.js189.uqc.service.IBssSystemService;
import cn.js189.uqc.util.SmartBssSystemUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author yxl
 * @Date 2024/3/7
 */
@Service
public class IBssSystemServiceImpl implements IBssSystemService {
    private static final Logger logger = LoggerFactory.getLogger(IBssSystemServiceImpl.class);

    @Autowired
    private RedisOperation redisOperation;

    @Autowired
    private SmartBssSystemUtil smartBssSystemImpl;


    @Override
    public String qryOfferSpecDetail(String requestJSON) {
        JSONObject response = null;
        logger.debug("智慧查询销售品规格详情 195531 input：{}" , requestJSON);
        RespInfo responseJson = new RespInfo();
        RespHead responseHead = new RespHead();
        responseJson.setHead(responseHead);
        String tranId="";
        try{
            ReqInfo reqInfo = BeanUtil.toBean(requestJSON,ReqInfo.class);
            JSONObject requestJson = JSON.parseObject(requestJSON);
            JSONObject head = requestJson.getJSONObject("head");// head体
            JSONObject body = reqInfo.getBody();
            return smartBssSystemImpl.qryOfferSpecDetail(head, body);
        } catch (Exception e) {
            response = Utils.dealExceptionResp(responseHead, tranId, responseJson);
            logger.debug("智慧查询销售品规格详情 195531  Exception :{}" , e.getMessage());
            return response.toString();
        }
    }
}
