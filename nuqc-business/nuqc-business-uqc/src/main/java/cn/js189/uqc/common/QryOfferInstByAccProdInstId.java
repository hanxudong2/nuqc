package cn.js189.uqc.common;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * 通过产品实例ID的方式查询销售品信息，生成请求报文
 * 
 * @author ly
 */
public class QryOfferInstByAccProdInstId extends QryOfferInstBase {

	// 日志组件
	private static final Logger LOGGER = LoggerFactory.getLogger(QryOfferInstByAccProdInstId.class);

	//产品实例id
	private JSONArray accProdInstIds;
	
	public QryOfferInstByAccProdInstId(List<String> offerIds, String roleId, String regionId, String uncompletedFlag, String isIndependent, JSONArray accProdInstIds) {
		super(regionId, uncompletedFlag, isIndependent,roleId,offerIds);
		this.accProdInstIds = accProdInstIds;
	}

	
	@Override
	protected JSONObject improveRequestObject(JSONObject requestObject) {
		requestObject.put("accProdInstIds", accProdInstIds);
		LOGGER.debug("requestObject:{}", requestObject);
		return requestObject;
	}

}
