package cn.js189.uqc.domain.invoice;

import cn.js189.uqc.domain.req.JTRequestParamBase;

public class InterceptParam extends JTRequestParamBase {
    /**
     * 系统编码
     */
    public static final String SYSCODE = "S10300";
    /**
     * 应用编码
     */
    public static final String APPCODE = "P105010201";
    /**
     * 密钥
     */
    public static final String KEY = "6c0d89a4109111ec9044968740be47e7";

    @Override
    protected String generateXAuthDq(String requestBody, String method) {
        return super.generateXAuthDq(requestBody,KEY,SYSCODE,APPCODE,method);
    }
}
