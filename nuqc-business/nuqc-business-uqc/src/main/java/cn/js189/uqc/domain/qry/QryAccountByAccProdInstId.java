package cn.js189.uqc.domain.qry;

import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class QryAccountByAccProdInstId extends QryAccountBase{
	// 日志组件
		private static final Logger LOGGER = LoggerFactory.getLogger(QryAccountByAccProdInstId.class);

		//接入类产品实例标识accProdInstId
		private String accProdInstId;

		public QryAccountByAccProdInstId(String regionId, String uncompletedFlag, String accProdInstId) {
			super(regionId, uncompletedFlag);
			this.accProdInstId = accProdInstId;
		}

		@Override
		protected JSONObject improveRequestObject(JSONObject requestObject) {
			requestObject.put("accProdInstId", accProdInstId);
			LOGGER.debug("requestObject:{}", requestObject.toString());
			return requestObject;
		}
}
