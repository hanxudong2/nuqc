package cn.js189.uqc.controller;

import cn.js189.uqc.service.IWxInterService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping
public class WxController {
	
	@Resource
	private IWxInterService wxInterService;
	
	/**
	 * 微信侧获取授权状态定时任务
	 * @param reqParams 参数
	 * @return 结果
	 */
	@PostMapping("/iBssSystem/getInvoiceAuthTask")
	public String getInvoiceAuthTask(@RequestBody String reqParams){
		return wxInterService.getInvoiceAuthTask(reqParams);
	}
	
	/**
	 * 微信插卡定时任务
	 * @param reqParams 参数
	 * @return 结果
	 */
	@PostMapping("/iBssSystem/inserWxCardTask")
	public String inserWxCardTask(@RequestBody String reqParams){
		return wxInterService.inserWxCardTask(reqParams);
	}
	
	/**
	 * 查询微信一次授权状态
	 * @param reqParams 参数
	 * @return 结果
	 */
	@PostMapping("/iBssSystem/checkBatchAuthInfo")
	public String checkBatchAuthInfo(@RequestBody String reqParams){
		return wxInterService.checkBatchAuthInfo(reqParams);
	}
	
	/**
	 * 获取微信授权页url
	 * @param reqParams 参数
	 * @return 结果
	 */
	@PostMapping("/iBssSystem/getWxAuthUrl")
	public String getWxAuthUrl(@RequestBody String reqParams){
		return wxInterService.getWxAuthUrl(reqParams);
	}
	
	/**
	 * 智能网查询
	 * @param reqParams 参数
	 * @return 结果
	 */
	@PostMapping("/iBssSystem/getZnwInfo")
	public String getZnwInfo(@RequestBody String reqParams){
		return wxInterService.getZnwInfo(reqParams);
	}
}
