package cn.js189.uqc.domain.qry;

import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * 通过接入号的方式查询客户信息，生成请求报文
 * 
 * @author Mid
 */
public class QryOfferDetailByOfferId extends QryOfferDetailBase {
	// 日志组件
	private static final Logger LOGGER = LoggerFactory.getLogger(QryOfferDetailByOfferId.class);

	// 省内销售品编码
	private String offerId;
	//集团长编码
	private String offerNbr;
	//状态
	private List<String> statusCds;

	public QryOfferDetailByOfferId(String regionId, String offerId, String offerNbr, List<String> statusCds) {
		super(regionId);
		this.offerId = offerId;
		this.offerNbr = offerNbr;
		this.statusCds = statusCds;
	}

	@Override
	protected JSONObject improveRequestObject(JSONObject requestObject) {
		requestObject.put("offerId", offerId);
		requestObject.put("offerNbr", offerNbr);
		requestObject.put("statusCds", statusCds);
		return requestObject;
	}
	
}
