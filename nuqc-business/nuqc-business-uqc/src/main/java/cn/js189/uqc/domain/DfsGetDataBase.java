package cn.js189.uqc.domain;

import com.alibaba.fastjson.JSONObject;

public abstract class DfsGetDataBase extends EOPRequestParamBase{
	// svcCode 提供的API接口名称
	public static final String EOP_TCP_CONT_SVC_CODE = "dfsgetData";
	public static final String EOP_TCP_CONT_AUTHORIZATION = "aXLXvXA3zBvKrZ1VyC6NoDhPWsWMlfbQ"; //需要申请
	private String sysCode;
	private String busCode;
	private String storeKey;
	
	public DfsGetDataBase(String sysCode, String busCode, String storeKey ) {
		super();
		this.sysCode=sysCode;
		this.busCode=busCode;
		this.storeKey=storeKey;
	}
	
	
	@Override
	protected JSONObject generateTcpCont() {
		return super.generateTcpCont(EOP_TCP_CONT_APP_KEY, EOP_TCP_CONT_SVC_CODE, null);
	}

	@Override
	protected JSONObject generateSvcCont() {
		JSONObject svcCont = new JSONObject();
		svcCont.put("sysCode", sysCode);
		svcCont.put("busCode", busCode);
		svcCont.put("storeKey", storeKey);
		svcCont.put("Authorization", EOP_TCP_CONT_AUTHORIZATION);
		return svcCont;
	}
	/**
	 * 生成 svcCont 下的请求对象
	 * 
	 * @return
	 */
	protected JSONObject generateRequestObject() {
		JSONObject requestObject = new JSONObject();
		return improveRequestObject(requestObject);
	}
	/**
	 * 补齐 svcCont 下的请求对象
	 * 
	 * @param requestObject:svcCont下的请求对象
	 * @return
	 */
	protected abstract JSONObject improveRequestObject(JSONObject requestObject);


	public String getSysCode() {
		return sysCode;
	}


	public void setSysCode(String sysCode) {
		this.sysCode = sysCode;
	}


	public String getBusCode() {
		return busCode;
	}


	public void setBusCode(String busCode) {
		this.busCode = busCode;
	}


	public String getStoreKey() {
		return storeKey;
	}


	public void setStoreKey(String storeKey) {
		this.storeKey = storeKey;
	}

	
	
}
