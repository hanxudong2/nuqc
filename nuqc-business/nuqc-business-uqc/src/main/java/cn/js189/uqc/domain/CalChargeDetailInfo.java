package cn.js189.uqc.domain;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang.StringUtils;

public class CalChargeDetailInfo extends CalChargeDetailInfoBase{

	private String custOrderId;
	
	private String custOrderNbr;

	private String areaCode;

	private String orgId;

	private String staffCode;
	
	public CalChargeDetailInfo(String regionId, String custOrderId, String custOrderNbr, String areaCode, String orgId, String staffCode){
		super(regionId);
		this.custOrderId = custOrderId;
		this.custOrderNbr = custOrderNbr;
		this.areaCode = areaCode;
		this.orgId = orgId;
		this.staffCode = staffCode;
	}

	@Override
	protected JSONObject improveRequestObject(JSONObject requestObject) {
		if(!StringUtils.isEmpty(custOrderId)){
    		requestObject.put("custOrderId", custOrderId);
    	}
		if(!StringUtils.isEmpty(custOrderNbr)){
    		requestObject.put("custOrderNbr", custOrderNbr);
    	}
		if(!StringUtils.isEmpty(areaCode)){
    		requestObject.put("areaCode", areaCode);
    	}
		if(!StringUtils.isEmpty(orgId)){
    		requestObject.put("orgId",orgId );
    	}
		if(!StringUtils.isEmpty(staffCode)){
    		requestObject.put("staffCode",staffCode );
    	}
		return requestObject;
	}
}
