/**************************************************************************************** 

 ****************************************************************************************/
package cn.js189.uqc.mapper;

import java.util.Map;

/**
 * 
 * <Description> <br> 
 *  
 * @author yangyang<br>
 * @version 1.0<br>
 * @taskId <br>
 * @CreateDate 2019年6月3日 <br>
 */
public interface MnpCheckMapper {
    void insertMNPRecord(Map map);
    int updateMnpRecord(Map map);
    void insertAuthCodeRecord(Map map);
    void insertMNPAbilityRecord(Map map);
    void insertMNPRmOutableRecord(Map map);
    void insertNewSaleMNPRecord(Map map);
    void insertNewSaleMNPAbilityRecord(Map map);
}
