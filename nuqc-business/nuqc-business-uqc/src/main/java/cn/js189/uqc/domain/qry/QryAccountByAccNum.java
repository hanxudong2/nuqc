package cn.js189.uqc.domain.qry;

import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class QryAccountByAccNum extends QryAccountBase{
	// 日志组件
		private static final Logger LOGGER = LoggerFactory.getLogger(QryAccountByAccNum.class);

		// 接入号码
		private String accNum;
		// 账号
		private String account;
		// 接入号码类型
		private String prodId;

		public QryAccountByAccNum(String regionId, String uncompletedFlag, String accNum, String account, String prodId) {
			super(regionId, uncompletedFlag);
			this.accNum = accNum;
			this.account = account;
			this.prodId = prodId;
		}

		@Override
		protected JSONObject improveRequestObject(JSONObject requestObject) {
			requestObject.put("accNum", accNum);
			requestObject.put("account", account);
			requestObject.put("prodId", prodId);
			LOGGER.debug("requestObject:{}", requestObject.toString());
			return requestObject;
		}
}
