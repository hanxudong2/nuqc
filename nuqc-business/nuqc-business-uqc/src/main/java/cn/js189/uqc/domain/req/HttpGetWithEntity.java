package cn.js189.uqc.domain.req;

import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;

import java.net.URI;

/**
 * get请求带body入参
 *
 * @author xyc
 * @date 2023-10-11
 */
public class HttpGetWithEntity extends HttpEntityEnclosingRequestBase {
    private static final String METHOD_NAME = "GET";

    @Override
    public String getMethod() {
        return METHOD_NAME;
    }

    public HttpGetWithEntity() {
        super();
    }

    public HttpGetWithEntity(final URI uri) {
        super();
        setURI(uri);
    }

    public HttpGetWithEntity(final String uri) {
        super();
        setURI(
                URI.create(uri));
    }
}