package cn.js189.uqc.domain.invoice;

import java.util.Date;

public class InvoiceAuthDTO {

	private Long id;

	/**
	 * 临时ticket 申请授权使用,与用户提交时的商户订单关联
	 */
	private String ticketId;

	/**
	 * 发票实例id
	 */
	private String invoiceId;

	/**
	 * 金额 单位:分
	 */
	private String money;

	/**
	 * 开票涞源
	 *  app：app开票，web：微信h5开票，wxa：小程序开发票，wap：普通网页开票
	 */
	private String source;

	/**
	 * 申请开票类型
	 */
	private String type;

	/**
	 * 授权耶成功后跳转页面
	 */
	private String redirectUrl;

	/**
	 * 商户内部订单号
	 */
	private String orderId;

	/**
	 * 微信授权标识 0未授权 1已授权
	 */
	private String wxAuth;

	private Date gmtCreate;

	private Date gmtUpdate;

	public InvoiceAuthDTO() {
	}

	public InvoiceAuthDTO(String ticketId, String invoiceId, String money, String source, String type, String redirectUrl, String wxAuth) {

		this.ticketId = ticketId;
		this.invoiceId = invoiceId;
		this.money = money;
		this.source = source;
		this.type = type;
		this.redirectUrl = redirectUrl;
		this.wxAuth = wxAuth;
	}

	public InvoiceAuthDTO(String ticket, String orderId) {
		this.ticketId = ticket;
		this.orderId = orderId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	public String getOrderId() {
		return orderId;
	}

	public String getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getWxAuth() {
		return wxAuth;
	}

	public void setWxAuth(String wxAuth) {
		this.wxAuth = wxAuth;
	}

	public Date getGmtCreate() {
		return gmtCreate;
	}

	public void setGmtCreate(Date gmtCreate) {
		this.gmtCreate = gmtCreate;
	}

	public Date getGmtUpdate() {
		return gmtUpdate;
	}

	public void setGmtUpdate(Date gmtUpdate) {
		this.gmtUpdate = gmtUpdate;
	}

	public String getMoney() {
		return money;
	}

	public void setMoney(String money) {
		this.money = money;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getRedirectUrl() {
		return redirectUrl;
	}

	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}
}
