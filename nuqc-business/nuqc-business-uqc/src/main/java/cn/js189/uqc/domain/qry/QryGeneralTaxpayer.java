package cn.js189.uqc.domain.qry;

import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 通过接入号的方式查询客户信息，生成请求报文
 * 
 * @author Mid
 */
public class QryGeneralTaxpayer extends QryGeneralTaxpayerBase {
	// 日志组件
	private static final Logger LOGGER = LoggerFactory.getLogger(QryGeneralTaxpayer.class);

	// 查询custId
	private String custId;
	

	public QryGeneralTaxpayer(String regionId, String custId) {
		super(regionId);
		this.custId = custId;
	}

	@Override
	protected JSONObject improveRequestObject(JSONObject requestObject) {
		requestObject.put("custId", custId);
		LOGGER.debug("requestObject:{}", requestObject);
		return requestObject;
	}
	
}
