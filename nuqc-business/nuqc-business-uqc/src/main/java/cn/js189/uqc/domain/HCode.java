package cn.js189.uqc.domain;

import lombok.Data;

import java.io.Serializable;

@Data
public class HCode implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String hcode;
	
	private String province_name;
	
	private String city_name;
	
	private String areaCode;
	
	private String company_name;

}
