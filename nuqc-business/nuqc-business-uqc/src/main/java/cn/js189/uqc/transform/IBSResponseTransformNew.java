package cn.js189.uqc.transform;

import com.alibaba.fastjson.JSONObject;

public interface IBSResponseTransformNew {

    JSONObject transform(String responseString, String appendResponseString, String tranId);
}
