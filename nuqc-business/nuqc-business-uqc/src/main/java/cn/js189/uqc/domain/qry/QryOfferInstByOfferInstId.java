package cn.js189.uqc.domain.qry;

import cn.js189.uqc.common.QryOfferInstBase;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * 通过销售品ID的方式查询销售品信息，生成请求报文
 * 
 * @author ly
 */
public class QryOfferInstByOfferInstId extends QryOfferInstBase {

	// 日志组件
	private static final Logger LOGGER = LoggerFactory.getLogger(QryOfferInstByOfferInstId.class);


	// 销售品id
	private String offerInstId;

	public QryOfferInstByOfferInstId(List<String> offerIds, String roleId, String regionId, String uncompletedFlag, String isIndependent,
	                                 String offerInstId) {
		super(regionId, uncompletedFlag, isIndependent,roleId,offerIds);
		this.offerInstId = offerInstId;
	}

	@Override
	protected JSONObject improveRequestObject(JSONObject requestObject) {
		requestObject.put("offerInstId", offerInstId);
		LOGGER.debug("requestObject:{}", requestObject.toString());
		return requestObject;
	}

}
