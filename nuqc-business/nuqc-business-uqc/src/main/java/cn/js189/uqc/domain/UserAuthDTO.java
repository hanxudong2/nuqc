package cn.js189.uqc.domain;

import java.util.Date;

/**
 * 用户一次授权
 */
public class UserAuthDTO {

	private Long id;

	private String partyId;

	private String appId;

	private String openId;

	private String hasAuth;

	private String authSource;

	private Integer expDate;

	private Date gmtCreate;

	private Date gmtUpdate;

	public UserAuthDTO(String partyId, String appId, String openId, String hasAuth, String authSource, Integer expDate) {
		this.partyId = partyId;
		this.appId = appId;
		this.openId = openId;
		this.hasAuth = hasAuth;
		this.authSource = authSource;
		this.expDate = expDate;
	}

	public UserAuthDTO(String partyId, String appId, String openId, String hasAuth) {
		this.partyId = partyId;
		this.appId = appId;
		this.openId = openId;
		this.hasAuth = hasAuth;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPartyId() {
		return partyId;
	}

	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getHasAuth() {
		return hasAuth;
	}

	public void setHasAuth(String hasAuth) {
		this.hasAuth = hasAuth;
	}

	public String getAuthSource() {
		return authSource;
	}

	public void setAuthSource(String authSource) {
		this.authSource = authSource;
	}

	public Integer getExpDate() {
		return expDate;
	}

	public void setExpDate(Integer expDate) {
		this.expDate = expDate;
	}

	public Date getGmtCreate() {
		return gmtCreate;
	}

	public void setGmtCreate(Date gmtCreate) {
		this.gmtCreate = gmtCreate;
	}

	public Date getGmtUpdate() {
		return gmtUpdate;
	}

	public void setGmtUpdate(Date gmtUpdate) {
		this.gmtUpdate = gmtUpdate;
	}
}
