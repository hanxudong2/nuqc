package cn.js189.uqc.controller;

import cn.js189.uqc.service.IAreaBillService;
import cn.js189.uqc.service.IBssUserQueryService;
import cn.js189.uqc.service.IStoreService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;

@RestController
@RequestMapping
public class EopController {
	
	@Resource
	private IBssUserQueryService bssUserQueryService;
	@Resource
	private IStoreService storeService;
	@Resource
	private IAreaBillService areaBillService;
	@PostMapping("/iBssSystem/documentQuery")
	public String documentQuery(@RequestBody String reqParams){
		return bssUserQueryService.documentQuery(reqParams);
	}
	
	@PostMapping("/iBssSystem/getActivityInfo")
	public String getActivityInfo(@RequestBody String reqParams){
		return bssUserQueryService.getActivityInfo(reqParams);
	}
	
	@PostMapping("/iBssSystem/getAuthFail")
	public String getAuthFail(@RequestBody String reqParams){
		return bssUserQueryService.getAuthFail(reqParams);
	}
	
	@PostMapping("/iBssSystem/getCustManagerbyProdId")
	public String getCustomManagerByProdId(@RequestBody String reqParams){
		return bssUserQueryService.getCustomManagerByProdId(reqParams);
	}
	
	@PostMapping("/iBssSystem/getGMPropertyRecords")
	public String getGMPropertyRecords(@RequestBody String reqParams){
		return bssUserQueryService.getGMPropertyRecords(reqParams);
	}
	
	@PostMapping("/iBssSystem/queryStaff")
	public String queryStaffInfo(@RequestBody String reqParams){
		return bssUserQueryService.queryStaffInfo(reqParams);
	}
	
	@PostMapping("/iBssSystem/qryChannel")
	public String qryChannel(@RequestBody String reqParams){
		return bssUserQueryService.qryChannel(reqParams);
	}
	
	@PostMapping("/iBssSystem/rpeHandleHttpReq")
	public String rpeHandleHttpReq(@RequestBody String reqParams){
		return bssUserQueryService.rpeHandleHttpReq(reqParams);
	}
	
	@PostMapping("/iBssSystem/calChargeDetailInfo")
	public String calChargeDetailInfo(@RequestBody String reqParams){
		return bssUserQueryService.calChargeDetailInfo(reqParams);
	}
	
	@PostMapping("/iBssSystem/getStoreMsg")
	public String getStoreMsg(@RequestBody String reqParams){
		return storeService.getStoreMsg(reqParams);
	}

	@PostMapping("/iBssSystem/getUserInfo")
	public String getUserInfo(@RequestBody String reqParams){
		return bssUserQueryService.getUserInfo(reqParams);
	}
	
	@PostMapping("/iBssSystem/getSign")
	public String getSign(){
		return bssUserQueryService.getSign();
	}
	
	@PostMapping("/iBssSystem/getUserDet")
	public String getUserDet(@RequestBody String reqParams){
		return bssUserQueryService.getUserDet(reqParams);
	}
	
	@PostMapping("/iBssSystem/getUserOnline")
	public String getUserOnline(@RequestBody String reqParams){
		return bssUserQueryService.getUserOnline(reqParams);
	}
	
	@PostMapping("/iBssSystem/qryCustlevelAndKeyPerson")
	public String qryCustlevelAndKeyPerson(@RequestBody String reqParams){
		return bssUserQueryService.qryCustlevelAndKeyPerson(reqParams);
	}
	
	@PostMapping("/iBssSystem/qryCustomerWithNoFilter")
	public String qryCustomerWithNoFilter(@RequestBody String reqParams){
		return bssUserQueryService.qryCustomerWithNoFilter(reqParams);
	}
	
	@PostMapping("/iBssSystem/qryCustomerOrderByType")
	public String qryCustomerOrderByType(@RequestBody String reqParams){
		return bssUserQueryService.qryCustomerOrderByType(reqParams);
	}
	
	@PostMapping("/iBssSystem/callNewAreaCodeBasic")
	public String callNewAreaCodeBasic(@RequestBody String reqParams){
		return areaBillService.callNewAreaCodeBasic(reqParams);
	}
	
	@PostMapping("/iBssSystem/getArpuByPhoneNbr")
	public String getArpuByPhoneNbr(@RequestBody String reqParams){
		return areaBillService.getArpuByPhoneNbr(reqParams);
	}
	
	@PostMapping("/iBssSystem/getGoodsIdByAccNbrFK")
	public String getGoodsIdByAccNbrFK(@RequestBody String reqParams){
		return areaBillService.getGoodsIdByAccNbrFK(reqParams);
	}
}
