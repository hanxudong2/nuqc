package cn.js189.uqc.common;

import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class CheckServSpecByDevModelBase extends OSS3RequestParamBase{
	// 日志组件
	private static final Logger LOGGER = LoggerFactory.getLogger(CheckServSpecByDevModelBase.class);

	// svcCode 提供的API接口名称
	public static final String OSS3_TCP_CONT_SVC_CODE = "IOSSInterfaceForCRMCheckServSpecByDevModel";

	/**
	 * 查询无纸化信息构造器
	 */
	protected CheckServSpecByDevModelBase() {
		super();
	}

	@Override
	protected JSONObject generateTcpCont() {
		return super.generateTcpCont(OSS3_TCP_CONT_APP_KEY, OSS3_TCP_CONT_SVC_CODE, null);
	}

	@Override
	protected JSONObject generateSvcCont() {
		JSONObject requestObject = generateRequestObject();
		if (null == requestObject) {
			LOGGER.error("generate requestObject failure");
			return null;
		}
		return  requestObject;
	}

	/**
	 * 生成 svcCont 下的请求对象
	 */
	protected JSONObject generateRequestObject() {
		JSONObject requestObject = new JSONObject();
		return improveRequestObject(requestObject);
	}

	/**
	 * 补齐 svcCont 下的请求对象
	 * @param requestObject:svcCont下的请求对象
	 */
	protected abstract JSONObject improveRequestObject(JSONObject requestObject);

}
