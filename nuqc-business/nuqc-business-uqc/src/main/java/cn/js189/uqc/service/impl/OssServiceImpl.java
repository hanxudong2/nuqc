package cn.js189.uqc.service.impl;

import cn.js189.common.constants.Constants;
import cn.js189.common.domain.ReqInfo;
import cn.js189.common.domain.RespHead;
import cn.js189.common.domain.RespInfo;
import cn.js189.uqc.service.IOssService;
import cn.js189.uqc.service.PubAddressIpervice;
import cn.js189.uqc.util.EInvoiceUtil;
import cn.js189.uqc.util.HttpClientHelperForEop;
import cn.js189.common.util.Utils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.gson.Gson;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * @program: xxb
 * @description: OSS相关接口
 * @author: jzd
 * @create: 2024/3/4 14:21
 **/
@Service
public class OssServiceImpl implements IOssService {

    private static final Logger logger = LoggerFactory.getLogger(OssServiceImpl.class);

    @Resource
    private PubAddressIpervice pubAddressIpervice;


    @Override
    public String getCurrentTachInfoToCrm(String requestJSON) {
        logger.debug("JSOSS政企门户业务进度透明化查询 input: {}", requestJSON);
        JSONObject response = null;
        RespInfo responseJson = new RespInfo();
        RespHead responseHead = new RespHead();
        responseJson.setHead(responseHead);
        String tranId = "";
        try{
            ReqInfo reqInfo = new Gson().fromJson(requestJSON,ReqInfo.class);
            String areaCode = reqInfo.getHead().getAreaCode();
            tranId = reqInfo.getHead().getTranId();
            JSONObject body = reqInfo.getBody();

            JSONObject requestParam = new JSONObject();
            requestParam.put("productCode",body.getString("productCode"));
            requestParam.put("orderNbr",body.getString("orderNbr"));
            requestParam.put("proInstId",body.getString("proInstId"));
            requestParam.put("operateType",body.getString("operateType"));
            requestParam.put("areaCode",Utils.areaIdMap.get(Utils.areaCodeMap.get(areaCode)));

            logger.debug("JSOSS政企门户业务进度透明化查询  requestParam: {}", requestParam);

            String url = pubAddressIpervice.getActionUrl("CURRENT_TACH_INFO_TO_CRM",reqInfo.getHead().getLoginNbr());

            Map<String,String> headMap = new HashMap<>();
            headMap.put("X-APP-ID", EInvoiceUtil.APP_KEY);
            headMap.put("X-APP-KEY", EInvoiceUtil.APP_SECRET);

            String result = HttpClientHelperForEop.postWithHeader(url, requestParam.toJSONString(), headMap, 30000);
            logger.debug("JSOSS政企门户业务进度透明化查询  result: {}", result);

            if (!StringUtils.isEmpty(result)) {
                JSONObject respBody = JSON.parseObject(result);
                response = Utils.dealSuccessRespWithMsgAndStatus(responseHead, tranId, respBody, "success", "00", responseJson);
            } else {
                response = Utils.dealSuccessRespWithMsgAndStatus(responseHead, tranId, null, Constants.ERR_INF_NULL, "98", responseJson);
            }
            return response.toString();
        } catch (Exception e) {
            response = Utils.dealExceptionResp(responseHead, tranId, responseJson);
            logger.error("查询账户合并关系  Exception :{}",e.getMessage());
            return response.toString();
        }
    }

    @Override
    public String getODByCustOrderCode(String requestJSON) {
        logger.debug("JSOSS根据购物车判断是否分解成功查询 input: {}", requestJSON);
        String result = "";
        JSONObject response = null;
        RespInfo responseJson = new RespInfo();
        RespHead responseHead = new RespHead();
        responseJson.setHead(responseHead);
        String tranId = "";
        try{
            ReqInfo reqInfo = new Gson().fromJson(requestJSON,ReqInfo.class);
            String areaCode = reqInfo.getHead().getAreaCode();
            tranId = reqInfo.getHead().getTranId();
            JSONObject body = reqInfo.getBody();

            JSONObject requestParam = new JSONObject();
            requestParam.put("custOrderCode",body.getString("custOrderCode"));
            requestParam.put("areaId", Utils.areaCodeMap.get(areaCode));

            logger.info("JSOSS根据购物车判断是否分解成功查询  requestParam: {}", requestParam);
            String url = pubAddressIpervice.getActionUrl("GET_OD_BYCUSTORDERCODE",reqInfo.getHead().getLoginNbr());


            result = HttpClientHelperForEop.postWithHeader(url, requestParam.toJSONString(), null, 30000);
            logger.info("JSOSS根据购物车判断是否分解成功查询 requestParam: {} -- result: {}",requestParam, result);

            if (!StringUtils.isEmpty(result)) {
                JSONObject respBody = JSON.parseObject(result);
                response = Utils.dealSuccessRespWithMsgAndStatus(responseHead, tranId, respBody, "success", "00", responseJson);
            } else {
                response = Utils.dealSuccessRespWithMsgAndStatus(responseHead, tranId, null, Constants.ERR_INF_NULL, "98", responseJson);
            }
        }  catch (Exception e) {
            response = Utils.dealSuccessRespWithMsgAndStatus(responseHead, tranId, null, result , "99", responseJson);
            logger.error("JSOSS根据购物车判断是否分解成功  Exception :{}",e.getMessage());
        }
        return response.toString();
    }

}
