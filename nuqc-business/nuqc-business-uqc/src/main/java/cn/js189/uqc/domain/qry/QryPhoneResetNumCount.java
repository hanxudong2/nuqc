package cn.js189.uqc.domain.qry;

import com.alibaba.fastjson.JSONObject;


public class QryPhoneResetNumCount extends QryPhoneResetNumCountBase {

    private String certNum;

    public QryPhoneResetNumCount(String regionId, String certNum) {
        super(regionId);
        this.certNum = certNum;
    }


    @Override
    protected JSONObject improveRequestObject(JSONObject requestObject) {
        requestObject.put("certNum", certNum);
        return requestObject;
    }

}
