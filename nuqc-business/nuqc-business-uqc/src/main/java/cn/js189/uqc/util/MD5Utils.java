/**************************************************************************************** 

 ****************************************************************************************/
package cn.js189.uqc.util;

import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * <Description> MD5工具类<br>
 * 
 * @author zhangpzh<br>
 * @version 1.0<br>
 */

public final class MD5Utils {
	
	private MD5Utils(){}

    private static final Logger LOGGER = LoggerFactory.getLogger(MD5Utils.class);

    // 全局数组
    private static final  String[] strDigits = { "0", "1", "2", "3", "4", "5",
            "6", "7", "8", "9", "a", "b", "c", "d", "e", "f" };

    /**
     * MD5加密
     * 
     * @param str the str
     * @return the string
     */
    public static String mD5(String str) {
        return DigestUtils.md5Hex(str);
    }

    public static String getMd5Code(String strObj) {
        String resultString = null;
        try {
            resultString = strObj;
            MessageDigest md = MessageDigest.getInstance("MD5");
            // md.digest() 该函数返回值为存放哈希值结果的byte数组
            resultString = byteToString(md.digest(strObj.getBytes()));
        } catch (NoSuchAlgorithmException ex) {
            LOGGER.debug(ex.getMessage());
        }
        return resultString;
    }

    // 转换字节数组为16进制字串
    private static String byteToString(byte[] bByte) {
        StringBuilder sBuffer = new StringBuilder();
	    for (byte b : bByte) {
		    sBuffer.append(byteToArrayString(b));
	    }
        return sBuffer.toString();
    }

    // 返回形式为数字跟字符串
    private static String byteToArrayString(byte bByte) {
        int iRet = bByte;
        if (iRet < 0) {
            iRet += 256;
        }
        int iD1 = iRet / 16;
        int iD2 = iRet % 16;
        return strDigits[iD1] + strDigits[iD2];
    }

    /*************************************** 第三方支付接口 ***************************************/
    // 把字节数组转成16进位制数
    public static String bytesToHex(byte[] bytes) {
        StringBuilder md5str = new StringBuilder();
        // 把数组每一字节换成16进制连成md5字符串
        int digital;
	    for (byte aByte : bytes) {
		    digital = aByte;
		    if (digital < 0) {
			    digital += 256;
		    }
		    if (digital < 16) {
			    md5str.append("0");
		    }
		    md5str.append(Integer.toHexString(digital));
	    }
        return md5str.toString();
    }

    // 把字节数组转换成md5
    public static String bytesToMD5(byte[] input) {
        String md5str = null;
        try {
            // 创建一个提供信息摘要算法的对象，初始化为md5算法对象
            MessageDigest md = MessageDigest.getInstance("MD5");
            // 计算后获得字节数组
            byte[] buff = md.digest(input);
            // 把数组每一字节换成16进制连成md5字符串
            md5str = bytesToHex(buff);
        } catch (Exception e) {
            LOGGER.debug(e.getMessage());
        }
        return md5str;
    }

    /*************************************** 3G升4G订单查询接口 ***************************************/

    private static byte[] md5(String s) {
        MessageDigest algorithm;
        try {
            algorithm = MessageDigest.getInstance("MD5");
            algorithm.reset();
            algorithm.update(s.getBytes(StandardCharsets.UTF_8));
            return algorithm.digest();
        }
        catch (Exception e) {
            LOGGER.debug(e.getMessage());
        }
        return new byte[0];
    }

    private static String toHex(byte[] hash) {
        if(hash == null) {
            return null;
        }
        StringBuilder buf = new StringBuilder(hash.length * 2);
        int i;

        for (i = 0; i < hash.length; i++) {
            if ((hash[i] & 0xff) < 0x10) {
                buf.append("0");
            }
            buf.append(Long.toString(hash[i] & 0xff, 16));
        }
        return buf.toString();
    }

    public static String hash(String s) {
        try {
            String hexStr = toHex(md5(s));
            if (hexStr != null) {
                return new String(hexStr.getBytes(StandardCharsets.UTF_8), StandardCharsets.UTF_8);
            } else {
                return s;
            }
        } catch (Exception e) {
            LOGGER.debug(e.getMessage());
            return s;
        }
    }


}
