/**************************************************************************************** 
        湖南创发科技有限责任公司
 ****************************************************************************************/
package cn.js189.uqc.domain.qry;

import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 
 * <Description> <br> 
 *  
 * @author 刘添<br>
 * @version 1.0<br>
 * @taskId <br>
 * @CreateDate 2018年4月2日 <br>
 */

public class QryCustOrderAndPdfFlag extends QryCustBase {

	/**
	 *  日志组件
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(QryCustOrderAndPdfFlag.class);
	
	/**
	 * 地区标识
	 */
	private String regionId;
	
	/**
	 * 客户id
	 */
	private String custId;
	
	/**
	 * 开始时间
	 */
	private String startDt;
	
	/**
	 * 结束时间
	 */
	private String endDt;

	public QryCustOrderAndPdfFlag(String regionId, String regionId2, String custId,
	                              String startDt, String endDt) {
		super(regionId);
		this.custId = custId;
		this.endDt = endDt;
		this.startDt = startDt;
		this.regionId = regionId2;
	}
	
			
	/**
	 * Description: 构建请求参数 requestObject<br> 
	 *  
	 * @author 刘添<br>
	 * @taskId <br>
	 * @param requestObject
	 * @return <br>
	 */ 
	@Override
	protected JSONObject improveRequestObject(JSONObject requestObject) {
		requestObject.put("custId", custId);
		requestObject.put("regionId", regionId);
		requestObject.put("endDt", endDt);
		requestObject.put("startDt", startDt);
		LOGGER.debug("requestObject:{}", requestObject.toString());
		return requestObject;
	}

}
