package cn.js189.uqc.util;

import cn.hutool.extra.spring.SpringUtil;
import cn.js189.uqc.domain.invoice.InnerApiLog;
import cn.js189.uqc.mapper.CheckInfoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InnerApiLogThread implements Runnable {

	private static final Logger logger = LoggerFactory.getLogger(InnerApiLogThread.class);

	private InnerApiLog innerApiLog;

	public InnerApiLogThread(InnerApiLog innerApiLog) {
		this.innerApiLog = innerApiLog;
	}

	@Override
	public void run() {

		try {

			logger.debug("内部接口日志入库...");

			CheckInfoMapper checkInfoDao = SpringUtil.getBean("checkInfoDao");
			checkInfoDao.insertInnerApiLog(innerApiLog);

			logger.debug("内部接口日志入库完成");

		} catch (Exception e) {
			logger.debug("内部接口入库插入异常：{}" , e.getMessage());
		}



	}
}
