package cn.js189.uqc.domain.invoice;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.Date;

public class InvoiceDetail {
	

	private String id = "";

	private String titleName =  "";

	private String titleType =  "";

	private String money =  "";

	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	private Date gmtCreate;

	private String invoiceId;

	private String invoiceCode =  "";

	private String invoiceNo = "";

	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	private Date invoiceDate;

	private String pdfUrl =  "";

	private String mail =  "";

	private String odMail = "";

	private String remark =  "";

	private String bankName =  "";

	private String bankAccount =  "";

	private String companyAddr =  "";

	private String companyPhone =  "";

	private String description = "";

	private String sMediaId = "";

	private String cardId = "";

	private String encryptCode = "";

	private String wxCardInsert = "";

	private String checkCode = "";

	private String tax = "0";

	private String taxNum = "";

	private String source = "";

	private String wxOpenId = "";

	private String partyId = "";

	private String appId = "";

	private String wxAuth;

	private String aliAuth;

	private String aliInsert;

	private int totalInsert;

	private Integer expDate;

	private String type;
	
	private String status;	

	private String orderId;

	private String areaCode;
	
	private String phone;	

	private String bid = "";

	private String orderNumber = "";

	private String billMonth = "";
	
	private String productId;

	private String wxUserAuth = "";

	private String aliUserAuth = "";

	private String idCard = "";

	private String userMail = "";

	private String paymentDate = "";

	private String did = "";

	private String didNo = "";

	private String isMerge= "";
	public String getDidNo() {
		return didNo;
	}

	public void setDidNo(String didNo) {
		this.didNo = didNo;
	}

	public String getDid() {
		return did;
	}

	public void setDid(String did) {
		this.did = did;
	}

	public String getUserMail() {
		return userMail;
	}

	public void setUserMail(String userMail) {
		this.userMail = userMail;
	}

	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	public String getWxUserAuth() {
		return wxUserAuth;
	}

	public void setWxUserAuth(String wxUserAuth) {
		this.wxUserAuth = wxUserAuth;
	}

	public String getAliUserAuth() {
		return aliUserAuth;
	}

	public void setAliUserAuth(String aliUserAuth) {
		this.aliUserAuth = aliUserAuth;
	}

	public Integer getExpDate() {
		return expDate;
	}

	public void setExpDate(Integer expDate) {
		this.expDate = expDate;
	}

	public int getTotalInsert() {
		return totalInsert;
	}

	public void setTotalInsert(int totalInsert) {
		this.totalInsert = totalInsert;
	}

	public String getWxAuth() {
		return wxAuth;
	}

	public void setWxAuth(String wxAuth) {
		this.wxAuth = wxAuth;
	}

	public String getAliAuth() {
		return aliAuth;
	}

	public void setAliAuth(String aliAuth) {
		this.aliAuth = aliAuth;
	}

	public String getAliInsert() {
		return aliInsert;
	}

	public void setAliInsert(String aliInsert) {
		this.aliInsert = aliInsert;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getPartyId() {
		return partyId;
	}

	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}

	public String getEncryptCode() {
		return encryptCode;
	}

	public void setEncryptCode(String encryptCode) {
		this.encryptCode = encryptCode;
	}

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitleName() {
		return titleName;
	}

	public void setTitleName(String titleName) {
		this.titleName = titleName;
	}

	public String getTitleType() {
		return titleType;
	}

	public void setTitleType(String titleType) {
		this.titleType = titleType;
	}

	public String getMoney() {
		return money;
	}

	public void setMoney(String money) {
		this.money = money;
	}

	public Date getGmtCreate() {
		return gmtCreate;
	}

	public void setGmtCreate(Date gmtCreate) {
		this.gmtCreate = gmtCreate;
	}

	public String getInvoiceCode() {
		return invoiceCode;
	}

	public void setInvoiceCode(String invoiceCode) {
		this.invoiceCode = invoiceCode;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public String getPdfUrl() {
		return pdfUrl;
	}

	public void setPdfUrl(String pdfUrl) {
		this.pdfUrl = pdfUrl;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getOdMail() {
		return odMail;
	}

	public void setOdMail(String odMail) {
		this.odMail = odMail;
	}

	public String getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}

	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankAccount() {
		return bankAccount;
	}

	public void setBankAccount(String bankAccount) {
		this.bankAccount = bankAccount;
	}

	public String getCompanyAddr() {
		return companyAddr;
	}

	public void setCompanyAddr(String companyAddr) {
		this.companyAddr = companyAddr;
	}

	public String getCompanyPhone() {
		return companyPhone;
	}

	public void setCompanyPhone(String companyPhone) {
		this.companyPhone = companyPhone;
	}

	public String getsMediaId() {
		return sMediaId;
	}

	public void setsMediaId(String sMediaId) {
		this.sMediaId = sMediaId;
	}

	public String getWxCardInsert() {
		return wxCardInsert;
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setWxCardInsert(String wxCardInsert) {
		this.wxCardInsert = wxCardInsert;
	}

	public String getCheckCode() {
		return checkCode;
	}

	public void setCheckCode(String checkCode) {
		this.checkCode = checkCode;
	}

	public String getTax() {
		return tax;
	}

	public void setTax(String tax) {
		this.tax = tax;
	}

	public String getTaxNum() {
		return taxNum;
	}

	public void setTaxNum(String taxNum) {
		this.taxNum = taxNum;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getWxOpenId() {
		return wxOpenId;
	}

	public void setWxOpenId(String wxOpenId) {
		this.wxOpenId = wxOpenId;
	}
	
	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public String getBid() {
		return bid;
	}

	public void setBid(String bid) {
		this.bid = bid;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getBillMonth() {
		return billMonth;
	}

	public void setBillMonth(String billMonth) {
		this.billMonth = billMonth;
	}

	public String getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}

	public String getIsMerge() {
		return isMerge;
	}

	public void setIsMerge(String isMerge) {
		this.isMerge = isMerge;
	}
}
