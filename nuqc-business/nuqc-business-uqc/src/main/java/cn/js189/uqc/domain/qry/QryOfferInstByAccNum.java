package cn.js189.uqc.domain.qry;

import cn.js189.uqc.common.QryOfferInstBase;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * 通过接入号的方式查询销售品信息，生成请求报文
 * 
 * @author Mid
 */
public class QryOfferInstByAccNum extends QryOfferInstBase {
	// 日志组件
	private static final Logger LOGGER = LoggerFactory.getLogger(QryOfferInstByAccNum.class);

	// 接入号码
	private String accNum;
	// 接入号码类型
	private String prodId;

	public QryOfferInstByAccNum(List<String> offerIds, String roleId, String regionId, String uncompletedFlag, String isIndependent, String accNum,
	                            String prodId) {
		super(regionId, uncompletedFlag, isIndependent,roleId,offerIds);
		this.accNum = accNum;
		this.prodId = prodId;
	}

	@Override
	protected JSONObject improveRequestObject(JSONObject requestObject) {
		requestObject.put("accNum", accNum);
		requestObject.put("prodId", prodId);
		LOGGER.debug("requestObject:{}", requestObject.toString());
		return requestObject;
	}
}
