package cn.js189.uqc.service.impl;


import cn.hutool.extra.spring.SpringUtil;
import cn.js189.common.domain.ReqInfo;
import cn.js189.common.domain.RespHead;
import cn.js189.common.domain.RespInfo;
import cn.js189.common.enums.SystemEnum;
import cn.js189.common.util.BSExceptionUtil;
import cn.js189.common.util.exception.BSException;
import cn.js189.common.util.helper.DateHelper;
import cn.js189.common.util.helper.UUIDHelper;
import cn.js189.uqc.service.PubAddressIpervice;
import cn.js189.uqc.transform.IBSResponseTransform;
import cn.js189.uqc.transform.IBSResponseTransformNew;
import cn.js189.uqc.transform.ISGWResponseTransform;
import cn.js189.uqc.util.HttpClientHelperForEop;
import cn.js189.uqc.util.StaticDataMappingUtil;
import cn.js189.common.util.Utils;
import cn.js189.uqc.util.enums.BSResponseBusinessTypeEnum;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

@Component("BSService")
public class BSService {

	private static final Logger LOGGER = LoggerFactory.getLogger(BSService.class);

	@Autowired
	private PubAddressIpervice pubAddressIpervice;

	/**
	 * UQC: bsn费用查询-10113 callFeeBillQuery
	 * BS: 费用查询 getAcctItemFQr
	 */
	public String callFeeBillQueryNew(ReqInfo reqInfo) {

		RespInfo responseJson = new RespInfo();
		RespHead responseHead = new RespHead();
		responseJson.setHead(responseHead);
		String tranId = "";
		try {
			tranId = reqInfo.getHead().getTranId();
			String areaCode = reqInfo.getHead().getAreaCode();
			JSONObject body = reqInfo.getBody();
			LOGGER.info("费用查询 getAcctItemFQr 开始入参转换");
			String requestJson = this.getAcctItemFqrRequestJson(body, areaCode);
			LOGGER.info("费用查询 getAcctItemFQr 入参:{}", requestJson);
//			http://jsjteop.telecomjs.com:8764/jseop/billing_zw/getAcctItemFQr-controller/getAcctItemFQr
			String url  = pubAddressIpervice.getActionUrl("GET_ACCT_ITEM_FQR","");
			LOGGER.info("费用查询 getAcctItemFQr---url:{}", url);
			String result = HttpClientHelperForEop.postToBill(url, requestJson, areaCode);
			LOGGER.info("费用查询 getAcctItemFQr 出参:{}", result);
			IBSResponseTransform transform = SpringUtil.getBean(BSResponseBusinessTypeEnum.getBeanName("callFeeBillQuery"));
			return transform.transform(result, tranId).toString();
		} catch (Exception e) {
			LOGGER.error("费用查询 getAcctItemFQr Exception: {}", e.getMessage());
			return Utils.dealExceptionResp(responseHead, tranId, responseJson).toString();
		}
	}

	/**
	 * UQC: 一致性本月消费查询 callAcctItemConsumqr
	 * BS: 费用查询 getAcctItemFQr
	 */
	public JSONObject callAcctItemConsumqrNew(JSONObject body, String tranId, String areaCode) {
		RespInfo responseJson = new RespInfo();
		RespHead responseHead = new RespHead();
		responseJson.setHead(responseHead);
		try {
			LOGGER.info("一致性本月消费查询 getAcctItemFQr 开始入参转换");
			String requestJson = this.getAcctItemFqrRequestJson(body, areaCode);
			LOGGER.info("一致性本月消费查询 getAcctItemFQr 入参:{}", requestJson);
//			http://jsjteop.telecomjs.com:8764/jseop/billing_zw/getAcctItemFQr-controller/getAcctItemFQr
			String url  = pubAddressIpervice.getActionUrl("GET_ACCT_ITEM_FQR","");
			LOGGER.info("一致性本月消费查询 getAcctItemFQr---url:{}", url);
			String result = HttpClientHelperForEop.postToBill(url, requestJson, areaCode);
			LOGGER.info("一致性本月消费查询 getAcctItemFQr 出参:{}", result);
			IBSResponseTransform transform = SpringUtil.getBean(BSResponseBusinessTypeEnum.getBeanName("callAcctItemConsumqr"));
			return transform.transform(result, tranId);
		} catch (Exception e) {
			LOGGER.error("一致性本月消费查询 getAcctItemFQr Exception: {}", e.getMessage());
			return Utils.dealResponseForSgw("99", "服务器内部错误", "fail", null);
		}
	}

	/**
	 * 费用查询 getAcctItemFQr 入参转换
	 */
	public String getAcctItemFqrRequestJson(JSONObject body, String areaCode) {
		JSONObject requestObj = new JSONObject();
		requestObj.put("lQuerySourceID", SystemEnum.SGW_ORG_ID.getValue());
		requestObj.put("sQuerySerial", UUIDHelper.currentTimeStampString());
		requestObj.put("lFamilyId", body.getString("productId"));
		requestObj.put("sAccNbr", body.getString("accNbr"));
		requestObj.put("sAcctNbr97", body.getString("accNbr97"));
		requestObj.put("sAreaCode", areaCode);
		requestObj.put("lGetFlag", body.getString("getFlag"));
		requestObj.put("lPrintFlag", body.getString("printFlag"));
		requestObj.put("sBeginDate", body.getString("begDate"));
		requestObj.put("sEndDate", body.getString("endDate"));
		requestObj.put("lOnlyServ", body.getString("onlyServ"));

		return requestObj.toString();
	}

//	/**
//	 * 集团国漫查询地址
//	 *
//	 */
//	public static final String COUNTRY_ROAM_URL = "http://jsjteop.telecomjs.com:8764/jfJf/IOAccuUseQry/IOAccuUseQry";

	/**
	 * UQC: 套餐使用情况查询 callCurrAcuReq
	 * BS: 量本使用查询 AccuUseQry-controller/AccuUseQry
	 */
	public String callCurrAcuReqNew(ReqInfo reqInfo) {
		RespInfo responseJson = new RespInfo();
		RespHead responseHead = new RespHead();
		responseJson.setHead(responseHead);
		String tranId = "";
		try {
			tranId = reqInfo.getHead().getTranId();
			String areaCode = reqInfo.getHead().getAreaCode();
			JSONObject body = reqInfo.getBody();
			LOGGER.info("量本使用查询 accuUseQry 开始入参转换");
			String requestJson = this.getAccuUseQryRequestJson(body, areaCode);
			LOGGER.info("量本使用查询 accuUseQry 入参:{}", requestJson);
//			http://jsjteop.telecomjs.com:8764/jseop/billing_zw/accuUseQryProvince/accuUseQryProvince
			String url  = pubAddressIpervice.getActionUrl("ACC_USER_QRY_PROVINCE","");
			LOGGER.info("量本使用查询 accuUseQry---url:{}", url);
			String result = HttpClientHelperForEop.postToBill(url, requestJson, areaCode);
			LOGGER.info("量本使用查询 accuUseQry 出参:{}", result);
			//计费国漫业务下掉,改从集团查询,只有手机才有国漫，所以只有手机查询
			String appendResponseString = null;
			String family = body.getString("family");
			if (StringUtils.equals("2", family)) {
				LOGGER.info("量本使用查询 accuUseQry 国漫累积量开始入参转换");
				String requestRoamParam = this.getCountryRoamRequestJson(body);
				LOGGER.info("量本使用查询 accuUseQry 国漫累积量入参:{}", requestRoamParam);
//				http://jsjteop.telecomjs.com:8764/jfJf/IOAccuUseQry/IOAccuUseQry
				String accUrl  = pubAddressIpervice.getActionUrl("COUNTRY_ROAM_URL","");
				LOGGER.info("量本使用查询 accuUseQry 国漫累积量---url:{}", accUrl);
				appendResponseString = HttpClientHelperForEop.postWithMNP(accUrl, requestRoamParam, 3000);// 设定3秒
				LOGGER.info("量本使用查询 accuUseQry 国漫累积量出参:{}", appendResponseString);
			}
			IBSResponseTransformNew transform = SpringUtil.getBean(BSResponseBusinessTypeEnum.getBeanName("callCurrAcuReq"));
			return transform.transform(result, appendResponseString, tranId).toString();
		} catch (Exception e) {
			LOGGER.error("量本使用查询 accuUseQry Exception: {}", e.getMessage());
			return Utils.dealExceptionResp(responseHead, tranId, responseJson).toString();
		}
	}
	
	
	public String accuUseDetailQry(ReqInfo reqInfo) {
		RespInfo responseJson = new RespInfo();
		RespHead responseHead = new RespHead();
		responseJson.setHead(responseHead);
        String tranId = "";
        try {
            tranId = reqInfo.getHead().getTranId();
            String areaCode = reqInfo.getHead().getAreaCode();
            JSONObject body = reqInfo.getBody();
            LOGGER.info("量本使用明细查询 accuUseDetailQry 开始入参转换");
            String requestJson = this.getAccuUseDetailQryRequestJson(body, areaCode);
            LOGGER.info("量本使用明细查询 accuUseDetailQry 入参:{}", requestJson);
//			http://jsjteop.telecomjs.com:8764/jseop/billing_zw/accuUseDetailQryAbilityProvince
			String url  = pubAddressIpervice.getActionUrl("ACCU_USE_DETAIL_QRY","");
			LOGGER.info("量本使用明细查询 accuUseDetailQry---url:{}", url);
            String result = HttpClientHelperForEop.postWithMNP(url, requestJson);
            LOGGER.info("量本使用明细查询 accuUseDetailQry 出参:{}", result);
            JSONObject json = JSON.parseObject(result);
            if("0".equals(json.getString("resultCode"))){
                JSONArray accuQryUserList = json.getJSONArray("detailOfferInstInfo").getJSONObject(0).getJSONArray("accuQryUserList");
                for(int i =0;i<accuQryUserList.size();i++){
                    JSONObject accuQryUser = accuQryUserList.getJSONObject(i);
                    accuQryUser.put("accuTypeId", accuQryUser.getString("accuTypeAttr").substring(0, 6));
                }
                result = json.toString();
            }
            return result;
        } catch (Exception e) {
            LOGGER.error("量本使用查询 accuUseQry Exception: {}", e.getMessage());
            return Utils.dealExceptionResp(responseHead, tranId, responseJson).toString();
        }
    }
	
	/**
     * 量本使用明细查询 accuUseDetailQry 入参转换
     */
    public String getAccuUseDetailQryRequestJson(JSONObject body, String areaCode) {

        JSONObject requestObj = new JSONObject();

        requestObj.put("accNbr", body.getString("accNbr"));
        requestObj.put("areaCode", areaCode);
        requestObj.put("operAttrStruct", this.getOperAttrStruct(areaCode));
        requestObj.put("destinationAttr", StringUtils.isEmpty(body.getString("family")) ? "99" : body.getString("family"));
        // yyyyMM
        String curDate = body.getString("curDate");
        if (!StringUtils.isEmpty(curDate) && curDate.length() > 6) {
            curDate = curDate.substring(0, 6);
        }
        requestObj.put("billingCycle", curDate);
        requestObj.put("queryFlag", body.getString("queryFlag"));
        requestObj.put("offerInstId", body.getString("offerInstId"));
		requestObj.put("accuTypeId", body.getString("accuTypeId"));
		requestObj.put("accuId", body.getString("accuId"));
        return requestObj.toString();
    }

	/**
	 * 量本使用查询 accuUseQry 入参转换
	 */
	public String getAccuUseQryRequestJson(JSONObject body, String areaCode) {
		JSONObject requestObj = new JSONObject();
		requestObj.put("accNbr", body.getString("accNbr"));
		requestObj.put("areaCode", areaCode);
		requestObj.put("operAttrStruct", this.getOperAttrStruct(areaCode));
		requestObj.put("destinationAttr", StringUtils.isEmpty(body.getString("family")) ? "99" : body.getString("family"));
		// yyyyMM
		String curDate = body.getString("curDate");
		if (!StringUtils.isEmpty(curDate) && curDate.length() > 6) {
			curDate = curDate.substring(0, 6);
		}
		requestObj.put("billingCycle", curDate);
		return requestObj.toString();
	}

	/**
	 * 量本使用查询 国漫节点查询 入参转换
	 */
	private String getCountryRoamRequestJson(JSONObject body) {
		JSONObject requestObj = new JSONObject();
		requestObj.put("accNbr", body.getString("accNbr"));
		if((body.containsKey("curDate"))&&(body.getString("curDate").length()>=6)) {
			requestObj.put("billingCycle", body.getString("curDate").substring(0, 6));
			// 固定2
			requestObj.put("destinationAttr", "2");
			JSONObject operAttrStruct = new JSONObject();
			operAttrStruct.put("lanId", "0");
			operAttrStruct.put("operOrgId", "0");
			operAttrStruct.put("operPost", "0");
			operAttrStruct.put("operServiceId", "0");
			operAttrStruct.put("staffId", "0");
			operAttrStruct.put("operTime", new SimpleDateFormat("yyyyMMdd").format(new Date()));
			requestObj.put("operAttrStruct", operAttrStruct);
		}
		return requestObj.toString();
	}


	/**
	 * UQC: 流量卡有效期余额查询 callNewFlowCumulationQr
	 * BS: 流量卡余量查询 getFlowAccuQr
	 */
	public String callNewFlowCumulationQrNew(ReqInfo reqInfo) {
		RespInfo responseJson = new RespInfo();
		RespHead responseHead = new RespHead();
		responseJson.setHead(responseHead);
		String tranId = "";
		try {
			tranId = reqInfo.getHead().getTranId();
			String areaCode = reqInfo.getHead().getAreaCode();
			JSONObject body = reqInfo.getBody();
			LOGGER.info("流量卡余量查询 getFlowAccuQr 开始入参转换");
			String requestJson = this.getFlowAccuQrRequestJson(body, areaCode);
			LOGGER.info("流量卡余量查询 getFlowAccuQr 入参:{}", requestJson);
//			http://jsjteop.telecomjs.com:8764/jseop/billing_zw/getFlowAccuQr/getFlowAccuQr
			String url  = pubAddressIpervice.getActionUrl("GET_FLOW_ACCU_QR","");
			LOGGER.info("流量卡余量查询 getFlowAccuQr---url:{}", url);
			String result = HttpClientHelperForEop.postToBill(url, requestJson, areaCode);
			LOGGER.info("流量卡余量查询 getFlowAccuQr 出参:{}", result);
			IBSResponseTransform transform = SpringUtil.getBean(BSResponseBusinessTypeEnum.getBeanName("callNewFlowCumulationQr"));
			return transform.transform(result, tranId).toString();

		} catch (Exception e) {
			LOGGER.error("流量卡余量查询 getFlowAccuQr Exception: {}", e.getMessage());
			return Utils.dealExceptionResp(responseHead, tranId, responseJson).toString();
		}
	}

	/**
	 * 流量卡余量查询 getFlowAccuQr 入参转换
	 */
	private String getFlowAccuQrRequestJson(JSONObject body, String areaCode) {
		JSONObject requestObj = new JSONObject();
		requestObj.put("lQuerySourceID", SystemEnum.SGW_ORG_ID.getValue());
		requestObj.put("sQuerySerial", UUIDHelper.currentTimeStampString());
		requestObj.put("sAccNbr", body.getString("accNbr"));
		requestObj.put("sAreaCode", areaCode);
		// yyyyMMdd
		requestObj.put("sCurDate", body.getString("curDate"));
		requestObj.put("lGetFlag", body.getString("getFlag"));

		return requestObj.toString();
	}

	/**
	 * UQC: 区域编码查询 callNewAreaCode
	 * BS: 号码归属地查询 qryAreaCdByNbr
	 */
	public String callNewAreaCodeNew(ReqInfo reqInfo) {
		RespInfo responseJson = new RespInfo();
		RespHead responseHead = new RespHead();
		responseJson.setHead(responseHead);
		String tranId = "";
		try {
			tranId = reqInfo.getHead().getTranId();
			// 新区号查询入参与老入参body一致,直接用body当作入参,无需转换
			JSONObject body = reqInfo.getBody();
			String requestJson = body.toString();
			LOGGER.info("号码归属地查询 qryAreaCdByNbr 入参:{}", requestJson);
//			http://jsjteop.telecomjs.com:8764/jseop/billing_zw/qryAreaCdByNbr/qryAreaCdByNbr
			String url  = pubAddressIpervice.getActionUrl("QRY_AREA_CD_BY_NBR","");
			LOGGER.info("号码归属地查询 qryAreaCdByNbr---url:{}", url);
			String result = HttpClientHelperForEop.postToBill(url, requestJson, null);
			LOGGER.info("号码归属地查询 qryAreaCdByNbr 出参:{}", result);
			IBSResponseTransform transform = SpringUtil.getBean(BSResponseBusinessTypeEnum.getBeanName("callNewAreaCode"));
			return transform.transform(result, tranId).toString();
		} catch (Exception e) {
			LOGGER.error("号码归属地查询 qryAreaCdByNbr Exception: {}", e.getMessage());
			return Utils.dealExceptionResp(responseHead, tranId, responseJson).toString();
		}
	}

	/**
	 * UQC: 20007余额查询 callBalanceQrReq
	 * BS: 余额查询 getBalanceSvrBalanceQr
	 */
	public String callBalanceQrReqNew(ReqInfo reqInfo) {
		RespInfo responseJson = new RespInfo();
		RespHead responseHead = new RespHead();
		responseJson.setHead(responseHead);
		String tranId = "";
		try {
			tranId = reqInfo.getHead().getTranId();
			String areaCode = reqInfo.getHead().getAreaCode();
			JSONObject body = reqInfo.getBody();
			LOGGER.info("余额查询 getBalanceSvrBalanceQr 开始入参转换");
			String requestJson = this.getBalanceSvrBalanceQrRequestJson(body, areaCode);
			LOGGER.info("余额查询 getBalanceSvrBalanceQr 入参:{}", requestJson);
//			http://jsjteop.telecomjs.com:8764/jseop/billing_zw/getBalanceSvrBalanceQr/getBalanceSvrBalanceQr
			String url  = pubAddressIpervice.getActionUrl("GET_BALANCE_SVR_BALANCE_QR","");
			LOGGER.info("余额查询 getBalanceSvrBalanceQr---url:{}", url);
			String result = HttpClientHelperForEop.postToBill(url, requestJson, areaCode);
			LOGGER.info("余额查询 getBalanceSvrBalanceQr 出参:{}", result);
			IBSResponseTransform transform = SpringUtil.getBean(BSResponseBusinessTypeEnum.getBeanName("callBalanceQrReq"));
			return transform.transform(result, tranId).toString();
		} catch (Exception e) {
			LOGGER.error("余额查询 getBalanceSvrBalanceQr Exception: {}", e.getMessage());
			return Utils.dealExceptionResp(responseHead, tranId, responseJson).toString();
		}
	}

	/**
	 * 余额查询 getBalanceSvrBalanceQr 入参转换
	 * 原接口可能存在未传区号参数,新接口必传
	 */
	private String getBalanceSvrBalanceQrRequestJson(JSONObject body, String areaCode) {
		JSONObject requestObj = new JSONObject();
		requestObj.put("lQuerySourceID", SystemEnum.SGW_ORG_ID.getValue());
		requestObj.put("sQuerySerial", UUIDHelper.currentTimeStampString());
		requestObj.put("sAccNbr", body.getString("accNbr"));
		requestObj.put("sAreaCode", areaCode);
		requestObj.put("sAcctNbr97", body.getString("accNbr97"));
		requestObj.put("sServId", body.getString("servId"));
		requestObj.put("lFamilyId", body.getString("family"));
		return requestObj.toString();
	}

	/**
	 * UQC: 宽带清单查询 callNewBroadDataQr
	 * BS: 宽带清单查询 TktGuBroadDataQr
	 */
	public String callNewBroadDataQrNew(ReqInfo reqInfo) {
		RespInfo responseJson = new RespInfo();
		RespHead responseHead = new RespHead();
		responseJson.setHead(responseHead);
		String tranId = "";
		try {
			tranId = reqInfo.getHead().getTranId();
			String areaCode = reqInfo.getHead().getAreaCode();
			JSONObject body = reqInfo.getBody();
			LOGGER.info("宽带清单查询 TktGuBroadDataQr 开始入参转换");
			String requestJson = this.getTktGuBroadDataQrRequestJson(body, areaCode);
			LOGGER.info("宽带清单查询 TktGuBroadDataQr 入参:{}", requestJson);
//			http://jsjteop.telecomjs.com:8764/jseop/billing_zw/TktGuBroadDataQr/TktGuBroadDataQr
			String url  = pubAddressIpervice.getActionUrl("TKT_GU_BROAD_DATA_QR","");
			LOGGER.info("宽带清单查询 TktGuBroadDataQr---url:{}", url);
			String result = HttpClientHelperForEop.postToBill(url, requestJson, areaCode);
			LOGGER.info("宽带清单查询 TktGuBroadDataQr 出参:{}", result);
			IBSResponseTransform transform = SpringUtil.getBean(BSResponseBusinessTypeEnum.getBeanName("callNewBroadDataQr"));
			return transform.transform(result, tranId).toString();
		} catch (Exception e) {
			LOGGER.error("宽带清单查询 TktGuBroadDataQr Exception: {}", e.getMessage());
			return Utils.dealExceptionResp(responseHead, tranId, responseJson).toString();
		}
	}

	/**
	 * 宽带清单查询 TktGuBroadDataQr 入参转换
	 */
	private String getTktGuBroadDataQrRequestJson(JSONObject body, String areaCode) {
		String begDate = body.getString("begDate");
		String endDate = body.getString("endDate");
		String getFlag = DateHelper.isAllMonthFlag(begDate, endDate)?"1":"3";
		JSONObject requestObj = new JSONObject();
		requestObj.put("ACC_NBR", body.getString("accNbr"));
		requestObj.put("AREA_CODE", areaCode);
		requestObj.put("ACCT_NAME", body.getString("acctName"));
		requestObj.put("TICKET_ID", "0");
		requestObj.put("FAMILY_ID", body.getString("productId"));
		requestObj.put("GET_FLAG", getFlag);
		requestObj.put("BEGIN_DATE", begDate);
		requestObj.put("END_DATE", endDate);

		return requestObj.toString();

	}

	/**
	 * UQC: 10171长话话单信息 callNewCallTicket
	 * BS: 长途清单查询 TktGuCallTicketQr
	 */
	public String callNewCallTicketNew(ReqInfo reqInfo) {
		RespInfo responseJson = new RespInfo();
		RespHead responseHead = new RespHead();
		responseJson.setHead(responseHead);
		String tranId = "";
		try {
			tranId = reqInfo.getHead().getTranId();
			String areaCode = reqInfo.getHead().getAreaCode();
			JSONObject body = reqInfo.getBody();
			LOGGER.info("长途清单查询 TktGuCallTicketQr 开始入参转换");
			String requestJson = this.getTktGuCallTicketQr(body, areaCode);
			LOGGER.info("长途清单查询 TktGuCallTicketQr 入参:{}", requestJson);
//			http://jsjteop.telecomjs.com:8764/jseop/billing_zw/TktGuCallTicketQr/TktGuCallTicketQr
			String url  = pubAddressIpervice.getActionUrl("TKT_GU_CALL_TICKET_QR","");
			LOGGER.info("长途清单查询 TktGuCallTicketQr---url:{}", url);
			String result = HttpClientHelperForEop.postToBill(url, requestJson, areaCode);
			LOGGER.info("长途清单查询 TktGuCallTicketQr 出参:{}", result);
			IBSResponseTransform transform = SpringUtil.getBean(BSResponseBusinessTypeEnum.getBeanName("callNewCallTicket"));
			return transform.transform(result, tranId).toString();
		} catch (Exception e) {
			LOGGER.error("长途清单查询 TktGuCallTicketQr Exception: {}", e.getMessage());
			return Utils.dealExceptionResp(responseHead, tranId, responseJson).toString();
		}
	}

	/**
	 * 长途清单查询 TktGuCallTicketQr 入参转换
	 */
	private String getTktGuCallTicketQr(JSONObject body, String areaCode) {
		String begDate = body.getString("begDate");
		String endDate = body.getString("endDate");
		String getFlag = DateHelper.isAllMonthFlag(begDate, endDate)?"1":"3";
		JSONObject requestObj = new JSONObject();
		requestObj.put("ACC_NBR", body.getString("accNbr"));
		requestObj.put("AREA_CODE", areaCode);
		requestObj.put("GET_FLAG", getFlag);
		requestObj.put("BEGIN_DATE", begDate);
		requestObj.put("END_DATE", endDate);
		requestObj.put("TICKET_ID", "0");
		requestObj.put("FAMILY_ID", body.getString("productId"));
		return requestObj.toString();
	}

	/**
	 * UQC: 16101-彩信E详单 callCeTicketQr
	 * BS:  彩E使用详单 TktYiCeTicketQr
	 */
	public String callCeTicketQrNew(ReqInfo reqInfo) {
		RespInfo responseJson = new RespInfo();
		RespHead responseHead = new RespHead();
		responseJson.setHead(responseHead);
		String tranId = "";
		try {
			tranId = reqInfo.getHead().getTranId();
			String areaCode = reqInfo.getHead().getAreaCode();
			JSONObject body = reqInfo.getBody();
			LOGGER.info("彩E使用详单 TktYiCeTicketQr 开始入参转换");
			String requestJson = this.getTktYiCeTicketQrRequestJson(body, areaCode);
			LOGGER.info("彩E使用详单 TktYiCeTicketQr 入参:{}", requestJson);
//			http://jsjteop.telecomjs.com:8764/jseop/billing_zw/TktYiCeTicketQr/TktYiCeTicketQr
			String url  = pubAddressIpervice.getActionUrl("TKT_YI_CE_TICKET_QR","");
			LOGGER.info("彩E使用详单 TktYiCeTicketQr---url:{}", url);
			String result = HttpClientHelperForEop.postToBill(url, requestJson, areaCode);
			LOGGER.info("彩E使用详单 TktYiCeTicketQr 出参:{}", result);
			IBSResponseTransform transform = SpringUtil.getBean(BSResponseBusinessTypeEnum.getBeanName("callCeTicketQr"));
			return transform.transform(result, tranId).toString();
		} catch (Exception e) {
			LOGGER.error("彩E使用详单 TktYiCeTicketQr Exception: {}", e.getMessage());
			return Utils.dealExceptionResp(responseHead, tranId, responseJson).toString();
		}
	}

	/**
	 * 彩E使用详单 TktYiCeTicketQr 入参转换
	 */
	private String getTktYiCeTicketQrRequestJson(JSONObject body, String areaCode) {
		String begDate = body.getString("begDate");
		String endDate = body.getString("endDate");
		String getFlag = DateHelper.isAllMonthFlag(begDate, endDate)?"1":"3";
		JSONObject requestObj = new JSONObject();
		requestObj.put("ACC_NBR", body.getString("accNbr"));
		requestObj.put("AREA_CODE", areaCode);
		requestObj.put("GET_FLAG", getFlag);
		requestObj.put("BEGIN_DATE", begDate);
		requestObj.put("END_DATE", endDate);
		requestObj.put("FAMILY_ID", body.getString("productId"));
		return requestObj.toString();
	}

	/**
	 * UQC: 流量卡充值记录查询 callNewFlowPayQr
	 * BS: 流量卡充值记录查询 getFlowPayQr
	 */
	public String callNewFlowPayQrNew(ReqInfo reqInfo) {
		RespInfo responseJson = new RespInfo();
		RespHead responseHead = new RespHead();
		responseJson.setHead(responseHead);
		String tranId = "";
		try {
			tranId = reqInfo.getHead().getTranId();
			String areaCode = reqInfo.getHead().getAreaCode();
			JSONObject body = reqInfo.getBody();
			LOGGER.info("流量卡充值记录查询 getFlowPayQr 开始入参转换");
			String requestJson = this.getFlowPayQrRequestJson(body, areaCode);
			LOGGER.info("流量卡充值记录查询 getFlowPayQr 入参:{}", requestJson);
//			http://jsjteop.telecomjs.com:8764/jseop/billing_zw/getFlowPayQr/getFlowPayQr
			String url  = pubAddressIpervice.getActionUrl("GET_FLOW_PAY_QR","");
			LOGGER.info("流量卡充值记录查询 getFlowPayQr---url:{}", url);
			String result = HttpClientHelperForEop.postToBill(url, requestJson, areaCode);
			LOGGER.info("流量卡充值记录查询 getFlowPayQr 出参:{}", result);
			IBSResponseTransform transform = SpringUtil.getBean(BSResponseBusinessTypeEnum.getBeanName("callNewFlowPayQr"));
			return transform.transform(result, tranId).toString();
		} catch (Exception e) {
			LOGGER.error("流量卡充值记录查询 getFlowPayQr Exception: {}", e.getMessage());
			return Utils.dealExceptionResp(responseHead, tranId, responseJson).toString();
		}

	}

	/**
	 * 流量卡充值记录查询 getFlowPayQr 入参转换
	 */
	private String getFlowPayQrRequestJson(JSONObject body, String areaCode) {
		JSONObject requestObj = new JSONObject();
		requestObj.put("lQuerySourceID", SystemEnum.SGW_ORG_ID.getValue());
		requestObj.put("sQuerySerial", UUIDHelper.currentTimeStampString());
		// 新增
		requestObj.put("sCurDate", body.getString("curDate"));
		requestObj.put("sAccNbr", body.getString("accNbr"));
		requestObj.put("sAreaCode", areaCode);
		requestObj.put("lGetFlag", body.getString("getFlag"));
		requestObj.put("sBeginDate", body.getString("begDate"));
		requestObj.put("sEndDate", body.getString("endDate"));
		return requestObj.toString();
	}

	/**
	 * UQC: 流量卡使用详单查询 callNewFlowDatauseQr
	 * BS: 流量卡使用记录查询 getTktYiFFlowCardUserdQr
	 */
	public String callNewFlowDatauseQrNew(ReqInfo reqInfo){
		RespInfo responseJson = new RespInfo();
		RespHead responseHead = new RespHead();
		responseJson.setHead(responseHead);
		String tranId = "";
		try {
			tranId = reqInfo.getHead().getTranId();
			String areaCode = reqInfo.getHead().getAreaCode();
			JSONObject body = reqInfo.getBody();
			LOGGER.info("流量卡使用记录查询 getTktYiFFlowCardUserdQr 开始入参转换");
			String requestJson = this.getTktYiFFlowCardUserdQrRequestJson(body, areaCode);
			LOGGER.info("流量卡使用记录查询 getTktYiFFlowCardUserdQr 入参:{}", requestJson);
//			http://jsjteop.telecomjs.com:8764/jseop/billing_zw/getTktYiFFlowCardUserdQr/getTktYiFFlowCardUserdQr
			String url  = pubAddressIpervice.getActionUrl("TGET_TKT_YI_F_FLOW_CARD_USERD_QR","");
			LOGGER.info("流量卡使用记录查询 getTktYiFFlowCardUserdQr---url:{}", url);
			String result = HttpClientHelperForEop.postToBill(url, requestJson, areaCode);
			LOGGER.info("流量卡使用记录查询 getTktYiFFlowCardUserdQr 出参:{}", result);
			IBSResponseTransform transform = SpringUtil.getBean(BSResponseBusinessTypeEnum.getBeanName("callNewFlowDatauseQr"));
			return transform.transform(result, tranId).toString();
		} catch (Exception e) {
			LOGGER.error("流量卡使用记录查询 getTktYiFFlowCardUserdQr Exception: {}", e.getMessage());
			return Utils.dealExceptionResp(responseHead, tranId, responseJson).toString();
		}
	}

	/**
	 * 流量卡使用记录查询 getTktYiFFlowCardUserdQr 入参转换
	 */
	private String getTktYiFFlowCardUserdQrRequestJson(JSONObject body, String areaCode) {
		JSONObject requestObj = new JSONObject();
		requestObj.put("ACCT_ID", body.getString("acctId"));
		requestObj.put("SERV_ID", body.getString("servId"));
		requestObj.put("ACC_NBR", body.getString("accNbr"));
		requestObj.put("AREA_CODE", areaCode);
		requestObj.put("BEGIN_DATE", body.getString("begDate"));
		requestObj.put("END_DATE", body.getString("endDate"));
		requestObj.put("GET_FLAG", body.getString("getFlag"));

		return requestObj.toString();
	}

	/**
	 * UQC: 互联星空清单 callNewInfoDataQr
	 * BS: 互联星空清单 TktGuInfoDataQr
	 */
	public String callNewInfoDataQrNew(ReqInfo reqInfo) {
		RespInfo responseJson = new RespInfo();
		RespHead responseHead = new RespHead();
		responseJson.setHead(responseHead);
		String tranId = "";
		try {
			tranId = reqInfo.getHead().getTranId();
			String areaCode = reqInfo.getHead().getAreaCode();
			JSONObject body = reqInfo.getBody();
			LOGGER.info("互联星空清单 TktGuInfoDataQr 开始入参转换");
			String requestJson = this.getTktGuInfoIptvDataValueQrRequestJson(body, areaCode);
			LOGGER.info("互联星空清单 TktGuInfoDataQr 入参:{}", requestJson);
//			http://jsjteop.telecomjs.com:8764/jseop/billing_zw/TktGuInfoDataQr/TktGuInfoDataQr
			String url  = pubAddressIpervice.getActionUrl("TKT_GU_INFO_DATA_QR","");
			LOGGER.info("互联星空清单 TktGuInfoDataQr---url:{}", url);
			String result = HttpClientHelperForEop.postToBill(url, requestJson, areaCode);
			LOGGER.info("互联星空清单 TktGuInfoDataQr 出参:{}", result);
			IBSResponseTransform transform = SpringUtil.getBean(BSResponseBusinessTypeEnum.getBeanName("callNewInfoDataQr"));
			return transform.transform(result, tranId).toString();
		} catch (Exception e) {
			LOGGER.error("互联星空清单 TktGuInfoDataQr Exception: {}", e.getMessage());
			return Utils.dealExceptionResp(responseHead, tranId, responseJson).toString();
		}
	}

	/**
	 * UQC: IPTV使用清单 callNewIptvDataQr
	 * BS: ITV清单 TktGuIptvDataQr
	 */
	public String callNewIptvDataQrNew(ReqInfo reqInfo) {
		RespInfo responseJson = new RespInfo();
		RespHead responseHead = new RespHead();
		responseJson.setHead(responseHead);
		String tranId = "";
		try {
			tranId = reqInfo.getHead().getTranId();
			String areaCode = reqInfo.getHead().getAreaCode();
			JSONObject body = reqInfo.getBody();
			LOGGER.info("ITV清单 TktGuIptvDataQr 开始入参转换");
			String requestJson = this.getTktGuInfoIptvDataValueQrRequestJson(body, areaCode);
			LOGGER.info("ITV清单 TktGuIptvDataQr 入参:{}", requestJson);
//			http://jsjteop.telecomjs.com:8764/jseop/billing_zw/TktGuIptvDataQr/TktGuIptvDataQr
			String url  = pubAddressIpervice.getActionUrl("TKT_GU_IPTV_DATA_QR","");
			LOGGER.info("ITV清单 TktGuIptvDataQr---url:{}", url);
			String result = HttpClientHelperForEop.postToBill(url, requestJson, areaCode);
			LOGGER.info("ITV清单 TktGuIptvDataQr 出参:{}", result);
			IBSResponseTransform transform = SpringUtil.getBean(BSResponseBusinessTypeEnum.getBeanName("callNewIptvDataQr"));
			return transform.transform(result, tranId).toString();
		} catch (Exception e) {
			LOGGER.error("ITV清单 TktGuIptvDataQr Exception: {}", e.getMessage());
			return Utils.dealExceptionResp(responseHead, tranId, responseJson).toString();
		}
	}

	/**
	 * UQC: ITV点播清单 callNewIptvValueAdderQr
	 * BS: ITV点播清单 TktGuIptvValueAddedQr
	 */
	public String callNewIptvValueAdderQrNew(ReqInfo reqInfo) {
		RespInfo responseJson = new RespInfo();
		RespHead responseHead = new RespHead();
		responseJson.setHead(responseHead);
		String tranId = "";
		try {
			tranId = reqInfo.getHead().getTranId();
			String areaCode = reqInfo.getHead().getAreaCode();
			JSONObject body = reqInfo.getBody();
			String requestJson = this.getTktGuInfoIptvDataValueQrRequestJson(body, areaCode);
			LOGGER.info("ITV点播清单 TktGuIptvValueAddedQr 入参:{}", requestJson);
//			http://jsjteop.telecomjs.com:8764/jseop/billing_zw/TktGuIptvValueAddedQr/TktGuIptvValueAddedQr
			String url  = pubAddressIpervice.getActionUrl("TKT_GU_IPTV_VALUE_ADDED_QR","");
			LOGGER.info("ITV点播清单 TktGuIptvValueAddedQr---url:{}", url);
			String result = HttpClientHelperForEop.postToBill(url, requestJson, areaCode);
			LOGGER.info("ITV点播清单 TktGuIptvValueAddedQr 出参:{}", result);
			IBSResponseTransform transform = SpringUtil.getBean(BSResponseBusinessTypeEnum.getBeanName("callNewIptvValueAdderQr"));
			return transform.transform(result, tranId).toString();
		} catch (Exception e) {
			LOGGER.error("ITV点播清单 TktGuIptvValueAddedQr Exception: {}", e.getMessage());
			return Utils.dealExceptionResp(responseHead, tranId, responseJson).toString();
		}
	}

	/**
	 * 互联星空清单 TktGuInfoDataQr 入参转换
	 * ITV清单 TktGuIptvDataQr 入参转换
	 * ITV点播清单 TktGuIptvValueAddedQr 入参转换
	 */
	public String getTktGuInfoIptvDataValueQrRequestJson(JSONObject body, String areaCode) {
		String begDate = body.getString("begDate");
		String endDate = body.getString("endDate");
		String getFlag = DateHelper.isAllMonthFlag(begDate, endDate)?"1":"3";
		JSONObject requestObj = new JSONObject();
		requestObj.put("ACC_NBR", body.getString("accNbr"));
		requestObj.put("AREA_CODE", areaCode);
		requestObj.put("ACCT_NAME", body.getString("acctName"));
		requestObj.put("GET_FLAG", getFlag);
		requestObj.put("BEGIN_DATE", begDate);
		requestObj.put("END_DATE", endDate);
		requestObj.put("TICKET_ID", body.getString("ticketId"));
		requestObj.put("FAMILY_ID", body.getString("productId"));
		return requestObj.toString();
	}

	/**
	 * UQC: 市话话单信息 callNewLsTicket
	 * BS: 市话清单 TktGuLsTicketQr
	 */
	public String callNewLsTicketNew(ReqInfo reqInfo) {
		RespInfo responseJson = new RespInfo();
		RespHead responseHead = new RespHead();
		responseJson.setHead(responseHead);
		String tranId = "";
		try {
			tranId = reqInfo.getHead().getTranId();
			String areaCode = reqInfo.getHead().getAreaCode();
			JSONObject body = reqInfo.getBody();
			String requestJson = this.getTktGuLsTicketQrRequestJson(body, areaCode);
			LOGGER.info("市话清单 TktGuLsTicketQr 入参:{}", requestJson);
//			http://jsjteop.telecomjs.com:8764/jseop/billing_zw/TktGuLsTicketQr/TktGuLsTicketQr
			String url  = pubAddressIpervice.getActionUrl("TKT_GU_LS_TICKET_QR","");
			LOGGER.info("市话清单 TktGuLsTicketQr---url:{}", url);
			String result = HttpClientHelperForEop.postToBill(url, requestJson, areaCode);
			LOGGER.info("市话清单 TktGuLsTicketQr 出参:{}", result);
			IBSResponseTransform transform = SpringUtil.getBean(BSResponseBusinessTypeEnum.getBeanName("callNewLsTicket"));
			return transform.transform(result, tranId).toString();
		} catch (Exception e) {
			LOGGER.error("市话清单 TktGuLsTicketQr Exception: {}", e.getMessage());
			return Utils.dealExceptionResp(responseHead, tranId, responseJson).toString();
		}
	}

	/**
	 * 市话清单 TktGuLsTicketQr 入参转换
	 */
	private String getTktGuLsTicketQrRequestJson(JSONObject body, String areaCode) {
		String begDate = body.getString("begDate");
		String endDate = body.getString("endDate");
		String getFlag = DateHelper.isAllMonthFlag(begDate, endDate)?"1":"3";
		JSONObject requestObj = new JSONObject();
		requestObj.put("ACC_NBR", body.getString("accNbr"));
		requestObj.put("AREA_CODE", areaCode);
		requestObj.put("GET_FLAG", getFlag);
		requestObj.put("BEGIN_DATE", begDate);
		requestObj.put("END_DATE", endDate);
		requestObj.put("TICKET_ID", body.getString("ticketId"));
		requestObj.put("FAMILY_ID", body.getString("productId"));
		requestObj.put("MODULE_ID", body.getString("moduleId"));
		requestObj.put("INVOICE_CNT", body.getString("invoiceCnt"));
		return requestObj.toString();
	}

	/**
	 * UQC: 电话清单查询 callNewMessageTicketQr
	 * BS: 电话信息清单 TktGuMessageTicketQr
	 */
	public String callNewMessageTicketQrNew(ReqInfo reqInfo) {
		RespInfo responseJson = new RespInfo();
		RespHead responseHead = new RespHead();
		responseJson.setHead(responseHead);
		String tranId = "";
		try {
			tranId = reqInfo.getHead().getTranId();
			String areaCode = reqInfo.getHead().getAreaCode();
			JSONObject body = reqInfo.getBody();
			String requestJson = this.getTktGuMessageTicketQrRequestJson(body, areaCode);
			LOGGER.info("电话信息清单 TktGuMessageTicketQr 入参:{}", requestJson);
//			http://jsjteop.telecomjs.com:8764/jseop/billing_zw/TktGuMessageTicketQr/TktGuMessageTicketQr
			String url  = pubAddressIpervice.getActionUrl("TKT_GU_MESSAGE_TICKET_QR","");
			LOGGER.info("电话信息清单 TktGuMessageTicketQr---url:{}", url);
			String result = HttpClientHelperForEop.postToBill(url, requestJson, areaCode);
			LOGGER.info("电话信息清单 TktGuMessageTicketQr 出参:{}", result);
			IBSResponseTransform transform = SpringUtil.getBean(BSResponseBusinessTypeEnum.getBeanName("callNewMessageTicketQr"));
			return transform.transform(result, tranId).toString();
		} catch (Exception e) {
			LOGGER.error("电话信息清单 TktGuMessageTicketQr Exception: {}", e.getMessage());
			return Utils.dealExceptionResp(responseHead, tranId, responseJson).toString();
		}
	}

	/**
	 * 电话信息清单 TktGuMessageTicketQr 入参转换
	 */
	public String getTktGuMessageTicketQrRequestJson(JSONObject body, String areaCode) {
		String begDate = body.getString("begDate");
		String endDate = body.getString("endDate");
		String getFlag = DateHelper.isAllMonthFlag(begDate, endDate)?"1":"3";
		JSONObject requestObj = new JSONObject();
		requestObj.put("ACC_NBR", body.getString("accNbr"));
		requestObj.put("AREA_CODE", areaCode);
		requestObj.put("GET_FLAG", getFlag);
		requestObj.put("BEGIN_DATE", begDate);
		requestObj.put("END_DATE", endDate);
		requestObj.put("TICKET_ID", body.getString("ticketId"));
		requestObj.put("FAMILY_ID", body.getString("productId"));
		return requestObj.toString();
	}

	/**
	 * UQC: 窄带清单查询 callNewNarrowDataQr
	 * BS: 窄带清单 TktGuNarrowDataQr
	 */
	public String callNewNarrowDataQrNew(ReqInfo reqInfo) {
		RespInfo responseJson = new RespInfo();
		RespHead responseHead = new RespHead();
		responseJson.setHead(responseHead);
		String tranId = "";
		try {
			tranId = reqInfo.getHead().getTranId();
			String areaCode = reqInfo.getHead().getAreaCode();
			JSONObject body = reqInfo.getBody();
			String requestJson = this.getTktGuNarrowDataQrRequestJson(body, areaCode);
			LOGGER.info("窄带清单 TktGuNarrowDataQr 入参:{}", requestJson);
//			http://jsjteop.telecomjs.com:8764/jseop/billing_zw/TktGuNarrowDataQr/TktGuNarrowDataQr
			String url  = pubAddressIpervice.getActionUrl("TKT_GU_NARROW_DATA_QR","");
			LOGGER.info("窄带清单 TktGuNarrowDataQr---url:{}", url);
			String result = HttpClientHelperForEop.postToBill(url, requestJson, areaCode);
			LOGGER.info("窄带清单 TktGuNarrowDataQr 出参:{}", result);
			IBSResponseTransform transform = SpringUtil.getBean(BSResponseBusinessTypeEnum.getBeanName("callNewNarrowDataQr"));
			return transform.transform(result, tranId).toString();
		} catch (Exception e) {
			LOGGER.error("窄带清单 TktGuNarrowDataQr Exception: {}", e.getMessage());
			return Utils.dealExceptionResp(responseHead, tranId, responseJson).toString();
		}
	}

	/**
	 * 窄带清单 TktGuNarrowDataQr 入参转换
	 */
	public String getTktGuNarrowDataQrRequestJson(JSONObject body, String areaCode) {
		String begDate = body.getString("begDate");
		String endDate = body.getString("endDate");
		String getFlag = DateHelper.isAllMonthFlag(begDate, endDate)?"1":"3";
		JSONObject requestObj = new JSONObject();
		requestObj.put("ACC_NBR", body.getString("accNbr"));
		requestObj.put("ACCT_NAME", body.getString("acctName"));
		requestObj.put("AREA_CODE", areaCode);
		requestObj.put("GET_FLAG", getFlag);
		requestObj.put("BEGIN_DATE", begDate);
		requestObj.put("END_DATE", endDate);
		requestObj.put("TICKET_ID", body.getString("ticketId"));
		requestObj.put("FAMILY_ID", body.getString("productId"));
		return requestObj.toString();
	}

	/**
	 * UQC: 账单查询 callNewBillReqNew
	 * BS: 客户化帐单接口 qryNewBillSvr
	 */
	public String callNewBillReqNew(ReqInfo reqInfo) {
		RespInfo responseJson = new RespInfo();
		RespHead responseHead = new RespHead();
		responseJson.setHead(responseHead);
		String tranId = "";
		try {
			tranId = reqInfo.getHead().getTranId();
			String areaCode = reqInfo.getHead().getAreaCode();
			JSONObject body = reqInfo.getBody();
			String requestJson = this.getQryCustBillRequestJson(body, areaCode);
			LOGGER.info("客户化帐单接口 qryNewBillSvr 入参:{}", requestJson);
//			http://jsjteop.telecomjs.com:8764/jseop/billing_zw/qryNewBillSvr/qryNewBillSvr
			String url  = pubAddressIpervice.getActionUrl("QRY_NEW_BILL_SVR","");
			LOGGER.info("客户化帐单接口 qryNewBillSvr---url:{}", url);
			String result = HttpClientHelperForEop.postToBill(url, requestJson, areaCode);
			LOGGER.info("客户化帐单接口 qryNewBillSvr 出参:{}", result);
			IBSResponseTransform transform = SpringUtil.getBean(BSResponseBusinessTypeEnum.getBeanName("callNewBillReq"));
			return transform.transform(result, tranId).toString();

		} catch (Exception e) {
			LOGGER.error("客户化帐单接口 qryNewBillSvr Exception: {}", e.getMessage());
			return Utils.dealExceptionResp(responseHead, tranId, responseJson).toString();
		}
	}
	public String qrySpDetail(ReqInfo reqInfo) {
		RespInfo responseJson = new RespInfo();
		RespHead responseHead = new RespHead();
		responseJson.setHead(responseHead);
        String tranId = "";
        try {
            tranId = reqInfo.getHead().getTranId();
            String areaCode = reqInfo.getHead().getAreaCode();
            JSONObject body = reqInfo.getBody();
            String requestJson = this.getQrySpDetailRequestJson(body, areaCode);
            LOGGER.info("增值业务明细 qrySpDetail 入参:{}", requestJson);
//			http://jsjteop.telecomjs.com:8764/jseop/billing_zw/qrySpDetail
			String url  = pubAddressIpervice.getActionUrl("QRY_SP_DETAIL","");
			LOGGER.info("增值业务明细 qrySpDetail---url:{}", url);
            String result = HttpClientHelperForEop.postToBill(url, requestJson, areaCode);
            LOGGER.info("增值业务明细 qrySpDetail 出参:{}", result);
            IBSResponseTransform transform = SpringUtil.getBean(BSResponseBusinessTypeEnum.getBeanName("commonMethod"));
            return transform.transform(result, tranId).toString();
        } catch (Exception e) {
            LOGGER.error("增值业务明细 qrySpDetail Exception: {}", e.getMessage());
            return Utils.dealExceptionResp(responseHead, tranId, responseJson).toString();
        }
    }
	
	public String getTktYiValueTicketQrByDayCount(ReqInfo reqInfo) {
		RespInfo responseJson = new RespInfo();
		RespHead responseHead = new RespHead();
		responseJson.setHead(responseHead);
        String tranId = "";
        try {
            tranId = reqInfo.getHead().getTranId();
            String areaCode = reqInfo.getHead().getAreaCode();
            JSONObject body = reqInfo.getBody();
            String requestJson = this.getTktYiValueTicketQrByDayCountRequestJson(body, areaCode);
            LOGGER.info("移动用户当月数据详单按天累计查询 getTktYiValueTicketQrByDayCount 入参:{}", requestJson);
//			http://jsjteop.telecomjs.com:8764/jseop/billing_cx/getTktYiValueTicketQrByDayCount
			String url  = pubAddressIpervice.getActionUrl("QRY_TKT_YI_VALUE_TICKET_QR_BY_DAYCOUNT","");
			LOGGER.info("移动用户当月数据详单按天累计查询 getTktYiValueTicketQrByDayCount---url:{}", url);
            String result = HttpClientHelperForEop.postToBill(url, requestJson, areaCode);
            LOGGER.info("移动用户当月数据详单按天累计查询 getTktYiValueTicketQrByDayCount 出参:{}", result);
            IBSResponseTransform transform = SpringUtil.getBean(BSResponseBusinessTypeEnum.getBeanName("getTktYiValueTicketQrByDayCount"));
            return transform.transform(result, tranId).toString();
        } catch (Exception e) {
            LOGGER.error("移动用户当月数据详单按天累计查询 getTktYiValueTicketQrByDayCount Exception: {}", e.getMessage());
            return Utils.dealExceptionResp(responseHead, tranId, responseJson).toString();
        }
    }

	/**
	 * 客户化帐单接口 qryNewBillSvr 入参转换
	 */
	private String getQryCustBillRequestJson(JSONObject body, String areaCode) {
		JSONObject requestObj = new JSONObject();
		requestObj.put("lQuerySourceID", SystemEnum.SGW_ORG_ID.getValue());
		requestObj.put("sQuerySerial", UUIDHelper.currentTimeStampString());
		requestObj.put("sAccNbr", body.getString("accNbr"));
		requestObj.put("areaCode", areaCode);
		requestObj.put("lBillingCycle", body.getString("billingCycleId"));
		requestObj.put("lQueryFlag", body.getString("queryFlag"));
		requestObj.put("lFamilyId", body.getString("productId"));
		return requestObj.toString();
	}
	
	private String getQrySpDetailRequestJson(JSONObject body, String areaCode) {
        JSONObject requestObj = new JSONObject();
        requestObj.put("servId", body.getString("servId"));
        requestObj.put("areaCode", areaCode);
        requestObj.put("acctId", body.getString("acctId"));
        requestObj.put("billItemTypeID", body.getString("billItemTypeID"));
        requestObj.put("lBillingCycle", body.getString("lBillingCycle"));
        return requestObj.toString();
    }
	
	private String getTktYiValueTicketQrByDayCountRequestJson(JSONObject body, String areaCode) {
        JSONObject requestObj = new JSONObject();
        requestObj.put("accNbr", body.getString("accNbr"));
        requestObj.put("areaCode", areaCode);
        requestObj.put("GET_FLAG", body.getString("GET_FLAG"));
        requestObj.put("END_DATE", body.getString("END_DATE"));
        requestObj.put("BEGIN_DATE", body.getString("BEGIN_DATE"));
        requestObj.put("FAMILY_ID", body.getString("FAMILY_ID"));
        return requestObj.toString();
    }


	/**
	 * UQC: 移动用户上网及数据通信详单查询 callNewDataTicketQrReq
	 * BS: 移动用户上网及数据通信详单查询 TktYiNewDataTicketQr
	 */
	public String callNewDataTicketQrReqNew(ReqInfo reqInfo) {
		RespInfo responseJson = new RespInfo();
		RespHead responseHead = new RespHead();
		responseJson.setHead(responseHead);
		String tranId = "";
		try {
			tranId = reqInfo.getHead().getTranId();
			String areaCode = reqInfo.getHead().getAreaCode();
			JSONObject body = reqInfo.getBody();
			String requestJson = this.getTktYiNewDataTicketQrRequestJson(body, areaCode);
			LOGGER.info("移动用户上网及数据通信详单查询 TktYiNewDataTicketQr 入参:{}", requestJson);
//			http://jsjteop.telecomjs.com:8764/jseop/billing_zw/TktYiNewDataTicketQr/TktYiNewDataTicketQr
			String url  = pubAddressIpervice.getActionUrl("TKT_YI_NEW_DATA_TICKET_QR","");
			LOGGER.info("移动用户上网及数据通信详单查询 TktYiNewDataTicketQr---url:{}", url);
			String result = HttpClientHelperForEop.postToBill(url, requestJson, areaCode);
			LOGGER.info("移动用户上网及数据通信详单查询 TktYiNewDataTicketQr 出参:{}", result);
			IBSResponseTransform transform = SpringUtil.getBean(BSResponseBusinessTypeEnum.getBeanName("callNewDataTicketQrReq"));
			return transform.transform(result, tranId).toString();
		} catch (Exception e) {
			LOGGER.error("移动用户上网及数据通信详单查询 TktYiNewDataTicketQr Exception: {}", e.getMessage());
			return Utils.dealExceptionResp(responseHead, tranId, responseJson).toString();
		}
	}

	/**
	 * 移动用户上网及数据通信详单查询 TktYiNewDataTicketQr 入参转换
	 */
	private String getTktYiNewDataTicketQrRequestJson(JSONObject body, String areaCode) {
		String accNbr = body.getString("accNbr");
		String family = body.getString("family");
		String begDate = body.getString("begDate");
		String endDate = body.getString("endDate");
		String getFlag = DateHelper.isAllMonthFlag(begDate, endDate)?"1":"3";
		JSONObject requestObj = new JSONObject();
		requestObj.put("ACC_NBR", accNbr);
		requestObj.put("AREA_CODE", areaCode);
		requestObj.put("GET_FLAG", getFlag);
		requestObj.put("BEGIN_DATE", begDate);
		requestObj.put("END_DATE", endDate);
		requestObj.put("FAMILY_ID", family );

		return requestObj.toString();
	}

	/**
	 * UQC: 充值缴费查询 callNewPayReq
	 * BS: 充值缴费记录查询 getNewQrPay
	 */
	public String callNewPayReqNew(ReqInfo reqInfo) {
		RespInfo responseJson = new RespInfo();
		RespHead responseHead = new RespHead();
		responseJson.setHead(responseHead);
		String tranId = "";
		try {
			tranId = reqInfo.getHead().getTranId();
			String areaCode = reqInfo.getHead().getAreaCode();
			JSONObject body = reqInfo.getBody();
			String requestJson = this.getGetNewQrPayRequestJson(body, areaCode);
			LOGGER.info("充值缴费记录查询 getNewQrPay 入参:{}", requestJson);
//			http://jsjteop.telecomjs.com:8764/jseop/billing_zw/accuUseQryProvince/accuUseQryProvince
			String url  = pubAddressIpervice.getActionUrl("GET_NEW_QR_PAY","");
			LOGGER.info("充值缴费记录查询 getNewQrPay---url:{}", url);
			String result = HttpClientHelperForEop.postToBill(url, requestJson, areaCode);
			LOGGER.info("充值缴费记录查询 getNewQrPay 出参:{}", result);
			IBSResponseTransform transform = SpringUtil.getBean(BSResponseBusinessTypeEnum.getBeanName("callNewPayReq"));
			return transform.transform(result, tranId).toString();
		} catch (Exception e) {
			LOGGER.error("充值缴费记录查询 getNewQrPay Exception: {}", e.getMessage());
			return Utils.dealExceptionResp(responseHead, tranId, responseJson).toString();
		}
	}

	/**
	 * 充值缴费记录查询 getNewQrPay 入参转换
	 */
	private String getGetNewQrPayRequestJson(JSONObject body, String areaCode) {
		JSONObject requestObj = new JSONObject();
		requestObj.put("lQuerySourceID", SystemEnum.SGW_ORG_ID.getValue());
		requestObj.put("sQuerySerial", UUIDHelper.currentTimeStampString());
		requestObj.put("sAccNbr", body.getString("accNbr"));
		requestObj.put("sAcctNbr97", body.getString("accNbr97"));
		requestObj.put("sAreaCode", areaCode);
		requestObj.put("lGetFlag", body.getString("getFlag"));
		requestObj.put("sBeginDate", body.getString("begDate"));
		requestObj.put("sEndDate", body.getString("endDate"));

		return requestObj.toString();
	}

	/**
	 * UQC: 一致性余额查询 callConQueryBalance
	 * BS: 余额查询 QueryBalance
	 */
	public JSONObject callConQueryBalanceNew(JSONObject requestObj) {
		try {
			String areaCode = requestObj.getString("dccAreaCode");
			String requestJson = this.getQueryBalanceRequestJson(requestObj, areaCode);
			LOGGER.info("余额查询 QueryBalance 入参:{}", requestJson);
//			http://jsjteop.telecomjs.com:8764/jseop/billing_zw/ECQueryBalance
			String url  = pubAddressIpervice.getActionUrl("CON_QUERY_BALANCE","");
			LOGGER.info("余额查询 QueryBalance---url:{}", url);
			String result = HttpClientHelperForEop.postToBill(url, requestJson, areaCode);
			LOGGER.info("余额查询 QueryBalance 出参:{}", result);
			ISGWResponseTransform transform = SpringUtil.getBean(BSResponseBusinessTypeEnum.getBeanName("callConQueryBalance"));
			return transform.transform(result);
		} catch (Exception e) {
			LOGGER.error("余额查询 QueryBalance Exception: {}", e.getMessage());
			return Utils.dealResponseForSgw("99", "服务器内部错误", "fail", null);
		}
	}

	/**
	 * 余额查询 QueryBalance 入参转换
	 */
	private String getQueryBalanceRequestJson(JSONObject body, String areaCode) {
		JSONObject requestObj = new JSONObject();
		requestObj.put("areaCode", areaCode);
		requestObj.put("queryItemType", body.getString("dccQueryType"));
		requestObj.put("queryFlag", body.getString("dccQueryFlag"));
		requestObj.put("operAttrStruct", this.getOperAttrStruct(areaCode));
		String objType = "3";
		String objValue = body.getString("dccAccNbr");
		String objAttr = body.getString("dccDestinationAttr");
		String dataArea = "2";
		requestObj.put("svcObjectStruct", this.getSvcObjectStruct(objType, objValue, objAttr, dataArea));
		return requestObj.toString();
	}

	/**
	 * 操作人属性对象
	 */
	private JSONObject getOperAttrStruct(String areaCode) {
		JSONObject operAttrStruct = new JSONObject();
		operAttrStruct.put("staffId", "10001");
		operAttrStruct.put("operOrgId", areaCode);
		operAttrStruct.put("operTime", "");
		operAttrStruct.put("operPost", "");
		operAttrStruct.put("operServiceId", "");
		operAttrStruct.put("lanId", StaticDataMappingUtil.getRegionByAreaCode(areaCode));
		return operAttrStruct;
	}

	/**
	 * 服务对象条件
	 */
	private JSONObject getSvcObjectStruct(String objType, String objValue, String objAttr, String dataArea) {
		JSONObject svcObjectStruct = new JSONObject();
		svcObjectStruct.put("objType", objType);
		svcObjectStruct.put("objValue", objValue);
		svcObjectStruct.put("objAttr", objAttr);
		svcObjectStruct.put("dataArea", dataArea);
		return svcObjectStruct;
	}
	
	
	public String qryNewCustBill(ReqInfo reqInfo) {
		RespInfo responseJson = new RespInfo();
		RespHead responseHead = new RespHead();
		responseJson.setHead(responseHead);
		String tranId = "";
		try {
			tranId = reqInfo.getHead().getTranId();
			String areaCode = reqInfo.getHead().getAreaCode();
			JSONObject body = reqInfo.getBody();
			String requestJson = this.getQryNewCustBillRequestJson(body, areaCode);
			LOGGER.info("客户化帐单接口 QryNewCustBill 入参:{}", requestJson);
//			http://jsjteop.telecomjs.com:8764/jsbillcx/ECQryCustBill
			String url  = pubAddressIpervice.getActionUrl("QRY_NEW_CUST_BILL","");
			LOGGER.info("客户化帐单接口 QryNewCustBill---url:{}", url);
			String result = HttpClientHelperForEop.postToBill(url, requestJson, areaCode);
			LOGGER.info("客户化帐单接口 QryNewCustBill 出参:{}", result);
			JSONObject resObj = BSExceptionUtil.checkBillResponse(result, "resultCode", "resultMsg", "0");
			return Utils.dealSuccessRespWithBodyAndTranId(tranId, resObj).toJSONString();
		}catch (BSException e) {
			LOGGER.error("客户化帐单接口 qryNewCustBill 参数转换 BSException: {}", e.getErrorMsg());
			return Utils.dealBaseAppExceptionRespNew(tranId, e.getErrorMsg(), e.getErrorCode()).toString();
		} catch (Exception e) {
			LOGGER.error("客户化帐单接口 qryNewCustBill Exception: {}", e.getMessage());
			return Utils.dealExceptionResp(responseHead, tranId, responseJson).toString();
		}
	}
	
	public String qryInstantFeeList(ReqInfo reqInfo) {
		RespInfo responseJson = new RespInfo();
		RespHead responseHead = new RespHead();
		responseJson.setHead(responseHead);
		String tranId = "";
		try {
			tranId = reqInfo.getHead().getTranId();
			String areaCode = reqInfo.getHead().getAreaCode();
			JSONObject body = reqInfo.getBody();
			String requestJson = this.getQryNewCustBillRequestJson(body, areaCode);
			LOGGER.info("实时话费明细查询接口 qryInstantFeeList 入参:{}", requestJson);
//			http://jsjteop.telecomjs.com:8764/jseop/billingjf/ECQryInstantFeeList_DQ
			String url  = pubAddressIpervice.getActionUrl("QRY_INSTANT_FEE_LIST","");
			LOGGER.info("实时话费明细查询接口 qryInstantFeeList---url:{}", url);
			String result = HttpClientHelperForEop.postToBill(url, requestJson, areaCode);
			LOGGER.info("实时话费明细查询接口 qryInstantFeeList:{}", result);
			JSONObject resObj = BSExceptionUtil.checkBillResponse(result, "resultCode", "resultMsg", "0");
			
			return Utils.dealSuccessRespWithBodyAndTranId(tranId, resObj).toJSONString();
		}catch (BSException e) {
			LOGGER.error("实时话费明细查询接口 qryInstantFeeList 参数转换 BSException: {}", e.getErrorMsg());
			return Utils.dealBaseAppExceptionRespNew(tranId, e.getErrorMsg(), e.getErrorCode()).toString();
		} catch (Exception e) {
			LOGGER.error("实时话费明细查询接口 qryInstantFeeList Exception: {}", e.getMessage());
			return Utils.dealExceptionResp(responseHead, tranId, responseJson).toString();
		}
	}


	
	public String othBackoutFeeInfo(ReqInfo reqInfo) {
		RespInfo responseJson = new RespInfo();
		RespHead responseHead = new RespHead();
		responseJson.setHead(responseHead);
		String tranId = "";
		try {
			tranId = reqInfo.getHead().getTranId();
			String areaCode = reqInfo.getHead().getAreaCode();
			JSONObject body = reqInfo.getBody();
			String requestJson = this.getOthBackoutFeeInfoRequestJson(body, areaCode);
			LOGGER.info("拆机号码资料信息、欠费信息查询 othBackoutFeeInfo 入参:{}", requestJson);
//			http://jsjteop.telecomjs.com:8764/jsjf/CecServices/qry/othBackoutFeeInfo
			String url  = pubAddressIpervice.getActionUrl("BACK_OUT_FEE_INFO_URL","");
			LOGGER.info("拆机号码资料信息、欠费信息查询 othBackoutFeeInfo---url:{}", url);
			String result = HttpClientHelperForEop.postToBill(url, requestJson, areaCode);
			LOGGER.info("拆机号码资料信息、欠费信息查询 othBackoutFeeInfo出参:{}", result);
			JSONObject resObj = BSExceptionUtil.checkBillResponse(result, "resultCode", "resultMsg", "0");
			
			return Utils.dealSuccessRespWithBodyAndTranId(tranId, resObj).toJSONString();
		}catch (BSException e) {
			LOGGER.error("拆机号码资料信息、欠费信息查询 othBackoutFeeInfo 参数转换 BSException: {}", e.getErrorMsg());
			return Utils.dealBaseAppExceptionRespNew(tranId, e.getErrorMsg(), e.getErrorCode()).toString();
		} catch (Exception e) {
			LOGGER.error("拆机号码资料信息、欠费信息查询 othBackoutFeeInfo: {}", e.getMessage());
			return Utils.dealExceptionResp(responseHead, tranId, responseJson).toString();
		}
	}
	
	public String feeTrend(ReqInfo reqInfo) {
		RespInfo responseJson = new RespInfo();
		RespHead responseHead = new RespHead();
		responseJson.setHead(responseHead);
		String tranId = "";
		try {
			tranId = reqInfo.getHead().getTranId();
			String areaCode = reqInfo.getHead().getAreaCode();
			JSONObject body = reqInfo.getBody();
			LOGGER.info("消费趋势查询接口feeTrend 开始入参转换");
			String requestJson = this.getFeeTrendRequestJson(body, areaCode);
			LOGGER.info("消费趋势查询接口feeTrend 入参:{}", requestJson);
//			http://jsjteop.telecomjs.com:8764/jseop/billingjf/FeeTrend
			String url  = pubAddressIpervice.getActionUrl("QRY_FEE_TREND","");
			LOGGER.info("消费趋势查询接口feeTrend---url:{}", url);
			String result = HttpClientHelperForEop.postToBill(url, requestJson, areaCode);
			LOGGER.info("消费趋势查询接口feeTrend 出参:{}", result);
			JSONObject resObj = BSExceptionUtil.checkBillResponse(result, "errorCode", "errorMsg", "0");
			LOGGER.debug("消费趋势查询接口feeTrend 出参转换成功",resObj.toString());
			return Utils.dealSuccessRespWithBodyAndTranId(tranId, resObj).toJSONString();
		}catch (BSException e) {
			LOGGER.error("消费趋势查询接口feeTrend 参数转换 BSException: {}", e.getErrorMsg());
			return Utils.dealBaseAppExceptionRespNew(tranId, e.getErrorMsg(), e.getErrorCode()).toString();
		} catch (Exception e) {
			LOGGER.error("消费趋势查询接口feeTrend Exception: {}", e.getMessage());
			return Utils.dealExceptionResp(responseHead, tranId, responseJson).toString();
		}
	}



	
	/** @version: 
	* @Description: 1 获取账单查询接口入参billingCycle  2获取实时话费明细查询入参 billingCycleId
	* @author: zhy 
	* @date 2022年4月8日 下午4:36:09
	* @return  
	*/
	private String getQryNewCustBillRequestJson(JSONObject body, String areaCode) {
		JSONObject requestObj = new JSONObject();
		String accNbr=body.getString("accNbr");
		//查询业务类型 1:帐户标识 2:用户标识 3:用户号码 4:客户标识 5:销售品实例 6:用户账号
		String accNbrType=body.getString("accNbrType");
		String queryFlag=body.getString("queryFlag");
		//0-固话 1-小灵通 2-移动	
		String dccDestinationAttr=body.getString("dccDestinationAttr");
		String billingCycle=body.getString("billingCycle");
		JSONObject svcObjectStruct = getSvcObjectStruct(accNbrType, accNbr, dccDestinationAttr, queryFlag);
		JSONObject operAttrStruct = getOperAttrStruct(areaCode);
		requestObj.put("svcObjectStruct",svcObjectStruct);
		requestObj.put("operAttrStruct", operAttrStruct);
		requestObj.put("billingCycleId", billingCycle);

		return requestObj.toString();
	}
	
	private String getOthBackoutFeeInfoRequestJson(JSONObject body, String areaCode) {
		JSONObject requestObj = new JSONObject();
		String accNbr=body.getString("accNbr");
		//必填，0-固化 2-天翼
		Long familyId=body.getLong("familyId");

		//必填，拆机用户身份证号码
		String identityCard=body.getString("identityCard");

		requestObj.put("accNbr",accNbr);
		requestObj.put("areaCode", areaCode);
		requestObj.put("familyId",familyId);
		requestObj.put("identityCard", identityCard);
		return requestObj.toString();
	}

	/** @version: 
	* @Description:getFlag:0：账户级 1:用户级    default:0:默认最近12个月 1：以开始账期 结束账期为准
	* @author: shty 
	* @date 2022.11.11
	* @return  
	*/
	private String getFeeTrendRequestJson(JSONObject body, String areaCode) {
		JSONObject requestObj = new JSONObject();
		requestObj.put("lGetFlag",body.getString("getFlag"));
		requestObj.put("sAreaCode",areaCode);
		requestObj.put("sAccNbr",body.getString("accNbr"));
		requestObj.put("lDefault",body.getString("default"));
		requestObj.put("startBillingCycle",body.getString("startBillingCycle"));
		requestObj.put("endBillingCycle",body.getString("endBillingCycle"));
		return requestObj.toString();
	}

	public String trialPricingInfo(ReqInfo reqInfo) {
		RespInfo responseJson = new RespInfo();
		RespHead responseHead = new RespHead();
		responseJson.setHead(responseHead);
		String result="";
		String tranId="";
		try {
			tranId = reqInfo.getHead().getTranId();
			JSONObject body = reqInfo.getBody();
			String areaCode = reqInfo.getHead().getAreaCode();
			String requestJson = this.trialPricingJson(body);
			LOGGER.info("电渠套餐试算接口trialPricingInfo入参:{}", requestJson);
			if(areaCode.equals("025")){
//				http://openapi.telecomjs.com/eop/bam/trialPricing/trialPricing
				String njUrl  = pubAddressIpervice.getActionUrl("TRIAL_PRICING_INFO_NJ_URL","");
				LOGGER.info("电渠套餐试算接口trialPricingInfo--025---njUrl:{}", njUrl);
				result = HttpClientHelperForEop.newPostToBill(njUrl, requestJson, areaCode);
			}else{
//				http://jsjteop.telecomjs.com:8764/jseop/billingjf/TrialPricing
				String url  = pubAddressIpervice.getActionUrl("TRIAL_PRICING_INFO_URL","");
				LOGGER.info("电渠套餐试算接口trialPricingInfo---url:{}", url);
				result = HttpClientHelperForEop.postToBill(url, requestJson, areaCode);
			}
			LOGGER.info("电渠套餐试算接口trialPricingInfo 出参:{}", result);
			JSONObject resObj = BSExceptionUtil.checkBillResponse(result, "result_code", "result_msg", "0");
			LOGGER.debug("电渠套餐试算接口trialPricingInfo 出参转换成功");
			return Utils.dealSuccessRespWithBodyAndTranId(tranId, resObj).toJSONString();
		}catch (BSException e) {
			LOGGER.error("电渠套餐试算接口trialPricingInfo 参数转换 BSException: {}", e.getErrorMsg());
			return Utils.dealBaseAppExceptionRespNew(tranId, e.getErrorMsg(), e.getErrorCode()).toString();
		} catch (Exception e) {
			LOGGER.error("电渠套餐试算接口trialPricingInfo Exception: {}", e.getMessage());
			return Utils.dealExceptionResp(responseHead, tranId, responseJson).toString();
		}
	}


	private String trialPricingJson(JSONObject body) {
		JSONObject requestObj = new JSONObject();
		JSONArray acctList = body.getJSONArray("acct_list");
		requestObj.put("acct_list",acctList);
		requestObj.put("message_id",body.getString("message_id"));
		requestObj.put("request_date",body.getString("request_date"));
		requestObj.put("pricing_type",body.getString("pricing_type"));
		requestObj.put("access_channel",body.getString("access_channel"));
		return requestObj.toString();
	}



}
