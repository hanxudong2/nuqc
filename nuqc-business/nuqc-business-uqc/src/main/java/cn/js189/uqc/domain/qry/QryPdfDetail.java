package cn.js189.uqc.domain.qry;

import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 查询无纸化信息，生成请求报文
 * 
 * @author Mid
 */
public class QryPdfDetail extends QryPdfByNbrBase{

	// 日志组件
	private static final Logger LOGGER = LoggerFactory.getLogger(QryPdfDetail.class);
	
	private String regionId;
	
	private String sysCode;
	
	private String busCode;
	
	private String routeCode;

	private String storeKey;

	private String authorization;
	
	public QryPdfDetail(String regionId, String sysCode, String busCode, String routeCode, String storeKey, String authorization) {
		super(regionId);
		this.regionId = regionId;
		this.sysCode = sysCode;
		this.busCode = busCode;
		this.routeCode = routeCode;
		this.storeKey = storeKey;
		this.authorization = authorization;
	}

	@Override
	protected JSONObject improveRequestObject(JSONObject requestObject) {
	    //2019-08-23 EOP迁移修改
//		requestObject.put("sysCode", sysCode);//测试
	    requestObject.put("sysCode", "");//正式
		requestObject.put("busCode", busCode);
		requestObject.put("storeKey", storeKey);
		requestObject.put("routeCode", routeCode);
		//2019-08-23 EOP迁移修改
//		requestObject.put("Authorization", authorization);//测试需要此字段，正式不需要
		LOGGER.debug("requestObject:{}", requestObject.toString());
		return requestObject;
	}

}
