package cn.js189.uqc.domain.qry;

import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 所有可订购销售品
 * 
 * @author ly
 */
public class QryAllCanBeOrderedOfferInst extends QryOptionalProdBase {

	// 日志组件
	private static final Logger LOGGER = LoggerFactory.getLogger(QryAllCanBeOrderedOfferInst.class);

	//业务号码
	private String prodId;
	
	//宽带账号
	private String prodFuncType;
	
	
	
	public QryAllCanBeOrderedOfferInst(String regionId, String uncompletedFlag, String isIndependent, String prodFuncType, String prodId) {
		super(regionId);
		this.prodFuncType = prodFuncType;
		this.prodId = prodId;
	}

	
	@Override
	protected JSONObject improveRequestObject(JSONObject requestObject) {
		requestObject.put("prodFuncType", prodFuncType);
		requestObject.put("prodId", prodId);
		LOGGER.debug("requestObject:{}", requestObject.toString());
		return requestObject;
	}

}
