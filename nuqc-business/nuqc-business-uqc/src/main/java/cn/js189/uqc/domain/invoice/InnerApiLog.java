package cn.js189.uqc.domain.invoice;

import java.util.Date;

public class InnerApiLog {

	private int id;

	private String channelId;

	private String areaCode;

	private String accNbr;

	private String accNbr2;

	// 业务类型 1综调故障生单
	private String serviceType;

	private String resultCode;

	private String paramIn;

	private String paramOut;

	private String execTime;

	private Date gmtCreate;

	public InnerApiLog() {
	}

	public InnerApiLog(String channelId, String areaCode, String accNbr, String accNbr2, String serviceType, String resultCode, String paramIn, String paramOut, String execTime) {
		this.channelId = channelId;
		this.areaCode = areaCode;
		this.accNbr = accNbr;
		this.accNbr2 = accNbr2;
		this.serviceType = serviceType;
		this.resultCode = resultCode;
		this.paramIn = paramIn;
		this.paramOut = paramOut;
		this.execTime = execTime;
	}

	public InnerApiLog(String areaCode, String channelId, String accNbr, String serviceType, String resultCode, String paramIn, String paramOut, String execTime) {
		this.areaCode = areaCode;
		this.channelId = channelId;
		this.accNbr = accNbr;
		this.serviceType = serviceType;
		this.resultCode = resultCode;
		this.paramIn = paramIn;
		this.paramOut = paramOut;
		this.execTime = execTime;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getAccNbr() {
		return accNbr;
	}

	public void setAccNbr(String accNbr) {
		this.accNbr = accNbr;
	}

	public String getAccNbr2() {
		return accNbr2;
	}

	public void setAccNbr2(String accNbr2) {
		this.accNbr2 = accNbr2;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public String getParamIn() {
		return paramIn;
	}

	public void setParamIn(String paramIn) {
		this.paramIn = paramIn;
	}

	public String getParamOut() {
		return paramOut;
	}

	public void setParamOut(String paramOut) {
		this.paramOut = paramOut;
	}

	public String getExecTime() {
		return execTime;
	}

	public void setExecTime(String execTime) {
		this.execTime = execTime;
	}

	public Date getGmtCreate() {
		return gmtCreate;
	}

	public void setGmtCreate(Date gmtCreate) {
		this.gmtCreate = gmtCreate;
	}
}
