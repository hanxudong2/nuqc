package cn.js189.uqc.domain.qry;

import cn.js189.uqc.domain.EOPRequestParamBase;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 智慧BSS，查询无纸化信息
 * 
 * @author Mid
 */
public abstract class QryPdfByNbrBase extends EOPRequestParamBase {
	// 日志组件
	private static final Logger LOGGER = LoggerFactory.getLogger(QryPdfByNbrBase.class);

	// svcCode 提供的API接口名称
//	public static final String EOP_TCP_CONT_SVC_CODE = "dfsgetData";
	public static final String EOP_TCP_CONT_SVC_CODE = "dfsGetData";

	// 地区标识
	protected String regionId;

	/**
	 * 查询无纸化信息构造器
	 * 
	 * @param regionId:地区标识
	 */
	public QryPdfByNbrBase(String regionId) {
		super();
		this.regionId = regionId;
	}

//	@Override
//	protected JSONObject generateTcpCont() {
//		return super.generateTcpCont("6001030001", EOP_TCP_CONT_SVC_CODE, null);
//	}
	
	 protected JSONObject generateTcpCont() {
	     return super.generateTcpCont("JS00000113", EOP_TCP_CONT_SVC_CODE, null);
	 }

	@Override
	protected JSONObject generateSvcCont() {
		JSONObject authenticationInfo = generateAuthenticationInfo();
		if (null == authenticationInfo) {
			LOGGER.error("generate authenticationInfo failure");
			return null;
		}

		JSONObject requestObject = generateRequestObject();
		if (null == requestObject) {
			LOGGER.error("generate requestObject failure");
			return null;
		}

//		JSONObject svcCont = new JSONObject();
//		svcCont.put("sysCode", requestObject.getString("sysCode"));
//		svcCont.put("busCode", requestObject.getString("busCode"));
//		svcCont.put("routeCode", requestObject.getString("routeCode"));
//		svcCont.put("storeKey", requestObject.getString("storeKey"));
		//2019-08-23 EOP迁移修改
//		svcCont.put("Authorization", requestObject.getString("Authorization"));
		return requestObject;
	}

	/**
	 * 生成 svcCont 下的认证信息
	 * 
	 * @return
	 */
	protected JSONObject generateAuthenticationInfo() {
		return super.generateSvcContAuthenticationInfo(regionId, null, null, null);
	}

	/**
	 * 生成 svcCont 下的请求对象
	 * 
	 * @return
	 */
	protected JSONObject generateRequestObject() {
		JSONObject requestObject = new JSONObject();
		requestObject.put("regionId", regionId);
		return improveRequestObject(requestObject);
	}

	/**
	 * 补齐 svcCont 下的请求对象
	 * 
	 * @param requestObject:svcCont下的请求对象
	 * @return
	 */
	protected abstract JSONObject improveRequestObject(JSONObject requestObject);

}
