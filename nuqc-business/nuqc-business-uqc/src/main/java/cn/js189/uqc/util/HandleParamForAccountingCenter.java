package cn.js189.uqc.util;

import cn.js189.common.constants.Constant;
import cn.js189.common.constants.ExpConstant;
import cn.js189.common.domain.*;
import cn.js189.common.util.MainUtils;
import cn.js189.common.util.Utils;
import cn.js189.common.util.helper.UUIDHelper;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 计费重新对接账务中心参数处理
 *
 */
public class HandleParamForAccountingCenter {

	private static final Logger LOGGER =  LoggerFactory.getLogger(HandleParamForAccountingCenter.class);
	
    /** 
    * 新业务清单回参解析
    */ 
    public static JSONObject builderResponseForNewSvrTic(String result, RespHead responseHead, RespInfo responseJson, String tranId) {
        JSONObject response;
        String responseTime = Utils.getCurrentTime();
        
        if (StringUtils.isNotBlank(result)) {
            JSONObject jsonObject = JSON.parseObject(result);
            JSONObject json = new JSONObject();
            if (jsonObject != null && !(jsonObject.isEmpty())) {
                String resultCode = jsonObject.getString(Constant.RESULT_CODE);
                String resultMsg = jsonObject.getString("resultMsg");
                if("0".equals(resultCode)) {//接口调用成功，拼接出参
                    json.put("changeCntch", MainUtils.getNullEmpty(jsonObject.getString("CHARGE_CNT_CH")));//费用合计
                    json.put("totalTicketChargeCh", MainUtils.getNullEmpty(jsonObject.getString("TOTAL_TICKET_CHARGE_CH")));//通信费用总计
                    json.put("totalDisctChargeCh", MainUtils.getNullEmpty(jsonObject.getString("TOTAL_DISCT_CHARGE_CH")));//优惠费用总计
                    json.put("duartionCntCh", MainUtils.getNullEmpty(jsonObject.getString("DURATION_CNT_CH")));//使用时长合计
                    JSONArray blances = new JSONArray();
                    JSONArray array ;
                    String str = jsonObject.getString("data");
                    if(str.startsWith("[")) {
                        array = jsonObject.getJSONArray("data");
                        json.put("count", String.valueOf(array.size()));
                        for(int i = 0; i< array.size(); i++) {
                            JSONObject obj = new JSONObject();
                            JSONObject jsonBlance = array.getJSONObject(i);
                            obj.put("ticketNumber",MainUtils.getNullEmpty(jsonBlance.getString("TICKET_NUMBER")));// 序号
                            obj.put("ticketId",MainUtils.getNullEmpty(jsonBlance.getString("TICKET_ID")));// 话单标识
                            obj.put("areaCode",MainUtils.getNullEmpty(jsonBlance.getString("AREA_CODE")));// 区域编码
                            obj.put("nbr",MainUtils.getNullEmpty(jsonBlance.getString("ACC_NBR")));//接入号码
                            obj.put("calledNbr", MainUtils.getNullEmpty(jsonBlance.getString("CALLED_NBR")));//被叫号码
                            obj.put("calledAeraCode", MainUtils.getNullEmpty(jsonBlance.getString("CALLED_AREA_CODE")));//被叫地区
                            obj.put("ticketType", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_TYPE"))); //业务类型
                            obj.put("startDateNew", MainUtils.getNullEmpty(jsonBlance.getString("START_DATE_NEW")));//上线日期
                            obj.put("startTimeNew",MainUtils.getNullEmpty(jsonBlance.getString("START_TIME_NEW")));//上线时间
                            obj.put("duartionCh",MainUtils.getNullEmpty(jsonBlance.getString("DURATION_CH")));//使用时长
                            obj.put("acctItemChargeCh",MainUtils.getNullEmpty(jsonBlance.getString("ACCT_ITEM_CHARGE_CH")));//话费
                            obj.put("ticketChargeCh",MainUtils.getNullEmpty(jsonBlance.getString("TICKET_CHARGE_CH")));//
                            obj.put("disctChargeCh",MainUtils.getNullEmpty(jsonBlance.getString("DISCT_CHARGE_CH")));
                            obj.put("productName",MainUtils.getNullEmpty(jsonBlance.getString("PRODUCT_NAME")));
                            obj.put("agentCode", MainUtils.getNullEmpty(jsonBlance.getString("AGENT_CODE")));
                            obj.put("ticketTypeNew",MainUtils.getNullEmpty(jsonBlance.getString("TICKET_TYPE_NEW")));//流量
                            obj.put("durationType",MainUtils.getNullEmpty(jsonBlance.getString("DURATION＿TYPE")));//流量
                            blances.add(obj);
                        }
                    } else {
                        JSONObject jsonBlance = jsonObject.getJSONObject("data");
                        JSONObject obj=new JSONObject();
                        json.put("count", "1");
                        obj.put("ticketNumber",MainUtils.getNullEmpty(jsonBlance.getString("TICKET_NUMBER")));// 序号
                        obj.put("ticketId",MainUtils.getNullEmpty(jsonBlance.getString("TICKET_ID")));// 话单标识
                        obj.put("areaCode",MainUtils.getNullEmpty(jsonBlance.getString("AREA_CODE")));// 区域编码
                        obj.put("nbr",MainUtils.getNullEmpty(jsonBlance.getString("ACC_NBR")));//接入号码
                        obj.put("calledNbr", MainUtils.getNullEmpty(jsonBlance.getString("CALLED_NBR")));//被叫号码
                        obj.put("calledAeraCode", MainUtils.getNullEmpty(jsonBlance.getString("CALLED_AREA_CODE")));//被叫地区
                        obj.put("ticketType", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_TYPE"))); //业务类型
                        obj.put("startDateNew", MainUtils.getNullEmpty(jsonBlance.getString("START_DATE_NEW")));//上线日期
                        obj.put("startTimeNew",MainUtils.getNullEmpty(jsonBlance.getString("START_TIME_NEW")));//上线时间
                        obj.put("duartionCh",MainUtils.getNullEmpty(jsonBlance.getString("DURATION_CH")));//使用时长
                        obj.put("acctItemChargeCh",MainUtils.getNullEmpty(jsonBlance.getString("ACCT_ITEM_CHARGE_CH")));//话费
                        obj.put("ticketChargeCh",MainUtils.getNullEmpty(jsonBlance.getString("TICKET_CHARGE_CH")));//
                        obj.put("disctChargeCh",MainUtils.getNullEmpty(jsonBlance.getString("DISCT_CHARGE_CH")));
                        obj.put("productName",MainUtils.getNullEmpty(jsonBlance.getString("PRODUCT_NAME")));
                        obj.put("agentCode", MainUtils.getNullEmpty(jsonBlance.getString("AGENT_CODE")));
                        obj.put("ticketTypeNew",MainUtils.getNullEmpty(jsonBlance.getString("TICKET_TYPE_NEW")));//流量
                        obj.put("durationType",MainUtils.getNullEmpty(jsonBlance.getString("DURATION＿TYPE")));//流量
                        blances.add(obj);
                    }
                    json.put("items", blances);
                    json.put("iresult", resultCode);
                    json.put("smsg", resultMsg);
                    responseHead.setMsg(Utils.SUCCESS);
                    responseHead.setStatus("00");
                    responseHead.setResponseId(Utils.RSP + tranId);
                    responseHead.setResponseTime(responseTime);
                    responseJson.setBody(json);
                } else {
                    responseHead.setMsg(resultMsg);
                    responseHead.setStatus("01");
                    responseHead.setResponseId(Utils.RSP + tranId);
                    responseHead.setResponseTime(responseTime);
                    responseJson.setBody(jsonObject);
                }
            } else {
                responseHead.setMsg("计费接口出参null");
                responseHead.setStatus("99");
                responseHead.setResponseId(Utils.RSP + tranId);
                responseHead.setResponseTime(responseTime);
            }
        } else {
            responseHead.setMsg("计费接口出参null");
            responseHead.setStatus("99");
            responseHead.setResponseId(Utils.RSP + tranId);
            responseHead.setResponseTime(responseTime);
        }
        response = (JSONObject) JSON.toJSON(responseJson);
        return response;
    }
    
    
    
    /** 
    * 短信通信清单出参
    */ 
    public static JSONObject builderResponseForSmTicketQr(String result, RespHead responseHead, RespInfo responseJson, String tranId){
        JSONObject response;
        String responseTime = Utils.getCurrentTime();
        
        if (StringUtils.isNotBlank(result)) {
            JSONObject jsonObject = JSON.parseObject(result);
            JSONObject json = new JSONObject();
            if (jsonObject != null && !(jsonObject.isEmpty())) {
                String resultCode = jsonObject.getString("resultCode");
                String resultMsg = jsonObject.getString("resultMsg");
                if("0".equals(resultCode)) {//接口调用成功，拼接出参
                	//changeCntch可能没有
                	if(jsonObject.containsKey("CHARGE_CNT_CH") && null != jsonObject.getString("CHARGE_CNT_CH")){
                        json.put("changeCntch", MainUtils.getNullEmpty(jsonObject.getString("CHARGE_CNT_CH")));//费用合计
                	}
                	
                    JSONArray blances = new JSONArray();
                    JSONArray array ;
                    String str = jsonObject.getString("data");
                    if(str.startsWith("[")) {
                        array = jsonObject.getJSONArray("data");
                        for(int i = 0; i< array.size(); i++) {
                            JSONObject obj = new JSONObject();
                            JSONObject jsonBlance = array.getJSONObject(i);
                            obj.put("accNbr",MainUtils.getNullEmpty(jsonBlance.getString("ACC_NBR")));
                            obj.put("productId",MainUtils.getNullEmpty(jsonBlance.getString("PRODUCT_ID")));
                            obj.put("productName",MainUtils.getNullEmpty(jsonBlance.getString("PRODUCT_NAME")));
                            obj.put("startDateNew",MainUtils.getNullEmpty(jsonBlance.getString("START_DATE_NEW")));
                            obj.put("startTime", MainUtils.getNullEmpty(jsonBlance.getString("START_TIME")));
                            obj.put("startTimeNew", MainUtils.getNullEmpty(jsonBlance.getString("START_TIME_NEW")));
                            obj.put("ticketCharge", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_CHARGE"))); 
                            obj.put("ticketChargeCh", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_CHARGE_CH")));
                            obj.put("ticketNumber",MainUtils.getNullEmpty(jsonBlance.getString("TICKET_NUMBER")));
                            obj.put("ticketType",MainUtils.getNullEmpty(jsonBlance.getString("TICKET_TYPE")));
                            obj.put("ticketTypeId",MainUtils.getNullEmpty(jsonBlance.getString("TICKET_TYPE_ID")));
                            obj.put("flowAmountNew",MainUtils.getNullEmpty(jsonBlance.getString("FLOW_AMOUNT_NEW")));
                            blances.add(obj);
                        }
                    } else {
                        JSONObject jsonBlance = jsonObject.getJSONObject("data");
                        JSONObject obj=new JSONObject();
                        obj.put("accNbr",MainUtils.getNullEmpty(jsonBlance.getString("ACC_NBR")));
                        obj.put("productId",MainUtils.getNullEmpty(jsonBlance.getString("PRODUCT_ID")));
                        obj.put("productName",MainUtils.getNullEmpty(jsonBlance.getString("PRODUCT_NAME")));
                        obj.put("startDateNew",MainUtils.getNullEmpty(jsonBlance.getString("START_DATE_NEW")));
                        obj.put("startTime", MainUtils.getNullEmpty(jsonBlance.getString("START_TIME")));
                        obj.put("startTimeNew", MainUtils.getNullEmpty(jsonBlance.getString("START_TIME_NEW")));
                        obj.put("ticketCharge", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_CHARGE"))); 
                        obj.put("ticketChargeCh", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_CHARGE_CH")));
                        obj.put("ticketNumber",MainUtils.getNullEmpty(jsonBlance.getString("TICKET_NUMBER")));
                        obj.put("ticketType",MainUtils.getNullEmpty(jsonBlance.getString("TICKET_TYPE")));
                        obj.put("ticketTypeId",MainUtils.getNullEmpty(jsonBlance.getString("TICKET_TYPE_ID")));
                        obj.put("flowAmountNew",MainUtils.getNullEmpty(jsonBlance.getString("FLOW_AMOUNT_NEW")));
                        obj.put("durationType",MainUtils.getNullEmpty(jsonBlance.getString("DURATION＿TYPE")));//流量
                        blances.add(obj);
                    }

                    json.put("smTicketQrlist", blances);
                    responseHead.setMsg(Utils.SUCCESS);
                    responseHead.setStatus("00");
                    responseHead.setResponseId(Utils.RSP + tranId);
                    responseHead.setResponseTime(responseTime);
                    responseJson.setBody(json);
                } else {
                    responseHead.setMsg(resultMsg);
                    responseHead.setStatus("01");
                    responseHead.setResponseId(Utils.RSP + tranId);
                    responseHead.setResponseTime(responseTime);
                    responseJson.setBody(jsonObject);
                }
            } else {
                responseHead.setMsg("计费接口出参null");
                responseHead.setStatus("99");
                responseHead.setResponseId(Utils.RSP + tranId);
                responseHead.setResponseTime(responseTime);
            }
        } else {
            responseHead.setMsg("调用企信失败");
            responseHead.setStatus("02");
            responseHead.setResponseId(Utils.RSP + tranId);
            responseHead.setResponseTime(responseTime);
        }
        response = (JSONObject) JSON.toJSON(responseJson);
        return response;
 	
    }
    /** 
    * C网短信_彩信增值详单_移动
    */ 
    public static JSONObject builderResponseForSmsValueTicketQr(String result, RespHead responseHead, RespInfo responseJson, String tranId){
        JSONObject response;
        String responseTime = Utils.getCurrentTime();
        if (StringUtils.isNotBlank(result)) {
            JSONObject jsonObject = JSON.parseObject(result);
            JSONObject json = new JSONObject();
            if (jsonObject != null && !(jsonObject.isEmpty())) {
                String resultCode = jsonObject.getString("resultCode");
                String resultMsg = jsonObject.getString("resultMsg");
                if("0".equals(resultCode)) {//接口调用成功，拼接出参
                	//changeCntch可能没有
                	if(jsonObject.containsKey("TOTAL_TICKET_CHARGE_CH")){
                        json.put("totalTicketChargeCh", MainUtils.getNullEmpty(jsonObject.getString("TOTAL_TICKET_CHARGE_CH")));//费用合计
                	}

                    JSONArray blances = new JSONArray();
                    JSONArray array ;
                    String str = jsonObject.getString("data");
                    if(str.startsWith("[")) {
                        array = jsonObject.getJSONArray("data");
                        json.put("count", String.valueOf(array.size()));
                        for(int i = 0; i< array.size(); i++) {
                            JSONObject obj = new JSONObject();
                            JSONObject jsonBlance = array.getJSONObject(i);
                            obj.put("ticketNumber", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_NUMBER")));// 序号
                            obj.put("ticketType", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_TYPE")));// 使用方式
                            obj.put("nbr", MainUtils.getNullEmpty(jsonBlance.getString("ACC_NBR")));// 对方号码
                            obj.put("startDateNew", MainUtils.getNullEmpty(jsonBlance.getString("START_DATE_NEW")));// 发送日期
                            obj.put("startTimeNew", MainUtils.getNullEmpty(jsonBlance.getString("START_TIME_NEW")));// 发送时间
                            obj.put("agentCode", MainUtils.getNullEmpty(jsonBlance.getString("AGENT_CODE")));// 发送时间
                            obj.put("ticketChargeCh", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_CHARGE_CH")));// 话费
                            obj.put("spName", MainUtils.getNullEmpty(jsonBlance.getString("SP_NAME")));// 流量
                            obj.put("connectName", MainUtils.getNullEmpty(jsonBlance.getString("CONNECT_NAME")));
                            blances.add(obj);
                        }
                    } else {
                        JSONObject jsonBlance = jsonObject.getJSONObject("data");
                        JSONObject obj=new JSONObject();
                        json.put("count", "1");
                        obj.put("ticketNumber", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_NUMBER")));// 序号
                        obj.put("ticketType", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_TYPE")));// 使用方式
                        obj.put("nbr", MainUtils.getNullEmpty(jsonBlance.getString("ACC_NBR")));// 对方号码
                        obj.put("startDateNew", MainUtils.getNullEmpty(jsonBlance.getString("START_DATE_NEW")));// 发送日期
                        obj.put("startTimeNew", MainUtils.getNullEmpty(jsonBlance.getString("START_TIME_NEW")));// 发送时间
                        obj.put("agentCode", MainUtils.getNullEmpty(jsonBlance.getString("AGENT_CODE")));// 发送时间
                        obj.put("ticketChargeCh", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_CHARGE_CH")));// 话费
                        obj.put("spName", MainUtils.getNullEmpty(jsonBlance.getString("SP_NAME")));// 流量
                        obj.put("connectName", MainUtils.getNullEmpty(jsonBlance.getString("CONNECT_NAME")));
                        blances.add(obj);
                    }
                    json.put("items", blances);
                    json.put("iresult", resultCode);
                    json.put("smsg", resultMsg);
                    responseHead.setMsg(Utils.SUCCESS);
                    responseHead.setStatus("00");
                    responseHead.setResponseId(Utils.RSP + tranId);
                    responseHead.setResponseTime(responseTime);
                    responseJson.setBody(json);
                } else {
                    responseHead.setMsg(resultMsg);
                    responseHead.setStatus("01");
                    responseHead.setResponseId(Utils.RSP + tranId);
                    responseHead.setResponseTime(responseTime);
                    responseJson.setBody(jsonObject);
                }
            } else {
                responseHead.setMsg("计费接口出参null");
                responseHead.setStatus("99");
                responseHead.setResponseId(Utils.RSP + tranId);
                responseHead.setResponseTime(responseTime);
            }
        } else {
            responseHead.setMsg("调用企信失败");
            responseHead.setStatus("02");
            responseHead.setResponseId(Utils.RSP + tranId);
            responseHead.setResponseTime(responseTime);
        }
        response = (JSONObject) JSON.toJSON(responseJson);
        return response;
 	
    }
    
    /** 
    * 本金、赠款销账金额查询CUSTOMER_QRY(cust_use_qry) 出参解析
    */ 
    public static String builderResponseForCustomerQry(String rst){
    	JSONObject requestParam = JSON.parseObject(rst);
        JSONObject response = new JSONObject();
        JSONObject head = new JSONObject();
        JSONObject body = new JSONObject();
        String responseTime = Utils.getCurrentTime();
        if(null == rst){
            return response.toJSONString();
        }
        
        //新出参以IRESULT，SMSG作为接口返回结果
          String status = requestParam.getString("errorCode"); // 返回结果码
          String msg = requestParam.getString("errorMsg"); // 返回结果描述
        if ("0".equals(status)) {
            JSONObject  customerQryInfo = requestParam.getJSONObject("customerQryInfo");      	
        	body.put("BALANCE",customerQryInfo.getString("lCFYBalance") );
        	body.put("charge", customerQryInfo.getString("lCFNBalance"));
        	body.put("IRESULT",status);
        	body.put("SMSG",msg );
        	head.put("msg", "success");
            head.put("responseTime", responseTime);
            head.put("status", "00");
        } else {
        	body.put("IRESULT",status);
        	body.put("SMSG",msg );
        	head.put("msg", msg);
            head.put("responseTime", responseTime);
            head.put("status", "01");
        }
        //将旧入参中错误码和错误信息字段换成旧出参对应字段名
        response.put("body", body);
        response.put("head", head);
        return response.toJSONString();
    }

    /** 
    * 账户信息查询
    */ 
    public static JSONObject builderResponseForOthGetAcct(String result, RespHead responseHead, RespInfo responseJson, String tranId){
        JSONObject response;
        String responseTime = Utils.getCurrentTime();
        if (StringUtils.isNotBlank(result)) {
            JSONObject jsonObject = JSON.parseObject(result);
            JSONObject json = new JSONObject();
            if (jsonObject != null && !(jsonObject.isEmpty())) {
                String resultCode = jsonObject.getString("errorCode");
                String resultMsg = jsonObject.getString("errorMsg");
                if("0".equals(resultCode)) {//接口调用成功，拼接出参
                	JSONObject othAcctDet =jsonObject.getJSONObject("othAcctDet"); 
                	
                	json.put("accNbr", MainUtils.getNullEmpty(othAcctDet.getString("sAccNbr")));
                	json.put("acctAddress", MainUtils.getNullEmpty(othAcctDet.getString("sAddress")));
                	json.put("acctId", MainUtils.getNullEmpty(othAcctDet.getString("lAcctId")));
                	json.put("acctName", MainUtils.getNullEmpty(othAcctDet.getString("sAcctName")));
                	json.put("acctNbr97", MainUtils.getNullEmpty(othAcctDet.getString("sAccNbr97")));
                	json.put("balance", MainUtils.getNullEmpty(othAcctDet.getString("lBalance")));
                	json.put("bankAcct", MainUtils.getNullEmpty(othAcctDet.getString("sBankAcctId")));
                	json.put("iresult", resultCode);
                	json.put("paymentMethod", MainUtils.getNullEmpty(othAcctDet.getString("lPaymentMethod")));
                	json.put("servId", MainUtils.getNullEmpty(othAcctDet.getString("lServId")));
                	json.put("state", MainUtils.getNullEmpty(othAcctDet.getString("sState")));
                    responseHead.setMsg(Utils.SUCCESS);
                    responseHead.setStatus("00");
                    responseHead.setResponseId(Utils.RSP + tranId);
                    responseHead.setResponseTime(responseTime);
                    responseJson.setBody(json);
                } else {
                    responseHead.setMsg(resultMsg);
                    responseHead.setStatus("01");
                    responseHead.setResponseId(Utils.RSP + tranId);
                    responseHead.setResponseTime(responseTime);
                    responseJson.setBody(jsonObject);
                }
            } else {
                responseHead.setMsg("计费接口出参null");
                responseHead.setStatus("99");
                responseHead.setResponseId(Utils.RSP + tranId);
                responseHead.setResponseTime(responseTime);
            }
        } else {
            responseHead.setMsg("调用企信失败");
            responseHead.setStatus("02");
            responseHead.setResponseId(Utils.RSP + tranId);
            responseHead.setResponseTime(responseTime);
        }
        response = (JSONObject) JSON.toJSON(responseJson);
        return response;
 	
    
    }
    
    
    /** 
    * 宽带清单查询 TKT_GU_BROAD_DATA_QR
    */ 
    public static String builderResponseForTktGuBroadDataQr(String param,RespHead responseHead, RespInfo responseJson, String tranId){
    	JSONObject requestParam = JSON.parseObject(param);
        String responseTime = Utils.getCurrentTime();
        if("0".equals(requestParam.getString("resultCode"))){
            JSONObject rsp = new JSONObject();
            
            //原有出参参数字段
            String accNbr = null;
            String acctItemChargeCh =null;
            String acctName =null;
            String areaCode = null;
            String bytesCnt =null;
            String cycleEndDate =null;
            String duration = null;
            String durationCh = null;
            String endDateNew = null;
            String endTime = null;
            String endTimeNew = null;
            String inBytesCnt = null;
            String outBytesCnt = null;
            String paymentCharge = null;
            String productId = null;
            String productName = null;
            String startDateNew = null;
            String startTime = null;
            String startTimeNew = null;
            String ticketId = null;
            String ticketNumber = null;
            String ticketType = null;
            String ticketTypeId = null;
            
            JSONArray data = requestParam.getJSONArray("data");
            JSONObject a;
            JSONObject b = new JSONObject();
            JSONArray c = new JSONArray();
            JSONObject d = new JSONObject();
	        
	        for (Object datum : data) {
		        a = (JSONObject) datum;
		        accNbr = a.getString("ACC_NBR");
		        acctItemChargeCh = a.getString("ACCT_ITEM_CHARGE_CH");
		        acctName = a.getString("ACCT_NAME");
		        areaCode = a.getString("AREA_CODE");
		        bytesCnt = a.getString("BYTES_CNT");
		        cycleEndDate = a.getString("CYCLE_END_DATE");
		        duration = a.getString("DURATION");
		        durationCh = a.getString("DURATION_CH");
		        endDateNew = a.getString("END_DATE_NEW");
		        endTime = a.getString("END_TIME");
		        endTimeNew = a.getString("END_TIME_NEW");
		        inBytesCnt = a.getString("IN_BYTES_CNT");
		        outBytesCnt = a.getString("OUT_BYTES_CNT");
		        paymentCharge = a.getString("PAYMENT_CHARGE");
		        productId = a.getString("PRODUCT_ID");
		        productName = a.getString("PRODUCT_NAME");
		        startDateNew = a.getString("START_DATE_NEW");
		        startTime = a.getString("START_TIME");
		        startTimeNew = a.getString("START_TIME_NEW");
		        ticketId = a.getString("TICKET_ID");
		        ticketNumber = a.getString("TICKET_NUMBER");
		        ticketType = a.getString("TICKET_TYPE");
		        ticketTypeId = a.getString("TICKET_TYPE_ID");
		        
		        //组装新出参
		        b.put("accNbr", accNbr);
		        b.put("acctItemChargeCh", acctItemChargeCh);
		        b.put("acctName", acctName);
		        b.put("areaCode", areaCode);
		        b.put("bytesCnt", bytesCnt);
		        b.put("cycleEndDate", cycleEndDate);
		        b.put("duration", duration);
		        b.put("durationCh", durationCh);
		        b.put("endDateNew", endDateNew);
		        b.put("endTime", endTime);
		        b.put("endTimeNew", endTimeNew);
		        b.put("inBytesCnt", inBytesCnt);
		        b.put("outBytesCnt", outBytesCnt);
		        b.put("paymentCharge", paymentCharge);
		        b.put("productId", productId);
		        b.put("productName", productName);
		        b.put("startDateNew", startDateNew);
		        b.put("startTime", startTime);
		        b.put("startTimeNew", startTimeNew);
		        b.put("ticketId", ticketId);
		        b.put("ticketNumber", ticketNumber);
		        b.put("ticketType", ticketType);
		        b.put("ticketTypeId", ticketTypeId);
		        
		        //避免循环引用检测，直接循环add输出结果是重复某一项
		        c.add(JSON.parseObject(b.toJSONString()));
	        }
            d.put("broadDataGulist",c);
            JSONObject broadData1 = new JSONObject();
            broadData1.put("durationCnt", requestParam.getString("DURATION_CNT"));
            broadData1.put("totalBytesCnt", requestParam.getString("TOTAL_BYTES_CNT"));
            d.put("broadData1", broadData1);
            d.put("chargeCntCh", requestParam.getString("CHARGE_CNT_CH"));
            d.put("durationCntCh", requestParam.getString("DURATION_CNT_CH"));
            d.put("iresult", requestParam.getString("resultCode"));
            d.put("smsg",requestParam.getString("resultMsg") );
            
            
            responseHead.setMsg("success");
            responseHead.setStatus("00");
            responseHead.setResponseTime(responseTime);
            rsp.put("body", d);
            rsp.put("head", responseHead);
            return rsp.toJSONString();
         
        }else{
        	LOGGER.debug("responseJson:{} , tranId {}",responseJson,tranId);
        	JSONObject rsp = new JSONObject();
        	//失败情况需要与原接口一致
            responseHead.setMsg(requestParam.getString("resultMsg"));
            responseHead.setStatus(requestParam.getString("resultCode"));
            responseHead.setResponseTime(responseTime);
            rsp.put("head", responseHead);
            return rsp.toJSONString();        	
        }
    }
    
    /** 
    * 语音详单查询
    */ 
    public static String builderResponseForTktYiVoiceTicketQr(String param){
        LOGGER.info("callNewVoiceTicket 语音详单查询 builderResponseForTktYiVoiceTicketQr入参:{}", param);
		JSONObject requestParam = JSON.parseObject(param);
		if("00".equals(requestParam.getJSONObject("head").getString("status"))){
		JSONObject rsp = new JSONObject();
		
		//原有VoiceTicketQr出参参数字段
		
		String accNbr = null;
		String areaCode = null;
		String duration = null;
		String durationCh = null;
		String durationSecond = null;
		String durationType = null;
		String productId = null;
		String productName = null;
		String startDateNew = null;
		String startTime = null;
		String startTimeNew = null;
		String ticketChargeCh = null;
		String ticketNumber = null;
		String ticketType = null;
		String ticketTypeId = null;
		String ticketTypeNew = null;
		
		//查询中心新增返回字段
		String flowAmountNew = null;
		String volteName = null;
		String ticketCharge = null;
		
		JSONArray data = requestParam.getJSONObject("body").getJSONArray("data");
		JSONObject a = null;
		JSONObject b = new JSONObject();
		JSONArray c = new JSONArray();
		JSONObject d = new JSONObject();
			
			for (Object datum : data) {
				a = (JSONObject) datum;
				accNbr = a.getString("ACC_NBR");
				areaCode = a.getString("AREA_CODE");
				duration = a.getString("DURATION");
				durationCh = a.getString("DURATION_CH");
				durationSecond = a.getString("DURATION_SECOND");
				durationType = a.getString("DURATION_TYPE");
				flowAmountNew = a.getString("FLOW_AMOUNT_NEW");
				productId = a.getString("PRODUCT_ID");
				productName = a.getString("PRODUCT_NAME");
				startDateNew = a.getString("START_DATE_NEW");
				startTime = a.getString("START_TIME");
				startTimeNew = a.getString("START_TIME_NEW");
				ticketCharge = a.getString("TICKET_CHARGE");
				ticketChargeCh = a.getString("TICKET_CHARGE_CH");
				ticketNumber = a.getString("TICKET_NUMBER");
				ticketType = a.getString("TICKET_TYPE");
				ticketTypeId = a.getString("TICKET_TYPE_ID");
				ticketTypeNew = a.getString("TICKET_TYPE_NEW");
				volteName = a.getString("VOLTE_NAME");
				
				//组装新出参
				b.put("accNbr", accNbr);
				b.put("areaCode", areaCode);
				if (areaCode != null) {
					String areaName = Utils.getAreaName(areaCode);
					if (areaName != null && (!"null".equals(areaName))) {
						b.put("areaName", areaName);
					} else {
						b.put("areaName", areaCode);
					}
				} else {
					b.put("areaName", "");
				}
				b.put("duration", duration);
				b.put("durationCh", durationCh);
				b.put("durationSecond", durationSecond);
				b.put("durationType", durationType);
				b.put("flowAmountNew", flowAmountNew);
				b.put("productId", productId);
				b.put("productName", productName);
				b.put("startDateNew", startDateNew);
				b.put("startTime", startTime);
				b.put("startTimeNew", startTimeNew);
				b.put("ticketCharge", ticketCharge);
				b.put("ticketChargeCh", ticketChargeCh);
				b.put("ticketNumber", ticketNumber);
				b.put("ticketType", ticketType);
				b.put("ticketTypeId", ticketTypeId);
				b.put("ticketTypeNew", ticketTypeNew);
				b.put("volteName", volteName);
				//避免循环引用检测，直接循环add输出结果是重复某一项
				c.add(JSON.parseObject(b.toJSONString()));
			}
        JSONObject body = requestParam.getJSONObject("body");
        d.put("items",c);
		d.put("changeCntch", body.getString("CHARGE_CNT_CH"));
		d.put("duartionCntCh", body.getString("DURATION_CNT_CH"));
		d.put("durationCnt", body.getString("DURATION_CNT"));
		d.put("durationSecondCnt", body.getString("DURATION_SECOND_CNT"));
		d.put("transactionId", UUIDHelper.currentTimeStampString());
		d.put("iresult", body.getString("resultCode"));
		d.put("smsg",body.getString("resultMsg") );
        rsp.put("body", d);
        rsp.put("head", requestParam.getJSONObject("head"));
        return rsp.toJSONString();
		}else{
			JSONObject body = requestParam.getJSONObject("body");
			body.put("IRESULT", requestParam.getJSONObject("body").getString("resultCode"));
			body.put("SMSG", requestParam.getJSONObject("body").getString("resultMsg"));
			requestParam.put("body", body);
		    return requestParam.toJSONString();
		}
	}
    
    
    /** 
    * 账单查询接口出参解析
    */ 
    public static JSONObject builderResponseForQryCustBill(String param){
    	JSONObject respObj = JSON.parseObject(param);
        JSONObject response = new JSONObject();
        JSONObject body  = new JSONObject();
        
        if(null == respObj){
            response.put("errorCode", ExpConstant.ERROR_OUTPUT_NULL);
            response.put("errorMsg", "unknown error，response is empty,plc check.");
            response.put("success", "OK");
            response.put("bodys", respObj);
            LOGGER.debug("一致性回参:空");
            return response;
        }
        
        String resultCode = respObj.getString("resultCode"); // 返回结果码
        String resultMsg = respObj.getString("resultMsg"); // 返回结果描述
    	
        response.put("errorCode", resultCode);
        response.put("errorMsg", resultMsg);
        response.put("success", "OK");
        body.put("dccResultCode", resultCode);
        body.put("dccResultMsg", resultMsg);
    	
        if("0".equals(resultCode)){	   
            //信息组个数测试环境暂时没有
            String dccControl101 = "";
            
            JSONObject dccControl101list = new JSONObject();
            String dccAcctName = respObj.getString("acctName");
            String dccBalOweUsed = respObj.getString("chargePayed");
            String dccCharge = respObj.getString("sumCharge");
            //套餐信息组个数测试环境暂时没有  暂时代码计算
            String dccControl102 = Integer.toString(respObj.getJSONArray("itemInformationList").size());
            String dccOweCharge = respObj.getString("chargeShouldPayed");

            JSONArray dccControl102list = new JSONArray();  
            //dccControl102list节点组装
            JSONArray itemInformationList = respObj.getJSONArray("itemInformationList");
            for (int i = 0; i < itemInformationList.size(); i++) {
    			JSONObject dccControlObj = new JSONObject();
    			JSONObject itemInformation = itemInformationList.getJSONObject(i);

    			String accNbrDetail = itemInformation.getString("accNbrDetail");
    			String charge = itemInformation.getString("charge");
    			String chargeTypeName = itemInformation.getString("chargeTypeName");
    			String classId = itemInformation.getString("classId");
    			String parentClassId = itemInformation.getString("parentClassId");
    			String productOffName = itemInformation.getString("productOffName");
    			//测试环境暂时没有
    			String showLevel = itemInformation.getString("showLevel");
    			
    			
    			dccControlObj.put("accNbr", accNbrDetail);
    			dccControlObj.put("dccBillFee", charge);
    			dccControlObj.put("dccBillItemName", chargeTypeName);
    			dccControlObj.put("dccClassId", classId);
    			dccControlObj.put("dccParentClassId", parentClassId);
    			dccControlObj.put("dccShowLevel", showLevel);
    			dccControlObj.put("productOffName", productOffName);			
    			dccControl102list.add(dccControlObj);	
    		}
            
            
            //dccControl101list节点组装
            dccControl101list.put("dccAcctName", dccAcctName);
            dccControl101list.put("dccBalOweUsed", dccBalOweUsed);
            dccControl101list.put("dccCharge", dccCharge);
            dccControl101list.put("dccControl102", dccControl102);
            dccControl101list.put("dccControl102list", dccControl102list);
            dccControl101list.put("dccOweCharge", dccOweCharge);
            
            //body节点组装
            body.put("dccControl101", dccControl101);
            body.put("dccControl101list", dccControl101list); 
        }

        response.put("bodys", body);
        return response;
    }
    
    
    /** 
    * 缴费充值记录查询API
    */ 
    public static JSONObject builderResponseForQryPayment(String param){
    	JSONObject respObj = JSON.parseObject(param);
        JSONObject response = new JSONObject();
        JSONObject body  = new JSONObject();
        
        if(null == respObj){
            response.put("errorCode", ExpConstant.ERROR_OUTPUT_NULL);
            response.put("errorMsg", "unknown error，response is empty,plc check.");
            response.put("success", "OK");
            response.put("bodys", respObj);
            LOGGER.debug("一致性回参:空");
            return response;
        }
        
        String resultCode = respObj.getString("resultCode"); // 返回结果码
        String resultMsg = respObj.getString("resultMsg"); // 返回结果描述
    	
        response.put("errorCode", resultCode);
        response.put("errorMsg", resultMsg);
        response.put("success", "OK");
        body.put("dccResultCode", resultCode);
        body.put("dccResultMsg", resultMsg);
        if("0".equals(resultCode)){
            String dccControl101;
            JSONArray dccControl101list = new JSONArray(); 
            JSONObject dccControl101listObj = new JSONObject();

            if(respObj.containsKey("paymentInfoList")&&(!respObj.getJSONArray("paymentInfoList").isEmpty())){

            //套餐信息组个数测试环境暂时没有  暂时代码计算
            dccControl101 = Integer.toString(respObj.getJSONArray("paymentInfoList").size());



            //dccControl101list节点组装
            JSONArray paymentInfo = respObj.getJSONArray("paymentInfoList");
            for (int i = 0; i < paymentInfo.size(); i++) {
    			JSONObject dccControlObj = new JSONObject();
    			JSONObject paymentInfoObj = paymentInfo.getJSONObject(i);

    			String dccPaymentNbr = paymentInfoObj.getString("accNbrDetail");
    			String dccChargeSourceId = paymentInfoObj.getString("paymentChannel");
    			String dccPaymentMethod = paymentInfoObj.getString("paymentMethod");
    			String dccPaymentAmount = paymentInfoObj.getString("amount");
    			String tBalanceTypeId = paymentInfoObj.getString("balanceTypeId");//余额类型标识
    			String dccPaidTime = paymentInfoObj.getString("paymentDate");
    			String dccPaySerialNbr = paymentInfoObj.getString("paymentId");
    			
    			
    			dccControlObj.put("dccPaymentNbr", dccPaymentNbr);
    			dccControlObj.put("dccChargeSourceId", dccChargeSourceId);
    			dccControlObj.put("dccPaymentMethod", dccPaymentMethod);
    			dccControlObj.put("dccPaymentAmount", dccPaymentAmount);
    			dccControlObj.put("tBalanceTypeId", tBalanceTypeId);
    			dccControlObj.put("dccPaidTime", dccPaidTime);
    			dccControlObj.put("dccPaySerialNbr", dccPaySerialNbr);
    			dccControlObj.put("paymentChannelName", paymentInfoObj.getString("paymentChannelName")==null?"":paymentInfoObj.getString("paymentChannelName"));//added1 by yangyang
    			//单个返回obj 多个返回 list
    			if("1".equals(dccControl101)){
    				dccControl101listObj =dccControlObj;
    			}
    			dccControl101list.add(dccControlObj);	
    		}
            }else{
                dccControl101 ="0";
            }
            
            //body节点组装
            body.put("dccControl101", dccControl101);
            if("1".equals(dccControl101)){
            	body.put("dccControl101list", dccControl101listObj);
            }else{
            	body.put("dccControl101list", dccControl101list);
            }
        }
        response.put("bodys", body);
        return response;
    }
    
    /** 
    * 余额变动查询
    */ 
    public static JSONObject builderResponseForQryBalanceRecord(String param){
    	JSONObject respObj = JSON.parseObject(param);
        JSONObject response = new JSONObject();
        JSONObject body  = new JSONObject();
        
        if(null == respObj){
            response.put("errorCode", ExpConstant.ERROR_OUTPUT_NULL);
            response.put("errorMsg", "unknown error，response is empty,plc check.");
            response.put("success", "OK");
            response.put("bodys", respObj);
            LOGGER.debug("一致性回参:空");
            return response;
        }
        
        String resultCode = respObj.getString("resultCode"); // 返回结果码
        String resultMsg = respObj.getString("resultMsg"); // 返回结果描述
    	
        response.put("errorCode", resultCode);
        response.put("errorMsg", resultMsg);
        response.put("success", "OK");
        body.put("dccResultCode", resultCode);
        body.put("dccResultMsg", resultMsg);
        if("0".equals(resultCode)){

            //套餐信息组个数测试环境暂时没有  暂时代码计算
            String dccControl101 = Integer.toString(respObj.getJSONArray("balanceTypeFlagQuery").size());

            JSONArray dccControl101list = new JSONArray(); 
            JSONObject dccControl101listObj = new JSONObject();

            String dccBillingCycle =  respObj.getString("billingCycleId");
            //dccControl101list节点组装
            JSONArray balanceTypeFlagQuery = respObj.getJSONArray("balanceTypeFlagQuery");
            
            
            for (int i = 0; i < balanceTypeFlagQuery.size(); i++) {
    			JSONObject dccControlObj = new JSONObject();
    			JSONObject balanceTypeFlagObj = balanceTypeFlagQuery.getJSONObject(i);

    			String aocUnit = balanceTypeFlagObj.getString("balanceTypeFlag");//余额类型标识
    			String dccPaymentAmount = balanceTypeFlagObj.getString("balanceIn");//本期入帐
    			String balanceAmount = balanceTypeFlagObj.getString("balanceEnd");//本期末余额
    			String dccBalUsedAmount = balanceTypeFlagObj.getString("balanceOut");//本期支出
    			//  此字段中台文档没有  是否为上期末余额还待确认
    			String aocBalance = balanceTypeFlagObj.getString("balanceBegin");//上期末余额
    			dccControlObj.put("aocUnit", aocUnit);
    			dccControlObj.put("dccPaymentAmount", dccPaymentAmount);
    			dccControlObj.put("balanceAmount", balanceAmount);
    			dccControlObj.put("dccBalUsedAmount", dccBalUsedAmount);
    			dccControlObj.put("aocBalance", aocBalance);
    			dccControlObj.put("dccBillingCycle", dccBillingCycle);
    			//单个返回obj 多个返回 list
    			if("1".equals(dccControl101)){
    				dccControl101listObj =dccControlObj;
    			}
    			dccControl101list.add(dccControlObj);	
    		}
            //body节点组装
            body.put("dccControl101", dccControl101);
            if("1".equals(dccControl101)){
            	body.put("dccControl101list", dccControl101listObj);
            }else{
            	body.put("dccControl101list", dccControl101list);
            }
        }
        response.put("bodys", body);
        return response;
    }
    
    
    /** 
    * 余额变动明细查询
    */ 
    public static JSONObject builderResponseForQryBalanceRecordDetail(String param){
    	JSONObject respObj = JSON.parseObject(param);
        JSONObject response = new JSONObject();
        JSONObject body  = new JSONObject();
        
        if(null == respObj){
            response.put("errorCode", ExpConstant.ERROR_OUTPUT_NULL);
            response.put("errorMsg", "unknown error，response is empty,plc check.");
            response.put("success", "OK");
            response.put("bodys", respObj);
            LOGGER.debug("一致性回参:空");
            return response;
        }
        
        String resultCode = respObj.getString("resultCode"); // 返回结果码
        String resultMsg = respObj.getString("resultMsg"); // 返回结果描述
    	
        response.put("errorCode", resultCode);
        response.put("errorMsg", resultMsg);
        response.put("success", "OK");
        body.put("dccResultCode", resultCode);
        body.put("dccResultMsg", resultMsg);
        if("0".equals(resultCode)){

            //套餐信息组个数测试环境暂时没有  暂时代码计算
            String dccControl101 = Integer.toString(respObj.getJSONArray("balanceChangeList").size());

            JSONArray dccControl101list = new JSONArray(); 
            JSONObject dccControl101listObj = new JSONObject();
            //dccControl101list节点组装
            JSONArray balanceChangeList = respObj.getJSONArray("balanceChangeList");
            for (int i = 0; i < balanceChangeList.size(); i++) {
    			JSONObject dccControlObj = new JSONObject();
    			JSONObject balanceChangeObj = balanceChangeList.getJSONObject(i);

    			
    			String dccBalUnitTypeId = balanceChangeObj.getString("balanceChangeType");//余额变动类型
    			String aocUnit = balanceChangeObj.getString("balanceTypeFlag");//余额类型标识
    			String dccPaymentAmount = balanceChangeObj.getString("balanceIn");//本期入帐
    			String balanceAmount = balanceChangeObj.getString("balanceEnd");//本期末余额
    			String dccStateDate = balanceChangeObj.getString("stateDate");//状态时间（变动时间） 			
    			String dccBalUsedAmount = balanceChangeObj.getString("balanceOut");//本期支出	
    			String dccCounts = balanceChangeObj.getString("accNbrDetail");//本期支出
                String dccBillingCycle =  balanceChangeObj.getString("billingCycleId");

                dccControlObj.put("dccBalUnitTypeId", dccBalUnitTypeId);
    			dccControlObj.put("aocUnit", aocUnit);
    			dccControlObj.put("dccPaymentAmount", dccPaymentAmount);
    			dccControlObj.put("balanceAmount", balanceAmount);  			
    			dccControlObj.put("dccStateDate", dccStateDate);
    			dccControlObj.put("dccBalUsedAmount", dccBalUsedAmount);
    			dccControlObj.put("dccCounts", dccCounts);
    			dccControlObj.put("dccBillingCycle", dccBillingCycle);
    			//单个返回obj 多个返回 list
    			if("1".equals(dccControl101)){
    				dccControl101listObj =dccControlObj;
    			}
    			dccControl101list.add(dccControlObj);	
    		}

            
            //body节点组装
            body.put("dccControl101", dccControl101);
            if("1".equals(dccControl101)){
            	body.put("dccControl101list", dccControl101listObj);
            }else{
            	body.put("dccControl101list", dccControl101list);
            }  
        }
        response.put("bodys", body);
        return response;
    }
    
    
    /** 
    * 话费返还记录查询
    */ 
    public static JSONObject builderResponseForQryReturnBalanceDetail(String param){
    	JSONObject respObj = JSON.parseObject(param);
        JSONObject response = new JSONObject();
        JSONObject body  = new JSONObject();
        
        if(null == respObj){
            response.put("errorCode", ExpConstant.ERROR_OUTPUT_NULL);
            response.put("errorMsg", "unknown error，response is empty,plc check.");
            response.put("success", "OK");
            response.put("bodys", respObj);
            LOGGER.debug("一致性回参:空");
            return response;
        }
        
        String resultCode = respObj.getString("resultCode"); // 返回结果码
        String resultMsg = respObj.getString("resultMsg"); // 返回结果描述
    	
        response.put("errorCode", resultCode);
        response.put("errorMsg", resultMsg);
        response.put("success", "OK");
        body.put("dccResultCode", resultCode);
        body.put("dccResultMsg", resultMsg);
        if("0".equals(resultCode)){
            //套餐信息组个数测试环境暂时没有  暂时代码计算
            String dccControl101 = Integer.toString(respObj.getJSONArray("returnPlanInfo").size());
            JSONArray dccControl101list = new JSONArray(); 
            //dccControl101list节点组装
            JSONArray returnPlanInfo = respObj.getJSONArray("returnPlanInfo");
            for (int i = 0; i < returnPlanInfo.size(); i++) {
    			JSONObject dccControlObj = new JSONObject();
    			JSONObject returnPlanInfoObj = returnPlanInfo.getJSONObject(i);

    			// 此处与中台文档不一致  源文档为  dccExpDate 待确认
    			String dccExpDate = returnPlanInfoObj.getString("endTime");//失效时间
    			String dccState = returnPlanInfoObj.getString("state");//状态 00A 有效00X 失效
    			String dccPaymentMethod = returnPlanInfoObj.getString("paymentMethod");//付款方式 
    			String dccEffDate = returnPlanInfoObj.getString("startTime");//生效时间
    			String dccReturnPlanId = returnPlanInfoObj.getString("returnPlanId");//返还计划实例ID 			
    			String dccStateDate = returnPlanInfoObj.getString("stateDate");//状态时间	
    			String dccReturnSumMonth = returnPlanInfoObj.getString("returnSumMonth");//累积返还月数
                String dccReturnSumBalance =  returnPlanInfoObj.getString("returnSumBalance");//累积返还金额 
    			String dccReturnLeftMonth = returnPlanInfoObj.getString("returnMonth");//剩余返还月数 
    			String dccReturnLeftBalance = returnPlanInfoObj.getString("returnBalance");//剩余返还金额 
                String dccBalanceTypeFlag =  returnPlanInfoObj.getString("balanceTypeId");//余额类型标识
                String dccReturnPlanName =  returnPlanInfoObj.getString("returnPlanName");//返还计划名称
                dccControlObj.put("dccExpDate", dccExpDate);
    			dccControlObj.put("dccState", dccState);
    			dccControlObj.put("dccPaymentMethod", dccPaymentMethod);
    			dccControlObj.put("dccEffDate", dccEffDate);  			
    			dccControlObj.put("dccReturnPlanId", dccReturnPlanId);
    			dccControlObj.put("dccStateDate", dccStateDate);
    			dccControlObj.put("dccReturnSumMonth", dccReturnSumMonth);
    			dccControlObj.put("dccReturnSumBalance", dccReturnSumBalance);
    			
    			dccControlObj.put("dccReturnLeftMonth", dccReturnLeftMonth);
    			dccControlObj.put("dccReturnLeftBalance", dccReturnLeftBalance);

    			dccControlObj.put("dccBalanceTypeFlag", dccBalanceTypeFlag);		
    			dccControlObj.put("dccReturnPlanName", dccReturnPlanName);	
    			dccControlObj.put("unlimitedReturn", returnPlanInfoObj.getString("unlimitedReturn"));//是否无限返还 0否1是

    			dccControl101list.add(dccControlObj);	
    		}
            
            //body节点组装
            body.put("dccControl101", dccControl101);
            body.put("dccControl101list", dccControl101list);   
        }
        response.put("bodys", body);
        return response;
    }
    
    /** 
    * 话费返还记录明细查询
    */ 
    public static JSONObject builderResponseForQryReturnBalanceInfoDetail(String param){
    	JSONObject respObj = JSON.parseObject(param);
        JSONObject response = new JSONObject();
        JSONObject body  = new JSONObject();
        
        if(null == respObj){
            response.put("errorCode", ExpConstant.ERROR_OUTPUT_NULL);
            response.put("errorMsg", "unknown error，response is empty,plc check.");
            response.put("success", "OK");
            response.put("bodys", respObj);
            LOGGER.debug("一致性回参:空");
            return response;
        }
        
        String resultCode = respObj.getString("resultCode"); // 返回结果码
        String resultMsg = respObj.getString("resultMsg"); // 返回结果描述
    	
        response.put("errorCode", resultCode);
        response.put("errorMsg", resultMsg);
        response.put("success", "OK");
        body.put("dccResultCode", resultCode);
        body.put("dccResultMsg", resultMsg);
        if("0".equals(resultCode)){

            //套餐信息组个数测试环境暂时没有  暂时代码计算
            String dccControl101 = Integer.toString(respObj.getJSONArray("returnPlanDetInfo").size());

            JSONArray dccControl101list = new JSONArray(); 
            //dccControl101list节点组装
            JSONArray returnPlanDetInfo = respObj.getJSONArray("returnPlanDetInfo");
            for (int i = 0; i < returnPlanDetInfo.size(); i++) {
    			JSONObject dccControlObj = new JSONObject();
    			JSONObject returnPlanDetInfoObj = returnPlanDetInfo.getJSONObject(i);

    			// 此处与中台文档不一致  源文档为  dccExpDate 待确认
    			String dccExpDate = returnPlanDetInfoObj.getString("endTime");//失效时间
    			String dccEffDate = returnPlanDetInfoObj.getString("startTime");//生效时间	
    			//TODO此参数不返回  dccPaymentAmount未确定
    			String dccPaymentAmount = returnPlanDetInfoObj.getString("returnBalance");//returnBalance 	
    			String dccReturnPlanId = returnPlanDetInfoObj.getString("returnPlanId");//返还计划实例ID	
    			String dccReturnFlag = returnPlanDetInfoObj.getString("returnFlag");//状态时间	   
    			String dccStateDate = returnPlanDetInfoObj.getString("stateDate");//状态时间	   
                String dccBalanceTypeFlag =  returnPlanDetInfoObj.getString("balanceTypeId");//余额类型标识
                String dccReturnPlanName =  returnPlanDetInfoObj.getString("returnPlanName");//返还计划名称

                dccControlObj.put("dccExpDate", dccExpDate);
    			dccControlObj.put("dccEffDate", dccEffDate);  		
    			dccControlObj.put("dccPaymentAmount", dccPaymentAmount);
    			dccControlObj.put("dccReturnPlanId", dccReturnPlanId);
    			dccControlObj.put("dccReturnFlag", dccReturnFlag);
    			dccControlObj.put("dccStateDate", dccStateDate);
    			dccControlObj.put("dccBalanceTypeFlag", dccBalanceTypeFlag);		
    			dccControlObj.put("dccReturnPlanName", dccReturnPlanName);		

    			dccControl101list.add(dccControlObj);	
    		}

            
            //body节点组装
            body.put("dccControl101", dccControl101);
            body.put("dccControl101list", dccControl101list);   
        }
        response.put("bodys", body);
        return response;
    }
    
    
    
    public static JSONObject builderResponseForOthGetOwe(String rst){
    	JSONObject requestParam = JSON.parseObject(rst);
        JSONObject response = new JSONObject();
        if(null == rst){
            return response;
        }
        
        //新出参以IRESULT，SMSG作为接口返回结果
          String status = requestParam.getString("errorCode"); // 返回结果码
          String msg = requestParam.getString("errorMsg"); // 返回结果描述
          
        if ("0".equals(status)) {
            JSONObject  oweInfo = requestParam.getJSONObject("oweInfo");  
            JSONArray vOweInfo = oweInfo.getJSONArray("vOweInfo");

            String acctName =oweInfo.getString("sAcctName");  
            String balance =oweInfo.getString("lBalance");
            String iresult =status;
            String realBalance =oweInfo.getString("lRealTimeBalance");
            
            //未找到对应关系
            String agentAreaCode =oweInfo.getString("sOweAreaCode");
            String preFlag =oweInfo.getString("sAcctName");
            
            //遍历vOweInfo 塞入othgetowelist
            JSONArray othgetowelist = new JSONArray();
            if(null == vOweInfo || vOweInfo.isEmpty()){
            	//不欠费用户  按老逻辑 iresult 不能为0 
            	iresult = "201013";
            	response.put("iresult",iresult);
            	response.put("smsg","该帐户无欠费！" );
            	return response;
            }
            
            for (int i = 0; i < vOweInfo.size(); i++) {
            	JSONObject othGetOweObj = new JSONObject();
                JSONObject vOweInfoObj = vOweInfo.getJSONObject(i);
                othGetOweObj.put("acctId", vOweInfoObj.getString("lAcctID"));
                othGetOweObj.put("acctItemCharge",vOweInfoObj.getString("lBillCharge") );
                othGetOweObj.put("acctItemTypeId",vOweInfoObj.getString("lAcctItemTypeId") );
                othGetOweObj.put("acctItemTypeName", vOweInfoObj.getString("sAcctItemTypeName"));
                othGetOweObj.put("areaCode",vOweInfoObj.getString("sAgentAreaCode") );
                othGetOweObj.put("billed", vOweInfoObj.getString("lBilled"));
                othGetOweObj.put("billingCycleId",vOweInfoObj.getString("lBillingCycleId") );
                othGetOweObj.put("cycleBeginDate",vOweInfoObj.getString("sCycleBeginDate") );
                othGetOweObj.put("cycleEndDate",vOweInfoObj.getString("sCycleEndDate") );
                othGetOweObj.put("due", vOweInfoObj.getString("lDue"));
                othGetOweObj.put("extServId", vOweInfoObj.getString("sExtServID"));               
                othgetowelist.add(othGetOweObj);
                
			}
            response.put("acctName", acctName);
            response.put("agentAreaCode", agentAreaCode);
            response.put("balance", balance);
            response.put("iresult", iresult);
            response.put("othgetowelist", othgetowelist);
            response.put("preFlag", preFlag);
            response.put("realBalance", realBalance);
        } else {
        	LOGGER.debug("一致性回参:{}",requestParam);
        	response.put("iresult",status);
        	response.put("smsg",msg );
        }
        return response;
    }
    
    
    /** 
    * 查询业务接口类型OTH_GET_TYPE
    */
    public static JSONObject builderResponseForOthGetServType(String rst){
    	JSONObject requestParam = JSON.parseObject(rst);
		
        JSONObject response = new JSONObject();
        if(null == rst){
        	LOGGER.debug("一致性回参:空");
            return response;
        }
        
        //新出参以IRESULT，SMSG作为接口返回结果
          String status = requestParam.getString("errorCode"); // 返回结果码
          response.put("iresult", status);
          if ("0".equals(status)){
        	 JSONObject  servType = requestParam.getJSONObject("servType");
        	 response.put("acctName", servType.getString("sAccName"));
        	 response.put("fundsType", servType.getString("lUserType"));      	 
          }
          
          return response;	
    }
    
    
    /** 
    * 短信订制清单  TKT_YI_UNI_MSG_TICKET_QR
    */ 
    public static JSONObject builderResponseForTktYiUniMsgTicketQr(String result, RespHead responseHead, RespInfo responseJson, String tranId){

        JSONObject response;
        String responseTime = Utils.getCurrentTime();
        
        if (StringUtils.isNotBlank(result)) {
            JSONObject jsonObject = JSON.parseObject(result);
            JSONObject json = new JSONObject();
            if (jsonObject != null && !(jsonObject.isEmpty())) {
                String resultCode = jsonObject.getString("resultCode");
                String resultMsg = jsonObject.getString("resultMsg");
                if("0".equals(resultCode)) {//接口调用成功，拼接出参

                	 json.put("TotalAcctItemCharge", MainUtils.getNullEmpty(jsonObject.getString("TOTAL_ACCT_ITEM_CHARGE")));
                	 json.put("totalAcctItemChargeCh", MainUtils.getNullEmpty(jsonObject.getString("TOTAL_ACCT_ITEM_CHARGE_CH")));
                	 json.put("totalTicketCharge", MainUtils.getNullEmpty(jsonObject.getString("TOTAL_TICKET_CHARGE")));
                	 json.put("totalTicketChargeCh", MainUtils.getNullEmpty(jsonObject.getString("TOTAL_TICKET_CHARGE_CH")));
                	
                	
                    JSONArray blances = new JSONArray();
                    JSONArray array ;
                    String str = jsonObject.getString("data");
                    if(str.startsWith("[")) {
                        array = jsonObject.getJSONArray("data");
                        for(int i = 0; i< array.size(); i++) {
                            JSONObject obj = new JSONObject();
                            JSONObject jsonBlance = array.getJSONObject(i);
                            obj.put("ticketNumber", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_NUMBER")));// 序号
                            obj.put("ticketTypeId", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_TYPE_ID")));// 序号
                            obj.put("ticketType", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_TYPE")));// 使用方式
                            obj.put("nbr", MainUtils.getNullEmpty(jsonBlance.getString("ACC_NBR")));// 对方号码
                            obj.put("startTime", MainUtils.getNullEmpty(jsonBlance.getString("START_TIME")));// 发送日期
                            obj.put("startDateNew", MainUtils.getNullEmpty(jsonBlance.getString("START_DATE_NEW")));// 发送日期
                            obj.put("startTimeNew", MainUtils.getNullEmpty(jsonBlance.getString("START_TIME_NEW")));// 发送时间
                            obj.put("agentCode", MainUtils.getNullEmpty(jsonBlance.getString("AGENT_CODE")));// 业务码
                            
                            obj.put("acctItemCharge", MainUtils.getNullEmpty(jsonBlance.getString("ACCT_ITEM_CHARGE")));// 接入码
                            
                            obj.put("acctItemChargeCh", MainUtils.getNullEmpty(jsonBlance.getString("ACCT_ITEM_CHARGE_CH")));// 接入码     
                            obj.put("ticketCharge", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_CHARGE")));// 话费
                            obj.put("ticketChargeCh", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_CHARGE_CH")));// 话费
                            obj.put("spName", MainUtils.getNullEmpty(jsonBlance.getString("SP_NAME")));// 流量
                            obj.put("connectName", MainUtils.getNullEmpty(jsonBlance.getString("CONNECT_NAME")));
                            obj.put("productId", MainUtils.getNullEmpty(jsonBlance.getString("PRODUCT_ID")));
                            obj.put("productName", MainUtils.getNullEmpty(jsonBlance.getString("PRODUCT_NAME")));
                            blances.add(obj);
                        }
                    } else {
                        JSONObject jsonBlance = jsonObject.getJSONObject("data");
                        JSONObject obj=new JSONObject();
                        obj.put("ticketNumber", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_NUMBER")));// 序号
                        obj.put("ticketTypeId", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_TYPE_ID")));// 序号
                        obj.put("ticketType", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_TYPE")));// 使用方式
                        obj.put("nbr", MainUtils.getNullEmpty(jsonBlance.getString("ACC_NBR")));// 对方号码
                        obj.put("startTime", MainUtils.getNullEmpty(jsonBlance.getString("START_TIME")));// 发送日期
                        obj.put("startDateNew", MainUtils.getNullEmpty(jsonBlance.getString("START_DATE_NEW")));// 发送日期
                        obj.put("startTimeNew", MainUtils.getNullEmpty(jsonBlance.getString("START_TIME_NEW")));// 发送时间
                        obj.put("agentCode", MainUtils.getNullEmpty(jsonBlance.getString("AGENT_CODE")));// 业务码
                        
                        obj.put("acctItemCharge", MainUtils.getNullEmpty(jsonBlance.getString("ACCT_ITEM_CHARGE")));// 接入码
                        
                        obj.put("acctItemChargeCh", MainUtils.getNullEmpty(jsonBlance.getString("ACCT_ITEM_CHARGE_CH")));// 接入码     
                        obj.put("ticketCharge", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_CHARGE")));// 话费
                        obj.put("ticketChargeCh", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_CHARGE_CH")));// 话费
                        obj.put("spName", MainUtils.getNullEmpty(jsonBlance.getString("SP_NAME")));// 流量
                        obj.put("connectName", MainUtils.getNullEmpty(jsonBlance.getString("CONNECT_NAME")));
                        obj.put("productId", MainUtils.getNullEmpty(jsonBlance.getString("PRODUCT_ID")));
                        obj.put("productName", MainUtils.getNullEmpty(jsonBlance.getString("PRODUCT_NAME")));
                        blances.add(obj);
                    }
                    json.put("items", blances);
                    json.put("iresult", resultCode);
                    json.put("smsg", resultMsg);
                    responseHead.setMsg(Utils.SUCCESS);
                    responseHead.setStatus("00");
                    responseHead.setResponseId(Utils.RSP + tranId);
                    responseHead.setResponseTime(responseTime);
                    responseJson.setBody(json);
                } else {
                    responseHead.setMsg(resultMsg);
                    responseHead.setStatus("01");
                    responseHead.setResponseId(Utils.RSP + tranId);
                    responseHead.setResponseTime(responseTime);
                    json.put("iresult", resultCode);
                    json.put("smsg", resultMsg);
                    responseJson.setBody(json);
                }
            } else {
                responseHead.setMsg("计费接口出参null");
                responseHead.setStatus("99");
                responseHead.setResponseId(Utils.RSP + tranId);
                responseHead.setResponseTime(responseTime);
            }
        } else {
            responseHead.setMsg("调用企信失败");
            responseHead.setStatus("02");
            responseHead.setResponseId(Utils.RSP + tranId);
            responseHead.setResponseTime(responseTime);
        }
        response = (JSONObject) JSON.toJSON(responseJson);
        return response;
 	
    }
    
    /** 
    * 其他增值详单
    */ 
    public static JSONObject builderResponseForTktYiValueTicketQr(String rst){
   	JSONObject requestParam = JSON.parseObject(rst);
		
        JSONObject response = new JSONObject();
        if(null == rst){
        	LOGGER.debug("其他增值详单回参:空");
            return response;
        }
        
        //新出参以IRESULT，SMSG作为接口返回结果
          String status = requestParam.getString("resultCode"); // 返回结果码
          String msg = requestParam.getString("resultMsg"); // 返回结果描述
        
        if ("0".equals(status)) {
        	String totalTicketChargeCh =  requestParam.getString("TOTAL_TICKET_CHARGE_CH");
        	JSONArray valueTicketgulist = new JSONArray();
        	
            JSONArray data = requestParam.getJSONArray("data");

            for (int i = 0; i < data.size(); i++) {
            	JSONObject valueTicketguObj = new JSONObject();
            	
                JSONObject dataObj = data.getJSONObject(i);
                     
                valueTicketguObj.put("accNbr", dataObj.getString("ACC_NBR"));
                valueTicketguObj.put("agentCode",dataObj.getString("AGENT_CODE") );
                valueTicketguObj.put("productId",dataObj.getString("PRODUCT_ID") );
                valueTicketguObj.put("productName", dataObj.getString("PRODUCT_NAME"));
                valueTicketguObj.put("startDateNew",dataObj.getString("START_DATE_NEW") );
                valueTicketguObj.put("startTime", dataObj.getString("START_TIME"));
                valueTicketguObj.put("startTimeNew",dataObj.getString("START_TIME_NEW") );
                valueTicketguObj.put("ticketCharge",dataObj.getString("TICKET_CHARGE") );
                valueTicketguObj.put("ticketChargeCh",dataObj.getString("TICKET_CHARGE_CH") );
                valueTicketguObj.put("ticketNumber", dataObj.getString("TICKET_NUMBER"));
                valueTicketguObj.put("ticketType", dataObj.getString("TICKET_TYPE"));
                valueTicketguObj.put("ticketTypeId", dataObj.getString("TICKET_TYPE_ID"));
                valueTicketgulist.add(valueTicketguObj);                
			}
            response.put("iresult", status);
            response.put("smsg", msg);
            response.put("totalTicketChargeCh", totalTicketChargeCh);
            response.put("valueTicketgulist", valueTicketgulist);

        } else {
        	LOGGER.debug("一致性回参:{}",requestParam);
        	response.put("iresult",status);
        	response.put("smsg",msg );
        }
        return response;
    }
    
    /** 
    * WLAN清单查询
    */ 
    public static JSONObject builderResponseForTktGuWlanTicketQr(String rst){
   	JSONObject requestParam = JSON.parseObject(rst);
		
        JSONObject response = new JSONObject();
        if(null == rst){
        	LOGGER.debug("其他增值详单回参:空");
            return response;
        }
        
        //新出参以IRESULT，SMSG作为接口返回结果
          String status = requestParam.getString("resultCode"); // 返回结果码
          String msg = requestParam.getString("resultMsg"); // 返回结果描述

        
        if ("0".equals(status)) {
        	String chargeCntCh = MainUtils.getNullEmpty(requestParam.getString("CHARGE_CNT_CH")); //总计费用
        	String durationCntch = MainUtils.getNullEmpty(requestParam.getString("DURATION_CNT_CH"));
        	//String totalBytesCnt = MainUtils.getNullEmpty(requestParam.getString("TOTAL_BYTES_CNT"));//总计流量
            JSONArray blances = new JSONArray();
            String str = requestParam.getString("data");
            JSONArray array ;
      
            if(str.startsWith("[")) {
                array = requestParam.getJSONArray("data");
                response.put("count", String.valueOf(array.size()));
                for(int i = 0; i< array.size(); i++) {
                    JSONObject obj = new JSONObject();
                    JSONObject jsonBlance = array.getJSONObject(i);
                    
                    obj.put("acctItemCharge",MainUtils.getNullEmpty(jsonBlance.getString("ACCT_ITEM_CHARGE_CH")));//金额单位是0.00元
                    obj.put("startTimeNew",MainUtils.getNullEmpty(jsonBlance.getString("START_TIME_NEW")));//上线时间
                    obj.put("endDateNew",MainUtils.getNullEmpty(jsonBlance.getString("END_DATE_NEW")));//上线时间
                    obj.put("ticketNumber",MainUtils.getNullEmpty(jsonBlance.getString("TICKET_NUMBER")));// 序号
                    obj.put("bytesCnt",MainUtils.getNullEmpty(jsonBlance.getString("BYTES_CNT")));// 序号
                    obj.put("ticketType", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_TYPE"))); //业务类型
                    obj.put("startDateNew", MainUtils.getNullEmpty(jsonBlance.getString("START_DATE_NEW")));//上线日期
                    obj.put("productName",MainUtils.getNullEmpty(jsonBlance.getString("PRODUCT_NAME")));
                    obj.put("duartionCh",MainUtils.getNullEmpty(jsonBlance.getString("DURATION_CH")));//使用时长
                    obj.put("acctName",MainUtils.getNullEmpty(jsonBlance.getString("ACCT_NAME")));// 话单标识
                    obj.put("endTimeNew",MainUtils.getNullEmpty(jsonBlance.getString("END_TIME_NEW")));// 话单标识
                    obj.put("chargeCntCh",chargeCntCh);// 话单标识
                    obj.put("durationCntch",durationCntch);// 话单标识
                    blances.add(obj);
                }
            } else {
                JSONObject jsonBlance = requestParam.getJSONObject("data");
                JSONObject obj=new JSONObject();
                response.put("count", "1");
                obj.put("acctItemCharge",MainUtils.getNullEmpty(jsonBlance.getString("ACCT_ITEM_CHARGE_CH")));//金额单位是0.00元
                obj.put("startTimeNew",MainUtils.getNullEmpty(jsonBlance.getString("START_TIME_NEW")));//上线时间
                obj.put("endDateNew",MainUtils.getNullEmpty(jsonBlance.getString("END_DATE_NEW")));//上线时间
                obj.put("ticketNumber",MainUtils.getNullEmpty(jsonBlance.getString("TICKET_NUMBER")));// 序号
                obj.put("bytesCnt",MainUtils.getNullEmpty(jsonBlance.getString("BYTES_CNT")));// 序号
                obj.put("ticketType", MainUtils.getNullEmpty(jsonBlance.getString("TICKET_TYPE"))); //业务类型
                obj.put("startDateNew", MainUtils.getNullEmpty(jsonBlance.getString("START_DATE_NEW")));//上线日期
                obj.put("productName",MainUtils.getNullEmpty(jsonBlance.getString("PRODUCT_NAME")));
                obj.put("duartionCh",MainUtils.getNullEmpty(jsonBlance.getString("DURATION_CH")));//使用时长
                obj.put("acctName",MainUtils.getNullEmpty(jsonBlance.getString("ACCT_NAME")));// 话单标识
                obj.put("endTimeNew",MainUtils.getNullEmpty(jsonBlance.getString("END_TIME_NEW")));// 话单标识
                obj.put("chargeCntCh",chargeCntCh);// 话单标识
                obj.put("durationCntch",durationCntch);// 话单标识
                blances.add(obj);
            }

            response.put("items", blances);
        } else {
        	LOGGER.debug("一致性回参:{}",requestParam);
        }
	    response.put("iresult", status);
	    response.put("smsg", msg);
	    return response;
    	
    	
    }
    
    public static JSONObject builderResponseForForQryBill(String param){
    	JSONObject respObj = JSON.parseObject(param);
        JSONObject response = new JSONObject();
        JSONObject body  = new JSONObject();
        
        if(null == respObj){
            response.put("errorCode", ExpConstant.ERROR_OUTPUT_NULL);
            response.put("errorMsg", "unknown error，response is empty,plc check.");
            response.put("success", "OK");
            response.put("bodys", respObj);
            LOGGER.debug("一致性回参:空");
            return response;
        }
        
        String resultCode = respObj.getString("resultCode"); // 返回结果码
        String resultMsg = respObj.getString("resultMsg"); // 返回结果描述
    	
        response.put("errorCode", resultCode);
        response.put("errorMsg", resultMsg);
        response.put("success", "OK");
        body.put("dccResultCode", resultCode);
        body.put("dccResultMsg", resultMsg);
    	
        if("0".equals(resultCode)){	   
            //信息组个数测试环境暂时没有
            String dccControl101 = "1";
            
            JSONObject dccControl101list = new JSONObject();

            String dccAcctId = respObj.getString("acctId");
            String dccAccNbr = respObj.getString("accNbr");
            
            //付费方式没找到对应参数
            String dccPaymentMethod = respObj.getString("未对应");
            
            //欠费帐务周期信息组
            String dccControl102 = Integer.toString(respObj.getJSONArray("feeBillingCycle").size());
            //欠费帐务周期信息组
            JSONArray dccControl102list = new JSONArray();  
            //欠费帐务周期信息组节点组装
            JSONArray feeBillingCycleArray = respObj.getJSONArray("feeBillingCycle");
            for (int i = 0; i < feeBillingCycleArray.size(); i++) {
    			JSONObject dccControl102Obj = new JSONObject();
    			JSONObject feeBillingCycleObj = feeBillingCycleArray.getJSONObject(i);
    			//账务周期标识
    			String billingCycleId = feeBillingCycleObj.getString("billingCycleId");
    			//费用类型信息组
    			JSONArray acctItemGroupList =feeBillingCycleObj.getJSONArray("acctItemGroupList");
    			JSONArray dccControl103list = new JSONArray(); 
    			 for (int j = 0; j < acctItemGroupList.size(); j++){
    	    			JSONObject dccControl103Obj = new JSONObject();
    	    			JSONObject acctItemGroupObj= acctItemGroupList.getJSONObject(j);
    	    			// 费用类型 0：通讯费 1：滞纳金  99：信息费   ----- 账目组标识
    	    			String acctItemGroupId = acctItemGroupObj.getString("acctItemGroupId");
    	    			JSONArray acctItemTypeList =acctItemGroupObj.getJSONArray("acctItemType");
    	    			JSONArray dccControl104list = new JSONArray();
    	    			 for (int k = 0; k < acctItemTypeList.size(); k++){
    	    	    			JSONObject dccControl104Obj = new JSONObject();
    	    	    			JSONObject acctItemTypeObj= acctItemTypeList.getJSONObject(k);
    	    	    			String acctItemCharge = acctItemTypeObj.getString("acctItemCharge");
    	    	    			String acctItemTypeName = acctItemTypeObj.getString("acctItemTypeName");
 	    			
    	    	    			dccControl104Obj.put("dccAcctItemTypeName", acctItemTypeName);
    	    	    			dccControl104Obj.put("dccOweDigits",acctItemCharge );
    	    	    			dccControl104list.add(dccControl104Obj);
    	    			 }
    	    			 
    	    			 dccControl103Obj.put("dccControl104", acctItemTypeList.size());
    	    			 dccControl103Obj.put("dccControl104list", dccControl104list);
    	    			 dccControl103Obj.put("dccPtyRoleId", acctItemGroupId);
    	    			 dccControl103list.add(dccControl103Obj);			
    			 }

    			//本期已付费用 未找到
    			dccControl102Obj.put("dccBillFee", "0");
    			//欠费帐务周期
    			dccControl102Obj.put("dccBillingCycle", billingCycleId);
    			//费用类型个数
    			dccControl102Obj.put("dccControl103", feeBillingCycleArray.size());
    			//费用类型信息组
    			dccControl102Obj.put("dccControl103list", dccControl103list);
    			//总费用
    			dccControl102Obj.put("dccTotalFee", "111");

    			dccControl102list.add(dccControl102Obj);	
    		}
            
            //dccControl101list节点组装
            dccControl101list.put("dccAccNbr", dccAccNbr);
            dccControl101list.put("dccControl102", dccControl102);
            dccControl101list.put("dccControl102list", dccControl102list);
            dccControl101list.put("dccPaymentMethod", dccPaymentMethod);
            
            //body节点组装
            body.put("dccAcctId", dccAcctId);
            body.put("dccControl101", dccControl101);
            body.put("dccControl101list", dccControl101list); 
        }

        response.put("bodys", body);
        return response;
    }
    
    
    public static JSONObject builderResponseForForQryBillNew(String param){
    	JSONObject respObj = JSON.parseObject(param);
        JSONObject response = new JSONObject();
        
        if(null == respObj){
            response.put("errorCode", ExpConstant.ERROR_OUTPUT_NULL);
            response.put("errorMsg", "unknown error，response is empty,plc check.");
            response.put("success", "OK");
            response.put("bodys", respObj);
            LOGGER.debug("一致性回参:空");
            return response;
        }
        
        String resultCode = respObj.getString("resultCode"); // 返回结果码
        String resultMsg = respObj.getString("resultMsg"); // 返回结果描述
        respObj.put("shouldCharge", respObj.getString("shouldCharge"));
        respObj.put("derateDue", respObj.getString("derateDue"));
        respObj.put("due", respObj.getString("due"));
        response.put("errorCode", resultCode);
        response.put("errorMsg", resultMsg);
        response.put("success", "OK");

        response.put("bodys", respObj);
        return response;
    }
    
    // 响应结果构建
    public JSONObject builderResponse(JSONObject respJO) {
        JSONObject response = new JSONObject();
        if(null == respJO){
            response.put("errorCode", ExpConstant.ERROR_OUTPUT_NULL);
            response.put("errorMsg", "unknown error，response is empty,plc check.");
            response.put("success", "OK");
            response.put("bodys", respJO);
            LOGGER.debug("一致性回参:空");
            return response;
        }
        String dccResultCode = respJO.getString("dccResultCode"); // 返回结果码
        String dccResultMsg = respJO.getString("dccResultMsg"); // 返回结果描述
        response.put("errorCode", dccResultCode);
        response.put("errorMsg", dccResultMsg);
        response.put("success", "OK");
        response.put("bodys", respJO);
        return response;
    }
    /** 
    * 出参解析成查询域 head body格式
    */ 
    public static String builderQueryResponse(String param){
    	JSONObject requestParam = JSON.parseObject(param);
        JSONObject response = new JSONObject();
        JSONObject head = new JSONObject();
        String responseTime = Utils.getCurrentTime();
        if(null == param){
        	LOGGER.debug("一致性回参:空");
            return response.toJSONString();
        }
        
        //新出参以resultCode，resultMsg作为接口返回结果
        String status = requestParam.getString("resultCode"); // 返回结果码
        String msg = requestParam.getString("resultMsg"); // 返回结果描述
       // String responseId = requestParam.getString("TRANSACTION_ID"); // 查询域受理流水
       //查询域受理流水 企信不再返回
        String responseId ="";
        
        if ("0".equals(status)) {
        	head.put("msg", "success");
            head.put("responseId", responseId);
            head.put("responseTime", responseTime);
            head.put("status", "00");
        } else {
        	head.put("msg", msg);
            head.put("responseId", responseId);
            head.put("responseTime", responseTime);
            head.put("status", "01");
        }
        response.put("body", requestParam);
        response.put("head", head);
        return response.toJSONString();
    }
}
