package cn.js189.uqc.mapper;

import cn.js189.uqc.domain.*;
import cn.js189.uqc.domain.check.CheckInfo;
import cn.js189.uqc.domain.invoice.*;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface CheckInfoMapper {
    
    void insertCheckInfoLog(CheckInfo checkInfo);
    
    String getTotalCountByChannelId(@Param("channelId") String channelId);
    
    List<HashMap<String,String>> getChannelApiAccessCountMap();
    
    void updateTotalCountByChannelId(@Param("channelId") String channelId, @Param("actionCode") String actionCode ,@Param("addCount") String addCount);
    
    void deleteByChannelIdAndActionCode(@Param("channelId") String channelId, @Param("actionCode") String actionCode);
    
    void insertInnerApiLog(InnerApiLog innerApiLog);
    
    List<String> selectOfferSpecIdList();
    
    List<OfferInfo> selectOfferInfoByAreaCode(@Param("areaCode") String areaCode);
    
    String selectOfferChargeMoney(@Param("offerIds") List<String> offerIds);
    
    Set<String> selectOfferConflictDependIds(@Param("offerOriginalId") String offerOriginalId, @Param("type") String type);
    
    List<OfferInfo> selectOfferInfoById(@Param("offerIds") List<String> offerIds);
    
    void insertServiceLog(ServiceLog serviceLog);
    
    int selectIfExistAccnbr(@Param("accnbr") String accnbr);
    
    List<String> selectIfExistDepeTargetId(@Param("offerId") String offerId,@Param("targetId") String targetId);
    
    List<String> selectIfExistMutuTargetId(@Param("offerId") String offerId,@Param("targetId") String targetId);
    
    Set<String> selectVoucherConflictOrDependIds(@Param("type") String type);
    
    /***************** plus会员 *****************/
    void insertPlusVipInfo(PlusVipInfo plusVipInfo);
    
    void updatePlusVipInfoNotifyStatus(@Param("orderId") String orderId,@Param("notifyStatus") String notifyStatus);
    
    /***************** 宽带加价提速 *****************/
    
    List<BroadbandOffer> selectOffers(@Param("offerCategory") String offerCategory);
    
    Set<String> selectAllOfferConflictDependIds(@Param("offerOriginalId") String offerOriginalId, @Param("type") String type, @Param("offerCategory") String offerCategory);
    
    BroadbandOffer selectBroadbandConflictOfferInfoByOfferId(@Param("targetOfferId") String targetOfferId, @Param("type") String type, @Param("offerCategory") String offerCategory);
    
    int selectOfferChargeByOfferCategory(@Param("offerIds") List<String> offerIds, @Param("offerCategory") String offerCategory);
    
    String selectOfferChargeIdByOfferCategory(@Param("offerIds") List<String> offerIds, @Param("offerCategory") String offerCategory);
    
    /***************** 电子发票 *****************/
    
    List<QxError> selectQxErrorInfo();
    
    // 更新商户s_pappid信息
    void updatesPappid(@Param("sPappid") String sPappid, @Param("appId") String appId, @Param("appSecret") String appSecret);
    
    // appid,appsecret获取s_pappid
    String selectSPappidByAppidAndAppsecret(@Param("appId") String appId, @Param("appSecret") String appSecret);
    
    // 获取商户联系方式
    String selectContactPhone(@Param("appId") String appId, @Param("appSecret") String appSecret);
    
    // 插入订单信息
    int insertInvoiceOrder(InvoiceOrder invoiceOrder);
    
    // 插入订单详情信息
    int insertInvoiceOrderDetail(@Param("list") List<InvoiceOrderDetail> list);
    
    // 账单id和产品实例id查询是否有发票信息
    InvoiceOrderDetail selectDetailByIdAndProductId(@Param("id") String id, @Param("productId") String productId);
    
    // 发票实例id查询发票信息
    InvoiceDetail selectInvoiceById(@Param("invoiceId") String invoiceId);
    
    // 发票实例id查询发票信息
    InvoiceDetail selectInvoiceByPartyId(@Param("invoiceId") String invoiceId,@Param("partyId") String partyId);
    
    // 一次授权查询发票信息
    InvoiceDetail selectInvoiceByIdOnceAuth(@Param("invoiceId") String invoiceId);
    
    // 一次授权查询发票信息
    InvoiceDetail selectInvoiceByPartyIdOnceAuth(@Param("invoiceId") String invoiceId,@Param("partyId") String partyId,@Param("appId") String appId);
    
    // 更新订单详情cardId
    int updateInvoiceCardIdById(@Param("orderId") String orderId ,@Param("invoiceId") String invoiceId, @Param("cardId") String cardId);
    
    // 更新订单详情sMediaId
    int updateInvoiceMediaIdById(@Param("orderId") String orderId ,@Param("invoiceId") String invoiceId, @Param("mediaId") String mediaId);
    
    // 更新订单表中的openid
    int updateInvoiceOrderWxOpenId(@Param("invoiceId") String invoiceId, @Param("openId") String openId);
    
    InvoiceDetail selectInvoiceType1000(InvoiceInfoDTO invoiceInfoDTO);
    
    InvoiceDetail selectInvoiceType1100(InvoiceInfoDTO invoiceInfoDTO);
    
    InvoiceDetail selectInvoiceType1200(InvoiceInfoDTO invoiceInfoDTO);
    
    // 更新发票信息
    int updateInvoiceOrderDetail(InvoiceOrderDetail invoiceOrderDetail);
    
    // 订单号更新开票状态信息
    int updateInvoiceDetailStatusByOrderId(@Param("orderId") String orderId, @Param("status") String status);
    
    // 更新发票信息
    int updateInvoiceOrderDetailByPK(InvoiceOrderDetail invoiceOrderDetail);
    
    // appid查询卡券模板id
    String selectCardIdByAppid(@Param("appId") String appId);
    
    // 根据appid查询数据库中模板信息
    BaseInvoiceTemplate selectCardTemplateByAppid(@Param("appId") String appId);
    
    // appid更新卡券模板id
    int updateCardIdByAppid(@Param("appid") String appId, @Param("cardId") String cardId);
    
    // 客户id查询用户信息
    User selectUserByPartyId(@Param("partyId") String partyId);
    
    // 插入用户信息
    int insertUserInfos(User user);
    
    // 更新用户信息
    int updateUserInfos(User user);
    
    // 更新微信用户授权状态
    int updateUserWxAuth(@Param("partyId") String partyId, @Param("wxAuth") String wxAuth);
    
    // 微信侧回调更新微信用户授权状态
    int updateUserWxAuthAndWxCardInsert(@Param("partyId") String partyId, @Param("wxAuth") String wxAuth);
    
    // 查询用户微信插卡总次数
    int selectWxUserTotalInsert(@Param("partyId") String partyId);
    
    // 更新用户微信插卡总次数
    int updateUserWxCardInsert(@Param("partyId") String partyId);
    
    InvoiceAuthDTO selectInvoiceAuthInfoByInvoiceIdAndSource(@Param("invoiceId") String invoiceId, @Param("source") String source);
    
    // 根据partyId,appid查询用户有无对应授权信息
    int selectUserAuthByPartyIdAndAppid(@Param("partyId") String partyId, @Param("appid") String appId);
    
    // 新增用户在appid应用内授权信息
    int insertUserAuthInfo(UserAuthDTO userAuthDTO);
    
    // 更新用户在appid应用内授权信息
    int updateUserAuthInfo(UserAuthDTO userAuthDTO);
    
    // 查询发票授权信息
    List<InvoiceAuthDTO> selectInvoiceAuthStatus();
    
    // 插入开票授权记录
    int insertInvoiceAuthInfo(InvoiceAuthDTO invoiceAuthDTO);
    
    // 更新授权表微信授权状态
    int updateInvoiceAuthStatus(@Param("id") String id, @Param("wxAuth") String wxAuth);
    
    // 发票实例id更新插卡状态
    int updateWxCardInsertByInvoiceId(@Param("orderId") String orderId ,@Param("invoiceId") String invoiceId, @Param("wxCardInsert") String wxCardInsert, @Param("encryptCode") String encryptCode);
    
    // 查询已授权并开具成功的发票id
    List<String> selectAllAuthSuccessInvoiceId();
    
    // 查询一次授权并开具成功的发票id
    List<Map<String,String>> selectAllOnceAuthSuccessInvoiceId();
    List<Map<String,String>> selectOnceAuthSuccessInvoiceByOrderId(@Param("orderId") String orderId);
    
    // 查询开票历史
    List<InvoiceHisDto> selectInvoiceHis(@Param("startTime") String startTime, @Param("endTime") String endTime, @Param("idCard") String idCard);
    
    List<InvoiceHisDto> selectMonthlyHis(@Param("phone") String phone, @Param("type") String type, @Param("startTime") String startTime, @Param("endTime") String endTime, @Param("phoneType") String phoneType);
    
    List<InvoiceHisDto> selectPaymentHis(@Param("phone") String phone, @Param("type") String type, @Param("startTime") String startTime, @Param("endTime") String endTime, @Param("phoneType") String phoneType);
    
    List<InvoiceHisDto> selectOrderOneItemHis(@Param("phone") String phone, @Param("type") String type, @Param("startTime") String startTime, @Param("endTime") String endTime, @Param("phoneType") String phoneType);
    
    List<SyncDetailDTO> selectType1000();
    
    int updateDetailBy1000(@Param("invoiceId") String invoiceId, @Param("orderId") String orderId, @Param("billMonth") String billMonth);
    
    List<SyncDetailDTO> selectType1100();
    
    int updateDetailBy1100(@Param("invoiceId") String invoiceId, @Param("orderId") String orderId, @Param("id") String id);
    
    List<SyncDetailDTO> selectType1200();
    
    int updateDetailBy1200(@Param("invoiceId") String invoiceId, @Param("orderId") String orderId, @Param("orderNumber") String orderNumber);
    
    List<Map<String, String>> selectListUpdatePdf();
    
    List<Map<String, String>> selectListUpdateDelayPdf();
    
    // 根据ticketId更新对应订单号
    int updateInvoiceAuthOrderIdByTicketId(@Param("ticketId") String ticketId, @Param("orderId") String orderId);
    
    // 根据ticketId更新发票授权状态
    int updateInvoiceAuthWxByTicketId(@Param("ticketId") String ticketId, @Param("authStatus") String authStatus);
    
    // 根据发票实例id更新发票授权状态
    int updateInvoiceAuthWxByInvoiceId(@Param("invoiceId") String invoiceId, @Param("authStatus") String authStatus);
    int updateDetailByInvoiceId(@Param("invoiceId") String invoiceId, @Param("orderId") String orderId, @Param("url") String url,
                                @Param("invoiceDate") String invoiceDate, @Param("invoiceCode") String invoiceCode, @Param("invoiceNo") String invoiceNo, @Param("fakeCode") String fakeCode);
    
    List<Map<String, String>> selectListByPartyId(@Param("partyId") String partyId);
    
    List<Map<String, String>> selectInfoByOrderId();
    
    int updateAliUserInfo(@Param("openId") String openId, @Param("partyId") String partyId);
    
    Map<String,String> selectWxAliAuthStatus(@Param("partyId") String partyId);
    
    String selectByInvoiceId(@Param("invoiceId") String invoiceId);
    
    String selectByInvoiceInfo(@Param("invoiceId") String invoiceId,@Param("invoiceCode") String invoiceCode,@Param("invoiceNo") String invoiceNo);
    
    int updateAliStatus(@Param("partyId") String partyId);
    
    int updateAliStatusSend(@Param("invoiceId") String invoiceId,@Param("invoiceCode") String invoiceCode,@Param("invoiceNo") String invoiceNo);
    
    int deleteOrderByOrderId(@Param("orderId") String orderId);
    
    int deleteOrderDetailByOrderId(@Param("orderId") String orderId);
    
    InvoiceDetail selectInvoiceType(@Param("did") String did);
    
    List<InvoiceDetail> selectInvoiceTypeForMerge(@Param("idList") List<String> idList);
    
    List<InvoiceDetail>	selectInvoiceTypeByOrderId(@Param("orderId") String orderId);
    
    /***************** 电子发票_开票历史_新 *****************/
    
    String selectAccountInvoiceDidByServiceId(@Param("idCard") String idCard, @Param("serviceVal") String serviceVal, @Param("columnName") String columnName,@Param("phone") String phone);
    
    List<String> selectAccountInvoiceDidByServiceIdForMerge(@Param("idCard") String idCard, @Param("serviceValList") List<String> serviceVal, @Param("columnName") String columnName,@Param("phone") String phone);
    
    String selectInvoiceDidByInvoiceIdAndType(@Param("idCard") String idCard, @Param("invoiceId") String invoiceId, @Param("type") String type);
    
    List<String> selectInvoiceDidByInvoiceIdAndTypeForMerge(@Param("idCard") String idCard, @Param("invoiceId") String invoiceId, @Param("type") String type);
    
    /***************** 电子发票_业务操作日志入库 *****************/
    void insertOperationLog(LogOperation logOperation);
    
    String selectOrderDetailById(@Param("invoiceId") String orderId ,@Param("columnName") String columnName ,@Param("serviceVal") String serviceVal);
    
    int deleteOrderDetailById(@Param("orderId") String orderId ,@Param("columnName") String columnName ,@Param("serviceVal") String serviceVal);
    
    Integer selectExpDate(@Param("partyId") String partyId ,@Param("source") String source);
    
    int updateDetailDelayFlag(@Param("invoiceId") String invoiceId, @Param("orderId") String orderId,@Param("delayFlag") String delayFlag);
    
    void insertWxInvoiceInsert(InvoiceInsert invoiceInsert);
    
    Integer selectInvoiceInsertCount(@Param("orderId") String orderId ,@Param("invoiceId") String invoiceId);
    
    int updateInvoiceInsertCount(@Param("orderId") String orderId ,@Param("invoiceId") String invoiceId,@Param("insertCount") Integer insertCount);
    
    int selectInvoiceTimeCount(@Param("invoiceId") String invoiceId);
    
    int insertUpOcrFtpInfo(UpOcrFtpInfo uofi);
    
    UpOcrFtpInfo getUpOcrFtpInfo(UpOcrFtpInfo uofi);
}
