package cn.js189.uqc.domain.qry;

import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 通过接入号的方式查询产品信息，生成请求报文
 * 
 * @author Mid
 */
public class QryProdInstByAcc extends QryProdInstBase {
	// 日志组件
	private static final Logger LOGGER = LoggerFactory.getLogger(QryProdInstByAcc.class);

	// 接入号码
	private String accNum;
	// 接入号码
	private String account;
	// 接入号码类型
	private String prodId;
	// 查询模式:1 查询本产品;2 查询拥有该号码的客户的所有的产品;
	private String qryMode;

	public QryProdInstByAcc(String regionId, String uncompletedFlag, String accNum, String account, String prodId, String qryMode) {
		super(regionId, uncompletedFlag);
		this.accNum = accNum;
		this.account = account;
		this.prodId = prodId;
		this.qryMode = qryMode;
	}

	@Override
	protected JSONObject improveRequestObject(JSONObject requestObject) {
		requestObject.put("accNum", accNum);
		requestObject.put("account", account);
		requestObject.put("prodId", prodId);
		requestObject.put("qryMode", qryMode);
		LOGGER.debug("requestObject:{}", requestObject.toString());
		return requestObject;
	}
}
