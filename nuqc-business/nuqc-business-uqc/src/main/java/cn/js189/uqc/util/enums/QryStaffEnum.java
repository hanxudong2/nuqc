package cn.js189.uqc.util.enums;

public enum QryStaffEnum {

    STAFF("Staff","员工"),
    STAFF_ORG_REL("StaffOrgRel","员工组织关系"),
    STAFF_ATTR("StaffAttr","员工属性"),
    STAFF_CONTACT_INFO("StaffContactInfo","员工联系信息");

    // 范围编码
    private String scopeCode;
    // 范围描述
    private String scopeDesc;

    private QryStaffEnum(String scopeCode, String scopeDesc) {
        this.scopeCode = scopeCode;
        this.scopeDesc = scopeDesc;
    }

    public String getScopeCode() {
        return scopeCode;
    }

    public String getScopeDesc() {
        return scopeDesc;
    }
}
