package cn.js189.uqc.service;

import cn.js189.common.domain.EuaAuthority;
import cn.js189.common.domain.EuaChannel;

import java.util.List;

public interface SysEuaService {

	List<EuaAuthority> listAll(String channelId);
	
	Integer listCount();
	
	List<EuaChannel> listAllKey(String channelId);
	
}
