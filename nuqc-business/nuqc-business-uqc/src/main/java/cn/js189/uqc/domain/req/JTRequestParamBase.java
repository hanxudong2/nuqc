package cn.js189.uqc.domain.req;

import cn.hutool.crypto.SecureUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * 集团生成请求头参基础类
 */
public abstract class JTRequestParamBase {

	/**
	 * 日志组件
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(JTRequestParamBase.class);

	/**
	 * 接口版本
	 */
	public static final String VERSION_PORT = "0000";

	/**
	 * 密钥版本
	 */
	public static final String VERSION_KEY = "00";

	/**
	 * 接口名称
	 */
	public static final String INTERFACE_NAME = "/osa/interface";

	// 时间格式yyyyMMddHHmmss
	private static final String BSS_TCP_CONT_DATE_FORMAT = "yyyyMMddHHmmssSSS";

	/**
	 * 生成 x-auth-dq 请求参数
	 *
	 * @return
	 */
	public String generateXAuthDqRoot(String requestBody,String method) {
		return generateXAuthDq(requestBody,method);
	}

	/**
	 * 生成 x-auth-dq 会话控制信息
	 *
	 * @return
	 */
	protected abstract String generateXAuthDq(String requestBody,String method);

	/**
	 *  生成 x-auth-dq 会话控制信息
	 * @param requestBody:请求参数
	 * @param key: 密钥
	 * @param sysCode:系统编码
	 * @param appCode:应用编码
	 * @param method:方法编码
	 * @return
	 */
	protected String generateXAuthDq(String requestBody, String key, String sysCode, String appCode, String method) {
		String reqTime = new SimpleDateFormat(BSS_TCP_CONT_DATE_FORMAT).format(new Date()).substring(0,14);

		String uuid = UUID.randomUUID().toString().replaceAll("-", "");
		String transactionId = sysCode+appCode+reqTime.substring(0,12)+uuid;
		String sign = VERSION_KEY + SecureUtil.md5(transactionId+key+INTERFACE_NAME+requestBody);
//		String xAuthDq = transactionId+";"+sysCode+";"+appCode+";"+method+";"+VERSION_PORT+";"+sign+";"+reqTime;
		StringBuilder xAuthDq = new StringBuilder();
		xAuthDq.append(transactionId);
		xAuthDq.append(";");
		xAuthDq.append(sysCode);
		xAuthDq.append(";");
		xAuthDq.append(appCode);
		xAuthDq.append(";");
		xAuthDq.append(method);
		xAuthDq.append(";");
		xAuthDq.append(VERSION_PORT);
		xAuthDq.append(";");
		xAuthDq.append(sign);
		xAuthDq.append(";");
		xAuthDq.append(reqTime);
		LOGGER.info("---- x-auth-dq ----{}",xAuthDq.toString());

		return xAuthDq.toString();
	}

}
