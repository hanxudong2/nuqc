package cn.js189.uqc.controller.telecomgroup;


import cn.js189.common.util.HttpUtil;
import cn.js189.common.util.StringUtils;
import cn.js189.uqc.service.IAreaBillService;
import cn.js189.uqc.service.IBssUserQueryService;
import com.alibaba.fastjson.JSONObject;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @program: xxb
 * @description:
 * @author: jzd
 * @create: 2024/2/28 10:31
 **/
@RestController
public class TelecomGroupController {
    
    @Resource
    private IAreaBillService areaBillService;
    @Resource
    private IBssUserQueryService bssUserQueryService;
    
    /**
     * @Description 集团地区编码查询
     * @Param
     * @Return {@link String}
     * @Author jzd
     * @Date 2024/2/28 10:32
     */
    //http://dqzt.telecomjs.com:8081/uqc_services/iAreaBill/callHnumberCountry
    @PostMapping("/iAreaBill/callHnumberCountry")
    public String callHnumberCountry(@RequestBody JSONObject inParamJson) throws Exception {
        String accNbr = inParamJson.getString("accNbr");//号码
        if(StringUtils.isEmpty(accNbr)){
            throw new Exception("号码不能为空");
        }
        //查询接口地址，请求头等信息
        String url= "";
        String headJson = "";
        int timeout = 0;
        url+=accNbr;
        String result = HttpUtil.sendPost(url,headJson,timeout,"");
        return result;
    }
    
    /**
     *  查询全国号码H码，从而查到lanId
     * @param reqInfo 入参
     * @return 结果
     */
    @PostMapping("/iAreaBill/callHnumberCountry")
    public String callHnumberCountry(@RequestBody String reqInfo) {
        return areaBillService.callNumberCountry(reqInfo);
    }

    
    /**
     * 限购策略校验
     * @param reqParams 入参
     * @return 结果
     */
    @PostMapping("/iBssSystem/checkPurchaseRestrictions")
    public JSONObject checkPurchaseRestrictions(@RequestBody String reqParams)
    {
        return bssUserQueryService.checkPurchaseRestrictions(reqParams);
    }
    
    /**
     * 拦截校验接口
     * @param reqParams 参数
     * @return 结果
     */
    @PostMapping("/iBssSystem/checkRcBlackList")
    public JSONObject checkRcBlackList(@RequestBody String reqParams){
        return bssUserQueryService.checkRcBlackList(reqParams);
    }
    
}
