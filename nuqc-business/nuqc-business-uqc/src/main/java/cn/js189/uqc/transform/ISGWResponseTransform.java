package cn.js189.uqc.transform;

import com.alibaba.fastjson.JSONObject;

public interface ISGWResponseTransform {

	JSONObject transform(String responseString);
}
