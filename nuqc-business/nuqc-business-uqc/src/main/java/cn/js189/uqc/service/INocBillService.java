package cn.js189.uqc.service;

/**
 * @Author yxl
 * @Date 2024/3/1
 */

public interface INocBillService {
    String addInvoiceInformation(String requestJson);

    String askUserSessionType(String requestJSON);

    String bussinessOcr(String request);

    String callAdslOrLanOnline(String param);

    String checkInvoiceInformation(String requestJson);

    String checkServSpecByDevModel(String request);

    String getAutoCheckResult(String request);

    String getCardInfoByNum(String request);

    String queryDevSN(String requestJSON1);

    String queryOrderDetail(String requestJSON1);

    String queryResAbility(String request);

    String queryServiceNum(String requestJSON1);

    String queryUserDevInfo(String requestJSON1);

    String stampDect(String request);

    String startAutoCheck(String request);

    String toIvrCheck(String request);

    String updateInvoiceInformation(String requestJson);

    String uploadOcrFtp(String request);
}
