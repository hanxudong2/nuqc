package cn.js189.system.service;


import cn.js189.system.api.domain.SysInterCount;
import cn.js189.system.api.domain.SysInterCountDto;
import cn.js189.system.api.domain.SysLogPlus;
import java.util.Map;

public interface ISysLogPlusService {


    /**
     * 保存日志信息
     * @param sysLogPlus 日志对象
     */
    void saveLog(SysLogPlus sysLogPlus);
    
    /**
     * 接口访问统计数据持久化
     * @param sysInterCount 接口统计对象
     */
    void saveOrEditData(SysInterCountDto sysInterCount);
    
    /**
     * 新增接口访问次数统计
     * @param batchMap 接口访问次数
     */
    void saveInterfaceCount(Map<String,SysInterCount> batchMap);
    
    /**
     * 修改接口访问次数统计
     * @param batchMap 接口统计对象
     */
    void editInterfaceCount(Map<String,SysInterCount> batchMap);

}
