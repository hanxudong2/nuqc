package cn.js189.system.controller;

import cn.js189.common.domain.AjaxResult;
import cn.js189.common.domain.TableDataInfo;
import cn.js189.system.api.domain.SysInterCount;
import cn.js189.system.api.domain.SysInterCountDto;
import cn.js189.system.api.domain.SysLogPlus;
import cn.js189.system.service.ISysLogPlusService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/sysLog")
public class SysLogPlusController {

    @Resource
    private ISysLogPlusService sysLogPlusService;

    @PostMapping("/saveLog")
    public AjaxResult saveLog(@RequestBody SysLogPlus sysLogPlus){
        sysLogPlusService.saveLog(sysLogPlus);
        return AjaxResult.success();
    }
    
    @PostMapping("/saveInterCount")
    public AjaxResult saveInterfaceCount(@RequestBody SysInterCountDto sysInterCount){
        sysLogPlusService.saveOrEditData(sysInterCount);
        return AjaxResult.success();
    }

}
