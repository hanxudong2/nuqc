package cn.js189.system.domain;

public class PubAddressIp {

	/**
	 * 接口编号，查询条件
	 */
	private String addreCode;
	
	/**
	 * 区号
	 */
	private String regionId;
	
	/**
	 * 区名
	 */
	private String regionName;
	
	/**
	 * ip地址
	 */
	private String ipAdd;
	
	/**
	 * 端口
	 */
	private String port;
	
	/**
	 * 接口路径，或者是ftp上传路径
	 */
	private String accessPath;
	
	/**
	 * 接口命名空间
	 */
	private String addNamespace;
	
	/**
	 * 用户名
	 */
	private String userName;
	
	/**
	 * 密码
	 */
	private String password;
	
	/**
	 * 超时时间
	 */
	private String timeOut;
	
	/**
	 * sysCode
	 */
	private String sysCode;
	
	/**
	 * addreDesc	
	 */
	private String addreDesc;

	/**
	 * @return the addreCode
	 */
	public String getAddreCode() {
		return addreCode;
	}

	/**
	 * @param addreCode the addreCode to set
	 */
	public void setAddreCode(String addreCode) {
		this.addreCode = addreCode;
	}

	/**
	 * @return the regionId
	 */
	public String getRegionId() {
		return regionId;
	}

	/**
	 * @param regionId the regionId to set
	 */
	public void setRegionId(String regionId) {
		this.regionId = regionId;
	}

	/**
	 * @return the regionName
	 */
	public String getRegionName() {
		return regionName;
	}

	/**
	 * @param regionName the regionName to set
	 */
	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

	/**
	 * @return the ipAdd
	 */
	public String getIpAdd() {
		return ipAdd;
	}

	/**
	 * @param ipAdd the ipAdd to set
	 */
	public void setIpAdd(String ipAdd) {
		this.ipAdd = ipAdd;
	}

	/**
	 * @return the port
	 */
	public String getPort() {
		return port;
	}

	/**
	 * @param port the port to set
	 */
	public void setPort(String port) {
		this.port = port;
	}

	/**
	 * @return the accessPath
	 */
	public String getAccessPath() {
		return accessPath;
	}

	/**
	 * @param accessPath the accessPath to set
	 */
	public void setAccessPath(String accessPath) {
		this.accessPath = accessPath;
	}

	/**
	 * @return the addNamespace
	 */
	public String getAddNamespace() {
		return addNamespace;
	}

	/**
	 * @param addNamespace the addNamespace to set
	 */
	public void setAddNamespace(String addNamespace) {
		this.addNamespace = addNamespace;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the timeOut
	 */
	public String getTimeOut() {
		return timeOut;
	}

	/**
	 * @param timeOut the timeOut to set
	 */
	public void setTimeOut(String timeOut) {
		this.timeOut = timeOut;
	}

	/**
	 * @return the sysCode
	 */
	public String getSysCode() {
		return sysCode;
	}

	/**
	 * @param sysCode the sysCode to set
	 */
	public void setSysCode(String sysCode) {
		this.sysCode = sysCode;
	}

	/**
	 * @return the addreDesc
	 */
	public String getAddreDesc() {
		return addreDesc;
	}

	/**
	 * @param addreDesc the addreDesc to set
	 */
	public void setAddreDesc(String addreDesc) {
		this.addreDesc = addreDesc;
	}
	
	
}
