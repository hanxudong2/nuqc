package cn.js189.system.mapper;

import cn.js189.system.api.domain.SysInterCount;
import cn.js189.system.api.domain.SysLogPlus;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SysLogPlusMapper{
	
	/**
	 * 保存日志信息
	 * @param sysLogPlus 日志信息
	 */
	void saveLog(SysLogPlus sysLogPlus);
	
	/**
	 * 新增日志次数统计
	 * @param list 接口统计实体
	 */
	void saveInterfaceCount(@Param("list") List<SysInterCount> list);
	
	/**
	 * 修改日志次数统计
	 * @param list 接口统计实体
	 */
	void editInterfaceCount(@Param("list") List<SysInterCount> list);
	
	/**
	 * 获取日志次数统计
	 * @param list 接口地址
	 */
	List<SysInterCount> getInterfaceCountByUrl(@Param("list") List<String> list);

}
