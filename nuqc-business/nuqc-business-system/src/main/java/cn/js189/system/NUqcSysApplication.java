package cn.js189.system;

import cn.js189.common.util.annotation.EnableCustomConfig;
import cn.js189.common.util.annotation.EnableNFeignClients;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Slf4j
@EnableCustomConfig
@EnableNFeignClients
@SpringBootApplication
public class NUqcSysApplication {

    public static void main(String[] args) {
        SpringApplication.run(NUqcSysApplication.class,args);
        log.info("系统服务启动成功......");
    }

}
