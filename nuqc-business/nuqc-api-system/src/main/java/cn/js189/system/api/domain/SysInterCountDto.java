package cn.js189.system.api.domain;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 接口访问次数统计
 */
@Data
public class SysInterCountDto implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private List<SysInterCount> sysInterCountList;
	
	public SysInterCountDto(){}
	
	public SysInterCountDto(List<SysInterCount> sysInterCountList){
		this.sysInterCountList = sysInterCountList;
	}

}
