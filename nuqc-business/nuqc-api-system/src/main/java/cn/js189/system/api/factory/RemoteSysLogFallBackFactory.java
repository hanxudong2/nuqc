package cn.js189.system.api.factory;

import cn.js189.common.domain.AjaxResult;
import cn.js189.common.domain.TableDataInfo;
import cn.js189.system.api.RemoteSysLogApi;
import cn.js189.system.api.domain.SysInterCount;
import cn.js189.system.api.domain.SysInterCountDto;
import cn.js189.system.api.domain.SysLogPlus;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

@Component
public class RemoteSysLogFallBackFactory  implements FallbackFactory<RemoteSysLogApi> {

    @Override
    public RemoteSysLogApi create(Throwable cause) {
        return new RemoteSysLogApi() {
            
            @Override
            public AjaxResult saveLog(SysLogPlus sysLogPlus) {
                return AjaxResult.error("新增日志信息失败！");
            }
            
            @Override
            public AjaxResult saveInterfaceCount(SysInterCountDto interCountList) {
                return AjaxResult.error("接口次数统计保存失败！");
            }
            
        };
    }
}
