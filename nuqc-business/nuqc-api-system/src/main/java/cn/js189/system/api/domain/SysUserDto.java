package cn.js189.system.api.domain;

import lombok.Data;

import java.io.Serializable;

@Data
public class SysUserDto implements Serializable {

    private static final long serialVersionUID = 1L;

    /** 用户id */
    private Long userId;

    /** 单位id */
    private Long deptId;

    /** 用户姓名 */
    private String userName;

    /** 昵称 */
    private String nickName;

    /** 用户类型 */
    private String userType;

    /** 邮箱 */
    private String email;

    /** 手机号 */
    private String phoneNumber;

    /** 性别 */
    private String sex;

    /** 头像 */
    private String avatar;

    /** 密码 */
    private String password;

    /** 状态 */
    private String status;

    /** 删除状态 */
    private String delFlag;

    /** 登录ip */
    private String loginIp;

    /** 登录日期 */
    private String loginDate;

    /** 备注 */
    private String remark;

}
