package cn.js189.system.api.domain;

import lombok.Data;
import java.io.Serializable;

/**
 * 日志收集实体
 */
@Data
public class SysLogPlus implements Serializable {

    private static final long serialVersionUID = 1L;

    private String logId;

    /** 响应码 */
    private Integer code;

    /** 客户端ip */
    private String ip;

    /** 请求方法 */
    private String method;

    /** 返回消息 */
    private String msg;

    /** 请求入参 */
    private String param;

    /** 请求路径 */
    private String path;

    /** 请求时间 */
    private String requestTime;

    /** 响应式时间 */
    private String responseTime;

    /** 返回结果 */
    private String result;

    /** 子接口信息 */
    private String logList;

    /** 方法执行时间 */
    private Long useTime;

}
