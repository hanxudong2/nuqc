package cn.js189.system.api.domain;

import lombok.Data;

import java.io.Serializable;

/**
 * 接口访问次数统计
 */
@Data
public class SysInterCount implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	/** id */
	private String id;
	
	/** 接口地址 */
	private String url;
	
	/** 接口访问次数 */
	private Long interCount;
	
	/** 渠道id */
	private String channelId;

	/** 创建时间 */
	private String createTime;
}
