package cn.js189.system.api;

import cn.js189.common.constants.ServerNameConstants;
import cn.js189.common.domain.AjaxResult;
import cn.js189.common.domain.TableDataInfo;
import cn.js189.system.api.domain.SysInterCount;
import cn.js189.system.api.domain.SysInterCountDto;
import cn.js189.system.api.domain.SysLogPlus;
import cn.js189.system.api.factory.RemoteSysLogFallBackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient(contextId = "remoteSysLogApi",value = ServerNameConstants.UQC_SYSTEM,fallback = RemoteSysLogFallBackFactory.class)
public interface RemoteSysLogApi {
    
    @PostMapping("/sysLog/saveLog")
    AjaxResult saveLog(@RequestBody SysLogPlus sysLogPlus);
    
    @PostMapping("/sysLog/saveInterCount")
    AjaxResult saveInterfaceCount(@RequestBody SysInterCountDto interCountList);
    
}
