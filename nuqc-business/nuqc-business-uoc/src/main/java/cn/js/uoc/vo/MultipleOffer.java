/**************************************************************************************** 

 ****************************************************************************************/
package cn.js.uoc.vo;

import java.io.Serializable;
import java.util.List;

/** 
 * <Description> 销售品实体<br> 
 *  
 * @author zhangpzh<br>
 * @version 1.0<br>
 * @taskId <br>
 * @CreateDate 2016年2月29日 <br>
 */

public class MultipleOffer implements Serializable {
    /**
     * 销售品ID
     */
    private String id;
    /**
     * 销售品属性
     */
    private List<Property> propertys;
   
    
    /**
     * 动作类型
     */
    private String action;
    
    /**
     * 销售品名称
     */
    private String name;
    
 

    
    private List<AccNumsInfo> accNumsInfo;



	public List<Property> getPropertys() {
		return propertys;
	}



	public void setPropertys(List<Property> propertys) {
		this.propertys = propertys;
	}



	public String getId() {
		return id;
	}



	public void setId(String id) {
		this.id = id;
	}



	public String getAction() {
		return action;
	}



	public void setAction(String action) {
		this.action = action;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public List<AccNumsInfo> getAccNumsInfo() {
		return accNumsInfo;
	}



	public void setAccNumsInfo(List<AccNumsInfo> accNumsInfo) {
		this.accNumsInfo = accNumsInfo;
	}

	
    
    
    
    
    
}
