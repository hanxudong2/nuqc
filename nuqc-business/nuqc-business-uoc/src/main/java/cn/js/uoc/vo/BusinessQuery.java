/**************************************************************************************** 

 ****************************************************************************************/
package cn.js.uoc.vo;

import java.util.List;


public class BusinessQuery extends BusinessBase {
    /**
     * 查询类型
     */
    private String queryType;
    
    /**
     * 业务类型
     */
    private Integer businessType;
    
    private Integer serviceType;

    /**
     * 查询模式
     */
    private String queryMode;
    //订单id 
    private String trandId;
    //开始时间
    private String startDate;
    //结束时间
    private String endDate;
    
    private String orderStatus;
    
    private String payStatus;
    
    private String faliReason;
    
    private String businessId;
    
    private String streamingNo; 
    
    private String srcDeviceType;
    
    private String srcDeviceID;
    
    private String userID;
    
    private String userIDType;
    
    private String telphone;
    
    private String flowType;
    
    private String flowNum;
    
    private String friendPhone;
    
    private String tradeNo;
    
    private String reqTime;
    
    private String flowCnt;
    
    private String flowPerNum;
    
    private String flowBonusNo;
    
    private String uniqueCode;
    
    private String seq;
    
    private String bonusType;
    
    private String date;
    
    private String type;
    
    private String puk;
    private String orderId;
    private String url;

    /**
     * 办理人证件号码
     */
    private String certNumber;
    
    private String offerSpecId;   
    
    private String token;
    
    //反馈触点 销售品中台是否配置，能否支持退订 销售品信息查询
    private List<String> productList;
    //宽带自助排障 检测信息查询（多机顶盒场景）
    private String sn;    
    private String oui; 
    private String netAccount;
    private String bandwidth;
   
    public List<String> getProductList() {
		return productList;
	}
	public void setProductList(List<String> productList) {
		this.productList = productList;
	}
	
	private String sourceId;
	 
	   
	public String getSourceId() {
			return sourceId;
		}
	public void setSourceId(String sourceId) {
			this.sourceId = sourceId;
		}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getTelphone() {
		return telphone;
	}
	public void setTelphone(String telphone) {
		this.telphone = telphone;
	}
	public String getFlowType() {
		return flowType;
	}
	public void setFlowType(String flowType) {
		this.flowType = flowType;
	}
	public String getFlowNum() {
		return flowNum;
	}
	public void setFlowNum(String flowNum) {
		this.flowNum = flowNum;
	}
	public String getFriendPhone() {
		return friendPhone;
	}
	public void setFriendPhone(String friendPhone) {
		this.friendPhone = friendPhone;
	}
	public String getTradeNo() {
		return tradeNo;
	}
	public void setTradeNo(String tradeNo) {
		this.tradeNo = tradeNo;
	}
	public String getReqTime() {
		return reqTime;
	}
	public void setReqTime(String reqTime) {
		this.reqTime = reqTime;
	}
	public String getFlowCnt() {
		return flowCnt;
	}
	public void setFlowCnt(String flowCnt) {
		this.flowCnt = flowCnt;
	}
	public String getFlowPerNum() {
		return flowPerNum;
	}
	public void setFlowPerNum(String flowPerNum) {
		this.flowPerNum = flowPerNum;
	}
	public String getFlowBonusNo() {
		return flowBonusNo;
	}
	public void setFlowBonusNo(String flowBonusNo) {
		this.flowBonusNo = flowBonusNo;
	}
	public String getUniqueCode() {
		return uniqueCode;
	}
	public void setUniqueCode(String uniqueCode) {
		this.uniqueCode = uniqueCode;
	}
	public String getSeq() {
		return seq;
	}
	public void setSeq(String seq) {
		this.seq = seq;
	}
	public String getBonusType() {
		return bonusType;
	}
	public void setBonusType(String bonusType) {
		this.bonusType = bonusType;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getStreamingNo() {
		return streamingNo;
	}
	public void setStreamingNo(String streamingNo) {
		this.streamingNo = streamingNo;
	}
	public String getSrcDeviceType() {
		return srcDeviceType;
	}
	public void setSrcDeviceType(String srcDeviceType) {
		this.srcDeviceType = srcDeviceType;
	}
	public String getSrcDeviceID() {
		return srcDeviceID;
	}
	public void setSrcDeviceID(String srcDeviceID) {
		this.srcDeviceID = srcDeviceID;
	}
	public String getUserID() {
		return userID;
	}
	public void setUserID(String userID) {
		this.userID = userID;
	}
	public String getUserIDType() {
		return userIDType;
	}
	public void setUserIDType(String userIDType) {
		this.userIDType = userIDType;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
	public Integer getServiceType() {
		return serviceType;
	}
	public void setServiceType(Integer serviceType) {
		this.serviceType = serviceType;
	}
	/**
     * get queryType
     * 
     * @return Returns the queryType.<br>
     */
    public String getQueryType() {
        return queryType;
    }

    /**
     * set queryType
     * 
     * @param queryType The queryType to set. <br>
     */
    public void setQueryType(String queryType) {
        this.queryType = queryType;
    }

    /**
     * get queryMode
     * 
     * @return Returns the queryMode.<br>
     */
    public String getQueryMode() {
        return queryMode;
    }

    /**
     * set queryMode
     * 
     * @param queryMode The queryMode to set. <br>
     */
    public void setQueryMode(String queryMode) {
        this.queryMode = queryMode;
    }

    /** 
     * get businessType
     * @return Returns the businessType.<br> 
     */
    public Integer getBusinessType() {
        return businessType;
    }

    /** 
     * set businessType
     * @param businessType The businessType to set. <br>
     */
    public void setBusinessType(Integer businessType) {
        this.businessType = businessType;
    }

	public String getTrandId() {
		return trandId;
	}

	public void setTrandId(String trandId) {
		this.trandId = trandId;
	}
	public String getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	public String getPayStatus() {
		return payStatus;
	}
	public void setPayStatus(String payStatus) {
		this.payStatus = payStatus;
	}
	public String getFaliReason() {
		return faliReason;
	}
	public void setFaliReason(String faliReason) {
		this.faliReason = faliReason;
	}
	public String getBusinessId() {
		return businessId;
	}
	public void setBusinessId(String businessId) {
		this.businessId = businessId;
	}
	public String getPuk() {
		return puk;
	}
	public void setPuk(String puk) {
		this.puk = puk;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getCertNumber() {
		return certNumber;
	}
	public void setCertNumber(String certNumber) {
		this.certNumber = certNumber;
	}
	public String getOfferSpecId() {
		return offerSpecId;
	}
	public void setOfferSpecId(String offerSpecId) {
		this.offerSpecId = offerSpecId;
	}
    public String getToken()
    {
        return token;
    }
    public void setToken(String token)
    {
        this.token = token;
    }
	public String getSn() {
		return sn;
	}
	public void setSn(String sn) {
		this.sn = sn;
	}
	public String getOui() {
		return oui;
	}
	public void setOui(String oui) {
		this.oui = oui;
	}
	public String getNetAccount() {
		return netAccount;
	}
	public void setNetAccount(String netAccount) {
		this.netAccount = netAccount;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getBandwidth() {
		return bandwidth;
	}
	public void setBandwidth(String bandwidth) {
		this.bandwidth = bandwidth;
	}
}
