package cn.js.uoc.vo;

import java.io.Serializable;

/**
 * <Description> 产品信息<br>
 *
 * @author gbg<br>
 * @version 1.0<br>
 * @taskId <br>
 * @CreateDate 2022年10月21日 <br>
 */

public class Product implements Serializable {

    /**
     * 产品ID
     */
    private String prodInstId;

    /**
     * 号码
     */
    private String accNum;

    /**
     * 联系人名称
     */
    private String contactName;

    /**
     * 联系人号码
     */
    private String contactPhone;

    /**
     * 产品标识
     */

    private String prodId;

    /**
     * 联系信息状态
     */
    private String statusCd;

    /**
     * 四级地区号
     */
    private String regionId;

    public String getProdInstId() {
        return prodInstId;
    }

    public void setProdInstId(String prodInstId) {
        this.prodInstId = prodInstId;
    }

    public String getAccNum() {
        return accNum;
    }

    public void setAccNum(String accNum) {
        this.accNum = accNum;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public String getProdId() {
        return prodId;
    }

    public void setProdId(String prodId) {
        this.prodId = prodId;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }

    public String getRegionId() {
        return regionId;
    }

    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }
}
