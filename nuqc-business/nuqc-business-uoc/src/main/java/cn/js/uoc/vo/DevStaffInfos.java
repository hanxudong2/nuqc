package cn.js.uoc.vo;

import java.io.Serializable;

public class DevStaffInfos implements Serializable {
    // 协销人标识
    private String devStaffCode;
    // 协销人类型
    private String devStaffType;

    public DevStaffInfos() {
    }

    public DevStaffInfos(String devStaffId, String devStaffType) {
        this.devStaffCode = devStaffId;
        this.devStaffType = devStaffType;
    }

    public String getDevStaffCode() {
        return devStaffCode;
    }

    public void setDevStaffCode(String devStaffCode) {
        this.devStaffCode = devStaffCode;
    }

    public String getDevStaffType() {
        return devStaffType;
    }

    public void setDevStaffType(String devStaffType) {
        this.devStaffType = devStaffType;
    }

    @Override
    public String toString() {
        return "DevStaffInfos{" +
                "devStaffId='" + devStaffCode + '\'' +
                ", devStaffType='" + devStaffType + '\'' +
                '}';
    }
}
