/**************************************************************************************** 

 ****************************************************************************************/
package cn.js.uoc.vo;

import java.io.Serializable;

/** 
 * <Description> 销售品属性<br> 
 *  
 * @author zhangpzh<br>
 * @version 1.0<br>
 * @taskId <br>
 * @CreateDate 2016年3月1日 <br>
 */

public class Property implements Serializable {
    /**
     * 属性ID
     */
    private String propertyId;
    /**
     * 属性值
     */
    private String propertyValue;
    /**
     * 属性关键字
     */
    private String propertyKey;
    
    /**
     * 属性名称
     */
    private String propertyName;
    
    
    /**
     * 属性类型,1-接入类产品属性； 2-功能类产品属性； 17-接入类产品订单属性；18-功能类产品订单属性；3-销售品属性
     */
    private String busiTypeId;


	public String getPropertyId() {
		return propertyId;
	}


	public void setPropertyId(String propertyId) {
		this.propertyId = propertyId;
	}


	public String getPropertyValue() {
		return propertyValue;
	}


	public void setPropertyValue(String propertyValue) {
		this.propertyValue = propertyValue;
	}


	public String getPropertyKey() {
		return propertyKey;
	}


	public void setPropertyKey(String propertyKey) {
		this.propertyKey = propertyKey;
	}


	public String getPropertyName() {
		return propertyName;
	}


	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}


	public String getBusiTypeId() {
		return busiTypeId;
	}


	public void setBusiTypeId(String busiTypeId) {
		this.busiTypeId = busiTypeId;
	}
    

   
}
