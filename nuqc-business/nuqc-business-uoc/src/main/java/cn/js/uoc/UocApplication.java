package cn.js.uoc;

import cn.js189.common.util.annotation.EnableCustomConfig;
import cn.js189.common.util.annotation.EnableNFeignClients;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@Slf4j
@EnableCustomConfig
@EnableNFeignClients
@SpringBootApplication
public class UocApplication {

    public static void main(String[] args) {
        SpringApplication.run(UocApplication.class,args);
        log.info("受理域域服务启动成功......");
    }

}
