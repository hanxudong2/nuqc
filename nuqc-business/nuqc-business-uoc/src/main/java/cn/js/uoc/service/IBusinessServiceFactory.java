package cn.js.uoc.service;


import cn.js.uoc.vo.BusinessAcceptance;
import cn.js.uoc.vo.BusinessQuery;
import cn.js189.core.factory.MatchingBean;
import cn.js189.core.vo.ResponseVo;

/**
 * 
 * <Description> 业务受理工厂接口<br>
 */
public interface IBusinessServiceFactory extends MatchingBean<Integer> {
    /**
     * Description: <br>
     * @param businessQuery 查询实体
     * @return <br>
     */
    public ResponseVo<Object> query(BusinessQuery businessQuery);
    /**
     * 
     * Description: <br> 
     *  
     * @author zhangpzh<br>
     * @taskId <br>
     * @param businessAcceptance 请求实体
     * @return <br>
     */
    public ResponseVo<Object> check(BusinessAcceptance businessAcceptance);
    /**
     * 
     * Description: <br> 
     *  
     * @author zhangpzh<br>
     * @taskId <br>
     * @param businessAcceptance 请求实体
     * @return <br>
     */
    public ResponseVo<Object> acceptance(BusinessAcceptance businessAcceptance);
}
