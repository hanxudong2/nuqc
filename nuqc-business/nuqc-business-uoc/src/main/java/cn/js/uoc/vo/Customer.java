/**************************************************************************************** 

 ****************************************************************************************/
package cn.js.uoc.vo;

import java.io.Serializable;


/** 
 * <Description> 客户实体<br> 
 *  
 * @author 林盛业<br>
 * @version 1.0<br>
 * @taskId <br>
 * @CreateDate 2020年5月15日 <br>
 */

public class Customer implements Serializable {
	//基本信息类
    private String custAddr;
    private String custName;
    /*
     *CUS-0001	客户战略分群	1000政企客户；1100公众客户
     */
    private String custType;
/*
 *   记录参与人的类型。LOVB=PTY-0001。partyType=2组织（政企客户）时，经办人、责任人、联系人这3项客户属性必填
 *   PTY-0001	参与人类型	1	个人
 *   PTY-0001	参与人类型	2	组织
 */
    private String partyType;
    //联系信息类
    private String contactAddr;
    private String contactName;
    //证件信息类
    private String certAddr;
    private String certNum;
    /*
     * 描述参与人证件的类型。LOVB=PTY-0004。1	居民身份证
     */
    private String certType;
    /*
     * 是否默认证件, 1是；0不是 
     */
    private String isDefault;
    
    
	public String getCustAddr() {
		return custAddr;
	}
	public void setCustAddr(String custAddr) {
		this.custAddr = custAddr;
	}
	public String getCustName() {
		return custName;
	}
	public void setCustName(String custName) {
		this.custName = custName;
	}
	public String getCustType() {
		return custType;
	}
	public void setCustType(String custType) {
		this.custType = custType;
	}
	public String getPartyType() {
		return partyType;
	}
	public void setPartyType(String partyType) {
		this.partyType = partyType;
	}
	public String getContactAddr() {
		return contactAddr;
	}
	public void setContactAddr(String contactAddr) {
		this.contactAddr = contactAddr;
	}
	public String getContactName() {
		return contactName;
	}
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	public String getCertAddr() {
		return certAddr;
	}
	public void setCertAddr(String certAddr) {
		this.certAddr = certAddr;
	}
	public String getCertNum() {
		return certNum;
	}
	public void setCertNum(String certNum) {
		this.certNum = certNum;
	}
	public String getCertType() {
		return certType;
	}
	public void setCertType(String certType) {
		this.certType = certType;
	}
	public String getIsDefault() {
		return isDefault;
	}
	public void setIsDefault(String isDefault) {
		this.isDefault = isDefault;
	}
    
    
    

    


    
    
    
}
