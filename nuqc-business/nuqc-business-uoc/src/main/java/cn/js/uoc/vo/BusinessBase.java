/**************************************************************************************** 

 ****************************************************************************************/
package cn.js.uoc.vo;

import java.io.Serializable;

/** 
 * <Description> <br> 
 *  
 * @author zhangpzh<br>
 * @version 1.0<br>
 * @taskId <br>
 * @CreateDate 2016年2月29日 <br>
 */

public class BusinessBase implements Serializable{
    /**
     * 来源
     */
    private String appId;
    /**
     * 受理渠道
     */
    private String channelId;
    /**
     * 受理工号
     */
    private String staffCode;
    /**
     * 接入号
     */
    private String accNbr;
    /**
     * 接入类型
     */
    private Integer accType;
    /**
     * 业务受理类型
     */
    private String areaCode;
    
    //受理的号码
    private String acceptPhone;
    
    /** 
     * get channelId
     * @return Returns the channelId.<br> 
     */
    public String getChannelId() {
        return channelId;
    }
    /** 
     * set channelId
     * @param channelId The channelId to set. <br>
     */
    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }
    
    /** 
     * get staffCode
     * @return Returns the staffCode.<br> 
     */
    public String getStaffCode() {
        return staffCode;
    }
    /** 
     * set staffCode
     * @param staffCode The staffCode to set. <br>
     */
    public void setStaffCode(String staffCode) {
        this.staffCode = staffCode;
    }
    /** 
     * get accNbr
     * @return Returns the accNbr.<br> 
     */
    public String getAccNbr() {
        return accNbr;
    }
    /** 
     * set accNbr
     * @param accNbr The accNbr to set. <br>
     */
    public void setAccNbr(String accNbr) {
        this.accNbr = accNbr;
    }
    /** 
     * get accType
     * @return Returns the accType.<br> 
     */
    public Integer getAccType() {
        return accType;
    }
    /** 
     * set accType
     * @param accType The accType to set. <br>
     */
    public void setAccType(Integer accType) {
        this.accType = accType;
    }
    /** 
     * get areaCode
     * @return Returns the areaCode.<br> 
     */
    public String getAreaCode() {
        return areaCode;
    }
    /** 
     * set areaCode
     * @param areaCode The areaCode to set. <br>
     */
    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }
    /** 
     * get appId
     * @return Returns the appId.<br> 
     */
    public String getAppId() {
        return appId;
    }
	public void setAppId(String appId) {
		this.appId = appId;
	}
	public String getAcceptPhone() {
		return acceptPhone;
	}
	public void setAcceptPhone(String acceptPhone) {
		this.acceptPhone = acceptPhone;
	}
}
