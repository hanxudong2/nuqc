/**************************************************************************************** 

 ****************************************************************************************/
package cn.js.uoc.vo;

import java.io.Serializable;

/** 
 * <Description> 销售品实体<br> 
 *  
 * @author zhangpzh<br>
 * @version 1.0<br>
 * @taskId <br>
 * @CreateDate 2016年2月29日 <br>
 */

public class AccNumsInfo implements Serializable {
   private String accNum;
   
   private String accType;
   
   private String roleId;
   
   private String prodInstPwd;
   
   

public String getAccNum() {
	return accNum;
}

public void setAccNum(String accNum) {
	this.accNum = accNum;
}

public String getAccType() {
	return accType;
}

public void setAccType(String accType) {
	this.accType = accType;
}

public String getRoleId() {
	return roleId;
}

public void setRoleId(String roleId) {
	this.roleId = roleId;
}

public String getProdInstPwd() {
	return prodInstPwd;
}

public void setProdInstPwd(String prodInstPwd) {
	this.prodInstPwd = prodInstPwd;
}
   
   
}
