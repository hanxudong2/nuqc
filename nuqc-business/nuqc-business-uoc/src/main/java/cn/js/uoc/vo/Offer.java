/**************************************************************************************** 

 ****************************************************************************************/
package cn.js.uoc.vo;

import java.io.Serializable;
import java.util.List;
import java.util.Map;


/** 
 * <Description> 销售品实体<br> 
 *  
 * @author zhangpzh<br>
 * @version 1.0<br>
 * @taskId <br>
 * @CreateDate 2016年2月29日 <br>
 */

public class Offer implements Serializable {
    /**
     * 销售品ID
     */
    private String id;
    
    /**
     * 销售品下纳入的功能产品规格ID集合（一般是200开头的集合）
     */
    private List<String> prodIds;
    /**
     * 销售品类型
     */
    private Integer type;
    
    /**
     * 动作类型
     */
    private Integer action;
    
    /**
     * 销售品名称
     */
    private String name;
    
    /**
     * 销售品下功能产品规格所对应的状态编码，描述销售品下配置的每个功能是否已经被开通
     */
    private Map<String,String> prodStatusCds;
    
    /**
     * 销售品下功能产品规格所对应的状态描述
     */
    private Map<String,String> prodStatusNames;
    
    
    /**
     * 销售品的状态编码，描述销售品是否已经被订购
     */
    private String statusCd;
    
    /**
     * 销售品的状态描述
     */
    private String statusName;
    
    
    /**
     * 生效时间
     */
    private String effDate;
    
    
    /**
     * 失效时间
     */
    private String expDate;
    
    /**
     * 是否可退订
     */
    private Integer isUnsub;
    
    /**
     * 销售品-产品角色（记录产品纳入于销售品时 所设置的角色）
     */
    private Map<String,String> roleIds;
    
    /**
     * 销售品属性
     */
    private List<Property> propertys;
    
    /**
     * 销售品订购后，如果同时产生功能产品，此字段记录功能产品实例ID，一般用于功能类300销售品退订时关闭功能节点使用
     * 部分功能类销售品会开通多个功能，会有多个功能产品实例ID
     */
    private Map<String,String> prodInstIds;

    /**
     * 销售品实例ID，销售品订购后，非智慧bss地市肯定会产生销售品实例ID，
     * 智慧bss地市除部分功能类销售品只会产生功能产品实例外，大部分均会产生销售品实例
     */
    private String offerInstId;
    
    private List<AccNumsInfo> accNumsInfo;
    
    /**
     * 订购数目，指当前销售品订购次数，20200325 5G国漫权益业务首次使用
     */
    private String count;
    
    /**
     * 权益编码，指当前权益销售品所属的权益大类编码，20200325 5G国漫权益业务首次使用
     */
    private String vipCode;

    /**
     * 记录销售品标识归属的销售品类型，冗余存储
     */
    private String offerType;
    
    /**
     * 记录销售品系统分类,记录内部的系统分类，
     * 如可选包分单接入、多接入等对销售品类型更进一步的划分。LOVB=OFF-C-0064
     */
    private String offerSysType;
    
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<String> getProdIds() {
		return prodIds;
	}

	public void setProdIds(List<String> prodIds) {
		this.prodIds = prodIds;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getAction() {
		return action;
	}

	public void setAction(Integer action) {
		this.action = action;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Map<String, String> getProdStatusCds() {
		return prodStatusCds;
	}

	public void setProdStatusCds(Map<String, String> prodStatusCds) {
		this.prodStatusCds = prodStatusCds;
	}

	public Map<String, String> getProdStatusNames() {
		return prodStatusNames;
	}

	public void setProdStatusNames(Map<String, String> prodStatusNames) {
		this.prodStatusNames = prodStatusNames;
	}

	public String getStatusCd() {
		return statusCd;
	}

	public void setStatusCd(String statusCd) {
		this.statusCd = statusCd;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public String getEffDate() {
		return effDate;
	}

	public void setEffDate(String effDate) {
		this.effDate = effDate;
	}

	public String getExpDate() {
		return expDate;
	}

	public void setExpDate(String expDate) {
		this.expDate = expDate;
	}

	public Integer getIsUnsub() {
		return isUnsub;
	}

	public void setIsUnsub(Integer isUnsub) {
		this.isUnsub = isUnsub;
	}

	public Map<String, String> getRoleIds() {
		return roleIds;
	}

	public void setRoleIds(Map<String, String> roleIds) {
		this.roleIds = roleIds;
	}

	public List<Property> getPropertys() {
		return propertys;
	}

	public void setPropertys(List<Property> propertys) {
		this.propertys = propertys;
	}

	public Map<String, String> getProdInstIds() {
		return prodInstIds;
	}

	public void setProdInstIds(Map<String, String> prodInstIds) {
		this.prodInstIds = prodInstIds;
	}

	public String getOfferInstId() {
		return offerInstId;
	}

	public void setOfferInstId(String offerInstId) {
		this.offerInstId = offerInstId;
	}

	public List<AccNumsInfo> getAccNumsInfo() {
		return accNumsInfo;
	}

	public void setAccNumsInfo(List<AccNumsInfo> accNumsInfo) {
		this.accNumsInfo = accNumsInfo;
	}

	public String getCount() {
		return count;
	}

	public void setCount(String count) {
		this.count = count;
	}

	public String getVipCode() {
		return vipCode;
	}

	public void setVipCode(String vipCode) {
		this.vipCode = vipCode;
	}

	public String getOfferType() {
		return offerType;
	}

	public void setOfferType(String offerType) {
		this.offerType = offerType;
	}

	public String getOfferSysType() {
		return offerSysType;
	}

	public void setOfferSysType(String offerSysType) {
		this.offerSysType = offerSysType;
	}
    
    
    
    
}
