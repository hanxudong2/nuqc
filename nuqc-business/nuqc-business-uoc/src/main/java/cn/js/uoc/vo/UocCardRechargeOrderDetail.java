package cn.js.uoc.vo;

import java.io.Serializable;

public class UocCardRechargeOrderDetail implements Serializable {
	private static final long serialVersionUID = 4935902674028030364L;
	// 主键 uuid
	private String detailId;
	// order主表的主键
	private String id;
	// 充值请求流水(一次充值卡充值唯一)
	private String requestId;
	// 充值卡卡密
	private String cardPin;
	// 充值子流水
	private String subId;
	// 充值地市区号
	private String areaCode;
	// 充值卡充值号码类型 0-固话, 2-手机, 3-宽带, 6-翼支付帐号, 54-学子E行, 55-C+W账号
	// 流量卡充值号码类型 58-手机, 59-C+W账号
	private String destinationAttr;
	// 充值号码
	private String destinationId;
	// 实际充值号码, 针对宽带充值, 镇江和常州需要传递接入号; 宽带需要加传区号
	private String accNbr;
	// 充值账本类型, 默认值:0
	private String balanceType;
	// 号码类型, 5BA-账户; 5BB-客户; 5BC-用户, 默认值:5BA
	//新协议：20210804去O上云：充值级别，1：帐户级；3：用户级（原5BA、5BB对应1，原5BC对应3）
	private String objType;
	// 充值金额数量（卡分拆充值时使用，其他情况填缺省值0）, 默认值:0
	private String rechargeAmount;
	// 充值金额单位:0-分,2-KB
	private String rechargeUnit;
	// 单次充值内部序号
	private int sequence;
	// 状态 0-初始状态, 1-充值成功, 2-充值失败
	private int status;
	// 创建时间
	private String createTime;
	// 更新时间
	private String updateTime;
	// 错误编码
	private String errorCode;
	// 错误信息
	private String errorMsg;
	//被充值用户属性明细:普通宽带   999 ，企业闪讯   103  默认填 999就可以，集团规范定义的扩展字段
	private String destinationAttrDetail;

	public String getDetailId() {
		return detailId;
	}

	public void setDetailId(String detailId) {
		this.detailId = detailId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getCardPin() {
		return cardPin;
	}

	public void setCardPin(String cardPin) {
		this.cardPin = cardPin;
	}

	public String getSubId() {
		return subId;
	}

	public void setSubId(String subId) {
		this.subId = subId;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getDestinationAttr() {
		return destinationAttr;
	}

	public void setDestinationAttr(String destinationAttr) {
		this.destinationAttr = destinationAttr;
	}

	public String getDestinationId() {
		return destinationId;
	}

	public void setDestinationId(String destinationId) {
		this.destinationId = destinationId;
	}

	public String getAccNbr() {
		return accNbr;
	}

	public void setAccNbr(String accNbr) {
		this.accNbr = accNbr;
	}

	public String getBalanceType() {
		return balanceType;
	}

	public void setBalanceType(String balanceType) {
		this.balanceType = balanceType;
	}

	public String getObjType() {
		return objType;
	}

	public void setObjType(String objType) {
		this.objType = objType;
	}

	public String getRechargeAmount() {
		return rechargeAmount;
	}

	public void setRechargeAmount(String rechargeAmount) {
		this.rechargeAmount = rechargeAmount;
	}

	public String getRechargeUnit() {
		return rechargeUnit;
	}

	public void setRechargeUnit(String rechargeUnit) {
		this.rechargeUnit = rechargeUnit;
	}

	public int getSequence() {
		return sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public String getDestinationAttrDetail() {
		return destinationAttrDetail;
	}

	public void setDestinationAttrDetail(String destinationAttrDetail) {
		this.destinationAttrDetail = destinationAttrDetail;
	}

}
