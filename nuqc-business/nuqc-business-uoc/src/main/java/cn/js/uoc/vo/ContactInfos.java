package cn.js.uoc.vo;

import java.io.Serializable;

public class ContactInfos implements Serializable {
    private static final long serialVersionUID = 5077893242306618659L;
    // 联系人姓名
    private String contactName;
    // 联系人电话
    private String contactPhone;

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    @Override
    public String toString() {
        return "ContactInfos{" +
                "contactName='" + contactName + '\'' +
                ", contactPhone='" + contactPhone + '\'' +
                '}';
    }
}
