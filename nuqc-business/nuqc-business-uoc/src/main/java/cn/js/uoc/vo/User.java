package cn.js.uoc.vo;

import java.io.Serializable;

public class User implements Serializable {
    private String telNum;
    
    private String cornet;
    
    private int userType;
    
    private String psName;
    
    private String prodId;
    
    private  String failReason;

    public String getTelNum()
    {
        return telNum;
    }

    public void setTelNum(String telNum)
    {
        this.telNum = telNum;
    }

    public String getCornet()
    {
        return cornet;
    }

    public void setCornet(String cornet)
    {
        this.cornet = cornet;
    }

    public int getUserType()
    {
        return userType;
    }

    public void setUserType(int userType)
    {
        this.userType = userType;
    }

    public String getPsName()
    {
        return psName;
    }

    public void setPsName(String psName)
    {
        this.psName = psName;
    }

    public String getProdId()
    {
        return prodId;
    }

    public void setProdId(String prodId)
    {
        this.prodId = prodId;
    }

    public String getFailReason()
    {
        return failReason;
    }

    public void setFailReason(String failReason)
    {
        this.failReason = failReason;
    }
}
