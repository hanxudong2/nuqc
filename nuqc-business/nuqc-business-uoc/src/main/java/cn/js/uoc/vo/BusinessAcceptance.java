/****************************************************************************************

 ****************************************************************************************/
package cn.js.uoc.vo;

import com.alibaba.fastjson.JSONArray;

import java.util.Date;
import java.util.List;

/**
 * <Description> 业务受理实体 <br>
 *
 * @author zhangpzh<br>
 * @version 1.0<br>
 * @taskId <br>
 * @CreateDate 2016年2月29日 <br>
 */

public class BusinessAcceptance extends BusinessBase {
	/**
	 * 计费渠道编码
	 */
	private String billChannel;

	/**
	 * 交易流水号
	 */
	private String trandId;

	/**
	 * 渠道密码
	 */
	private String channelPasswd;

	/**
	 * 业务受理类型
	 */
	private Integer businessType;

	/**
	 * 业务受理动作
	 */
	private Integer businessAction;

	private String recommendPerson;

	private String recommendCode;

	private String recommendId;

	private String interfacetype;// 接口类型
	private String username;// 用户名
	private String cityid;// 城市id
	private String spid;
	private String serviceid;// 服务id
	private String servicename;// 服务名称
	private String consumedate;// 消费日期
	private String spaccount;//
	private String feetype;// 费用类型
	private String fee;// 费用
	private String begintime;// 开始时间
	private String endtime;// 结束时间
	//	private String timestamp;// 时间戳
	private String ip;// ip地址
	private String reserved1;
	private String accountType;// 账户类型
	private String customerid;// 客户id
	private String spcode;//
	private String sppassword;
	private String port;// 端口
	private String policycode;// 政策吗

	private String serverip;// 服务器ip

	private String type;// type

	private String authenticator;

	private String openType;
	// 用户id
	private String userId;

//	private String ordertimelen;

	private String param;

	private String acctName;

	private String indentNbr;

	private String linkNum="";

	private String linkName="";

	private String toxml;
	// 宽带帐号 接入号
	private String netAcct;
	// 用户IP
	private String visitIp;
	// 用户端口（用户访问你们网页的宽口号） Y
	private String userPort;

	private String serviceType;
	// //定购时长 单位分钟
	private String orderTimeLen;

	private String unsubscribe;

	private String flag;

	private String maxRate;

	private String startTime;

	private String stopTime;

	// //用户下达的使用策略：0开始提速；1停止提速
	private String operType;
	// 实际提速后的带宽值
	private String effectSpeed;

	private String querymonth;
	// 家庭宽带IP
	private String vnetSpid;
	private String clientIP;
	private String timeStamp;
	private String times;
	// 渠道号
	private String chanal;
	// '0','上行','1','下行','2','上行+下行 免费提速'
	private String prodType;

	private String operationType="";

	/**
	 * 故障id
	 */
	private String operationCode="";

	/**
	 * 故障名称
	 */
	private String operationName="";

	private String accessType;

	private String reqAmount;

	private String reqSerail;

	private String reqSource;

	private String rechargeInfoList;

	private String memberAreaCode;

	private List<User> partyUsers;

	private String reason;

	private String token;

	private String sign;

	private Date invokeStart = new Date();
	private Date invokeEnd = new Date();

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	private String ext1;

	private String ext2;

	private String ext3;

	// vsop 回调 更新状态，1，表示成功，0表示失败
	private String status;
	// vsop 回调更新状态。
	private String errorMsg;
	private String actionCode;// 开通VIP用于区分不同的VIP


	/**
	 * 销售品集合
	 */
	private List<Offer> offers;

	/**
	 * 订单信息集合
	 */
	private List<MultipleOffer> multipleOffers;


	/**
	 * 购物车id
	 */
	private String olId;

	/**
	 * 购物车流水号
	 */
	private String olNbr;

	/**
	 * 分销编码
	 */
	private String coMarketCode;

	/**
	 * 分销Id
	 */
	private String coMarketId;

	public String getCoMarketCode2() {
		return coMarketCode2;
	}

	public void setCoMarketCode2(String coMarketCode2) {
		this.coMarketCode2 = coMarketCode2;
	}

	private String coMarketCode2;


	/**
	 * 产品ID
	 */
	private String productId;

	/**
	 * 请求IP
	 */
	private String remoteIp;

	/**
	 * 支付方式
	 */
	private String payMethod;

	/**
	 *
	 */
	private String billCode;
	/**
	 * 用户id
	 */
//	private String userID;
	/**
	 * 用户类型
	 */
	private String userIDType;
	/**
	 * 订单编号
	 */
	private String billId;

	/**
	 * 办理人身份姓名
	 */
	private String certName;

	/**
	 * 办理人证件号码
	 */
	private String certNumber;
	/**
	 * 是否换卡
	 */
	private Boolean changeUim;
	/**
	 * 卡类型
	 */
	private String uimCode;

	/**
	 * 单位
	 */
	private String amount;

	private String payerWaitTime;
	private String payerName;
	private String payerAreaNo;
	private String payerAddress;
	private String payerPhone;
	private String payerOtherPhone;
	private String payerRemark;
	private String payerZipCode;
	private String receiverName;
	private String receiverAreaNo;
	private String receiverAddress;
	private String receiverWaitTime;
	private String receiverPhone;
	private String receiverOtherPhone;
	private String receiverZipCode;
	private String createTime;
	private String receiverRemark;
	private String invoiceContent;
	private String remark;
	private transient List items;

	private String orderId;
	private String billState;
	private String upTranSeq;
	private String retncode;
	private String payType;

	private String payCode;
	private String payMoney;
	private String tranDate;
	private String deliveMoney;

	private String streamingNo;
	private String srcDeviceType;
	private String srcDeviceID;
	private String oa;
	private String oatype;
	private String da;
	private String datype;
	private String id;
	private String idType;
	private String chanelPlayerID;
	private String eventID;
	private String fa;
	private String faType;

	private String regionid;// 地域ID
	private String reaionname;// 地域名
	//	private String prodtype;// 产品类型
	private String prodnum;// 产品号码
	private String custId;// 客户姓名
	//	private String custname;// 客户姓名
//	private String servicetype;// 服务类型
	private String servicetypedesc;// 服务类型名
	private String relaman;// 联系人
	private String relainfo;// 联系信息
	private String acceptchanneldesc="";// 受理渠道名
	private String acceptcomefrom;// 受理来源
	private String acceptcomefromdesc;// 受理来源名
	private String acceptstaffname;// 受理员工名
	private String acceptorgid;// 受理部门
	private String acceptorgname;// 受理部门名
	private String urgencygrade;// 紧急程度
	private String urgencygradedesc;// 紧急程度名
	private String custemail;// 邮件号码
	private String servicedate;// 流程号
	private String servicedatedesc;// 流程名
	private String isowner;
	private String morerelainfo;
	private String custemotion;
	private String custemotiondesc;
	private String appealprodid;// 一级目录
	private String appealprodname;// 一级目录名
	private String appealreasonid;// 二级目录
	private String appealreasondesc;// 二级目录名
	private String appealchild;// 三级目录
	private String appealchilddesc;// 三级目录名
	private String fougradecatalog;// 四级目录
	private String fougradecatalogdesc;// 四级目录名
	private String appealdetailid;// 申诉分类
	private String appealdetaildesc;// 申诉分类名
	private String acceptcontent;
	private String acceptstaffid;// 受理员工工号

	private String telphone;
	private String flowType;
	private String flowNum;
	private String friendPhone;
	private String tradeNo;
	private String reqTime;
	private String flowCnt;
	private String flowPerNum;
	private String flowBonusNo;
	private String uniqueCode;
	private String seq;
	private String bonusType;
	private String date;
	private String startDate;
	private String endDate;

	private String accessNumber;
	private String pointAcctCd;
	private String transNbr;
	private String transNbrType;
	private String exchangeType;
	private String pointRelaObjCd;
	private String exchangeCount;
	private String chargeNumberType;
	private String chargeType;
	private String pointCount;
	private String custName;
	private String custNbr;
	private String postAddr;
	private String postCode;
	private String linkNbr;
	private String sendDate;
	private String joinActionReq;
	private String encryptStr;
	private String familyId;
	private String servTypeId;
	private String puk;
	private String subClubChangeReq;
	private String pointExchangeReq;

	private String transactionId;
	private String attach;
	private String compCode;

	private String userAccount;
	private String areaCode;
	private String sourceName;

	// 充值卡充值(新版), 入参
	private UocCardRechargeOrder uocCardRechargeOrder;

	//自助排障功能新增
	private String troubleDesc="";
	private String rowId="";
	private String checkResult;
	private String errorTip="";
	private String reviseResult="";

	private String loginAccount="";
	private String checkName="";
	private String errorCheckName="";
	private String openId="";
	private String uuid="";
	private String checkTime="";

	//订单属性节点增加
	private JSONArray orderAttrs;

	// 营销资源实例编码，补换卡时为UIM卡号
	private String mktResInstNbr;

	//新装订单项生成的产品实例接入号码信息集合，生单成功情况下返回
	private JSONArray ordProdInstAccNumRelates;

	//客户操作订单相关的客户信息节点
	private List<Customer> customers;

	//记录参与人唯一标识
	private String partyId;

	/**
	 * 接入号集合
	 */
	private List<String> accNbrList;

	private List<Product> products;


	private String billCheck;

	private String mobile;

	/**
	 * 翼券平台的授权途径
	 * 1、微信，2、单点登录，3、广东天翼4、支付宝，5、QQ，6 、pc端，7、189平台
	 */
	private String authSource;



	/**
	 * 领取优惠券的id
	 */
	private String couponId;

	/**
	 * 系统标志
	 */
	private String extSysType;

	/**
	 * 系统流水
	 */
	private String extSysValue;


	private String nickname;
	private String sex;
	private String language;
	private String city;
	private String province;
	private String country;
	/**
	 * 头像地址
	 */
	private String headImgUrl;
	/**
	 * 身份证号,创建用户时使用
	 */
	private String idCardNo;
	private String name;
	/**
	 * 证件号码
	 */
	private String identityCard;
	private String email;
	/**
	 * 核销门店id
	 */
	private String orgId;
	/**
	 * 核销通知url
	 */
	private String notifyUrl;
	/**
	 * 来源
	 */
	private String source;
	/**
	 * 透传参数
	 */
	private String passData;
	/**
	 * 领券端,可选值：1微信H5，6非微信端（pc、手机浏览器），7小程序
	 */
	private String endPoint;

	/**
	 * 定时领券时间
	 */
	private String receiveDate;

	/**
	 * 所属活动标识
	 */
	private String activeCode;
	/**
	 * 活动金额（分）
	 */
	private String activeMoney;

	/**
	 * 优惠券领取id
	 */
	private String getId;

	/**
	 * 优惠券兑换码短码
	 */
	private String shortCode;

	/**
	 * 核销人员id(线下核销必填)
	 */
	private String salesId;

	/**
	 * 手机串码
	 */
	private String imei;

	/**
	 * 卡号
	 */
	private String cardNo;
	/**
	 * 宽带号码
	 */
	private String ad;

	/**
	 * 接入方账号
	 */
	private String partner;

	public String getChannelCode() {
		return channelCode;
	}

	public void setChannelCode(String channelCode) {
		this.channelCode = channelCode;
	}

	public String getContactCode() {
		return contactCode;
	}

	public void setContactCode(String contactCode) {
		this.contactCode = contactCode;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getShopSalesCode() {
		return shopSalesCode;
	}

	public void setShopSalesCode(String shopSalesCode) {
		this.shopSalesCode = shopSalesCode;
	}

	/**
	 * 渠道编码
	 */
	private String channelCode;

	/**
	 * 触点编码
	 */
	private String contactCode;

	/**
	 * 核销门店名称
	 */
	private String shopName;

	/**
	 * 核销门店编码,线下核销必填
	 */
	private String shopCode;

	/**
	 * 领券门店名称
	 */
	private String shopSalesCode;



	/**
	 * 如意对接订单信息集合
	 */
	private List<MultipleOffer> goodsInfo;

	private List<ContactInfos> contactInfos;

	private List<DevStaffInfos> devStaffInfos;

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getHeadImgUrl() {
		return headImgUrl;
	}

	public void setHeadImgUrl(String headImgUrl) {
		this.headImgUrl = headImgUrl;
	}

	public String getIdCardNo() {
		return idCardNo;
	}

	public void setIdCardNo(String idCardNo) {
		this.idCardNo = idCardNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIdentityCard() {
		return identityCard;
	}

	public void setIdentityCard(String identityCard) {
		this.identityCard = identityCard;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getNotifyUrl() {
		return notifyUrl;
	}

	public void setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getPassData() {
		return passData;
	}

	public void setPassData(String passData) {
		this.passData = passData;
	}

	public String getEndPoint() {
		return endPoint;
	}

	public void setEndPoint(String endPoint) {
		this.endPoint = endPoint;
	}

	public String getReceiveDate() {
		return receiveDate;
	}

	public void setReceiveDate(String receiveDate) {
		this.receiveDate = receiveDate;
	}

	public String getActiveCode() {
		return activeCode;
	}

	public void setActiveCode(String activeCode) {
		this.activeCode = activeCode;
	}

	public String getActiveMoney() {
		return activeMoney;
	}

	public void setActiveMoney(String activeMoney) {
		this.activeMoney = activeMoney;
	}

	public String getGetId() {
		return getId;
	}

	public void setGetId(String getId) {
		this.getId = getId;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getSalesId() {
		return salesId;
	}

	public void setSalesId(String salesId) {
		this.salesId = salesId;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public String getAd() {
		return ad;
	}

	public void setAd(String ad) {
		this.ad = ad;
	}

	public String getCouponId() {
		return couponId;
	}

	public void setCouponId(String couponId) {
		this.couponId = couponId;
	}

	public String getExtSysType() {
		return extSysType;
	}

	public void setExtSysType(String extSysType) {
		this.extSysType = extSysType;
	}

	public String getExtSysValue() {
		return extSysValue;
	}

	public void setExtSysValue(String extSysValue) {
		this.extSysValue = extSysValue;
	}

	public String getBillCheck() {
		return billCheck;
	}

	public void setBillCheck(String billCheck) {
		this.billCheck = billCheck;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getAttach() {
		return attach;
	}

	public void setAttach(String attach) {
		this.attach = attach;
	}

	public String getCompCode() {
		return compCode;
	}

	public void setCompCode(String compCode) {
		this.compCode = compCode;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getBonusType() {
		return bonusType;
	}

	public void setBonusType(String bonusType) {
		this.bonusType = bonusType;
	}

	public String getSeq() {
		return seq;
	}

	public void setSeq(String seq) {
		this.seq = seq;
	}

	public String getFlowBonusNo() {
		return flowBonusNo;
	}

	public void setFlowBonusNo(String flowBonusNo) {
		this.flowBonusNo = flowBonusNo;
	}

	public String getUniqueCode() {
		return uniqueCode;
	}

	public void setUniqueCode(String uniqueCode) {
		this.uniqueCode = uniqueCode;
	}

	public String getFlowCnt() {
		return flowCnt;
	}

	public void setFlowCnt(String flowCnt) {
		this.flowCnt = flowCnt;
	}

	public String getFlowPerNum() {
		return flowPerNum;
	}

	public void setFlowPerNum(String flowPerNum) {
		this.flowPerNum = flowPerNum;
	}

	public String getTelphone() {
		return telphone;
	}

	public void setTelphone(String telphone) {
		this.telphone = telphone;
	}

	public String getFlowType() {
		return flowType;
	}

	public void setFlowType(String flowType) {
		this.flowType = flowType;
	}

	public String getFlowNum() {
		return flowNum;
	}

	public void setFlowNum(String flowNum) {
		this.flowNum = flowNum;
	}

	public String getFriendPhone() {
		return friendPhone;
	}

	public void setFriendPhone(String friendPhone) {
		this.friendPhone = friendPhone;
	}

	public String getTradeNo() {
		return tradeNo;
	}

	public void setTradeNo(String tradeNo) {
		this.tradeNo = tradeNo;
	}

	public String getReqTime() {
		return reqTime;
	}

	public void setReqTime(String reqTime) {
		this.reqTime = reqTime;
	}

	public String getAcceptstaffid() {
		return acceptstaffid;
	}

	public void setAcceptstaffid(String acceptstaffid) {
		this.acceptstaffid = acceptstaffid;
	}

	public String getRegionid() {
		return regionid;
	}

	public void setRegionid(String regionid) {
		this.regionid = regionid;
	}

	public String getReaionname() {
		return reaionname;
	}

	public void setReaionname(String reaionname) {
		this.reaionname = reaionname;
	}

	/*public String getProdtype() {
		return prodtype;
	}*/

	/*public void setProdtype(String prodtype) {
		this.prodtype = prodtype;
	}*/

	public String getProdnum() {
		return prodnum;
	}

	public void setProdnum(String prodnum) {
		this.prodnum = prodnum;
	}

	public String getCustId() {
		return custId;
	}

	public void setCustId(String custId) {
		this.custId = custId;
	}

	/*public String getCustname() {
		return custname;
	}*/

	/*public void setCustname(String custname) {
		this.custname = custname;
	}*/

	/*public String getServicetype() {
		return servicetype;
	}*/

	/*public void setServicetype(String servicetype) {
		this.servicetype = servicetype;
	}*/

	public String getServicetypedesc() {
		return servicetypedesc;
	}

	public void setServicetypedesc(String servicetypedesc) {
		this.servicetypedesc = servicetypedesc;
	}

	public String getRelaman() {
		return relaman;
	}

	public void setRelaman(String relaman) {
		this.relaman = relaman;
	}

	public String getRelainfo() {
		return relainfo;
	}

	public void setRelainfo(String relainfo) {
		this.relainfo = relainfo;
	}

	public String getAcceptchanneldesc() {
		return acceptchanneldesc;
	}

	public void setAcceptchanneldesc(String acceptchanneldesc) {
		this.acceptchanneldesc = acceptchanneldesc;
	}

	public String getAcceptcomefrom() {
		return acceptcomefrom;
	}

	public void setAcceptcomefrom(String acceptcomefrom) {
		this.acceptcomefrom = acceptcomefrom;
	}

	public String getAcceptcomefromdesc() {
		return acceptcomefromdesc;
	}

	public void setAcceptcomefromdesc(String acceptcomefromdesc) {
		this.acceptcomefromdesc = acceptcomefromdesc;
	}

	public String getAcceptstaffname() {
		return acceptstaffname;
	}

	public void setAcceptstaffname(String acceptstaffname) {
		this.acceptstaffname = acceptstaffname;
	}

	public String getAcceptorgid() {
		return acceptorgid;
	}

	public void setAcceptorgid(String acceptorgid) {
		this.acceptorgid = acceptorgid;
	}

	public String getAcceptorgname() {
		return acceptorgname;
	}

	public void setAcceptorgname(String acceptorgname) {
		this.acceptorgname = acceptorgname;
	}

	public String getUrgencygrade() {
		return urgencygrade;
	}

	public void setUrgencygrade(String urgencygrade) {
		this.urgencygrade = urgencygrade;
	}

	public String getUrgencygradedesc() {
		return urgencygradedesc;
	}

	public void setUrgencygradedesc(String urgencygradedesc) {
		this.urgencygradedesc = urgencygradedesc;
	}

	public String getCustemail() {
		return custemail;
	}

	public void setCustemail(String custemail) {
		this.custemail = custemail;
	}

	public String getServicedate() {
		return servicedate;
	}

	public void setServicedate(String servicedate) {
		this.servicedate = servicedate;
	}

	public String getServicedatedesc() {
		return servicedatedesc;
	}

	public void setServicedatedesc(String servicedatedesc) {
		this.servicedatedesc = servicedatedesc;
	}

	public String getIsowner() {
		return isowner;
	}

	public void setIsowner(String isowner) {
		this.isowner = isowner;
	}

	public String getMorerelainfo() {
		return morerelainfo;
	}

	public void setMorerelainfo(String morerelainfo) {
		this.morerelainfo = morerelainfo;
	}

	public String getCustemotion() {
		return custemotion;
	}

	public void setCustemotion(String custemotion) {
		this.custemotion = custemotion;
	}

	public String getCustemotiondesc() {
		return custemotiondesc;
	}

	public void setCustemotiondesc(String custemotiondesc) {
		this.custemotiondesc = custemotiondesc;
	}

	public String getAppealprodid() {
		return appealprodid;
	}

	public void setAppealprodid(String appealprodid) {
		this.appealprodid = appealprodid;
	}

	public String getAppealprodname() {
		return appealprodname;
	}

	public void setAppealprodname(String appealprodname) {
		this.appealprodname = appealprodname;
	}

	public String getAppealreasonid() {
		return appealreasonid;
	}

	public void setAppealreasonid(String appealreasonid) {
		this.appealreasonid = appealreasonid;
	}

	public String getAppealreasondesc() {
		return appealreasondesc;
	}

	public void setAppealreasondesc(String appealreasondesc) {
		this.appealreasondesc = appealreasondesc;
	}

	public String getAppealchild() {
		return appealchild;
	}

	public void setAppealchild(String appealchild) {
		this.appealchild = appealchild;
	}

	public String getAppealchilddesc() {
		return appealchilddesc;
	}

	public void setAppealchilddesc(String appealchilddesc) {
		this.appealchilddesc = appealchilddesc;
	}

	public String getFougradecatalog() {
		return fougradecatalog;
	}

	public void setFougradecatalog(String fougradecatalog) {
		this.fougradecatalog = fougradecatalog;
	}

	public String getFougradecatalogdesc() {
		return fougradecatalogdesc;
	}

	public void setFougradecatalogdesc(String fougradecatalogdesc) {
		this.fougradecatalogdesc = fougradecatalogdesc;
	}

	public String getAppealdetailid() {
		return appealdetailid;
	}

	public void setAppealdetailid(String appealdetailid) {
		this.appealdetailid = appealdetailid;
	}

	public String getAppealdetaildesc() {
		return appealdetaildesc;
	}

	public void setAppealdetaildesc(String appealdetaildesc) {
		this.appealdetaildesc = appealdetaildesc;
	}

	public String getAcceptcontent() {
		return acceptcontent;
	}

	public void setAcceptcontent(String acceptcontent) {
		this.acceptcontent = acceptcontent;
	}

	public String getFa() {
		return fa;
	}

	public void setFa(String fA) {
		fa = fA;
	}

	public String getFaType() {
		return faType;
	}

	public void setFaType(String fAType) {
		faType = fAType;
	}

	public String getStreamingNo() {
		return streamingNo;
	}

	public void setStreamingNo(String streamingNo) {
		this.streamingNo = streamingNo;
	}

	public String getSrcDeviceType() {
		return srcDeviceType;
	}

	public void setSrcDeviceType(String srcDeviceType) {
		this.srcDeviceType = srcDeviceType;
	}

	public String getSrcDeviceID() {
		return srcDeviceID;
	}

	public void setSrcDeviceID(String srcDeviceID) {
		this.srcDeviceID = srcDeviceID;
	}

	public String getOa() {
		return oa;
	}

	public void setOa(String oA) {
		oa = oA;
	}

	public String getOatype() {
		return oatype;
	}

	public void setOatype(String oAType) {
		oatype = oAType;
	}

	public String getDa() {
		return da;
	}

	public void setDa(String dA) {
		da = dA;
	}

	public String getDatype() {
		return datype;
	}

	public void setDatype(String dAType) {
		datype = dAType;
	}

	public String getId() {
		return id;
	}

	public void setId(String iD) {
		id = iD;
	}

	public String getIdType() {
		return idType;
	}

	public void setIdType(String iDType) {
		idType = iDType;
	}

	public String getChanelPlayerID() {
		return chanelPlayerID;
	}

	public void setChanelPlayerID(String chanelPlayerID) {
		this.chanelPlayerID = chanelPlayerID;
	}

	public String getEventID() {
		return eventID;
	}

	public void setEventID(String eventID) {
		this.eventID = eventID;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getBillState() {
		return billState;
	}

	public void setBillState(String billState) {
		this.billState = billState;
	}

	public String getUpTranSeq() {
		return upTranSeq;
	}

	public void setUpTranSeq(String uPTRANSEQ) {
		upTranSeq = uPTRANSEQ;
	}

	public String getRetncode() {
		return retncode;
	}

	public void setRetncode(String rETNCODE) {
		retncode = rETNCODE;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public String getPayCode() {
		return payCode;
	}

	public void setPayCode(String payCode) {
		this.payCode = payCode;
	}

	public String getPayMoney() {
		return payMoney;
	}

	public void setPayMoney(String payMoney) {
		this.payMoney = payMoney;
	}

	public String getTranDate() {
		return tranDate;
	}

	public void setTranDate(String tRANDATE) {
		tranDate = tRANDATE;
	}

	public String getDeliveMoney() {
		return deliveMoney;
	}

	public void setDeliveMoney(String deliveMoney) {
		this.deliveMoney = deliveMoney;
	}

	public List getItems() {
		return items;
	}

	public void setItems(List items) {
		this.items = items;
	}

	public String getBillId() {
		return billId;
	}

	public void setBillId(String billId) {
		this.billId = billId;
	}

	public String getPayerWaitTime() {
		return payerWaitTime;
	}

	public void setPayerWaitTime(String payerWaitTime) {
		this.payerWaitTime = payerWaitTime;
	}

	public String getPayerName() {
		return payerName;
	}

	public void setPayerName(String payerName) {
		this.payerName = payerName;
	}

	public String getPayerAreaNo() {
		return payerAreaNo;
	}

	public void setPayerAreaNo(String payerAreaNo) {
		this.payerAreaNo = payerAreaNo;
	}

	public String getPayerAddress() {
		return payerAddress;
	}

	public void setPayerAddress(String payerAddress) {
		this.payerAddress = payerAddress;
	}

	public String getPayerPhone() {
		return payerPhone;
	}

	public void setPayerPhone(String payerPhone) {
		this.payerPhone = payerPhone;
	}

	public String getPayerOtherPhone() {
		return payerOtherPhone;
	}

	public void setPayerOtherPhone(String payerOtherPhone) {
		this.payerOtherPhone = payerOtherPhone;
	}

	public String getPayerRemark() {
		return payerRemark;
	}

	public void setPayerRemark(String payerRemark) {
		this.payerRemark = payerRemark;
	}

	public String getPayerZipCode() {
		return payerZipCode;
	}

	public void setPayerZipCode(String payerZipCode) {
		this.payerZipCode = payerZipCode;
	}

	public String getReceiverName() {
		return receiverName;
	}

	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}

	public String getReceiverAreaNo() {
		return receiverAreaNo;
	}

	public void setReceiverAreaNo(String receiverAreaNo) {
		this.receiverAreaNo = receiverAreaNo;
	}

	public String getReceiverAddress() {
		return receiverAddress;
	}

	public void setReceiverAddress(String receiverAddress) {
		this.receiverAddress = receiverAddress;
	}

	public String getReceiverWaitTime() {
		return receiverWaitTime;
	}

	public void setReceiverWaitTime(String receiverWaitTime) {
		this.receiverWaitTime = receiverWaitTime;
	}

	public String getReceiverPhone() {
		return receiverPhone;
	}

	public void setReceiverPhone(String receiverPhone) {
		this.receiverPhone = receiverPhone;
	}

	public String getReceiverOtherPhone() {
		return receiverOtherPhone;
	}

	public void setReceiverOtherPhone(String receiverOtherPhone) {
		this.receiverOtherPhone = receiverOtherPhone;
	}

	public String getReceiverZipCode() {
		return receiverZipCode;
	}

	public void setReceiverZipCode(String receiverZipCode) {
		this.receiverZipCode = receiverZipCode;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getReceiverRemark() {
		return receiverRemark;
	}

	public void setReceiverRemark(String receiverRemark) {
		this.receiverRemark = receiverRemark;
	}

	public String getInvoiceContent() {
		return invoiceContent;
	}

	public void setInvoiceContent(String invoiceContent) {
		this.invoiceContent = invoiceContent;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**
	 * get billChannel
	 *
	 * @return Returns the billChannel.<br>
	 */
	public String getBillChannel() {
		return billChannel;
	}

	/**
	 * set billChannel
	 *
	 * @param billChannel
	 *            The billChannel to set. <br>
	 */
	public void setBillChannel(String billChannel) {
		this.billChannel = billChannel;
	}

	/**
	 * get trandId
	 *
	 * @return Returns the trandId.<br>
	 */
	public String getTrandId() {
		return trandId;
	}

	/**
	 * set trandId
	 *
	 * @param trandId
	 *            The trandId to set. <br>
	 */
	public void setTrandId(String trandId) {
		this.trandId = trandId;
	}

	/**
	 * get channelPasswd
	 *
	 * @return Returns the channelPasswd.<br>
	 */
	public String getChannelPasswd() {
		return channelPasswd;
	}

	/**
	 * set channelPasswd
	 *
	 * @param channelPasswd
	 *            The channelPasswd to set. <br>
	 */
	public void setChannelPasswd(String channelPasswd) {
		this.channelPasswd = channelPasswd;
	}

	/**
	 * get businessType
	 *
	 * @return Returns the businessType.<br>
	 */
	public Integer getBusinessType() {
		return businessType;
	}

	/**
	 * set businessType
	 *
	 * @param businessType
	 *            The businessType to set. <br>
	 */
	public void setBusinessType(Integer businessType) {
		this.businessType = businessType;
	}

	/**
	 * get businessAction
	 *
	 * @return Returns the businessAction.<br>
	 */
	public Integer getBusinessAction() {
		return businessAction;
	}

	/**
	 * set businessAction
	 *
	 * @param businessAction
	 *            The businessAction to set. <br>
	 */
	public void setBusinessAction(Integer businessAction) {
		this.businessAction = businessAction;
	}

	/**
	 * get offers
	 *
	 * @return Returns the offers.<br>
	 */
	public List<Offer> getOffers() {
		return offers;
	}

	/**
	 * set offers
	 *
	 * @param offers
	 *            The offers to set. <br>
	 */
	public void setOffers(List<Offer> offers) {
		this.offers = offers;
	}

	/**
	 * get olId
	 *
	 * @return Returns the olId.<br>
	 */
	public String getOlId() {
		return olId;
	}

	/**
	 * set olId
	 *
	 * @param olId
	 *            The olId to set. <br>
	 */
	public void setOlId(String olId) {
		this.olId = olId;
	}

	/**
	 * get olNbr
	 *
	 * @return Returns the olNbr.<br>
	 */
	public String getOlNbr() {
		return olNbr;
	}

	/**
	 * set olNbr
	 *
	 * @param olNbr
	 *            The olNbr to set. <br>
	 */
	public void setOlNbr(String olNbr) {
		this.olNbr = olNbr;
	}

	/**
	 * get coMarketCode
	 *
	 * @return Returns the coMarketCode.<br>
	 */
	public String getCoMarketCode() {
		return coMarketCode;
	}

	/**
	 * set coMarketCode
	 *
	 * @param coMarketCode
	 *            The coMarketCode to set. <br>
	 */
	public void setCoMarketCode(String coMarketCode) {
		this.coMarketCode = coMarketCode;
	}

	/**
	 * get coMarketId
	 *
	 * @return Returns the coMarketId.<br>
	 */
	public String getCoMarketId() {
		return coMarketId;
	}

	/**
	 * set coMarketId
	 *
	 * @param coMarketId
	 *            The coMarketId to set. <br>
	 */
	public void setCoMarketId(String coMarketId) {
		this.coMarketId = coMarketId;
	}

	/**
	 * get productId
	 *
	 * @return Returns the productId.<br>
	 */
	public String getProductId() {
		return productId;
	}

	/**
	 * set productId
	 *
	 * @param productId
	 *            The productId to set. <br>
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}

	/**
	 * get remoteIp
	 *
	 * @return Returns the remoteIp.<br>
	 */
	public String getRemoteIp() {
		return remoteIp;
	}

	/**
	 * set remoteIp
	 *
	 * @param remoteIp
	 *            The remoteIp to set. <br>
	 */
	public void setRemoteIp(String remoteIp) {
		this.remoteIp = remoteIp;
	}

	/**
	 * get billCode
	 *
	 * @return Returns the billCode.<br>
	 */
	public String getBillCode() {
		return billCode;
	}

	/**
	 * set billCode
	 *
	 * @param billCode
	 *            The billCode to set. <br>
	 */
	public void setBillCode(String billCode) {
		this.billCode = billCode;
	}

	public String getPayMethod() {
		return payMethod;
	}

	public void setPayMethod(String payMethod) {
		this.payMethod = payMethod;
	}

	public String getUserIDType() {
		return userIDType;
	}

	public void setUserIDType(String userIDType) {
		this.userIDType = userIDType;
	}

	public String getRecommendPerson() {
		return recommendPerson;
	}

	public void setRecommendPerson(String recommendPerson) {
		this.recommendPerson = recommendPerson;
	}

	public String getRecommendCode() {
		return recommendCode;
	}

	public void setRecommendCode(String recommendCode) {
		this.recommendCode = recommendCode;
	}

	public String getRecommendId() {
		return recommendId;
	}

	public void setRecommendId(String recommendId) {
		this.recommendId = recommendId;
	}

	public String getExt1() {
		return ext1;
	}

	public void setExt1(String ext1) {
		this.ext1 = ext1;
	}

	public String getExt2() {
		return ext2;
	}

	public void setExt2(String ext2) {
		this.ext2 = ext2;
	}

	public String getExt3() {
		return ext3;
	}

	public void setExt3(String ext3) {
		this.ext3 = ext3;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public String getActionCode() {
		return actionCode;
	}

	public void setActionCode(String actionCode) {
		this.actionCode = actionCode;
	}

	public String getInterfacetype() {
		return interfacetype;
	}

	public void setInterfacetype(String interfacetype) {
		this.interfacetype = interfacetype;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getCityid() {
		return cityid;
	}

	public void setCityid(String cityid) {
		this.cityid = cityid;
	}

	public String getSpid() {
		return spid;
	}

	public void setSpid(String spid) {
		this.spid = spid;
	}

	public String getServiceid() {
		return serviceid;
	}

	public void setServiceid(String serviceid) {
		this.serviceid = serviceid;
	}

	public String getServicename() {
		return servicename;
	}

	public void setServicename(String servicename) {
		this.servicename = servicename;
	}

	public String getConsumedate() {
		return consumedate;
	}

	public void setConsumedate(String consumedate) {
		this.consumedate = consumedate;
	}

	public String getSpaccount() {
		return spaccount;
	}

	public void setSpaccount(String spaccount) {
		this.spaccount = spaccount;
	}

	public String getFeetype() {
		return feetype;
	}

	public void setFeetype(String feetype) {
		this.feetype = feetype;
	}

	public String getFee() {
		return fee;
	}

	public void setFee(String fee) {
		this.fee = fee;
	}

	public String getBegintime() {
		return begintime;
	}

	public void setBegintime(String begintime) {
		this.begintime = begintime;
	}

	public String getEndtime() {
		return endtime;
	}

	public void setEndtime(String endtime) {
		this.endtime = endtime;
	}

	/*public String getTimestamp() {
		return timestamp;
	}*/

	/*public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}*/

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getReserved1() {
		return reserved1;
	}

	public void setReserved1(String reserved1) {
		this.reserved1 = reserved1;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getCustomerid() {
		return customerid;
	}

	public void setCustomerid(String customerid) {
		this.customerid = customerid;
	}

	public String getSpcode() {
		return spcode;
	}

	public void setSpcode(String spcode) {
		this.spcode = spcode;
	}

	public String getSppassword() {
		return sppassword;
	}

	public void setSppassword(String sppassword) {
		this.sppassword = sppassword;
	}

	public String getPolicycode() {
		return policycode;
	}

	public void setPolicycode(String policycode) {
		this.policycode = policycode;
	}

	public String getServerip() {
		return serverip;
	}

	public void setServerip(String serverip) {
		this.serverip = serverip;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	/*public String getUserID() {
		return userID;
	}*/

	/*public void setUserID(String userID) {
		this.userID = userID;
	}*/

	public String getCertName() {
		return certName;
	}

	public void setCertName(String certName) {
		this.certName = certName;
	}

	public String getCertNumber() {
		return certNumber;
	}

	public void setCertNumber(String certNumber) {
		this.certNumber = certNumber;
	}

	public Boolean getChangeUim() {
		return changeUim;
	}

	public void setChangeUim(Boolean changeUim) {
		this.changeUim = changeUim;
	}

	public String getUimCode() {
		return uimCode;
	}

	public void setUimCode(String uimCode) {
		this.uimCode = uimCode;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getAuthenticator() {
		return authenticator;
	}

	public void setAuthenticator(String authenticator) {
		this.authenticator = authenticator;
	}

	public String getOpenType() {
		return openType;
	}

	public void setOpenType(String openType) {
		this.openType = openType;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	/*public String getOrdertimelen() {
		return ordertimelen;
	}*/

/*	public void setOrdertimelen(String ordertimelen) {
		this.ordertimelen = ordertimelen;
	}*/

	public String getParam() {
		return param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	public String getAccessNumber() {
		return accessNumber;
	}

	public void setAccessNumber(String accessNumber) {
		this.accessNumber = accessNumber;
	}

	public String getPointAcctCd() {
		return pointAcctCd;
	}

	public void setPointAcctCd(String pointAcctCd) {
		this.pointAcctCd = pointAcctCd;
	}

	public String getTransNbr() {
		return transNbr;
	}

	public void setTransNbr(String transNbr) {
		this.transNbr = transNbr;
	}

	public String getTransNbrType() {
		return transNbrType;
	}

	public void setTransNbrType(String transNbrType) {
		this.transNbrType = transNbrType;
	}

	public String getExchangeType() {
		return exchangeType;
	}

	public void setExchangeType(String exchangeType) {
		this.exchangeType = exchangeType;
	}

	public String getPointRelaObjCd() {
		return pointRelaObjCd;
	}

	public void setPointRelaObjCd(String pointRelaObjCd) {
		this.pointRelaObjCd = pointRelaObjCd;
	}

	public String getExchangeCount() {
		return exchangeCount;
	}

	public void setExchangeCount(String exchangeCount) {
		this.exchangeCount = exchangeCount;
	}

	public String getChargeNumberType() {
		return chargeNumberType;
	}

	public void setChargeNumberType(String chargeNumberType) {
		this.chargeNumberType = chargeNumberType;
	}

	public String getChargeType() {
		return chargeType;
	}

	public void setChargeType(String chargeType) {
		this.chargeType = chargeType;
	}

	public String getPointCount() {
		return pointCount;
	}

	public void setPointCount(String pointCount) {
		this.pointCount = pointCount;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getCustNbr() {
		return custNbr;
	}

	public void setCustNbr(String custNbr) {
		this.custNbr = custNbr;
	}

	public String getPostAddr() {
		return postAddr;
	}

	public void setPostAddr(String postAddr) {
		this.postAddr = postAddr;
	}

	public String getPostCode() {
		return postCode;
	}

	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	public String getLinkNbr() {
		return linkNbr;
	}

	public void setLinkNbr(String linkNbr) {
		this.linkNbr = linkNbr;
	}

	public String getLinkName() {
		return linkName;
	}

	public void setLinkName(String linkName) {
		this.linkName = linkName;
	}

	public String getSendDate() {
		return sendDate;
	}

	public void setSendDate(String sendDate) {
		this.sendDate = sendDate;
	}

	public String getJoinActionReq() {
		return joinActionReq;
	}

	public void setJoinActionReq(String joinActionReq) {
		this.joinActionReq = joinActionReq;
	}

	public String getEncryptStr() {
		return encryptStr;
	}

	public void setEncryptStr(String encryptStr) {
		this.encryptStr = encryptStr;
	}

	public String getFamilyId() {
		return familyId;
	}

	public void setFamilyId(String familyId) {
		this.familyId = familyId;
	}

	public String getServTypeId() {
		return servTypeId;
	}

	public void setServTypeId(String servTypeId) {
		this.servTypeId = servTypeId;
	}

	public String getPuk() {
		return puk;
	}

	public void setPuk(String puk) {
		this.puk = puk;
	}

	public String getSubClubChangeReq() {
		return subClubChangeReq;
	}

	public void setSubClubChangeReq(String subClubChangeReq) {
		this.subClubChangeReq = subClubChangeReq;
	}

	public String getAcctName() {
		return acctName;
	}

	public void setAcctName(String acctName) {
		this.acctName = acctName;
	}

	public String getIndentNbr() {
		return indentNbr;
	}

	public void setIndentNbr(String indentNbr) {
		this.indentNbr = indentNbr;
	}

	public String getLinkNum() {
		return linkNum;
	}

	public void setLinkNum(String linkNum) {
		this.linkNum = linkNum;
	}

	public String getPointExchangeReq() {
		return pointExchangeReq;
	}

	public void setPointExchangeReq(String pointExchangeReq) {
		this.pointExchangeReq = pointExchangeReq;
	}

	public String getToxml() {
		return toxml;
	}

	public void setToxml(String toxml) {
		this.toxml = toxml;
	}

	public String getNetAcct() {
		return netAcct;
	}

	public void setNetAcct(String netAcct) {
		this.netAcct = netAcct;
	}

	public String getVisitIp() {
		return visitIp;
	}

	public void setVisitIp(String visitIp) {
		this.visitIp = visitIp;
	}

	public String getUserPort() {
		return userPort;
	}

	public void setUserPort(String userPort) {
		this.userPort = userPort;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getOrderTimeLen() {
		return orderTimeLen;
	}

	public void setOrderTimeLen(String orderTimeLen) {
		this.orderTimeLen = orderTimeLen;
	}

	public String getUnsubscribe() {
		return unsubscribe;
	}

	public void setUnsubscribe(String unsubscribe) {
		this.unsubscribe = unsubscribe;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getMaxRate() {
		return maxRate;
	}

	public void setMaxRate(String maxRate) {
		this.maxRate = maxRate;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getStopTime() {
		return stopTime;
	}

	public void setStopTime(String stopTime) {
		this.stopTime = stopTime;
	}

	public String getOperType() {
		return operType;
	}

	public void setOperType(String operType) {
		this.operType = operType;
	}

	public String getEffectSpeed() {
		return effectSpeed;
	}

	public void setEffectSpeed(String effectSpeed) {
		this.effectSpeed = effectSpeed;
	}

	public String getQuerymonth() {
		return querymonth;
	}

	public void setQuerymonth(String querymonth) {
		this.querymonth = querymonth;
	}

	public String getVnetSpid() {
		return vnetSpid;
	}

	public void setVnetSpid(String vnetSpid) {
		this.vnetSpid = vnetSpid;
	}

	public String getClientIP() {
		return clientIP;
	}

	public void setClientIP(String clientIP) {
		this.clientIP = clientIP;
	}

	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getTimes() {
		return times;
	}

	public void setTimes(String times) {
		this.times = times;
	}

	public String getChanal() {
		return chanal;
	}

	public void setChanal(String chanal) {
		this.chanal = chanal;
	}

	public String getProdType() {
		return prodType;
	}

	public void setProdType(String prodType) {
		this.prodType = prodType;
	}

	public String getOperationType() {
		return operationType;
	}

	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}

	public String getOperationCode() {
		return operationCode;
	}

	public void setOperationCode(String operationCode) {
		this.operationCode = operationCode;
	}

	public String getAccessType() {
		return accessType;
	}

	public void setAccessType(String accessType) {
		this.accessType = accessType;
	}

	public String getReqAmount() {
		return reqAmount;
	}

	public void setReqAmount(String reqAmount) {
		this.reqAmount = reqAmount;
	}

	public String getReqSerail() {
		return reqSerail;
	}

	public void setReqSerail(String reqSerail) {
		this.reqSerail = reqSerail;
	}

	public String getReqSource() {
		return reqSource;
	}

	public void setReqSource(String reqSource) {
		this.reqSource = reqSource;
	}

	public String getRechargeInfoList() {
		return rechargeInfoList;
	}

	public void setRechargeInfoList(String rechargeInfoList) {
		this.rechargeInfoList = rechargeInfoList;
	}

	public List<User> getPartyUsers() {
		return partyUsers;
	}

	public void setPartyUsers(List<User> partyUsers) {
		this.partyUsers = partyUsers;
	}

	public String getUserAccount() {
		return userAccount;
	}

	public void setUserAccount(String userAccount) {
		this.userAccount = userAccount;
	}
	@Override
	public String getAreaCode() {
		return areaCode;
	}
	@Override
	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getSourceName() {
		return sourceName;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	public Date getInvokeStart() {
		return invokeStart;
	}

	public void setInvokeStart(Date invokeStart) {
		this.invokeStart = invokeStart;
	}

	public Date getInvokeEnd() {
		return invokeEnd;
	}

	public void setInvokeEnd(Date invokeEnd) {
		this.invokeEnd = invokeEnd;
	}

	public String getMemberAreaCode() {
		return memberAreaCode;
	}

	public void setMemberAreaCode(String memberAreaCode) {
		this.memberAreaCode = memberAreaCode;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public UocCardRechargeOrder getUocCardRechargeOrder() {
		return uocCardRechargeOrder;
	}

	public void setUocCardRechargeOrder(UocCardRechargeOrder uocCardRechargeOrder) {
		this.uocCardRechargeOrder = uocCardRechargeOrder;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getTroubleDesc() {
		return troubleDesc;
	}

	public void setTroubleDesc(String troubleDesc) {
		this.troubleDesc = troubleDesc;
	}

	public String getRowId() {
		return rowId;
	}

	public void setRowId(String rowId) {
		this.rowId = rowId;
	}

	public String getCheckResult() {
		return checkResult;
	}

	public void setCheckResult(String checkResult) {
		this.checkResult = checkResult;
	}

	public String getErrorTip() {
		return errorTip;
	}

	public void setErrorTip(String errorTip) {
		this.errorTip = errorTip;
	}

	public String getReviseResult() {
		return reviseResult;
	}

	public void setReviseResult(String reviseResult) {
		this.reviseResult = reviseResult;
	}

	public String getLoginAccount() {
		return loginAccount;
	}

	public void setLoginAccount(String loginAccount) {
		this.loginAccount = loginAccount;
	}


	public String getCheckName() {
		return checkName;
	}

	public void setCheckName(String checkName) {
		this.checkName = checkName;
	}

	public String getErrorCheckName() {
		return errorCheckName;
	}

	public void setErrorCheckName(String errorCheckName) {
		this.errorCheckName = errorCheckName;
	}


	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getCheckTime() {
		return checkTime;
	}

	public void setCheckTime(String checkTime) {
		this.checkTime = checkTime;
	}

	public String getOperationName() {
		return operationName;
	}

	public void setOperationName(String operationName) {
		this.operationName = operationName;
	}

	public List<MultipleOffer> getMultipleOffers() {
		return multipleOffers;
	}


	public void setMultipleOffers(List<MultipleOffer> multipleOffers) {
		this.multipleOffers = multipleOffers;
	}

	public JSONArray getOrderAttrs() {
		return orderAttrs;
	}

	public void setOrderAttrs(JSONArray orderAttrs) {
		this.orderAttrs = orderAttrs;
	}

	public String getMktResInstNbr() {
		return mktResInstNbr;
	}

	public void setMktResInstNbr(String mktResInstNbr) {
		this.mktResInstNbr = mktResInstNbr;
	}

	public JSONArray getOrdProdInstAccNumRelates() {
		return ordProdInstAccNumRelates;
	}

	public void setOrdProdInstAccNumRelates(JSONArray ordProdInstAccNumRelates) {
		this.ordProdInstAccNumRelates = ordProdInstAccNumRelates;
	}

	public List<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(List<Customer> customers) {
		this.customers = customers;
	}

	public String getPartyId() {
		return partyId;
	}

	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}

	public List<String> getAccNbrList() {
		return accNbrList;
	}

	public void setAccNbrList(List<String> accNbrList) {
		this.accNbrList = accNbrList;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

	public String getAuthSource() {
		return authSource;
	}

	public void setAuthSource(String authSource) {
		this.authSource = authSource;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getPartner() {
		return partner;
	}

	public void setPartner(String partner) {
		this.partner = partner;
	}

	public List<MultipleOffer> getGoodsInfo() {
		return goodsInfo;
	}

	public void setGoodsInfo(List<MultipleOffer> goodsInfo) {
		this.goodsInfo = goodsInfo;
	}

	public List<ContactInfos> getContactInfos() {
		return contactInfos;
	}

	public void setContactInfos(List<ContactInfos> contactInfos) {
		this.contactInfos = contactInfos;
	}

	public List<DevStaffInfos> getDevStaffInfos() {
		return devStaffInfos;
	}

	public void setDevStaffInfos(List<DevStaffInfos> devStaffInfos) {
		this.devStaffInfos = devStaffInfos;
	}
}
