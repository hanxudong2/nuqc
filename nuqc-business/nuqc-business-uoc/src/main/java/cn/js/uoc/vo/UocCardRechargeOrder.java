package cn.js.uoc.vo;

import java.io.Serializable;
import java.util.List;

public class UocCardRechargeOrder implements Serializable {
	private static final long serialVersionUID = 4935902674028030364L;
	// 主键 uuid
	private String id;
	// 渠道来源ID
	private String appId;
	// 充值流水(一次充值卡充值唯一)
	private String requestId;
	// 充值来源, 默认值:10001
	private String requestSource;
	// 充值方式 0:全量(一卡一充) 1:分批(多卡一充) 2:分拆(一卡多充)
	private String operationCode;
	// 登陆号码
	private String requestUser;
	// 充值号码区号
	private String areaCode;
	// 接入类型, 默认值:3
	private String accessType;
	// 状态 0-初始状态, 1-充值成功, 2-部分成功, 3-充值失败
	private int status;
	// 创建时间
	private String createTime;
	// 更新时间
	private String updateTime;
	// 充值明细
	private List<UocCardRechargeOrderDetail> uocCardRechargeOrderDetailList;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getRequestSource() {
		return requestSource;
	}

	public void setRequestSource(String requestSource) {
		this.requestSource = requestSource;
	}

	public String getOperationCode() {
		return operationCode;
	}

	public void setOperationCode(String operationCode) {
		this.operationCode = operationCode;
	}

	public String getRequestUser() {
		return requestUser;
	}

	public void setRequestUser(String requestUser) {
		this.requestUser = requestUser;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getAccessType() {
		return accessType;
	}

	public void setAccessType(String accessType) {
		this.accessType = accessType;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

	public List<UocCardRechargeOrderDetail> getUocCardRechargeOrderDetailList() {
		return uocCardRechargeOrderDetailList;
	}

	public void setUocCardRechargeOrderDetailList(List<UocCardRechargeOrderDetail> uocCardRechargeOrderDetailList) {
		this.uocCardRechargeOrderDetailList = uocCardRechargeOrderDetailList;
	}

}
